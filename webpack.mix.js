const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
// general

mix.js('resources/js/app.js', 'public/js/general.js')
    .sass('resources/sass/app.scss', 'public/css/general.css')
    // authentication
    .js('app/Modules/Auth/Resources/js/app.js', 'public/js/auth/app.js')
    .sass('app/Modules/Auth/Resources/css/app.scss', 'public/css/auth/app.css')
    // trainee
    .js('app/Modules/Trainee/Resources/js/app.js', 'public/js/trainee/app.js')
    .sass('app/Modules/Trainee/Resources/css/app.scss', 'public/css/trainee/app.css')
    //dashboard admin
    .js('app/Modules/Admindashboard/Resources/js/app.js','public/js/admin/dashboard/app.js')
    .sass('app/Modules/Admindashboard/Resources/css/app.scss','public/css/admin/dashboard/app.css');

mix.webpackConfig(webpack => {
    return {
        node: {fs: 'empty'},
        plugins: [
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery',
            })
        ]
    };
});
