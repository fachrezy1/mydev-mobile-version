@php($user = \Sentinel::check())
@php($helper = new \App\Helpers\UserRolesHelper )
@extends($helper->inAdmin() ? 'core.admin.main' :'core.trainee.main')
@section('head')
    <link rel="stylesheet" href="{{url('css/admin/dashboard/app.css')}}">
@endsection
@section('content')

    <main class="main-content">
        <div class="main-dashboard secondary-nav">
            <div class="dashboard-breadcrumb profile">
                <div class="container">
                    <h1>Profile</h1>
                </div>
            </div>
            <div class="container">

                <div class="profile">
                    @if(!$helper->inAdmin())
                        <div class="top-profile">
                            <div class="profile-stats">
                                <div class="circle-badge">
                                    <i class="icon-training"></i>
                                </div>
                                <span class="numeric">{{$completed_trainee}}</span>
                                <h3>Trainee Completed</h3>
                            </div>
                            <div class="profile-stats">
                                <div class="circle-badge">
                                    <i class="icon-trophy-1"></i>
                                </div>
                                <span class="numeric">{{$employee['points']}}</span>
                                <h3>Point(s)</h3>
                            </div>
                            <div class="profile-stats">
                                <div class="circle-badge">
                                    <i class="icon-time"></i>
                                </div>
                                <span class="numeric">{{$enrolled_trainee}}</span>
                                <h3>Trainee Enrolled</h3>
                            </div>
                        </div>
                    @endif
                    <div class="profile-box">
                        <div class="left-profile">
                            <h5 class="d-none d-md-block">Profile Menu</h5>
                            <ul class="nav nav-pills nav-pills-icons" id="tab-nav" role="tablist">

                                <li class="nav-item">
                                    <a class="nav-link active show" href="#myProfile" role="tab" data-toggle="tab">
                                        <i class="icon-user-3"></i> My Profile
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#myLog" role="tab" data-toggle="tab">
                                        <i class="icon-settings-1"></i> Log Activity
                                    </a>
                                </li>
                                @if(!$helper->inAdmin())
                                <li class="nav-item">
                                    <a class="nav-link" href="#items" role="tab" data-toggle="tab">
                                        <i class="icon-conference"></i> Item History
                                    </a>
                                </li>
                                @endif
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('logout')}}">
                                        <i class="icon-power"></i> Logout
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="right-profile">

                            <div class="profile-content">
                                <div class="tab-content tab-space">
                                    <div class="tab-pane active" id="myProfile">
                                        <div class="profile-title">
                                            <h3>My Profile</h3>
                                            <p>Your profile detail</p>
                                        </div>
                                        <div class="profile-entry">
                                            <div class="form-group reset px-4">
                                                <label class="control-label">
                                                    Name
                                                </label>
                                                <div class="form-field">
                                                    {{$employee['full_name']}}
                                                </div>
                                            </div>
                                            @if(!$helper->inAdmin())
                                            <div class="form-group reset px-4">
                                                <label class="control-label">
                                                    NIK
                                                </label>
                                                <div class="form-field">
                                                    {{$employee['nik']}}
                                                </div>
                                            </div>
                                            <div class="form-group reset px-4">
                                                <label class="control-label">
                                                    Position
                                                </label>
                                                <div class="form-field">
                                                    {{$employee['position']}}
                                                </div>
                                            </div>
                                            <div class="form-group reset px-4">
                                                <label class="control-label">
                                                    Division
                                                </label>
                                                <div class="form-field">
                                                    {{$employee['division']}}
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="myLog">
                                        <div class="profile-title">
                                            <h3>Log Activity</h3>
                                            <p>Here you can see your activities</p>
                                        </div>
                                        <div class="profile-entry clearfix">
                                            <table id="activityTable" class="table table-hover table-striped table-lite" cellspacing="0" width="100%">
                                                <thead>
                                                <tr>
                                                    <th class="all">Type</th>
                                                    <th class="desktop">Detail</th>
                                                    <th class="all">Status</th>
                                                    <th class="desktop">At</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="items">
                                        <div class="profile-title">
                                            <h3>Items History</h3>
                                            <p>Here you can see items history</p>
                                        </div>
                                        <div class="profile-entry clearfix">
                                            <table id="itemTable" class="table table-hover table-striped table-lite" cellspacing="0" width="100%">
                                                <thead>
                                                <tr>
                                                    <th class="all">Name</th>
                                                    <th class="desktop">Image</th>
                                                    <th class="desktop">Detail</th>
                                                    <th class="desktop">Point Info</th>
                                                    <th class="desktop">At</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection
@section('styles')
<style>
	@media(width: 768px) {
		.secondary-nav {
			padding-top: 0!important
		}
	}
	@media(max-width: 767px) {
		.secondary-nav {
			padding-top: 68px!important
		}
		.left-profile .nav-item {
			flex-direction: row;
			width: 25%;
		}
		.left-profile .nav-link{
			text-align: center!important;
			line-height: 16px!important;
			white-space: nowrap;
		}
		.left-profile .nav-link i {
			text-align: center;
			display: block;
			margin: 0 auto;
		}

		.right-profile {
			border-left: none transparent;
		}
		.profile-title {
			border-bottom: none transparent
		}
	}

	.left-profile .nav-pills .nav-item .nav-link.active:hover,
	.left-profile .nav-pills .nav-item .nav-link.active:hover i {
		color: #fff!important
	}

</style>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $('#activityTable').DataTable( {
                "processing": true,
                "serverSide": true,
                "scrollX": true,
                "pageLength":5,
                "ajax": {
                    url: "{{route('profile.log.get')}}"
                },
            } )

            $('#itemTable').DataTable( {
                "processing": true,
                "serverSide": true,
                "scrollX": true,
                "pageLength":5,
                "ajax": {
                    url: "{{route('trainee.point.list')}}"
                },
            } );
        } );
    </script>
@endsection
