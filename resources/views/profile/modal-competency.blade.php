
<div class="modal fade" id="competencyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Competency to be developed</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body table-responsive">
                <table class="table" id="tableCompetencies" width="100%">
                    <thead>
                        <th>Category</th>
                        <th>Competency</th>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
