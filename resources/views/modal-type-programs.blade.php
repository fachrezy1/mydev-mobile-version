
<div class="modal fade" id="typeProgramsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Developement type</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body modal-competency">
                <div class="development-goal">
                    <div class="row">
                        <div class="col-12">
                            <h3 class="form-check-label mt-2 mb-1" style="font-weight: bold">
                                Certification
                            </h3>
                            <span>
                                    Training to fulfill specific profession's requirement & company's standard
                                </span><br><br>
                        </div>
                        <div class="col-12">
                            <h3 class="form-check-label mt-2 mb-1" style="font-weight: bold">
                                General training
                            </h3>
                            <span>
                                   Training for general skill upgrade
                                </span><br><br>
                        </div>
                        <div class="col-12">
                            <h3 class="form-check-label mt-2 mb-1" style="font-weight: bold">
                                Seminar/Conference
                            </h3>
                            <span>
                                   Formal presentation by Subject Matter Experts
                                </span><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
