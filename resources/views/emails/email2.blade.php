<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Email</title>
        <style type="text/css">
		
			#outlook a{padding:0;} 
			.ReadMsgBody{width:100%;} .ExternalClass{width:100%;} 
			.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} 
			body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}
			table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
			img{-ms-interpolation-mode:bicubic;} 

			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

		
			#bodyCell{padding:20px;}
			#templateContainer{width:600px;}

			body, #bodyTable{
				background-color:#DEE0E2;
			}

			
			#bodyCell{
				border-top:4px solid #BBBBBB;
			}

			
			#templateContainer{
				border:1px solid #BBBBBB;
			}

			
			h1{
				 color:#ed1c24;
				display:block;
				 font-family:Helvetica;
				 font-size:26px;
				 font-style:normal;
				 font-weight:bold;
				 line-height:100%;
				 letter-spacing:normal;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				 text-align:center;
			}

			
			h2{
				 color:#404040 !important;
				display:block;
				 font-family:Helvetica;
				 font-size:20px;
				 font-style:normal;
				 font-weight:bold;
				 line-height:100%;
				 letter-spacing:normal;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				 text-align:center;
			}

			
			h3{
				 color:#606060 !important;
				display:block;
				 font-family:Helvetica;
				 font-size:16px;
				 font-style:normal;
				 font-weight:normal;
				 line-height:100%;
				 letter-spacing:normal;
				margin-top:0;
				margin-right:0;
				margin-bottom:24px;
				margin-left:0;
				 text-align:center;
                 color: #909090;
			}

			
			h4{
				 color:#808080 !important;
				display:block;
				 font-family:Helvetica;
				 font-size:14px;
				 font-style:italic;
				 font-weight:normal;
				 line-height:100%;
				 letter-spacing:normal;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				 text-align:left;
			}

			#email-logo{
				width : 100px;
			}

			

			#templatePreheader{
				background-color: #ffffff;
    			border-bottom: 3px solid #ED1C25;
			}

			
			.preheaderContent{
				 color:#808080;
				 font-family:Helvetica;
				 font-size:10px;
				 line-height:125%;
				 text-align:left;
			}

			
			.preheaderContent a:link, .preheaderContent a:visited, .preheaderContent a .yshortcuts {
				 color:#606060;
				 font-weight:normal;
				 text-decoration:underline;
			}

			
			#templateHeader{
				 background-color:#F4F4F4;
				 border-top:1px solid #FFFFFF;
				 border-bottom:1px solid #CCCCCC;
			}

			
			.headerContent{
				 color:#505050;
				 font-family:Helvetica;
				 font-size:20px;
				 font-weight:bold;
				 line-height:100%;
				 padding-top:0;
				 padding-right:0;
				 padding-bottom:0;
				 padding-left:0;
				 text-align:left;
				 vertical-align:middle;
			}

			
			.headerContent a:link, .headerContent a:visited {
				 color:#EB4102;
				 font-weight:normal;
				 text-decoration:underline;
			}

			#headerImage{
				height:auto;
				max-width:600px;
			}

			

			#templateBody{
				 background-color:#fff;
				 border-top:1px solid #f4f4f4;
				 border-bottom:1px solid #e5e5e5;
			}

			
			.bodyContent{
				 color:#505050;
				 font-family:Helvetica;
				 font-size:14px;
				 line-height:150%;
				padding-top:20px;
				padding-right:20px;
				padding-bottom:20px;
				padding-left:20px;
				 text-align:left;
			}

			
			.bodyContent a:link, .bodyContent a:visited,  .bodyContent a .yshortcuts {
				color:#ed1c24;
			}

            .bodyContent a:link:hover, .bodyContent a:visited:hover,  .bodyContent a:hover .yshortcuts {
                color: #fcd401;
            }

			.bodyContent img{
				display:inline;
				height:auto;
				max-width:560px;
			}
		
			#templateFooter{
				background-color:#ed1c24;
			}

			.footerContent{
				color : #fff;
				font-family:Helvetica;
				font-size:10px;
				line-height:150%;
				padding-top:20px;
				padding-right:20px;
				padding-bottom:20px;
				padding-left:20px;
				text-align:center;
			}

			.footerContent a:link, .footerContent a:visited, .footerContent a .yshortcuts, .footerContent a span{
				color:#606060;
				font-weight:normal;
				text-decoration:underline;
			}

			

            @media only screen and (max-width: 480px){
				
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} 
                body{width:100% !important; min-width:100% !important;} 

			
				#bodyCell{padding:10px !important;}

				

				#templateContainer{
					max-width:600px !important;
					 width:100% !important;
				}

			
				h1{
					 font-size:24px !important;
					 line-height:100% !important;
				}

				
				h2{
					 font-size:20px !important;
					 line-height:100% !important;
				}

				
				h3{
					 font-size:18px !important;
					 line-height:100% !important;
				}

				
				h4{
					 font-size:16px !important;
					 line-height:100% !important;
				}

				

				#templatePreheader{display:none !important;} 

				
				#headerImage{
					height:auto !important;
					 max-width:600px !important;
					 width:100% !important;
				}

				
				.headerContent{
					 font-size:20px !important;
					 line-height:125% !important;
				}

			

				#bodyImage{
					height:auto !important;
					 max-width:560px !important;
					 width:100% !important;
				}

				
				.bodyContent{
					 font-size:18px !important;
					 line-height:125% !important;
				}

				

				
				.footerContent{
					 font-size:14px !important;
					 line-height:115% !important;
				}

				.footerContent a{display:block !important;} 
				.bodyContent h1,
				h1 {
					color: #ed1c24;
				}

               

                a.btn-email,
                .bodyContent .btn-email {
                    font-style: normal;
                    text-decoration: normal !important;
                }
			}
		</style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
            	<tr>
                	<td align="center" valign="top" id="bodyCell">
                    
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templatePreheader">
                                        <tr>
                                            <td valign="top" class="preheaderContent" style="padding-top:10px; padding-right:20px; padding-bottom:10px; padding-left:20px;" mc:edit="preheader_content00">
												<img id="email-logo" style="width : 100px;" src="{{ url('/images/logo.png') }}" alt="ooredoo" class="img-fluid">
                                            </td>
                                          
                                            <td valign="top" width="180" class="preheaderContent" style="padding-top:10px; padding-right:20px; padding-bottom:10px; padding-left:0;" mc:edit="preheader_content01">
                                                Email not displaying correctly?<br /><a href="#0" target="_blank">View in your browser</a>.
                                            </td>
                                          
                                        </tr>
                                    </table>
                                  
                                </td>
                            </tr>
                        	
                        	<tr>
                            	<td align="center" valign="top">
                                
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td valign="top" class="bodyContent" mc:edit="body_content00">
                                                <h1>{{$mail['message_header']}}</h1>

												@if(!empty($mail['form_group']))
													<h3>{{$mail['form_group']}} Form</h3>
												@endif

												{!! $mail['message_body'] !!}
												
												@if(!empty($mail['url']))
													<a href="{{url($mail['url'])}}" class="btn-email">Click here for detail</a>
												@endif
                                            </td>
                                        </tr>
                                    	
                                       
                                    </table>
                                   
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter">
                                        <tr>
                                            <td valign="top" class="footerContent" mc:edit="footer_content00">
												PT. Indosat, Tbk © {{ date('Y') }} All Right Reserved
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                       
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>
