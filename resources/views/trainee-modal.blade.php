
<div class="modal fade" id="assignment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">How Was The Overall Sessions</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('trainee.rating.submit')}}" method="post">
                    @csrf
                    <input type="hidden" name="staff_id" class="rating_staff_id" id="rating_staff_id" value="">
                    <input type="hidden" name="mentor_id" class="rating_mentor_id" id="rating_mentor_id" value="">
                    <input type="hidden" name="request_group_id" class="rating_request_group" id="rating_request_group" value="">
                    <div class="form-group reset ">
                        <div class="check-group">
                            <div class="form-group reset w-100">
                                <label class="control-label">Rating</label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="rating" type="radio" value="1" checked=""> Terrible
                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="rating" type="radio" value="2"> Poor
                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="rating" type="radio" value="3"> OK
                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="rating" type="radio" value="4"> Near Perfect
                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="rating" type="radio" value="5"> Perfect
                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                </label>
                            </div>
                        </div>
                        <div class="form-field">
                            <textarea class="form-control counter-text" name="notes" required rows="6" placeholder="Write A review"></textarea>
                            <p class="form-helper text-count"><span class="counter-text-value">0</span>/255</p>
                        </div>
                    </div>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-default"> Submit </button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- new -->
<div class="modal fade" id="assignment2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">How Was The Overall Sessions</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('trainee.rating.submit2')}}" method="post">
                    @csrf
                    <input type="hidden" name="staff_id" class="rating_staff_id" id="rating_staff_id" value="">
                    <input type="hidden" name="mentor_id" class="rating_mentor_id" id="rating_mentor_id" value="">
                    <input type="hidden" name="request_group_id" class="rating_request_group" id="rating_request_group" value="">
                    <div class="form-group reset ">
                        <div class="check-group">
                            <div class="form-group reset w-100">
                                <label class="control-label">Rating</label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="rating" type="radio" value="1" checked=""> Terrible
                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="rating" type="radio" value="2"> Poor
                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="rating" type="radio" value="3"> OK
                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="rating" type="radio" value="4"> Near Perfect
                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="rating" type="radio" value="5"> Perfect
                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                </label>
                            </div>
                        </div>
                        <div class="form-field">
                            <textarea class="form-control counter-text" name="notes" required rows="6" placeholder="Write A review"></textarea>
                            <p class="form-helper text-count"><span class="counter-text-value">0</span>/255</p>
                        </div>
                    </div>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-default"> Submit </button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end new -->

<div class="modal fade" id="mentoringForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">How Was The Overall Sessions</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('trainee.rating.submit')}}" method="post">
                    @csrf
                    <input type="hidden" name="staff_id" class="rating_staff_id" id="rating_staff_id" value="">
                    <input type="hidden" name="mentor_id" class="rating_mentor_id" id="rating_mentor_id" value="">
                    <input type="hidden" name="request_group_id" class="rating_request_group" id="rating_request_group" value="">
                    <div class="form-group reset ">
                        <div class="check-group">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="rating" type="radio" value="1" checked=""> Terrible
                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="rating" type="radio" value="2"> Poor
                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="rating" type="radio" value="3"> OK
                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="rating" type="radio" value="4"> Near Perfect
                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="rating" type="radio" value="5"> Perfect
                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                </label>
                            </div>
                        </div>
                        <div class="form-field">
                            <textarea class="form-control counter-text" name="notes" rows="6" placeholder="Write a review"></textarea>
                            <p class="form-helper text-count"><span class="counter-text-value">0</span>/255</p>
                        </div>
                    </div>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-default"> Confirm </button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="trainingForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">How Was The Overall Sessions</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('trainee.rating.submit')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="staff_id" class="rating_staff_id" id="rating_staff_id" value="">
                    <input type="hidden" name="mentor_id" class="rating_mentor_id" id="rating_mentor_id" value="">
                    <input type="hidden" name="request_group_id" class="rating_request_group" id="rating_request_group" value="">
                    <div class="form-group reset ">
                        <div class="check-group">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="rating" type="radio" value="1" checked=""> Terrible
                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="rating" type="radio" value="2"> Poor
                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="rating" type="radio" value="3"> OK
                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="rating" type="radio" value="4"> Near Perfect
                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="rating" type="radio" value="5"> Perfect
                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                </label>
                            </div>
                        </div>
                        <div class="form-field">
                            <textarea class="form-control counter-text" name="notes" rows="6" placeholder="Write a review"></textarea>
                            <p class="form-helper text-count"><span class="counter-text-value">0</span>/255</p>
                        </div>
                    </div>
                    <div class="form-group reset split">
                        <div class="form-field">
                            <input id="files" type="file" accept=".pdf,doc,docx,image/*" name="attachment" class="btn btn-default reset" required>
                            <button type="button" class="btn btn-default reset">
                                <label for="files" style="margin-bottom: 0;width: 100%; padding: 0;"><i class="icon-contact"></i> Add attachment</label>
                            </button><br>
                            <small class="name-files">Brochure/Proposal of Training , file pdf,doc,docx,jpeg,jpg and png</small>
                        </div>
                    </div>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-default"> Submit </button>
                </form>
            </div>
        </div>
    </div>
</div>
