@php
    $user = \sentinel::check()->toArray();
    $trainee = \App\Modules\Trainee\Http\Controllers\DashboardController::getLeaderboards();
@endphp

<aside class="right-side">
    <div class="user-side">
        <div class="top-side">
            <h3>My Point</h3>
            <a href="javascript:void(0);" class="btn-fba hide-side">
                <i class="icon-next"></i>
            </a>
            <div class="point-user">
                <div class="point-circle">
                    <div class="point-data">
                        <div class="point-numeric">
                            {{ get_user_point($user['id']) }}
                            <span>Total Points</span>
                        </div>
                    </div>
                </div>
            </div>
            <a href="{{route('trainee.point.redeem')}}" class="redeem-btn" style="background-color:#ED1C24;color: #FFFFFF;">Redeem Item</a>
        </div>
        <div class="recent-act">
            <h4 class="side-title">Point Leaderboards</h4>
            @foreach($trainee as $item)
            <div class="trainee-list">
              <span class="circle-icon">
                <i class="mdi mdi-account-check"></i>
              </span>
                <div class="list-description">
                    <h5>{{ $item['point'] }} Points </h5>
                    <span> <b>{{$item['first_name']}} ({{$item['username']}})</b> </span>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</aside>
