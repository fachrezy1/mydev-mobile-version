@php($helper = new \App\Helpers\UserRolesHelper )
@php($user = \Sentinel::check())
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, viewport-fit=cover"/>
    <meta name="theme-color" content="#fff" />
  <meta name="csrf" value="{{ csrf_token() }}">
    <meta name="format-detection" content="telephone=no" />
    <title>Trainee Request-Indosat ooredoo</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="apple-touch-icon" sizes="180x180" href="{{url('images/touch/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{url('images/touch/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="194x194" href="{{url('images/touch/favicon-194x194.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{url('images/touch/android-chrome-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('images/touch/favicon-16x16.png')}}">
    <link rel="mask-icon" href="{{url('images/touch/safari-pinned-tab.svg')}}" color="#5bbad5">
    <link rel="shortcut icon" href="{{url('images/touch/favicon.ico')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{url('images/touch/mstile-144x144.png')}}">
    <meta name="msapplication-config" content="{{url('images/touch/browserconfig.xml')}}">
    <meta name="theme-color" content="#ffffff">

    <link href="{{url('css/vendor.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="{{url('css/main.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="{{url('css/device.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="{{url('css/custom-dev.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
    <!--
      <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.css" type="text/css" rel="stylesheet" media="screen,projection"/>
      <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
  -->
     <link href="{{url('css/select2.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
     <link href="{{url('css/select2-bootstrap-theme/select2-bootstrap.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
     <link href="{{url('css/glyphicon.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>


    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
      <input type="hidden" name="baseUrl" value="{{url('/')}}">
    
    @yield('styles')
  </head>
  <body class="{{$helper->inAdmin() ? 'for-hr for-admin' : ($helper->inManager() ? 'for-hr' : 'coachee')}}">
    <div class="main-site">
        @include('core.trainee.header')
        
        @yield('content')

        @include('core.footer')
    </div>

    <!-- jquery block -->
       <script type="text/javascript" src="{{url('js/jquery.blockUI.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.validate.min.js')}}"></script>
    <!-- end jquery validate -->

    <aside class="mobile-side">
        <div class="sidebar-box mobile">
            <div class="top-mob">
                <div class="top-mob-nav">
                    <h3>Ooredoo</h3>
                    <button type="button" class="btn btn-default btn-side">
                        <i class="icon-close"></i>
                    </button>
                </div>
                <div class="mob-name">
                    <h4>Hi, {{$user->first_name}}</h4>
                </div>
            </div>

            <div class="mid-mob">
                <div class="mid-nav">
                    <a href="{{ route('trainee.point.redeem') }}" class="col-12">
                        <span class="numeric">{{$user->point}}</span>
                        <em>Points</em>
                    </a>
                </div>
                <h4 class="text-uppercase">Quick Menu</h4>
                <nav class="mob-list">
                    <a href="{{route('profile.index')}}" class="link-notif">
                     <span>
                      <i class="icon-settings-1"></i> Profile
                    </span>
                    </a>
                    <a href="{{route('logout')}}" class="link-notif">
                     <span>
                      <i class="icon-power"></i> Logout
                    </span>
                    </a>
                </nav>
            </div>
        </div>
    </aside>

    @if(!in_array(\Route::current()->getName(),['auth.choose']))
      <div class="bottom-nav">
        <nav class="bottom-menu">
        @if ($helper->inManager() || $helper->inAdmin())
          <button class="btn btn-iconic {{ (\Route::current()->getName()== 'dashboard.traineeRequest') ? 'active' :'' }}" onclick="location.href='{{ route('dashboard.traineeRequest') }}'">
            <i class="icon-home">
            </i>
            <span>Dashboard
            </span>
          </button>
        @else
        <button class="btn btn-iconic {{ (\Route::current()->getName()== 'trainee.dashboard.index') ? 'active' :'' }}" onclick="location.href='{{ route('trainee.dashboard.index') }}'">
            <i class="icon-home">
            </i>
            <span>Dashboard
          </span>
        </button>
        @endif
          <button class="btn btn-iconic {{ (\Route::current()->getName()== 'trainee.myTrainee') ? 'active' :'' }}" onclick="location.href='{{ route('trainee.myTrainee') }}'">
            <i class="icon-conference">
            </i>
            <span>Training History
            </span>
          </button>
          <button class="btn btn-iconic {{ (\Route::current()->getName()== 'trainee.index') ? 'active' :'' }}" onclick="location.href='{{ route('trainee.index') }}'">
            <i class="icon-podium">
            </i>
            <span>Create Request
            </span>
          </button>
          <button class="btn btn-iconic {{ (\Route::current()->getName()== 'trainee.point.redeem') ? 'active' :'' }}" onclick="location.href='{{ route('trainee.point.redeem') }}'">
            <i class="icon-note-1">
            </i>
            <span>Redeem Point
            </span>
          </button>
        </nav>
      </div>
    @endif

<!-- buat block -->
<script type="text/javascript">
$(document).ready(function(){
  $(".btn_block").click(function(){
    $.blockUI({ 
        message:"<img src='{{ url('images/loading.gif') }}' style='width:80px;height:80px;'><br>Loading...",
        css: {color : '#ffffff',
        background: 'none',
        border : 'none'}
    });
  });
});
</script>
<!-- end block -->

  </body>

</html>
