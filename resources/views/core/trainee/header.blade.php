@php
$user = \Sentinel::check();
$notifications = App\User::find($user->id);
    //dd($notifications->notifications->take(5))
@endphp
@php($helper = new \App\Helpers\UserRolesHelper )

@if(!in_array(\Route::current()->getName(),['trainee.assignment','trainee.coaching','trainee.mentoring','trainee.externalSpeaker','trainee.internalCoach','trainee.inhouse', 'trainee.publicTrainingStep1']))
<header class="main-header {{ (\Route::current()->getName() == 'trainee.index') ? 'clean' : '' }}">
	<div class="container-fluid">
		<div class="top-header">

			<button type="button" class="btn btn-mob btn-side mobile">
				<i class="icon-list-menu"></i>
			</button>
			@if ($helper->inManager() || $helper->inAdmin())
			<a href="{{ route('dashboard.traineeRequest') }}" class="main-brand reset">
				<img src="{{ url('/images/logo.png') }}" alt="ooredoo">
			</a>
			@else
			<a href="{{ url('/') }}" class="main-brand reset">
				<img src="{{ url('/images/logo.png') }}" alt="ooredoo">
			</a>
			@endif

			<div class="mobile">
				<button type="button" class="btn btn-mob" onclick="location.href='{{route('trainee.notification.index')}}'">
					<i class="icon-bell"></i>
				</button>
			</div>
			@if ( ($helper->inStaff()) && (in_array(\Route::current()->getName(), ['trainee.report.index','profile.index','dashboard.trainee.history.list','trainee.reviewByUserId','trainee.myAchievement','trainee.point.show','trainee.point.redeem','trainee.notification.index','trainee.dashboard.index','trainee.myTrainee','trainee.review','trainee.preview'])))
			<nav class="main-nav ml-auto">

				@if ($helper->inManager() || $helper->inAdmin())
				<a href="{{ route('dashboard.traineeRequest') }}" class="{{ (\Route::current()->getName()== 'dashboard.traineeRequest') ? 'active' :'' }}">Dashboard</a>
				@else
				<a href="{{ route('trainee.dashboard.index') }}"  class="{{ (\Route::current()->getName()== 'trainee.dashboard.index') ? 'active' :'' }}">Dashboard</a>
				@endif
				<a href="{{ route('trainee.myTrainee') }}" class="{{ (\Route::current()->getName()== 'trainee.myTrainee') ? 'active' :'' }}">Training History</a>
				<!-- <a href="{{ route('trainee.report.index') }}">Report</a> -->
				<a href="{{ route('trainee.point.redeem') }}" class="{{ (\Route::current()->getName()== 'trainee.point.redeem') ? 'active' :'' }}" >Redeem Point</a>
				<a href="{{ route('trainee.index') }}">Create Requests</a>
			</nav>

			<div class="top-user">
				<div class="dropdown notif active">
					<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="icon-bell"></i>
					</button>

					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

						@if ($notifications->unreadNotifications->isEmpty() && $notifications->notifications->isEmpty())
						<a class="dropdown-item" href="javascript:void(0)">
							<span class="top-notif">
								<i class="icon-spam"></i>
								<span class="notif-short">
									<span class="small-text">
										Empty
									</span>
									<span class="notif-short-text">
										You have no notifications yet &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</span>
								</span>
							</span>
						</a>
						@else
						@php($unread = [])
						@if($notifications->unreadNotifications->isNotEmpty())
						<span class="text-center">
							<h6>New Notifications</h6>
						</span>
						@endif
						@foreach($notifications->unreadNotifications->take(5) as $notification)
						@if($notification->data['type'] != 'form manage request')
						@php(array_push($unread,$notification->id))
						@php($notif = $notification->data)
						<a class="dropdown-item" href="{{route('trainee.notification.redirect',$notification->id).'?url='.urlencode($notif['url']).''}}">
							<span class="top-notif">
								<i class="icon-spam"></i>
								<span class="notif-short">
									<span class="small-text">
										{{\Carbon\Carbon::createFromTimestamp($notif['message_date'])}}
									</span>
									<span class="notif-short-text">
										{!! strip_tags($notif['message_body']) !!}
									</span>
								</span>
							</span>
						</a>
						@endif
						@endforeach
						@php($first = true)
						@foreach($notifications->notifications->take(5) as $notification)
						@if($notification->data['type'] != 'form manage request')
						@if (!in_array($notification->id,$unread))
						@if($notifications->unreadNotifications->isNotEmpty() && $first)
						@php($first = false)
						<span class="text-center">
							<h6>All Notifications</h6>
						</span>
						@endif
						@php($notif = $notification->data)
						<a class="dropdown-item" href="{{route('trainee.notification.redirect',$notification->id).'?url='.urlencode($notif['url']).''}}">
							<span class="top-notif">
								<i class="icon-spam"></i>
								<span class="notif-short">
									<span class="small-text">
										{{\Carbon\Carbon::createFromTimestamp($notif['message_date'])}}
									</span>
									<span class="notif-short-text">
										{!! strip_tags($notif['message_body']) !!}
									</span>
								</span>
							</span>
						</a>
						@endif
						@endif
						@endforeach
						@endif
						<a class="dropdown-item" href="{{route('trainee.notification.index')}}">
							<span class="top-notif text-center">
								View all Notification
							</span>
						</a>
					</div>
				</div>
				<div class="dropdown user">
					<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<img src="{{ url('/images/profile.jpeg') }}" alt="" class="img-circle"> {{$user->first_name}}
					</button>
					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
						<a class="dropdown-item" href="{{route('profile.index')}}">Profile</a>
						<a class="dropdown-item" href="{{url('logout')}}">Sign out</a>
					</div>
				</div>
			</div>
			@elseif(in_array(\Route::current()->getName(), ['auth.choose']))
			<nav class="main-nav">
				<a href="{{ route('logout') }}">Logout</a>
			</nav>
			@else
			@if ($helper->inManager() || $helper->inAdmin())
			<nav class="main-nav">
				<a href="{{ route('dashboard.traineeRequest') }}" class="{{ (\Route::current()->getName()== 'dashboard.traineeRequest') ? 'active' :'' }}">Dashboard</a>
			</nav>
			@else
			<nav class="main-nav">
				<a href="{{ route('trainee.dashboard.index') }}" class="{{ (\Route::current()->getName()== 'trainee.dashboard.index') ? 'active' :'' }}">Dashboard</a>
			</nav>
			@endif
			@endif
		</div>
	</div>
</header>
@endif
