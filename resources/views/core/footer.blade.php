<footer>
    {{--<script src="{{ url('js/popper.min.js') }}"></script>--}}
    {{--<script src="{{ url('js/jquery.min.js') }}"></script>--}}
    {{--<script src="{{ url('js/jquery-3.2.1.slim.min.js') }}" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>--}}
    <script src="{{ url('js/jquery-3.3.1.min.js') }}" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="{{ url('js/popper.js') }}" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>

    {{--<script src="{{ url('js/bootstrap_footer.min.js') }}" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>--}}
    <script src="{{ url('js/lib/moment.min.js') }}"></script>
    <script src="{{ url('js/lib/bootstrap-datetimepicker.js') }}"></script>
    <script src="{{ url('js/bootstrap-material-design.js') }}" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>

    {{--    <script src="{{ url('js/bootstrap.min.js') }}"></script>--}}

    {{--<script src="{{ url('js/dashboard-user.js') }}"></script>--}}
    <script src="{{ url('js/lib/owl.carousel.js') }}"></script>
    {{--<script src="{{ url('js/lib/chart.js') }}"></script>--}}
    <script src="{{ url('js/Chart.min.js') }}"></script>
    <script src="{{ url('js/lib/jquery.slimscroll.js') }}"></script>

    <!-- new -->
    <script src="{{ url('js/sweetalert2.min.js') }}"></script>
    <!-- end new -->
    

    <script src="{{ url('js/lib/datatables.js') }}"></script>
    <script src="{{ url('js/lib/datatable/responsive/js/dataTables.responsive.js') }}"></script>
    <script src="{{ url('js/coach-form.js') }}"></script>
    <script src="{{ url('js/coachee.js') }}"></script>
    <script src="{{ url('js/review.js') }}"></script>
    <script src="{{ url('js/lib/datatable/jszip.min.js') }}"></script>
    <script src="{{ url('js/lib/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ url('js/lib/datatable/vfs_fonts.js') }}"></script>
    <script src="{{ url('js/mytrainee.js') }}"></script>

    <script src="{{ url('js/main.js') }}"></script>
    {{--<script src="{{ url('js/select2.full.min.js') }}"></script>--}}
    <script src="{{ url('js/select2.min.js') }}"></script>
    <script src="{{ url('js/custom-dev.js') }}"></script>

    {{--import excel fuction--}}
    <script src="{{url('js/canvas-datagrid.js')}}"></script>

    <script src="{{url('js/sheet/shim.js')}}"></script>
    <script src="{{url('js/xlsx.full.min.js')}}"></script>
    <script src="{{ url('js/sheet/dropsheet.js') }}"></script>
    <script src="{{ url('js/sheet/main.js') }}"></script>
    <script src="{{ url('js/sheet/spin.js') }}"></script>
    <script src="{{ url('js/lib/jspdf.js') }}"></script>
    <script src="{{ url('js/custom.js') }}"></script>
    {{--end import function--}}
  @yield('scripts')
  
  {{--<script src="{{ url('js/app.js') }}"></script>--}}
    <script>
        $(document).ready(function () {
            $('input').on('change',function () {
                let val = $(this).val();
                if (val != ''){
                    $(this).addClass('input-valid')
                } else {
                    $(this).removeClass('input-valid')
                }
            })
        });

        $(".add-more").click(function () {

            let clone = $(".field-group.has-more:last").clone();
            let counter = $('.more-item .field-group').length + 1;
            console.log(counter);
            clone.find('.select-employee-name')
                .removeClass('select-employee-name')
                .addClass('select-employee-name'+counter);

            clone.find('.nik_coachee')
                .removeClass('nik_coachee')
                .addClass('nik_coachee'+counter);

            clone.find('.position_employee')
                .removeClass('position_employee')
                .addClass('position_employee'+counter);

            clone.find('.select-employee-name'+counter)
                .attr('type','hidden');

            clone.find('select').remove();
            clone.find('.select2').remove();

            $('<select class="form-control select-employee'+counter+'"></select>').insertAfter(clone.find('.control-label')[0]);

            clone.find('.select-employee'+counter).select2({
                theme: "bootstrap",
                ajax: {
                    url: baseUrl+'/trainee/request/employee-request',
                    dataType: 'json',
                    type:'GET',
                    delay: 250,
                    data: function (params) {
                        return {
                            name: params.term
                        }
                    },
                    placeholder: 'Search for a repository',
                    // escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                    // minimumInputLength: 1,
                }
            });
            clone.find('.select-employee'+counter).on("select2:select", function(e) {
                let nik = e.params.data.id;
                let position = e.params.data.position;
                // console.log(e.params.data.text);
                $('.select-employee-name'+counter).val(e.params.data.text);
                // console.log(nik);
                // $('#nik_coachee').val(nik);
                $('.nik_coachee'+counter).val(nik);

                if($('.position_employee'+counter).length > 0) {
                    $('.position_employee'+counter).val(position)
                }
            });

            clone.find('input').val('');
            clone.find('textarea').val('');
            clone.find('.remove-item').addClass('d-block');
            clone.appendTo(".more-item-target");
            $('.more-item-target').find('.field-group').removeClass('has-more');

            $(".more-item-target").on('click', '.remove-item', function () {
                // $(".more-item-target :last").remove();
                $(this).parents('.field-group').remove();
            });

        });

        $('.venue_button').unbind('click').on('click',function () {
            // console.log('ada');

            $('.venue_button').removeClass('active');

            $(this).addClass('active');

            let target = $(this).data('target');
            // .click();
            if ($(target).attr('checked')) {
                $(target).removeAttr('checked');
            } else {
                $(target).attr('checked', 'checked');
            }
            // $('.venue_button').toggleClass('ada');
            // $(this).toggleClass('ada');
        })
    </script>
</footer>
