@php
$user = \Sentinel::check();
$notifications = App\User::find($user->id);
@endphp
@php($helper = new \App\Helpers\UserRolesHelper )
<header class="main-header">
	<div class="container-fluid">
		<div class="top-header">
			<button type="button" class="btn btn-mob btn-side mobile">
				<i class="icon-list-menu"></i>
			</button>
			<a href="{{ route('dashboard.dashboardIndex') }}" class="main-brand">
				<img src="{{url('images/logo.png')}}" alt="ooredoo" class="img-fluid">
			</a>
			<div class="d-flex">
				<nav class="main-nav mr-md-4">
					<a href="{{ route('dashboard.dashboardIndex') }}" class="{{ (\Route::current()->getName()== 'dashboard.dashboardIndex') ? 'active' :'' }}">Dashboard</a>
					@if ($helper->inAdmin())

					<a href="{{ route('dashboard.traineeRequest') }}" class="{{ (\Route::current()->getName()== 'dashboard.traineeRequest') ? 'active' :'' }}">Trainee Request</a>
					<a href="{{ route('trainee.myTrainee') }}" class="{{ (\Route::current()->getName()== 'trainee.myTrainee') ? 'active' :'' }}">Create Trainee</a>

					@elseif ($helper->inManager() || $helper->inAdmin())
					<a href="{{ route('dashboard.traineeRequest') }}" class="{{ (\Route::current()->getName()== 'dashboard.traineeRequest') ? 'active' :'' }}">Trainee Request</a>
					@endif
					<!-- tambahan -->
					@if($helper->inAdmin())
					<a href="{{ route('trainee.request-admin') }}" class="{{ (\Route::current()->getName()== 'trainee.request-admin') ? 'active' :'' }}">Request For User</a>
					@else
					@endif
					<!-- end tambahan -->

					@if ($helper->inAdmin() || $helper->inManager())
					<a href="{{route('dashboard.report.index')}}" class="{{ (\Route::current()->getName()== 'dashboard.report.index') ? 'active' :'' }}">Report</a>
					@endif
					@if ($helper->inAdmin())

					@endif
					@if($helper->inAdmin())
					<a href="{{route('dashboard.setting.index')}}" class="{{ (\Route::current()->getName()== 'dashboard.setting.index') ? 'active' :'' }}">Setting</a>
					@endif
				</nav>
				<div class="mobile">
					<button type="button" class="btn btn-mob" onclick="location.href='{{route('trainee.notification.index')}}'">
						<i class="icon-bell"></i>
					</button>
				</div>
				<div class="top-user">
					<div class="dropdown notif active">
						<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="icon-bell"></i>
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

							@if ($notifications->unreadNotifications->isEmpty() && $notifications->notifications->isEmpty())
							<a class="dropdown-item" href="javascript:void(0)">
								<span class="top-notif">
									<i class="icon-spam"></i>
									<span class="notif-short">
										<span class="small-text">
											Empty
										</span>
										<span class="notif-short-text">
											You have no notifications yet &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</span>
									</span>
								</span>
							</a>
							@else
							@php($unread = [])
							@if($notifications->unreadNotifications->isNotEmpty())
							<span class="text-center">
								<h6>New Notifications</h6>
							</span>
							@endif
							@foreach($notifications->unreadNotifications->take(5) as $notification)
							@php(array_push($unread,$notification->id))
							@php($notif = $notification->data)
							{{--<script>--}}
								{{--console.log('{{$notif['url']}}')--}}
							{{--</script>--}}
							<a class="dropdown-item" href="{{route('trainee.notification.redirect',$notification->id).'?url='.urlencode($notif['url']).''}}">
								<span class="top-notif">
									<i class="icon-spam"></i>
									<span class="notif-short">
										<span class="small-text">
											{{\Carbon\Carbon::createFromTimestamp($notif['message_date'])}}
										</span>
										<span class="notif-short-text">
											{!! strip_tags($notif['message_body']) !!}
										</span>
									</span>
								</span>
							</a>
							@endforeach
							@php($first = true)
							@foreach($notifications->notifications->take(5) as $notification)
							@if (!in_array($notification->id,$unread) && (!in_array($notification->data['type'], ['form manage request', 'form request'])))
							@if($notifications->unreadNotifications->isNotEmpty() && $first)
							@php($first = false)
							<span class="text-center">
								<h6>All Notifications</h6>
							</span>
							@endif
							@php($notif = $notification->data)
							{{--<script>--}}
								{{--console.log('{{$notif['url']}}')--}}
							{{--</script>--}}
							<a class="dropdown-item" href="{{route('trainee.notification.redirect',$notification->id).'?url='.urlencode($notif['url']).''}}">
								<span class="top-notif">
									<i class="icon-spam"></i>
									<span class="notif-short">
										<span class="small-text">
											{{\Carbon\Carbon::createFromTimestamp($notif['message_date'])}}
										</span>
										<span class="notif-short-text">
											{!! strip_tags($notif['message_body']) !!}
										</span>
									</span>
								</span>
							</a>
							@endif
							@endforeach
							@endif


							<a class="dropdown-item" href="{{route('trainee.notification.index')}}">
								<span class="top-notif text-center">
									View all Notification
								</span>
							</a>
						</div>
					</div>
					<div class="dropdown user">
						<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<img src="{{ url('images/profile.jpeg') }}" alt="" class="img-circle"> {{$user->first_name}}
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="{{route('profile.index')}}">Profile</a>
							<a class="dropdown-item" href="{{url('logout')}}">Sign out</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
