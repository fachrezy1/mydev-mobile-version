@php($helper = new \App\Helpers\UserRolesHelper )
<!DOCTYPE html>
<html lang="en">
  <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, viewport-fit=cover"/>
        <meta name="theme-color" content="#fff" />
        <meta name="csrf" value="{{ csrf_token() }}">
        <meta name="format-detection" content="telephone=no" />
        <title>Trainee Request-Indosat ooredoo</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="apple-touch-icon" sizes="180x180" href="{{url('images/touch/apple-touch-icon.png')}}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{url('images/touch/favicon-32x32.png')}}">
        <link rel="icon" type="image/png" sizes="194x194" href="{{url('images/touch/favicon-194x194.png')}}">
        <link rel="icon" type="image/png" sizes="192x192" href="{{url('images/touch/android-chrome-192x192.png')}}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{url('images/touch/favicon-16x16.png')}}">
        <link rel="mask-icon" href="{{url('images/touch/safari-pinned-tab.svg')}}" color="#5bbad5">
        <link rel="shortcut icon" href="{{url('images/touch/favicon.ico')}}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{url('images/touch/mstile-144x144.png')}}">
        <meta name="msapplication-config" content="{{url('images/touch/browserconfig.xml')}}">
        <meta name="theme-color" content="#ffffff">

        <link href="{{url('css/vendor.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="{{url('css/main.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="{{url('css/device.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="{{url('css/custom.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="{{url('css/custom-dev.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>

        <!--
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    -->

       <link href="{{url('css/select2.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
     <link href="{{url('css/select2-bootstrap-theme/select2-bootstrap.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
     <link href="{{url('css/glyphicon.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>


				<!-- Datatables responsive -->
        <link href="{{url('js/lib/datatable/responsive/css/responsive.dataTables.css')}}" type="text/css" rel="stylesheet"/>

      {{--<script src="{{ url('js/jquery.min.js') }}"></script>--}}


    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
      <input type="hidden" name="baseUrl" value="{{url('/')}}">
    
    @yield('styles')
  </head>
  <body class="{{$helper->inAdmin() ? 'for-hr' : ($helper->inManager() ? 'for-hr' : 'coachee')}}">
    <div class="main-site">
        @yield('content')
        @include('core.footer',['helper'=>$helper])
    </div>
  </body>
</html>
