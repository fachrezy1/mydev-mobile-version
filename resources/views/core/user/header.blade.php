@section('style-header')
    <link rel="stylesheet" href="{{url('css/header.css')}}">
@endsection
@section('header')
<section class="container header">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container navbar-brand-container">
            <div class="row">
                <div class="col-12 header-icon">
                    <div class="header-icon_image">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP88ffHfwAJ0APuzD6SLQAAAABJRU5ErkJggg==">
                    </div>
                    <div class="header-icon_text">Indosat Ooredoo app name</div>
                </div>
            </div>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    </nav>
</section>
@endsection
