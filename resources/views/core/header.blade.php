<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container navbar-brand-container">
        <div class="row">
            <div class="col-12">
                <a class="navbar-brand" href="#">
                    <img class="img-fluid navbar-brand_img" src="{{url('/img/indosat-logo.png')}}">
                </a>
            </div>
            <div class="col-12">
                <hr>
            </div>
        </div>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    </div>
</nav>
