<div class="modal fade" id="ratingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Rating Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body modal-competency">
                <div class="development-goa">
                    <div class="row">
                        @if(!empty($form['form_detail']['rating_detail']))
                            @foreach($form['form_detail']['rating_detail'] as $ratingDetail)
                                <div class="form-check col-12 px-3">
                                    <h5 class="text-danger">
                                        {{$ratingDetail['full_name']}} ({{number_format($ratingDetail['rating'],'1')}}) :
                                    </h5>
                                    <span>
                                            {{$ratingDetail['notes']}}
                                        </span><br>
                                    @if($ratingDetail['attachment'])
                                    <a href="{{asset('uploads/'.$ratingDetail['attachment'])}}" target="_blank">
                                            See Attachment
                                        </a><br>
                                    @endif
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
