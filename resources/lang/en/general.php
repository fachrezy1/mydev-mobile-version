<?php
/**
 * Created by PhpStorm.
 * User: Tandang
 * Date: 27/01/2019
 * Time: 21:58
 */

return [

    /*
    |--------------------------------------------------------------------------
    | General Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'validation' => [
        'error' => 'Lengh must 256 character or less'
    ],
    'next' => 'Next &raquo;',

];
