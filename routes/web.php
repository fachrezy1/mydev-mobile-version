<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('auth.'));
});

Route::group(['prefix'=>'profile','as'=>'profile.', 'middleware' => 'sentinel.login'],function (){
   Route::get('/','ProfileController@index')->name('index');
   Route::group(['prefix'=>'log','as'=>'log.'],function (){
       Route::get('/','LogActivityController@index')->name('get');
   });
});

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});
