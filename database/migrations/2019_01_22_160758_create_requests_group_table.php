<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests_group', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('form_group_id')->unsigned();
            $table->tinyInteger('last_step')->nullable();
            $table->boolean('is_completed')->default(0);
            $table->boolean('is_completed_by_manager')->default(0);
            $table->boolean('is_completed_by_admin')->default(0);
            $table->boolean('is_rejected')->default(false);
            $table->boolean('is_accepted')->default(false);
            $table->string('first_accepter',15)->default(0);
            $table->tinyInteger('second_accepter')->default(0);
            $table->string('first_accepted',30)->default(null);
            $table->string('second_accepted',30)->default(null);
            $table->integer('accepter_id')->comment('Person who accept this requests');
            $table->date('accepted_at')->nullable();
            $table->string('rejected_notes',500)->nullable();
            $table->tinyInteger('rejected_by')->unsigned()->default(0)->comment('id from hr/manager');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests_group');
    }
}
