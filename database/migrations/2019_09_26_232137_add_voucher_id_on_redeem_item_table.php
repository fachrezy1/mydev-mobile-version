<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVoucherIdOnRedeemItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::table('item_redeem', function (Blueprint $table) {
            $table->string('voucher_code')->nullable();
         });
     }
 
     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::table('item_redeem', function (Blueprint $table) {
             $table->dropColumn('voucher_code');
         });
     }
}
