<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rating', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('staff_id')->default(0);
            $table->tinyInteger('mentor_id')->default(0);
            $table->tinyInteger('request_group_id')->default(0);
            $table->tinyInteger('form_type_id')->default(0);
            $table->tinyInteger('rating')->default(0);
            $table->string('attachment',80)->nullable()->default(null);
            $table->string('notes',500)->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rating');
    }
}
