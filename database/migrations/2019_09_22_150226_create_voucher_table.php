<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 300)->default(null);
            $table->text('image');
            $table->text('description')->nullable();
            $table->decimal('amount', 20, 2);
            $table->integer('point')->unsigned()->default(0);
            $table->integer('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('voucher_codes', function (Blueprint $table) {
            $table->integer('voucher_id')->default('0');
            $table->string('code', 300)->default(null);
            $table->integer('user_id')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher_codes');
        Schema::dropIfExists('voucher');
    }
}
