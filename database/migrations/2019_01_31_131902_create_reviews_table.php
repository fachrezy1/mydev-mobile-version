<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('request_group_id')->unsigned();
            $table->string('form_type',30)->nullable();
            $table->string('reviews_goal',500)->nullable();
            $table->string('reviews_result',500)->nullable();
            $table->string('reviews_feedback',500)->nullable();
            $table->string('reviews_action',500)->nullable();
            $table->date('reviews_date')->nullable();
            $table->string('reviews_duration',30)->nullable();
            $table->date('reviews_session_number')->nullable();
            $table->string('reviews_course_name',30)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
