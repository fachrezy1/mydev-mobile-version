<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsRejectedFieldToRequestsGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requests_group', function (Blueprint $table) {
            //
            $table->integer('is_rejected')->unsigned()->nullable();
            $table->integer('rejected_by')->unsigned()->nullable();
            $table->string('rejected_notes',500)->nullable();
            $table->date('rejected_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requests_group', function (Blueprint $table) {
            //
            $table->dropColumn('is_rejected');
            $table->dropColumn('rejected_by');
            $table->dropColumn('rejected_notes');
            $table->dropColumn('rejected_at');
        });
    }
}
