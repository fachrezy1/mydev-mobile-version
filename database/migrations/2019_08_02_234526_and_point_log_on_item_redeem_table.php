<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AndPointLogOnItemRedeemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_redeem', function (Blueprint $table) {
            $table->string('point_from',80)->nullable();
            $table->string('point_to',80)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_redeem', function (Blueprint $table) {
            $table->dropColumn('point_to');
            $table->dropColumn('point_from');
        });
    }
}
