<?php

use Illuminate\Database\Seeder;
use App\Modules\Trainee\Models\Competency;
use App\Modules\Trainee\Models\CompetencyCategory;
use Illuminate\Support\Facades\DB;

class CompetenciesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	DB::table("competency_category")->truncate();
    	DB::table("competency")->truncate();

        $cc = new CompetencyCategory;
        $cc->name = "ACCELERATE BUSINESS AND CUSTOMER";
        $cc->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Customer Focus";
	        $c->definition = "Ensuring that the internal or external customer’s perspective is a driving force behind strategic priorities, business decisions, organizational processes, and individual activities; crafting and implementing service practices that meet customers’ and own organization’s needs; promoting and operationalizing customer service as a value.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Business Savvy";
	        $c->definition = "Demonstrating  a keen understanding of basic business operations and the organizational levers (systems, processes, departments, functions) that drive profitable growth;  quickly able to  evaluate business plans and processes to identify data or recommendations that need further investigation.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Entrepreneurship";
	        $c->definition = "Using own understanding of key market drivers and calculated risk to create and seize business opportunities, expand into new markets, and launch new products, services, and/or profitable endeavors.";
	        $c->save();

        $cc = new CompetencyCategory;
        $cc->name = "CULTIVATE  NETWORKS & PARTNERSHIPS";
        $cc->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Building Networks";
	        $c->definition = "Taking action to establish and maintain connections with people outside one’s formal work group, including those outside the organization (e.g., peers, cross-functional partners, and vendors) who are willing and able to provide the information, ideas, expertise, and/or influence needed to achieve work goals.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Collaboration &  Partnership";
	        $c->definition = "Developing and leveraging relationships beyond hierarchy with  internal and external partners to take action in order to achieve results and/or address customers’ needs and/or advance sales";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Resolving Conflict";
	        $c->definition = "Helping others deal effectively with an antagonistic situation to minimize damage to the relationships and promote shared goals; using appropriate interpersonal methods to reduce tension or conflict between two or more people and facilitate agreement.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Valuing Diversity";
	        $c->definition = "Working effectively with individuals of diverse cultures, interpersonal styles, abilities, motivations, or backgrounds; seeks out and uses unique abilities, insights, and ideas from diverse individuals.";
	        $c->save();

	    $cc = new CompetencyCategory;
        $cc->name = "DECISIVENESS";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Demonstrating Decisiveness";
	        $c->definition = "Determining the best alternative and making  timely decision  after weighing alternative solutions against important decision criteria.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Involving Others";
	        $c->definition = "Involving others in the decision-making process as needed to obtain information, generate alternatives, make the best decision, and ensure buy-in; builds consensus when appropriate.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Making Connections";
	        $c->definition = "Securing and comparing information from multiple sources to identify business issues.";
	        $c->save();

	    $cc = new CompetencyCategory;
        $cc->name = "LEARNING AGILITY";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Continous Learning";
	        $c->definition = "Actively identifying new areas for learning; regularly creating and taking advantage of learning opportunities; learning through various ways of tools or methods, and using knowledge and skill on the job and learning through application in timely manner. ";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Leveraging Feedback";
	        $c->definition = "Taking full advantage of opportunities to receive and explore feedback about own performance (from assessments, managers, coworkers,internal/external partners, or customers); responding favorably to feedback and using it constructively to take action to improve knowledge, skills, behavior, and impact on others.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Technology Savvy";
	        $c->definition = "Leveraging one’s practical knowledge and understanding of recent technology tools, solutions, and trends to improve work results, solve work problems, and take advantage of new business opportunities.";
	        $c->save();

	    $cc = new CompetencyCategory;
        $cc->name = "MAKING A DIFFERENCE";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Building Trusting Relationship";
	        $c->definition = "";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Courage in Action";
	        $c->definition = "Taking prompt action to accomplish work goals; taking action to achieve results beyond what is required; being proactive through  positively confronting difficult issues; making valiant choices and taking bold action in the face of opposition or fear.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Leading Change";
	        $c->definition = "Adjusting effectively to change with  a positive mind-set and encouraging others to implement better approaches to address problems and opportunities; leading the implementation and acceptance of change within the workplace.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Resilience & Perseverance";
	        $c->definition = "Staying with a position or plan of action until the desired objective is obtained or is no longer reasonably attainable; Able to handling disappointment and/or rejection while maintaining effectiveness.";
	        $c->save();

        $cc = new CompetencyCategory;
        $cc->name = "PEOPLE MANAGEMENT";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Building Talent Capability";
	        $c->definition = "Attracting, developing, engaging, and retaining talented individuals; creating a work environment where people can realize their full potential, thus allowing the organization to meet current and future business challenges.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Coaching and Developing Others";
	        $c->definition = "Providing feedback, instruction, and development guidance to help others excel in their current or future job responsibilities based on performance, assessment,  or evidence; planning, supporting, and monitoring the development of individual skills and abilities.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Delegation and Empowerment";
	        $c->definition = "Sharing authority and responsibilities with others to move decision making and accountability downward through the organization to stretch individual capabilities while accomplishing the business unit’s strategic priorities.";
	        $c->save();

        $cc = new CompetencyCategory;
        $cc->name = "TRANSLATING STRATEGY INTO ACTION";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Continuous Improvement";
	        $c->definition = "Originating action to improve existing conditions and processes; identifying improvement opportunities, generating ideas, and implementing solutions.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Driving Execution";
	        $c->definition = "Translating strategic priorities into operational priorities/programs; aligning communication, accountabilities, resource capabilities, internal processes, and ongoing measurement system to ensure that strategic priorities yield measurable and sustainable results with high quality standards.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Establishing Strategic Direction";
	        $c->definition = "Establishing and committing to a long-term business direction based on an analysis of systemic information and consideration of resources, market drivers, organizational values, and emerging economic, technological, and regulatory conditions.";
	        $c->save();

        $cc = new CompetencyCategory;
        $cc->name = "Audit, Risk And Monitoring Competencies";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Business Continuity Management";
	        $c->definition = "Knowledge and practical skills related to the need to maintain the continuity of the organization or business continuity.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Fraud Management";
	        $c->definition = "Knowledge, capabilities , practices, and expertise related with prevention, detection, analyzing, investigation, conduct action, and improvement for the purpose managing fraud. The activities do to avoid or minimize loss opportunity due to fraud as effect from weakness of system-procedures-ability.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Internal Control";
	        $c->definition = "Knowledge and skills associated with best practices in internal control and good corporate governance.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Revenue Assurance Management";
	        $c->definition = "Knowledge, capabilities , practices, and expertise related with monitoring, analyzing, and recommend improvement in managing Revenue Leakage. To avoid or minimize loss opportunity due to lekage as effect from weakness of system-procedures-ability.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Risk Based Audit";
	        $c->definition = "Knowledge, capabilities and practices associated with performing audit activities with focus of improvement and adding value to the company by assessing the adequacy of the activities of the business units to identify and review the following risk mitigation that has been done in the form of risk management and internal control. The objectives are to maintain process integrity, ensuring measures and reporting are all accurate and reliable, and ensuring policy and regulations are complied with.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Risk Management";
	        $c->definition = "Knowledge and skills associated with assessing, mitigating and monitoring of risks that will impact on the resources (human and capital), the products and services, the customers, as well as external impacts on society, markets or the environment.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Technology / It Audit";
	        $c->definition = "Conducts audits on Technology / IT audit operation systems/ equipment/ infrastructure to make a technical assessment on the relevant field and determine the state of functionality/security/efficiency across the system/equipment/infrastructure.";
	        $c->save();

        $cc = new CompetencyCategory;
        $cc->name = "Business And Commercial Competencies";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Channel Management ( Business and Commercial Competencies)";
	        $c->definition = "Knowledge and skills associated with direct and indirect sales channel management to deliver product, service and/or brand. Activities include management recruitment and performance monitoring of sales channels including direct selling (direct sales force, telesales, and owned shop) as well as indirect selling with retailers, distributors and/or other third party.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Complaint Handling Management";
	        $c->definition = "Knowledge and capabilities to recognize different circumstances of a complaint address to organization and the ability toreceive manage and solve issue according to applicable service standard.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Customer Experience Management";
	        $c->definition = "Monitor and evaluate overall customer journey in order to ensure customer experience is maximized and relevant KPIs are met. Activities also include propose solutions to address any shortcomings and monitor on improvement process.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Sales Force And Activity Management";
	        $c->definition = "Kowledge and skills required to manage and enable sales force and sales activities to work effectively in achieving sales target and company’s objectives.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Sales Planning And Analysis";
	        $c->definition = "Knowledge and skills required to develop strategy and planning for sales based on historical performance data, competitor and market size analysis, market survey as well as customer insight. Activities includemonitor and evaluate sales report to ensure sales targets in each entity is achieved and provide feedback on gaps found in sales entity. Based on analysis of internal report and external factors, forecast sales production and revenue is required periodically.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Territory Management";
	        $c->definition = "Knowledge and skills required to manage a defined area to maximize sales opportunities, service, and expand existing customer relationships through activities such as mapping, marketing activities, and competitor analysis.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Trade Marketing";
	        $c->definition = "Knowledge and skill relate to increasing the demand at wholesaler, retailer, distributor or other third party in order to align with brand strategy. Activities include to ensure that deliver sales, volume and value, trade markting support sales forces with well-designed fundamental enhancement plans.";
	        $c->save();
	    
	    $cc = new CompetencyCategory;
        $cc->name = "Corporate Communication And Stakeholder Relationship Competencies";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Banking & Monetary Law";
	        $c->definition = "Expertise, knowledge and practice methods related to the law, rules and regulations in banking & monetary.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Capital Market Law And Regulations";
	        $c->definition = "Knowledge and skills related to capital market law and regulations such as: UU Pasar Modal Indonesia, Bapepam-LK / OJK regulations, and IDX regulations, and related foreign capital market law.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Corporate Communication And Public Relations";
	        $c->definition = "Knowledge and skill that relates with managing communication both internally and externally in terms of initiatives/activities in maintaining, repairing or developing the reputation of organization towards all stakeholders.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Corporate Social Responsibility Framework";
	        $c->definition = "Knowledge and expertise regarding the commitment to act ethically and transparently in upholding respect and contribute to the development of all stakeholders, which include customers, employees, shareholders, communities and the environment in all aspects of the company's operations include economic, social, and environment.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "GCG Principles Management";
	        $c->definition = "Knowledge and expertise regarding practices related to regulatory practices, processes, and laws related to how PT. INDOSAT is run, managed, and control business activities.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "ITU Radio Regulations";
	        $c->definition = "Knowledge and expertise related to the ITU Radio Regulations, especially to the regulations related to Indosat business including the dynamics and governance of how Radio Regulations reviewed and if necessary revised by WRC (World Radiocommunication Conferences).";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Media Handling And Relation";
	        $c->definition = "Knowledge , practices and skills that relate to communication using media tools to be able to provide information, branding, image is good and right on target.";
	        $c->save();
		
		$cc = new CompetencyCategory;
        $cc->name = "Corporate Strategy And Perfomance Competencies";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Analytics And Reporting";
	        $c->definition = "Knowledge and skills to extract data, interpret and build report on every aspect related to market, industry, business, and consumer from different data sources (internal and external). Activities include crunch data, dice-and-slice, drill down and conduct analysis of different data sets as well as present reports on business analysis, market situation, profitability product analysis, pricing (rates), short term and long term forecasting, feasibility study, etc. providing critical insights to Company and facilitating decision making process.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Business Process Design And Improvement";
	        $c->definition = "Knowledge and skills related to the process of identifying, analyzing and improving business processes of new or existing withing an organization to meet the goals and objectives, including methodology or strategy to create successful results.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Business Strategy Management";
	        $c->definition = "Knowledge and skills in relevance to conducting a sound and effective company strategy management, including understanding of Business Strategic direction and focus; cascading alignment and accountability by developing appropriate performance indicators; choosing the most appropriate tracking sources, evaluation tools and metrics to measure the completion of performance objectives";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Corporate Development";
	        $c->definition = "Knowledge and understanding of strategic planning and executions to meet specific organizational objectives. The activities may include initiatives such as recruitment of a new management team, plans for phasing in or out of certain markets or products, considering a partner for a strategic alliance, establishing relationships with strategic business partners, identifying and acquiring companies, securing financing, divesting of assets or divisions, increasing intellectual property assets and so on.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Corporate Strategy And Roadmap Development";
	        $c->definition = "Knowledge and skills to analyze industry development in ordert to see opportunity and threats; analyze internal capability in order to see the stregth and weaknesses; and to compose vision, mission, goals and company’s strategy.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Economics";
	        $c->definition = "Knowledge and skills associated with macro and micro principles of economics, including its major indicators, such as: Interest Rate, Inflation Rate, Exchange Rate, Supply and Demand Theory, etc";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Strategic Operational Excellence";
	        $c->definition = "Element of Organizational Leadership that stresses the application of a variety of principles, systems, and tools toward the sustainable improvement of key performance metrics.";
	        $c->save();

	    $cc = new CompetencyCategory;
        $cc->name = "Facilities And Office Management Competencies";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Building Civil, Mechanical And Electrical";
	        $c->definition = "Knowledge and skills related to the operation and maintenance of building and it’s facilities including physical maintenance of the building , mechanical and electrical means.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Filling System And Administration";
	        $c->definition = "Knowledge, practices and skills related to file compiling system based on categories and / or classification.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Hse Management";
	        $c->definition = "Knowledge, skill and practices regarding the policies, rules, laws and regulations related to Health, Safety and Environmental aspects in all activities of the company.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Office And Facilities Management";
	        $c->definition = "Knowledge, practices, and skill on the process or administrative activities associated with the management of the office including office space management, providing office facilities, housekeeping management, building security, visitor management, flow system of goods and people, and information or front office activities, in order to create a safe and comfortable office and working environment.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Property Management";
	        $c->definition = "Knowledge, skill and practices related to the property management from the process of preparing building construction and / or supporting infrastructure, designing mechanical and electrical system, building layout, and landscaping (the arrangement of open space).";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Tenant Management";
	        $c->definition = "Knowledge and skills related to the management of tenants, which include tenant administration, marketing of workspace/land, tenant identification, tenant fitting in/out process, tenant evaluation, maintenance of relationships with tenants (customer retention) and tenants evaluation.";
	        $c->save();


        $cc = new CompetencyCategory;
        $cc->name = "Finance And Accounting Competencies";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Accounting Policies And Procedures";
	        $c->definition = "Knowledge,abilities and skills associated with standard accounting principles, policies and procedures.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Budgeting";
	        $c->definition = "Knowledge and ability in analyze, formulate, as well as develop budget based on data assumptions, internal (infrastructure) and external (competition/market, regulatory) factors to meet corporate needs.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Capital Expenditure";
	        $c->definition = "Knowledge and skill related to capital management, including making analysis on capital expenditure projection, develop capital budgeting, monitoring execution, and analyzing the impact of capital expenditure on company’s financial performance, to achieve cost efficiency and corporate value enhancement, including develop future corporate investment planning based on previous year finance perfomance.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Cashflow Management";
	        $c->definition = "Knowledge, and skill related to company's cash flow management, that include operational activity, investment, and company's funding.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Cost Accounting";
	        $c->definition = "Knowledge and skill related in doing cost accounting activities, including establishing budget and actual cost of operations, processes, departments or product and the analysis of variances, profitability or social use of funds, in order to support decision-making to cut a company's cost and improve profitability.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Financial Accounting";
	        $c->definition = "Knowledge and skill related to mechanism, verification, analysis, and interpretation of accounting record in order to give professional advice or to produce company financial report.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Financial Analysis";
	        $c->definition = "Knowledge, abilities and skills to perform analysis of financial reporting with certain methods and techniques of financial analysis.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Management Accounting";
	        $c->definition = "Knowledge, abilities and skills associated with provision and analysis of financial and non-financial information on the organization’s products, markets and competitors’ costs, and the monitoring of theenterprise’s strategies and those of its competitors in these markets over a period of time.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Taxation";
	        $c->definition = "Knowledge, skills and expertise necessary to prepare, understand, analyze, advise, control and resolve problems all aspects of taxation.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Treasury Policies And Procedures";
	        $c->definition = "Knowledge, ability and skill required to perform activity from preparing, understanding, managing, analyzing, monitoring, controlling, up to providing recommendations related with cash position, company’s bank, investations and the associated risks, according to policies and procedures stated by Headquarter.";
	        $c->save();

	    $cc = new CompetencyCategory;
        $cc->name = "Human Resources Management Competencies";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Career And Talent Management";
	        $c->definition = "Knowledge and skills related to the process of planning, implementing and evaluating human resource development system that is designed to increase productivity through human resource management processes ongoing in order to attract, develop, retain and optimize the contribution of human resources in accordance with the skills / expertise needed to meet current business needs and future.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Change Management ( HR Mgt )";
	        $c->definition = "Knowledge and skill in identifying and implementing organizational changes which help the company to win in the marketplace.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Compensation & Benefit Management";
	        $c->definition = "Knowledge and skills related to the concept of job evaluation and compensation and benefits management, as well as accurately simulated (time, quantity, target), the analysis of market prices through surveys and benchmarking for the application and implementation.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Competency Development & Assessment";
	        $c->definition = "Knowledge, skills and practices related to the competencies, including an understanding of the model and the type of competencies, the methodology used in compiling the dictionary and competency profiles matrix, the process of evaluation and / or a valid and reliable assessment of the competencies, as well as its application in the human resources management system .";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Industrial Relations And Labor Law";
	        $c->definition = "Knowledge and skills associated with the Employment Act and Regulations of Industrial Relations";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Learning And Development Management";
	        $c->definition = "Knowledge and expertise in managing competency development activities in order to optimize the capabilities of human resources to support the achievement of the company's business strategy, including knowledge and skills related to managing the existing knowledge in the company through the creation, organization and use of knowledge in company.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Organization Design";
	        $c->definition = "Knowledge and skills related to organization effetiveness improvement at all levels by using a systematic, integrated and planned approaches, including the compiled form of organizational structure, conduct job analysis and planning of optimal manpower needs for the organization.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Performance Management";
	        $c->definition = "Knowledge and expertise in designing and implementing performance measures of performance management, including the planning, review and evaluation process.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Recruitment Process";
	        $c->definition = "Knowledge and skills related to the process of obtaining human resources within the right amount, time and quality, appropriate for the position needed by organization, including the ability to run a behavioral-based interview process (competence).";
	        $c->save();

	    $cc = new CompetencyCategory;
        $cc->name = "Legal Competencies";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Hukum Acara (Procedural Law)";
	        $c->definition = "Knowledge and expertise in the resolution of a dispute through non - forum litigation, both trial and arbitration related to the field of civil and criminal cases.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Hukum Bisnis (Business Law)";
	        $c->definition = "Knowledge and expertise related to telecommunication industry business law.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Hukum Perusahaan (Company Law)";
	        $c->definition = "Knowledge and expertise related to legal / regulatory / internal documents regulating the licensing and business activities, including but not limited to capital markets, investment, banking and finance as well as the Articles of Association and other documents and internal policies.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Legal Drafting";
	        $c->definition = "Knowledge and expertise in drafting legal agreements and using appropriate legal terminology in related documents.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Telco & Digital Laws, Regulatory Framework And Environment";
	        $c->definition = "Knows and understands both local and global telco & digital industrys law and regulatory.";
	        $c->save();

	    $cc = new CompetencyCategory;
        $cc->name = "Marketing Management Competencies";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Event Management";
	        $c->definition = "The knowledge, skills and practices related to the management and event activity settings to suit the purpose ranging from planning, preparation, execution and evaluation by moving people and resources, administration, finance and others.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Market And Business Intelligence";
	        $c->definition = "Knowledge and skill of gathering information from customer, market/industry trend, competition, industry regarding products and services offered in order to have business growth and maintain competitiveness in the market.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Marketing Communication";
	        $c->definition = "Knowledge and skills related to coordination and integration of all communication tools (media and collaterals), avenues, functions, and sources within a company into a program than maximizes the impact on consumers/corporate/business efficiently and effectively.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Marketing Strategy";
	        $c->definition = "Knowledge and skills associated with translating business goals and marketing objectives into a series of strategy and guidance for marketing activities and initiatives planning, which include planning on how the organization can best compete in its markets, by providing short term and long term strategic analysis and plan to achieve a sustainable competitive advantage.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Pricing";
	        $c->definition = "Knowledge and skill in performing analytical process to determine the pricing of company’s product and services at the most valuable price.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Product And Brand Management";
	        $c->definition = "Knowledge and skills associated with the planning, development, activation and compliance product as a brand. Activities include determining strategy of segmentation, targeting and positioning for the brand.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Product Development";
	        $c->definition = "Knowledge and skills associated with translating business goals and marketing objectives into a series of product development activities, which include product ideation and planning, design, implementation, testing, and launching.";
	        $c->save();

	    $cc = new CompetencyCategory;
        $cc->name = "Supply Chain Management Competencies";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Asset Management";
	        $c->definition = "Knowledge and skills associated with the asset, i.e. how to perform the procedure in the movement of assets and make a note of the condition of assets, from acquisition to deletion of assets in order to improve the control of assets utility for the benefit of the company.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Card Material Management";
	        $c->definition = "Knowledge and skills associated with the card raw materials and finish product, including components and characteristics of material, process and activities in handling the material, and work flow and characteristics in card manufacturing process.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Category Planning";
	        $c->definition = "Knowledge and skills to create and implement category plan based on developed category profile and INDOSAT’s broad strategic vision and external events that impact the category planning process, in order to secure the best value in purchasing.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Category Profile";
	        $c->definition = "Has knowledge of internal needs, products specifications, supply base and market dynamics. Analyzes the structure and dynamics of industry specific markets and suppliers to identify potential strategic levers. Based on this knowledge, is able to make relevant decisions and define sourcing strategies.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Contract Management";
	        $c->definition = "Develops contracts using standard templates and coordinates Legal review as required. Maintains the contracts until obligations have been met. Elevates concerns with contract execution to appropriate leadership. Monitors contract compliance, takes improvement actions, if required.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Custom Clearance And Taxation";
	        $c->definition = "Understands key commercial , documents, policies and procedures of the custom clearance and tax and how they can be used in identifying cost reduction opportunities.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Fact Based Negotiation";
	        $c->definition = "Prepares, coordinates and conducts negotiations with suppliers on a fact-based manner. Negotiates agreements to achieve results which support sourcing strategies and INDOSAT's business objectives.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Inventory Accounting/Management";
	        $c->definition = "Knowledge and skills that deals with valuing and accounting for changes in inventoried assets, in order to control inventory movement thru record and physic, including determining and applying methods to account inventory (perpetual or periodic by FIFO or LIFO system, or average weighted method).";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Procurement Law";
	        $c->definition = "Understands key commercial and developmental agreements, documents, policies, and procedures. Applies pertinent commercial principles and ethics to activities. Uses these agreements to protect INDOSAT from legal liability, and deliver a strategic advantage where applicable.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Procurement Management";
	        $c->definition = "Knowledge, practices and skills associated with supply a product or services to company. Understanding the steps in procurement process from sourcing, bidding, contract, purchase order, monitoring order, receiving product or services and evaluating supplier/vendor. Applied knowledge and skill in find, assess and evaluate supplier or vendor. Knowledge in make read and understand procurement contract.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Procurement Process Auditing";
	        $c->definition = "Designs, develops, implements, and measures business processes to ensure quality and continuous improvement. Monitors and assesses compliance with a procedure and recommendation of corrective actions.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Stakeholder Management";
	        $c->definition = "Putting the customer’s (internal & external) interest first by identifying / anticipating their needs and expectations and by adapting behaviors and services accordingly.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Supplier Relationship Management (Partnership Management)";
	        $c->definition = "Supplier (or partnership) management involves linking external supplier capability with INDOSAT’s strategic business plans. Supplier (or partnership) management involves sharing strategic information back and forth with suppliers to build mutually beneficial business relationships and synergies.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Supplier Selection";
	        $c->definition = "Developing and following-up the supplier screening and selection process through “Request For Information” (RFI) document and “Request For Proposal” (RFP) document.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Supply Chain Management";
	        $c->definition = "Knowledge and skill related with system information flow on dealer management, incentive schemes for dealer partners and its suppliers in the management of sales of products and services.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "TCO (Total Cost of Ownership ) Modeling";
	        $c->definition = "Understanding the concept of TCO and applying it systematically in sourcing decision processes. Calculating the Total Cost of Ownership (TCO) including Net Present Value.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Valuation And Appraisal";
	        $c->definition = "Knowledge and expertise in evaluate or appraise economic value of an asset objectively that use valid method, parameters and appraise principals.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Value Tracking";
	        $c->definition = "Knowledge and skill related with estimating, calculating and monitoring benefits from category sourcing initiatives in accordance with INDOSAT's benefit tracking approach; business case and TCO modeling with forecasted and calculated benefit, and monitoring of benefits through benefit reports throughout implementation.";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Warehouse Management";
	        $c->definition = "Knowledge, abilities, and skills related with the concept and activities of warehouse management, such as receiving, placement, storing, loading/unloading, security and safety; distribution requirement planning to maintain accuracy of receiving and distribution.";
	        $c->save();

	    $cc = new CompetencyCategory;
        $cc->name = "Business Management";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Channel Management (Business Mgt)";
	        $c->definition = "Utilize knowledge and skills associated with ability to manage direct/indirect sales channel activities to ensure business performance achievement; ensure recruitment effectiveness, program execution, performance monitoring & all related support for both direct (sales force, telesales, owned shop) & indirect channel (retailers, distributor/ other third party).";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Event Management";
	        $c->definition = "The knowledge, skills and practices to manage event plan & activities to suit with the actual purpose and/or to achieved quality result starting from planning, executing to evaluation process by utilizing all of the resources";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Partner Management";
	        $c->definition = "Ability to build mutual relation and beneficial synergies between Indosat Ooredoo Ooredoo & external partner; able to connect partner capability to achieve Indosat Ooredoo Ooredoo strategic business plan";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Territory Management";
	        $c->definition = "Knowledge and skill required to manage a defined area in order to maximize/expand sales performance and services; the activities may include potential area identification & competitor analysis, the execution of selling activities, able to formulate a more effective sales strategy/ customer approach within a defined area";
	        $c->save();

	    $cc = new CompetencyCategory;
        $cc->name = "Customer Management";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Customer Experience Management (Incl. Complain Handling)";
	        $c->definition = "Knowledge and skill to ensure the best customer journey is achieved with no/minimum hurdles for the customer in every touch point; Able to propose & monitor suitable solution and to anticipate/manage occurred complain to address any shortcoming";
	        $c->save();

	    $cc = new CompetencyCategory;
        $cc->name = "Generic Competencies";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Project Management";
	        $c->definition = "Knowledge and skills to define clear business requirements at a strategic level and from there develops a roadmap, or program, as to how best plan, manage and deliver the business needs, which is framed in the context of a high level roadmap, that clearly articulates how projects within each program connects from inception till delivery (DDI)";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Telco And Digital Business";
	        $c->definition = "Knowledge and understanding in Telco and Digital business industry, as well as understanding on its aspects such as business model, key players and market trend (DDI)";
	        $c->save();

	    $cc = new CompetencyCategory;
        $cc->name = "Industrial Knowledge";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Industrial Knowledge & Business Intelligence";
	        $c->definition = "Knowledge and skill to understand dynamic within client business industrial area (government, banking, etc) which include future/current trend in client industry, challenges, competitor and customer target; Able to utilize the knowledge and to use the business acumen to support their sales and service performance";
	        $c->save();

	    $cc = new CompetencyCategory;
        $cc->name = "IT";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Application Integration";
	        $c->definition = "Integration of data or functions from one application program with that of another application program - involves development of an integration plan, programming and the identification and utilization of appropriate middleware to optimize the connectivity and performance of disparate applications across target environments";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "IT and Network Governance";
	        $c->definition = "Setting and monitoring of IT and network infrastructure, information, digital services and associated technology. This involves developing policies and practices to govern the organization's approach toward handling and using IT and network products and services, in order to ensure conformance with regulations, and accountability in decision making in alignment with the business strategic plans and service standards";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "IT and Network Strategy";
	        $c->definition = "Planning, development and communication of effective inward- and outward-facing IT and network strategies, solutions and action plans, driven by environment scanning and assessment of the business' future needs and long-term strategic direction. This involves devising internal management strategies and models to support and sustain IT and network transformations, and alignment of IT and network investments and programmes with the strategy to optimize the business value from IT and network operations";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "IT Asset Management";
	        $c->definition = "Manage, optimize and protect the organization's IT assets. This includes the timely purchase, deployment, categorization, maintenance and phase out of IT assets within the organization in a way that optimizes business value. Also includes development and implementation of procedures to guide the proper handling, usage and storage of IT assets to limit potential business or legal risks";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "IT Governance";
	        $c->definition = "Setting and monitoring of IT infrastructure, information, digital services and associated technology. This involves developing policies and practices to govern the organization's approach toward handling and using IT products and services, in order to ensure conformance with regulations, and accountability in decision making in alignment with the business strategic plans and service standards";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "IT Infrastructure Design";
	        $c->definition = "Establishment of design policies and principles covering elements of connectivity, capacity, security, access, interfacing etc., as well as the translation of that into the specifications, outline and design of IT infrastructure (e.g., hardware components) within the organization, in order to support the business requirements";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "IT Performance Management";
	        $c->definition = "Evaluation and optimization of IT system and/or performance against user and business requirements. This involves the introduction and utilization of new tools and mechanisms to gather, analyze and fully optimize performance data. Also includes the initiation of controls, modifications and new investments to enhance end-to-end performance of IT components, systems and services";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "IT Problem Management";
	        $c->definition = "Management of the lifecycle of IT problems to prevent problems and incidents from occurring, eliminate recurring incidents and minimise impact of unavoidable incidents";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "IT Standards";
	        $c->definition = "Development and review of standard operating procedures as well as service expectations for IT-related activities and processes. This includes the provision of clear guidelines for the organization to carry out IT-related tasks in a manner that is effective, efficient and consistent with the IT service standards and quality standards of the organization";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "IT Strategy";
	        $c->definition = "Planning, development and communication of effective inward- and outward-facing IT strategies, solutions and action plans, driven by environment scanning and assessment of the business' future needs and long-term strategic direction. This involves devising internal management strategies and models to support and sustain IT transformations, and alignment of IT investments and programmes with the strategy to optimize the business value from IT";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Quality Standards";
	        $c->definition = "Development, review and communication of clear, quality expectations and standards within an organization, that are aligned to the company's values and business objectives. This encompasses the setting and implementation of quality expectations for IT products and services delivered to both internal or external clients";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Security Strategy";
	        $c->definition = "Establishment of the organization's security vision, strategy and initiatives to ensure that assets are adequately protected. This involves the planning, implementation and review of enterprise-wide security controls (including policies, processes, physical infrastructure, software and hardware functions etc.) to govern and preserve the privacy, security and confidentiality of the organization's information and assets";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Solution Architecture";
	        $c->definition = "Design or refine a solution blueprint or structure to guide the development of IT solutions in hardware, software, processes or related components, to meet current and future business needs. The solution architecture developed may lead to broad or specific changes to IT services, operating models and processes, and should provide a framework to guide the development and modification of solutions";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "System Integration";
	        $c->definition = "Development and implementation of a roadmap and specific integration solutions to facilitate integration of various IT components and optimise inter-operability of systems and their interfaces. Includes the integration of various architectural components such as networks, servers, system platforms and their interfaces";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "User Experience Design";
	        $c->definition = "Conceptualization, projection and enhancement of the user's interaction and engagement with an IT product / service based on a robust analysis and understanding of the product / service's performance vis-a-vis the user's desired experience and outcomes. This involves creating wire frames to adequately guide and inform subsequent planning and development processes, and making enhancements to optimise the user's experience of the product or service";
	        $c->save();

	    $cc = new CompetencyCategory;
        $cc->name = "Network";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Agile Methodology (Scrum)";
	        $c->definition = "Awareness and understanding of the Scrum Agile Methodology, demonstrating knowledge to applicable terms, developing guidelines for Scrum Events, anticipating future trend related to Scrum methodology and leading the organization in Scrum adoption";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Business Process Optimization";
	        $c->definition = "Analysis of business processes and workflows within the organization, and identification of new approaches to completely redesign business activities or optimize / improve performance, quality and speed of services / processes. Includes the exploration of automating / streamlining processes, evaluation of associated costs and benefits of redesigning business processes, as well as the identification of the potential impact and the change management activities / resources required";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Network Administration and Maintenance";
	        $c->definition = "Monitoring of network performance in order to provide for optimum levels of network performance and minimization of downtime. This includes detection, isolation, recovery and limitation of the impact of failures on the network as well as provision of  support to system users through ongoing maintenance information sharing and training";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Network Architecture";
	        $c->definition = "Planning and development of network structure and models to facilitate the evolution to its desired future state.  This involves the review and prioritization of market trends, evaluation of alternative strategies, as well as the strategic evaluation and utilization of network capability and technology to support business requirements";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Network Asset Management";
	        $c->definition = "Manage, optimize and protect the organization's network assets. This includes the timely purchase, deployment, categorization, maintenance and phase out of network assets within the organization in a way that optimizes business value. Also includes development and implementation of procedures to guide the proper handling, usage and storage of network assets to limit potential business or legal risks";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Network Configuration";
	        $c->definition = "Configuration of network hardware and software components according to organizational guidelines and technical requirements. Includes the implementation and configuration of multiple servers, network devices and network management tools, as well as the management of user network access, to ensure stable and reliable network operations";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Network Deployment";
	        $c->definition = "Set up, deployment and decommissioning of network infrastructure components and associated equipment, in accordance to a set plan and established safety / quality procedures. This includes the assessment and preparation of appropriate site locations and network infrastructure, the development of an installation plan and layout at the site, the testing of on-site systems, infrastructure components and equipment, and the correction of issues and malfunctions";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Network Design";
	        $c->definition = "Establishment of design policies and principles covering elements of connectivity, capacity, security, access, interfacing etc., as well as the translation of that into the specifications, outline and design of networkinfrastructure within the organization, in order to support the business requirements";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Network Incident Management";
	        $c->definition = "Detection and reporting of incidents, identification of affected systems and user groups, triggering of alerts and announcements to relevant stakeholders, and efficient resolution of the situation. This includes the analysis of incidents' root causes, and implementation of mitigation / prevention processes and policies";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Network Knowledge";
	        $c->definition = "Awareness and comprehension of network industry and network terminologies, including knowledge of market trend, various network management tools, network components, network vendors, and network issues ";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Network Optimization";
	        $c->definition = "Optimization of network performance in the organization, through evaluation of existing network infrastructure, solving of network problems, and identification of advanced technological network equipment";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Network Performance Management";
	        $c->definition = "Evaluation and optimization of network system and/or performance against user and business requirements. This involves the introduction and utilization of new tools and mechanisms to gather, analyze and fully optimize performance data. Also includes the initiation of controls, modifications and new investments to enhance end-to-end performance of network components, systems and services";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Network Problem Management";
	        $c->definition = "Management of the lifecycle of network problems to prevent problems and incidents from occurring, eliminate recurring incidents and minimise impact of unavoidable incidents";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Network Quality Assurance";
	        $c->definition = "Application of quality standards to review network performance, through the planning and conduct of quality assurance audits, to ensure that quality expectations are upheld. Includes the analysis of network quality audit results and setting of follow-up actions to improve or enhance the quality of network products, services or processes";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Network Strategy";
	        $c->definition = "Planning, development and communication of effective inward- and outward-facing network strategies, solutions and action plans, driven by environment scanning and assessment of the business' future needs and long-term strategic direction. This involves devising internal management strategies and models to support and sustain network transformations, and alignment of network investments and programmes with the strategy to optimize the business value from network operation";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Network Support";
	        $c->definition = "Provision of services to end users by systematically identifying, classifying and troubleshooting technical issues and incidents that disrupt and impact their day-to-day business activities, within a specified timeframe. This also includes implementing an end-to-end problem management process to analyze underlying problems, advising on network related upgrades and improvements, and developing user guides and training materials";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Project Management";
	        $c->definition = "Ability to manage project and deliver accordingly, which include coordination, monitoring and implementation of project planning, resource control, proper procedures, and protocols to achieve specific goals in a timely manner";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Stakeholder Management";
	        $c->definition = "Management of stakeholder expectations and needs by aligning those with requirements and objectives of the organization. This involves planning of actions to effectively communicate with, negotiate with and influence stakeholders";
	        $c->save();

	    $cc = new CompetencyCategory;
        $cc->name = "Network / IT";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Change Management (Network/IT)";
	        $c->definition = "Planning and systematic execution of processes to facilitate the transition of individuals, teams and organizations to a desired end state in a manner that is seamless, sustainable and aligned with business objectives. This includes the redirection of resources, business processes, finances and operating models, as well as stakeholder engagement to facilitate implementation and maximise adoption";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Data Analytics";
	        $c->definition = "Validation, analysis and interpretation of data, including the application of appropriate statistical algorithms and data modelling techniques, to address corporate data requirements, verify / disprove existing theories or models, or uncover new insights / trends";
	        $c->save();

	    $cc = new CompetencyCategory;
        $cc->name = "Product Management";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "B2B Product Knowledge";
	        $c->definition = "Knowledge and understanding related with Indosat Ooredoo Ooredoo Ooredoo product features, specification and its advantages of own product (Mobile, MIDI, Device Bundling, etc) and its positioning on the market compared with competitors";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Pricing";
	        $c->definition = "Knowledge and skill to determine the pricing of company product; able to make pricing recommendations given internal cost structure, track & analyze competitor pricing strategy to ensure competitive pricing to customer";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Solution Integration & Design";
	        $c->definition = "Knowledge to recommend the right solution to customer based on their needs and requirement; Able to provide quality feedback and recommendation for future product/solution improvement";
	        $c->save();

	    $cc = new CompetencyCategory;
        $cc->name = "Sales Management";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Account Management";
	        $c->definition = "Able to maintain customer database and manage multiple leads at the same time, to set the priorities based on the best practice and possible judgment; able to convert the leads into actual sales";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Communication Skill (presentation, negotiation)";
	        $c->definition = "Knowledge and skill to effectively convey information and ideas to help the audience  understand and retain the message; This skills including the ability to effectively explore needs, present, negotiate and deliver possible solution to reach outcomes that gain the support and acceptance of all parties";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Cross Sell / Market Expansion";
	        $c->definition = "Ability to analyze customer business process and to identify opportunity for cross or up sell in order to maximize possible business outcome;";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Sales Force & Activity Management";
	        $c->definition = "Knowledge and skills required to manage and enable sales force and sales activities to work effectively in achieving sales target and company’s objectives. Which include skills and ability to manage sales performance (performance management), to conduct coaching and counselling, as well as identified and facilitate required development for sales force";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Sales Planning & Analysis";
	        $c->definition = "Knowledge and skills required to develop strategy and planning for sales based on historical performance data, competitor and market size analysis, market survey as well as customer insight";
	        $c->save();

	    $cc = new CompetencyCategory;
        $cc->name = "Technology Common Competencies";
        $cc->save();

        	$c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Contract Management";
	        $c->definition = "Formalization of  contracts / service level agreements with providers of products and services, including measuring and managing supplier performance and fulfilment of agreed-upon service level agreements. This includes resolution of contractual issues and maintenance of vendor / provider relationships";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Digital Transformation";
	        $c->definition = "Conduct of market research, monitoring of digital technology trends and developments, and propose digital transformation ideas to the organization. This will include feasiblity study, cost-benefit analysis and evaluation of project relevance, viability and potential value add to the business";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Enterprise Architecture";
	        $c->definition = "Operationalization of a business strategy - the planning and development of business structures and models to facilitate the evolution of a business to its desired future state.  This involves the review and prioritization of market trends, evaluation of alternative strategies, as well as the strategic evaluation and utilization of enterprise capability and technology to support business requirements";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Financial Acumen";
	        $c->definition = "Management and control financial activities to effectively manage company's finance. This involves the review of organization financial capability with knowledge on financial methodologies and investment opportunities";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Financial Budgeting / Investment Management";
	        $c->definition = "Forecasting of income and expenditure as a mean to monitor organization performance, including recommendation on multiple investment scenarios with cost-benefit analysis";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Partnership Management";
	        $c->definition = "Building of cooperative partnerships with inter-organizational and external stakeholders and leveraging of relations to meet organizational objectives. This includes coordination and strategizing with internal and external stakeholders through close cooperation and exchange of information to solve problems";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Portfolio Management";
	        $c->definition = "Systematic management of the IT investments, projects, services and activities within a company, in line with business objectives and priorities. This involves the development of a framework to evaluate potential costs and benefits and make key decisions about IT investments, internal allocation and utilization of IT resources / assets, and any changes to IT processes or services offered";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Predictive Modelling";
	        $c->definition = "Analysis and insight gathering of existing data model to design offerings and forecast future outcomes. This include performing data mining and profitability analysis to predict customer behavior and incorporate insights to business goals";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Procurement";
	        $c->definition = "Development and application of procurement processes related to the solicitation of technology services through external providers. This includes the review of  proposals, setting of vendor selection guidelines, risk assessment through appropriate audits and tests, and selection of external service providers based on stipulated evaluation criteria";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Programme Management";
	        $c->definition = "Perform planning, organization, monitoring and control of all aspects of an IT programme and the strategic utilization of resources to achieve the objectives within the agreed timelines, costs and performance expectations. In addition, the identification, coordination and management of project interdependencies, ensuring alignment with and achievement of business objectives";
	        $c->save();

	        $c = new Competency;
	        $c->category_id = $cc->id;
	        $c->name = "Vendor Management";
	        $c->definition = "Management of vendors' products and services, including measuring their performance with agreed SLAs and maintaining a level of relationship with the vendors";
	        $c->save();

    }
}
