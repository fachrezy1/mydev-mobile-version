<?php

use Illuminate\Database\Seeder;

class CreateUserRoles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //begin staff roles
        $roleStaff = Sentinel::getRoleRepository()->createModel()->create([
           'name' => 'Staff',
           'slug' => 'staff',
           'permissions' => [
               'trainee.manage' => true,
               'trainee.review' => true
           ]
        ]);
        //end staff roles

        //begin atasan roles
        $roleAtasan = Sentinel::getRoleRepository()->createModel()->create([
           'name' => 'Atasan',
           'slug' => 'atasan',
           'permissions' => [
               'trainee.list_req' => true,
               'trainee.view' => true,
               'trainee.approve' => true,
               'trainee.edit' => false,
           ]
        ]);
        //end atasan roles

        //begin admin roles
        $roleAdmin = Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'HR',
            'slug' => 'hr',
            'permissions' => [
                'trainee.list_req' => true,
                'trainee.view' => true,
                'trainee.approve' => true,
                'trainee.edit' => false,
                'admin.createTrainee' => true,
                'admin.manageItems' => true,
                'admin.manageUsers' => true,
            ]
        ]);
        //end admin roles

        //add staff here
        $credentialsStaff1 = [
            'email'    => 'mydevstaff@mailinator.com',
            'password' => '%%B47054I%%',
            'username' => 'Staff'
        ];

        $user = Sentinel::registerAndActivate($credentialsStaff1);
        $roleStaff->users()->attach($user);

        //add atasan here
        $credentialsAtasan = [
            'email'    => 'mydevatasan@mailinator.com',
            'password' => '%%B47054I%%',
            'username' => 'Manager'
        ];

        $user = Sentinel::registerAndActivate($credentialsAtasan);
        $roleAtasan->users()->attach($user);

        //add staff2 here
        $credentialsStaff2 = [
            'email'    => 'mydevstaff2@mailinator.com',
            'password' => '%%B47054I%%',
            'username' => 'Staff'
        ];

        $user = Sentinel::registerAndActivate($credentialsStaff2);
        $roleStaff->users()->attach($user);

        //add atasan2 here
        $credentialsAtasan2 = [
            'email'    => 'mydevatasan2@mailinator.com',
            'password' => '%%B47054I%%',
            'username' => 'Manager'
        ];

        $user = Sentinel::registerAndActivate($credentialsAtasan2);
        $roleAtasan->users()->attach($user);

        //add admin here
        $credentialsAdmin = [
            'email'    => 'mydevadmin@mailinator.com',
            'password' => '%%B47054I%%',
            'username' => 'HR Admin'
        ];

        $user = Sentinel::registerAndActivate($credentialsAdmin);
        $roleAdmin->users()->attach($user);

    }
}
