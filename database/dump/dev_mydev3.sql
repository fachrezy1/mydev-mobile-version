-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 17, 2019 at 03:25 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dev_mydev3`
--

-- --------------------------------------------------------

--
-- Table structure for table `activations`
--

CREATE TABLE `activations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activations`
--

INSERT INTO `activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'S9w1HS2AAtwGuYfE3Kd4Fd3Vby5Cctlq', 1, '2019-02-16 09:31:37', '2019-02-16 09:31:37', '2019-02-16 09:31:37'),
(2, 2, 'vRBg8wmk2GWfwRClV4lOYdEapRP1xane', 1, '2019-02-16 09:31:38', '2019-02-16 09:31:37', '2019-02-16 09:31:38'),
(3, 3, 'TYMtAW9UQ6zvtoIMDBZeXNkjf8S3REWU', 1, '2019-02-16 09:31:38', '2019-02-16 09:31:38', '2019-02-16 09:31:38');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_07_02_230147_migration_cartalyst_sentinel', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_01_19_055606_create_sessions_table', 2),
(6, '2019_01_22_163605_add_soft_deletes_to_requests_table', 4),
(7, '2019_01_22_152552_create_requests_table', 5),
(13, '2019_01_31_131902_create_reviews_table', 7),
(16, '2019_01_22_160758_create_requests_group_table', 8),
(17, '2019_01_23_144134_add_soft_deletes_to_requests_group_table', 8),
(18, '2019_02_06_153632_create_users_relation_table', 9),
(19, '2019_02_15_011310_add_is_rejected_field_to_requests_group_table', 10),
(20, '2019_02_16_181130_create_notifications_table', 11);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `persistences`
--

CREATE TABLE `persistences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `persistences`
--

INSERT INTO `persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
(1, 1, 'E8PCG7vpStM5ZkpZEaJeJup5DO9qMhNk', '2019-01-17 12:38:39', '2019-01-17 12:38:39'),
(2, 1, 'FJHhqwkDGWD7geWrfAC89lccqG0q8reU', '2019-01-17 12:41:35', '2019-01-17 12:41:35'),
(3, 1, 'DpIE1SIP5b318HkO5C3eHKJcn0qwsBJq', '2019-01-17 12:41:35', '2019-01-17 12:41:35'),
(4, 1, '1ZiK55eL7XoXgtouT41aQpoYgitXvObZ', '2019-01-17 12:42:58', '2019-01-17 12:42:58'),
(5, 1, 'k2oB5njRkroWGjU07emOuNIYinmq1CKZ', '2019-01-17 12:42:58', '2019-01-17 12:42:58'),
(6, 1, 'LQWuRY3YTU74LtSLkSTdF1rAi5LJ1Elr', '2019-01-17 12:43:01', '2019-01-17 12:43:01'),
(7, 1, 'GmsNaJkYltMKORTG67ZtEmRDsKprMY1g', '2019-01-17 12:43:01', '2019-01-17 12:43:01'),
(8, 1, '8aKPjLIR4FcBZalcrDppqfOASSfnJ0uX', '2019-01-17 12:43:13', '2019-01-17 12:43:13'),
(9, 1, 'q6edoML4BQQ45kxangFYa99CFrEiPJjY', '2019-01-17 12:43:13', '2019-01-17 12:43:13'),
(10, 1, 'PxdA5kux1aHuTsw3h2Dw1w8NlzBr6GqW', '2019-01-17 12:43:33', '2019-01-17 12:43:33'),
(11, 1, 'BI0GuXx31mds9yQyIJvdPS7TG8n9WwW1', '2019-01-17 12:43:33', '2019-01-17 12:43:33'),
(12, 1, '6kufDcc6QysEJ3OhxHyRO7y3iyixd13J', '2019-01-17 12:43:59', '2019-01-17 12:43:59'),
(13, 1, 'NP0oGr54UEWz4K78tiR0VZRGMhuKlF53', '2019-01-17 12:43:59', '2019-01-17 12:43:59'),
(14, 1, 'tjtacGfVeauiT0IAxaNekuyMqvVZF7fw', '2019-01-17 12:44:06', '2019-01-17 12:44:06'),
(15, 1, 'XS9ugRhdvqEGYzIZyHrMlnPTJhtHpkra', '2019-01-17 12:44:06', '2019-01-17 12:44:06'),
(16, 1, 'eZmpPzyjEC7LHqxSKO9zey4cIoe5t4DN', '2019-01-17 12:45:17', '2019-01-17 12:45:17'),
(17, 1, 'sjOesmlHU35nOgFACQSSQyb2hvX2cpQf', '2019-01-17 12:45:17', '2019-01-17 12:45:17'),
(18, 1, 'i8RHOPkJWGG8oa2HzxASI0lENBRQbD2K', '2019-01-17 12:45:41', '2019-01-17 12:45:41'),
(19, 1, 'dpfAA9AWg4UHmFAhUI0AFfNFKOi6dIwb', '2019-01-17 12:45:41', '2019-01-17 12:45:41'),
(20, 1, 'jh0usMn6Hc5XdYu2ulM2xF2qhC8yYWkj', '2019-01-17 12:46:16', '2019-01-17 12:46:16'),
(21, 1, 'dDiCL12UQfvRAm5Wm8nQtdmWCVkJP9co', '2019-01-17 12:46:16', '2019-01-17 12:46:16'),
(22, 1, 'Xeqe1bWxXq9aYyDrQtzFeCpiwzy4ZOLg', '2019-01-17 12:46:23', '2019-01-17 12:46:23'),
(23, 1, 'awfwc7lNJeb2DjI8JzJapEHcd2G6dpVd', '2019-01-17 12:46:23', '2019-01-17 12:46:23'),
(24, 1, 'lM1wN8SeeI39OLvxCAa2XqX6cJbs81iS', '2019-01-17 12:46:31', '2019-01-17 12:46:31'),
(25, 1, 'G0lrwsEdsQn1fyGrqlnSqraCpOLaX1hC', '2019-01-17 12:46:53', '2019-01-17 12:46:53'),
(27, 1, 'Fzw4t9UXduzaWmLe19KBJjeAzLInB0Zl', '2019-01-17 12:47:59', '2019-01-17 12:47:59'),
(28, 1, 'nggrIwOsRXh5LW5kPFhSBneo9Bnb75vk', '2019-01-17 12:47:59', '2019-01-17 12:47:59'),
(29, 1, '5meOapWnKDpF23xYBJvTYs8Z0gb0X2Re', '2019-01-17 12:48:03', '2019-01-17 12:48:03'),
(30, 1, 'FdEeBjf41KuYSoepfs8WXRxOh5J1N1aK', '2019-01-17 12:48:03', '2019-01-17 12:48:03'),
(31, 1, 'H0AfibdYMObVRXJY0rPiAZTaKMEo9YFQ', '2019-01-17 12:48:05', '2019-01-17 12:48:05'),
(32, 1, '8b9KAfxjQ3SsmNsBLkMpkknb77YTPvse', '2019-01-17 12:48:05', '2019-01-17 12:48:05'),
(33, 1, 'SdHMdFi4Vb1uY8hB8G0t11FQ4kgupD8p', '2019-01-17 12:48:11', '2019-01-17 12:48:11'),
(35, 1, 'TVBLtP222etQ8Orv5WRbMNdiLvELIR62', '2019-01-17 12:48:14', '2019-01-17 12:48:14'),
(37, 1, '9YgioO4G5NR0umL1GOuMQGBxqxXrSGXl', '2019-01-17 12:48:27', '2019-01-17 12:48:27'),
(39, 1, 'LToh9GOoFzH5USTa3UrFwWuhDS45OTC2', '2019-01-17 12:48:29', '2019-01-17 12:48:29'),
(41, 1, 'KCesIGXXCbLpDT1Zv7D6z56way3ONANC', '2019-01-17 12:48:32', '2019-01-17 12:48:32'),
(42, 2, 'ulzJUJ2eGjPc5W4irxl5VHQArtjzzae6', '2019-01-24 17:29:01', '2019-01-24 17:29:01'),
(43, 2, 'fWpmWzoiz8v55hYq6sZLcocONhCIBDbH', '2019-01-24 17:30:05', '2019-01-24 17:30:05'),
(44, 2, 'FFX8hEmFiOHiu5ti6NYIpSxHpICcLBV5', '2019-01-24 17:30:23', '2019-01-24 17:30:23'),
(46, 2, 'ZQW7n2DxatQheAvWPpx35Vtek3de6X2n', '2019-01-24 18:11:33', '2019-01-24 18:11:33'),
(48, 2, 'g8tK9Em7oGUAkqeHxnMlhSsiuYDqLIMR', '2019-01-24 18:14:57', '2019-01-24 18:14:57'),
(50, 2, 'jehGSaSPskZju5sAW2qAxIibw6YgBuiR', '2019-01-24 18:20:58', '2019-01-24 18:20:58'),
(52, 2, 'nlsSMeg5Kx9umkoPYgOlrU7P4XbI5S6W', '2019-01-24 18:44:49', '2019-01-24 18:44:49'),
(54, 2, 'a3Lif3SuOtLCJ1W3tGwo9OhVHMcrQ80W', '2019-01-24 18:46:04', '2019-01-24 18:46:04'),
(56, 2, 'oVXW9lSVPSJM4GmIGyYNf303NSod3s2m', '2019-01-24 18:46:28', '2019-01-24 18:46:28'),
(58, 2, '4P0QFKIJJlpU8s4VFwgetLjXQePQs5n5', '2019-01-24 18:46:43', '2019-01-24 18:46:43'),
(60, 2, '3Ay6ic4orCcfoLo965l67ciE0slnu139', '2019-01-24 18:46:53', '2019-01-24 18:46:53'),
(61, 2, 'DrjxLZWotLfDo3F0UcGuM3eo9yTfaF7b', '2019-01-24 18:47:09', '2019-01-24 18:47:09'),
(62, 2, 'lQ1AyMbgHS5bchytIn29FYxle64xoDWB', '2019-01-24 18:47:23', '2019-01-24 18:47:23'),
(64, 2, 'szTwvdWt7oMNvPW61bTD713GOZLjcuQX', '2019-01-24 18:55:34', '2019-01-24 18:55:34'),
(65, 2, 'qtp6J53EPtm1OzdXhAmfcahL4hh2qxFb', '2019-01-24 18:55:34', '2019-01-24 18:55:34'),
(66, 3, 'rnAvTpnaQU43hn9zGuQdLCYLFl65yvEL', '2019-01-24 20:33:25', '2019-01-24 20:33:25'),
(67, 3, 'RbL0ONK0o9oVAAQsYtk0ZzudrWQH2khM', '2019-01-24 20:33:25', '2019-01-24 20:33:25'),
(68, 3, 'qYBBCk9izuQHHx9BpJ87akEAcJFkdOF5', '2019-01-25 08:33:09', '2019-01-25 08:33:09'),
(70, 3, '65qvZsvH3hrFCNbzw5UitYdxuVFSfupI', '2019-01-25 10:49:55', '2019-01-25 10:49:55'),
(71, 3, 'CnzdLjOOyXgav9RzlgAMywrOWv2klIKh', '2019-01-25 10:49:55', '2019-01-25 10:49:55'),
(72, 3, 'sOLE5mh7Ekd18PewP3MBnSFYPbCYhp3h', '2019-01-26 08:55:01', '2019-01-26 08:55:01'),
(74, 3, '6mInWeMvqoHwkrbEz6ARdmevHZ01G6Do', '2019-01-26 09:41:22', '2019-01-26 09:41:22'),
(75, 3, 'ZLKZbrDzt0ennaxgJJDujpIueg6tOMVn', '2019-01-26 09:41:22', '2019-01-26 09:41:22'),
(76, 3, 'D8cqrQ73fIrWBT2TrzhPBBg9uTcLszvQ', '2019-01-27 05:32:10', '2019-01-27 05:32:10'),
(77, 3, 'AL0t09DwqsYSNHzXVpBGJkjeWdQr9nsR', '2019-01-27 05:32:10', '2019-01-27 05:32:10'),
(78, 3, 'ET0NHJQCYvh3EfNPDasxd1mCO2FpxDAf', '2019-01-28 06:01:33', '2019-01-28 06:01:33'),
(80, 3, 't30YsV2X3Lk3fjCPMChbOV4FhfFztwRD', '2019-01-28 07:43:30', '2019-01-28 07:43:30'),
(81, 3, 'ntl0dUSE69S97DR0CYRAvL7TxrFVtV9q', '2019-01-28 07:43:30', '2019-01-28 07:43:30'),
(82, 3, 'GVP1EBuWBW8tgjbGCev9nej2ZcuByFms', '2019-01-28 08:45:41', '2019-01-28 08:45:41'),
(83, 3, 'veHXFzjwKSzdeqTRpjb70DgACu9nkYQx', '2019-01-28 08:45:41', '2019-01-28 08:45:41'),
(84, 5, 'ihAYll4S3NzfAvnTD81NGCtMELyFY2Iz', '2019-01-29 08:20:35', '2019-01-29 08:20:35'),
(85, 5, 'cqwn3hHDh4284xP5nKNqEQIhz0Irk4il', '2019-01-29 08:22:50', '2019-01-29 08:22:50'),
(86, 3, 'CirxZMwWOzgwH3HokXNlbAp3Exna2nHD', '2019-01-30 10:26:26', '2019-01-30 10:26:26'),
(87, 3, 'TaW41vLXGwEeowJm5ZL9OFeJX0gps2Gw', '2019-01-30 10:26:26', '2019-01-30 10:26:26'),
(88, 5, 'NzLG3CZudzuQAnfgdZTO2IL1MQ5a7mrY', '2019-01-31 18:41:08', '2019-01-31 18:41:08'),
(89, 5, 'L9LR0taU5IJhgFeKwZXAwz2nvRn43q0N', '2019-01-31 18:41:08', '2019-01-31 18:41:08'),
(90, 4, 'x54nu2X5YHIcM3kS2MF6kF3wVitMXjPI', '2019-01-31 18:43:27', '2019-01-31 18:43:27'),
(91, 4, 'Orkz28ZioE1n9RoqKON1cDa81YlKYL5D', '2019-01-31 18:43:27', '2019-01-31 18:43:27'),
(92, 5, 'J6RfYwrtI5VWrdJTEkJxryZ4dmywhg6X', '2019-01-31 18:44:01', '2019-01-31 18:44:01'),
(93, 5, 'KO6up7pvCBOel1GdmhyFJI9hvuNnVkM8', '2019-01-31 18:44:01', '2019-01-31 18:44:01'),
(94, 3, 'ZlFTby9Sv6QJjK5S21z7O5JPgKE9zFwp', '2019-02-02 00:04:54', '2019-02-02 00:04:54'),
(95, 3, 'dx82bAd3yB3kQgaKNKGZAabwjmkKfjE1', '2019-02-02 00:04:54', '2019-02-02 00:04:54'),
(96, 5, 'Q94jaC5W75FisXkdaaksO9iab6Ji8uIQ', '2019-02-02 02:10:29', '2019-02-02 02:10:29'),
(97, 5, '1QDgFj8vUQlo8rWNFWJdonOwfnD2ATOC', '2019-02-02 02:10:29', '2019-02-02 02:10:29'),
(98, 5, '8egBrH4tBtZOhFOBC3MMUAZeJrA91pZ0', '2019-02-02 18:58:58', '2019-02-02 18:58:58'),
(99, 5, 'WTPO3A5E5wPnc8EoWM7ZKfMkNIWoKvQ1', '2019-02-02 18:58:58', '2019-02-02 18:58:58'),
(100, 3, 'LcxVnnb28flwEOvaxfHomOHnADeQg38l', '2019-02-06 07:47:00', '2019-02-06 07:47:00'),
(101, 3, 'AeokFX4PK8xMjnleQChmI6O5LoSV9vUD', '2019-02-06 07:47:00', '2019-02-06 07:47:00'),
(102, 5, 'N3zcPrKDoG9wLhNbN44KOjbGstGLsiip', '2019-02-06 07:51:19', '2019-02-06 07:51:19'),
(104, 6, '8SaY3GOB0QN9hrO1YtAngUH02lYOQSHD', '2019-02-06 08:42:34', '2019-02-06 08:42:34'),
(106, 6, 'fQaeZw1KFMYUgw4HvHPzscum9yHfmQ7D', '2019-02-06 08:46:18', '2019-02-06 08:46:18'),
(108, 6, 'ws9w4IBWtQ0yA6gZKY5HIExO1uQz7nlU', '2019-02-06 09:29:56', '2019-02-06 09:29:56'),
(109, 6, 'RvXa1HmHiWFlFMJnyUnkwWSKhs1hSiYW', '2019-02-06 09:29:56', '2019-02-06 09:29:56'),
(110, 6, 'DO7oQCtOrFT48LIlOZBpUbeEu1mofydQ', '2019-02-06 15:30:47', '2019-02-06 15:30:47'),
(112, 2, 'wVCv4lH5k8f1cKxEejPawSF3YiZZIfcj', '2019-02-06 16:55:05', '2019-02-06 16:55:05'),
(114, 3, 'XSMSJKnVyx1Zq2VjBImksL21riGx3xKY', '2019-02-06 16:55:43', '2019-02-06 16:55:43'),
(116, 4, 'ESMH6N1LxIvAwr2gH8UouVeUUhVC2xSi', '2019-02-06 16:56:18', '2019-02-06 16:56:18'),
(118, 2, '6TTpARJwVyaLN706cD89It2o7rfkv9za', '2019-02-06 16:56:35', '2019-02-06 16:56:35'),
(120, 3, 'oP5KGDItu9XcMwTb8a5fEvhJqtpxk5O7', '2019-02-06 16:57:16', '2019-02-06 16:57:16'),
(122, 2, 'cQIElJCYkaoZRArnxI4tezrxbNAg0G6M', '2019-02-06 17:00:12', '2019-02-06 17:00:12'),
(123, 2, 'Hm49qfDbEAy4UZ2kJ3NEDqzc985dYSeV', '2019-02-06 17:00:12', '2019-02-06 17:00:12'),
(124, 1, 'S3Vdi4nasVPW3BIp9wbUS6GGKwzc0Um9', '2019-02-07 04:22:01', '2019-02-07 04:22:01'),
(126, 3, 'rnXeqGV2x8DZQlOeEbqTvjgtlGeDyGQ6', '2019-02-07 04:23:04', '2019-02-07 04:23:04'),
(128, 4, 'nOlxO9crqySlepoCxDgOPsmxDkD1aOlB', '2019-02-07 04:49:44', '2019-02-07 04:49:44'),
(130, 2, 'DQ8EaqIjRmy2AyAJ8Q91RCfpHL0PbLYL', '2019-02-07 04:56:10', '2019-02-07 04:56:10'),
(132, 4, 'mavEOnvll7iI7vDiqMngoxnRUHYVxRkD', '2019-02-07 04:56:26', '2019-02-07 04:56:26'),
(134, 2, '5L4pWsgX5UGsmbREVTFPCMyZYo5ZuqQc', '2019-02-07 04:57:02', '2019-02-07 04:57:02'),
(136, 3, 'KebCDmH49NMOMGM8W1s5KXHDT6Avy4Ri', '2019-02-07 04:57:20', '2019-02-07 04:57:20'),
(138, 2, 'UdkzHtob674Bowx8qoyusqDHXIYrFVaA', '2019-02-07 04:57:41', '2019-02-07 04:57:41'),
(140, 1, 'XxBEaVtzrePn10MhL4ZvtFx2Sl0slZdj', '2019-02-07 05:00:01', '2019-02-07 05:00:01'),
(142, 3, 'lPyeJ2mEf8ZRs3ON9Om8yujheA68HHJD', '2019-02-07 05:01:11', '2019-02-07 05:01:11'),
(144, 4, 'UoVCwjcfXSg0TqCdkZWMevysN83Tqxnp', '2019-02-07 05:01:28', '2019-02-07 05:01:28'),
(145, 4, '44OuMJMkC9sQcOdmP6bJGwOMoftbD3jb', '2019-02-07 05:01:28', '2019-02-07 05:01:28'),
(146, 1, 'DWw5tD7j4nWDRhzYOClTWTG8sLiD4TRS', '2019-02-07 07:33:33', '2019-02-07 07:33:33'),
(148, 3, 'GeBRTJZkm8HW0Blje8564eV0bmAlfPKZ', '2019-02-07 08:33:50', '2019-02-07 08:33:50'),
(150, 1, 'qpbvKU8fSCXe0vnUTbbSkEs8C9NEF0XT', '2019-02-07 08:56:25', '2019-02-07 08:56:25'),
(152, 3, 'WuJeQMmB7ffI7t3ErrOjtvzLldrsagUf', '2019-02-07 09:08:07', '2019-02-07 09:08:07'),
(154, 2, 'Dfb5OBaGqYMMtBaB8qY5DTrOdVgMAmBo', '2019-02-07 09:11:54', '2019-02-07 09:11:54'),
(156, 1, 'jeETH5lzDLfGtIEJfpMsMPywtV4s4JVY', '2019-02-07 09:40:24', '2019-02-07 09:40:24'),
(158, 1, 'lo8r65njDrqnDdt6RJFK5i5N6ONrSOn3', '2019-02-07 10:03:59', '2019-02-07 10:03:59'),
(160, 2, 'XEHt72DBikzCQIOPf2iKjsAQo1hh77GC', '2019-02-07 10:30:04', '2019-02-07 10:30:04'),
(162, 1, 'tGoDYKF2bRu1v9kc0BVRhUlchwlqI2tP', '2019-02-07 10:30:44', '2019-02-07 10:30:44'),
(163, 1, 'CPrbdes9nXqQru7A7JsA4knOOmPj4zWS', '2019-02-07 10:30:44', '2019-02-07 10:30:44'),
(164, 2, 'sXlNFtnZBgablciRwEAUCsijsEGs59qI', '2019-02-07 10:31:47', '2019-02-07 10:31:47'),
(166, 3, '6tf1ijjtAk3KrkMfJzTWBKwsaZQAzISV', '2019-02-07 10:47:22', '2019-02-07 10:47:22'),
(168, 1, 'IlCMkKjsAui8rtnfpuAsYGjH3osEFQbl', '2019-02-07 10:47:39', '2019-02-07 10:47:39'),
(170, 1, 'fLAWYL11bxYRUQEjXWDA9o3NubKKf3Mg', '2019-02-07 10:55:21', '2019-02-07 10:55:21'),
(172, 2, '3BixQheJe6QzwiBs27OBFovJknMIcTUi', '2019-02-07 10:55:42', '2019-02-07 10:55:42'),
(174, 3, 'oLG6wdpvgX22bpYLDC6YGSNXHp7tXoL3', '2019-02-07 10:58:43', '2019-02-07 10:58:43'),
(176, 2, 'l7KXT1rUI0HbrllGSHAvEAiH2L3z5rcX', '2019-02-07 10:59:01', '2019-02-07 10:59:01'),
(178, 2, 'bQvet0uHDZoQhnbISVjdYA5N3BplDAk2', '2019-02-07 11:01:16', '2019-02-07 11:01:16'),
(180, 3, 'i5uc1e78GvuSMh0bWSsjOQAllF8YHZv0', '2019-02-07 11:01:32', '2019-02-07 11:01:32'),
(182, 2, 'pguGxfkeg89eaWARJLBD9sKoyFuJLpj0', '2019-02-07 11:01:46', '2019-02-07 11:01:46'),
(184, 3, 'wPLlxYRIr2rw3lAsr9W4HeZdEuQrRZuQ', '2019-02-07 11:04:40', '2019-02-07 11:04:40'),
(185, 3, '8WRVDUAOoYv1k8mOfIFU6wUu83u2nF5Z', '2019-02-07 11:04:40', '2019-02-07 11:04:40'),
(186, 1, 'BxmMltL6XKbOfeqrpHbuT99floAMS3PI', '2019-02-07 17:59:39', '2019-02-07 17:59:39'),
(188, 2, 'ASUEyYoXfHUQkWbmIb2BgLKdt6IQaGBQ', '2019-02-07 18:03:19', '2019-02-07 18:03:19'),
(190, 3, 'UiejLlDKcATDDnqPKKbvndqHBedtRPmE', '2019-02-07 18:05:57', '2019-02-07 18:05:57'),
(192, 2, 'lKJcjwaj9mSAuvgnRgYvfW021twM7hFi', '2019-02-07 18:08:45', '2019-02-07 18:08:45'),
(194, 1, 'EH1ZhTZEsqBVNCiZiOAhuGmMEWC9QOhU', '2019-02-07 18:09:24', '2019-02-07 18:09:24'),
(195, 1, 'kWXlNa6aBUR0NDYHuG32RUr1I2sP5OSC', '2019-02-07 18:09:24', '2019-02-07 18:09:24'),
(196, 3, 'byquGOVWADKKYSNOK3pkwAVThyRrGiYS', '2019-02-07 18:10:36', '2019-02-07 18:10:36'),
(198, 1, 'KJNajgDOQzotDkz4FArU7PVr6ZqQ6ZXn', '2019-02-07 19:12:01', '2019-02-07 19:12:01'),
(199, 1, 'zlqRRGsaX7g9GeJvub0pBSdLWJTPovC1', '2019-02-07 19:12:01', '2019-02-07 19:12:01'),
(200, 3, 'QajW0sieko8zCAJ2leIlSBAVZPBVoPHH', '2019-02-07 19:18:05', '2019-02-07 19:18:05'),
(201, 3, 'w1rWuZTjbo8w0TajS8gMV7D51cNgULE1', '2019-02-07 19:18:05', '2019-02-07 19:18:05'),
(202, 2, 'jvYiH6ySCTzfsgmSoXnnEHbUV5dmNOV2', '2019-02-08 00:33:44', '2019-02-08 00:33:44'),
(204, 1, 'tWLPsiDz0TeagSlKwmDKy1TT8GakepRc', '2019-02-08 00:34:31', '2019-02-08 00:34:31'),
(205, 1, 'FNqyAUknR539d4NAMh4lJJ3gc5BgxLCi', '2019-02-08 00:34:31', '2019-02-08 00:34:31'),
(206, 3, 'LiPrOjZzuyEpSVKGdrisgAdACjzKvSFm', '2019-02-08 00:35:37', '2019-02-08 00:35:37'),
(208, 2, 'B57gCDRLBAJLwFXVemUo8WaiMzlopOVO', '2019-02-08 00:38:27', '2019-02-08 00:38:27'),
(209, 2, '5zInUsgmVhRWaRUvPZfni29iobTuuUky', '2019-02-08 00:38:27', '2019-02-08 00:38:27'),
(210, 1, 'mVbmzHEWDwUdBtoi8pwJEaJC4SZOY63j', '2019-02-08 00:47:10', '2019-02-08 00:47:10'),
(211, 1, '5LfhhFE0BElHjJ8NZ7y8eGtfePDuuxSo', '2019-02-08 00:47:10', '2019-02-08 00:47:10'),
(212, 1, '6eGbTZ0EJhbG5aEQBKqaBXzBT7zGOVKJ', '2019-02-08 20:50:57', '2019-02-08 20:50:57'),
(214, 3, 'ndNn9HGf4S2UtzDrIRUi0FO6lOEcjcpe', '2019-02-08 20:53:16', '2019-02-08 20:53:16'),
(215, 3, 'xNYlFW88UOSVSdVrNDIon8OXIuSdUDtu', '2019-02-08 20:53:16', '2019-02-08 20:53:16'),
(216, 3, '3vNfwoh8UyJUiX17iuq18msKF1Juro61', '2019-02-10 20:01:43', '2019-02-10 20:01:43'),
(218, 1, 'SIkVoLrlakKKtAZ5Ih1bSehXL9sZaVYQ', '2019-02-10 20:03:57', '2019-02-10 20:03:57'),
(219, 1, 'jYZkL2LJaaufze7M3ILMohVksISI7sG6', '2019-02-10 20:03:57', '2019-02-10 20:03:57'),
(220, 3, 'wJw2JVLB7HvBOdYvRvzl4ywzAQCzPsiT', '2019-02-11 04:31:03', '2019-02-11 04:31:03'),
(222, 2, 'lFii6zMgtg1PpxlR2wdARTznqTlaGeqY', '2019-02-11 04:31:48', '2019-02-11 04:31:48'),
(224, 1, 'MrTI895IrppU5vf3LCLiX8obn3zUqQwA', '2019-02-11 04:32:02', '2019-02-11 04:32:02'),
(226, 1, 'sR5XKw8T7VjiZwqF15AJxCXaxZCdFxfk', '2019-02-11 05:01:56', '2019-02-11 05:01:56'),
(227, 1, 'OHtO3VxIguFSFAI5494O2WI4Tr07UCAH', '2019-02-11 05:01:56', '2019-02-11 05:01:56'),
(228, 1, 'geSpChXgULHFP7T8Smt6YDiuTlIG6KIK', '2019-02-11 14:27:58', '2019-02-11 14:27:58'),
(230, 1, 'eBVCVphe5TFwwrqni7BUjb0g2EDo94nQ', '2019-02-11 14:39:33', '2019-02-11 14:39:33'),
(232, 1, 'tYJanfgBeC3nQgAhI0RYElo3uXUahpMq', '2019-02-11 14:42:55', '2019-02-11 14:42:55'),
(234, 1, 'ce8IDXr2RQbIWwNp3IPeC5fEQ1bqZ2Ii', '2019-02-11 15:01:28', '2019-02-11 15:01:28'),
(236, 1, '2EZPj2Wq9EmGCXDTnGE6vcu9XK4RIyfz', '2019-02-11 15:11:40', '2019-02-11 15:11:40'),
(238, 1, '4mZOgq7Nvvc944pv7LaglFIYpus7qaUa', '2019-02-11 15:42:24', '2019-02-11 15:42:24'),
(240, 1, 'wPgR0cuid5vl8ZbWNF0PwfvSyGNI0WT6', '2019-02-11 16:36:54', '2019-02-11 16:36:54'),
(242, 1, 'JK8KnEDVTAGMJ4Kzwb205gL0HZbLkdEe', '2019-02-11 16:38:07', '2019-02-11 16:38:07'),
(244, 1, 'Rb3hN3MAJPHuFcuS1Fm6Kg78XwZS3azE', '2019-02-11 16:40:15', '2019-02-11 16:40:15'),
(246, 1, 'yC8QrcovS8qj2yeSiVf6QqmV0KmKkqbv', '2019-02-11 17:34:04', '2019-02-11 17:34:04'),
(248, 2, 'aMUd4ARnSkHBkiwnO875ByKnGyJ0Dmrj', '2019-02-11 17:37:35', '2019-02-11 17:37:35'),
(250, 1, 'JN8gcu7cQX1VqLJRCXB5DseqCGri6WJL', '2019-02-11 17:39:19', '2019-02-11 17:39:19'),
(251, 1, '120qa8lO7y1z4DPC60nsUy341KsEDJRp', '2019-02-11 17:39:19', '2019-02-11 17:39:19'),
(252, 1, 'TQ7zfbpcuK2aqlFf1LKtnTDJ8VTm6ADU', '2019-02-12 04:39:32', '2019-02-12 04:39:32'),
(253, 1, 'pW5S3t8E7nL3ymyA81YfGqqCJMc7jXBc', '2019-02-12 04:39:32', '2019-02-12 04:39:32'),
(254, 1, 'WZFoM9XbmVo6X88Bck3Px3rEAiazQtOW', '2019-02-12 11:58:03', '2019-02-12 11:58:03'),
(255, 1, 'z66jrBmgDPV3ZHSnyi0evohNZgv5C3Po', '2019-02-12 11:58:03', '2019-02-12 11:58:03'),
(256, 1, 'TuqHqvfZ9XgBiulmcAgBXFP8QKgezSpa', '2019-02-12 22:33:49', '2019-02-12 22:33:49'),
(258, 1, 'HUrYkKpw0B3LG9ZlpdUmz5pcZPVU1M0u', '2019-02-13 01:07:49', '2019-02-13 01:07:49'),
(260, 1, '7VqGSezWRz1VlrGgl6ErgYvN9rMq7nVt', '2019-02-13 01:26:50', '2019-02-13 01:26:50'),
(261, 1, 'P81ROOWoCdVveEt9fd1qWI7n1clV4AZ0', '2019-02-13 01:26:50', '2019-02-13 01:26:50'),
(262, 1, 'X4LL0rmlBjek6SX7oZ4SQXKU9g88dBLE', '2019-02-13 14:15:49', '2019-02-13 14:15:49'),
(264, 1, 'oFlqxzlSA97s27ogF7Dl89WTtgasFP33', '2019-02-13 14:37:08', '2019-02-13 14:37:08'),
(266, 1, 'ZVAkPUYMJU4nP871oHHNtRWAm9mHY0Ka', '2019-02-13 14:37:36', '2019-02-13 14:37:36'),
(268, 2, 'OcUgIJYtWXcJUBMelwzkMMtkT5uwREQs', '2019-02-13 14:39:14', '2019-02-13 14:39:14'),
(270, 3, 'mgYLvg6dWGB8Tn7xl5didIvi2TtznyH3', '2019-02-13 14:39:31', '2019-02-13 14:39:31'),
(272, 1, 'O2hskdr9m5LZP5apGKFVjLCHNoJqPvvV', '2019-02-13 14:45:50', '2019-02-13 14:45:50'),
(273, 1, 'MF2B5wd6tY95KURIPu3hcq1sluMvRksW', '2019-02-13 14:45:50', '2019-02-13 14:45:50'),
(274, 1, 'uTsnm5WIZn85qkkB3a1uqeaFbLwIRXUY', '2019-02-13 09:37:12', '2019-02-13 09:37:12'),
(275, 1, 'znLradFsCJ5FOZereYt7d0j2A2m7viWW', '2019-02-13 09:37:13', '2019-02-13 09:37:13'),
(276, 1, 'LzAzumoB5oKu4QJb1ujXyNsvmo0HkRu5', '2019-02-13 14:35:26', '2019-02-13 14:35:26'),
(277, 1, 'vXTt0IhNdzAxUc2hOHxebkXCC2oYr6iq', '2019-02-13 14:35:27', '2019-02-13 14:35:27'),
(278, 1, 'gVwFfAxMIoGyMnaoEu4mPbj4dlWiA7Lg', '2019-02-14 08:13:01', '2019-02-14 08:13:01'),
(279, 1, 'zbcesX0cd8voH5ftaIEUJhPZSlPcJ08o', '2019-02-14 08:13:01', '2019-02-14 08:13:01'),
(280, 3, '8bvw6DPp8Tsq1KhLYSSolsI6mTIsMbul', '2019-02-14 13:45:50', '2019-02-14 13:45:50'),
(282, 5, '3nllzeNOvRtm8nbxB26MMkjniOkpcaXW', '2019-02-14 13:58:44', '2019-02-14 13:58:44'),
(283, 5, 'wtN1lksmfZKp3V6N93JwYLOHgB4RCkCf', '2019-02-14 13:58:44', '2019-02-14 13:58:44'),
(284, 5, 'jOW9NZmhQB8wS1uTmQSkLpwCQRXbRtJ4', '2019-02-14 14:34:45', '2019-02-14 14:34:45'),
(286, 1, 'xirJyleE0b53GcLo4ya6CoubJWrN9NHH', '2019-02-14 17:11:07', '2019-02-14 17:11:07'),
(288, 2, 'XpyhCrpFAV6blYszBueaSeh323q4iFb4', '2019-02-14 17:11:40', '2019-02-14 17:11:40'),
(290, 1, 'Yx2CsFPfv65HonbJd6HPxTDc1DgS7e2S', '2019-02-14 17:33:18', '2019-02-14 17:33:18'),
(292, 3, 'vm7QFOsa4vNk9Vcz98LKv84IVF3TsM7S', '2019-02-14 17:42:17', '2019-02-14 17:42:17'),
(293, 3, 'cbPDfgCPcAmZeSe5Q00TbE3fs705gZjL', '2019-02-14 17:42:17', '2019-02-14 17:42:17'),
(294, 1, 'D9XI0HEhy3yiNEXxO1lhLcN9nQiWufWh', '2019-02-14 18:52:13', '2019-02-14 18:52:13'),
(295, 1, 'yrGbp9SVpfQRPdTwmqSUbWeGw0yV1hd7', '2019-02-14 18:52:13', '2019-02-14 18:52:13'),
(296, 1, 'hnX9wYRAX5RZjwa49eRJaEgWYbzblkTq', '2019-02-15 19:48:02', '2019-02-15 19:48:02'),
(297, 1, '0OXS8iiz2MZfaQlJJvTvnP8QJLQl0svp', '2019-02-15 19:48:02', '2019-02-15 19:48:02'),
(298, 2, 'yy51ojiUQwMNf8Dj8TL0DIVY4O1C2km3', '2019-02-15 20:26:13', '2019-02-15 20:26:13'),
(299, 2, '0gxB8EynHUIYp0ve78r1DSlslqawfPlM', '2019-02-15 20:26:13', '2019-02-15 20:26:13'),
(300, 2, 'KMYkbFyTQtKMvQlM57QVvodWYcI8ylyv', '2019-02-15 23:31:35', '2019-02-15 23:31:35'),
(302, 3, 'c6OKGPuRxMv5UZdkGpszdQwJMENOc5mj', '2019-02-15 23:54:04', '2019-02-15 23:54:04'),
(303, 3, 'cyfEKvvESG4b8SEiG6z6E9RMRJ9NHSOE', '2019-02-15 23:54:04', '2019-02-15 23:54:04'),
(304, 3, 'hXaADXetsWI08HtEmyX0lAH8QZKDiHmy', '2019-02-16 02:21:54', '2019-02-16 02:21:54'),
(306, 1, '8xTnXsnhpqvJlzKCKqNXthdhOwqcNssy', '2019-02-16 02:22:10', '2019-02-16 02:22:10'),
(308, 3, 'AmtsyYG909kVFvoOefh7r8CEkeuXOtPA', '2019-02-16 03:36:33', '2019-02-16 03:36:33'),
(309, 3, 'b0muFPPIeZQC22nlAihxvq9FB4pbX2SM', '2019-02-16 03:36:33', '2019-02-16 03:36:33'),
(310, 3, 'G79fkGFYJT4SCy7ioLG1P9OPuZPiAlze', '2019-02-16 08:44:19', '2019-02-16 08:44:19'),
(311, 3, 'PBAT703CGxEyeAW5bmBZBKJIHtL6NnR6', '2019-02-16 08:44:19', '2019-02-16 08:44:19'),
(312, 1, 'wYz0uBNIRt2DZLsHbPFcVRm2ebmdpmnk', '2019-02-16 20:10:21', '2019-02-16 20:10:21'),
(314, 1, 'E4aklndHHohZSQ1a1DC1M9VZ6qITz5tx', '2019-02-16 20:11:46', '2019-02-16 20:11:46'),
(316, 1, 'u0EuRWpQROpIMNDLDPp2UGXiUKvBS0dP', '2019-02-16 20:16:19', '2019-02-16 20:16:19'),
(318, 1, 'XgLDcDvdVZREZTR98lHswSQtNLY3QJHA', '2019-02-16 20:16:25', '2019-02-16 20:16:25'),
(320, 1, 'i5HuGP98qF398gOQmWjURZog27c6bvRQ', '2019-02-16 20:17:07', '2019-02-16 20:17:07'),
(322, 1, 'iuKqKKGJCHfVFRPnQn4ql7txWKRTbxfV', '2019-02-16 20:17:25', '2019-02-16 20:17:25'),
(324, 1, '3e1Bq29M6iMPsx8YZ8N0p4iQtQRB6AUI', '2019-02-16 20:17:41', '2019-02-16 20:17:41'),
(326, 3, 'gKTvkdvstmoCwOLbyuqi4SizSUHymRl6', '2019-02-16 20:22:14', '2019-02-16 20:22:14'),
(328, 2, 'e2TYsqoPktU4KDeXMMmDGI41EKdUG4wP', '2019-02-16 20:28:13', '2019-02-16 20:28:13'),
(330, 3, 'DXSxAFmbB9F7McS3CWnju1VTUeYHkwSq', '2019-02-16 20:33:41', '2019-02-16 20:33:41'),
(331, 3, 'INV3RvQUZVD9g0KscjgEIA6PPUoMqFdV', '2019-02-16 20:33:42', '2019-02-16 20:33:42'),
(332, 2, 'sBJUvQP3WCBbRR5NgvtRgamIpytfmpNv', '2019-02-16 20:36:45', '2019-02-16 20:36:45'),
(333, 2, 'TEWprtynusH0L5cSmZ6y8DFzRLYtLY9V', '2019-02-16 20:36:45', '2019-02-16 20:36:45'),
(334, 1, '0HtqbTKry5Xwe5Pq589Dh0LudGNuRZEM', '2019-02-16 20:43:37', '2019-02-16 20:43:37'),
(336, 1, 'PuOiyuWH0JnRhTNTJtjsuV4zFsUTwAj9', '2019-02-16 23:27:52', '2019-02-16 23:27:52'),
(338, 2, '4n7GbY5a2gwytKy40DYJUy1CmKtZXv4O', '2019-02-16 23:40:03', '2019-02-16 23:40:03'),
(340, 1, 'wzOeGNfclPIb9KMUaPrwt7ppulAfV2j0', '2019-02-17 00:51:22', '2019-02-17 00:51:22'),
(341, 1, 'EMwmEas2m90nLWESeBMxY3sMggFc2SdB', '2019-02-17 00:51:22', '2019-02-17 00:51:22'),
(342, 3, 'XcQwQwyWkyLGQ5eHiGXlhEtWHsB1Jv5A', '2019-02-17 01:29:19', '2019-02-17 01:29:19'),
(344, 2, 'Y53Ic0txdb5E5mBdoSsiWAcLj1NxCTbR', '2019-02-17 02:44:56', '2019-02-17 02:44:56'),
(345, 2, 'iaLuWeysHoWbIJmjX41kXvsTR7TKKoFI', '2019-02-17 02:44:56', '2019-02-17 02:44:56'),
(346, 1, 'SmY2NZ3uRhsVrzBGXHrJiIKF05EJCWOS', '2019-02-17 05:45:44', '2019-02-17 05:45:44'),
(347, 1, 'E0Cg0MSmsI5BYZLj0f48WomivJ28o9yA', '2019-02-17 05:45:44', '2019-02-17 05:45:44'),
(348, 3, 'M9tKRqwFvEYAohvxU3c4ncPzbAi88KuS', '2019-02-17 05:46:01', '2019-02-17 05:46:01'),
(349, 3, '3yeBY8kXI2baljLeVzmMIAr0dlAKeKol', '2019-02-17 05:46:01', '2019-02-17 05:46:01');

-- --------------------------------------------------------

--
-- Table structure for table `reminders`
--

CREATE TABLE `reminders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `form_group_id` int(10) UNSIGNED NOT NULL,
  `form_question` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_answer` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `request_content` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `requests_group_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `requests_step` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `requests_group`
--

CREATE TABLE `requests_group` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `form_group_id` int(10) UNSIGNED NOT NULL,
  `last_step` tinyint(4) DEFAULT NULL,
  `is_completed` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'when from step completed',
  `is_accepted` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'when get accepted from 2 people',
  `first_accepter` tinyint(4) DEFAULT NULL COMMENT 'Person who accept this requests',
  `second_accepter` tinyint(4) DEFAULT NULL COMMENT 'Person who accept this requests',
  `first_accepted` date DEFAULT NULL COMMENT 'Date accepted by first person',
  `second_accepted` date DEFAULT NULL COMMENT 'Date accepted by second person',
  `is_rejected` tinyint(1) NOT NULL DEFAULT '0',
  `rejected_notes` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rejected_by` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `request_group_id` int(10) UNSIGNED NOT NULL,
  `form_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reviews_goal` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reviews_result` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reviews_feedback` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reviews_action` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reviews_date` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reviews_duration` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reviews_session_number` int(11) DEFAULT NULL,
  `reviews_course_name` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'staff', 'Staff', '{\"trainee.manage\":true,\"trainee.review\":true}', '2019-02-16 09:31:37', '2019-02-16 09:31:37'),
(2, 'atasan', 'Atasan', '{\"trainee.list_req\":true,\"trainee.view\":true,\"trainee.approve\":true,\"trainee.edit\":false}', '2019-02-16 09:31:37', '2019-02-16 09:31:37'),
(3, 'hr', 'HR', '{\"trainee.list_req\":true,\"trainee.view\":true,\"trainee.approve\":true,\"trainee.edit\":false,\"admin.createTrainee\":true,\"admin.manageItems\":true,\"admin.manageUsers\":true}', '2019-02-16 09:31:37', '2019-02-16 09:31:37');

-- --------------------------------------------------------

--
-- Table structure for table `role_users`
--

CREATE TABLE `role_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_users`
--

INSERT INTO `role_users` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-02-16 09:31:37', '2019-02-16 09:31:37'),
(2, 2, '2019-02-16 09:31:38', '2019-02-16 09:31:38'),
(3, 3, '2019-02-16 09:31:38', '2019-02-16 09:31:38');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `type`, `ip`, `created_at`, `updated_at`) VALUES
(1, NULL, 'global', NULL, '2019-01-17 12:40:41', '2019-01-17 12:40:41'),
(2, NULL, 'ip', '127.0.0.1', '2019-01-17 12:40:41', '2019-01-17 12:40:41'),
(3, 1, 'user', NULL, '2019-01-17 12:40:41', '2019-01-17 12:40:41'),
(4, NULL, 'global', NULL, '2019-01-17 12:40:53', '2019-01-17 12:40:53'),
(5, NULL, 'ip', '127.0.0.1', '2019-01-17 12:40:53', '2019-01-17 12:40:53'),
(6, 1, 'user', NULL, '2019-01-17 12:40:53', '2019-01-17 12:40:53'),
(7, NULL, 'global', NULL, '2019-01-17 12:41:28', '2019-01-17 12:41:28'),
(8, NULL, 'ip', '127.0.0.1', '2019-01-17 12:41:28', '2019-01-17 12:41:28'),
(9, 1, 'user', NULL, '2019-01-17 12:41:28', '2019-01-17 12:41:28'),
(10, NULL, 'global', NULL, '2019-01-17 13:47:52', '2019-01-17 13:47:52'),
(11, NULL, 'ip', '127.0.0.1', '2019-01-17 13:47:53', '2019-01-17 13:47:53'),
(12, NULL, 'global', NULL, '2019-01-17 13:50:21', '2019-01-17 13:50:21'),
(13, NULL, 'ip', '127.0.0.1', '2019-01-17 13:50:22', '2019-01-17 13:50:22'),
(14, NULL, 'global', NULL, '2019-01-17 13:50:37', '2019-01-17 13:50:37'),
(15, NULL, 'ip', '127.0.0.1', '2019-01-17 13:50:37', '2019-01-17 13:50:37'),
(16, NULL, 'global', NULL, '2019-01-17 13:50:38', '2019-01-17 13:50:38'),
(17, NULL, 'ip', '127.0.0.1', '2019-01-17 13:50:38', '2019-01-17 13:50:38'),
(18, NULL, 'global', NULL, '2019-01-17 14:10:23', '2019-01-17 14:10:23'),
(19, NULL, 'ip', '127.0.0.1', '2019-01-17 14:10:24', '2019-01-17 14:10:24'),
(20, NULL, 'global', NULL, '2019-01-24 17:22:26', '2019-01-24 17:22:26'),
(21, NULL, 'ip', '127.0.0.1', '2019-01-24 17:22:27', '2019-01-24 17:22:27'),
(22, NULL, 'global', NULL, '2019-01-24 17:23:12', '2019-01-24 17:23:12'),
(23, NULL, 'ip', '127.0.0.1', '2019-01-24 17:23:12', '2019-01-24 17:23:12'),
(24, NULL, 'global', NULL, '2019-01-24 17:23:21', '2019-01-24 17:23:21'),
(25, NULL, 'ip', '127.0.0.1', '2019-01-24 17:23:21', '2019-01-24 17:23:21'),
(26, NULL, 'global', NULL, '2019-01-24 17:23:52', '2019-01-24 17:23:52'),
(27, NULL, 'ip', '127.0.0.1', '2019-01-24 17:23:52', '2019-01-24 17:23:52'),
(28, 1, 'user', NULL, '2019-01-24 17:23:52', '2019-01-24 17:23:52'),
(29, NULL, 'global', NULL, '2019-01-24 17:24:05', '2019-01-24 17:24:05'),
(30, NULL, 'ip', '127.0.0.1', '2019-01-24 17:24:05', '2019-01-24 17:24:05'),
(31, 1, 'user', NULL, '2019-01-24 17:24:05', '2019-01-24 17:24:05'),
(32, NULL, 'global', NULL, '2019-01-24 18:49:08', '2019-01-24 18:49:08'),
(33, NULL, 'ip', '127.0.0.1', '2019-01-24 18:49:08', '2019-01-24 18:49:08'),
(34, NULL, 'global', NULL, '2019-01-24 18:50:23', '2019-01-24 18:50:23'),
(35, NULL, 'ip', '127.0.0.1', '2019-01-24 18:50:23', '2019-01-24 18:50:23'),
(36, 1, 'user', NULL, '2019-01-24 18:50:23', '2019-01-24 18:50:23'),
(37, NULL, 'global', NULL, '2019-01-24 18:51:32', '2019-01-24 18:51:32'),
(38, NULL, 'ip', '127.0.0.1', '2019-01-24 18:51:32', '2019-01-24 18:51:32'),
(39, NULL, 'global', NULL, '2019-01-24 18:51:40', '2019-01-24 18:51:40'),
(40, NULL, 'ip', '127.0.0.1', '2019-01-24 18:51:40', '2019-01-24 18:51:40'),
(41, 1, 'user', NULL, '2019-01-24 18:51:40', '2019-01-24 18:51:40'),
(42, NULL, 'global', NULL, '2019-01-24 18:55:06', '2019-01-24 18:55:06'),
(43, NULL, 'ip', '127.0.0.1', '2019-01-24 18:55:07', '2019-01-24 18:55:07'),
(44, NULL, 'global', NULL, '2019-01-29 08:19:46', '2019-01-29 08:19:46'),
(45, NULL, 'ip', '127.0.0.1', '2019-01-29 08:19:46', '2019-01-29 08:19:46'),
(46, NULL, 'global', NULL, '2019-01-31 18:40:54', '2019-01-31 18:40:54'),
(47, NULL, 'ip', '127.0.0.1', '2019-01-31 18:40:55', '2019-01-31 18:40:55'),
(48, 5, 'user', NULL, '2019-01-31 18:40:55', '2019-01-31 18:40:55'),
(49, NULL, 'global', NULL, '2019-02-07 04:49:33', '2019-02-07 04:49:33'),
(50, NULL, 'ip', '127.0.0.1', '2019-02-07 04:49:33', '2019-02-07 04:49:33'),
(51, NULL, 'global', NULL, '2019-02-07 04:51:34', '2019-02-07 04:51:34'),
(52, NULL, 'ip', '127.0.0.1', '2019-02-07 04:51:34', '2019-02-07 04:51:34'),
(53, NULL, 'global', NULL, '2019-02-07 10:46:49', '2019-02-07 10:46:49'),
(54, NULL, 'ip', '127.0.0.1', '2019-02-07 10:46:49', '2019-02-07 10:46:49'),
(55, NULL, 'global', NULL, '2019-02-07 19:11:49', '2019-02-07 19:11:49'),
(56, NULL, 'ip', '182.253.176.228', '2019-02-07 19:11:49', '2019-02-07 19:11:49'),
(57, NULL, 'global', NULL, '2019-02-08 00:34:16', '2019-02-08 00:34:16'),
(58, NULL, 'ip', '182.0.241.123', '2019-02-08 00:34:16', '2019-02-08 00:34:16'),
(59, NULL, 'global', NULL, '2019-02-08 00:46:58', '2019-02-08 00:46:58'),
(60, NULL, 'ip', '182.0.241.123', '2019-02-08 00:46:58', '2019-02-08 00:46:58'),
(61, NULL, 'global', NULL, '2019-02-11 04:29:46', '2019-02-11 04:29:46'),
(62, NULL, 'ip', '103.10.61.86', '2019-02-11 04:29:46', '2019-02-11 04:29:46'),
(63, NULL, 'global', NULL, '2019-02-13 01:12:55', '2019-02-13 01:12:55'),
(64, NULL, 'ip', '192.168.64.1', '2019-02-13 01:12:55', '2019-02-13 01:12:55');

-- --------------------------------------------------------

--
-- Table structure for table `trainees`
--

CREATE TABLE `trainees` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_description` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trainees_for` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trainees`
--

INSERT INTO `trainees` (`id`, `name`, `short_description`, `description`, `icon`, `slug`, `trainees_for`, `created_at`, `updated_at`) VALUES
(1, 'Assignment', 'Assignment short description', 'Assignment long description', 'icon-contract-1', 'trainee/request/assignment', 'staff', '2019-02-13 22:34:44', '2019-02-13 22:34:48'),
(2, 'Mentoring & Coaching', 'Mentoring & Coaching short description', 'Mentoring & Coaching long description', 'icon-presentation-1', 'trainee/request/mentoring', 'staff', '2019-02-13 22:34:44', '2019-02-13 22:34:48'),
(3, 'Public Training', 'Public Training short description', 'Public Training long description', 'icon-conference', 'trainee/request/public-training', 'staff', '2019-02-13 22:34:44', '2019-02-13 22:34:48'),
(4, 'External Speaker', 'External Speaker short description', 'External Speaker long description', 'icon-training-1', 'trainee/request/external-speaker', 'staff', '2019-02-13 22:34:44', '2019-02-13 22:34:48'),
(5, 'Inhouse', 'Inhouse short description', 'Inhouse long description', 'icon-conference', 'trainee/request/inhouse', 'admin', '2019-02-13 22:34:44', '2019-02-13 22:34:48'),
(6, 'Internal Coach', 'Internal Coach short description', 'Internal Coach long description', 'icon-training-1', 'trainee/request/internal-coach', 'admin', '2019-02-13 22:34:44', '2019-02-13 22:34:48');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `permissions`, `last_login`, `first_name`, `last_name`, `created_at`, `updated_at`) VALUES
(1, 'mydevstaff@mailinator.com', '$2y$10$fAm616fwu1rVwhnNadi6zuSvIl6xhL4fetaQcvl5isBXvQ2BfalkC', NULL, '2019-02-17 05:45:44', NULL, NULL, '2019-02-16 09:31:37', '2019-02-17 05:45:44'),
(2, 'mydevatasan@mailinator.com', '$2y$10$TjVgLkf3uXkc2IE00WeSTeBT6Xj/bTF3R6qBHzHPqYBDCHhzpD/qK', NULL, '2019-02-17 02:44:56', NULL, NULL, '2019-02-16 09:31:37', '2019-02-17 02:44:56'),
(3, 'mydevadmin@mailinator.com', '$2y$10$aWw1PYTHa5jBVQMJvTie.u51MvqoSbbRjEgdYvNaPZL5mivnDmep2', NULL, '2019-02-17 05:46:01', NULL, NULL, '2019-02-16 09:31:38', '2019-02-17 05:46:01');

-- --------------------------------------------------------

--
-- Table structure for table `users_relation`
--

CREATE TABLE `users_relation` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` tinyint(4) NOT NULL,
  `user_role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_staff_id` tinyint(4) DEFAULT NULL,
  `user_first_atasan_id` tinyint(4) DEFAULT NULL,
  `user_second_atasan_id` tinyint(4) DEFAULT NULL,
  `user_group_head_id` tinyint(4) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_relation`
--

INSERT INTO `users_relation` (`id`, `user_id`, `user_role`, `user_staff_id`, `user_first_atasan_id`, `user_second_atasan_id`, `user_group_head_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'user', NULL, 2, 3, NULL, NULL, NULL, NULL),
(3, 2, 'user', NULL, 2, 3, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activations`
--
ALTER TABLE `activations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `persistences`
--
ALTER TABLE `persistences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persistences_code_unique` (`code`);

--
-- Indexes for table `reminders`
--
ALTER TABLE `reminders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requests_group`
--
ALTER TABLE `requests_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `throttle_user_id_index` (`user_id`);

--
-- Indexes for table `trainees`
--
ALTER TABLE `trainees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `trainees_slug_unique` (`slug`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users_relation`
--
ALTER TABLE `users_relation`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activations`
--
ALTER TABLE `activations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `persistences`
--
ALTER TABLE `persistences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=350;

--
-- AUTO_INCREMENT for table `reminders`
--
ALTER TABLE `reminders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `requests_group`
--
ALTER TABLE `requests_group`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `trainees`
--
ALTER TABLE `trainees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users_relation`
--
ALTER TABLE `users_relation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
