<?php

    // Mydev System
    $servername_dev = "localhost";
    // $servername_dev = "10.34.42.23";
    $username_dev = "root";
    $password_dev = "";
    // $password_dev = "b47054i!";

    // Mylearning System
    $servername_learning = "10.34.87.7";
    $username_learning = "root";
    $password_learning = "mylearning@2019!!";
    
    // Create connection Mydev
    $conn = new mysqli($servername_dev, $username_dev, $password_dev);

    // Create connection Mylearning
    $conn_learning = new mysqli($servername_learning, $username_learning, $password_learning);
    
    // Check connection
    if ($conn->connect_error && $conn_learning->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }else{

        // Mydev
        $db_dev = $conn->select_db('mydev');
        $sql_dev_point = "SELECT u.username, u.first_name, ph.created_at, u.id as userid from users u left join users_point_history ph ON ph.user_id=u.id";
        $result = mysqli_query($conn, $sql_dev_point);
        $result_dev = array();
        // Fetch all
        while ($row = mysqli_fetch_array($result)) {
            $result_dev[] = $row;
        }

        foreach($result_dev as $row){
            // Mylearning
            $db_mylearning = $conn_learning->select_db('mylearning_db');
            $sql_mylearning_point = "SELECT u.username, u.firstname, x.xp, x.eventname, x.time, u.id as userid from mdl_user u left join mdl_block_xp_log x ON x.userid=u.id WHERE x.time !=".strtotime($row['created_at']);
            $result_learn = mysqli_query($conn_learning, $sql_mylearning_point);
            $result_learning = array();

            // Fetch all Mylearning
            while ($row_learning = mysqli_fetch_array($result_learn)) {
                $result_learning[] = $row_learning;
            }
            foreach($result_learning as $row_learning){
                if($row['username']==$row_learning['username']){
                    $insert_mydev_history = "INSERT INTO users_point_history (user_id, point, form_group_id, requests_group_id, created_at, description) VALUES (".$row_learning['userid'].",".$row_learning['xp'].",0,0,"."'".date('Y-m-d H:i:s', $row_learning['time'])."'".","."'".$row_learning['eventname']."'".")";
                    $result_ins = mysqli_query($conn, $insert_mydev_history);
                    $result_ins_dev = array();
                }
            }
        }
        mysqli_close($conn_learning);
        mysqli_close($conn);
    }
    
?>