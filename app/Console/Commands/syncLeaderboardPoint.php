<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Modules\Auth\Models\UserModel;
use App\Modules\Trainee\Models\users_point_history_model;

class syncLeaderboardPoint extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:leaderboard';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync the Mylearning leaderboard';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', env('API_URL', 'http://localhost/').'course/mydevsyncleaderboardpoint');
        $callback = json_decode($response->getBody()->getContents());

        if(!$callback->error){
            foreach($callback->result as $row){
                $user_data = UserModel::where('email',$row->email)->first();

                if($user_data){
                    if(empty($user_data->point)){
                        $point = $row->xp; 
                        $point_history = users_point_history_model::where('user_id', $user_data->id)
                            ->where('description', 'Mylearning - Sync Point')
                            ->first();

                        if(!$point_history){
                            UserModel::where('id',$user_data->id)->update(['point'=> $point]);
    
                            $point_history_model = new users_point_history_model();
                            $point_history = [
                                'user_id' => $user_data->id,
                                'form_group_id' => 0,
                                'requests_group_id' => 0,
                                'point' => $point,
                                'description' => 'Mylearning - Sync Point'
                            ];
                            
                            $point_history_model::create($point_history);
                        }
                    }
                }
            }

            $this->info('success sync leaderboard !' . PHP_EOL);
        }else{
            $this->error('failed sync leaderboard !' . PHP_EOL);
            echo '<pre>'; print_r($callback);
        }
    }
}
