<?php

namespace App\Modules\Auth\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class LdapAuthController extends Controller
{
    //


    function index() {
        $ldap_password = 'MyL34rn1ng';
        $ldap_username = 'CN=Mylearning,OU=Account and Mailbox Operational,DC=office,DC=corp,DC=indosat,DC=com';
        $ldap_connection = ldap_connect("ldap://10.10.100.137:389");

        $login_username='81147033';
        $login_password='%B47054I%';

        if (FALSE === $ldap_connection){
            echo 'Failed to connect into LDAP server';
        }

// We have to set this option for the version of Active Directory we are using.
        ldap_set_option($ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3) or die('Unable to set LDAP protocol version');
        ldap_set_option($ldap_connection, LDAP_OPT_REFERRALS, 0); // We need this for doing an LDAP search.


        /**** CALL THE FUNCTION HERE****/
        $this->get_users($ldap_connection, $ldap_username, $ldap_password);
        $this->user_login($ldap_connection, $ldap_username, $ldap_password, $login_username, $login_password);
    }

/***** FUNCTION HERE ******/
function user_login($ldap_connection, $ldap_username, $ldap_password, $login_username, $login_password){
    if (TRUE === ldap_bind($ldap_connection, $ldap_username, $ldap_password)){
        $ldap_base_dn = 'dc=office,dc=corp,dc=indosat,dc=com';
        $search_filter = '(&(objectCategory=person)(samaccountname=*))';
        $attributes = array();
        $attributes[] = 'givenname';
        $attributes[] = 'mail';
        $attributes[] = 'samaccountname';
        $attributes[] = 'sn';
        $attributes[] = 'manager';
        $attributes[] = 'streetaddress';
        $attributes[] = 'department';
        $attributes[] = 'homephone';
        $result = ldap_search($ldap_connection, $ldap_base_dn, $search_filter, $attributes);

        if (FALSE !== $result){
            $entries = ldap_get_entries($ldap_connection, $result);
            for ($x=0; $x<$entries['count']; $x++){
                $ad_users[strtoupper(trim($entries[$x]['samaccountname'][0]))] = array(
                    'email' => strtolower(trim($entries[$x]['mail'][0])),
                    'first_name' => trim($entries[$x]['givenname'][0]),
                    'last_name' => trim($entries[$x]['sn'][0]),
                    'manager' => trim($entries[$x]['manager'][0]),
                    'streetaddress' => trim($entries[$x]['streetaddress'][0]),
                    'department' => trim($entries[$x]['department'][0]),
                    'homephone' => trim($entries[$x]['homephone'][0]),
                );
            }
        }

        ldap_unbind($ldap_connection); // Clean up after ourselves.
    }

    $nik_list = array_keys($ad_users);

    if(in_array($login_username, $nik_list)){
        echo 'Login Success !';
    }else{
        echo 'Login Failed !';
    }

    echo '<pre>'; print_r($nik_list);
}

function get_users($ldap_connection, $ldap_username, $ldap_password, $show_all_entries = false){
    if (TRUE === ldap_bind($ldap_connection, $ldap_username, $ldap_password)){
        $ldap_base_dn = 'dc=office,dc=corp,dc=indosat,dc=com';
        $search_filter = '(&(objectCategory=person)(samaccountname=*))';
        $attributes = array();
        $attributes[] = 'givenname';
        $attributes[] = 'mail';
        $attributes[] = 'samaccountname';
        $attributes[] = 'sn';
        $attributes[] = 'manager';
        $attributes[] = 'streetaddress';
        $attributes[] = 'department';
        $attributes[] = 'homephone';
        $result = ldap_search($ldap_connection, $ldap_base_dn, $search_filter, $attributes);

        if (FALSE !== $result){
            $entries = ldap_get_entries($ldap_connection, $result);

            if($show_all_entries){
                echo '<pre>'; print_r($entries); exit;
            }

            for ($x=0; $x<$entries['count']; $x++){
                // if (!empty($entries[$x]['givenname'][0]) &&
                //      !empty($entries[$x]['mail'][0]) &&
                //      !empty($entries[$x]['samaccountname'][0]) &&
                //      !empty($entries[$x]['sn'][0]) &&
                //      'Shop' !== $entries[$x]['sn'][0] &&
                //      'Account' !== $entries[$x]['sn'][0] &&
                //    	 !empty($entries[$x]['manager'][0]) &&
                //    	 !empty($entries[$x]['streetaddress'][0]) &&
                //    	 !empty($entries[$x]['department'][0]) &&
                //    	 !empty($entries[$x]['homephone'][0])
                //    	){

                $ad_users[strtoupper(trim($entries[$x]['samaccountname'][0]))] = array(
                    'email' => strtolower(trim($entries[$x]['mail'][0])),
                    'first_name' => trim($entries[$x]['givenname'][0]),
                    'last_name' => trim($entries[$x]['sn'][0]),
                    'manager' => trim($entries[$x]['manager'][0]),
                    'streetaddress' => trim($entries[$x]['streetaddress'][0]),
                    'department' => trim($entries[$x]['department'][0]),
                    'homephone' => trim($entries[$x]['homephone'][0]),
                );
                // }
            }
        }

        ldap_unbind($ldap_connection); // Clean up after ourselves.
    }

    echo "Retrieved ". count($ad_users) ." Active Directory users\n";
    echo '<pre>'; print_r($ad_users);
}
}
