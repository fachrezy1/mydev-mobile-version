<?php

namespace App\Modules\Auth\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Auth\Models\UserModel;
use App\Modules\Auth\Models\UserRolesModel;
use App\Modules\Auth\Models\EmployeeModel;
use App\Helpers\UserRolesHelper;
use App\Helpers\ActivityHelper;
use App\Modules\Trainee\Models\users_point_history_model;
// add data
use Spatie\Activitylog\Models\Activity;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use DB;
// end add
use mysql_xdevapi\Session;
use Sentinel;

class AuthenticationController extends Controller
{

    private $activity;

    function __construct()
    {
        $this->activity = new ActivityHelper();
    }

    private function checkAndRedirect($set_as = null) {
        $user = Sentinel::getUser();
        
        if ($set_as) {
            session(['roles'=>$set_as]);
        }

        if (session('multi_roles')) {
            if(session('roles') == 'staff' ) {

                return redirect(route('trainee.index'));

            } else if (session('roles') == 'hr' || session('roles') == 'atasan' ) {
                return redirect(route('dashboard.traineeRequest'));
            }

        } else {
            if($user->inRole('staff') ) {
                return redirect(route('trainee.index'));
            } else if ($user->inRole('hr') || $user->inRole('atasan') ) {
                return redirect(route('dashboard.dashboardIndex'));
            }
        }
        return redirect('404');
    }

    public function index() {
        if(Sentinel::check()){
            return $this->checkAndRedirect();
        }

        $error = session('error');
        if ($error) {
            $data['error'] = $error['msg'];
        } else {
            $data['error'] = null;
        }
        return view('auth::index',$data);
    }

    public function setRoleUser(Request $request){
        if(Sentinel::check()){
            $role = $request['attach'];
            session(['roles' => $role]);

            return $this->checkAndRedirect();

        } return route('404');
    }

    public function add_log($log_name,$desc,$subject_id,$browser,$internet_protocol,$creat,$update){
        //for log

        DB::insert('insert into login_log (log_name,description,subject_username,browser,internet_protocol,created_at,updated_at) values (?,?,?,?,?,?,?)',[$log_name,$desc,$subject_id,$browser,$internet_protocol,$creat,$update]);

        return "log_success";
    }

    public function process(Request $request) {
        $login = true;
        $ldap_success = false;
        $login_ldap = env("LDAP_LOGIN", false);
        $credentials = $request->toArray();

        $user = Sentinel::check();

        unset($credentials['login']);
        $credentials['username'] = $credentials['email'];
        unset($credentials['email']);

        if ($user){
            Sentinel::logout($user);
        }

        // get Agent
        // add
        $agent = new \Jenssegers\Agent\Agent();
        $browser = $agent->browser();
        date_default_timezone_set('Asia/Jakarta');
        // end add

        if(is_numeric($credentials['username']) && $login_ldap){
            $ldap_success = $this->processLDAPLogin($credentials);
            if($ldap_success !== true){
                if($ldap_success == 'unregister_employee'){
                    $session = ['msg' => 'Your employee ID not registered as Indosat employee'];

                    //log
                    $this->add_log("default","Your employee ID not registered as Indosat employee",$credentials['username'],$browser,\Request::getClientIp(true),now(),now());

                }else{
                    $session = ['msg' => 'Wrong password and username combinations'];

                    //log
                    $this->add_log("default","Wrong password and username combinations",$credentials['username'],$browser,\Request::getClientIp(true),now(),now());

                }

                session(['error' => $session]);
                $log_user = User::where(['username'=>$credentials['username']])->get();
                return redirect(route('auth.'));
            }
        }

        if($login){
            $user = Sentinel::authenticate($credentials);

            if (!$user) {
                $session = ['msg' => 'Wrong password and username combinations'];
                session(['error' => $session]);

                $log_user = User::where(['username'=>$credentials['username']])->get();

                //log
                $this->add_log("default","Wrong password and username combinations",$credentials['username'],$browser,\Request::getClientIp(true),now(),now());

                return redirect(route('auth.'));
            }

            $user = Sentinel::login($user);
            $userRole = new UserRolesHelper();

            // if(!$userRole->inAdmin()){
            //     $client = new \GuzzleHttp\Client();
            //     $response = $client->request('GET', env('API_URL', 'http://localhost/').'course/mydevgetpoint?email='.$user->email);
            //     $callback = json_decode($response->getBody()->getContents());
                
            //     if(!$callback->error){
            //         $user_history_point = get_user_point($user->id);
            //         if($callback->result->xp != $user_history_point){
            //             $point = $callback->result->xp - $user_history_point;
                        
            //             UserModel::where('id',$user->id)->update(['point'=> $point]);

            //             $point_history_model = new users_point_history_model();
            //             $point_history = [
            //                 'user_id' => $user->id,
            //                 'form_group_id' => 0,
            //                 'requests_group_id' => 0,
            //                 'point' => $point,
            //                 'description' => 'Mylearning - Sync Point'
            //             ];
            //             $point_history_model::create($point_history);
            //         }
            //     }else{
            //         //handling error
            //     }
            // }

            $this->add_log("default","Login Success",$credentials['username'],$browser,\Request::getClientIp(true),now(),now());

            if(!$userRole->inManager()){
                return $this->checkAndRedirect('staff');
            } else {
                if($userRole->inStaff() && $userRole->inManager()) {
                    session(['multi_roles' => true]);
                    return redirect(route('auth.choose'));
                } else {
                    if($userRole->inManager()) {
                        return $this->checkAndRedirect('atasan');
                    }elseif($userRole->inStaff()){
                        return $this->checkAndRedirect('staff');
                    }elseif($userRole->inAdmin()){
                        return $this->checkAndRedirect('hr');
                    }
                }
            }

        }
    }

    public function logout() {
        $user = Sentinel::check();

        Sentinel::logout();
        session()->flush();
        return redirect(route('auth.'));
    }

    public function choose(){
        return view('auth::switch');
    }

    public function processLDAPLogin($credentials){
        $ldap_info = $this->LDAPAuth($credentials['username'], $credentials['password']);
        
        // get Agent
        // add
        $agent = new \Jenssegers\Agent\Agent();
        $browser = $agent->browser();
        date_default_timezone_set('Asia/Jakarta');
        // end add

        if(is_array($ldap_info)){
            $user_model = new UserModel();
            $user_data = $user_model->where('email', $ldap_info[0]['email'])->first();
            if(!$user_data){
                $allowed_manager_key = ['vp', 'svp', 'dir'];
                $employee_data = EmployeeModel::where('nik', $credentials['username'])->first();

                if(!$employee_data){

                    //log
                    $this->add_log("error","unregister_employee",$credentials['username'],$browser,\Request::getClientIp(true),now(),now());

                    return 'unregister_employee';
                }

                $register_user = Sentinel::registerAndActivate([
                    'email'    => $ldap_info[0]['email'],
                    'password' => $credentials['password'],
                    'username' => $credentials['username'],
                    'first_name' => $employee_data->full_name
                ]);

                //assign staff role to user
                if(!empty($employee_data->manager_id) && !in_array(strtolower($employee_data->job_key_short), ['dir', 'cxo'])){
                    $staff_role = UserRolesModel::where('slug','staff')->first();
                    $register_user->roles()->save($staff_role);
                }

                //assign head role to user
                if(in_array(strtolower($employee_data->job_key_short), $allowed_manager_key)){
                    $staff_role = UserRolesModel::where('slug','atasan')->first();
                    $register_user->roles()->save($staff_role);
                }
            }else{
                $employee_data = EmployeeModel::where('nik', $credentials['username'])->first();
                UserModel::where('id', $user_data->id)
                    ->update([
                        'email'    => $ldap_info[0]['email'],
                        'password' => bcrypt($credentials['password']),
                        'username' => $credentials['username'],
                        'first_name' => $employee_data->full_name
                    ]);
            }

            return true;
        }elseif($ldap_info == "Can't contact LDAP server"){

            //log
            $this->add_log("error","Can't contact LDAP server",$credentials['username'],$browser,\Request::getClientIp(true),now(),now());

            return true;
        }elseif($ldap_info == 'Invalid credentials'){

            //log
            $this->add_log("error","Invalid credentials",$credentials['username'],$browser,\Request::getClientIp(true),now(),now());

            if(in_array($credentials['username'], ['73126499', '30000028'])){
                return true;
            }else{
                return false;
            }
        }
    }

    public function LDAPAuth($username, $password){
        // $datapri[] = array(
        //     'nik' => 81147033,
        //     'fullname' => 'Miftahuddin Syarief',
        //     'email' => 'miftahuddin.syarief@indosatooredoo.com'
        // );

        // return $datapri;

        $adServer = env("LDAP_HOST", "10.10.100.137");
        $ldap = ldap_connect($adServer,env("LDAP_PORT", "389"));
        $ldaprdn = 'indosat' . "\\" . $username;

        ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);
        ldap_set_option($ldap, LDAP_OPT_NETWORK_TIMEOUT, 2); //set ldap timeout to 2 sec

        $bind = @ldap_bind($ldap, $ldaprdn, $password);

        if ($bind) {
            $filter="(sAMAccountName=$username)";
            $result = ldap_search($ldap,"dc=office,dc=corp,dc=Indosat,dc=com",$filter);
            $info = ldap_get_entries($ldap, $result);

            $datapri = array();

            for ($i=0; $i<$info["count"]; $i++) {
                $fetchdata["nik"] = $username;
                $fetchdata["fullname"] = isset($info[$i]["cn"]) ? $info[$i]["cn"][0] : null;
                $fetchdata["personal_number"] = isset($info[$i]["mobile"]) ? $info[$i]["mobile"][0] : null;
                $fetchdata["email"] = isset($info[$i]["mail"]) ? $info[$i]["mail"][0] : null;
                array_push($datapri, $fetchdata);
            }
            @ldap_close($ldap);

            return $datapri;
        } else {
            return ldap_error($ldap);
        }

    }
}
