<?php

namespace App\Modules\Auth\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;


class EmployeeModel extends Model
{
    //
    use SoftDeletes;
    protected $table = 'employee';
    protected $fillable = ['nik','full_name','gender','date_of_birth','personal_number','employee_type','religion','address','city','state','country','telp_number','postal_code','email','department_code_1','department_code_2','organizational_unit','joining_date','retiring_date','employee_status','position','location','directorate_code','directorate','chief_code','chief','group_code','group_name','division_code','division','personnel_area','job_key_short','cell_phone','manager_id','office_name','office_phone','office_address','office_city','office_province_state','office_postal_code','direct_line','ext','function_division','cell_2'];
    protected $primaryKey = 'id';
}
