<?php

namespace App\Modules\Auth\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modules\Trainee\Models\requestsGroupModel;
use Carbon\Carbon;


class UserModel extends Model
{
    //
    protected $table = 'users';
    protected $fillable = ['email','password','first_name','last_name'];
    protected $primaryKey = 'id';

    public function requestGroup()
    {
        return $this->hasMany('App\Modules\Trainee\Models\requestsGroupModel', 'user_id');
    }
}


