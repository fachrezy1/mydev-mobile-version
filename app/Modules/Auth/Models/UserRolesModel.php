<?php

namespace App\Modules\Auth\Models;

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Roles\EloquentRole;

class UserRolesModel extends EloquentRole
{
    //
    protected $table = 'roles';
    protected $fillable = ['slug','name','permissions'];
    protected $primaryKey = 'id';
}
