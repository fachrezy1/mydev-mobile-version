var mailSelector = $('#login-admin-content_form-email'),
    passSelector = $('#login-admin-content_form-password'),
    mailNotValid = $('#notValid'),
    passNotValid = $('#passwordChars'),
    form= $('.login-admin-content_form'),
    isPasswordValid,
    isMailValid;

const validateEmail = (input, selector) => {
    if (validator.isEmail(input)) {
        selector.removeClass('invalid-input')
        mailNotValid.addClass('d-none')
        isMailValid = true
        if (isPasswordValid) {
            form.unbind('submit')
        }
    }else{
        selector.addClass('invalid-input')
        mailNotValid.html('Email Not Valid')
        mailNotValid.removeClass('d-none')
        isMailValid = false
        form.submit(false)
    }
}

const validatePassword = (input, selector) => {
    if (validator.isEmpty(input)) {
        passSelector.addClass('invalid-input')
        passNotValid.html('Password Must not Empty')
        passNotValid.removeClass('d-none')
        isPasswordValid = false
        form.submit(false)
    } else if (validator.isLength(input,{min:1,max:7})) {
        passSelector.addClass('invalid-input')
        passNotValid.html('Password Must be 8 to 16 chars')
        passNotValid.removeClass('d-none')
        isPasswordValid = false
        form.submit(false)
    } else {
        passSelector.removeClass('invalid-input')
        passNotValid.addClass('d-none')
        isPasswordValid = true
        if (isMailValid) {
            form.unbind('submit')
        }
    }
}

mailSelector.on('change',() => {
    validateEmail(mailSelector.val(), mailSelector)
})

mailSelector.on('keyup',(input) => {
    validateEmail(mailSelector.val(), mailSelector)
})

mailSelector.on('keypress',() => {
    validateEmail(mailSelector.val(), mailSelector)
})


passSelector.on('change',() => {
    validatePassword(passSelector.val(), passSelector)
})

passSelector.on('keyup',() => {
    validatePassword(passSelector.val(), passSelector)
})

passSelector.on('keypress',() => {
    validatePassword(passSelector.val(), passSelector)
})
