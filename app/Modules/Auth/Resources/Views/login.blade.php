<div class="login-page">
    <div class="left-login">
        <img src="{{ url('images/splash.jpg') }}" alt="ooredoo" class="img-fluid">
    </div>
    <div class="right-login">
        <div class="login-auth">
            <div class=" top-login-auth">
                <img src="{{ url('images/logo-medium.png') }}" alt="indosat ooredoo" class="img-fluid">
            </div>

            <div class="login-input">
                <h1 class="text-center">Sign In</h1>
                <form action="{{ route('auth.process') }}" method="POST" class="login-form" autocomplete="off">
                    @csrf
                    <div class="form-group bmd-form-group">
                        <label for="input1" class="bmd-label-floating">Employee ID</label>
                        <input type="text" name="email" class="form-control" id="login-admin-content_form-email" required autocomplete="off">
                        <span class="bmd-help">Use your Employee ID</span>
                        @if ($error)
                            <small id="notValid" class="form-text text-danger"> {{ $error }}</small>
                        @else
                            <small id="notValid" class="form-text text-danger d-none"></small>
                        @endif
                    </div>

                    <div class="form-group bmd-form-group">
                        <label for="input2" class="bmd-label-floating">Password</label>
                        <input type="password" name="password" class="form-control" id="login-admin-content_form-password" required autocomplete="off">
                        <span class="bmd-help">Use your password</span>
                        <small id="passwordChars" class="form-text text-danger d-none">Password Must be 8 to 16 chars</small>
                    </div>

                    <div class="login-group">
                        <div class="employee-level">
                            <button type="submit" name="login" class="btn btn-primary btn-lg">Login<div class="ripple-container"></div></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
