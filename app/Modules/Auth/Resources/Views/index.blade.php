@php($agent = new \Jenssegers\Agent\Agent())
@extends('core.main')

@section('content')
    <section class="auth-page">
        {{-- @if($agent->isMobile())
            @include('auth::login-mobile')
        @else --}}
            @include('auth::login')
        {{-- @endif --}}
    </section>
@endsection



@section('scripts')
    <script>
        $(document).ready(function() {
            $('body').bootstrapMaterialDesign();
        })
    </script>
    @if (session('message'))
        <script>
            alert('{{session('message')}}')
        </script>
    @endif
@endsection
