@extends('core.trainee.main')
@php
    $user = \Sentinel::check();
@endphp

@section('content')
<main class="main-content">
    <section class="auth-page">


        <section class="role-page">
            <div class="role-box">
                <div class="role-title">
                    <h1>What I'm Here For</h1>
                </div>
                <div class="role-people">

                    @if($user->inRole('staff'))
                    <div class="role-item">
                      <span class="selected-role">
                        <i class="icon-tick"></i>
                      </span>
                        <div class="role-icon">
                            <i class="icon-user-1"></i>
                        </div>
                        <div class="roll-text">
                            <h3>For My Development</h3>
                            <p>Use this screen if you're staff employee</p>
                            <form action="{{route('auth.setRoleUser')}}" method="post">
                                @csrf
                                <input type="hidden" name="attach" value="staff">
                                <button type="submit" class="btn btn-default" >Login as User <i class="icon-next-2"></i> </button>
                            </form>
                        </div>
                    </div>
                    @endif
                    @if($user->inRole('atasan'))

                    <div class="role-item">
                      <span class="selected-role">
                        <i class="icon-tick"></i>
                      </span>
                        <div class="role-icon">
                            <i class="icon-link"></i>
                        </div>
                        <div class="roll-text">
                            <h3>For My Team development</h3>
                            <p>Use this screen if you're Manager</p>

                            <form action="{{route('auth.setRoleUser')}}" method="post">
                                @csrf
                                <input type="hidden" name="attach" value="atasan">
                                <button type="submit" class="btn btn-default" >Login as Line Manager <i class="icon-next-2"></i> </button>
                            </form>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </section>
    </section>
</main>
@endsection

@section('scripts')
    <script type="text/javascript">
        jQuery(function($){
            $('#traineeDetail').on('shown.bs.modal', function (e) {
                $('.scroll-content').slimScroll({
                    height: '330px',
                    size: '5px',
                    position: 'right',
                    color: '#fcd401',
                    alwaysVisible: true,
                    distance: '0px',

                    railVisible: true,
                    railColor: '#222',
                    railOpacity: 0.1,

                    allowPageScroll: true,
                    disableFadeOut: false
                });
            });

            $(".trainee-option").owlCarousel({
                items: 4,
                loop: false,
                margin: 10,
                nav: true,
                dots: false,
                smartSpeed: 900,
                autoplay: true,
                navigationText: ["<i class='mdi mdi-chevron-left'></i>", "<i class='mdi mdi-chevron-right'></i>"],
                responsive: {
                    0: {
                        items: 2
                    },
                    990: {
                        items: 3
                    },
                    1023: {
                        items: 5
                    }
                }
            });
        });

    </script>
@endsection
