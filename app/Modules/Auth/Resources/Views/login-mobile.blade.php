<div class="login-screen">
    <div class="login-auth">
        <div class="top-login-auth">
            <img src="{{ url('images/logo-medium.png') }}" alt="indosat ooredoo" class="img-fluid">
        </div>
        <div class="login-input">
            <h1>Sign In</h1>
            <form action="{{ route('auth.process') }}" method="POST" class="login-form">
                @csrf
                <div class="form-group bmd-form-group">
                    <label for="input1" class="bmd-label-floating">Employee ID</label>
                    <input type="text" name="email" class="form-control" id="input1">
                    <span class="bmd-help">Use your user name</span>
                    @if ($error)
                        <small id="notValid" class="form-text text-danger"> {{ $error }}</small>
                    @else
                        <small id="notValid" class="form-text text-danger d-none"></small>
                    @endif
                </div>
                <div class="form-group bmd-form-group">
                    <label for="input2" class="bmd-label-floating">Password</label>
                    <input type="password" name="password" class="form-control" id="login-admin-content_form-password" required>
                    <small id="passwordChars" class="form-text text-danger d-none">Password Must be 8 to 16 chars</small>
                </div>
                <button type="submit" class="btn btn-primary btn-lg" >Login<div class="ripple-container"></div></button>
            </form>
        </div>

    </div>
</div>
