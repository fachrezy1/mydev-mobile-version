<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'login','as'=>'auth.', 'middleware' => 'sentinel.if_login'], function () {
    Route::get('/','AuthenticationController@index');
    Route::post('/process', 'AuthenticationController@process')->name('process');

    Route::get('/chooses', 'AuthenticationController@choose')->name('choose');
    Route::post('/set-role-user', 'AuthenticationController@setRoleUser')->name('setRoleUser');
});

Route::get('/logout','AuthenticationController@logout')->name('logout');
//sementara
Route::group(['prefix' => 'ldap','as'=>'ldap.'], function () {
    Route::get('/sample-list','LdapAuthController@index');
});
