<?php

namespace App\Modules\Admindashboard\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Auth\Models\EmployeeModel;
use App\Modules\Auth\Models\UserModel;
use App\Helpers\UserRolesHelper;
use App\Modules\Auth\Models\UserRolesModel;
use sentinel;
use App\User;
use App\Notifications\Trainee;
use Yajra\DataTables\Facades\DataTables;
use DB;

use App\Http\Controllers\Controller;

// guzzle execption
    use GuzzleHttp\Exception\GuzzleException;
    use GuzzleHttp\Client;
// guzzle

use Mail;
use App\Mail\TraineeMailNew;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $controller = new Controller();
        $this->client = new Client();
        $this->akses_api = $controller->akses_api();
    }

    public function index()
    {
        //

//        $data['employee'] = EmployeeModel::get()->toArray();
        
        $data['user'] = UserModel::get()->groupBy('username')->toArray();
        $data['roles'] = UserRolesModel::get()->toArray();

        return view('admindashboard::setting.employees',$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $user = new UserRolesHelper();
        $sentinel = sentinel::check();

        if (!$sentinel || !$user->inAdmin()) {
            return redirect('404');
        }

        $employeeModel = new EmployeeModel();

        $data['employee'] = $employeeModel->where('nik',$id)->get(['email','job_key_short','manager_id','nik','position','division','joining_date','retiring_date','employee_status','points']);

        if ($data['employee']->isEmpty()) {
            return redirect('404');
        } else {
            $data['employee'] = $data['employee']->first()->toArray();
        }
        $user_account = UserModel::where('username',$id)->get()->first();

        if($user_account) {
            $user_account = $user_account->toArray();
        } else {

            $allowed_manager_key = ['vp', 'svp', 'dir'];

            $register_user = Sentinel::registerAndActivate([
                'email'    => $data['employee']['email'],
                'password' => now()->timestamp,
                'username' => $id
            ]);

            //assign staff role to user
            if(!empty($data['employee']['manager_id'])){
                $staff_role = UserRolesModel::where('slug','staff')->first();
                $register_user->roles()->save($staff_role);
            }

            //assign head role to user
            if(in_array(strtolower($data['employee']['job_key_short']), $allowed_manager_key)){
                $staff_role = UserRolesModel::where('slug','atasan')->first();
                $register_user->roles()->save($staff_role);
            }
            $user_account = UserModel::where('username',$id)->get()->first()->toArray();
        }


        $data['employee']['isStaff'] = $user->inStaff($user_account['id']);
        $data['employee']['isManager'] = $user->inManager($user_account['id']);
        $data['employee']['isAdmin'] = $user->inAdmin($user_account['id']);

        return view('admindashboard::setting.employees-detail',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function setRole(Request $request) {
        $request = $request->toarray();

        $user = new UserModel();

        $user = $user->where('username',$request['employee_nik'])->get();

        if ($user->isNotEmpty()) {
            $user = sentinel::findById($user[0]->id);

            if (isset($request['is_staff']) && $request['is_staff'] == 1 ) {
                $this->attachIfNotIn('staff',$user);
            } else {
                $this->detachIfInRole('staff',$user);
            }

            if (isset($request['is_manager']) && $request['is_manager'] == 1 ) {
                $this->attachIfNotIn('atasan',$user);
            } else {
                $this->detachIfInRole('atasan',$user);
            }

            if (isset($request['is_admin']) && $request['is_admin'] == 1 ) {
                $this->attachIfNotIn('hr',$user);
            } else {
                $this->detachIfInRole('hr',$user);
            }
            return back()->withSuccess('Action completed');
        }
    }

    private function attachIfNotIn($role,$user) {
        $helper = new UserRolesHelper();

        $roles = sentinel::findRoleBySlug($role);

        if ($role == 'staff' && !$helper->inStaff($user->id)) {

            $roles->users()->attach($user);
        }

        if ($role == 'atasan' && !$helper->inManager($user->id)) {

            $roles->users()->attach($user);
        }

        if ($role == 'hr' && !$helper->inAdmin($user->id)) {

            $roles->users()->attach($user);
        }

        return true;

    }

    private function detachIfInRole($role,$user) {
        $helper = new UserRolesHelper();

        $roles = sentinel::findRoleBySlug($role);

        if ($role == 'staff' && $helper->inStaff($user->id)) {

            $roles->users()->detach($user);
        }

        if ($role == 'atasan' && $helper->inManager($user->id)) {

            $roles->users()->detach($user);
        }

        if ($role == 'hr' && $helper->inAdmin($user->id)) {

            $roles->users()->detach($user);
        }

        return true;
    }

    public function find(Request $request) {
        $request = $request->toArray();

        if($request[0][0] == 'name') {
            unset($request[0]);
        }

        $where = [];

        foreach ($request as $item) {
            array_push($where,$item['1']);
        }

        $data = EmployeeModel::whereIn('nik',[$where])->get();

        if ($data->isEmpty()) {
            return false;
        }

        $data = $data->toArray();

        if (count($data) != count($request)){
            return false;
        }

        return true;
    }

    public function getEmployee() {
        $employee = EmployeeModel::leftJoin('users', 'users.username', '=', 'employee.nik')
            ->get(['full_name','employee.nik','position','division', 'is_block', 'users.id']);

        $return =  Datatables::of($employee)
        ->addColumn('options',function ($user){

           if($user->nik){
            return '<button type="button" class="btn btn-default btn-sm" onclick="location.href=\''.route('dashboard.setting.employeeDetail',$user->nik).'\'">
                <i class="icon-edit-2"></i> Detail
            </button>';
           }
        })
        ->rawColumns(['options'])
        ->make();

        return $return;
    }

    public function loglist(){
        $employee = DB::select("SELECT description,subject_username,browser,internet_protocol,created_at from login_log order by id DESC");
        $return =  Datatables::of($employee)
        ->make();

        return $return;
    }

    public function userRequestList() {
        $employee = EmployeeModel::select('full_name','employee.nik','position','division', 'is_block', 'users.id')
            ->join('users', 'users.username', '=', 'employee.nik')
            ->whereIn('is_block', [0,1,2])
            ->orderBy('is_block', 'asc')
            ->get();

        $return =  Datatables::of($employee)
            ->addColumn('options',function ($user){
                return '<button type="button" class="btn '.(($user->is_block == 1) ? 'btn-warning' : 'btn-default').' btn-sm blocking-user" '.(($user->id == null || $user->is_block == 0) ? '' : '').' data-href="'.route('dashboard.setting.employeeBlocking',['nik' => $user->nik, 'status' => (int)$user->is_block]).'">
                    <i class="icon"></i>'.(($user->is_block == 0 || $user->is_block == 1) ? 'Unblock' : 'Block').'
                </button>';
            })
            ->rawColumns(['options'])
            ->make();

        return $return;
    }

    public function check(Request $request) {

        if (EmployeeModel::where(['nik'=>$request->get('nik')])->get(['nik'])->first()) {
            return 1;
        } else {
            return 0;
        }
    }

    public function blocking($nik, $status){
        $return = [
            'error' => true,
            'message' => 'This employee didn\'t have user data',
            'url' => '',
            'block_text' => ''
        ];
 
        $update_status = ($status == '1') ? 2 : 0;
        $update_data = UserModel::where('username', $nik)
                ->update(['is_block' => $update_status]);

        if($status == '1'){
            $user = User::where('username', $nik)->first();
            $messages = [
                'message_header' => 'Registration form unlocked',
                'message_date' => now()->timestamp,
                'url' => '/trainee/request',
                'type' => 'registration form unlocked',
                'subject' => 'Let’s Start Requesting Your Development Program',
                'message_body' => '<p>Your access request has been granted!</p>
                    <p>You can start requesting for joining coaching, mentoring, assignment, public training & being external speaker <a href="'.url('trainee/request').'">here</a></p>',
                'form_group' => '0',
                'to' => $user->email
            ];
    
            $user->notify(new Trainee($messages));

           
            // if(isset($user->email)){
            //     Mail::to($user->email)->send(new TraineeMailNew($messages));
            // }
            
            
            
        }
        
        if($update_data){
            $return['error'] = false;
            $return['block_text'] = ($status == '1') ? '<i class="icon"></i> Block' : '<i class="icon"></i> Unblock';
            $return['url'] = route('dashboard.setting.employeeBlocking',['nik' => $nik, 'status' => (int)(($status == '1') ? 0 : 1)]);
            $return['message'] = ($status == '1') ? 'User '.$nik.' has been successfully unblocked' : 'User '.$nik.' has been successfully blocked';
        }
        
        return json_encode($return);
    }




/*
    public function integrasi_odsys(){
        // buat api get employee
        $res = $this->client->get($this->akses_api['uri'].'/api/karyawan_mix', [
            'headers' => $this->akses_api['headers']
        ]);
        //$body = $res->getBody();
        //$data = json_decode($body, true);
        $client = new \GuzzleHttp\Client();

        $body = $res->getBody()->getContents();
        $data = json_decode($body, true);
        $hitungan = 1;
        echo "<h2><center>Mohon Tunggu, sedang melakukan Sync data Employee dengan ODSys...</center></h2>";
        foreach ($data['data'] as $dat) {
            //echo .'<br>';
            $nik_dat[] = $dat['nik'];
            $query = EmployeeModel::where('nik', '=', $dat['nik'])->get();
            $countemp = $query->count();
            
            $dat_nik = null;
            if (isset($dat['nik'])) {
                $dat_nik = $dat['nik'];
            }else{
                $dat_nik = '0';
            }

            if ($countemp == 0) {
                EmployeeModel::create([

                    'nik' => $dat_nik,
                    'full_name' => strtoupper($dat['full_name']),
                    'position' => $dat['position_name'],
                    'email' => $dat['email'],
                    'manager_id' => $dat['line_manager'],
                    'directorate_code' =>$dat['directorate_code'],
                    'directorate' => $dat['directorate'],
                    'chief_code' => $dat['chief_code'],
                    'chief' => $dat['chief'],
                    'group_code' => $dat['group_code'],
                    'group_name' => $dat['group'],
                    'division_code' => $dat['division_code'],
                    'division' => $dat['division'],
                    'job_key_text' => $dat['job_title'],
                    'gender' => strtoupper($dat['gender_text']),
                    //new 
                    'date_of_birth' => $dat['date_of_birth'],
                    'religion' => $dat['religious_denomination'],
                    'joining_date' => $dat['tanggal_gabung_indosat'],
                    'point' => '0',
                    //endnew
                ]);
            //echo "insert".$hitungan."<br>";    
            }else{
                $employ = EmployeeModel::where('nik', '=' ,$dat['nik'])
                ->update([
                    
                    'full_name' => strtoupper($dat['full_name']),
                    'position' => $dat['position_name'],
                    'email' => $dat['email'],
                    'manager_id' => $dat['line_manager'],
                    'directorate_code' =>$dat['directorate_code'],
                    'directorate' => $dat['directorate'],
                    'chief_code' => $dat['chief_code'],
                    'chief' => $dat['chief'],
                    'group_code' => $dat['group_code'],
                    'group_name' => $dat['group'],
                    'division_code' => $dat['division_code'],
                    'division' => $dat['division'],
                    'job_key_text' => $dat['job_title'],
                    //'gender' => strtoupper($dat['gender_text']),
                    //new 
                    'date_of_birth' => $dat['date_of_birth'],
                    'religion' => $dat['religious_denomination'],
                    'joining_date' => $dat['tanggal_gabung_indosat'],
                    //endnew
                ]);
            //echo "update".$hitungan."<br>";
            }
            $hitungan++;
        }
        date_default_timezone_set('Asia/Jakarta');
        $date = date('Y-m-d h:i:s');
        $up_emp = EmployeeModel::whereNotIn('nik',$nik_dat)->update(['deleted_at' => $date]);
        $user_id_get = UserModel::whereNotIn('username',$nik_dat)->where('username','!=','Administrator')->get();
        
        $co_user_id = count($user_id_get);
        
        if ($co_user_id != 0) {
            $id_us = array();
            foreach ($user_id_get as $us) {
                $id_us[] = $us->id;
            }
            $up_activation = DB::table('activations')->whereIn('user_id',$id_us)->update(['completed' => '0']);
        }
        //dd($up_activation);

        return back()->withSuccess('Sync with ODSys Completed');
        //return dd($data['data'][0]);
        //return dd($data);
        //
    }
    */

     public function integrasi_odsys(){
        // buat api get employee
        $res = $this->client->get($this->akses_api['uri'].'/api/karyawan_mix', [
            'headers' => $this->akses_api['headers']
        ]);
        //$body = $res->getBody();
        //$data = json_decode($body, true);
        $client = new \GuzzleHttp\Client();

        $body = $res->getBody()->getContents();
        $data = json_decode($body, true);
       
        

       


        $hitungan = 1;
        echo "<h2><center>Mohon Tunggu, sedang melakukan Sync data Employee dengan ODSys...</center></h2>";
        foreach ($data['data'] as $dat) {
            //echo .'<br>';
            $nik_dat[] = $dat['nik'];
            $email_dat = $dat['email'];
            $query = EmployeeModel::where('nik', '=', $dat['nik'])->get();
            $countemp = $query->count();

            
            
            if ($countemp == 0 and $email_dat) {


            

                EmployeeModel::create([
                    /*
                    'nik' => $dat['nik'],
                    'full_name' => strtoupper($dat['full_name']),
                    //'date_of_birth' => $dat['date_of_birth'],
                    'position' => $dat['position_name'],
                    'email' => $dat['email'],
                    'manager_id' => $dat['line_manager'],
                    'directorate' => $dat['directorate'],
                    'chief' => $dat['chief'],
                    'group_name' => $dat['group'],
                    'division' => $dat['division'],
                    'job_key_text' => $dat['job_title'],
                    'gender' => strtoupper($dat['gender_text'])
                    */
                    'nik' => $dat['nik'],
                    'full_name' => strtoupper($dat['full_name']),
                    'position' => $dat['position_name'],
                    'email' => $dat['email'],
                    'manager_id' => $dat['line_manager'],
                    'directorate_code' =>$dat['directorate_code'],
                    'directorate' => $dat['directorate'],
                    'chief_code' => $dat['chief_code'],
                    'chief' => $dat['chief'],
                    'group_code' => $dat['group_code'],
                    'group_name' => $dat['group'],
                    'division_code' => $dat['division_code'],
                    'division' => $dat['division'],
                    'job_key_text' => $dat['job_title'],
                    'gender' => strtoupper($dat['gender_text']),


                    'date_of_birth' => $dat['date_of_birth'],
                    'joining_date' => $dat['tanggal_gabung_indosat'],



                ]);
            //echo "insert".$hitungan."<br>";  

                $query_user = UserModel::where('username', '=', $dat['nik'])->get();
                $count_user = $query_user->count();

                if($count_user==0){

                    $id = UserModel::insertGetId(array(
                       'email' => $dat['email'],
                       'password'=>bcrypt('indosat2020'),
                       'first_name'=>strtoupper($dat['full_name']),
                       'nik'=>$dat['nik'],
                       'username'=>$dat['nik'],
                       'point'=>0,
                       'is_block'=>2,
                       'training_request'=>0,
                    ));


                    DB::table('role_users')->insert([
                        'user_id' => $id,
                        'role_id' => 5
                    ]);   


                     $str=rand(); 
                     $result = md5($str); 
                     DB::table('activations')->insert([

                        'user_id' => $id,
                        'code' => $result,
                        'completed' => 1

                    ]);   
   

                }


             
            



            }else{

                
              if($email_dat){
            
                $employ = EmployeeModel::where('nik', '=' ,$dat['nik'])
                ->update([
                    /*
                    'position' => $dat['position_name'],
                    'division' => $dat['division'],
                    'manager_id' => $dat['line_manager']
                    */
                    'full_name' => strtoupper($dat['full_name']),
                    'position' => $dat['position_name'],
                    'email' => $dat['email'],
                    'manager_id' => $dat['line_manager'],
                    'directorate_code' =>$dat['directorate_code'],
                    'directorate' => $dat['directorate'],
                    'chief_code' => $dat['chief_code'],
                    'chief' => $dat['chief'],
                    'group_code' => $dat['group_code'],
                    'group_name' => $dat['group'],
                    'division_code' => $dat['division_code'],
                    'division' => $dat['division'],
                    'job_key_text' => $dat['job_title'],


                     'date_of_birth' => $dat['date_of_birth'],
                    'joining_date' => $dat['tanggal_gabung_indosat'],
                    //'gender' => strtoupper($dat['gender_text']),
                ]);

            }


            //echo "update".$hitungan."<br>";
            }
            $hitungan++;
        }

         
        date_default_timezone_set('Asia/Jakarta');
        $date = date('Y-m-d h:i:s');


        $up_emp = EmployeeModel::whereNotIn('nik',$nik_dat)->get();

        $up_tos = EmployeeModel::get();

       


        if(count($up_tos) > 0){

            foreach($up_tos as $key){

                 if (in_array($key->nik, $nik_dat)==false) { 

                      $res=EmployeeModel::where('nik',$key->nik)->delete();


                 }

            }

        }

        

        $user_id_get = UserModel::where('username','!=','Administrator')->get();
        
        $co_user_id = count($user_id_get);
        
        if ($co_user_id != 0) {
            $id_us = array();
            foreach ($user_id_get as $us) {

                if (in_array($us->username, $nik_dat)==false) { 
                     $id_us[] = $us->id;
                }
            }
            $up_activation = DB::table('activations')->whereIn('user_id',$id_us)->update(['completed' => '0']);
        }



        //dd($up_activation);

        return back()->withSuccess('Sync with ODSys Completed');
        //return dd($data['data'][0]);
        //return dd($data);
        //
    }

}
