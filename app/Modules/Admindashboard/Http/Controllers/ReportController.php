<?php

namespace App\Modules\Admindashboard\Http\Controllers;

use App\Helpers\UserRolesHelper;
use App\Modules\Trainee\Models\RequestsUserModel;
use App\User;
use Sentinel;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Trainee\Models\requestsGroupModel;
use Carbon\Carbon;

use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Excel;
use App\Exports\DefaultExport;


class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $role;

    function __construct()
    {
        $this->role = new UserRolesHelper();
    }

    public function index()
    {
        //
        $all_month = [];
        for ($m=1; $m<=12; $m++) {
            $month = date('M', mktime(0,0,0,$m, 1, date('Y')));
            $all_month[] = $month;
        }

        if($this->role->inManager()) {
            //jika dalam role manager ambil data dari bawahannya saja
            $staffs = $this->role->getLeaders(Sentinel::check()->id);
            $staff_nik = [];
            foreach ($staffs as $staff){
                if($staff && is_array($staff)){
                    foreach ($staff as $item){
                        $staff_nik[] = $item['nik'];
                    }
                }
            }
        } else {
            $users_staff = User::get()
                ->toArray();

            if(isset($users_staff['user_type'])) {

                if($users_staff['user_type'] == 'dir') {
                    $allowerd_user_staff['svp'] = $users_staff['svp'];
                } elseif($users_staff['user_type'] == 'svp') {
                    $allowerd_user_staff['vp'] = $users_staff['vp'];
                } elseif($users_staff['user_type'] == 'vp') {
                    unset($users_staff['svp']);
                    unset($users_staff['dir']);
                    $allowerd_user_staff = $users_staff;
                }
            } else {
                $allowerd_user_staff = $users_staff;
            }

            $staff_ids = [];
            foreach ($allowerd_user_staff as $staffs){

                if (is_array($staffs) && !$this->role->inAdmin()){
                    foreach ($staffs as $staff) {

                        array_push($staff_ids,$staff['user_id']);
                    }
                } if(is_array($staffs) && $this->role->inAdmin()){

                    array_push($staff_ids,$staffs['id']);

                }
            }
            $staff_ids = array_filter($staff_ids);
            $staff_nik = User::whereIn('id',$staff_ids)
                ->get(['username'])->toArray();
        }


//        if($this->role->inManager()){
            //jika dalam role manager ambil data dari bawahannya saja
            $requests['completed'] = requestsGroupModel::
                where(['is_completed'=>1])
                ->where(['is_completed_by_staff'=>1])
                ->join('users as u',function ($join) use($staff_nik) {
                    $join->on('u.id','=','requests_group.user_id')
                        ->whereIn('username',$staff_nik);
                })->count();
            $requests['requests'] = requestsGroupModel::
                join('users as u',function ($join) use($staff_nik) {
                    $join->on('u.id','=','requests_group.user_id')
                        ->whereIn('username',$staff_nik);
                })->count();

            $requests['not_completed'] = requestsGroupModel::
                where(['is_completed'=>0])
                ->where(['is_completed_by_staff'=>1])
                ->join('users as u',function ($join) use($staff_nik) {
                    $join->on('u.id','=','requests_group.user_id')
                        ->whereIn('username',$staff_nik);
                })->count();

            $requests['completed_request_by_month'] = requestsGroupModel::where(['is_completed'=>1])
                ->where(['is_completed_by_staff'=>1])
                ->join('users as u',function ($join) use($staff_nik) {
                    $join->on('u.id','=','requests_group.user_id')
                        ->whereIn('username',$staff_nik);
                })
                ->get(['form_group_id','requests_group.created_at'])
                ->groupBy(function($d) {
                    return [Carbon::parse($d->created_at)->format('M')];
                })->toArray();

            $request_by_month = requestsGroupModel::
                join('users as u',function ($join) use($staff_nik) {
                    $join->on('u.id','=','requests_group.user_id')
                        ->whereIn('username',$staff_nik);
                })
                ->get([
                    'form_group_id',
                    'requests_group.created_at',
                    DB::raw('CASE 
                    WHEN requests_group.form_group_id = 5 THEN "Inhouse" 
                    WHEN requests_group.form_group_id = 1 THEN "Assignment" 
                    WHEN requests_group.form_group_id = 2 THEN "Mentoring / Coaching" 
                    WHEN requests_group.form_group_id = 3 THEN "Public Training" 
                    WHEN requests_group.form_group_id = 4 THEN "External Speaker" 
                    ELSE "" 
                    END as form_group_alias')])
                ->groupBy(function($d) {
                    return [Carbon::parse($d->created_at)->format('M')];
                });
            $request_by_form_type = [];

            $request_by_form_type = $request_by_month->map(function ($value,$key) use ($request_by_form_type) {
               return $request_by_form_type[$key] = $value->groupBy('form_group_alias')->toArray();
            })->all();
            $requests['request_by_month'] = $request_by_month->toArray();
            $requests['request_by_form_type'] = $request_by_form_type;

            $completed_request_by_month = [];
            $request_by_month = [];
            $request_by_form_type = [];
            foreach ($all_month as $month){
                if(!isset($requests['completed_request_by_month'][$month])) {
                    $completed_request_by_month[$month] = null;
                } else {
                    $completed_request_by_month[$month] = $requests['completed_request_by_month'][$month];
                }
                if(!isset($requests['request_by_month'][$month])) {
                    $request_by_month[$month] = null;
                } else {
                    $request_by_month[$month] = $requests['request_by_month'][$month];
                }

                if(!isset($requests['request_by_form_type'][$month])) {
                    $request_by_form_type[$month] = null;
                } else {
                    $request_by_form_type[$month] = $requests['request_by_form_type'][$month];
                }
            }

            $requests['completed_request_by_month'] = $completed_request_by_month;
            $requests['request_by_month'] = $request_by_month;
            $requests['request_by_form_type'] = $request_by_form_type;

            $data['requests'] = $requests;
//        }


        if($this->role->inManager()){
            //jika dalam role manager ambil data dari bawahannya saja
            $data['users']['Mentor'] = User::whereIn('username',$staff_nik)
                ->join('requests_user as ru',function ($join){
                    $join->on('ru.user_id','=','users.id')
                        ->where(['user_position'=> 'Mentor']);
                })
                ->count();
            $data['users']['Employee'] = User::whereIn('username',$staff_nik)
                ->join('requests_user as ru',function ($join){
                    $join->on('ru.user_id','=','users.id')
                        ->where(['user_position'=> 'employee']);
                })
                ->count();
        } else {

            $data['users']['Mentor'] = RequestsUserModel::where(['user_position'=>'mentor'])->count();
            $data['users']['Employee'] = RequestsUserModel::where(['user_position'=>'employee'])->count();
        }


        if($this->role->inManager()){
            //jika dalam role manager ambil data dari bawahannya saja
            $data['points']['sum'] = User::
                whereIn('username',$staff_nik)
                ->get(['point'])->sum('point');
            $pointMax = User::
                whereIn('username',$staff_nik)
                ->get(['point'])->max('point');
        } else {
            $data['points']['sum'] = User::get(['point'])->sum('point');
            $pointMax = User::get(['point'])->max('point');
        }


        $data['points']['max'] = $pointMax;

        $data['users'] = $data['users'];

        $data['sum']['Inhouse'] = requestsGroupModel::where(['form_group_id'=>'5'])->count();
        $data['sum']['Public Training'] = requestsGroupModel::where(['form_group_id'=>'3'])->count();
        $data['sum']['Assignment'] = requestsGroupModel::where(['form_group_id'=>'1'])->count();
        $data['sum']['Mentoring'] = requestsGroupModel::where(['form_group_id'=>'2'])->count();
        $data['sum']['External'] = requestsGroupModel::where(['form_group_id'=>'4'])->count();

        return view('admindashboard::report.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getEmployeeData($id=null) {

        if($this->role->inManager()) {
            //jika dalam role manager ambil data dari bawahannya saja
            $staffs = $this->role->getLeaders(Sentinel::check()->id);
            $staff_nik = [];
            foreach ($staffs as $staff){
                if($staff && is_array($staff)){
                    foreach ($staff as $item){
                        $staff_nik[] = $item['nik'];
                    }
                }
            }
        }

        if($this->role->inManager()){

            $data = requestsGroupModel::
            leftJoin('users as u',function ($join) use($staff_nik){
                $join
                    ->on('requests_group.user_id','=','u.id')
                    ->whereIn('username',$staff_nik);
            })->where(['is_completed'=>1])
                ->where(['is_completed_by_staff'=>1])
                ->whereNotIn('u.id',[5]);

            if(!empty(request()->get('month'))){
                $data->whereMonth('requests_group.created_at', request()->get('month'));
            }

            if(!empty(request()->get('year'))){
                $data->whereYear('requests_group.created_at', 'like', '%'.request()->get('year').'%');  
            }

            if(!empty(request()->get('name'))){
                $data->where('u.first_name', 'like', '%'.request()->get('name').'%');
            }

            $data = $data->get([
                'u.first_name as first_name',
                'requests_group.form_group_id',
                'u.id',
                'u.point',
                'u.username'
            ])
            ->groupBy('id');
        } else {

            $data = requestsGroupModel::
            leftJoin('users as u',function ($join){
                $join->on('requests_group.user_id','=','u.id');
            })->whereNotIn('u.id',[5])
            ->where(['is_completed'=>1])
            ->where(['is_completed_by_staff'=>1]);

            if(!empty(request()->get('month'))){
                $data->whereMonth('requests_group.created_at', request()->get('month'));
            }

            if(!empty(request()->get('year'))){
                $data->whereYear('requests_group.created_at', 'like', '%'.request()->get('year').'%');  
            }

            if(!empty(request()->get('name'))){
                $data->where('u.first_name', 'like', '%'.request()->get('name').'%');
            }

            $data = $data->get([
                'u.first_name as first_name',
                'requests_group.form_group_id',
                'u.id',
                'u.point',
                'u.username'
            ])
            ->groupBy('id');
        }

        $employees = [];

        foreach($data->toArray() as $key=>$items) {
            $assignment = $mentoring = $external = $training = 0;
            foreach ($items as $item){
                switch ($item['form_group_id']){
                    case 1:
                        $assignment++;
                        break;
                    case 2:
                        $mentoring++;
                        break;
                    case 3:
                        $training++;
                        break;
                    case 4:
                        $external++;
                        break;
                }
            }
            $employees[$key]['assignment'] = $assignment;
            $employees[$key]['mentoring'] = $mentoring;
            $employees[$key]['training'] = $training;
            $employees[$key]['external'] = $external;
            $employees[$key]['inhouse'] = RequestsUserModel::where(['requests_user.user_id'=>$key])
                ->join('requests_group as rg',function ($join){
                    $join->on('rg.id','=','requests_user.requests_group_id')
                        ->where(['rg.form_group_id'=>5]);
                })
                ->count();
            $employees[$key]['id'] = $key;
            $employees[$key]['username'] = $item['username'];
            $employees[$key]['point'] = $item['point'];
            $employees[$key]['first_name'] = $item['first_name'] ? $item['first_name'] : ' ';
        }
        
        $return = DataTables::of($employees)
                ->addColumn('options',function ($user){
                    return '
                    <div class="table-button">
                        <a href="'.route('trainee.report.byId',$user['username']).'">
                            <button type="button" class="btn btn-default btn-sm">
                                <i class="icon-edit-2"></i> Overview
                            </button>
                        </a>
                    </div>';
                })
                ->rawColumns(['options'])
                ->make();
                
        return $return;
    }

    private function getUserReport($id = null) {
        $role = new UserRolesHelper();

        if(!$role->inAdmin()) {
            return route('404');
        }

        $employee_model = new RequestsUserModel();

        if($id) {
            $query = $employee_model->where(['user_id',$id]);
        } else {
            $query = $employee_model;
        }

        $employee_data = $query->join('requests_user as ru2',function ($join) {
                $join->on('requests_user.requests_group_id','=','ru2.requests_group_id');
            })
            ->join('requests_group',function ($join) {
                $join
                    ->on('requests_group.id','=','ru2.requests_group_id');
            })
            ->join('users',function($join) {
                $join->on('users.id','=','ru2.user_id');
            })
            ->join('employee',function ($join){
                $join->on('employee.nik','=','users.username');
            })
            ->join('requests_group as rg',function ($join){
                $join
                    ->on('requests_group.id','=','ru2.requests_group_id');
            })
            ->limit(55)
            ->get([
                'rg.id','rg.user_id',
                'ru2.user_position',
                'employee.full_name',
                DB::raw('CASE WHEN requests_group.form_group_id = 5 THEN "Inhouse" ELSE "Public Training" END as form_group_id'),
                'users.point'
            ])
            ->groupBy('user_position');

        return $employee_data;
    }
    public function user($id){

    }

    public function exportAllUsers(){
        if($this->role->inManager()) {
            //jika dalam role manager ambil data dari bawahannya saja
            $staffs = $this->role->getLeaders(Sentinel::check()->id);
            $staff_nik = [];
            foreach ($staffs as $staff){
                if($staff && is_array($staff)){
                    foreach ($staff as $item){
                        $staff_nik[] = $item['nik'];
                    }
                }
            }
        }

        if($this->role->inManager()){
            $data = requestsGroupModel::
            leftJoin('users as u',function ($join) use($staff_nik){
                $join
                    ->on('requests_group.user_id','=','u.id')
                    ->whereIn('username',$staff_nik);
            })->join('employee as e', 'e.nik', '=','u.username')
                ->whereNotIn('u.id',[5]);

            if(!empty(request()->get('month'))){
                $data->whereMonth('requests_group.created_at', request()->get('month'));
            }

            if(!empty(request()->get('year'))){
                $data->whereYear('requests_group.created_at', 'like', '%'.request()->get('year').'%');  
            }

            if(!empty(request()->get('name'))){
                $data->where('u.first_name', 'like', '%'.request()->get('name').'%');
            }
            
            $data = $data->get([
                'e.nik',
                'u.first_name as first_name',
                'e.position',
                'e.division',
                'e.employee_status',
                'requests_group.form_group_id',
                'u.id',
                'u.username',
                'e.position',
                'u.point'
            ])
            ->groupBy('id');
        } else {
            $data = requestsGroupModel::
            leftJoin('users as u',function ($join){
                $join->on('requests_group.user_id','=','u.id');
            })->join('employee as e', 'e.nik', '=','u.username')
                ->whereNotIn('u.id',[5]);

            if(!empty(request()->get('month'))){
                $data->whereMonth('requests_group.created_at', request()->get('month'));
            }

            if(!empty(request()->get('year'))){
                $data->whereYear('requests_group.created_at', 'like', '%'.request()->get('year').'%');  
            }

            if(!empty(request()->get('name'))){
                $data->where('u.first_name', 'like', '%'.request()->get('name').'%');
            }
            
            $data = $data ->get([
                'e.nik',
                'u.first_name as first_name',
                'e.position',
                'e.division',
                'e.employee_status',
                'requests_group.form_group_id',
                'u.id',
                'u.username',
                'e.position',
                'u.point'
            ])
            ->groupBy('id');
        }

        $employees = [];

        foreach($data->toArray() as $key=>$items) {
            $assignment = $mentoring = $external = $training = 0;
            foreach ($items as $item){
                switch ($item['form_group_id']){
                    case 1:
                        $assignment++;
                        break;
                    case 2:
                        $mentoring++;
                        break;
                    case 3:
                        $training++;
                        break;
                    case 4:
                        $external++;
                        break;
                }
            }

            $employees[$key] = $item;
            $employees[$key]['inhouse'] = RequestsUserModel::where(['requests_user.user_id'=>$key])
                ->join('requests_group as rg',function ($join){
                    $join->on('rg.id','=','requests_user.requests_group_id')
                        ->where(['rg.form_group_id'=>5]);
                })
                ->count();
            $employees[$key]['training'] = $training;
            $employees[$key]['assignment'] = $assignment;
            $employees[$key]['mentoring'] = $mentoring;
            $employees[$key]['external'] = $external;

            unset($employees[$key]['point']);
            $employees[$key]['point'] = $item['point'];
        }

        $employees = array_values($employees);

        $excel_data[] = [
            'Nik' => '',
            'First Name' => '',
            'Position' => '',
            'Division' => '',
            'Employee Status' => '',
            'Username' => '',
            'Inhouse' => '',
            'Public Training' => '',
            'Assignment' => '',
            'Mentoring & Coaching' => '',
            'External Speaker' => '',
            'Total Point' => '',
        ];

        foreach($employees as $key => $employee){
            foreach($employee as $field => $value){
                if(!in_array($field, ['id', 'form_group_id'])){
                    switch($field){
                        case 'training' :
                            $field = 'public_training';
                        break;
                        case 'mentoring' :
                            $field = 'mentoring_&_coaching';
                        break;
                        case 'external' :
                            $field = 'external_speaker';
                        break;
                        case 'point' :
                            $field = 'total_point';
                        break;
                    }

                    $field_name = ucwords(str_replace('_', ' ', $field));
                    $excel_data[$key][$field_name] = $value;
                }
            }
        }

        return Excel::download(new DefaultExport($excel_data), 'user-report.xlsx');
    }
}
