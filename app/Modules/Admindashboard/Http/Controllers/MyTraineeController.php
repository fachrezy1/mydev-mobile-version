<?php

namespace App\Modules\Admindashboard\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Trainee\Models\requestsModel;

use App\Http\Controllers\Controller;

use Sentinel;

class MyTraineeController extends Controller
{
    //
    function view() {
        $user = Sentinel::check();
        if (!$user->hasAccess('trainee.manage')) {
            if (!$user->hasAccess('admin.all')) {
                return back();
            }
        }

        $model = new requestsModel();

        $data['requests'] = $model->getGroupedRequest($user->id);

        return view('admindashboard::traineeList',$data);
    }
}
