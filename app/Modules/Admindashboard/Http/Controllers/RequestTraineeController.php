<?php

namespace App\Modules\Admindashboard\Http\Controllers;

use Notification;

use App\Http\Controllers\Controller;
use App\Modules\Trainee\Models\requestsModel;
use App\Modules\Trainee\Models\RequestsUserModel;
use App\Modules\Trainee\Models\requestsGroupModel;
use App\Modules\Auth\Models\EmployeeModel;
use App\Modules\Auth\Models\Usermodel;
use App\Helpers\UserRolesHelper;
use App\User;
use Excel;
use App\Exports\DefaultExport;
use DataTables;

use Sentinel;

class RequestTraineeController extends Controller
{
    //

    public function view() {
        $role = new UserRolesHelper();

        if ($role->inStaff()){
            return redirect('404');
        }

        $model = new requestsModel();

        $data['requests'] = [];
        $data['requests'] = null;
        $data['user_id'] = Sentinel::check()->id;

        if (!$role->inAdmin()) {
            $users_staff = $role->getLeaders($data['user_id']);

            if($users_staff) {
                $staff_ids = [];
                foreach($users_staff as $idx_staff => $div){
                    if($idx_staff != 'user_type'){
                        if(!empty($div)){
                            foreach($div as $staff){
                                array_push($staff_ids,$staff['user_id']);
                            }
                        }
                    }
                }
    
                $staff_ids = array_filter($staff_ids);   
            }
        } else {
            $staff_ids = User::pluck('id')
                ->toArray();
        }

        if(!empty($staff_ids)){
            if(!$role->inAdmin()){
                $data['requests'] = $model->getNotificationRequest($staff_ids,$data['user_id'],$staff_ids[key($staff_ids)]);
            }
        }
        if(!$role->inAdmin()){
            if(!empty($data['requests'])){
                $data['requests'] = $model->groupRequest($data['requests']);
                $data['requests'] = array_filter($data['requests']);
            }
        }
        $data['form_type'] = requestsModel::groupById;
        
        if($role->inAdmin()){
            $data['full_name'] = 'Administrator';
        }else{
            $data['full_name'] = EmployeeModel::where(['nik'=>Sentinel::check()->username])->get(['full_name'])->first();
            $line_manager = $data['full_name']['full_name'];
            $data['line_manager'] = ucwords(strtolower($line_manager));;
        }

        return view('admindashboard::requestTrainee',$data);
    }

    private function getTraineRequest($type, $forData=null){
        $role = new UserRolesHelper();
        $staff_ids = $this->getStaffIds();

        $trainee_request = requestsGroupModel::whereIn("form_group_id",[1,2,3,4,6])->whereIn('user_id',$staff_ids);
        
        if (request("search.value") && strlen(request("search.value"))>0){
            $trainee_request = $trainee_request->where(function($q) {
                $q->whereHas('user', function ($query) {
                    $query->where("first_name","LIKE",'%'.request("search.value").'%');
                })->orWhereHas('trainee', function ($query) {
                    $query->where("name","LIKE",'%'.request("search.value").'%');
                })->orWhereHas('request_data', function ($query) {
                    $query->where("request_content","LIKE",'%'.request("search.value").'%');
                });
            });
        }

        if (request("year") && strlen(request("year"))>0 && request("month") && strlen(request("month"))>0){
            $year = explode('-',request('year'));
            $month = explode('-',request('month'));

            if (count($year)==2 && count($month)==2){

                $date_filter = $month[1].'/'.$year[1];
                $trainee_request = $trainee_request->where(function($q) use ($date_filter) {
                        $q->whereHas('request_data', function ($query) use ($date_filter) {
                            $query->where("request_content","LIKE",'%'.$date_filter.'%');
                        });
                    });
            }
        }

        switch($type){
            case 'waiting' :
                $trainee_request = $trainee_request->where([
                'is_completed'=>0, 
                'is_rejected' => 0
            ])
            ->whereNotNull('published_at')
            ->where(function($query) {
                    $query->whereNull('first_accepter')
                        ->orWhereNull('second_accepter');
            })
            ->where('is_accepted', 0);
            break;
            case 'ongoing' :
                $trainee_request = $trainee_request->where('is_completed_by_staff', 0)
            ->where('is_completed', 0)
            ->whereNotNull('first_accepter')
            ->where('first_accepter', '!=', 0)
            ->whereNotNull('second_accepter')
            ->where('second_accepter', '!=', 0);
            break;
            case 'completed' :
                $trainee_request = $trainee_request->where('is_completed','!=',0)
                ->where(['is_completed_by_staff'=>1])
                ->whereNotNull('first_accepter')
                ->where('first_accepter', '!=', 0)
                ->whereNotNull('second_accepter')
                ->where('second_accepter', '!=', 0);
            break;
            case 'rejected' :
                $trainee_request = $trainee_request->where(['is_rejected'=>1])
                ->where('is_rejected', 1)
                ->where('rejected_by',"!=", 0);
            break;
        }

        $trainee_request = $trainee_request->orderBy("id","desc")->with(["request_data","trainee","user"]);
        if ($forData=="excel"){
            return $trainee_request->get();
        }
        return $trainee_request;
    }

    private function getTraineRequest2($type, $forData=null){
        $role = new UserRolesHelper();
        $staff_ids = $this->getStaffIds();

        $trainee_request = requestsGroupModel::whereIn("form_group_id",[1,2,3,4,6])->whereIn('user_id',$staff_ids);
        
        if (request("search.value") && strlen(request("search.value"))>0){
            $trainee_request = $trainee_request->where(function($q) {
                $q->whereHas('user', function ($query) {
                    $query->where("first_name","LIKE",'%'.request("search.value").'%');
                })->orWhereHas('trainee', function ($query) {
                    $query->where("name","LIKE",'%'.request("search.value").'%');
                })->orWhereHas('request_data', function ($query) {
                    $query->where("request_content","LIKE",'%'.request("search.value").'%');
                });
            });
        }

        if (request("year") && strlen(request("year"))>0 && request("month") && strlen(request("month"))>0){
            $year = explode('-',request('year'));
            $month = explode('-',request('month'));

            if (count($year)==2 && count($month)==2){

                $date_filter = $month[1].'/'.$year[1];
                $trainee_request = $trainee_request->where(function($q) use ($date_filter) {
                        $q->whereHas('request_data', function ($query) use ($date_filter) {
                            $query->where("request_content","LIKE",'%'.$date_filter.'%');
                        });
                    });
            }
        }

        
        switch($type){
            case 'waiting' :
                $trainee_request = $trainee_request->where([
                'is_completed'=>0, 
                'is_rejected' => 0
            ])
            ->whereNotNull('published_at')
            ->where(function($query) {
                    $query->whereNull('first_accepter')
                        ->orWhereNull('second_accepter');
            })
            ->where('is_accepted', 0);
            break;
            case 'ongoing' :
                $trainee_request = $trainee_request->where('is_completed_by_staff', 0)
            ->where('is_completed', 0)
            ->whereNotNull('first_accepter')
            //->where('first_accepter', '!=', 0)
            ->whereNotNull('second_accepter')
            ->where('second_accepter', '!=', 0);
            break;
            case 'completed' :
                $trainee_request = $trainee_request->where('is_completed','!=',0)
                ->where(['is_completed_by_staff'=>1])
                ->whereNotNull('first_accepter')
                //->where('first_accepter', '!=', 0)
                ->whereNotNull('second_accepter');
                //->where('second_accepter', '!=', 0);
            break;
            case 'rejected' :
                $trainee_request = $trainee_request->where(['is_rejected'=>1])
                ->where('is_rejected', 1)
                ->where('rejected_by',"!=", 0);
            break;
        }

        $trainee_request = $trainee_request->orderBy("id","desc")->with(["request_data","trainee","user"]);
        if ($forData=="excel"){
            return $trainee_request->get();
        }
        return $trainee_request;
    }

    public function datatableTraineeRequest($type){
        $role = new UserRolesHelper();
        
        if ($role->inAdmin()) {
            $trainee_request = $this->getTraineRequest($type);
        }else{
            $trainee_request = $this->getTraineRequest2($type);
        }
        
        return Datatables::of($trainee_request)
            ->addIndexColumn()
            ->addColumn('title', function($trainee_request) {
                switch($trainee_request->form_group_id){
                    case 1:
                        $request_data = $trainee_request->request_data()->where('form_answer','assignment_pg_project_title')->first();

                        if($request_data){
                            $title = $request_data->request_content;
                        }else{
                            $title = '-';
                        }
                    break;

                    case 2:
                        $request_data = $trainee_request->request_data()->where('form_answer','coaching_cc_development_goal')->first();

                        if($request_data){
                            $title = $request_data->request_content;
                        }else{
                            $title = '-';
                        }
                    break;

                    case 3:
                        $request_data = $trainee_request->request_data()->where('form_answer','training_ti_title')->first();
                        if($request_data){
                            $title = $request_data->request_content;
                        }else{
                            $title = '-';
                        }
                    break;

                    case 4:
                        $request_data = $trainee_request->request_data()->where('form_answer','mentoring_ci_line_topic')->first();

                        if($request_data){
                            $title = $request_data->request_content;
                        }else{
                            $title = '-';
                        }
                    break;
                    
                    case 5:
                        $request_data = $trainee_request->request_data()->where('form_answer','training_ti_title')->first();

                        if($request_data){
                            $title = $request_data->request_content;
                        }else{
                            $title = '-';
                        }
                    break;

                    default :
                        $title = '-';
                    break;
                }

                return $title;
            })
            ->addColumn('type', function($trainee_request) {
                if (isset($trainee_request->trainee->name)){
                    $type = $trainee_request->trainee->name;
                } else {
                    $type = "-";
                }
                return str_replace("&", "/", $type);
            })
            ->addColumn('employee_name', function($trainee_request) {
                
                if (isset($trainee_request->user->first_name)){
                    $full_name = $trainee_request->user->first_name;
                } else {
                    $full_name = "-";
                }

                return $full_name;
            })

            ->addColumn('atasan', function($trainee_request){
               $role = new UserRolesHelper();

               //request_grup id
               $id_train2 =  $trainee_request->id;

               //user id dari request id
               $id_us =  $trainee_request->user->id; 

               $arr_line = $role->getLeaders((int)$id_us);
               
               $user_typ = NULL;
               $user_typ = $arr_line['user_type'];
               $atasan = '';
               
               //$managerid = '';
               //$email = '';
               $name_manager = '-';
               $email = '';
            
                //$email = $trainee_request->user->email;
                if (isset($trainee_request->user->email)) {
                    $email = $trainee_request->user->email;
                }else{
                   $email = '';   
                }
               
               if ($arr_line['user_type'] != NULL) {
                    if ( $user_typ == 'staff' || $user_typ == 'sroff' || $user_typ == 'off') {
                        //mengambil avp
                        if (array_key_exists('avp', $arr_line)) {
                            if (is_array($arr_line['avp']) && array_key_exists('name', $arr_line['avp'])) {
                                $atasan = $arr_line['avp']['name'];
                            }else{
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }else{
                            //mengambil vp
                            if (array_key_exists('vp', $arr_line)) {
                                if (is_array($arr_line['vp']) && array_key_exists('name', $arr_line['vp'])) {
                                    $atasan = $arr_line['vp']['name'];
                                }else{
                                    $atasan = '-';
                                    $manag = $role->manag_id($email);
                                    $atasan = $manag['manager'];
                                }
                            }else{
                                //mengambil svp
                                if (array_key_exists('svp', $arr_line)) {
                                    if (is_array($arr_line['svp']) && array_key_exists('name', $arr_line['svp'])) {
                                        $atasan = $arr_line['svp']['name'];
                                    }else{
                                        $atasan = '-';
                                        $manag = $role->manag_id($email);
                                        $atasan = $manag['manager'];
                                    }
                                }else{
                                    //end else
                                    $atasan = '-';
                                    $manag = $role->manag_id($email);
                                    $atasan = $manag['manager'];
                                }
                            }
                        }
                    }elseif ( $user_typ == 'avp') {
                        if (array_key_exists('vp', $arr_line)) {
                            if (is_array($arr_line['vp']) && array_key_exists('name', $arr_line['vp'])) {
                                $atasan = $arr_line['vp']['name'];
                            }else{
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }else{
                            if (array_key_exists('svp', $arr_line)) {
                                if (is_array($arr_line['svp']) && array_key_exists('name', $arr_line['svp'])) {
                                    $atasan = $arr_line['svp']['name'];
                                }else{
                                    $atasan = '-';
                                }
                            }else{
                                //end else
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }
                    }elseif ( $user_typ == 'vp') {
                        if (array_key_exists('svp', $arr_line)) {
                            if (is_array($arr_line['svp']) && array_key_exists('name', $arr_line['svp'])) {
                                $atasan = $arr_line['svp']['name'];
                            }else{
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }else{
                            if (array_key_exists('dir', $arr_line)) {
                                if ( is_array($arr_line['dir']) && array_key_exists('name', $arr_line['dir'])) {
                                    $atasan = $arr_line['dir']['name'];
                                }else{
                                    $atasan = '-';
                                    $manag = $role->manag_id($email);
                                    $atasan = $manag['manager'];
                                }
                            }else{
                                //end else
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }
                    }elseif ($user_typ == 'svp') {
                        //ambil dir
                        if (array_key_exists('dir', $arr_line)) {
                            if (is_array($arr_line['dir']) && array_key_exists('name', $arr_line['dir'])) {
                                $atasan = $arr_line['dir']['name'];
                            }else{
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }else{
                            //end else
                            $atasan = '-';
                            $manag = $role->manag_id($email);
                            $atasan = $manag['manager'];
                        }
                    }else{
                    //isi sesuai manager_id
                    $manag = $role->manag_id($email);
                    $atasan = $manag['manager'];
                    }
               }else{
                    //kosong di bagian user type
                    $atasan = '-';
               }

                if ($atasan == '' || $atasan == NULL) {
                   $atasan = 'Nama belum diinput di database';
                }
               
               return $atasan;

            })

            ->addColumn('start', function($trainee_request) {
                switch($trainee_request->form_group_id){
                    case 1:
                        $date = $trainee_request->request_data()->where('form_answer','assignment_pg_start_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 2:
                        $date = $trainee_request->request_data()->where('form_answer','coaching_ci_start_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 3:
                        $date = $trainee_request->request_data()->where('form_answer','training_ti_start_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 4:
                        $date = $trainee_request->request_data()->where('form_answer','mentoring_ci_start_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;
                    
                    case 5:
                        $date = $trainee_request->request_data()->where('form_answer','training_ti_start_date')->first();

                        if($date){
                            $date = $date->request_content;
                        }else{
                            $date = '-';
                        }
                    break;

                    default :
                        $date = '-';
                    break;
                }

                return $date;
            })
            ->addColumn('end', function($trainee_request) {
                switch($trainee_request->form_group_id){
                    case 1:
                        $date = $trainee_request->request_data()->where('form_answer','assignment_pg_end_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 2:
                        $date = $trainee_request->request_data()->where('form_answer','coaching_ci_end_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 3:
                        $date = $trainee_request->request_data()->where('form_answer','training_ti_end_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 4:
                        $date = $trainee_request->request_data()->where('form_answer','mentoring_ci_end_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;
                    
                    case 5:
                        $date = $trainee_request->request_data()->where('form_answer','training_ti_end_date');
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    default :
                        $date = '-';
                    break;
                }

                return $date;
            })
            ->addColumn('manager', function($trainee_request) {
                if ($trainee_request->is_rejected==1 && $trainee_request->rejected_by != 0){
                    $status = '<span class="badge badge-pill badge-default">Rejected</span>';
                } else {
                    $status = '<span class="badge badge-pill '.(!empty($trainee_request->first_accepter) ? 'badge-success' : 'badge-info').'">
                    '.(!empty($trainee_request->first_accepter) ? 'Approved' : 'Waiting Approval').'</span>';
                }
                return $status;
            })
            ->addColumn('hr', function($trainee_request) {
                if ($trainee_request->is_rejected==1 && $trainee_request->rejected_by != 0){
                    $status = '<span class="badge badge-pill badge-default">Rejected</span>';
                } else {
                    $status = '<span class="badge badge-pill '.(!empty($trainee_request->second_accepter) ? 'badge-success' : 'badge-info').'">
                    '.(!empty($trainee_request->second_accepter) ? 'Approved' : 'Waiting Approval').'</span>';
                }
                return $status;
            })
            ->addColumn('option', function($trainee_request) use ($role) {

                if ($role->inAdmin()){
                    $accepted = User::select("id")->where("id",$trainee_request->second_accepter)
                        ->orWhere("id",$trainee_request->rejected_by)->first();
                } else {
                    $accepted = User::select("id")
                    ->where("id",$trainee_request->first_accepter)
                    ->orWhere("id",$trainee_request->rejected_by)->first();
                }

                if ($trainee_request->is_accepted_by_me){
                    $accepted = true;
                }

                $disabled = "";
                if ($accepted){ 
                    $disabled = "disabled";
                }

                switch($trainee_request->form_group_id){
                    case 1:
                        $type = 'assignment';
                    break;

                    case 2:
                        $type = 'mentoring';
                    break;

                    case 3:
                        $type = 'training';
                    break;

                    case 4:
                        $type = 'external_speaker';
                    break;
                    
                    case 5:
                        $type = 'inhouse';
                    break;

                    default:
                        $type = '-';
                    break;
                }

                $option = '<td>';

                    if ($trainee_request->is_rejected){
                        $option .= $trainee_request->rejected_notes;
                    }else{
                        if ($trainee_request->is_accepted==1 || $trainee_request->is_completed==1){
                        } else {
                            if($accepted){
                                $option .= '<button type="button" class="btn btn-default btn-sm btn-lite disabled" title="you have been approved this request" disabled>
                                    Approved
                                </button>'; 
                            }else{
                                if (request('page')){
                                    $indexPagination = $request('page');
                                } else {
                                    $indexPagination = substr(request('start'),0,1) + 1;
                                }
                                $option .= '<form action="'.route('request.acceptRequest').'" method="post" class="mb-0">
                                    <input type="hidden" name="_token" value="'.csrf_token().'">
                                    <input type="hidden" name="user_id" value="'.$trainee_request->user_id.'">
                                    <input type="hidden" name="id" value="'.$trainee_request->id.'">';
                                
                                if ($role->inAdmin()){
                                    $option .='<input type="hidden" name="tab" value="myTrainee">
                                    <input type="hidden" value="'.$indexPagination.'" name="page" id="page'.$trainee_request->id.'">';
                                }else{
                                    // -
                                }
                                    
                                $option .='<button type="submit" class="btn btn-success btn-sm btn-lite" '.$disabled.'>
                                        <i class="icon-tick"></i>
                                        Approve
                                    </button>
                                </form>
                                <button type="submit" class="btn btn-danger btn-sm btn-lite reject_button_trainee" data-requestgroup="'.$trainee_request->id.'" data-userid="'.$trainee_request->user_id.'" data-toggle="modal" data-target="#rejectNotes" '.$disabled.' data-page="'.$indexPagination.'">
                                    <i class="icon-close"></i>
                                    Reject </button>';
                            }
                        }

                        $option .= '<a id="preview'.$trainee_request->id.'" href="'.route('trainee.previewByUserId',['user_id' => $trainee_request->user_id,'id'=>$trainee_request->id]).'">
                            <button type="submit" class="btn btn-dark btn-sm btn-lite ">
                                <i class="icon-view"></i>
                                Preview </button>
                        </a>';
                        
                        
                        if ($trainee_request->is_accepted==1 || $trainee_request->is_completed==1){
                        } else {
                            if($role->inAdmin()){
                                $option .= '<a href="'.route('dashboard.trainee.edit.show',['type'=> $type, 'user_id' => $trainee_request->user_id, 'id'=>$trainee_request->id]).'">
                                    <button type="button" class="btn btn-default btn-sm btn-lite">
                                        <i class="icon-edit-1"></i> Edit
                                    </button>
                                </a>';
                            }
                        }
                        $option .= '<a href="'.route('trainee.export',['user_id' => $trainee_request->user_id,'id'=>$trainee_request->id]).'" target="_blank">
                                <button type="button" class="btn btn-default btn-sm btn-lite">
                                    <i class="icon-transfer-data-between-documents"></i> Export
                                </button>
                            </a>';

                    }

                    
                
                $option .='</td>';

                return $option;
            })
            ->rawColumns(['option', 'manager', 'hr'])
            ->make(true);
    }

    private function getStaffIds(){
        $role = new UserRolesHelper();

        if (!$role->inAdmin()) {
            $users_staff = $role->getLeaders(Sentinel::check()->id);

            if($users_staff) {
                $staff_ids = [];
                foreach($users_staff as $idx_staff => $div){
                    if($idx_staff != 'user_type'){
                        if(!empty($div)){
                            foreach($div as $staff){
                                array_push($staff_ids,$staff['user_id']);
                            }
                        }
                    }
                }
    
                $staff_ids = array_filter($staff_ids);   
            }
        } else {
            $staff_ids = User::pluck('id')->toArray();
        }

        return $staff_ids;
    }

    public function traineeRequestExportTrainee(){
        
        $role = new UserRolesHelper();
        $export_type = request()->get('type');
        $excel_data[] = [
            'Title/Topic' => '',
            'Type' => '',
            'Type of Programs' => '',
            'Start Date' => '',
            'End Date' => '',
            'Employee NIK' => '',
            'Employee Name' => '',
            'Line Manager' => '',
            'Manager' => '',
            'HR' => '',
            'Mentor/Coach Nik' => '',
            'Mentor/Coach Name' => '',
        ];

        if($export_type == 'rejected'){
            $excel_data[0]['Notes'] = '';
        }

        if ($export_type=="new"){
            $export_type = "waiting";
        } elseif ($export_type=="approved") {
            $export_type = "ongoing";
        } elseif ($export_type=="completed"){
            $export_type = "completed";
        } elseif ($export_type=="rejected") {
            $export_type = "rejected";
        }

        $trainee_request = $this->getTraineRequest($export_type,"excel");

        if(isset($trainee_request)){
            foreach($trainee_request as $key => $request){
                $title = '';
                $description = '';
                $venue = '';
                $program_type = '';
                $type_ofp = '';
                $start_date = '';
                $end_date = '';
                $atasan = '';
                $employee_name = '';
                $employee_nik = '';
                $coaching_nik = '-';
                $coaching_name = '-';
                if (isset($request->user)){
                    $employee_name = $request->user->first_name;
                    //$employee_nik = (!empty($request->user->nik) ? $request->user->nik : $request->user->username);
                    $tmp = EmployeeModel::where(['email'=>$request->user->email])->get(['nik'])->first();
                    $employee_nik = (!empty($tmp->nik) ? $tmp->nik : (!empty($request->user->nik) ? $request->user->nik : $request->user->username));
                }

                if (isset($request)) {
                $role = new UserRolesHelper();
                //request_grup id
                $id_train2 =  $request->id;
                
                $user_typ = NULL;
                $id_us =  $request->user->id; 
                $arr_line = $role->getLeaders((int)$id_us);
                $user_typ = $arr_line['user_type'];
                
                $name_manager = '-';
                $email = '';
                //$email = $request->user->email;
                if (isset($request->user->email)) {
                    $email = $request->user->email;
                }else{
                   $email = '';   
                }
                if ($arr_line['user_type'] != NULL) {
                     if ( $user_typ == 'staff' || $user_typ == 'sroff' || $user_typ == 'off') {
                         //mengambil avp
                         if (array_key_exists('avp', $arr_line)) {
                             if (is_array($arr_line['avp']) && array_key_exists('name', $arr_line['avp'])) {
                                 $atasan = $arr_line['avp']['name'];
                             }else{
                                 $atasan = '-';
                                 $manag = $role->manag_id($email);
                                 $atasan = $manag['manager'];
                             }
                         }else{
                             //mengambil vp
                             if (array_key_exists('vp', $arr_line)) {
                                 if (is_array($arr_line['vp']) && array_key_exists('name', $arr_line['vp'])) {
                                     $atasan = $arr_line['vp']['name'];
                                 }else{
                                     $atasan = '-';
                                     $manag = $role->manag_id($email);
                                     $atasan = $manag['manager'];
                                 }
                             }else{
                                 //mengambil svp
                                 if (array_key_exists('svp', $arr_line)) {
                                     if (is_array($arr_line['svp']) && array_key_exists('name', $arr_line['svp'])) {
                                         $atasan = $arr_line['svp']['name'];
                                     }else{
                                         $atasan = '-';
                                         $manag = $role->manag_id($email);
                                         $atasan = $manag['manager'];
                                     }
                                 }else{
                                     //end else
                                     $atasan = '-';
                                     $manag = $role->manag_id($email);
                                     $atasan = $manag['manager'];
                                 }
                             }
                         }
                     }elseif ( $user_typ == 'avp') {
                         if (array_key_exists('vp', $arr_line)) {
                             if (is_array($arr_line['vp']) && array_key_exists('name', $arr_line['vp'])) {
                                 $atasan = $arr_line['vp']['name'];
                             }else{
                                 $atasan = '-';
                                 $manag = $role->manag_id($email);
                                 $atasan = $manag['manager'];
                             }
                         }else{
                             if (array_key_exists('svp', $arr_line)) {
                                 if (is_array($arr_line['svp']) && array_key_exists('name', $arr_line['svp'])) {
                                     $atasan = $arr_line['svp']['name'];
                                 }else{
                                     $atasan = '-';
                                 }
                             }else{
                                 //end else
                                 $atasan = '-';
                                 $manag = $role->manag_id($email);
                                 $atasan = $manag['manager'];
                             }
                         }
                     }elseif ( $user_typ == 'vp') {
                         if (array_key_exists('svp', $arr_line)) {
                             if (is_array($arr_line['svp']) && array_key_exists('name', $arr_line['svp'])) {
                                 $atasan = $arr_line['svp']['name'];
                             }else{
                                 $atasan = '-';
                                 $manag = $role->manag_id($email);
                                 $atasan = $manag['manager'];
                             }
                         }else{
                             if (array_key_exists('dir', $arr_line)) {
                                 if ( is_array($arr_line['dir']) && array_key_exists('name', $arr_line['dir'])) {
                                     $atasan = $arr_line['dir']['name'];
                                 }else{
                                     $atasan = '-';
                                     $manag = $role->manag_id($email);
                                     $atasan = $manag['manager'];
                                 }
                             }else{
                                 //end else
                                 $atasan = '-';
                                 $manag = $role->manag_id($email);
                                 $atasan = $manag['manager'];
                             }
                         }
                     }elseif ($user_typ == 'svp') {
                         //ambil dir
                         if (array_key_exists('dir', $arr_line)) {
                             if (is_array($arr_line['dir']) && array_key_exists('name', $arr_line['dir'])) {
                                 $atasan = $arr_line['dir']['name'];
                             }else{
                                 $atasan = '-';
                                 $manag = $role->manag_id($email);
                                 $atasan = $manag['manager'];
                             }
                         }else{
                             //end else
                             $atasan = '-';
                             $manag = $role->manag_id($email);
                             $atasan = $manag['manager'];
                         }
                     }else{
                     //isi sesuai manager_id
                     $manag = $role->manag_id($email);
                     $atasan = $manag['manager'];
                     }
                }else{
                     //kosong di bagian user type
                     $atasan = '-';
                }
                
                if ($atasan == '' || $atasan == NULL) {
                       $atasan = 'Nama belum diinput di database';
                    }
                }
        
                if (isset($request->request_data)){
                    switch($request->form_group_id){
                        case 1:
                            $request_data = $request->request_data->where('form_answer','assignment_pg_project_title')->first();
                            $type_prog = $request->request_data->where('form_answer','assignment_dev_type')->first();
                            if($request_data){
                                $title = $request_data->request_content;
                            }else{
                                $title = '-';
                            }
                            
                            if (isset($type_prog)) {
                                $type_ofp = $type_prog->request_content;
                                if ($type_ofp == 'internship') {
                                    $type_ofp = 'Cross Functional assignment';
                                }elseif($type_ofp == 'globaltalent'){
                                    $type_ofp = 'Global Talent Mobility / National Talent Mobility';
                                }else{
                                    $type_ofp = 'project';
                                }
                            }else{
                                $type_ofp = '';
                            }

                        $program_type = "Project Assignment";
                        break;

                        case 2:
                            $request_data = $request->request_data->where('form_answer','coaching_cc_development_goal')->first();
                            
                            $type_prog = $request->request_data->where('form_answer','coaching_ci_type')->first();

                            $tmp_name = $request->request_data->where('form_answer','coaching_ci_line_manager_name')->first();
                            
                            $tmp_nik = $request->request_data->where('form_answer','coaching_ci_line_manager_nik')->first();

                            if($request_data){
                                $title = $request_data->request_content;
                            }else{
                                $title = '-';
                            }

                            if($tmp_name){
                                $coaching_name = $tmp_name->request_content;
                            }

                            if($tmp_nik){
                                $coaching_nik = $tmp_nik->request_content;
                            }

                            if ($type_prog) {
                                $type_ofp = $type_prog->request_content;
                            }else{
                                $type_ofp = '-';
                            }


                        $program_type = "Mentoring / Coaching";
                        break;

                        case 3:
                            $request_data = $request->request_data->where('form_answer','training_ti_title')->first();

                            $type_prog = $request->request_data->where('form_answer','training_ti_type_programs')->first();

                            if($request_data){
                                $title = $request_data->request_content;
                            }else{
                                $title = '-';
                            }

                            if (isset($type_prog)) {
                                $type_ofp = $type_prog->request_content;
                                if ($type_ofp == 'seminar certification') {
                                    $type_ofp = 'Seminar/Conference';
                                }elseif($type_ofp == 'technical certification'){
                                    $type_ofp = 'Professional/Technical Certification';
                                }else{
                                    $type_ofp = 'General Training';
                                }
                            }else{
                                $type_ofp = '';
                            }

                        $program_type = "Public Training";
                        break;

                        case 4:
                            $request_data = $request->request_data->where('form_answer','mentoring_ci_line_topic')->first();

                            if($request_data){
                                $title = $request_data->request_content;
                            }else{
                                $title = '-';
                            }
                        $type_ofp = 'External Speaker';
                        $program_type = "External Speaker";
                        break;
                        
                        case 5:
                            $request_data = $request->request_data->where('form_answer','training_ti_title')->first();

                            $type_prog = $request->request_data->where('form_answer','training_ti_type_programs')->first();

                            if($request_data){
                                $title = $request_data->request_content;
                            }else{
                                $title = '-';
                            }
                            if ($type_prog) {
                                $type_ofp = $type_prog->request_content;
                            }else{
                                $type_ofp = '-';
                            }

                        $program_type = "Inhouse";
                        break;
                        default :
                            $title = '-';
                            $type_ofp = '-';
                        break;
                    }
                    switch($request->form_group_id){
                        case 1:
                            $date = $request->request_data->where('form_answer','assignment_pg_start_date')->first();
                            if ($date){
                                $date = $date->request_content;
                            } else {
                                $date = "-";
                            }
                        break;

                        case 2:
                            $date = $request->request_data->where('form_answer','coaching_ci_start_date')->first();
                            if ($date){
                                $date = $date->request_content;
                            } else {
                                $date = "-";
                            }
                        break;

                        case 3:
                            $date = $request->request_data->where('form_answer','training_ti_start_date')->first();
                            if ($date){
                                $date = $date->request_content;
                            } else {
                                $date = "-";
                            }
                        break;

                        case 4:
                            $date = $request->request_data->where('form_answer','mentoring_ci_start_date')->first();
                            if ($date){
                                $date = $date->request_content;
                            } else {
                                $date = "-";
                            }
                        break;
                        
                        case 5:
                            $date = $request->request_data->where('form_answer','training_ti_start_date')->first();

                            if($date){
                                $date = $date->request_content;
                            }else{
                                $date = '-';
                            }
                        break;

                        default :
                            $date = '-';
                        break;
                    }
                    $start_date = $date;
                    switch($request->form_group_id){
                        case 1:
                            $date = $request->request_data->where('form_answer','assignment_pg_end_date')->first();
                            if ($date){
                                $date = $date->request_content;
                            } else {
                                $date = "-";
                            }
                        break;

                        case 2:
                            $date = $request->request_data->where('form_answer','coaching_ci_end_date')->first();
                            if ($date){
                                $date = $date->request_content;
                            } else {
                                $date = "-";
                            }
                        break;

                        case 3:
                            $date = $request->request_data->where('form_answer','training_ti_end_date')->first();
                            if ($date){
                                $date = $date->request_content;
                            } else {
                                $date = "-";
                            }
                        break;

                        case 4:
                            $date = $request->request_data->where('form_answer','mentoring_ci_end_date')->first();
                            if ($date){
                                $date = $date->request_content;
                            } else {
                                $date = "-";
                            }
                        break;
                        
                        case 5:
                            $date = $request->request_data->where('form_answer','training_ti_end_date');
                            if ($date){
                                $date = $date->request_content;
                            } else {
                                $date = "-";
                            }
                        break;

                        default :
                            $date = '-';
                        break;
                    }
                    $end_date = $date;
                }

                $excel_data[$key] = [
                    'Title/Topic' => $title,
                    'Type' => $program_type,
                    'Type of Programs' => $type_ofp,
                    'Start Date' => str_replace('/', '-', $start_date),
                    'End Date' => str_replace('/', '-', $end_date),
                    'Employee NIK' => $employee_nik,
                    'Employee Name' => $employee_name,
                    'Line Manager' => $atasan,
                    'Manager' => ($export_type == 'rejected') ? 'Rejected' : (!empty($request->first_accepter) ? 'Approved' : 'Waiting Approval'),
                    'HR' => ($export_type == 'rejected') ? 'Rejected' : (!empty($request->second_accepter) ? 'Approved' : 'Waiting Approval'),
                    'Mentor/Coach Nik' => $coaching_nik,
                    'Mentor/Coach Name' => $coaching_name,
                ];

                if($export_type == 'rejected'){
                    $excel_data[$key]['Notes'] = $request->rejected_notes;
                }
            }
        }

       return Excel::download(new DefaultExport($excel_data), 'trainee-request-'.$export_type.'.xlsx');
    }
}
