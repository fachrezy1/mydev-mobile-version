<?php

namespace App\Modules\Admindashboard\Http\Controllers;

use App\Helpers\UserRolesHelper;
use App\Modules\Trainee\Models\requestsGroupModel;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Trainee\Models\RequestsUserModel;
use Sentinel;
use App\Modules\Trainee\Models\requestsModel;

class traineeHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $role = new UserRolesHelper();

        if ($role->inAdmin()){
            return false;
        }

        $user_id = Sentinel::check()->id;

        $related = RequestsUserModel::where(['user_id'=>$user_id])->get(['user_id','requests_group_id','user_position']);
        $data['requests_group'] = [];
        if ($related->isNotEmpty()) {
            foreach (collect($related)->sortKeysDesc()->toArray() as $key=>$item) {

                $data['position'][$item['requests_group_id']] = $item['user_position'];
                array_push($data['requests_group'],$this->getRequests($item['requests_group_id'],$item['user_id'])['requests']);
            }
        } else {
            $related = null;
        }
        $data['form_type'] = requestsModel::groupById;
        return $data;
//        return view('admindashboard::history.index',$data);
    }

    private function getRequests($group_id,$user_id) {
        $role = new UserRolesHelper();
        $model = new requestsModel();

        $data['requests'] = [];

        $requestGroup = requestsGroupModel::where(['id'=>$group_id])->get();

        if($requestGroup->isEmpty()) {
            return null;
        } else {
            $requestGroup=$requestGroup->toArray()[0];
        }
//        dd($requestGroup);
        $data['requests'][$requestGroup['user_id']] = $model->getRequestByFormId($group_id,null,$requestGroup['user_id']);
//        dd($group_id);
//        dump($data);
        if(!empty($data['requests'])){
            foreach ($data['requests'] as $key=>$request){
//                dd($data['requests']);
                $data['requests'][$key] = $model->groupRequest($request,true);
            }

            $data['requests'] = array_filter($data['requests']);
        }
        $data['form_type'] = requestsModel::groupById;
        return $data;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
