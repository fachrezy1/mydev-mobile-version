<?php

namespace App\Modules\Admindashboard\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Trainee\Models\requestsModel;
use App\Http\Controllers\Controller;
use App\Modules\Admindashboard\Models\reviewModel;


class ReviewController extends Controller
{
    //

    public function view($id) {
        $model = new requestsModel();
        $reviewModel = new reviewModel();
        $form['form_group_id'] = $id;
        $request = $model->getRequestByFormId($id,['user_id','form_type']);
        $data['is_completed'] = $request['is_completed'];
        unset($request['is_completed']);

        $merged = array_merge($form, $request);
        $review = $reviewModel->where($merged)->get();

        $data['form'] = $merged;
        $data['current_data'] = null;
        if ($review->isNotEmpty()){
           $data['current_data'] = $review->toArray();
        }
        return view('admindashboard::review',$data);
    }

    public function preview() {

    }
}
