<?php

namespace App\Modules\Admindashboard\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\UserRolesHelper;
use App\Modules\Trainee\Models\requestsModel;
use sentinel;
use App\Modules\Auth\Models\EmployeeModel;
use App\Modules\Trainee\Http\Controllers\TraineeFormController;
use App\Modules\Trainee\Models\Competency;
use App\Modules\Trainee\Models\CompetencyCategory;

use App\Http\Controllers\Controller;

class EditTraineeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($type_form,$user_id,$id)
    {
        //
        $role = new UserRolesHelper();
//        dd($role->inAdmin());
        if (!$role->inAdmin()) {
            return redirect('404');
        }
//        dd($type_form);
        if($type_form == 'assignment' || $type_form == 'mentoring' || $type_form == 'internal_training'):
                //include('trainee::preview.preview-mentoring')
            return $this->mentoring($user_id,$id);
        elseif($type_form == 'external_speaker'):
//                include('trainee::preview.preview-external'):
            return $this->external($user_id,$id);
        elseif($type_form == 'training' || $type_form == 'inhouse'):
//                include('trainee::preview.preview-training')
            return $this->training($user_id,$id);
        endif;

        return redirect('404');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getCompetenciesData(){
        $competency_category = CompetencyCategory::orderBy("name","asc")->get();
        $competencies = [];
        foreach ($competency_category as $key => $value) {
            $competencies[$key] = [
                "option_group"=>$value->name,
                "option_relation"=>Competency::where('category_id',$value->id)->get()->toArray()
            ];
        }
        return $competencies;
    }

    private function getUser($user) {
        $user = sentinel::findById($user);
        $user = EmployeeModel::where('nik',$user['username'])->get();
        if ($user->isNotEmpty()) {
            $user = $user->first()->toArray();
        } else {
            $user = null;
        }
        return $user;
    }

    private function getRequest($user_id,$id) {
        $request = new requestsModel();

        $data = $request->getRequestByFormId($id,['user_id','form_type','request_group_id'],$user_id);

        if (!$data){
            return redirect('404');
        }

        $data['requester'] = $this->getUser($data['form_detail']['user_id']);

        return  $data;
    }

    public function mentoring($user_id,$id) {
        $formController = new TraineeFormController();
        $role = new UserRolesHelper();

        $data = $this->getRequest($user_id,$id);
        if (isset($data["form_detail"]["development_mtioc"])){
            $cmpcpr = $data["form_detail"]["development_mtioc"];
        } else if (isset($data["form_detail"]["employee_competency"])){
            $cmpcpr = $data["form_detail"]["employee_competency"];
        } else {
            $cmpcpr = [];
        }

        $arr = explode(':',$cmpcpr);
        $competency_selected = [];

        foreach ($arr as $key => $value) {
            if (strlen($value)>5){
                array_push($competency_selected,strtr($value,[";i"=>"",";}"=>"",'"'=>""]));
            }
        }

        $data['competency_selected'] = $competency_selected;

        if (isset($data["form_detail"]["reason"])){
            $gtcpr = $data["form_detail"]["reason"];
        } else {
            $gtcpr = "";
        }

        $arrgt = explode(':',$gtcpr);
        $reason_selected = [];

        foreach ($arrgt as $key => $value) {
            if (strlen($value)>5){
                array_push($reason_selected,strtr($value,[";i"=>"",";}"=>"",'"'=>""]));
            }
        }

        $data['form_detail']['reason'] = $reason_selected;

        if($data['form_detail']['form_group_id'] != 1 && $data['form_detail']['form_group_id'] != 2 && $data['form_detail']['form_group_id'] != 6) {
            return redirect('404');
        }

        $data['formType'] = $data['form_detail']['form_group_id'] == 1 ? 'Assignment' : ($data['form_detail']['form_group_id'] == 2 ? 'Mentoring' : 'Internal Coach' );
//        dd($data);

        $data['employee'] = $formController->getEmployee($role->inStaff($data['form_detail']['user_id']));
        $data['competencies'] = $this->getCompetenciesData();
//        dd($data);
        session(['group_id'=>$id]);
        session(['user_id'=>$data['form_detail']['user_id']]);
        session()->flash('edit_form',true);
        return view('admindashboard::preview.edit.index',$data);

    }
    public function external($user_id,$id) {
        $data = $this->getRequest($user_id,$id);

        if($data['form_detail']['form_group_id'] != 4) {
            return redirect('404');
        }

        $data['formType'] = 'External Speaker';
//        dd($data);
        session(['group_id'=>$id]);
        session(['user_id'=>$data['form_detail']['user_id']]);
        session()->flash('edit_form',true);
        return view('admindashboard::preview.edit.index',$data);
    }
    public function training($user_id,$id) {
        $data = $this->getRequest($user_id,$id);
        if($data['form_detail']['form_group_id'] != 3 && $data['form_detail']['form_group_id'] != 5) {
            return redirect('404');
        }


          $form_group=$data['form_detail']['form_group_id'];
       




        if($form_group=="5"){

        $cek_group_id=requestsModel::where('requests_group_id',$data["form_detail"]["request_group_id"])->get();

         if(count($cek_group_id)>0){

            foreach($cek_group_id as $ce){

                $form_ans[]=$ce->form_answer;
            }
         }
    
        if (in_array("training_obj_moderator_nik", $form_ans)) {
             $data['moderator_name']=$data["form_detail"]["moderator_name"];
             $data['moderator_nik']=$data["form_detail"]["moderator_nik"];
        }else{

             $data['moderator_name']='notfound';
             $data['moderator_nik']='notfound';
        }

    }
        

        
        $arr = explode(':',$data["form_detail"]["employee_competency"]);
        $competency_selected = [];

        foreach ($arr as $key => $value) {
            if (strlen($value)>5){
                array_push($competency_selected,strtr($value,[";i"=>"",";}"=>"",'"'=>""]));
            }
        }

        $data['competency_selected'] = $competency_selected;
        $data['formType'] = $data['form_detail']['form_group_id'] == 3 ? 'Public Training' : ($data['form_detail']['form_group_id'] == 5 ? 'Inhouse' : '' );
        $data['competencies'] = $this->getCompetenciesData();
//        dd($data);
        session(['group_id'=>$id]);
        session(['user_id'=>$data['form_detail']['user_id']]);
        session()->flash('edit_form',true);
        return view('admindashboard::preview.edit.index',$data);
    }
}
