<?php

namespace App\Modules\Admindashboard\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Admindashboard\Models\pointModel;
use App\Modules\Trainee\Models\traineeModel;
use App\Helpers\UserRolesHelper;
use DB;

use App\Http\Controllers\Controller;

use App\Modules\Admindashboard\Models\multiplicationModel;

class PointManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $trainee = traineeModel::whereIn('id',[2,5,6])->get()->toArray();
        $data['trainees'] = $trainee;
        $data['point'] = pointModel::whereIn('trainee_id',[2,5,6])->orderBy('updated_at','desc')->get()->toArray();
        $data['trainees_by_id'] = traineeModel::whereIn('id',[2,5,6])->get()->groupBy('id')->toArray();
        $data['multiplication_point'] = multiplicationModel::whereIn('trainee_id',[2,5,6])->where('active',0)->orderBy('updated_at','desc')->get();
//        dd($data);
        return view('admindashboard::setting.point.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $role = new UserRolesHelper();
        if (!$role->inAdmin()){
            return ('404');
        }

        $data['trainee'] = traineeModel::where('id',$id)->get()->first()->toArray();

        return view('admindashboard::setting.point.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $role = new UserRolesHelper();
        if (!$role->inAdmin()){
            return ('404');
        }
        $point = new pointModel();
        $trainee = new traineeModel();

        $trainee_id = $trainee->where(['name'=>$request->get('trainee')])->get();
        if ($trainee_id->isEmpty()) {
            return redirect('404');
        } else {
            $trainee_id = $trainee_id->first()->id;
        }


        $point->updateOrCreate(
            ['point'=>$request->get('point'),'trainee_id'=>$trainee_id],
            ['point' => $request->get('point')]
        );

        $trainee->where('id',$trainee_id)
            ->update(['point'=>$request->get('point')]);

        return back()->withSuccess('Updated');
    }

    public function resetpoint(){

        $reset = DB::table('users')->update(array('point' => '0'));
        //$reset_history = DB::table('users_point_history')->truncate();

        return back()->withSuccess('Point Berhasil di Reset');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function add_multiplication_point()
    {

        $role = new UserRolesHelper();
        if (!$role->inAdmin()){
            return ('404');
        }
        $data['status']="add";
        $data['header']="Add new multiplication point";

       

        $check=multiplicationModel::where('trainee_id',5)->orwhere('trainee_id',2)->where('active',0)->get();

        $data['count_check']=count($check);
        
        if(count($check)==2){
           $data['trainees'] = ""; 
       }elseif(count($check)==0){
           $data['trainees'] = traineeModel::whereIn('id',[2,5])->get();
       }elseif(count($check)==1){

         foreach ($check as $key) {
             $tr_id=$key->trainee_id;
         }

          $data['trainees'] = traineeModel::whereIn('id',[2,5])->where('id','!=',$tr_id)->get();

       }

        

        $data['tier_2_1']="";
        $data['tier_3']="";
        $data['tier_4']="";
        $data['tier_5']="";
        $data['information']="";
        $data['id_multiplication']="";
        $data['trainee_id']="";
        return view('admindashboard::setting.point.add_multiflication_point',$data);
        
    }


     public function edit_multiplication_point($id)
    {
        $role = new UserRolesHelper();
        if (!$role->inAdmin()){
            return ('404');
        }
        $multiplication=multiplicationModel::where('id',$id)->first();
        $edit=traineeModel::where('id',$multiplication->trainee_id)->first();
        $data['header']="Edit ".$edit->name;
        $data['status']="edit";
        $data['trainees'] = traineeModel::whereIn('id',[2,5])->get();
        
            $data['tier_2_1']=$multiplication->tier_2_1;
            $data['tier_3']=$multiplication->tier_3;
            $data['tier_4']=$multiplication->tier_4;
            $data['tier_5']=$multiplication->tier_5;
            $data['information']=$multiplication->information;
            $data['id_multiplication']=$multiplication->id;
            $data['trainee_id']=$multiplication->trainee_id;
        

        return view('admindashboard::setting.point.add_multiflication_point',$data);
        
    }



     public function store_multiplication_point(Request $request)
    {
       $role = new UserRolesHelper();
        if (!$role->inAdmin()){
            return ('404');
        }


       $id_multiplication=$request->id_multiplication;

       $data=array(
          'trainee_id'   => $request->id_trainee,
          'tier_2_1'     => $request->tier_1,
          'tier_3'       => $request->tier_3,
          'tier_4'       => $request->tier_4,
          'tier_5'       => $request->tier_5,
          'information'  => $request->information,
        );

            if($id_multiplication){
               $multiplication=multiplicationModel::where('id',$id_multiplication)->update($data);
               return back()->with('completed', 'Update successfully!');
            }else{
               $multiplication = multiplicationModel::create($data);
               return back()->with('completed', 'Add successfully!');
            }
       
    }


    public function delete_multiplication_point($id){

        $data=array(
          'active'   => 1,
        );

        $multiplication=multiplicationModel::where('id',$id)->update($data);
               return back()->withSuccess('Delete Successfully');

    }
}
