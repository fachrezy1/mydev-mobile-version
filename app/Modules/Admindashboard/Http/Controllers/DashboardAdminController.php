<?php

namespace App\Modules\Admindashboard\Http\Controllers;

use App\Modules\Trainee\Models\requestsGroupModel;
use App\Modules\Auth\Models\UserModel;
use App\Modules\Trainee\Models\requestsModel;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Helpers\UserRolesHelper;
use Sentinel;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\User;
use Excel;
use App\Exports\DefaultExport;

class DashboardAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $role = new UserRolesHelper();

        if($role->inStaff()) {
            //if nor admin or lm
            return route('404');
        }
        $allowerd_user_staff = array();

        $all_month = [];
        for ($m=1; $m<=12; $m++) {
            $month = date('M', mktime(0,0,0,$m, 1, date('Y')));
            $all_month[] = $month;
        }

        if (!$role->inAdmin()) {
            $users_staff = $role->getLeaders(Sentinel::check()->id);
        } else {
            $users_staff = User::get()
                ->toArray();
        }

        if(isset($users_staff['user_type'])) {

            if($users_staff['user_type'] == 'dir') {
                $allowerd_user_staff['svp'] = $users_staff['svp'];
            } elseif($users_staff['user_type'] == 'svp') {
                $allowerd_user_staff['vp'] = $users_staff['vp'];
            } elseif($users_staff['user_type'] == 'vp') {
                unset($users_staff['svp']);
                unset($users_staff['dir']);
                $allowerd_user_staff = $users_staff;
            }
        } else {
            $allowerd_user_staff = $users_staff;
        }

        $staff_ids = [];
        foreach ($allowerd_user_staff as $staffs){

            if (is_array($staffs) && !$role->inAdmin()){
                foreach ($staffs as $staff) {

                    array_push($staff_ids,$staff['user_id']);
                }
            } if(is_array($staffs) && $role->inAdmin()){

                array_push($staff_ids,$staffs['id']);

            }
        }
        $staff_ids = array_filter($staff_ids);

        $new = requestsGroupModel::whereIn('user_id',$staff_ids)
            ->where([
                'is_completed'=>0, 
                'is_rejected' => 0
            ])
            ->whereNotNull('published_at')
            ->where(function($query) {
                    $query->whereNull('first_accepter')
                        ->orWhereNull('second_accepter');
            })
            ->where('is_accepted', 0)->get(['id'])->count();

        $onProgress = requestsGroupModel::whereIn('user_id',$staff_ids)
            ->where('is_completed_by_staff', 0)
            ->where('is_completed', 0)
            ->whereNotNull('first_accepter')
            ->where('first_accepter', '!=', 0)
            ->whereNotNull('second_accepter')
            ->where('second_accepter', '!=', 0)->get(['id'])->count();
            

        $completed = requestsGroupModel::whereIn('user_id',$staff_ids)
            ->where('is_completed','!=',0)
                ->where(['is_completed_by_staff'=>1])
                ->whereNotNull('first_accepter')
                ->where('first_accepter', '!=', 0)
                ->whereNotNull('second_accepter')
                ->where('second_accepter', '!=', 0)
            ->get(['id'])
            ->count();

        $rejected = requestsGroupModel::whereIn('user_id',$staff_ids)
            ->where(['is_rejected'=>1])
                ->where('is_rejected', 1)
                ->where('rejected_by',"!=", 0)
            ->get(['id'])
            ->count();

        $data['requests'] = [
            'new' => $new,
            'onProgress' => $onProgress,
            'completed' => $completed,
            'rejected' => $rejected,
        ];

        $request_by_month = requestsGroupModel::
        join('users as u',function ($join) use($staff_ids) {
            $join->on('u.id','=','requests_group.user_id')
                ->whereIn('u.id',$staff_ids);
        })->where('is_completed', 1)
            ->get([
                'form_group_id',
                'requests_group.created_at',
                DB::raw('CASE 
                    WHEN requests_group.form_group_id = 5 THEN "Inhouse" 
                    WHEN requests_group.form_group_id = 1 THEN "Assignment" 
                    WHEN requests_group.form_group_id = 2 THEN "Mentoring / Coaching" 
                    WHEN requests_group.form_group_id = 3 THEN "Public Training" 
                    WHEN requests_group.form_group_id = 4 THEN "External Speaker" 
                    ELSE "" 
                    END as form_group_alias')])
            ->groupBy(function($d) {
                return [Carbon::parse($d->created_at)->format('M')];
            });
        $request_by_form_type = [];

        $request_by_form_type = $request_by_month->map(function ($value,$key) use ($request_by_form_type) {
            return $request_by_form_type[$key] = $value->groupBy('form_group_alias')->toArray();
        })->all();

        $requests['request_by_form_type'] = $request_by_form_type;

        $request_by_form_type = [];
        foreach ($all_month as $month){

            if(!isset($requests['request_by_form_type'][$month])) {
                $request_by_form_type[$month] = null;
            } else {
                $request_by_form_type[$month] = $requests['request_by_form_type'][$month];
            }
        }

        $requests['request_by_form_type'] = $request_by_form_type;
        $data['report'] = $requests;
        $data['division_stats'] = $this->getDivisionStats($staff_ids);
        $data['competencies'] = $this->getCompetencies($staff_ids);
        $data['cost_items'] = $this->getCostItems($staff_ids);

        if($role->inAdmin()){
            return view('admindashboard::dashboard', $data);
        } elseif($role->inManager()){
            return view('admindashboard::dashboard-lm', $data);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getDivisionStats($staff_ids){
        $division_stats = userModel::select(
                'users.id',
                DB::raw('CONCAT_WS(\' \', users.first_name, users.last_name) AS employee_name'),
                DB::raw('SUM(
                    CASE WHEN (is_completed_by_staff = \'0\' 
                    AND is_accepted = \'0\' 
                ) THEN 1 ELSE 0 END) as waiting_approval'),

                DB::raw('SUM(
                    CASE WHEN (is_completed_by_staff = \'0\' 
                    AND is_accepted = \'1\' 
                    AND first_accepter IS NOT NULL 
                    AND first_accepter != \'0\'
                ) THEN 1 ELSE 0 END) as on_progress'),

                DB::raw('SUM(
                    CASE WHEN (is_rejected = \'1\' 
                ) THEN 1 ELSE 0 END) as rejected'),

                DB::raw('SUM(
                    CASE WHEN (is_completed = \'1\' 
                    AND is_completed_by_staff = \'1\' 
                ) THEN 1 ELSE 0 END) as completed')

            )
            ->join('requests_group', 'requests_group.user_id', 'users.id')
            ->join('employee', 'employee.email', 'users.email')
            ->groupBy('users.id','users.first_name','users.last_name')
            ->whereIn('users.id', $staff_ids)
            ->where('users.username', '!=' , 'Administrator') 
            ->orderBy('completed', 'desc')
            ->orderBy('on_progress', 'desc')
            ->limit(10)
            ->get();
            
        return $division_stats;
    }

    private function getCompetencies($staff_ids){
         $competencies = [
            'accelerate_business_and_customer' => [1 => 0, 2 => 0, 3 => 0, 5 => 0],
            'cultivate_networks_&_partnerships' => [1 => 0, 2 => 0, 3 => 0, 5 => 0],
            'decisiveness' => [1 => 0, 2 => 0, 3 => 0, 5 => 0],
            'learning_agility' => [1 => 0, 2 => 0, 3 => 0, 5 => 0],
            'making_a_difference' => [1 => 0, 2 => 0, 3 => 0, 5 => 0],
            'people_management' => [1 => 0, 2 => 0, 3 => 0, 5 => 0],
            'translating_strategy_into_action' => [1 => 0, 2 => 0, 3 => 0, 5 => 0]
        ];
        
        $request_group_ids = requestsGroupModel::whereIn('form_group_id', [1,2,3,5])
            ->whereIn('user_id', $staff_ids)
            ->where(['is_completed'=>1])
            ->where(['is_completed_by_staff'=>1])
            ->pluck('id')->toArray();

        $request_data = requestsModel::select('form_group_id', 'form_answer', 'request_content')
            ->whereIn('form_group_id', [1,2,3,5])
            ->whereIn('form_answer', ['assignment_obj_employee_competency','coaching_cc_development_mtioc', 'training_obj_employee_competency'])
            ->groupBy('form_group_id', 'form_answer', 'request_content')
            ->whereIn('requests_group_id', $request_group_ids)
            ->get();

        foreach($request_data as $req){
            $req_content = unserialize($req->request_content);
            foreach($req_content as $row){
                $competency_name = strtolower(str_replace(' ', '_', $row));

                
                isset($competencies[$competency_name][$req->form_group_id]) ? $competencies[$competency_name][$req->form_group_id]++ : false;
            }    
        }
        return $competencies;
    }

    private function getCostItems($staff_ids){
        $cost_items = 0;
        $request_data = requestsModel::select(DB::raw('SUM(request_content) as cost_items'))
            ->join('requests_group', 'requests_group.form_group_id', '=', 'requests.id')
            ->where('form_answer', 'training_ti_cost')
            ->whereIn('requests.user_id', $staff_ids)
            ->where(['is_completed'=>1])
            ->where(['is_completed_by_staff'=>1])
            ->first();

        if($request_data){
            $cost_items = $request_data->cost_items;
        }

        return $cost_items;
    }

    public function exportCourseByMonth(){
        $role = new UserRolesHelper();

        if($role->inStaff()) {
            return route('404');
        }

        $all_month = [];
        for ($m=1; $m<=12; $m++) {
            $month = date('M', mktime(0,0,0,$m, 1, date('Y')));
            $all_month[] = $month;
        }

        if (!$role->inAdmin()) {
            $users_staff = $role->getLeaders(Sentinel::check()->id);
        } else {
            $users_staff = User::get()
                ->toArray();
        }

        $allowerd_user_staff = array();

        if(isset($users_staff['user_type'])) {

            if($users_staff['user_type'] == 'dir') {
                $allowerd_user_staff['svp'] = $users_staff['svp'];
            } elseif($users_staff['user_type'] == 'svp') {
                $allowerd_user_staff['vp'] = $users_staff['vp'];
            } elseif($users_staff['user_type'] == 'vp') {
                unset($users_staff['svp']);
                unset($users_staff['dir']);
                $allowerd_user_staff = $users_staff;
            }
        } else {
            $allowerd_user_staff = $users_staff;
        }

        $staff_ids = [];
        foreach ($allowerd_user_staff as $staffs){

            if (is_array($staffs) && !$role->inAdmin()){
                foreach ($staffs as $staff) {

                    array_push($staff_ids,$staff['user_id']);
                }
            } if(is_array($staffs) && $role->inAdmin()){

                array_push($staff_ids,$staffs['id']);

            }
        }
        $staff_ids = array_filter($staff_ids);

        $request_by_month = requestsGroupModel::join('users as u',function ($join) use($staff_ids) {
            $join->on('u.id','=','requests_group.user_id')
                ->whereIn('u.id',$staff_ids);
        })->where('is_completed', 1);

        if(request()->has('year')){
            $request_by_month->where('requests_group.created_at', 'like', '%'.request()->get('year').'%');
        }
        
        $request_by_month = $request_by_month->get([
            'form_group_id',
            'requests_group.created_at',
            DB::raw('CASE 
                WHEN requests_group.form_group_id = 5 THEN "Inhouse" 
                WHEN requests_group.form_group_id = 1 THEN "Assignment" 
                WHEN requests_group.form_group_id = 2 THEN "Mentoring / Coaching" 
                WHEN requests_group.form_group_id = 3 THEN "Public Training" 
                WHEN requests_group.form_group_id = 4 THEN "External Speaker" 
                ELSE "" 
                END as form_group_alias')])
        ->groupBy(function($d) {
            return [Carbon::parse($d->created_at)->format('M')];
        }); 
        
        $request_by_form_type = [];

        $request_by_form_type = $request_by_month->map(function ($value,$key) use ($request_by_form_type) {
            return $request_by_form_type[$key] = $value->groupBy('form_group_alias')->toArray();
        })->all();

        $requests['request_by_form_type'] = $request_by_form_type;

        $request_by_form_type = [];
        foreach ($all_month as $month){

            if(!isset($requests['request_by_form_type'][$month])) {
                $request_by_form_type[$month] = null;
            } else {
                $request_by_form_type[$month] = $requests['request_by_form_type'][$month];
            }
        }

        $requests['request_by_form_type'] = $request_by_form_type;

        foreach($requests as $row){
            foreach($row as $month => $course){
                $data[] =[
                    'Month' => date("F", strtotime($month)), 
                    'External Speaker' => isset($course['External Speaker']) ? count($course['External Speaker']) : 0, 
                    'Public Training' => isset($course['Public Training']) ? count($course['Public Training']) : 0, 
                    'Mentoring / Coaching' => isset($course['Mentoring / Coaching']) ? count($course['Mentoring / Coaching']) : 0, 
                    'Assignment' => isset($course['Assignment']) ? count($course['Assignment']) : 0,
                    'Inhouse' => isset($course['Inhouse']) ? count($course['Inhouse']) : 0
                ];
            } 
        }
        
        return Excel::download(new DefaultExport($data), 'course-by-month.xlsx');
    }

    public function filterChartValue(){
        $staff_ids = [];
        $request_by_form_type = [];

        $role = new UserRolesHelper();

        $all_month = [];
        for ($m=1; $m<=12; $m++) {
            $month = date('M', mktime(0,0,0,$m, 1, date('Y')));
            $all_month[] = $month;
        }

        if (!$role->inAdmin()) {
            $users_staff = $role->getLeaders(Sentinel::check()->id);
        } else {
            $users_staff = User::get()
                ->toArray();
        }

        if(isset($users_staff['user_type'])) {

            if($users_staff['user_type'] == 'dir') {
                $allowerd_user_staff['svp'] = $users_staff['svp'];
            } elseif($users_staff['user_type'] == 'svp') {
                $allowerd_user_staff['vp'] = $users_staff['vp'];
            } elseif($users_staff['user_type'] == 'vp') {
                unset($users_staff['svp']);
                unset($users_staff['dir']);
                $allowerd_user_staff = $users_staff;
            }
        } else {
            $allowerd_user_staff = $users_staff;
        }

        foreach ($allowerd_user_staff as $staffs){

            if (is_array($staffs) && !$role->inAdmin()){
                foreach ($staffs as $staff) {

                    array_push($staff_ids,$staff['user_id']);
                }
            } if(is_array($staffs) && $role->inAdmin()){

                array_push($staff_ids,$staffs['id']);

            }
        }
        $staff_ids = array_filter($staff_ids);

        $request_by_month = requestsGroupModel::join('users as u',function ($join) use($staff_ids) {
            $join->on('u.id','=','requests_group.user_id')
                ->whereIn('u.id',$staff_ids);
        })->where('is_completed', 1);

        if(request()->has('year')){
            $request_by_month->where('requests_group.created_at', 'like', '%'.request()->get('year').'%');
        }
        
        $request_by_month = $request_by_month->get([
            'form_group_id',
            'requests_group.created_at',
            DB::raw('CASE 
                WHEN requests_group.form_group_id = 5 THEN "Inhouse" 
                WHEN requests_group.form_group_id = 1 THEN "Assignment" 
                WHEN requests_group.form_group_id = 2 THEN "Mentoring / Coaching" 
                WHEN requests_group.form_group_id = 3 THEN "Public Training" 
                WHEN requests_group.form_group_id = 4 THEN "External Speaker" 
                ELSE "" 
                END as form_group_alias')])
        ->groupBy(function($d) {
            return [Carbon::parse($d->created_at)->format('M')];
        }); 

        $request_by_form_type = $request_by_month->map(function ($value,$key) use ($request_by_form_type) {
            return $request_by_form_type[$key] = $value->groupBy('form_group_alias')->toArray();
        })->all();

        $requests['request_by_form_type'] = $request_by_form_type;

        $request_by_form_type = [];
        foreach ($all_month as $month){

            if(!isset($requests['request_by_form_type'][$month])) {
                $request_by_form_type[$month] = null;
            } else {
                $request_by_form_type[$month] = $requests['request_by_form_type'][$month];
            }
        }

        $requests['request_by_form_type'] = $request_by_form_type;
        $data['report'] = $requests;

        return $data;
    }
}
