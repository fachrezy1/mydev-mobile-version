<?php

namespace App\Modules\Admindashboard\Http\Controllers\Requests;

use App\Modules\Admindashboard\Http\Requests\Notification;

use App\Http\Controllers\Controller;
use App\Modules\Trainee\Models\requestsGroupModel;
use App\Models\UsersRelationModel;
use App\Modules\Trainee\Models\requestsModel;
use App\User;
use App\Notifications\Trainee;
use App\Helpers\UserRolesHelper;
use App\Modules\Trainee\Models\RequestsUserModel;

use Sentinel;
use Mail;
use App\Mail\TraineeMailNew;

class NotificationRequestsController extends Controller
{
    public function store(Notification $request) {
        $where = $request->toArray();
        $where['is_completed'] = 0;
        $urlNext = route('dashboard.traineeRequest');
        
        if (isset($where["page"]) && isset($where["tab"])){
            $page = $where["page"];
            $tab = $where["tab"];
            $urlNext = route('dashboard.traineeRequest')."?page=".$page."&tab=".$tab;
        }

        unset($where['page']);
        unset($where['tab']);

        $user = Sentinel::check();
        $user_id = $user->id;

        $is_hr = true;


        if(!$user->inRole('hr')){
            $is_hr = false;
        }


        unset($where['_token']);

        $role = new UserRolesHelper();

        if ($role->isRelated($where['user_id'],$user->username) || $user->inRole('hr')) {
            $model = new requestsGroupModel();
            $model->acceptRequest($user_id,$where,$is_hr);
            $this->sendNotification($where['user_id'],false,$where,'Your request have been accepted : ');

            return redirect($urlNext)->withSuccess('Request Accepted');
        } else {
            return back()->withError('Anda bukan atasan dari user ini');
        }
    }
    public function rejectRequest(Notification $request) {
        $where = $request->toArray();
        $where['is_completed'] = 0;
        $urlNext = route('dashboard.traineeRequest');

        if (isset($where["next_page"]) && isset($where["tab"])){
            $page = $where["next_page"];
            $tab = $where["tab"];
            $urlNext = route('dashboard.traineeRequest')."?page=".$page."&tab=".$tab;
        }

        unset($where['next_page']);
        unset($where['page']);
        unset($where['tab']);

        $user = Sentinel::check();
        $user_id = $user->id;

        unset($where['_token']);

        $role = new UserRolesHelper();

        if ($role->isRelated($where['user_id'],$user->username) || $user->inRole('hr')) {
            $model = new requestsGroupModel();
            $notes = $where['rejected_notes'];
            unset($where['rejected_notes']);
            $model->rejectRequest($user_id,$where,$notes);
            $this->sendNotification($where['user_id'],true,$where,'Your request have been rejected : ',$notes);
            return redirect($urlNext)->withSuccess('Request Rejected');
        } else {
            return back()->withError('Anda bukan atasan dari user ini');
        }
    }

    private function sendNotification($user_id,$rejected=false,$whereGroup,$msg,$notes = null) {
        $role = new UserRolesHelper();
        $atasan = Sentinel::check();
        $groupModel = requestsGroupModel::where($whereGroup)->get()->first()->toArray();
        $form_group = requestsModel::getGroupName($groupModel['form_group_id']);

        if (!$role->inAdmin($atasan->id)) {
            $action_by = 'Line Manager ('.$atasan->first_name.' '.$atasan->last_name.')';
        } else {
            $action_by = 'HR ('.$atasan->first_name.' '.$atasan->last_name.')';
        }

        $user_model = new User();
        $user = $user_model->find($user_id);

        $form_group = strtr($form_group, ["mentoring"=>"Mentoring / Coaching","Mentoring"=>"Mentoring / Coaching"]);

        if($rejected){
            if (!$role->inAdmin($atasan->id)) {
                //Notif From Line Manager
                $messages = [
                    'message_header' => 'Form Request Rejected',
                    'message_date' => now()->timestamp,
                    'url' => '/trainee/preview/'.$user_id.'/'.$groupModel['id'],
                    'type' => 'form request',
                    'subject' => 'Oops, Your Development Needs Further Discussion with Your Line Manager',
                    'message_body' => '<p>Bad news!</p>
                        <p>Your development request has been rejected by Your Line Manager. For your next request, try asking these questions:</p>
                        <p>
                            <ul>
                                <li>What can I improve from my current skill & ability?</li>
                                <li>How can I apply my new skill & ability to improve our performance?</li>
                            </ul>
                        </p>
                        <p>Good luck on your next try!</p>',
                    'form_group' => $form_group,
                    'to' => $user->email
                ];

                notify_admin($messages);
            }else{
                //Notif From Admin
                $messages = [
                    'message_header' => 'Form Request Rejected',
                    'message_date' => now()->timestamp,
                    'url' => '/trainee/preview/'.$user_id.'/'.$groupModel['id'],
                    'type' => 'form request',
                    'subject' => 'Oops, Something’s Wrong with Your Development Request',
                    'message_body' => '<p>Bad news!</p>
                        <p>Your development request has been rejected by Admin MyDev. Check out what’s gone wrong with your request by replying this email and re-submit your request after revising it.</p>
                        <p>Good luck on your next try!</p>',
                    'form_group' => $form_group,
                    'to' => $user->email
                ];
            }
        } else {
            if (!$role->inAdmin($atasan->id)) {
                //Notif From Line Manager
                $messages = [
                    'message_header' => 'Form Request Accepted',
                    'message_date' => now()->timestamp,
                    'url' => '/trainee/preview/'.$user_id.'/'.$groupModel['id'],
                    'type' => 'form request',
                    'subject' => 'One More Step Towards Your Development',
                    'message_body' => '<p>Congratulations!</p>
                        <p>Your development: '.$form_group.' request has been accepted by your Line Manager.</p>
                        <p>Immediately MyDev Admin will review your request, just to double-check so that you can have the best development experience. Until then, keep calm and keep learning.</p>
                        <p>You will receive the notification email to start your development in 1... 2... 3...</p>',
                    'form_group' => $form_group,
                    'to' => $user->email
                ];
            } else {
                //Notif From Admin
                $messages = [
                    'message_header' => 'Form Request Accepted',
                    'message_date' => now()->timestamp,
                    'url' => '/trainee/preview/'.$user_id.'/'.$groupModel['id'],
                    'type' => 'form request',
                    'subject' => 'Time to Start Your Development',
                    'message_body' => '<p>Congratulations!</p>
                        <p>Your development: '.$form_group.' request has been accepted by MyDev Admin.</p>
                        <p>Now you can start your '.$form_group.', enjoy the sessions, explore the whole new world & improve to your best-self.</p><br/>
                        <p>PS:</p>
                        <p>If you\'re:</p>
                        <p>
                            <ul>
                                <li>Joining Coaching/Mentoring sessions, your sessions will be recognized as completed once your Coach/Mentor has submitted the review for your sessions.</li>
                                <li>Joining an Assignment or Public Training/Being an external speaker, be sure you’ve finished your assignment/public training/external workshop first before clicking \'done\' <a href="'.url('mydev/trainee/my-trainee').'">here</a></li>
                            </ul>
                        </p>',
                    'form_group' => $form_group,
                    'to' => $user->email
                ];
            }
        }

        $user->notify(new Trainee($messages));
        
        
        //Mail::to($user->email)->send(new TraineeMailNew($messages));
        
        

        $mentor_id = RequestsUserModel::where(['requests_group_id'=>$groupModel['id']])->get(['user_id']);

        if($mentor_id->isNotEmpty()) {
            $mentor_id = $mentor_id->first()['user_id'];
            $messages['message_body'] = str_replace('Your request','Request related to you',$messages['message_body']);
            User::find($mentor_id)->notify(new Trainee($messages));
        }

    }
}
