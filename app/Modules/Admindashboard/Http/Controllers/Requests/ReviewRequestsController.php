<?php

namespace App\Modules\Admindashboard\Http\Controllers\Requests;

use App\Modules\Admindashboard\Http\Requests\StoreReviews;

use App\Http\Controllers\Controller;
use App\Modules\Admindashboard\Models\reviewModel;
use App\Modules\Trainee\Models\requestsGroupModel;
use Illuminate\Support\Facades\Request;

use Sentinel;
class ReviewRequestsController extends Controller
{
    //

    protected $reviewModel;
    protected $requestGroupModel;

    function __construct()
    {
        $this->reviewModel = new reviewModel();
        $this->requestGroupModel = new requestsGroupModel();

    }

    function store(StoreReviews $request) {
        $staff = Sentinel::check();
        if ($staff->hasAccess('trainee.manage')) {
            $validated = $request->toArray();
            $submitButton = $validated['submit_button'];
            unset($validated['submit_button']);
            unset($validated['_token']);
            foreach ($validated['reviews_date'] as $key=>$item) {
                $data = [
                    'user_id' => $validated['user_id'],
                    'form_group_id' => $validated['form_group_id'],
                    'form_type' => $validated['form_type'],
                    'reviews_date' => $validated['reviews_date'][$key],
                    'reviews_goal' => $validated['reviews_goal'][$key],
                    'reviews_result' => $validated['reviews_result'][$key],
                    'reviews_feedback' => $validated['reviews_feedback'][$key],
                    'reviews_action' => $validated['reviews_action'][$key],
                ];
                $this->reviewModel->updateOrCreate(
                    [
                        'user_id'=>$validated['user_id'],
                        'form_group_id' => $validated['form_group_id'],
                        'form_type' => $validated['form_type'],
                        'reviews_date' => $validated['reviews_date'][$key]
                    ],
                    $data);
            }

            $where = [
                'user_id' => $validated['user_id'],
                'form_group_id' => $validated['form_group_id'],
            ];

            if ($submitButton === 'completed') {
                $this->requestGroupModel
                    ->where($where)
                    ->update(
                        [
                            'is_completed' => 1,
                        ]
                    );
            }

            return back()->with('success','input success');
        }

        return false;

    }
}
