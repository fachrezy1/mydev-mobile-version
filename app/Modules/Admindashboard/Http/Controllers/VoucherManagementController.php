<?php

namespace App\Modules\Admindashboard\Http\Controllers;

use Excel;
use Illuminate\Http\Request;
use App\Exports\DefaultExport;
use App\Modules\Admindashboard\Models\voucherModel;
use App\Modules\Admindashboard\Models\voucherCodeModel;
use App\Modules\Trainee\Models\traineeModel;
use App\Helpers\UserRolesHelper;
use Illuminate\Support\Facades\Storage;
use App\Modules\Admindashboard\Http\Requests\voucherRequest;

use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class VoucherManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admindashboard::voucher-management.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admindashboard::voucher-management.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(voucherRequest $request)
    {
        $errors = [];
        $insert_data = [];

        if ($request->hasFile('image')) {
            Storage::disk('uploads')->putFileAs('items',$request->file('image'),$request->file('image')->getClientOriginalName());
            $image = 'items/'.$request->file('image')->getClientOriginalName() ;
        } else {
            return back()->withError('Image is empty');
        }

        $voucher = new voucherModel;
        $voucher->name = $request->name;
        $voucher->amount = $request->amount;
        $voucher->point = $request->point;
        $voucher->image = $image;
        $voucher->status = ($request->has('published') ? 1 : 0);
        $voucher->save();

        if($request->hasFile('code')){
            $path = $request->file('code')->getRealPath();
            $data = array_map('str_getcsv', file($path));
            $csv_data = array_slice($data, 1, count($data));
            $voucher_codes = voucherCodeModel::pluck('code')->toArray();

            foreach($csv_data as $idx => $csv){
                if(empty($csv[0])){
                    $errors[$idx] = 'Voucher Code is Empty';
                }elseif(in_array($csv[0], $voucher_codes)){
                    $errors[$idx] = 'Voucher Code already exist';
                }else{
                    $insert_data[] = [
                        'voucher_id' => $voucher->id,
                        'code'  => $csv[0],
                        'created_at'  => date('Y-m-d H:i:s'),
                        'updated_at'  => date('Y-m-d H:i:s'),
                    ];
                }
            }

            voucherCodeModel::insert($insert_data);
            
            if(!empty($errors)){  
                return redirect()->route('dashboard.voucher.index')
                    ->with('error', 'Some voucher code can\'t be inserted')
                    ->with('error_items', $errors);
            }else{
                return redirect()->route('dashboard.voucher.index')
                    ->with('success', 'Successfully created a new voucher');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = new UserRolesHelper();
        if (!$role->inAdmin()){
            return ('404');
        }

        $data['voucher'] = voucherModel::find($id);
        return view('admindashboard::voucher-management.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(voucherRequest $request)
    {
        $errors = [];

        $role = new UserRolesHelper();
        if (!$role->inAdmin()){
            return ('404');
        }

        $update = [
            'name'=>$request->name,
            'description'=>$request->description,
            'amount'=>$request->amount,
            'point'=>$request->point,
            'status'=> ($request->has('published') ? 1 : 0)
        ];

        if ($request->hasFile('image')) {
            Storage::disk('uploads')->putFileAs('items',$request->file('image'),$request->file('image')->getClientOriginalName());
            $update['image'] = 'items/'.$request->file('image')->getClientOriginalName() ;
        }

        $voucher = new voucherModel();
        $voucher->where('id', $request->id)
            ->update($update);

        if($request->hasFile('code')){
            $insert_data = [];
            $path = $request->file('code')->getRealPath();
            $data = array_map('str_getcsv', file($path));
            $csv_data = array_slice($data, 1, count($data));

            $voucher_codes = voucherCodeModel::pluck('code')->toArray();

            foreach($csv_data as $idx => $csv){
                if(empty($csv[0])){
                    $errors[$idx] = 'Voucher Code is Empty';
                }else{
                    if(!in_array($csv[0], $voucher_codes)){
                        $insert_data[] = [
                            'voucher_id' => $request->id,
                            'code'  => $csv[0],
                            'created_at'  => date('Y-m-d H:i:s'),
                            'updated_at'  => date('Y-m-d H:i:s'),
                        ];
                    }
                }
            }

            voucherCodeModel::insert($insert_data);
        }

        if(!empty($errors)){  
            return redirect()->route('dashboard.voucher.index')
                ->with('error', 'Some voucher code can\'t be updated')
                ->with('error_items', $errors);
        }else{
            return redirect()->route('dashboard.voucher.index')
                ->with('success', 'Successfully update voucher data');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = new UserRolesHelper();

        if (!$user->inAdmin()) {
            return redirect('404');
        }

        $voucher_code = voucherCodeModel::where('voucher_id', $id)
            ->where('user_id', '!=', 0)
            ->count();

        if($voucher_code > 0){
            return redirect()->route('dashboard.voucher.index')
                ->with('error', 'Failed to delete voucher')
                ->with('error_items', ['The voucher code already used']);
        }else{
            $voucher = new voucherModel();
            $voucher_code = new voucherCodeModel();

            $voucher_code->where('voucher_id', $id)->delete();
            $voucher->where('id',$id)->delete();

            return redirect()->route('dashboard.voucher.index')
                ->with('success', 'Successfully delete voucher');
        }
    }

    public function voucherDatatable(){
        $role = new UserRolesHelper();

        if(!$role->inAdmin()){
            return '404';
        }

        $vouchers = voucherModel::get();
        $return = DataTables::of($vouchers)
            ->addColumn('options',function ($voucher){
                return '<div class="table-button">
                    <a href="'.route("dashboard.voucher.edit",$voucher["id"]).'">
                        <button type="button" class="btn btn-default btn-sm">
                            <i class="icon-edit-2"></i> Edit
                        </button>
                    </a>
                    <a href="'.route("dashboard.voucher.destroy",$voucher["id"]).'">
                        <button type="button" class="btn btn-default btn-sm">
                            <i class="icon-clean"></i> Delete
                        </button>
                    </a>
                </div>';
            })
            ->editColumn('amount',function ($voucher){
                return 'Rp.'.number_format($voucher->amount,2,',','.');
            })
            ->editColumn('description',function ($voucher){
                return limit_words($voucher->description, 20);
            })
            ->editColumn('status',function ($voucher){
                return $voucher->status == 1 ? 'Published' : 'Unpublished';
            })
            ->rawColumns(['options','image'])
            ->make();
        return $return;
    }

    public function voucherHistoryDatatable(){
        $role = new UserRolesHelper();

        if(!$role->inAdmin()){
            return '404';
        }

        $voucher_codes = voucherCodeModel::select('voucher_codes.*', 'voucher.name', 'voucher.description', 'voucher.amount', 'users.username')
            ->join('voucher', 'voucher.id', '=', 'voucher_codes.voucher_id')
            ->join('users', 'users.id', '=', 'voucher_codes.user_id')
            ->where('user_id', '!=', 0)
            ->get();

        $return = DataTables::of($voucher_codes)
            ->editColumn('amount',function ($voucher_code){
                return 'Rp.'.number_format($voucher_code->amount,2,',','.');
            })
            ->editColumn('description',function ($voucher_code){
                return limit_words($voucher_code->description, 20);
            })
            ->addColumn('used_date',function ($voucher_code){
                return date('j F Y', strtotime($voucher_code->created_at));
            })
            ->rawColumns(['options','image'])
            ->make();
        return $return;
    }

    public function voucherCodeDatatable($id){
        $role = new UserRolesHelper();

        if(!$role->inAdmin()){
            return '404';
        }

        $voucher_codes = voucherCodeModel::where('voucher_id', $id)->get();

        $return = DataTables::of($voucher_codes)
            ->addColumn('username',function ($voucher_code){
                return !empty($voucher_code->user_id) ? $voucher_code->user->username : '-';           
            })
            ->editColumn('status',function ($voucher_code){
                return !empty($voucher_code->user_id) ? 'Used' : 'Available';
            })
            ->rawColumns(['options','image'])
            ->make();
        return $return;
    }

    public function voucherExportHistory(){
        $excel_data[] = [
            'Name' => '',
            'Code' => '',
            'Description' => '',
            'Amount' => '',
            'Username' => '',
            'Used Date' => '',
        ];

        $role = new UserRolesHelper();

        if(!$role->inAdmin()){
            return '404';
        }
        
        $vouchers = voucherCodeModel::select('voucher_codes.*', 'voucher.name', 'voucher.description', 'voucher.amount', 'users.username')
            ->join('voucher', 'voucher.id', '=', 'voucher_codes.voucher_id')
            ->join('users', 'users.id', '=', 'voucher_codes.user_id')
            ->where('user_id', '!=', 0)
            ->get();

        foreach($vouchers as $idx => $voucher){
            $excel_data[$idx] = [
                'Name' => $voucher->name,
                'Code' => $voucher->code,
                'Description' => $voucher->name,
                'Amount' => 'Rp.'.number_format($voucher->amount,2,',','.'),
                'Username' => $voucher->username,
                'Used Date' => date('j F Y', strtotime($voucher->created_at)),
            ];
        }

        return Excel::download(new DefaultExport($excel_data), 'voucher-history.xlsx');
    }
}
