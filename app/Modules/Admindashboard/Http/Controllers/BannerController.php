<?php

namespace App\Modules\Admindashboard\Http\Controllers;

use function foo\func;
use Illuminate\Http\Request;
use App\Helpers\UserRolesHelper;
use App\Modules\Admindashboard\Models\SettingModel;
use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\Controller;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $role = new UserRolesHelper();
        if (!$role->inAdmin()) {
            return redirect('404');
        }

        return view('admindashboard::banner.add');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $role = new UserRolesHelper();
        if (!$role->inAdmin()) {
            return redirect('404');
        }

        $data = $request->toArray();
        $model = new SettingModel();

        if ($request->hasFile('banner_image')){

            Storage::disk('uploads')->putFileAs('',$request->file('banner_image'),$request->file('banner_image')->getClientOriginalName());

            $item = $request->file('banner_image')->getClientOriginalName();

        }
        unset($data['_token']);
        foreach ($data as $key=>$value) {
//            dd($item);
            if ($request->hasFile($key)){
                $input = [
                    'setting_key' => $key,
                    'setting_value' => $item,
                    'setting_group' => 'banner',
                ];
            } else {
                $input = [
                    'setting_key' => $key,
                    'setting_value' => $value,
                    'setting_group' => 'banner',
                ];
            }
            $model->create($input);
        }
        return redirect(route('dashboard.banner.show'))->withSuccess('Update success');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        $role = new UserRolesHelper();
        if (!$role->inAdmin()) {
            return redirect('404');
        }

        $banners = SettingModel::where('setting_group','banner')->get();

        if ($banners->isNotEmpty()) {
            $banners = $banners->toArray();
        } else{
            return redirect(route('dashboard.banner.add'));
        }
        foreach ($banners as $banner) {
            $data['banner'][$banner['setting_key']] = $banner['setting_value'];
        }

        return view('admindashboard::banner.edit',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $role = new UserRolesHelper();
        if (!$role->inAdmin()) {
            return redirect('404');
        }

        $data = $request->toArray();
        $model = new SettingModel();

        if ($request->hasFile('banner_image')){

            Storage::disk('uploads')->putFileAs('',$request->file('banner_image'),$request->file('banner_image')->getClientOriginalName());

            $item = $request->file('banner_image')->getClientOriginalName();

        }
        unset($data['_token']);
        foreach ($data as $key=>$value) {
//            foreach ($data as $key=>$item) {
                $update = [
                    'setting_group' => 'banner',
                    'setting_key' => $key,
                    'setting_value' => $item
                ];
                $model->where([
                    'setting_group' => 'banner',
                    'setting_key' => $key,
                ])->update($update);
//            }
//            $model->create($input);
        }
        return redirect(route('dashboard.banner.show'))->withSuccess('Update success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
