<?php

namespace App\Modules\Admindashboard\Http\Controllers;

use function foo\func;
use Carbon\Carbon;
use Illuminate\Routing\Route;
use Sentinel;
use App\Modules\Admindashboard\Models\itemModel;
use App\Http\Requests\StoreItem;
use Illuminate\Support\Facades\Storage;
use App\Helpers\UserRolesHelper;

use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use App\Modules\Admindashboard\Models\itemRedeemModel;

use App\User;
use App\Notifications\Trainee;
class ItemManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
//        $data['items'] = itemModel::get();
//        if ($data['items']->isNotEmpty()) {
//            $data['items'] = $data['items']->toArray();
//        } else {
//            $data['items'] = null;
//        }

        return view('admindashboard::item-management.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admindashboard::item-management.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreItem $request)
    {

        if ($request->hasFile('picture')) {

            Storage::disk('uploads')->putFileAs('items',$request->file('picture'),$request->file('picture')->getClientOriginalName());

            $item = 'items/'.$request->file('picture')->getClientOriginalName() ;

        } else {
            return back()->withError('Picture empty');
        }

        $data = $request->toArray();
        $data['picture'] = $item;

        if (isset($data['published'])) {
            $data['status'] = 'published';
        } else {
            $data['status'] ='unpublished';
        }

        $model = new itemModel();

        $id = $model->create($data);

        return redirect(route('dashboard.item.edit',$id->id))->withSuccess('Item added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }


    public function getItems(){
        $role = new UserRolesHelper();

        if(!$role->inAdmin()){
            return '404';
        }

        $items = itemModel::get();
        $return = DataTables::of($items)
            ->addColumn('image',function ($item){
                return '<img src="'.asset('uploads/'.$item['picture']).'" class="img-fluid" alt="'.$item['name'].'" />';
            })
            ->addColumn('options',function ($item){
                return '<div class="table-button">
                    <a href="'.route("dashboard.item.edit",$item["id"]).'">
                        <button type="button" class="btn btn-default btn-sm">
                            <i class="icon-edit-2"></i> Edit
                        </button>
                    </a>
                    <a href="'.route("dashboard.item.destroy",$item["id"]).'">
                        <button type="button" class="btn btn-default btn-sm">
                            <i class="icon-clean"></i> Delete
                        </button>
                    </a>
                </div>';
            })
            ->rawColumns(['options','image'])
            ->make();
        return $return;
    }

    public function approveItem($id){
        $role = new UserRolesHelper();

        if(!$role->inAdmin()){
            return route('404');
        }

        if(itemRedeemModel::where(['id'=>$id])->update(['approved'=>1])){
            $item = itemRedeemModel::where(['id'=>$id])->get(['item_id','user_id'])->first();
            return back()->withSuccess('Item Approved');
        }

        return back()->withSuccess('Something wrong please try again');

    }

    public function getItemRequests(){
        $role = new UserRolesHelper();

        if(!$role->inAdmin()){
            return route('404');
        }

        $items = itemRedeemModel::where(['approved'=>0])
            ->join('item as i','i.id','=','item_redeem.item_id')
            ->join('users as u','u.id','=','item_redeem.user_id')
            ->get(['i.name','i.description','u.first_name','u.username','i.picture','item_redeem.id']);

        $return = DataTables::of($items)
            ->addColumn('requester',function ($item){
                return '<span>'.$item['first_name'].'('.$item['username'].')'.'</span>';
            })
            ->addColumn('image',function ($item){
                return '<img src="'.asset('uploads/'.$item['picture']).'" class="img-fluid" />';
            })
            ->addColumn('options',function ($item){
                return '<div class="table-button">
                    <a href="'.route("dashboard.item.approveItem",$item["id"]).'">
                        <button type="button" class="btn btn-default btn-sm">
                            <i class="icon-edit-2"></i> Approve
                        </button>
                    </a>
                </div>';
            })
            ->rawColumns(['requester','options','image'])
            ->make();
        return $return;
    }

    public function getApprovedItems(){
        $role = new UserRolesHelper();

        if(!$role->inAdmin()){
            return route('404');
        }

        $items = itemRedeemModel::where(['approved'=>1])
            ->join('item as i','i.id','=','item_redeem.item_id')
            ->join('users as u','u.id','=','item_redeem.user_id')
            ->get(['i.name','i.description','u.first_name','u.username','i.picture','item_redeem.id', 'item_redeem.created_at', 'item_redeem.point_from', 'item_redeem.point_to']);

        $return = DataTables::of($items)
            ->addColumn('requester',function ($item){
                return '<span>'.$item['first_name'].'('.$item['username'].')'.'</span>';
            })
            ->addColumn('image',function ($item){
                return '<img src="'.asset('uploads/'.$item['picture']).'" class="img-fluid" />';
            })
            ->editColumn('created_at',function ($item){
                return Carbon::parse($item['created_at'])->format('l, d M, Y');
            })
            ->addColumn('point_info',function ($item){
                return 'from '.(int)$item['point_from'].'pt to '.(int)$item['item'].'pt';
            })
            ->rawColumns(['requester','image'])
            ->make();
        return $return;
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = new UserRolesHelper();

        if ($user->inAdmin()) {
            $model = new itemModel();
            $data['item'] = $model->where('id',$id)->get();
        } else {
            return redirect('404');
        }
        if ($data['item']->isNotEmpty()) {
            $data['item'] = $data['item']->first()->toArray();
        } else {
            return redirect('404');
        }

        return view('admindashboard::item-management.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreItem $request)
    {
        //
        $data = $request->toArray();
        $model = new itemModel();

        if ($request->hasFile('picture')) {

            Storage::disk('uploads')->putFileAs('items',$request->file('picture'),$request->file('picture')->getClientOriginalName());

            $item = 'items/'.$request->file('picture')->getClientOriginalName() ;
            $data['picture'] = $item;

        }
        unset($data['_token']);

        if(isset($data['published'])) {
            $data['status'] = 'published';
            unset($data['published']);
        } else {
            $data['status'] = 'unpublished';
        }
        $model->where('id',$data['id'])->update($data);

        return back()->withSuccess('Item Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = new UserRolesHelper();

        if (!$user->inAdmin()) {
            return redirect('404');
        }

        $model = new itemModel();
        $item = $model->where('id',$id)->get();

        $model->where('id',$id)->delete();

        return back()->withSuccess('Item : '.$item[0]->name.' Deleted');

    }
}
