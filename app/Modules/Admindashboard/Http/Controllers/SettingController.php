<?php

namespace App\Modules\Admindashboard\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Imports\PAImport;
use Excel;
use App\Modules\Auth\Models\EmployeeModel;
use App\Modules\Trainee\Models\ratingModel;
use App\Modules\Trainee\Models\requestsGroupModel;
use App\Modules\Trainee\Models\requestsModel;
use App\Traits\ResponsesTrait;
use App\Notifications\Trainee;
use Illuminate\Support\Facades\Storage;
use App\Modules\Trainee\Models\RequestsUserModel;
use App\User;
use App\Helpers\UserRolesHelper;
use App\Modules\Auth\Models\UserRolesModel;
use App\Modules\Auth\Models\UserModel;
use Illuminate\Support\Facades\DB;
use App\Modules\Admindashboard\Models\reviewModel;
use App\Modules\Trainee\Models\traineeModel;

use App\Helpers\ActivityHelper;

use Sentinel;

use Mail;
use App\Mail\TraineeMailNew;

class SettingController extends Controller
{
    //
    use ResponsesTrait;

    private $activity;

    function __construct()
    {
        $this->activity = new ActivityHelper();
    }

    function setSession($data, $key) {
        if (session()->has('formData')){
            $current = session('formData');
            $current[$key] = $data[$key];
            session(['formData'=>$current]);
        }else{
            session(['formData'=>$data]);
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admindashboard::setting.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function validateInHouse($data){
       $valid = [];
            $validation = [
                "training_ti_title",
                "training_ti_type_programs",
                "training_obj_trainer_name",
                "training_obj_trainer_nik",
                "training_ti_cost",
                "training_ti_start_date",
                "training_ti_end_date",
                "training_obj_description",
                "training_obj_venue",
                "training_obj_employee_name",
                "training_obj_employee_nik",
                "training_obj_employee_position",
                "training_obj_employee_competency"
            ];
       foreach ($validation as $key => $val) {
           if (!in_array($val, $data)){
                array_push($valid,$val);
           }
       }
       if (count($valid)==0){
            return "good";
       } 
       return $valid;
    }

    private function validateProjectAssigment($data){
       $valid = [];
            $validation = [
                "user_role",
                "user_nik",
                "assignment_dev_type",
                "assignment_obj_employee_competency",
                "assignment_pg_project_leader",
                "assignment_pg_project_leader_nik",
                "assignment_pg_project_title",
                "assignment_pg_start_date",
                "assignment_pg_end_date",
                "assignment_pg_project_description",
                "assignment_pg_development_goal",
                // "assignment_inter_group",
                // "assignment_inter_division",
                // "assignment_inter_start_date",
                // "assignment_inter_end_date",
                // "assignment_inter_development_goal"
                "assignment_gt_opco",
                "assignment_gt_type",
                "assignment_gt_start_date",
                "assignment_gt_end_date",
                "assignment_gt_purpose",
                "assignment_gt_reason",
                "assigment_gt_attachment"
            ];
       foreach ($validation as $key => $val) {
           if (!in_array($val, $data)){
                array_push($valid,$val);
           }
       }
       if (count($valid)==0){
            return "good";
       } 
       return $valid;
    }

    private function validateMentoring($data){
       $valid = [];
            $validation = [
              "user_role",
              "user_nik",
              "coaching_ci_type",
              "coaching_ci_line_manager_name",
              "coaching_ci_line_manager_nik",
              "coaching_ci_start_date",
              "coaching_ci_end_date",
              "coaching_cc_development_goal",
              "coaching_cc_development_mtioc",

              "reviews_date",
              "reviews_duration",
              "reviews_goal",
              "reviews_condition",
              "reviews_action",
              "reviews_committed_date",
              "reviews_result"
            ];
       foreach ($validation as $key => $val) {
           if (!in_array($val, $data)){
                array_push($valid,$val);
           }
       }
       if (count($valid)==0){
            return "good";
       } 
       return $valid;
    }

    private function validatePublicTraining($data){
       $valid = [];
            $validation = [
              "training_ti_title",
              "training_ti_type_programs",
              "training_ti_start_date",
              "training_ti_end_date",
              "training_obj_employee_competency",
              "training_obj_description",
              "training_obj_commitment_training",
              "training_obj_commitment_description",
              "training_obj_start_date2",
              "training_obj_end_date2",
              "training_ti_provider",
              "training_ti_cost",
              "training_obj_venue"
            ];
       foreach ($validation as $key => $val) {
           if (!in_array($val, $data)){
                array_push($valid,$val);
           }
       }
       if (count($valid)==0){
            return "good";
       } 
       return $valid;
    }

    private function validateExternalSpeaker($data){
       $valid = [];
            $validation = [
              "user_role",
              "user_nik",
              "mentoring_ci_line_topic",
              "mentoring_ci_organization",
              "mentoring_ci_line_venue",
              "mentoring_ci_start_date",
              "mentoring_ci_end_date"
            ];
       foreach ($validation as $key => $val) {
           if (!in_array($val, $data)){
                array_push($valid,$val);
           }
       }
       if (count($valid)==0){
            return "good";
       } 
       return $valid;
    }

    private function setData($validated) {
        $review_id = null;
        if (isset($validated['review_id'])) {
            $review_id = $validated['review_id'];
            unset($validated['review_id']);
        }
            if ($review_id) {
                //case 1: dimana data sebagian form adalah data lama dan sebagian lagi data baru
                foreach ($review_id as $keyId=>$id) {
                    //ambil data dari data original
                    foreach($validated as $key=>$item){
                        if (is_array($item)) {
                            if (isset($item[$keyId])) {
                                $data[$key] = $item[$keyId];
                            }
                            //hapus data yang telah didapatkan agar tidak diulang lagi
                            unset($validated[$key][$keyId]);
                        } else {
                            $data[$key] = $item;
                        }
                    }
                    //segera update data
                    reviewModel::updateOrCreate(
                        ['id'=>$id],
                        $data);
                }
                //panggil kembali fungsi jika masih ada item dalam array
                if (count($validated[$key]) > 0){
                    $this->setData($validated);
                }
            } else {
                $data=[];
                foreach ($validated as $key=>$item) {
                    //setelah review id direset jika masih ada data dalam $validated
                        if (is_array($item)) {
                            foreach ($item as $itemKey=>$value){
                                if (!isset($data[$key])) {
                                    $data[$key] = $value;
                                    unset($validated[$key][$itemKey]);
                                }
                            }
                        } else {
                            $data[$key] = $item;
                        }
                }
                //create data

                reviewModel::Create($data);
                //panggil kembali fungsi jika masih ada item dalam array
                if (count($validated[$key]) > 0 ){
                    $this->setData($validated);
                }
            }

    }

    private function submitReview($request) {
        $user = Sentinel::findById($request->user_id);

        if ($user->hasAccess('trainee.manage') || $user->hasAccess('admin.createTrainee') ) {
            
            $validated = (array) $request;

            $submitButton = $validated['submit_button'];
            unset($validated['submit_button']);

            $this->setData($validated);

            $where = [
                'user_id' => $validated['user_id'],
                'id' => $validated['request_group_id'],
            ];

            if ($submitButton == 'completed') {
                $role = new UserRolesHelper();
                $form_group_id = requestsGroupModel::where($where)->get(['form_group_id'])->first()->form_group_id;
                $where_group = $where;
                $where_group['is_completed_by_manager'] = 1;

                if (requestsGroupModel::where($where_group)->get()->isNotEmpty()){
                    return back()->with('success','request completed');
                }

                requestsGroupModel::where($where)
                    ->update(
                        [
                            'is_completed_by_manager' => 1,
                            'is_completed' => 1,
                        ]
                    );

                if($role->inAdmin($request->user_id)){
                    $employee_id = User::where(['username'=>$validated['user_nik']])->get(['id']);
                    $manager = $user;

                    if($employee_id->isEmpty()){
                        $allowed_manager_key = ['vp', 'svp', 'dir'];
                        $employee_data = EmployeeModel::where('nik', $validated['user_nik'])->first();

                        $register_user = Sentinel::registerAndActivate([
                            'email'    => $employee_data->email,
                            'password' => now()->timestamp,
                            'username' => $validated['user_nik']
                        ]);

                        //assign staff role to user
                        if(!empty($employee_data->manager_id)){
                            $staff_role = UserRolesModel::where('slug','staff')->first();
                            $register_user->roles()->save($staff_role);
                        }

                        //assign head role to user
                        if(in_array(strtolower($employee_data->job_key_short), $allowed_manager_key)){
                            $staff_role = UserRolesModel::where('slug','atasan')->first();
                            $register_user->roles()->save($staff_role);
                        }
                        $employee_id = $register_user->id;
                    } else {
                        $employee_id = $employee_id[0]['id'];
                    }

                    $employee = User::find($employee_id);

                    $request_group = requestsGroupModel::where($where)->get()->first()->toArray();

                    $form_name = requestsModel::groupById[$request_group['form_group_id']];

                    $messages = [
                        'message_header' => '',
                        'message_body' => '<p>Congratulations on your Coaching/Mentoring completion!</p>
                            <p>As your mentor/coach: '.$manager->first_name.' '.$manager->last_name.' has submitted your mentoring/coaching report, now you are able to finish the Coaching/Mentoring process by clicking \'done\' <a href="'.url('mydev/trainee/my-trainee').'">here</a></p>
                            <p>
                                Be sure to share your development experience with us & give us 5-star ;) <br/>
                                See you on your next development opportunities!
                            </p>',
                        'message_date' => now()->timestamp,
                        'url' => '/mydev/trainee/my-trainee',
                        'type' => 'form request',
                        'subject' => 'Your Coaching/Mentoring Process is Almost Complete',
                        'form_group' => $form_name,
                        'to' => $employee->email
                    ];

                    $request_group = requestsGroupModel::where(['id'=>$request->request_group_id])->get(['form_group_id'])->first();
                    $trainee_point = traineeModel::where(['id'=>$request_group->form_group_id])->get(['point'])->first()->point;
                    $employee_point  = User::where(['id'=>$employee_id])->get(['point'])->first()->point + $trainee_point;

                    User::where(['id'=>$employee_id])->update(['point'=>$trainee_point]);
                    EmployeeModel::where(['nik'=>$request->user_nik])->update(['points'=>$trainee_point]);

                    $employee->notify(new Trainee($messages));

                    
                    // if(isset($employee->email)){
                    //      Mail::to($employee->email)->send(new TraineeMailNew($messages));
                    // }
                    
                    
                    

                    $messages['url'] = '/dashboard/trainee-request';
                    notify_admin($messages);
                } else {

                    $staff = User::find($user->id);
                    $manager = $user;

                    $request_group = requestsGroupModel::where($where)->get()->first()->toArray();

                    $form_name = requestsModel::groupById[$request_group['form_group_id']];
                    $messages = [
                        'message_header' => '',
                        'message_body' => 'Form report '.$form_name.' Completed by :'.$manager->first_name.' '.$manager->last_name,
                        'message_date' => now()->timestamp,
                        'url' => '/trainee/preview/'.$request_group['user_id'].'/'.$request_group['id'],
                        'type' => 'form request',
                        'subject' => 'Form report completed',
                        'form_group' => $form_name,
                        'to' => $staff->email
                    ];

                    $staff->notify(new Trainee($messages));
                    
                   
                    // if(isset($staff->email)){
                    //      Mail::to($staff->email)->send(new TraineeMailNew($messages));
                    // }
                    
                    

                    User::find($request_group['second_accepter'])->notify(new Trainee($messages));
                }
            }

            return back()->with('success','Completed');
        }

        return false;
    }

    public function importExcelTodb(Request $request){

        $file = $request->file('file_import');
        $array = Excel::toArray(new PAImport, $file);
        $result = [];
        $resultTitle = [];
        $resultValue = [];

        foreach ($array[0] as $key => $value) {
            if ($key!=0){
                $resultValue[] = $value;
            } else {
                $resultTitle[] = $value;
            }
        }

        $result["title"] = $resultTitle[0];
        $result["value"] = $resultValue;
        $resMappingImport = [];
        $end = [];

        foreach ($result["value"] as $keyVal => $value) {
            foreach ($result["title"] as $keyTitle => $title) {
                $resMappingImport[$keyVal][$title] = $value[$keyTitle];
            }
        }

        $users = Sentinel::check();
        $user_id = $users->id; //Administrator 
        $user_role = $users->role;
        $type_request =  $request->request_type;
        $is_submit = false;

        $step1 = [];
        $step2 = [];

        foreach ($resMappingImport as $key => $obj) {
            if ($type_request=="inhouse"){
                $validation = $this->validateInHouse($result["title"]);
                if ($validation=="good"){
                    $step1["form_group_id"] = "Inhouse";
                    $step1["user_id"] = $user_id;
                    $step1["users"] = $users;                 
                    $step1["training_ti_title"] = $obj["training_ti_title"];
                    $step1["training_ti_type_programs"] = $obj["training_ti_type_programs"];
                    $step1["training_ti_provider"] = $obj["training_ti_provider"];
                    $step1["training_obj_trainer_name"] = $obj["training_obj_trainer_name"];
                    $step1["training_obj_trainer_nik"] = (int) $obj["training_obj_trainer_nik"];
                    $step1["training_ti_cost"] = $obj["training_ti_cost"];
                    $step1["training_ti_start_date"] = $obj["training_ti_start_date"];
                    $step1["training_ti_end_date"] = $obj["training_ti_end_date"];
                    $this->submitStep($step1);

                    $step2["form_group_id"] = "Inhouse";
                    $step2["user_id"] = $user_id;
                    $step2["users"] = $users;
                    $step2["is_last"] = "true";
                    $step2["training_obj_description"] = $obj["training_obj_description"];
                    $step2["training_obj_venue"] = $obj["training_obj_venue"];
                    $step2["training_obj_employee_name"] = explode(",",$obj["training_obj_employee_name"]);
                    $step2["training_obj_employee_nik"] = explode(",",(int) $obj["training_obj_employee_nik"]);
                    $step2["training_obj_employee_position"] = explode(",",$obj["training_obj_employee_position"]);
                    $step2["training_obj_employee_competency"] = explode(",",$obj["training_obj_employee_competency"]);
                    if ($this->submitStep($step2)=="success"){
                        $is_submit = true;
                    }
                } else {
                    return response()->json($validation,200);           
                }
            } else if ($type_request=="project_assigment"){
                $validation = $this->validateProjectAssigment($result["title"]);
                if ($validation=="good"){
                    $users = UserModel::where("username",$obj["user_nik"])->first();
                    $role_leader = "Project Leader";
                    if ($users){
                        $step1["form_group_id"] = "Assignment";
                        $step1["user_id"] = $users->id;
                        $step1["users"] = $users;
                        $step1["is_last"] = "true";
                        $step1["user_role"] = $role_leader;
                        $step1["user_nik"] = (string)(int)$obj["assignment_pg_project_leader_nik"];
                        $step1["assignment_ci_employee_name"] = $users->first_name." ".$users->last_name;
                        $step1["assignment_ci_employee_nik"] = $users->nik;
                        $step1["assignment_dev_type"] = $obj["assignment_dev_type"];
                        $step1["assignment_obj_employee_competency"] = explode(",",$obj["assignment_obj_employee_competency"]);
                        $step1["assignment_pg_project_leader"] = $obj["assignment_pg_project_leader"];
                        $step1["assignment_pg_project_leader_nik"] = (string)(int)$obj["assignment_pg_project_leader_nik"];
                        $step1["assignment_pg_project_title"] = isset($obj["assignment_pg_project_title"]) ? $obj["assignment_pg_project_title"] : null;
                        $step1["assignment_pg_start_date"] = isset($obj["assignment_pg_start_date"]) ? $obj["assignment_pg_start_date"] : null;
                        $step1["assignment_pg_end_date"] = isset($obj["assignment_pg_end_date"]) ? $obj["assignment_pg_end_date"] : null;
                        $step1["assignment_pg_project_description"] = isset($obj["assignment_pg_project_description"]) ? $obj["assignment_pg_project_description"] : null;
                        $step1["assignment_pg_development_goal"] = isset($obj["assignment_pg_development_goal"]) ? $obj["assignment_pg_development_goal"] : null;
                        $step1["assignment_inter_group"] = isset($obj["assignment_inter_group"]) ? $obj["assignment_inter_group"] : null;
                        $step1["assignment_inter_division"] = isset($obj["assignment_inter_division"]) ? $obj["assignment_inter_division"] : null;
                        $step1["assignment_inter_start_date"] = isset($obj["assignment_inter_start_date"]) ? $obj["assignment_inter_start_date"] : null;
                        $step1["assignment_inter_end_date"] = isset($obj["assignment_inter_end_date"]) ? $obj["assignment_inter_end_date"] : null;
                        $step1["assignment_inter_mentor_name"] = $obj["assignment_pg_project_leader"];
                        $step1["assignment_inter_mentor_nik"] = (string)(int)$obj["assignment_pg_project_leader_nik"];
                        $step1["assignment_inter_development_goal"] = isset($obj["assignment_inter_development_goal"]) ? $obj["assignment_inter_development_goal"] : null;

                        $step1["assignment_gt_opco"] = isset($obj["assignment_gt_opco"]) ? $obj["assignment_gt_opco"] : null;
                        $step1["assignment_gt_type"] = isset($obj["assignment_gt_type"]) ? $obj["assignment_gt_type"] : null;
                        $step1["assignment_gt_start_date"] = isset($obj["assignment_gt_start_date"]) ? $obj["assignment_gt_start_date"] : null;
                        $step1["assignment_gt_end_date"] = isset($obj["assignment_gt_end_date"]) ? $obj["assignment_gt_end_date"] : null;
                        $step1["assignment_gt_purpose"] = isset($obj["assignment_gt_purpose"]) ? $obj["assignment_gt_purpose"] : null;
                        $step1["assignment_gt_reason"] = isset($obj["assignment_gt_reason"]) ? $obj["assignment_gt_reason"] : null;
                        $step1["assigment_gt_attachment"] = isset($obj["assignment_gt_attachment"]) ? $obj["assignment_gt_attachment"] : null;

                        if ($this->submitStep($step1)=="success"){
                            $is_submit = true;
                        }
                    }
                } else {
                    return response()->json($validation,200);           
                }
            } else if ($type_request=="mentoring"){
                $validation = $this->validateMentoring($result["title"]);
                if ($validation=="good"){
                    $users = UserModel::where("username",$obj["user_nik"])->first();
                    $role_mentor = "Mentor";
                    if ($users){
                        $step1["form_group_id"] = "Mentoring";
                        $step1["user_role"] = $role_mentor;
                        $step1["user_nik"] = (int) $obj["coaching_ci_line_manager_nik"];
                        $step1["user_id"] = $users->id;
                        $step1["users"] = $users;
                        $step1["coaching_ci_employee_name"] = $users->full_name;
                        $step1["coaching_ci_type"] = $obj["coaching_ci_type"];
                        $step1["coaching_ci_employee_nik"] = $users->nik;
                        $step1["coaching_ci_line_manager_name"] = $obj["coaching_ci_line_manager_name"];
                        $step1["coaching_ci_line_manager_nik"] = (int) $obj["coaching_ci_line_manager_nik"];
                        $step1["coaching_ci_start_date"] = $obj["coaching_ci_start_date"];
                        $step1["coaching_ci_end_date"] = $obj["coaching_ci_end_date"];
                        $this->submitStep($step1);

                        $step2["form_group_id"] = "Mentoring";
                        $step2["is_last"] = "true";
                        $step2["user_id"] = $users->id;
                        $step2["users"] = $users;
                        $step2["coaching_cc_development_goal"] = $obj["coaching_cc_development_goal"];
                        $step2["coaching_cc_development_mtioc"] = explode(",",$obj["coaching_cc_development_mtioc"]);
                        $fgroup_id = $this->submitStep($step2);
                        $stepReviewMentoring["user_nik"] = $obj["user_nik"];
                        $stepReviewMentoring["request_group_id"] = $fgroup_id;
                        $stepReviewMentoring["submit_button"] = "save";
                        $stepReviewMentoring["form_type"] = $obj["coaching_ci_type"]; 
                        $stepReviewMentoring["user_id"] = $users->id;
                        $stepReviewMentoring["reviews_date"] = explode(",",$obj["reviews_date"]);
                        $stepReviewMentoring["reviews_duration"] = explode(",",$obj["reviews_duration"]);
                        $stepReviewMentoring["reviews_goal"] = explode(",",$obj["reviews_goal"]);
                        $stepReviewMentoring["reviews_condition"] = explode(",",$obj["reviews_condition"]);
                        $stepReviewMentoring["reviews_action"] = explode(",",$obj["reviews_action"]);
                        $stepReviewMentoring["reviews_committed_date"] = explode(",",$obj["reviews_committed_date"]);
                        $stepReviewMentoring["reviews_result"] = explode(",",$obj["reviews_result"]);

                        if ($this->submitReview((object)$stepReviewMentoring)){
                            $is_submit = true;
                        }
                    }
                } else {
                    return response()->json($validation,200);           
                }
            } else if ($type_request=="public_training"){
                $validation = $this->validatePublicTraining($result["title"]);
                if ($validation=="good"){
                    $users = UserModel::where("username",$obj["user_nik"])->first();
                    if ($users){
                        $step1["user_id"] = $users->id;
                        $step1["users"] = $users; 
                        $step1["form_group_id"] = "Public Training";
                        $step1["training_ti_title"] = $obj["training_ti_title"];
                        $step1["training_obj_employee_name"] = explode(",",$users->full_name);
                        $step1["training_obj_employee_nik"] = explode(",",$users->nik);
                        $step1["training_obj_employee_position"] = explode(",",$users->position);
                        $step1["training_ti_type_programs"] = $obj["training_ti_type_programs"];
                        $step1["training_ti_start_date"] = $obj["training_ti_start_date"];
                        $step1["training_ti_end_date"] = $obj["training_ti_end_date"];
                        $step1["training_obj_employee_competency"] = explode(",",$obj["training_obj_employee_competency"]);
                        $step1["training_obj_description"] = $obj["training_obj_description"];
                        $step1["training_obj_commitment_training"] = explode(",",$obj["training_obj_commitment_training"]);
                        $step1["training_obj_commitment_description"] = $obj["training_obj_commitment_description"];
                        $step1["training_obj_start_date2"] = $obj["training_obj_start_date2"];
                        $step1["training_obj_end_date2"] = $obj["training_obj_end_date2"];
                        $this->submitStep($step1);

                        $step2["form_group_id"] = "Public Training";
                        $step2["is_last"] = "true";
                        $step2["user_id"] = $users->id;
                        $step2["users"] = $users; 
                        $step2["training_ti_provider"] = $obj["training_ti_provider"];
                        $step2["training_ti_cost"] = (int) $obj["training_ti_cost"];
                        $step2["training_obj_venue"] = $obj["training_obj_venue"];
                        if ($this->submitStep($step2)=="success"){
                            $is_submit = true;
                        }
                    }
                } else {
                    return response()->json($validation,200);           
                }
            } else if ($type_request=="external_speaker"){
                $validation = $this->validateExternalSpeaker($result["title"]);
                if ($validation=="good"){
                    $users = UserModel::where("username",$obj["user_nik"])->first();
                    if ($users){
                        $step1["form_group_id"] = "External Speaker";
                        $step1["is_last"] = "true";
                        $step1["user_role"] = $obj["user_role"];
                        $step1["user_nik"] = (int) $obj["user_nik"];
                        $step1["user_id"] = $users->id;
                        $step1["users"] = $users;
                        $step1["mentoring_ci_employee_name"] = $users->full_name;
                        $step1["mentoring_ci_employee_nik"] = $users->nik;
                        $step1["mentoring_ci_line_topic"] = $obj["mentoring_ci_line_topic"];
                        $step1["mentoring_ci_organization"] = $obj["mentoring_ci_organization"];
                        $step1["mentoring_ci_line_venue"] = $obj["mentoring_ci_line_venue"];
                        $step1["mentoring_ci_start_date"] = $obj["mentoring_ci_start_date"];
                        $step1["mentoring_ci_end_date"] = $obj["mentoring_ci_end_date"];
                        if ($this->submitStep($step1)=="success"){
                            $is_submit = true;
                        }
                    }
                } else {
                    return response()->json($validation,200);           
                }
            }
        }
        if ($is_submit){
            return response()->json("success",200);
        }
        return response()->json("something wrong",500);
    }

     private function submitStep($datas){
      
        $model = new requestsModel();
        $roleHelper = new UserRolesHelper();

        if (isset($datas['user_role'])) {
            $user_role = $datas['user_role'];
            $user_role_nik = $datas['user_nik'];
        }

        $is_last = false;
        if (isset($datas['is_last'])) {
            $is_last = true;
        }
        if (isset($datas['form_group_id'])) {
            $group_id = $datas['form_group_id'];
            $valid_datas = $this->filterReponses($datas,$datas['form_group_id']);
            $user = $datas["users"];
            $user_id = $datas["user_id"];
            $step = 1;

            foreach ($valid_datas as $key=>$data) {
                if (is_array($data)) {
                    foreach ($data as $keyItem=>$item) {
                        if(isset($datas[$item])) {
                            if(is_array($datas[$item])) {
                                $datas[$item] = serialize(array_unique($datas[$item]));
                            }

                            $input_data = [
                                'form_group_id' => $group_id,
                                'user_id' => $user_id,
                                'form_question' => $key.'_'.$keyItem,
                                'form_answer' => $item,
                                'requests_step' => $step,
                                'request_content' => $datas[$item]
                            ];
                            $requestGroup = $model->add($input_data);
                        }
                    }
                }else{
                    $input_data = [
                        'form_group_id' => $group_id,
                        'user_id' => $user_id,
                        'form_question' => $key,
                        'form_answer' => $data,
                        'requests_step' => $step
                    ];
                    $requestGroup = $model->add($input_data);
                }
                $step = $step+1;
            }
        }else{
            return false;
        }

        if($requestGroup) {
            $request_group_id = $requestGroup->requests_group_id;
        } else {
            $request_group_id = session('group_id');
        }

        if( $request_group_id && isset($datas['user_role']) ) {

            if(!is_array($user_role_nik) && is_integer($user_role_nik) ){

                $user_role_id = User::where(['username'=>$user_role_nik])->get();
                if ($user_role_id->isEmpty()){
                    $user_role_id = $this->registerUser($user_role_nik);
                } else {
                    $user_role_id = $user_role_id->toArray()[0]['id'];
                }
                

                $user_role_data = [
                    'user_id' => $user_role_id,
                    'user_position' => $user_role,
                    'requests_group_id' => $request_group_id
                ];
                $requester = User::where(['id'=>$user_id])->get(['first_name','last_name'])->first();

                $assigned = User::find($user_role_id);

                if($assigned){

                    $message = [
                        'message_body' => 'You have been registered as '.$user_role. ' by '.$requester['first_name'].' '.$requester['last_name'],
                        'message_header' => 'You have been registered',
                        'message_date' => now()->timestamp,
                        'url' => '/trainee/preview/'.$user_id.'/'.$request_group_id,
                        'type' => 'form request',
                        'subject' => 'You have been Registered as '.$user_role.' at from '.$group_id.' Form',
                        'form_group' => $group_id,
                        'to' => $assigned->email
                    ];

                    $assigned->notify(new Trainee($message));
                    
                    
                    // if(isset($assigned->email)){
                    //      Mail::to($assigned->email)->send(new TraineeMailNew($message));
                    // }
                    
                    

                }
                
                RequestsUserModel::updateOrCreate(
                    [
                        'user_id'=>$user_role_id,
                        'requests_group_id' => $request_group_id
                    ],
                    $user_role_data
                );
            }
        }

        if($is_last && $group_id == 'Inhouse'){
            $employee_data = User::where(['username'=>$datas['training_obj_employee_nik']])->first();

            if($employee_data){
                $user_id = $employee_data->id;    
            }
        }
        
        if(isset($datas['coaching_ci_line_manager_nik']) && $datas['form_group_id'] != 'Public Training' ){
            // assign or remove mentor from requests
            $this->assignUsers($datas['coaching_ci_line_manager_nik'],'Mentor',$request_group_id,$group_id,$user_id,$user);
        }

        if(isset($datas['training_obj_employee_name']) && $datas['form_group_id'] != 'Public Training') {
            //assign or remove and notify user
            $this->assignUsers($datas['training_obj_employee_nik'],'Employee',$request_group_id,$group_id,$user_id,$user);
        }

        if(isset($datas['training_obj_trainer_name']) && $datas['form_group_id'] != 'Public Training') {
            //assign or remove and notify user as trainer
            $this->assignUsers($datas['training_obj_trainer_nik'],'Trainer',$request_group_id,$group_id,$user_id,$user);
        }



        if($is_last) {
            if (session('edit_form')) {
                //notify owner that form has been editted by admin
                if(!$roleHelper->inAdmin($datas['user_id'])){
                    $this->notifyEditedForm($user_id,$user,$group_id);
                }
                session()->flash('success','success');
            }
            else {
                // notify admin and manager that form request has been submitted
                $userPosition = $roleHelper->getLeaders($user_id);
                $first_atasan_id = null;

                $admin_id = User::where(['username'=>'Administrator'])->get()->first()->id;

                $second_atasan_id = $admin_id;

                $requestGroupModel = new requestsGroupModel();

                if($userPosition['user_type'] == 'dir') {
                    $requestGroupModel->where('id',$request_group_id)->update(['first_accepted'=>now()]);
                } elseif ($userPosition['user_type'] == 'svp') {
                    $first_atasan_id = $userPosition['dir']['nik'];
                }elseif ($userPosition['user_type'] == 'vp') {
                    $first_atasan_id = $userPosition['svp']['nik'];
                }else {
                    if(!empty( $userPosition['vp'])){
                        $first_atasan_id = $userPosition['vp']['nik'];
                    }elseif(!empty( $userPosition['svp'])){
                        $first_atasan_id = $userPosition['svp']['nik'];
                    }elseif(!empty( $userPosition['dir'])){
                        $first_atasan_id = $userPosition['dir']['nik'];
                    }
                }

                $user_model = new User();
                if (!$roleHelper->inAdmin($datas['user_id'])){
                    $atasan = $user_model->where(['username'=>$first_atasan_id])->get();
                    if($atasan->isEmpty()) {
                        $first_atasan_id = $this->registerUser($first_atasan_id);
                    } else {
                        $first_atasan_id = $atasan->first()->id;
                    }

                    $first_atasan = $user_model->find($first_atasan_id);
                    $second_atasan = $user_model->find($second_atasan_id);
                    $user_data = $user_model->find($user_id);

                    $message1 = [
                        'message_body' => 'You have '.$group_id.' form request from '.$user->first_name.' '.$user->last_name,
                        'message_header' => 'You have '.$group_id.' form request',
                        'message_date' => now()->timestamp,
                        'url' => '/trainee/preview/'.$user_id.'/'.$request_group_id,
                        'type' => 'form manage request',
                        'subject' => 'Trainee Form Requests',
                        'form_group' => $group_id,
                        'to' => $first_atasan->email
                    ];

                    $message2 = [
                        'message_body' => 'You have '.$group_id.' form request from '.$user->first_name.' '.$user->last_name,
                        'message_header' => 'You have '.$group_id.' form request',
                        'message_date' => now()->timestamp,
                        'url' => '/trainee/preview/'.$user_id.'/'.$request_group_id,
                        'type' => 'form manage request',
                        'subject' => 'Trainee Form Requests',
                        'form_group' => $group_id,
                        'to' => $second_atasan->email
                    ];

                    $message3 = [
                        'message_body' => '
                            <p>Congrats! You’ve successfully conquer the first step for your development. 2 more steps to go:</p>
                            <p>
                                <ul>
                                    <li>Get approval from your Line Manager</li>
                                    <li>Get approval from MyDev Admin</li>
                                </ul>
                            </p>
                            <p>This process won’t take long, we promise. So, please be patient & keep your development spirit high!</p>',
                        'message_header' => '',
                        'message_date' => now()->timestamp,
                        'url' => '/trainee/preview/'.$user_id.'/'.$request_group_id,
                        'type' => 'form manage request',
                        'subject' => 'Your Development Request Has Been Submitted',
                        'form_group' => $group_id,
                        'to' => $user_data->email
                    ];

                    $first_atasan->notify(new Trainee($message1));
                    $second_atasan->notify(new Trainee($message2));
                    $user_data->notify(new Trainee($message3));

                    
                    
                    //Mail::to($first_atasan->email)->send(new TraineeMailNew($message1));
                    //Mail::to($second_atasan->email)->send(new TraineeMailNew($message2));
                    //Mail::to($user_data->email)->send(new TraineeMailNew($message3));
                    
                    
                    
                }
                if($user->id==5){
                    //log for admin requests
                    $this->activity->logActivity($user,['log_type'=>'Create Trainee','log_data'=>['ip_address'=>\Request::getClientIp(true),'badge_class'=>'badge-info','status'=>'created','date'=>now()]],'You has been created '.$group_id.' trainee');
                } else {
                    $this->activity->logActivity($user,['log_type'=>'Trainee Request','log_data'=>['ip_address'=>\Request::getClientIp(true),'badge_class'=>'badge-info','status'=>'requested','date'=>now()]],'You has been requested for '.$group_id.' trainee request');
                }
            }

            requestsGroupModel::where(['id'=>$request_group_id])->update(['is_completed'=>'1','is_completed_by_staff'=>'1','first_accepter'=>'1','second_accepter'=>'1']);
            
            requestsGroupModel::where(['id'=>$request_group_id])->update(['published_at'=>date('Y-m-d H:i:s')]);

            session()->forget('group_id');
            // if($user_id != 5){
            //    UserModel::where('id', $user_id)->update(['is_block' => 0]);
            // }
        }

        if (isset($datas['form_group_id']) && $datas['form_group_id']=="Mentoring"){
            return $request_group_id;       
        } else {
            return "success";
        }
    }

    private function assignUsers($nik_user,$position,$request_group_id,$group_id,$user_id,$user,$new_users = null){
        if(@unserialize($nik_user)){
            $employee_nik = array_unique(unserialize($nik_user));
        } else {
            $employee_nik = [$nik_user];
        }
        
        foreach ($employee_nik as $value => $nik) {
            $employee_id = User::where(['username'=>$nik])->get(['id'])->first();

            if(!$employee_id) {

                $employee_id = $this->registerUser($nik);

            } else {
                $employee_id = $employee_id->id;
            }
            RequestsUserModel::updateOrCreate(
                [
                    'user_id' => $employee_id,
                    'requests_group_id' => $request_group_id,
                    'user_position' => $position
                ],
                [
                'user_id' => $employee_id,
                'requests_group_id' => $request_group_id,
                'user_position' => $position
                ]
            );

            if(RequestsUserModel::where([
                'user_id' => $employee_id,
                'requests_group_id' => $request_group_id,
                'user_position' => $position
            ])->get(['id'])->first() && (session('edit_form'))) {
                $employee_notice = User::find($employee_id);
                    $message = [
                        'message_body' => $group_id.' Has edited by '.$user->first_name.' '.$user->last_name,
                        'message_header' => $group_id.' Has edited',
                        'message_date' => now()->timestamp,
                        'url' => '/trainee/preview/'.$user_id.'/'.$request_group_id,
                        'type' => 'form manage request',
                        'subject' => 'Trainee Form Has Been Edited',
                        'form_group' => $group_id,
                        'to' => $employee_notice->email
                    ];
                $employee_notice->notify(new Trainee($message));

                
                
                //Mail::to($employee_notice->email)->send(new TraineeMailNew($message));
                
                

            } else {
                $user_data = User::find($user_id); 
                $employee_notice = User::find($employee_id);
                $message = [
                    'message_body' => '<p>Congrats '.$employee_notice->first_name.' '.$employee_notice->last_name.'!</p>
                        <p>You are choosen as a '.$position.' to '.$user_data->first_name.' '.$user_data->last_name.'. You can preview the '.$group_id.' request <a href="'.url('trainee/preview/'.$user_id.'/'.$request_group_id).'">here</a>.</p>
                        <p>We are glad to have your support in developing Sahabat Indosat Ooredoo & also thankful for your participation. Let’s start your '.$group_id.' discussion with your coachee/mentee.</p>',
                    'message_header' => 'You have assigned',
                    'message_date' => now()->timestamp,
                    'url' => '/trainee/preview/'.$user_id.'/'.$request_group_id,
                    'type' => 'form manage request',
                    'subject' => 'Time to Share Your Knowledge with Others',
                    'form_group' => $group_id,
                    'to' => $employee_notice->email
                ];

                $employee_notice->notify(new Trainee($message));
                
                
                //Mail::to($employee_notice->email)->send(new TraineeMailNew($message));
                
                
            }
        }

        //begin checking duplicated, new and removed users
        $current_user_ids = RequestsUserModel::where([
            'requests_group_id' => $request_group_id,
            'user_position' => $position
        ])
            ->select('user_id as id')
            ->get()->toArray();
        if(@unserialize($nik_user)){
            $new_users_id = User::whereIn('username',unserialize($nik_user))->get(['id'])->toArray();

        } else{
            $new_users_id = User::whereIn('username',[$nik_user])->get(['id'])->toArray();
        }
        $temp1 = [];
        $temp2 = [];
//            $current_user_ids = array_unique($current_user_ids);
        foreach ($current_user_ids as $current){
            if(!in_array($current['id'],$temp1)){
                $temp1[] = $current['id'];
            }
        }

        foreach ($new_users_id as $new) {
            $temp2[] = $new['id'];
        }
        $removed_users = (array_diff($temp1,$temp2));
        if(count($removed_users) > 0){
            foreach ($removed_users as $employee_id){

                if(ratingModel::where([
                    'staff_id' => $employee_id,
                    'request_group_id' => $request_group_id
                ])->get(['id'])->first()){
                    //remove rating user if exist
                    ratingModel::where([
                        'staff_id' => $employee_id,
                        'request_group_id' => $request_group_id
                    ])->delete();
                }

                //remove user from trainee
                RequestsUserModel::where([
                    'user_id' => $employee_id,
                    'requests_group_id' => $request_group_id,
                ])->delete();
                //if user removed
                $employee_notice = User::find($employee_id);
                $message = [
                    'message_body' =>'You have been removed from '. $group_id.' by '.$user->first_name.' '.$user->last_name,
                    'message_header' => 'You have been removed',
                    'message_date' => now()->timestamp,
                    'url' => '/trainee/preview/'.$user_id.'/'.$request_group_id,
                    'type' => 'form manage request',
                    'subject' => 'You have been Removed from Trainee Form',
                    'form_group' => $group_id,
                    'to' => $employee_notice->email
                ];
                $employee_notice->notify(new Trainee($message));

                
                
                //Mail::to($employee_notice->email)->send(new TraineeMailNew($message));
                
                
            }
        }
        //end checking duplicated, new and removed users
    }

    private function registerUser($nik) {
        $allowed_manager_key = ['vp', 'svp', 'dir'];
        $employee_data = EmployeeModel::where('nik', $nik)->first();
        if(!$employee_data){
            return back()->withMessage('Employee data with NIK : '.$nik.' not found');
        }
        $user_role_id = Sentinel::registerAndActivate([
            'email'    => $employee_data->email,
            'password' => now()->timestamp,
            'username' => $employee_data->nik
        ]);

        //assign staff role to user
        if(!empty($employee_data->manager_id)){
            $staff_role = UserRolesModel::where('slug','staff')->first();
            $user_role_id->roles()->save($staff_role);
        }

        //assign head role to user
        if(in_array(strtolower($employee_data->job_key_short), $allowed_manager_key)){
            $staff_role = UserRolesModel::where('slug','atasan')->first();
            $user_role_id->roles()->save($staff_role);
        }

        return $user_role_id->id;
    }

}
