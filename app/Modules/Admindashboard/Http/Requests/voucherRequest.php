<?php

namespace App\Modules\Admindashboard\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class voucherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
//        return Auth::check();
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(request()->has('id')){
            return [
                'name' => 'required|max:300|unique:voucher,name,{$id},id,deleted_at,null',
                'amount' => 'required|integer',
                'point' => 'required|integer',
                'code' => 'mimes:csv,txt'
            ];
        }else{
            return [
                'name' => 'required|max:300|unique:voucher,name,{$id},id,deleted_at,null',
                'amount' => 'required|integer',
                'point' => 'required|integer',
                'code' => 'required|mimes:csv,txt'
            ];
        }
    }

    public function messages()
    {
        return [
        ];
    }
}
