<?php

namespace App\Modules\Admindashboard\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreReviews extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
//        return Auth::check();
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'review_id.*' => 'nullable',
            'user_id' => 'required|integer',
            'request_group_id' => 'required|integer',
            'form_type' => 'required|max:30',
            'reviews_course_name.*' => 'required|max:500',
            'reviews_session_number.*' => 'required',
            'reviews_duration.*' => 'required|max:30',
            'reviews_date.*' => 'required',
            'reviews_goal.*' => 'required|max:500',
            'reviews_result.*' => 'required|max:500',
            'reviews_feedback.*' => 'required|max:500',
            'reviews_action.*' => 'max:500',
            'reviews_condition.*' => 'required',
            'reviews_committed_date.*' => 'required',
            'reviews_progress.*' => 'required',
            'reviews_end_date.*' => 'required',
            'reviews_project_title.*' => 'required',
            'reviews_criteria.*' => 'required',
            'reviews_provider.*' => 'required',
            'reviews_cost.*' => 'required',
            'reviews_location.*' => 'required',
            'reviews_country.*' => 'required',
            'reviews_topic.*' => 'required',
            'reviews_attachment.*' => 'mimes:pdf,doc,docx,jpeg,jpg,png'
        ];
    }

    public function messages()
    {
        return [
            'user_id' => 'This field is required',
            'request_group_id' => 'This field is required',
            'form_type' => 'This field is required and max use 30 chars',
            'reviews_course_name.*' => 'This field is required and max use 500 chars',
            'reviews_session_number.*' => 'This field is required',
            'reviews_duration.*' => 'This field is required and max use 30 chars',
            'reviews_date.*' => 'This field is required',
            'reviews_goal.*' => 'This field is required and max use 500 chars',
            'reviews_result.*' => 'This field is required and max use 500 chars',
            'reviews_feedback.*' => 'This field is required and max use 500 chars',
            'reviews_action.*' => 'Maximum 500 Characters',
            'reviews_condition.*' => 'This field is required',
            'reviews_committed_date.*' => 'This field is required',
            'reviews_progress.*' => 'This field is required',
            'reviews_end_date.*' => 'This field is required',
            'reviews_project_title.*' => 'This field is required',
            'reviews_criteria.*' => 'This field is required',
            'reviews_provider.*' => 'This field is required',
            'reviews_cost.*' => 'This field is required',
            'reviews_location.*' => 'This field is required',
            'reviews_country.*' => 'This field is required',
            'reviews_topic.*' => 'This field is required',
            'reviews_attachment.*' => 'Format file must pdf,doc,docx,jpeg,jpg or png'
        ];
    }
}
