<?php

namespace App\Modules\Admindashboard\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class importGroupModel extends Model
{
    //Request group adalah pengelompokan dari request yang diajukan oleh user
    use SoftDeletes;

    protected $table = 'requests_group';
    protected $dates = ['deleted_at'];
    protected $hidden = ['created_at','updated_at','deleted_at'];
    protected $fillable = ['published_at','user_id','form_group_id','last_step','is_completed','is_accepted','first_accepter','second_accepter','requests_step','is_completed_by_staff','is_completed_by_admin','is_completed_by_manager'];

    function addRequest($data) {
        if (!isset($data['user_id']) || !isset($data['form_group_id'])){
            return false;
        }
        $data['is_completed'] = true;
        return $this->create($data)->id;

    }

    function isExists($where){
        return $this
            ->where($where)
            ->get()->isNotEmpty();
    }

    public function acceptRequest($accepter_id,$where,$is_hr = true) {
        $data = $this
            ->where($where)
            ->get();
        if ($data->isEmpty()) {
            return false;
        } else {
            if ($is_hr) {
                $update = [
                    'second_accepter'=>$accepter_id,
                    'second_accepted' => Carbon::now()->toDateTimeString(),
                    'is_accepted' => $data[0]->first_accepter ? 1 : 0
                ];
                $this->where($where)->update($update);
            } else {
                $update = [
                    'first_accepter'=>$accepter_id,
                    'first_accepted' => Carbon::now()->toDateTimeString(),
                    'is_accepted' => $data[0]->second_accepter ? 1 : 0
                ];
                $this->where($where)->update($update);
            }
            return true;
        }
    }
    public function rejectRequest($rejecter_id,$where,$notes) {
        $data = $this
            ->where($where)
            ->get();
        if ($data->isEmpty()) {
            return false;
        } else {
            $update = [
                'rejected_by' => $rejecter_id,
                'is_rejected' => 1,
                'rejected_notes' => $notes
            ];
            $this->where($where)->update($update);
            return true;
        }
    }
}
