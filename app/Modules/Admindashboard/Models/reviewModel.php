<?php

namespace App\Modules\Admindashboard\Models;

use Illuminate\Database\Eloquent\Model;

class reviewModel extends Model
{
    //
    protected $table = 'reviews';
    protected $fillable = [
        'user_id','request_group_id','form_type','reviews_goal','reviews_result','reviews_feedback','reviews_action','reviews_date',
        'reviews_course_name','reviews_session_number','reviews_duration','reviews_list_action',
        'reviews_condition','reviews_committed_date','reviews_progress','reviews_end_date',
        'reviews_project_title','reviews_criteria','reviews_provider','reviews_cost','reviews_location',
        'reviews_country','reviews_topic','reviews_attachment','flagging'];
    protected $primaryKey = 'id';
}
