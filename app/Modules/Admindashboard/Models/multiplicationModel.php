<?php

namespace App\Modules\Admindashboard\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Trainee\Models\traineeModel;

class multiplicationModel extends Model
{
    //
   
    protected $table = 'mst_multiplication_point';
    protected $fillable = [
        'id','trainee_id','tier_5','tier_4','tier_3','tier_2_1','information'];

    protected $primaryKey = 'id';

/*
    public function get_point()
    {
    	return $this->hasMany('App\pointModel');
    }
    */

    public function get_trainee()
    {
    	return $this->hasOne('App\Modules\Trainee\Models\traineeModel','id','trainee_id');
    }
}
