<?php

namespace App\Modules\Admindashboard\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class voucherModel extends Model
{
    //

    use SoftDeletes;

    protected $table = 'voucher';
    protected $fillable = [
        'name','description','amount','point','status', 'image'];

    protected $primaryKey = 'id';
}
