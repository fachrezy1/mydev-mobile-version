<?php

namespace App\Modules\Admindashboard\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class itemRedeemModel extends Model
{
    //
    use SoftDeletes;

    protected $table = 'item_redeem';
    protected $fillable = [
        'user_id','item_id','voucher_code','approved','point_from','point_to'];

    protected $primaryKey = 'id';
}
