<?php

namespace App\Modules\Admindashboard\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class pointModel extends Model
{
    //
    use SoftDeletes;

    protected $table = 'trainees_point';
    protected $fillable = [
        'trainee_id','point'];

    protected $primaryKey = 'id';
}
