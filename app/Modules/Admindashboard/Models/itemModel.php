<?php

namespace App\Modules\Admindashboard\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class itemModel extends Model
{
    //

    use SoftDeletes;

    protected $table = 'item';
    protected $fillable = [
        'name','description','point','picture','status', 'quota'];

    protected $primaryKey = 'id';
}
