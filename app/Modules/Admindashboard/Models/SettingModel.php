<?php

namespace App\Modules\Admindashboard\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SettingModel extends Model
{
    //

    use SoftDeletes;

    protected $table = 'setting';
    protected $fillable = [
        'setting_key','setting_value','setting_group','admin_id'];

    protected $primaryKey = 'id';
}
