<?php

namespace App\Modules\Admindashboard\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class voucherCodeModel extends Model
{
    protected $table = 'voucher_codes';
    protected $fillable = [
        'voucher_id','user_id','code'];


    public function user()
    {
        return $this->belongsTo('App\Modules\Auth\Models\UserModel','user_id','id');
    }
}
