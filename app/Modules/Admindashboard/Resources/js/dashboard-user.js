jQuery(function($){
  $('.trainee-pick').owlCarousel({
    autoplay:true,
    margin:0,
    nav:false,
    dot: true,
    responsive:{
      0:{
        items:1
      },
      600:{
        items:1
      },
      1000:{
        items:1
      }
    }
  })



  var optionsOne = {
    type: 'pie',

    data: {
      labels: ["Green", "Blue"],
      datasets: [{
        backgroundColor: [
          "#2ecc71",
          "#3498db"
        ],
        data: [19, 12]
      }]
    },
    options: {
      legend: {
        display: false
      },
      responsive: true
    }
  }


  var optionsTwo = {
    type: 'line',
    data: {
      labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
      datasets: [
        {
          label: 'Training #1',
          data: [12, 19, 3, 5, 2, 3],
          borderWidth: 1
        },
        {
          label: 'Training #2',
          data: [7, 11, 5, 8, 3, 7],
          borderWidth: 1
        }
      ]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            reverse: false
          }
        }]
      }
    }
  }



  var ctxOne = document.getElementById('chartOneContainer').getContext('2d');
  new Chart(ctxOne, optionsOne);

  var ctxTwo = document.getElementById('chartTwoContainer').getContext('2d');
  new Chart(ctxTwo, optionsTwo);







});

