@php($user = \Sentinel::check())
@php($helper = new \App\Helpers\UserRolesHelper )
@php($agent = new \Jenssegers\Agent\Agent())

@extends($helper->inAdmin() ? 'core.admin.main' :'core.trainee.main')
@section('head')
    <link rel="stylesheet" href="{{url('css/admin/dashboard/app.css')}}">
@endsection
@section('content')
    <main class="main-content">
        <div class="main-dashboard">
            <div class="dashboard-content {{!$helper->inStaff() ? 'w-100' : ''}}">
                <div class="container">

                    <div class="dashboard-entry">
                        <div class="dashboard-title">
                            <h1></h1>
                        </div>
                        <i class="icon-exam"></i> You have <span>{{$requests['approvedCount']}} courses</span>
                        to review. <a href="#myTrainee" class="customTabToggler" >Click here</a>
                        @if($agent->isMobile())
                        <ul class="nav nav-pills nav-pills-icons mt-24" role="tablist">
                            <li class="nav-item col-12 inline">
                                <a class="nav-link active show" href="#myTrainee" role="tab" data-toggle="tab" aria-selected="true">
                                    <i class="icon-portfolio-1"></i> On Progress
                                </a>
                            </li>

                            <li class="nav-item col-12 inline">
                                <a class="nav-link" href="#myHistory" role="tab" data-toggle="tab" aria-selected="false">
                                    <i class="icon-profiles"></i> Completed
                                </a>
                            </li>
                        </ul>
                        @else
                            <ul class="nav nav-pills nav-pills-icons mt-24" role="tablist">
                                <li class="nav-item inline">
                                    <a class="nav-link active show" href="#myTrainee" role="tab" data-toggle="tab" aria-selected="true">
                                        <i class="icon-portfolio-1"></i> On Progress
                                    </a>
                                </li>

                                <li class="nav-item inline">
                                    <a class="nav-link" href="#myHistory" role="tab" data-toggle="tab" aria-selected="false">
                                        <i class="icon-profiles"></i> Completed
                                    </a>
                                </li>
                            </ul>
                        @endif
                        <div class="tab-content tab-space">
                            <div class="tab-pane active show" id="myTrainee">
                                <div class="panel-box mt-24">
                                    <div class="panel-title">
                                        <h3>Waiting for approval List</h3>
                                        <div class="right-notif">
                                            @if ((isset($requests['approved'])) && $helper->inStaff() && $requests['approvedCount'] > 0)
                                                <div class="review-notif">
                                                    
                                                </div>
                                            @endif

                                            @if ($helper->inStaff())
                                                <button type="button" class="btn btn-default" onclick="location.href='{{url(route('trainee.index'))}}'">
                                                    <i class="icon-plus"></i> Create Trainee
                                                </button>
                                            @endif

                                            @if ($helper->inAdmin())
                                                <button type="button" class="btn btn-default" onclick="location.href='{{url(route('trainee.index'))}}'">
                                                    <i class="icon-plus"></i> Create Trainee
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="panel-table">
                                        {{--<div class="table-option">--}}
                                            {{--<div class="table-search">--}}
                                                {{--<label class="control-label">Search data</label>--}}
                                                {{--<input id="searchTable" type="text" class="form-control reset" placeholder="enter keywords">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                        <table class="traineeTable table table-hover table-striped table-lite" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Topic/Title</th>
                                                <th>Type</th>
                                                <th>Employee Name</th>
                                                <!--<th>Line Manager</th>-->
                                                <th>Start</th>
                                                <th>End</th>
                                                @if ($helper->inStaff())
                                                    <th>Manager</th>
                                                    <th>HR.</th>
                                                @endif
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @php ($n=1)
                                            @if(isset($requests['new']) && !is_null($requests['new']))
                                                @foreach($requests['new'] as $key=>$item)
                                                    <tr>
                                                        <td>{{$n}}</td>
                                                        <td>{{ isset($item['title']) ? limit_words($item['title'], 19) : ''}}</td>
                                                        <td>{{ucfirst(str_replace('_',' ',$form_type[$item['form_group_id']]))}}</td>
                                                        <td>{{$item['employee_name'] }}</td>
                                                        <!--<td>{{ isset($line_manager) ? $line_manager : ''}}</td>-->
                                                        <td>{{isset($item['start_date']) ? $item['start_date'] : ''}}</td>
                                                        <td>{{isset($item['end_date']) ? $item['end_date'] : ''}}</td>

                                                        @if ($helper->inStaff())
                                                            <td>
                                                                    <span class="badge badge-pill {{ isset($item['first_accepter_class']) ? $item['first_accepter_class'] : ''}}">
                                                            {{ isset($item['first_accepter']) ? $item['first_accepter'] : ''}}</span>
                                                            </td>
                                                            <td>
                                                                    <span class="badge badge-pill {{ isset($item['second_accepter_class']) ? $item['second_accepter_class'] : ''}}">
                                                            {{ isset($item['second_accepter']) ? $item['second_accepter'] : ''}}</span>
                                                            </td>
                                                        @endif

                                                        <td >
                                                            <div class="table-option">
                                                                <a href="{{route('trainee.export',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}" target="_blank">
                                                                    <button type="button" class="btn btn-default btn-sm btn-lite">
                                                                        <i class="icon-transfer-data-between-documents"></i> Export
                                                                    </button>
                                                                </a>
                                                                @if($item['is_accepted'] == 1)
                                                                    {{--<a href="{{route('trainee.review',$item['request_group_id'])}}">--}}
                                                                        {{--<button type="button" class="btn btn-default btn-sm btn-lite">--}}
                                                                            {{--<i class="icon-edit-1"></i> Review--}}
                                                                        {{--</button>--}}
                                                                    {{--</a>--}}
                                                                @endif
                                                                <a href="{{route('trainee.previewByUserId',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}">
                                                                    <button type="button" class="btn btn-default btn-sm btn-lite">
                                                                        <i class="icon-contract-1"></i> Preview
                                                                    </button>
                                                                </a>
                                                                @if($item['form_group_id'] == 6)
                                                                    <a href="{{route('trainee.reviewByUserId',['user_id'=>$item['user_id'],'id'=>$item['request_group_id']])}}">
                                                                        <button type="button" class="btn btn-default btn-sm btn-lite">
                                                                            <i class="icon-contract-1"></i> Result
                                                                        </button>
                                                                    </a>
                                                                @endif

                                                                <a href="{{route('dashboard.trainee.edit.show',['type'=>strtolower($form_type[$item['form_group_id']]),'user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}">
                                                                    <button type="button" class="btn btn-default btn-sm btn-lite">
                                                                        <i class="icon-edit-1"></i> Edit
                                                                    </button>
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @php($n=$n+1)
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="myHistory">
                                <div class="panel-box mt-24">
                                    <div class="panel-title">
                                        <h3>My Trainee List</h3>
                                        <div class="right-notif">
                                            @if ((isset($requests['approved'])) && $helper->inStaff() && ['approvedCount'] > 0)
                                                <div class="review-notif">
                                                    <i class="icon-exam"></i> You have got

                                                    <span>{{$requests['approvedCount']}} course(s)</span>
                                                    need to add review. <a href="#myApproval" class="customTabToggler" >Click here</a>
                                                </div>
                                            @endif

                                            @if ($helper->inStaff())
                                                <button type="button" class="btn btn-default" onclick="location.href='{{url(route('trainee.index'))}}'">
                                                    <i class="icon-plus"></i> Create Trainee
                                                </button>
                                            @endif

                                            @if ($helper->inAdmin())
                                                <button type="button" class="btn btn-default" onclick="location.href='{{url(route('trainee.index'))}}'">
                                                    <i class="icon-plus"></i> Create Trainee
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="panel-table">
                                        {{--<div class="table-option">--}}
                                            {{--<div class="table-search">--}}
                                                {{--<label class="control-label">Search data</label>--}}
                                                {{--<input id="searchTable" type="text" class="form-control reset" placeholder="enter keywords">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                        <table class="table table-hover table-striped table-lite traineeTable" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Topic/Title</th>
                                                <th>Type</th>
                                                <th>Employee Name</th>
                                                <!--<th>Line Manager</th>-->
                                                <th>Start</th>
                                                <th>End</th>
                                                @if ($helper->inStaff())
                                                    <th>HR.</th>
                                                    <th>Manager</th>
                                                @endif
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @php ($n=1)
                                            @if(isset($requests['completed']) && !is_null($requests['completed']))
                                                @foreach($requests['completed'] as $key=>$item)

                                                    <tr>
                                                        <td>{{$n}}</td>
                                                        <td>{{ isset($item['title']) ? limit_words($item['title'], 19) : ''}}</td>
                                                        <td>{{ucfirst(str_replace('_',' ',$form_type[$item['form_group_id']]))}}</td>
                                                        <td>{{$item['employee_name'] }}</td>
                                                        <!--<td>{{ isset($line_manager) ? $line_manager : ''}}</td>-->
                                                        <td>{{isset($item['start_date']) ? $item['start_date'] : ''}}</td>
                                                        <td>{{isset($item['end_date']) ? $item['end_date'] : ''}}</td>

                                                        @if ($helper->inStaff())
                                                            <td>
                                                                    <span class="badge badge-pill {{ isset($item['first_accepter_class']) ? $item['first_accepter_class'] : ''}}">
                                                            {{ isset($item['first_accepter']) ? $item['first_accepter'] : ''}}</span>
                                                            </td>
                                                            <td>
                                                                    <span class="badge badge-pill {{ isset($item['second_accepter_class']) ? $item['second_accepter_class'] : ''}}">
                                                            {{ isset($item['second_accepter']) ? $item['second_accepter'] : ''}}</span>
                                                            </td>
                                                        @endif
                                                        <td >
                                                            <div class="table-option">

                                                                <a href="{{route('trainee.export',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}" target="_blank">
                                                                    <button type="button" class="btn btn-default btn-sm btn-lite">
                                                                        <i class="icon-transfer-data-between-documents"></i> Export
                                                                    </button>
                                                                </a>

                                                                @if($item['form_group_id'] == 2)
                                                                    <a href="{{route('trainee.review',$item['request_group_id'])}}">
                                                                        <button type="button" class="btn btn-default btn-sm btn-lite">
                                                                            <i class="icon-edit-1"></i> See Result
                                                                        </button>
                                                                    </a>
                                                                @endif

                                                                <a href="{{route('trainee.previewByUserId',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}">
                                                                    <button type="button" class="btn btn-default btn-sm btn-lite">
                                                                        <i class="icon-contract-1"></i> Preview
                                                                    </button>
                                                                </a>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @php($n=$n+1)
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @if (!$helper->inAdmin())
                                @include('admindashboard::history.index')
                            @endif
                        </div>

                    </div>

                </div>
                <button type="button" class="btn btn-float btn-fba">
                    <i class="mdi mdi-more"></i>
                </button>
            </div>

            @if (!$helper->inAdmin())
                @include('sidebar')
            @endif
        </div>
    </main>
    @include('trainee-modal')
    <div role="alert" aria-live="assertive" aria-atomic="true" class="toast mentor_toast d-none" data-autohide="false">
        <div class="toast-header">
            {{--<img src="..." class="rounded mr-2" alt="...">--}}
            <strong class="mr-auto">Mydev</strong>
            {{--<small>11 mins ago</small>--}}
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body">
            Mentor need to complete report first
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('.toast_mentor_trigger').on('click',function(){
            $('.mentor_toast').toast('show');
        });
        $('.customTabToggler').click(function () {

            console.log($(this).attr('href'),'sini');
            $('.tab-pane')
                .removeClass('active')
                .removeClass('show');
            $($(this).attr('href')).addClass('active').addClass('show');
            $('.nav-link')
                .removeClass('active')
                .removeClass('show');
            $('[href="'+$(this).attr('href')+'"]').addClass('active').addClass('show');
        })
    </script>
@endsection
