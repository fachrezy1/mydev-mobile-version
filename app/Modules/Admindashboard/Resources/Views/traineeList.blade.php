@php($user = \Sentinel::check())
@php($helper = new \App\Helpers\UserRolesHelper )
@php($agent = new \Jenssegers\Agent\Agent())

@extends($helper->inAdmin() ? 'core.admin.main' :'core.trainee.main')
@section('head')
    <link rel="stylesheet" href="{{url('css/admin/dashboard/app.css')}}">
@endsection
@section('content')
    <main class="main-content">
        <div class="main-dashboard secondary-nav">
            <div class="dashboard-content {{!$helper->inStaff() ? 'w-100' : ''}}">
                <div class="container">

                    <div class="dashboard-entry">
                        <div class="dashboard-title">
                            <h1>Trainee List</h1>
                        </div>

                        <div class="my-4 px-2">
                        	<i class="icon-exam"></i> You have <span>{{$requests['approvedCount']}} courses</span> to review. <a href="#myTrainee" class="customTabToggler" >Click here</a>
                        </div>
                        
                        <ul class="nav nav-pills nav-pills-icons" id="tab-nav" role="tablist">
                            <li class="nav-item col-6 inline">
                                <a class="nav-link myTrainee active show" href="#myTrainee" role="tab" data-toggle="tab" aria-selected="true">
                                    <i class="icon-portfolio-1"></i> <span>On Progress</span>
                                </a>
                            </li>

                            <li class="nav-item col-6 inline">
                                <a class="nav-link myHistory" href="#myHistory" role="tab" data-toggle="tab" aria-selected="false">
                                    <i class="icon-profiles"></i> <span>Completed</span>
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content tab-space">
                            <div class="tab-pane active show" id="myTrainee">
                                <div class="panel-box mt-24">
                                    <div class="panel-title pb-4">
                                        <h3>Waiting for approval List</h3>
                                        <div class="right-notif mx-auto mx-md-0">
                                            @if ((isset($requests['approved'])) && $helper->inStaff() && $requests['approvedCount'] > 0))
                                                <div class="review-notif">
                                                    
                                                </div>
                                            @endif

                                            @if ($helper->inStaff())
                                                <button type="button" class="btn btn-default" onclick="location.href='{{url(route('trainee.index'))}}'">
                                                    <i class="icon-plus"></i> Create Trainee
                                                </button>
                                            @endif

                                            @if ($helper->inAdmin())
                                                <button type="button" class="btn btn-default" onclick="location.href='{{url(route('trainee.index'))}}'">
                                                    <i class="icon-plus"></i> Create Trainee
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="panel-table">
                                        {{--<div class="table-option">--}}
                                            {{--<div class="table-search">--}}
                                                {{--<label class="control-label">Search data</label>--}}
                                                {{--<input id="searchTable" type="text" class="form-control reset" placeholder="enter keywords">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                        <table class="traineeOngoing table table-hover table-striped table-lite" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="all">No</th>
                                                <th class="all">Topic/Title</th>
                                                <th class="all">Type</th>
                                                <th class="desktop">Employee Name</th>
                                                <!--<th class="all">Line Manager</th>-->
                                                <th class="desktop">Start</th>
                                                <th class="desktop">End</th>
                                                @if ($helper->inStaff())
                                                    <th class="desktop">Manager</th>
                                                    <th class="desktop">HR.</th>
                                                @endif
                                                <th class="desktop">Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="myHistory">
                                <div class="panel-box mt-24">
                                    <div class="panel-title">
                                        <h3>My Trainee List</h3>
                                        <div class="right-notif">
                                            @if ((isset($requests['approved'])) && $helper->inStaff() && ['approvedCount'] > 0)
                                                <div class="review-notif">
                                                    <i class="icon-exam"></i> You have got

                                                    <span>{{$requests['approvedCount']}} course(s)</span>
                                                    need to add review. <a href="#myApproval" class="customTabToggler" >Click here</a>
                                                </div>
                                            @endif

                                            @if ($helper->inStaff())
                                                <button type="button" class="btn btn-default" onclick="location.href='{{url(route('trainee.index'))}}'">
                                                    <i class="icon-plus"></i> Create Trainee
                                                </button>
                                            @endif

                                            @if ($helper->inAdmin())
                                                <button type="button" class="btn btn-default" onclick="location.href='{{url(route('trainee.index'))}}'">
                                                    <i class="icon-plus"></i> Create Trainee
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="panel-table">
                                        {{--<div class="table-option">--}}
                                            {{--<div class="table-search">--}}
                                                {{--<label class="control-label">Search data</label>--}}
                                                {{--<input id="searchTable" type="text" class="form-control reset" placeholder="enter keywords">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                        <table class="traineeCompletedtab table table-hover table-striped table-lite" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="all">No</th>
                                                <th class="all">Topic/Title</th>
                                                <th class="all">Type</th>
                                                <th class="desktop">Employee Name</th>
                                                <!--<th class="desktop">Line Manager</th>-->
                                                <th class="desktop">Start</th>
                                                <th class="desktop">End</th>
                                                @if ($helper->inStaff())
                                                    <th class="desktop">HR.</th>
                                                    <th class="desktop">Manager</th>
                                                @endif
                                                <th class="desktop">Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @if (!$helper->inAdmin())
                                @include('admindashboard::history.index')
                            @endif
                        </div>

                    </div>

                </div>
                <button type="button" class="btn btn-float btn-fba">
                    <i class="mdi mdi-more"></i>
                </button>
            </div>

            @if (!$helper->inAdmin())
                @include('sidebar')
            @endif
        </div>
    </main>
    @include('trainee-modal')
    <div role="alert" aria-live="assertive" aria-atomic="true" class="toast mentor_toast d-none" data-autohide="false">
        <div class="toast-header">
            {{--<img src="..." class="rounded mr-2" alt="...">--}}
            <strong class="mr-auto">Mydev</strong>
            {{--<small>11 mins ago</small>--}}
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body">
            Mentor need to complete report first
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $( document ).ready(function() {
            $(document).on("click",".page-link",function(){
                $(".render").val("no");
                activeTab = $("ul > .nav-item").find('.nav-link.active').attr("href").replace("#","");
                dataidx = Number($(this).text())-1;

                if (typeof dataidx=="NaN"){
                    dataidx = Number($(this).data("data-id"))-1;
                }
                if(dataidx>0){
                    if (activeTab=="myTrainee"){
                        dtTableOngoing.page(dataidx).draw(false);
                        dtTableOngoing.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                            pageCurrent = dataidx+"1";
                            pageCurrent = Number(pageCurrent);
                            cell.innerHTML = i+1 * pageCurrent;
                        });
                    }

                    if (activeTab=="myHistory"){
                        dtTableCompleted.page(dataidx).draw(false);
                        dtTableCompleted.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                            pageCurrent = dataidx+"1";
                            pageCurrent = Number(pageCurrent);
                            cell.innerHTML = i+1 * pageCurrent;
                        });
                    }
                }
            });

            var urlParams = new URLSearchParams(location.search);
            // var dtTableWaiting = $('.traineeWaitingApprovalTable').DataTable({
            //     processing: true,
            //     serverSide: true,
            //     scrollX: true,
            //     ajax: {
            //         url: "{{ route('trainee.datatableTraineeRequest', ['type' => 'waiting']) }}",
            //         data: function (d) {
            //             if (urlParams.has('page') && urlParams.has('tab')){
            //                 currentTab = urlParams.get('tab');
            //                 if (currentTab=="myTrainee"){
            //                     if ($(".render").val()=="yes"){
            //                         pageNext = Number(urlParams.get('page'));
            //                         d.start = Number(pageNext-1 + "0");
            //                     }
            //                 }
            //             }
            //             d.month = $('.filterMonth').val();
            //             d.year = $('.filterYear').val();                
            //         }
            //     },
            //     columns: [
            //         { data: 'DT_RowIndex', name: 'no' ,"searchable": false,"orderable": false},
            //         { data: 'title', name: 'title' },
            //         { data: 'type', name: 'type' },
            //         { data: 'employee_name', name: 'employee_name' },
            //         { data: 'atasan', name: 'atasan'}, // ini tes data = nama column nama = return val
            //         { data: 'start', name: 'start' },
            //         { data: 'end', name: 'end' },
            //         { data: 'manager', name: 'manager' },
            //         { data: 'hr', name: 'hr' },
            //         { data: 'type_prog', name: 'type_prog' },
            //         { data: 'option', name: 'option' },
            //     ],
            //     initComplete : function(settings, json) {
            //         if (urlParams.has('page') && urlParams.has('tab')){
            //             currentTab = urlParams.get('tab');
            //             if (currentTab=="myTrainee"){
            //                 pageNext = Number(urlParams.get('page'));
            //                 dtTableWaiting.page(pageNext-1).draw(false);
            //                 dtTableWaiting.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            //                     pageCurrent = pageNext-1+"1";
            //                     pageCurrent = Number(pageCurrent);
            //                     cell.innerHTML = i+1 * pageCurrent;
            //                 });
            //             }
            //         }
                    
            //     }
            // })

            var dtTableOngoing = $('.traineeOngoing').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                ajax: {
                    url: "{{ route('trainee.DataTableTraineeInhouse', ['type' => 'ongoing']) }}",
                    data: function (d) {
                        if (urlParams.has('page') && urlParams.has('tab')){
                            currentTab = urlParams.get('tab');
                                if (currentTab=="myTrainee"){
                                    if ($(".render").val()=="yes"){
                                        pageNext = Number(urlParams.get('page'));
                                        d.start = Number(pageNext-1 + "0");
                                    }
                                }
                        }
                        d.month = $('#filterMonthOngoing').val();
                        d.year = $('#filterYearOngoing').val();                
                    }
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'no' ,"searchable": false,"orderable": false},
                    { data: 'title', name: 'title' },
                    { data: 'type', name: 'type' },
                    { data: 'employee_name', name: 'employee_name' },
                    { data: 'start', name: 'start' },
                    { data: 'end', name: 'end' },
                    { data: 'option', name: 'option' },
                ],
                initComplete : function(settings, json) {
                    if (urlParams.has('page') && urlParams.has('tab')){
                        currentTab = urlParams.get('tab');
                        if (currentTab=="myTrainee"){
                            pageNext = Number(urlParams.get('page'));
                            dtTableOngoing.page(pageNext-1).draw(false);
                            dtTableOngoing.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                pageCurrent = pageNext-1+"1";
                                pageCurrent = Number(pageCurrent);
                                cell.innerHTML = i+1 * pageCurrent;
                            });
                        }
                    }
                    
                }

            });

            var dtTableCompleted = $('.traineeCompletedtab').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                ajax: {
                    url: "{{ route('trainee.DataTableTraineeInhouse', ['type' => 'completed']) }}",
                    data: function (d) {
                        if (urlParams.has('page') && urlParams.has('tab')){
                            currentTab = urlParams.get('tab');
                                if (currentTab=="myHistory"){
                                    if ($(".render").val()=="yes"){
                                        pageNext = Number(urlParams.get('page'));
                                        d.start = Number(pageNext-1 + "0");
                                    }
                                }
                        }
                        d.month = $('#filterMonthOngoing').val();
                        d.year = $('#filterYearOngoing').val();                
                    }
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'no' ,"searchable": false,"orderable": false},
                    { data: 'title', name: 'title' },
                    { data: 'type', name: 'type' },
                    { data: 'employee_name', name: 'employee_name' },
                    { data: 'start', name: 'start' },
                    { data: 'end', name: 'end' },
                    { data: 'option', name: 'option' },
                ],
                initComplete : function(settings, json) {
                    if (urlParams.has('page') && urlParams.has('tab')){
                        currentTab = urlParams.get('tab');
                        if (currentTab=="myHistory"){
                            pageNext = Number(urlParams.get('page'));
                            dtTableCompleted.page(pageNext-1).draw(false);
                            dtTableCompleted.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                pageCurrent = pageNext-1+"1";
                                pageCurrent = Number(pageCurrent);
                                cell.innerHTML = i+1 * pageCurrent;
                            });
                        }
                    }
                    
                }

            });

            if (urlParams.has('tab')){
                currentTab = urlParams.get('tab');
                setTimeout(function(){
                    $("."+currentTab).trigger("click");
                }, 3000);
            }

            // $('.traineeWaitingApprovalTable tbody').on('click', 'tr', function () {
            //     activeTab = $("ul > .nav-item").find('.nav-link.active').attr("href").replace("#","");
            //     currentPage = dtTableWaiting.page.info().page;
            //     currentPage = currentPage+1;
            //     var data = dtTableWaiting.row( this ).data();
            //     $("#page" + data.id).val(currentPage);
            //     var previewUrl = $("#preview" + data.id).attr("href").split("?");
            //     previewUrl = previewUrl[0];
            //     $("#preview" + data.id).attr("href", previewUrl+"?page="+currentPage+"&tab="+activeTab);
            // });

            $('.traineeOngoing tbody').on('click', 'tr', function () {
                activeTab = $("ul > .nav-item").find('.nav-link.active').attr("href").replace("#","");
                currentPage = dtTableOngoing.page.info().page;
                currentPage = currentPage+1;
                var data = dtTableOngoing.row( this ).data();
                $("#page" + data.id).val(currentPage);
                var previewUrl = $("#preview" + data.id).attr("href").split("?");
                previewUrl = previewUrl[0];
                $("#preview" + data.id).attr("href", previewUrl+"?page="+currentPage+"&tab="+activeTab);
            });

            $('.traineeCompletedtab tbody').on('click', 'tr', function () {
                activeTab = $("ul > .nav-item").find('.nav-link.active').attr("href").replace("#","");
                currentPage = dtTableCompleted.page.info().page;
                currentPage = currentPage+1;
                var data = dtTableCompleted.row( this ).data();
                $("#page" + data.id).val(currentPage);
                var previewUrl = $("#preview" + data.id).attr("href").split("?");
                previewUrl = previewUrl[0];
                $("#preview" + data.id).attr("href", previewUrl+"?page="+currentPage+"&tab="+activeTab);
            });

            // $('.traineeRejectedTable tbody').on('click', 'tr', function () {
            //     activeTab = $("ul > .nav-item").find('.nav-link.active').attr("href").replace("#","");
            //     currentPage = dtTableRejected.page.info().page;
            //     currentPage = currentPage+1;
            //     var data = dtTableRejected.row( this ).data();
            //     $("#page" + data.id).val(currentPage);
            //     var previewUrl = $("#preview" + data.id).attr("href").split("?");
            //     previewUrl = previewUrl[0];
            //     $("#preview" + data.id).attr("href", previewUrl+"?page="+currentPage+"&tab="+activeTab);
            // });

                $('.dataTables_filter input').keyup(function(e) {
                    console.log($(this).parent().parent().parent().parent().parent().find(".filterMonth").attr("id"));
                    e.preventDefault();
                    var value = $(this).val();
                    if (value.length==0 || value.length>5) {
                        // dtTableWaiting.ajax.reload();
                        dtTableOngoing.ajax.reload();
                        dtTableCompleted.ajax.reload();
                        // dtTableRejected.ajax.reload();
                    }
                });

                
                // $('#filterMonthWaitingApproval, #filterYearWaitingApproval').on( 'change', function () {
                //     var year = $(document).find('#filterYearWaitingApproval').val();
                //     var month = $(document).find('#filterMonthWaitingApproval').val();
                //     //$(document).find('#exportWaitingApproval').attr('href', "{{route('dashboard.traineeRequestExportTrainee')}}?type=new&year="+year+"&month="+month);
                //     dtTableWaiting.ajax.reload();
                // });

                $('#filterMonthOngoing, #filterYearOngoing').on( 'change', function () {
                    var year = $(document).find('#filterYearOngoing').val();
                    var month = $(document).find('#filterMonthOngoing').val();
                    //$(document).find('#exportOngoing').attr('href', "{{route('dashboard.traineeRequestExportTrainee')}}?type=approved&year="+year+"&month="+month);
                    dtTableOngoing.ajax.reload();
                });

                $('#filterMonthCompleted, #filterYearCompleted').on( 'change', function () {
                    var year = $(document).find('#filterYearCompleted').val();
                    var month = $(document).find('#filterMonthCompleted').val();
                    //$(document).find('#exportCompleted').attr('href', "{{route('dashboard.traineeRequestExportTrainee')}}?type=completed&year="+year+"&month="+month);
                    dtTableCompleted.ajax.reload();
                });

                // $('#filterMonthRejected, #filterYearRejected').on( 'change', function () {
                //     var year = $(document).find('#filterYearRejected').val();
                //     var month = $(document).find('#filterMonthRejected').val();
                //     //$(document).find('#exportRejected').attr('href', "{{route('dashboard.traineeRequestExportTrainee')}}?type=rejected&year="+year+"&month="+month);
                //     dtTableRejected.ajax.reload();
                // });
                

                $(document).on("click", ".reject_button_trainee", function () {
                    currentPage = dtTableWaiting.page.info().page;
                    currentPage = currentPage+1;
                    
                    if (currentPage){
                        $("#rejected_request_next_page").val(currentPage);
                    } else {
                        $("#rejected_request_next_page").val(1);
                    }

                     var requestgroup = $(this).data('requestgroup');
                     var userid = $(this).data('userid');
                     $(".modal-body #rejected_request_id").val( requestgroup );
                     $(".modal-body #rejected_user_id").val( userid );
                });

                pp = $('.page-item').hasClass('active');

                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                         
                          dtTableOngoing.draw()
                          dtTableCompleted.draw()
                          
                        })

            });
    </script>
@endsection
