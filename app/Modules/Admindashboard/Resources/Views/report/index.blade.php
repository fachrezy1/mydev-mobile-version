@php($user = \Sentinel::check())
@php($helper = new \App\Helpers\UserRolesHelper )

@extends( !$helper->inStaff() ? 'core.admin.main' :'core.trainee.main')
@section('head')
    <link rel="stylesheet" href="{{url('css/admin/dashboard/app.css')}}">
@endsection
@section('content')
    <main class="main-content">
        <div class="main-inner secondary-nav">
            <div class="sub-menu has-option">
                <button type="button" class="btn btn-clean d-none" onclick="location.href='setting.html'">
                    <i class="icon-back"></i> Back to setting
                </button>
                <h1 class="text-center">Report</h1>
                <button type="button" class="btn btn-clean d-none" onclick="location.href='setting-import.html'">
                    Import and Export <i class="icon-next"></i>
                </button>
            </div>

            <div class="setting-page">
                <div class="container custom-mob">
                    <ul class="nav nav-pills nav-pills-icons mt-0 p-0 d-flex" id="tab-nav" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active show" href="#adminReport" role="tab" data-toggle="tab" aria-selected="false">
                                <i class="icon-resume"></i> <span>Courses Report</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#userReport" role="tab" data-toggle="tab" aria-selected="false">
                                <i class="icon-portfolio-1"></i>
                                <span>{{$helper->inAdmin() ? 'All Users' : 'All Staff'}}</span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content tab-space pb-0">
                        <div class="tab-pane active show" id="adminReport">
                            <div class="panel-box">
                                <div class="panel-title">
                                    <h3>Report Overview</h3>
                                </div>
                                <div class="report-inner">
                                    <div class="box-inline">
                                        <div class="box-items">
                                            <div class="numeric-box">
                                                <div class="numeric">{{$requests['requests']}}</div>
                                                <div class="box-text">
                                                    <h4>Employee course</h4>
                                                    <p>Number of course taken by employee</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-items">
                                            <div class="numeric-box">
                                                <div class="numeric">{{$requests['completed']}}</div>
                                                <div class="box-text">
                                                    <h4>Employee course completed</h4>
                                                    <p>Number of course completed</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-items">
                                            <div class="numeric-box">
                                                <div class="numeric">{{$requests['not_completed']}}</div>
                                                <div class="box-text">
                                                    <h4>Course not completed</h4>
                                                    <p>Number of course without review</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <canvas id="reportAdmin"></canvas>
                                <!-- <canvas id="reportByFormGroup" class="d-none d-md-block"></canvas> -->
                                <!-- <canvas id="reportByFormGroupMobile" class="d-block d-md-none" height="300"></canvas> -->
                            </div>
                        </div>
                        <div class="tab-pane" id="userReport">
                            <div class="panel-box">
                                <div class="panel-title">
                                    <h3>User Report Overview</h3>
                                </div>
                                <div class="dual-side has-border">
                                    <div class="dual-item">
                                        <div class="item-four">
                                            <div class="item-block">
                                                <div class="item-text">
                                                    <h4>{{$helper->inAdmin() ? 'Users' : 'Staff'}} as Mentor</h4>
                                                    <p>Period {{date('Y')}}</p>
                                                </div>
                                                <div class="numeric">
                                                    {{$users['Mentor']}}
                                                </div>
                                            </div>
                                            <div class="item-block">
                                                <div class="item-text">
                                                    <h4>Highest Point</h4>
                                                    <p>Period {{date('Y')}}</p>
                                                </div>
                                                <div class="numeric">
                                                    {{$points['max']}}
                                                </div>
                                            </div>
                                            <div class="item-block">
                                                <div class="item-text">
                                                    <h4>{{$helper->inAdmin() ? 'Users' : 'Staff'}} as Trainee</h4>
                                                    <p>Period {{date('Y')}}</p>
                                                </div>
                                                <div class="numeric">
                                                    {{$users['Employee']}}
                                                </div>
                                            </div>
                                            <div class="item-block">
                                                <div class="item-text">
                                                    <h4>Total Points</h4>
                                                    <p>Period {{date('Y')}}</p>
                                                </div>
                                                <div class="numeric">
                                                    {{$points['sum']}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dual-item">
                                        <div class="chart-box">
                                            <canvas id="reportEmp"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-option custom d-none">
                                    <div class="table-first">
                                        <div class="table-search has-advance">
                                            <div class="custom-search">
                                                <label class="control-label">Search report</label>
                                                <input id="searchItem" type="text" class="form-control reset" placeholder="enter keywords">
                                            </div>
                                            <div class="button-group">
                                                <button type="button" class="btn btn-default btn-adv"><i class="icon-list"></i> Advance Filter</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="table-advance">
                                        <div class="advance-search">
                                            <div class="form-group reset">
                                                <label class="control-label">Employee name</label>
                                                <input type="text" class="form-control" placeholder="enter keywords">
                                            </div>
                                            <div class="form-group reset">
                                                <label class="control-label">Update</label>
                                                <input type="text" class="form-control" placeholder="enter date">
                                            </div>
                                            <div class="form-group reset">
                                                <label class="control-label">Points</label>
                                                <input type="text" class="form-control" placeholder="example: 1500">
                                            </div>

                                        </div>
                                        <div class="button-group">
                                            <button type="button" class="btn btn-default btn-table">Cancel</button>
                                            <button type="button" class="btn btn-default">Search</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="export-table d-flex flex-column flex-md-row justify-content-between">
                                		<div class="d-flex flex-row">
                                			<select id="filterMonth" class="form-control select-filter">
	                                        <option value="">Filter Month</option>
	                                        @for($i=1;$i<=12;$i++)
	                                            <option value="{{ $i }}">{{ date('F', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}</option>
	                                        @endfor
	                                    </select>

	                                    <select id="filterYear" class="form-control select-filter">
	                                        <option value="">Filter Year</option>
	                                        @for($i=0;$i<=3;$i++)
	                                            <option value="{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
	                                        @endfor
	                                    </select>

	                                    <a href="{{route('dashboard.report.exportAllUsers')}}" id="exportAllUsers" class="btn btn-default btn-sm btn-lite">
	                                        <i class="icon-xlsx-file-format"></i> Excel
	                                    </a>
                                		</div>
                                    <div class="dataTables_wrapper" style="display: inline-block;">
                                        <div id="DataTables_Table_0_filter" class="dataTables_filter">
                                            <label>Search:
                                                <input type="search" id="filterName" class="form-control form-control-sm" placeholder="" aria-controls="DataTables_Table_0">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <table id="reportTable" class="table table-hover table-striped" cellspacing="0" width="100%">
                                    <thead>
                                    </thead>

                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>

@endsection
@section('scripts')
    <script>

        var reportChartByFormType = {
            type: 'bar',
            data: {
                labels: [
                    @foreach($requests['request_by_month'] as $key=>$item)
                        "{{$key}}",
                    @endforeach
                ],
                datasets: [{
                    data: [
                        @foreach($requests['request_by_form_type'] as $key=>$item)
                        @if(is_array($item))
                            "{{ isset($item['External Speaker']) ? count($item['External Speaker']) : 0}}",
                        @else
                            0,
                        @endif
                        @endforeach
                    ],
                    label: 'External Speaker',
                    borderColor: "#3e95cd",
                    backgroundColor:"#3e95cd",
                    fill: false
                }, {
                    data: [
                        @foreach($requests['request_by_form_type'] as $key=>$item)
                        @if(is_array($item))
                            "{{ isset($item['Public Training']) ? count($item['Public Training']) : 0}}",
                        @else
                            0,
                        @endif
                        @endforeach
                    ],
                    label:'Public Training',
                    borderColor: "#6a8da2",
                    backgroundColor:"#6a8da2",
                    fill: false
                }, {
                    data: [
                        @foreach($requests['request_by_form_type'] as $key=>$item)
                        @if(is_array($item))
                            "{{ isset($item['Mentoring / Coaching']) ? count($item['Mentoring / Coaching']) : 0}}",
                        @else
                            0,
                        @endif
                        @endforeach
                    ],
                    label:'Mentoring / Coaching',
                    borderColor: "#6ba26e",
                    backgroundColor:"#6ba26e",
                    fill: false
                }, {
                    data: [
                        @foreach($requests['request_by_form_type'] as $key=>$item)
                        @if(is_array($item))
                            "{{ isset($item['Assignment']) ? count($item['Assignment']) : 0}}",
                        @else
                            0,
                        @endif
                        @endforeach
                    ],
                    label:'Assignment',
                    borderColor: "#9aa26a",
                    backgroundColor:"#9aa26a",
                    fill: false
                }, {
                    data: [
                        @foreach($requests['request_by_form_type'] as $key=>$item)
                        @if(is_array($item))
                            "{{ isset($item['Inhouse']) ? count($item['Inhouse']) : 0}}",
                        @else
                            0,
                        @endif
                        @endforeach
                    ],
                    label:'Inhouse',
                    borderColor: "#a27966",
                    backgroundColor:"#a27966",
                    fill: false
                }
                ]
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Course by Month'
            },
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }};

        var reportChart = {
        type: 'line',
        data: {
            labels: [
                @foreach($requests['request_by_month'] as $key=>$item)
                    "{{$key}}",
                @endforeach
            ],
            datasets: [{
                data: [
                    @foreach($requests['request_by_month'] as $key=>$item)
                        @if(is_array($item))
                        "{{count($item)}}",
                        @else
                        0,
                        @endif
                    @endforeach
                ],
                label: 'Requested',
                borderColor: "#3e95cd",
                fill: false
            }, {
                data: [
                    @foreach($requests['completed_request_by_month'] as $key=>$item)
                        @if(is_array($item))
                        "{{count($item)}}",
                        @else
                        0,
                    @endif
                    @endforeach
                ],
                label:'Completed',
                borderColor: "#8e5ea2",
                fill: false
            }
            ]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Course Stats'
            }
        }}
        var ctxOne = document.getElementById('reportAdmin').getContext('2d');
        // var ctxTwo = document.getElementById('reportByFormGroup').getContext('2d');
        // var ctxTwoMobile = document.getElementById('reportByFormGroupMobile').getContext('2d');
        new Chart(ctxOne, reportChart);
        // new Chart(ctxTwo, reportChartByFormType);
        // new Chart(ctxTwoMobile, reportChartByFormType);


        //all users data
        var optionsFive = {
            type: 'horizontalBar',
            data: {
                labels: ["Inhouse", "Public Training", "Assignment", "Mentoring & Coaching",'External Speaker'],
                datasets: [
                    {
                        label: "Total",
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#e8c3b9"],
                        data: [{{$sum['Inhouse']}},{{$sum['Public Training']}},{{$sum['Assignment']}},{{$sum['Mentoring']}},{{$sum['External']}}]
                    }
                ]
            },
            options: {
                legend: { display: false },
                title: {
                    display: false,
                    text: 'User Report period 2018'
                }
            }
        }


        var ctxFive = document.getElementById('reportEmp').getContext('2d');
        new Chart(ctxFive, optionsFive);

        $(document).ready(function () {
            var table = $('#reportTable').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                pageLength:5,
                ajax : {
                    url: '{{route('dashboard.report.getEmployeeData')}}',
                    data: function ( d ) {
                        d.name = $(document).find('#filterName').val();
                        d.month = $(document).find('#filterMonth').val();
                        d.year = $(document).find('#filterYear').val();
                    }
                },
                columns: [
                    {data: 'first_name', name: 'first_name',title:'Full Name'},
                    {data: 'inhouse', name: 'inhouse',title:'Inhouse'},
                    {data: 'training', name: 'training',title:'Public Training'},
                    {data: 'assignment', name: 'assignment',title:'Assignment'},
                    {data: 'mentoring', name: 'mentoring',title:'Mentoring & Coaching'},
                    {data: 'external', name: 'external',title:'External Speaker'},
                    {data: 'point', name: 'point',title:'Total Points'},
                    {data: 'options', name: 'options',title: 'Options'}
                ]
            });

            $('#filterYear, #filterMonth').change(function(){
                var year = $(document).find('#filterYear').val();
                var month = $(document).find('#filterMonth').val();
                var name = $(document).find('#filterName').val();

                $(document).find('#exportAllUsers').attr('href', "{{route('dashboard.report.exportAllUsers')}}?year="+year+"&month="+month+"&name="+name);
                table.draw();
            });

            var typingTimer;                
            $('#filterName').keyup(function(){
                var year = $(document).find('#filterYear').val();
                var month = $(document).find('#filterMonth').val();
                var name = $(document).find('#filterName').val();

                clearTimeout(typingTimer);
                typingTimer = setTimeout(function(){
                    $(document).find('#exportAllUsers').attr('href', "{{route('dashboard.report.exportAllUsers')}}?year="+year+"&month="+month+"&name="+name);
                    table.draw();
                }, 1000);
            });

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
						  table.draw()
						})
        })
    </script>
    <script src="{{ url('js/report.js') }}"></script>
@endsection
