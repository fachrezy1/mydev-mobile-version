@extends('core.admin.main')
@section('head')
@endsection
@section('content')
    @include('core.admin.header')
    <main class="main-content">
        <div class="main-dashboard secondary-nav">
            <div class="dashboard-full">
                <div class="sub-menu has-option">
                    <h1 class="text-center">Item Management</h1>
                </div>
                <!-- <div class="dashboard-breadcrumb clean">
                    <div class="container-fluid">
                        <ul class="list-unstyled top-breadcrumb">
                            <li>
                                <a href="#">
                                    <i class="icon-home"></i>
                                    Dashboard
                                </a>
                            </li>
                            <li>
                                Item Management
                            </li>
                        </ul>
                    </div>
                </div> -->

                <div class="container">
                    <ul class="nav nav-pills nav-pills-icons p-0 mt-0 mt-md-4" id="tab-nav" role="tablist">
                        <li class="nav-item inline">
                            <a class="nav-link active show" href="#manage" role="tab" data-toggle="tab" aria-selected="true">
                                <i class="icon-portfolio-1"></i> Item Management
                            </a>
                        </li>
                        <li class="nav-item inline">
                            <a class="nav-link" href="#approved" role="tab" data-toggle="tab" aria-selected="true">
                                <i class="icon-portfolio-1"></i> Item History
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active show" id="manage">
                        	<div class="panel-box mt-4">
                              <div class="panel-title d-flex mb-4">
                                  <h3>Item Management</h3>
                                  <div class="button-group mx-auto mx-sm-0">
                                      <button type="button" class="btn btn-default" onclick="location.href='{{route('dashboard.item.add')}}'"><i class="icon-plus"></i> Add new item</button>
                                  </div>
                              </div>
                              <div class="panel-table">
                                  <table id="itemManagement" class="table table-hover table-striped" cellspacing="0" width="100%">
                                      <thead>
                                      <tr>
                                          <th class="all">Image</th>
                                          <th class="all">Name</th>
                                          <th class="desktop">Description</th>
                                          <th class="desktop">Points</th>
                                          <th class="all">Status</th>
                                          <th class="desktop">Options</th>
                                      </tr>
                                      </thead>
                                  </table>
                              </div>
                          </div>
                        </div>
                        <div class="tab-pane" id="requests">
                            <div class="panel-box mt-4">
                                    <div class="panel-title">
                                        <h3>Item Management</h3>
                                    </div>
                                    <div class="panel-table">
                                        <div class="table-option custom">
                                            <div class="table-search has-advance">
                                                <div class="button-group">
                                                    <button type="button" class="btn btn-default" onclick="location.href='{{route('dashboard.item.add')}}'"><i class="icon-plus"></i> Add new item</button>
                                                </div>
                                            </div>
                                        </div>
                                        <table id="itemRequests" class="table table-hover table-striped" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="all">Image</th>
                                                <th class="all">Name</th>
                                                <th class="desktop">Description</th>
                                                <th class="all">Requester</th>
                                                <th class="desktop">Options</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                        </div>
                        <div class="tab-pane" id="approved">
                            <div class="panel-box mt-4">
                                    <div class="panel-title">
                                        <h3>Item History</h3>
                                    </div>
                                    <div class="panel-table">
                                        <table id="itemApproved" class="table table-hover table-striped" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="all">Image</th>
                                                <th class="all">Name</th>
                                                <th class="desktop">Description</th>
                                                <th class="desktop">Trainee</th>
                                                <th class="desktop">Point Info</th>
                                                <th class="desktop">At</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                        </div>
                    </div>


                </div>

            </div>
        </div>

    </main>
@endsection
@section('scripts')
    <script src="{{ url('js/setting.js') }}"></script>
    @if (session('success'))
        <script>
            alert('{{session('success')}}');
        </script>
    @endif

    <script>
        $(document).ready(function () {
            $('#itemManagement').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                ajax: '{{route('dashboard.item.getItems')}}',
                columns: [
                    {data: 'image', name: 'image'},
                    {data: 'name', name: 'name'},
                    {data: 'description', name: 'description'},
                    {data: 'point', name: 'point'},
                    {data: 'status', name: 'status'},
                    {data: 'options', name: 'options',searchable:false}
                ]
            });
            $('#itemRequests').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                ajax: '{{route('dashboard.item.getItemRequests')}}',
                columns: [
                    {data: 'image', name: 'image'},
                    {data: 'name', name: 'name'},
                    {data: 'description', name: 'description'},
                    {data: 'requester', name: 'requester'},
                    {data: 'options', name: 'options',searchable:false}
                ]
            });
            $('#itemApproved').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                order : [[ 5, "asc" ]],
                ajax: '{{route('dashboard.item.getItemApproved')}}',
                columns: [
                    {data: 'image', name: 'image'},
                    {data: 'name', name: 'name'},
                    {data: 'description', name: 'description'},
                    {data: 'requester', name: 'requester'},
                    {data: 'point_info', name: 'point_info'},
                    {data: 'created_at', name: 'created_at'}
                ]
            });
        })
    </script>
@endsection
