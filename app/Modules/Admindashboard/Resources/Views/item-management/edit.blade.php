@extends('core.admin.main')
@section('head')
@endsection
@section('content')
    @include('core.admin.header')
    <main class="main-content">
        <div class="main-dashboard">
            <div class="dashboard-full">
                <div class="dashboard-breadcrumb clean">
                    <div class="container-fluid">
                        <div class="sub-breadcrumb">
                            <div class="sub-left">
                                <button type="button" class="btn btn-clean" onclick="location.href='{{route('dashboard.item.index')}}'">
                                    <i class="icon-back"></i>
                                    Item Management
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container">

                    <div class="dashboard-entry">
                        <div class="panel-box mt-24 medium">
                            <div class="panel-title mb-36">
                                <h3 class="big-title">Edit {{$item['name']}}</h3>
                            </div>

                            <div class="item-box">
                                <form action="{{route('dashboard.item.update')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$item['id']}}">
                                    <div class="form-group reset px-4">
                                        <label class="control-label">Item Name</label>
                                        <div class="form-field">
                                            <input type="text" name="name" value="{{$item['name']}}" maxlength="250" required class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group reset px-4">
                                        <label class="control-label">Item Description</label>
                                        <div class="form-field">
                                            <textarea class="form-control" name="description" maxlength="1200" rows="8">{{$item['description']}}</textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                    	<div class="col-12 col-md-5">
                                    		<div class="form-group reset px-4">
		                                        <label class="control-label">Points</label>
		                                        <div class="form-field">
		                                            <input type="number" name="point" value="{{$item['point']}}" maxlength="10" required class="form-control">
		                                        </div>
		                                    </div>
		                                    <div class="form-group reset px-4">
		                                        <label class="control-label">Quota</label>
		                                        <div class="form-field">
		                                            <input type="number" name="quota" value="{{$item['quota']}}" maxlength="10" required class="form-control">
		                                        </div>
		                                    </div>
                                    	</div>
                                    	<div class="col-12 col-md-7">
                                    		<div class="form-group reset px-4">
                                    			<label class="control-label" for="exampleFormControlFile1">Item Picture</label>
                                    			<img src="{{asset('uploads/'.$item['picture'])}}" class="img-fluid img-thumbnail">
                                    			<div class="form-field">
                                    				<input type="file" class="form-control-file" value="{{$item['picture']}}" name="picture" id="exampleFormControlFile1">
                                    			</div>
                                    		</div>
                                    		<div class="form-group reset px-4">
                                    			<label class="control-label">Status</label>
                                    			<div class="form-field">
                                    				<div class="togglebutton">
                                    					<label class="toggler-labeler">
                                    						Unpublished
                                    						<input type="checkbox" name="published" {{$item['status'] == 'published' ? 'checked' : ''}}>
                                    						<span class="toggle"></span>
                                    						Published
                                    					</label>
                                    				</div>
                                    			</div>
                                    </div>
                                    	</div>
                                    </div>
                                    <div class="button-group">
                                        <button type="button" class="btn btn-default" onclick="location.href='{{route('dashboard.item.index')}}'">Cancel</button>
                                        <button type="submit" class="btn btn-default">Submit</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>

    </main>
@endsection
@section('styles')
<style>
	.toggler-labeler {
		width: 100%!important;
	}
	@media(min-width: 375px){
		.toggler-labeler {max-width: 240px}
	}
</style>
@endsection
@section('scripts')
    <script src="{{ url('js/setting.js') }}"></script>
    @if (session('success'))
        <script>
            alert('{{session('success')}}');
        </script>
    @endif
@endsection
