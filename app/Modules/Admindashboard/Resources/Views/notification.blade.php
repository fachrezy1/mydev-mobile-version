@php($helper = new \App\Helpers\UserRolesHelper )

@extends($helper->inAdmin() ? 'core.admin.main' :'core.trainee.main')
@section('head')
    <link rel="stylesheet" href="{{url('css/admin/dashboard/app.css')}}">
@endsection
@section('content')
    <main class="main-content">
        <div class="main-dashboard">
            <div class="dashboard-content">
                <div class="container-fluid">

                    <div class="dashboard-entry">
                        <div class="dashboard-title">
                            <h1>Trainee</h1>
                        </div>

                        @if (session('success'))
                            <div class="dashboard-notify alert alert-success">
                                <div class="alert-icon">
                                    <i class="mdi mdi-comment-alert-outline"></i>
                                </div>
                                <div class="notify-content">
                                    <h4>Admin notes</h4>
                                    <p>{{session('success')}}</p>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true"><i class="mdi mdi-close"></i></span>
                                    </button>
                                </div>
                            </div>
                        @endif

                        
                        <div class="panel-box mt-24">
                            <div class="panel-title">
                                <h3>My Trainee List</h3>
                            </div>
                            <div class="panel-table">
                                <div class="table-option">
                                    <div class="table-search">
                                        <label class="control-label">Search data</label>
                                        <input id="searchTable" type="text" class="form-control reset" placeholder="enter keywords">
                                    </div>
                                </div>
                                {{--@if (session('error'))--}}
                                    {{--<div class="alert alert-danger alert-dismissible fade show" role="alert">--}}
                                        {{--{{session('error')}}--}}
                                        {{--<button type="button" class="close" data-dismiss="alert" aria-label="Close">--}}
                                            {{--<span aria-hidden="true">&times;</span>--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                {{--@endif--}}
                                <table class="table table-hover table-striped traineeTable" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Form Type</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @php ($n=1)
                                    @if($requests)
                                        @foreach($requests as $key=>$request)
                                            @if($request)
                                                @foreach($request as $idform=>$items)
                                                @foreach($items as $values)
                                                @foreach($values as $item)
                                                <tr>

                                                    <td>{{$n}}</td>
                                                    <td>{{ucfirst($idform)}}</td>
                                                    <td><i class="mdi mdi-calendar"></i>{{isset($item[$idform.'_start_date']) ? $item[$idform.'_start_date'] : ''}}</td>
                                                    <td><i class="mdi mdi-calendar"></i>{{isset($item[$idform.'_end_date']) ? $item[$idform.'_end_date'] : ''}}</td>
                                                    <td><span class="badge badge-pill {{isset($item['is_completed_class']) ? $item['is_completed_class'] : ''}}">
                                                            {{isset($item['is_completed'])?$item['is_completed']:''}}</span></td>
                                                    <td>
                                                        @if ($item['accepted_by_me'])
                                                            <input type="button" class="btn btn-disabled disabled" disabled value="Approved">
                                                        @else
                                                            <form action="{{route('request.acceptRequest')}}" method="post">
                                                                @csrf
                                                                <input type="hidden" name="user_id" value="{{$item['user_id']}}">
                                                                <input type="hidden" name="form_group_id" value="{{$item['form_group_id']}}">
                                                                <input type="submit" class="btn btn-primary" value="Approve">
                                                            </form>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                                @php($n=$n+1)
                                                @endforeach
                                            @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <button type="button" class="btn btn-float btn-fba">
                    <i class="mdi mdi-more"></i>
                </button>
            </div>
            @if (!$helper->inAdmin())
                @include('sidebar')
            @endif
        </div>
    </main>
@endsection
@section('scripts')
    <script src="{{url('js/general.js')}}"></script>
    <script src="{{url('js/admin/dashboard/app.js')}}"></script>
@endsection
