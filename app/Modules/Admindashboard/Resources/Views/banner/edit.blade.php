@extends('core.admin.main')
@section('head')
@endsection
@section('content')
	@include('core.admin.header')

	<main class="main-content">
		<div class="main-dashboard">
			<div class="dashboard-breadcrumb clean">
				<div class="container-fluid">
					<div class="sub-menu has-option">
						<button type="button" class="btn btn-clean" onclick="location.href='{{route('dashboard.setting.index')}}'">
							<i class="icon-back"></i> Back
						</button>
						<h1 class="text-center">Banner Setting</h1>
					</div>
				</div>
			</div>
			<div class="setting-page">
				<div class="container-fluid">
					<div class="side-elements">
						<div class="side-left w-100">
							<div class="panel-title">
								<h3>Set banner</h3>
							</div>
							<div class="item-box for-banner">
								<form action="{{route('dashboard.banner.update')}}" method="post" enctype="multipart/form-data">
									@csrf
									<div class="row">
										<div class="col-12 col-lg-6">
											<div class="form-group reset split">
												<label class="control-label">Title</label>
												<div class="form-field">
													<input type="text" name="banner_name" value="{{$banner['banner_name']}}" class="form-control">
												</div>
											</div>
											<div class="form-group reset split">
												<label class="control-label">Banner Description</label>
												<div class="form-field">
													<textarea class="form-control" name="banner_description" rows="8">{{$banner['banner_description']}}</textarea>
												</div>
											</div>
										</div>
										<div class="col-12 col-lg-6">
											<div class="form-group reset split">
												<label class="control-label text-left" for="exampleFormControlFile1">Banner Picture</label>
												<img src="{{asset('uploads/'.$banner['banner_image'])}}" class="img-thumbnail img-fluid img-preview">
												<div class="form-field">
													<input type="file" accept=".jpg,.jpeg,.png" value="{{$banner['banner_image']}}" name="banner_image" class="form-control-file" id="exampleFormControlFile1">
												</div>
											</div>
										</div>
									</div>
									
									<div class="button-group py-0">
										<button type="button" class="btn btn-default" onclick="location.href='{{route('dashboard.setting.index')}}'">Cancel</button>
										<button type="submit" class="btn btn-default">Save</button>
									</div>
								</form>
							</div>
						</div>
						<div class="side-right d-none">
							<div class="side-item">
								<h3>Banner Stats</h3>
								<canvas id="bannerChart"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</main>

@endsection
@section('styles')
<style>
	.img-preview {
		width: 100%;
		height: 100%;
		max-height: 300px;
		object-fit: contain;
	}
</style>
@endsection
@section('scripts')
    <script src="{{ url('js/setting.js') }}"></script>
    @if(session('success'))
        <script>
            alert({{session('success')}})
        </script>
    @endif
@endsection
