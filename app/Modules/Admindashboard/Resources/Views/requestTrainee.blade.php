@extends('core.admin.main')
@php($role= new \App\Helpers\UserRolesHelper())
@php($agent = new \Jenssegers\Agent\Agent())

@section('content')
    <main class="main-content">
        <div class="main-dashboard secondary-nav">
            <div class="dashboard-content w-100">
                <div class="container">

                    <div class="dashboard-entry">
                        <div class="dashboard-title">
                            <h1>Trainee Request</h1>
                        </div>

                        @if (session('success'))
                            <div class="dashboard-notify alert alert-success">
                                <div class="alert-icon">
                                    <i class="mdi mdi-comment-alert-outline"></i>
                                </div>
                                <div class="notify-content">
                                    <p>{{session('success')}}</p>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true"><i class="mdi mdi-close"></i></span>
                                    </button>
                                </div>
                            </div>
                        @endif

                        <ul class="nav nav-pills nav-pills-icons" id="tab-nav" role="tablist">
                            <li class="nav-item col-3 inline">
                                <a class="nav-link myTrainee active show" href="#myTrainee" role="tab" data-toggle="tab" aria-selected="true">
                                    <i class="icon-portfolio-1"></i> <span>Waiting for Approval</span>
                                </a>
                            </li>

                            {{--@if ($user->hasAccess('trainee.manage'))--}}
                            <li class="nav-item col-3 inline">
                                <a class="nav-link myApproval" href="#myApproval" role="tab" data-toggle="tab" aria-selected="false">
                                    <i class="icon-checklist"></i> <span>Ongoing Progress</span>
                                </a>
                            </li>
                            {{--@endif--}}
                            <li class="nav-item col-3 inline">
                                <a class="nav-link myHistory" href="#myHistory" role="tab" data-toggle="tab" aria-selected="false">
                                    <i class="icon-profiles"></i> <span>Completed</span>
                                </a>
                            </li>
                            {{--@if ($user->hasAccess('trainee.manage'))--}}
                            <li class="nav-item col-3 inline">
                                <a class="nav-link" href="#rejected" role="tab" data-toggle="tab" aria-selected="false">
                                    <i class="icon-profiles"></i> <span>Rejected</span>
                                </a>
                            </li>
                            {{--@endif--}}
                        </ul>


                        <div class="tab-content tab-space">
                            <div class="tab-pane active show" id="myTrainee">
                                <div class="panel-box mt-24">
                                    @if ($role->inAdmin())
                                        <div class="panel-title">
                                            <h3>Ongoing list</h3>
                                        </div>
                                    @else
                                        <div class="panel-title">
                                            <h3>Waiting for approval list</h3>
                                        </div>
                                    @endif
                                    <div class="panel-table">

                                        <div class="export-table export-mytrainee">
                                            <select id="filterMonthWaitingApproval" class="filterMonth form-control select-filter">
                                                <option value="">Filter Month</option>
                                                @for($i=1;$i<=12;$i++)
                                                    <option value="month-{{ date('m', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}">{{ date('F', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}</option>
                                                @endfor
                                            </select>

                                            <select id="filterYearWaitingApproval" class="filterYear form-control select-filter">
                                                <option value="">Filter Year</option>
                                                @for($i=0;$i<=3;$i++)
                                                    <option value="year-{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                                @endfor
                                            </select>

                                            <a href="{{route('dashboard.traineeRequestExportTrainee')}}?type=new" id="exportWaitingApproval" class="btn btn-default btn-sm btn-lite">
                                                <i class="icon-xlsx-file-format"></i> Excel
                                            </a>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div>
                                            <table class="table table-hover table-striped table-responsive traineeWaitingApprovalTable" cellspacing="0">
                                            <thead>
                                            <tr>
                                                <th width="10">No</th>
                                                <th>Title/Topic</th>
                                                <th>Type</th>
                                                <th>Employee Name</th>
                                                <th>Line Manager</th>
                                                <th>Start</th>
                                                <th>End</th>
                                                <th>Manager</th>
                                                <th>HR.</th>
                                                <th width="100">Option</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="myApproval">
                                <div class="panel-box mt-24">
                                    <div class="panel-title">
                                        <h3>Ongoing Progress list</h3>
                                    </div>
                                    <div class="panel-table">
                                        <div class="export-table export-mytrainee">
                                            <select id="filterMonthOngoing" class="filterMonth form-control select-filter">
                                                <option value="">Filter Month</option>
                                                @for($i=1;$i<=12;$i++)
                                                    <option value="month-{{ date('m', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}">{{ date('F', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}</option>
                                                @endfor
                                            </select>

                                            <select id="filterYearOngoing" class="filterYear form-control select-filter">
                                                <option value="">Filter Year</option>
                                                @for($i=0;$i<=3;$i++)
                                                    <option value="year-{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                                @endfor
                                            </select>

                                            <a href="{{route('dashboard.traineeRequestExportTrainee')}}?type=approved" id="exportOngoing" class="btn btn-default btn-sm btn-lite">
                                                <i class="icon-xlsx-file-format"></i> Excel
                                            </a>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div>
                                        <table class="traineeOngoingTable table table-hover table-striped table-responsive" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="all" width="10">No</th>
                                                <th class="all">Title/Topic</th>
                                                <th class="all">Type</th>
                                                <th class="desktop">Employee Name</th>
                                                <th class="desktop">Line Manager</th>
                                                <th class="desktop">Start</th>
                                                <th class="desktop">End</th>
                                                <th class="desktop">Manager</th>
                                                <th class="desktop">HR.</th>
                                                <th class="desktop" width="100">Option</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane" id="myHistory">
                                <div class="panel-box mt-24">
                                    <div class="panel-title">
                                        <h3>Completed list</h3>
                                    </div>
                                    <div class="panel-table">
                                        <div class="export-table export-mytrainee">
                                            <select id="filterMonthCompleted" class="filterMonth form-control select-filter">
                                                <option value="">Filter Month</option>
                                                @for($i=1;$i<=12;$i++)
                                                    <option value="month-{{ date('m', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}">{{ date('F', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}</option>
                                                @endfor
                                            </select>

                                            <select id="filterYearCompleted" class="filterYear form-control select-filter">
                                                <option value="">Filter Year</option>
                                                @for($i=0;$i<=3;$i++)
                                                    <option value="year-{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                                @endfor
                                            </select>

                                            <a href="{{route('dashboard.traineeRequestExportTrainee')}}?type=completed" id="exportCompleted" class="btn btn-default btn-sm btn-lite">
                                                <i class="icon-xlsx-file-format"></i> Excel
                                            </a>
                                        </div>
                                        <div class="clearfix"></div>
                                        
                                        <div>
                                        <table class="traineeCompletedTable table table-hover table-striped table-responsive" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="all" width="10">No</th>
                                                <th class="all">Title/Topic</th>
                                                <th class="all">Type</th>
                                                <th class="desktop">Employee Name</th>
                                                <th class="desktop">Line Manager</th>
                                                <th class="desktop">Start</th>
                                                <th class="desktop">End</th>
                                                <th class="desktop">Manager</th>
                                                <th class="desktop">HR.</th>
                                                <th class="desktop" width="100">Option</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane" id="rejected">
                                <div class="panel-box mt-24">
                                    <div class="panel-title">
                                        <h3>Rejected list</h3>
                                    </div>
                                    <div class="panel-table">
                                        <div class="export-table export-mytrainee">
                                            <select id="filterMonthRejected" class="filterMonth form-control select-filter">
                                                <option value="">Filter Month</option>
                                                @for($i=1;$i<=12;$i++)
                                                    <option value="month-{{ date('m', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}">{{ date('F', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}</option>
                                                @endfor
                                            </select>

                                            <select id="filterYearRejected" class="filterYear form-control select-filter">
                                                <option value="">Filter Year</option>
                                                @for($i=0;$i<=3;$i++)
                                                    <option value="year-{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                                @endfor
                                            </select>

                                            <a href="{{route('dashboard.traineeRequestExportTrainee')}}?type=rejected" id="exportRejected" class="btn btn-default btn-sm btn-lite">
                                                <i class="icon-xlsx-file-format"></i> Excel
                                            </a>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div>
                                        <table class="traineeRejectedTable table table-hover table-striped table-responsive" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="all" width="10">No</th>
                                                <th class="all">Title/Topic</th>
                                                <th class="all">Type</th>
                                                <th class="desktop">Employee Name</th>
                                                <th class="desktop">Line Manager</th>
                                                <th class="desktop">Start</th>
                                                <th class="desktop">End</th>
                                                <th class="desktop">Manager</th>
                                                <th class="desktop">HR.</th>
                                                <th class="desktop">Notes</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <button type="button" class="btn btn-float btn-fba">
                    <i class="mdi mdi-more"></i>
                </button>
            </div>
        </div>
    </main>

    @include('trainee-modal')
    <div class="modal fade" id="rejectNotes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Are you sure to reject it?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('request.rejectRequest')}}" method="post">
                        @csrf
                        <input type="hidden" name="user_id" id="rejected_user_id" value="">
                        <input type="hidden" name="id" id="rejected_request_id" value="">
                        <input type="hidden" name="tab" value="myTrainee">
                        <input type="hidden" name="next_page" id="rejected_request_next_page">
                        <div class="form-group reset ">
                            <div class="form-field">
                                <textarea class="form-control counter-text" name="rejected_notes" rows="6" placeholder="Notes for rejected trainee"></textarea>
                                <p class="form-helper text-count"><span class="counter-text-value">0</span>/255</p>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between">
	                        <button type="button" class="btn btn-dark" data-dismiss="modal">Cancel</button>
	                        <button type="submit" class="btn btn-default"> Confirm </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="render" class="render" value="yes">
@endsection
@section('scripts')
    <script>
        /** TABLE FILTER**/
        $( document ).ready(function() {
            $(document).on("click",".page-link",function(){
                $(".render").val("no");
                activeTab = $("ul > .nav-item").find('.nav-link.active').attr("href").replace("#","");
                dataidx = Number($(this).text())-1;

                if (typeof dataidx=="NaN"){
                    dataidx = Number($(this).data("data-id"))-1;
                }
                if(dataidx>0){
                    if (activeTab=="myTrainee"){
                        dtTableWaiting.page(dataidx).draw(false);
                        dtTableWaiting.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                            pageCurrent = dataidx+"1";
                            pageCurrent = Number(pageCurrent);
                            cell.innerHTML = i+1 * pageCurrent;
                        });
                    }

                    if (activeTab=="myApproval"){
                        dtTableOngoing.page(dataidx).draw(false);
                        dtTableOngoing.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                            pageCurrent = dataidx+"1";
                            pageCurrent = Number(pageCurrent);
                            cell.innerHTML = i+1 * pageCurrent;
                        });
                    }

                    if (activeTab=="rejected"){
                        dtTableRejected.page(dataidx).draw(false);
                        dtTableRejected.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                            pageCurrent = dataidx+"1";
                            pageCurrent = Number(pageCurrent);
                            cell.innerHTML = i+1 * pageCurrent;
                        });
                    }

                    if (activeTab=="myHistory"){
                        dtTableCompleted.page(dataidx).draw(false);
                        dtTableCompleted.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                            pageCurrent = dataidx+"1";
                            pageCurrent = Number(pageCurrent);
                            cell.innerHTML = i+1 * pageCurrent;
                        });
                    }
                }
            });

            var urlParams = new URLSearchParams(location.search);
            var dtTableWaiting = $('.traineeWaitingApprovalTable').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                pagingType: 'simple_numbers',

                ajax: {
                    url: "{{ route('dashboard.datatableTraineeRequest', ['type' => 'waiting']) }}",
                    data: function (d) {
                        if (urlParams.has('page') && urlParams.has('tab')){
                            currentTab = urlParams.get('tab');
                            if (currentTab=="myTrainee"){
                                if ($(".render").val()=="yes"){
                                    pageNext = Number(urlParams.get('page'));
                                    d.start = Number(pageNext-1 + "0");
                                }
                            }
                        }
                        d.month = $('.filterMonth').val();
                        d.year = $('.filterYear').val();                
                    }
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'no' ,"searchable": false,"orderable": false},
                    { data: 'title', name: 'title' },
                    { data: 'type', name: 'type' },
                    { data: 'employee_name', name: 'employee_name' },
                    { data: 'atasan', name: 'atasan'}, // ini tes data = nama column nama = return val
                    { data: 'start', name: 'start' },
                    { data: 'end', name: 'end' },
                    { data: 'manager', name: 'manager' },
                    { data: 'hr', name: 'hr' },
                    { data: 'option', name: 'option' },
                ],
                initComplete : function(settings, json) {
                    if (urlParams.has('page') && urlParams.has('tab')){
                        currentTab = urlParams.get('tab');
                        if (currentTab=="myTrainee"){
                            pageNext = Number(urlParams.get('page'));
                            dtTableWaiting.page(pageNext-1).draw(false);
                            dtTableWaiting.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                pageCurrent = pageNext-1+"1";
                                pageCurrent = Number(pageCurrent);
                                cell.innerHTML = i+1 * pageCurrent;
                            });
                        }
                    }
                    
                }
            })

            var dtTableOngoing = $('.traineeOngoingTable').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                pagingType: 'simple_numbers',
                ajax: {
                    url: "{{ route('dashboard.datatableTraineeRequest', ['type' => 'ongoing']) }}",
                    data: function (d) {
                        if (urlParams.has('page') && urlParams.has('tab')){
                            currentTab = urlParams.get('tab');
                                if (currentTab=="myApproval"){
                                    if ($(".render").val()=="yes"){
                                        pageNext = Number(urlParams.get('page'));
                                        d.start = Number(pageNext-1 + "0");
                                    }
                                }
                        }
                        d.month = $('#filterMonthOngoing').val();
                        d.year = $('#filterYearOngoing').val();      
                    }
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'no' ,"searchable": false,"orderable": false},
                    { data: 'title', name: 'title' },
                    { data: 'type', name: 'type' },
                    { data: 'employee_name', name: 'employee_name' },
                    { data: 'atasan', name: 'atasan'}, // ini tes data = nama column nama = return val
                    { data: 'start', name: 'start' },
                    { data: 'end', name: 'end' },
                    { data: 'manager', name: 'manager' },
                    { data: 'hr', name: 'hr' },
                    { data: 'option', name: 'option' },
                ],
                initComplete : function(settings, json) {
                    if (urlParams.has('page') && urlParams.has('tab')){
                        currentTab = urlParams.get('tab');
                        if (currentTab=="myApproval"){
                            pageNext = Number(urlParams.get('page'));
                            dtTableOngoing.page(pageNext-1).draw(false);
                            dtTableOngoing.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                pageCurrent = pageNext-1+"1";
                                pageCurrent = Number(pageCurrent);
                                cell.innerHTML = i+1 * pageCurrent;
                            });
                        }
                    }
                    
                }

            });

            var dtTableCompleted = $('.traineeCompletedTable').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                pagingType: 'simple_numbers',
                ajax: {
                    url: "{{ route('dashboard.datatableTraineeRequest', ['type' => 'completed']) }}",
                    data: function (d) {
                        if (urlParams.has('page') && urlParams.has('tab')){
                            currentTab = urlParams.get('tab');
                            if (currentTab=="myHistory"){
                                if ($(".render").val()=="yes"){
                                    pageNext = Number(urlParams.get('page'));
                                    d.start = Number(pageNext-1 + "0");
                                }
                            }
                        }
                        d.month = $('#filterMonthCompleted').val();
                        d.year = $('#filterYearCompleted').val();                
                    }
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'no' ,"searchable": false,"orderable": false},
                    { data: 'title', name: 'title' },
                    { data: 'type', name: 'type' },
                    { data: 'employee_name', name: 'employee_name' },
                    { data: 'atasan', name: 'atasan'}, // ini tes data = nama column nama = return val
                    { data: 'start', name: 'start' },
                    { data: 'end', name: 'end' },
                    { data: 'manager', name: 'manager' },
                    { data: 'hr', name: 'hr' },
                    { data: 'option', name: 'option' },
                ],
                initComplete : function(settings, json) {
                    if (urlParams.has('page') && urlParams.has('tab')){
                        currentTab = urlParams.get('tab');
                        if (currentTab=="myHistory"){
                            pageNext = Number(urlParams.get('page'));
                            dtTableCompleted.page(pageNext-1).draw(false);
                            dtTableCompleted.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                pageCurrent = pageNext-1+"1";
                                pageCurrent = Number(pageCurrent);
                                cell.innerHTML = i+1 * pageCurrent;
                            });
                        }
                    }
                    

                }

            });

            var dtTableRejected = $('.traineeRejectedTable').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                pagingType: 'simple_numbers',
                ajax: {
                    url: "{{ route('dashboard.datatableTraineeRequest', ['type' => 'rejected']) }}",
                    data: function (d) {
                        if (urlParams.has('page')){
                            if ($(".render").val()=="yes"){
                                pageNext = Number(urlParams.get('page'));
                                d.start = Number(pageNext-1 + "0");
                            }
                        }
                        d.month = $('#filterMonthRejected').val();
                        d.year = $('#filterYearRejected').val();                
                    }
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'no' ,"searchable": false,"orderable": false},
                    { data: 'title', name: 'title' },
                    { data: 'type', name: 'type' },
                    { data: 'employee_name', name: 'employee_name' },
                    { data: 'atasan', name: 'atasan'}, // ini tes data = nama column nama = return val
                    { data: 'start', name: 'start' },
                    { data: 'end', name: 'end' },
                    { data: 'manager', name: 'manager' },
                    { data: 'hr', name: 'hr' },
                    { data: 'option', name: 'option' },
                ],
                initComplete : function(settings, json) {
                    if (urlParams.has('page')){
                        // pageNext = Number(urlParams.get('page'));
                        // dtTableRejected.page(pageNext-1).draw(false);
                        // dtTableRejected.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        //     pageCurrent = pageNext-1+"1";
                        //     pageCurrent = Number(pageCurrent);
                        //     cell.innerHTML = i+1 * pageCurrent;
                        // });
                    }
                    
                }

            });

            if (urlParams.has('tab')){
                currentTab = urlParams.get('tab');
                setTimeout(function(){
                    $("."+currentTab).trigger("click");
                }, 3000);
            }

            $('.traineeWaitingApprovalTable tbody').on('click', 'tr', function () {
                activeTab = $("ul > .nav-item").find('.nav-link.active').attr("href").replace("#","");
                currentPage = dtTableWaiting.page.info().page;
                currentPage = currentPage+1;
                var data = dtTableWaiting.row( this ).data();
                $("#page" + data.id).val(currentPage);
                var previewUrl = $("#preview" + data.id).attr("href").split("?");
                previewUrl = previewUrl[0];
                $("#preview" + data.id).attr("href", previewUrl+"?page="+currentPage+"&tab="+activeTab);
            });

            $('.traineeOngoingTable tbody').on('click', 'tr', function () {
                activeTab = $("ul > .nav-item").find('.nav-link.active').attr("href").replace("#","");
                currentPage = dtTableOngoing.page.info().page;
                currentPage = currentPage+1;
                var data = dtTableOngoing.row( this ).data();
                $("#page" + data.id).val(currentPage);
                var previewUrl = $("#preview" + data.id).attr("href").split("?");
                previewUrl = previewUrl[0];
                $("#preview" + data.id).attr("href", previewUrl+"?page="+currentPage+"&tab="+activeTab);
            });

            $('.traineeCompletedTable tbody').on('click', 'tr', function () {
                activeTab = $("ul > .nav-item").find('.nav-link.active').attr("href").replace("#","");
                currentPage = dtTableCompleted.page.info().page;
                currentPage = currentPage+1;
                var data = dtTableCompleted.row( this ).data();
                $("#page" + data.id).val(currentPage);
                var previewUrl = $("#preview" + data.id).attr("href").split("?");
                previewUrl = previewUrl[0];
                $("#preview" + data.id).attr("href", previewUrl+"?page="+currentPage+"&tab="+activeTab);
            });

            $('.traineeRejectedTable tbody').on('click', 'tr', function () {
                activeTab = $("ul > .nav-item").find('.nav-link.active').attr("href").replace("#","");
                currentPage = dtTableRejected.page.info().page;
                currentPage = currentPage+1;
                var data = dtTableRejected.row( this ).data();
                $("#page" + data.id).val(currentPage);
                var previewUrl = $("#preview" + data.id).attr("href").split("?");
                previewUrl = previewUrl[0];
                $("#preview" + data.id).attr("href", previewUrl+"?page="+currentPage+"&tab="+activeTab);
            });

            $('.dataTables_filter input').keyup(function(e) {
                console.log($(this).parent().parent().parent().parent().parent().find(".filterMonth").attr("id"));
                e.preventDefault();
                var value = $(this).val();
                if (value.length==0 || value.length>5) {
                    dtTableWaiting.ajax.reload();
                    dtTableOngoing.ajax.reload();
                    dtTableCompleted.ajax.reload();
                    dtTableRejected.ajax.reload();
                }
            });

            $('#filterMonthWaitingApproval, #filterYearWaitingApproval').on( 'change', function () {
                var year = $(document).find('#filterYearWaitingApproval').val();
                var month = $(document).find('#filterMonthWaitingApproval').val();
                $(document).find('#exportWaitingApproval').attr('href', "{{route('dashboard.traineeRequestExportTrainee')}}?type=new&year="+year+"&month="+month);
                dtTableWaiting.ajax.reload();
            });

            $('#filterMonthOngoing, #filterYearOngoing').on( 'change', function () {
                var year = $(document).find('#filterYearOngoing').val();
                var month = $(document).find('#filterMonthOngoing').val();
                $(document).find('#exportOngoing').attr('href', "{{route('dashboard.traineeRequestExportTrainee')}}?type=approved&year="+year+"&month="+month);
                dtTableOngoing.ajax.reload();
            });

            $('#filterMonthCompleted, #filterYearCompleted').on( 'change', function () {
                var year = $(document).find('#filterYearCompleted').val();
                var month = $(document).find('#filterMonthCompleted').val();
                $(document).find('#exportCompleted').attr('href', "{{route('dashboard.traineeRequestExportTrainee')}}?type=completed&year="+year+"&month="+month);
                dtTableCompleted.ajax.reload();
            });

            $('#filterMonthRejected, #filterYearRejected').on( 'change', function () {
                var year = $(document).find('#filterYearRejected').val();
                var month = $(document).find('#filterMonthRejected').val();
                $(document).find('#exportRejected').attr('href', "{{route('dashboard.traineeRequestExportTrainee')}}?type=rejected&year="+year+"&month="+month);
                dtTableRejected.ajax.reload();
            });
        

            $(document).on("click", ".reject_button_trainee", function () {
                currentPage = dtTableWaiting.page.info().page;
                currentPage = currentPage+1;
                
                if (currentPage){
                    $("#rejected_request_next_page").val(currentPage);
                } else {
                    $("#rejected_request_next_page").val(1);
                }

                 var requestgroup = $(this).data('requestgroup');
                 var userid = $(this).data('userid');
                 $(".modal-body #rejected_request_id").val( requestgroup );
                 $(".modal-body #rejected_user_id").val( userid );
            });

            pp = $('.page-item').hasClass('active');
            
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
						  dtTableWaiting.draw()
						  dtTableOngoing.draw()
						  dtTableCompleted.draw()
						  dtTableRejected.draw()
						})

        });

				
    </script>
@endsection
