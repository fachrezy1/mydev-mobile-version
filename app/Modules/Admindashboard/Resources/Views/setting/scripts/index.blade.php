<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
$("#form_import").on("submit",function(e){
	e.preventDefault();
	var formData = new FormData(this);
	urlPath = window.location.origin;
	var request = $.ajax({
	  url: '{{ route("dashboard.setting.importExcelToDb") }}',
	  type: "POST",
	  data:formData,
	  cache:false,
	  contentType: false,
	  processData: false
	});
	request.done(function(msg) {
		if(msg=="success"){
			$("#import_msg").removeClass("text-danger").addClass("text-success").html("Import Successfully &#10004;");
			setTimeout(function(){ 
				$("#importExcel").modal("hide");
				setTimeout(function(){ 
					window.location.href = "{{ route('dashboard.setting.index') }}";
				}, 1000);
			}, 1000);
		} else {
			if (typeof msg == "object"){
				$("#import_msg").removeClass("text-success").addClass("text-danger");
				for (i=0;i<msg.length;i++){
					$("#import_msg").append("Field '" + msg[i] + "' required <br/>");
				}
				setTimeout(function(){ 
					$("#import_msg").empty();
				}, 3000);
			}
		}
	});
	request.fail(function(jqXHR, textStatus) {
	  $("#import_msg").removeClass("text-success").addClass("text-danger").html("Import Failed &#10006;");
	});
});
</script>