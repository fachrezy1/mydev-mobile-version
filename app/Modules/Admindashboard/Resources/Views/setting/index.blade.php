@extends('core.admin.main')
@section('head')
@endsection
@section('content')
    @include('core.admin.header')

    <main class="main-content">
        <div class="main-inner">

            <div class="setting-main">
                <div class="container">
                    <div class="trainee-title">
                        <h1>Admin Settings</h1>
                        <h4>Ooredoo online course settings</h4>
                    </div>
                    <div class="setting-box">
                        <button type="button" class="setting-item" onclick="location.href='{{route('dashboard.banner.show')}}'">
                            <i class="icon-spam"></i>
                            <span>Banner Setting</span>
                        </button>
                        <button type="button" class="setting-item" onclick="location.href='{{route('dashboard.point.index')}}'">
                            <i class="icon-reward"></i>
                            <span>Points Setting</span>
                        </button>
                        <button type="button" class="setting-item" onclick="location.href='{{route('dashboard.setting.employee')}}'">
                            <i class="icon-group-1"></i>
                            <span>User Setting</span>
                        </button>
                        <button type="button" class="setting-item" onclick="location.href='{{route('dashboard.item.index')}}'">
                            <i class="icon-profiles"></i>
                            <span>Item Management</span>
                        </button>
                        <button type="button" class="setting-item" onclick="location.href='{{route('dashboard.voucher.index')}}'">
                            <i class="icon-document"></i>
                            <span>Voucher Management</span>
                        </button>
                        <button type="button" class="setting-item" data-toggle="modal" data-target="#importExcel">
                            <i class="icon-transfer-data-between-documents"></i>
                            <span>Import</span>
                        </button>
                        <button type="button" class="setting-item d-none" onclick="location.href='setting-account.html'">
                            <i class="icon-user-2"></i>
                            <span>Account Settings</span>
                        </button>

                    </div>
                </div>
            </div>

            <div id="importExcel" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Import Data</h4>
                  </div>
                  <form id="form_import" enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="form-group">
                        @csrf
                        <select class="form-control" name="request_type">
                            <option value="inhouse">
                                In House
                            </option>
                            <option value="project_assigment">
                                Project Assigment
                            </option>
                            <option value="mentoring">
                                Mentoring / Coaching
                            </option>
                            <option value="public_training">
                                Public Training
                            </option>
                            <option value="external_speaker">
                                External Speaker
                            </option>
                        </select>
                    </div>
                    <div class="form-field">
                        <input type="file" name="file_import" class="btn btn-default reset"><br/>
                        <small class="name-files text-danger" id="import_msg"></small>
                    </div>
                  </div>
                  <div class="modal-footer d-flex justify-content-between">
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                  </form>
                </div>

              </div>
            </div>


        </div>
    </main>
@include('admindashboard::setting.scripts.index')
@endsection
@section('styles')
<style>
	.setting-box {
		display: flex;
		flex-direction: row;
		flex-wrap: wrap
	}
	.setting-box .setting-item {
		width: calc(100% / 3 - 8px);
		box-shadow: 0 2px 4px #3333;
		display: flex;
		flex-direction: column;
		align-items: center;
	}
</style>
@endsection
@section('scripts')
    <script src="{{ url('js/setting.js') }}"></script>
@endsection
