@extends('core.admin.main')
@section('head')
@endsection
@section('content')
    @include('core.admin.header')
    <main class="main-content">
        <div class="main-dashboard">
            <div class="dashboard-full">

                <div class="dashboard-breadcrumb clean">
                    <div class="container-fluid">
                        <div class="sub-breadcrumb">
                            <div class="sub-left">
                                <a href="{{route('dashboard.point.index')}}">
                                <button type="button" class="btn btn-clean" >
                                    <i class="icon-back"></i>
                                </button>
                                Point edit
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container">

                    <div class="dashboard-entry">
                        <div class="panel-box mt-24 medium">
                            <div class="panel-title mb-36">
                                <h3 class="big-title">Edit Point</h3>
                            </div>

                            <div class="item-box">
                                <form action="{{route('dashboard.point.update')}}" method="post">
                                    @csrf
                                    <div class="form-group reset px-4 pb-0">
                                        <label class="control-label">Course Type</label>
                                        <div class="form-field">
                                            <input type="text" class="form-control" name="trainee" value="{{$trainee['name']}}" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group reset px-4 pb-0">
                                        <label class="control-label">Value</label>
                                        <div class="form-field">
                                            <input type="number" class="form-control" name="point" value="{{$trainee['point']}}" required>
                                        </div>
                                    </div>

                                    <div class="button-group">
                                        <button type="button" class="btn btn-default" onclick="location.href='{{route('dashboard.point.index')}}'">Cancel</button>
                                        <button type="submit" class="btn btn-default">Submit</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>

    </main>
@endsection
@section('scripts')
    <script src="{{ url('js/setting.js') }}"></script>
    @if (session('success'))
        <script>
            alert('{{session('success')}}')
        </script>
    @endif
@endsection
