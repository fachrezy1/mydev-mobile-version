@extends('core.admin.main')
@section('head')
@endsection
@section('content')
    @include('core.admin.header')

    <main class="main-content">
        <div class="main-inner secondary-nav">
            <div class="sub-menu has-option">
                <button type="button" class="btn btn-clean" onclick="location.href='{{route('dashboard.setting.index')}}'">
                    <i class="icon-back"></i> Back
                </button>
                <h1 class="text-center">Point Setting</h1>
                <button type="button" class="btn btn-clean" onclick="location.href='{{route('dashboard.setting.employee')}}'">
                    User Setting <i class="icon-next"></i>
                </button>
            </div>
            <div class="setting-page py-3">
                <div class="container">
                    <div class="tab-content">

                    	<ul class="nav nav-pills nav-pills-icons mt-0 p-0 mb-md-4" id="tab-nav" role="tablist">
                          <li class="nav-item inline">
                              <a class="nav-link active show" href="#currentPoint" role="tab" data-toggle="tab" aria-selected="true">
                                  <i class="icon-portfolio-1"></i> Current Point
                              </a>
                          </li>
                          <li class="nav-item inline">
                              <a class="nav-link" href="#history" role="tab" data-toggle="tab" aria-selected="false">
                                  <i class="icon-checklist"></i> Update History
                              </a>
                          </li>
                           <li class="nav-item inline">
                              <a class="nav-link" href="#multiplication" role="tab" data-toggle="tab" aria-selected="false">
                                  <i class="icon-checklist"></i> Multiplication <span class="d-none d-md-inline-block">Point</span>
                              </a>
                          </li>
                      </ul>

                        <!-- new -->
                        @if (session('success'))
                            <div class="dashboard-notify alert alert-success">
                                <div class="alert-icon">
                                    <i class="mdi mdi-comment-alert-outline"></i>
                                </div>
                                <div class="notify-content">
                                    <p>{{session('success')}}</p>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true"><i class="mdi mdi-close"></i></span>
                                    </button>
                                </div>
                            </div>
                        @endif
                        <!-- end new -->
                        <div class="tab-pane active show" id="currentPoint">
                            <div class="panel-box">
                                <div class="panel-title">
                                    <h3>Points Settings</h3>
                                    
                                    <!-- new btn -->
                                    <div class="mx-auto mx-md-0">
	                                    <a href="{{ route('dashboard.point.resetpoint') }}" class="reset"></a>
	                                    <a onclick="return confirm(this); return false;" href="javascript:void(0)" class="btn btn-default btn-md btn_block sw">Reset Point</a>
                                    </div>
                                    <!-- end btn -->
                                </div>
                                <div class="table-option custom">
                                    <div class="table-first">
                                        <div class="table-search has-advance">
                                            <div class="custom-search">
                                                <label class="control-label">Search points</label>
                                                <input id="searchItem" type="text" class="form-control reset" placeholder="enter keywords">
                                            </div>
                                            <div class="button-group">
                                                <button type="button" class="btn btn-default btn-adv"><i class="icon-list"></i> Advance Filter</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="table-advance">
                                        <div class="advance-search">
                                            <div class="form-group reset">
                                                <label class="control-label">Form Types</label>
                                                <input type="text" class="form-control" placeholder="enter title">
                                            </div>
                                            <div class="form-group reset">
                                                <label class="control-label">Update</label>
                                                <input type="text" class="form-control" placeholder="enter date">
                                            </div>
                                            <div class="form-group reset">
                                                <label class="control-label">Value</label>
                                                <input type="text" class="form-control" placeholder="example: 1500">
                                            </div>

                                        </div>
                                        <div class="button-group">
                                            <button type="button" class="btn btn-default btn-table">Cancel</button>
                                            <button type="button" class="btn btn-default">Search</button>
                                        </div>
                                    </div>
                                </div>
                                <table id="pointsTable" class="table table-hover table-striped" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th class="all">No</th>
                                        <th class="all">Form Types</th>
                                        <th class="all">Value</th>
                                        <th class="desktop">Update</th>
                                        <th class="desktop">Options</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @php($n = 1)
                                    @foreach($trainees as $key=>$trainee)
                                        @if($trainee['id'] == 2 || $trainee['id'] == 5 || $trainee['id'] == 6)
                                        <tr>
                                            <td>{{$n++}}</td>
                                            <td>
                                                {{$trainee['name']}}
                                            </td>
                                            <td>
                                                {{$trainee['point']}}
                                            </td>
                                            <td>
                                                <i class="icon-calendar"></i> {{\Carbon\Carbon::parse($trainee['updated_at'])->format('d D M, Y')}}
                                            </td>
                                            <td>
                                                <div class="table-button">
                                                    <button type="button" class="btn btn-default btn-sm m-2 my-md-0" onclick="location.href='{{route('dashboard.point.edit',$trainee['id'])}}'">
                                                        <i class="icon-edit-2"></i> Edit
                                                    </button>
                                                    <button type="button" class="btn btn-default btn-sm m-2 my-md-0" data-toggle="modal" data-target="#confirm">
                                                        <i class="icon-clean"></i> Delete
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="history">
                            <div class="panel-box">
                                <div class="panel-title">
                                    <h3>Points Update History</h3>
                                </div>
                                <div class="table-option custom">
                                    <div class="table-first">
                                        <div class="table-search has-advance">
                                            <div class="custom-search">
                                                <label class="control-label">Search points</label>
                                                <input id="searchItem" type="text" class="form-control reset" placeholder="enter keywords">
                                            </div>
                                            <div class="button-group">
                                                <button type="button" class="btn btn-default btn-adv"><i class="icon-list"></i> Advance Filter</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="table-advance">
                                        <div class="advance-search">
                                            <div class="form-group reset">
                                                <label class="control-label">Form Types</label>
                                                <input type="text" class="form-control" placeholder="enter title">
                                            </div>
                                            <div class="form-group reset">
                                                <label class="control-label">Update</label>
                                                <input type="text" class="form-control" placeholder="enter date">
                                            </div>
                                            <div class="form-group reset">
                                                <label class="control-label">Value</label>
                                                <input type="text" class="form-control" placeholder="example: 1500">
                                            </div>

                                        </div>
                                        <div class="button-group">
                                            <button type="button" class="btn btn-default btn-table">Cancel</button>
                                            <button type="button" class="btn btn-default">Search</button>
                                        </div>
                                    </div>
                                </div>
                                <table id="pointsTable" class="table table-hover table-striped" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th class="all">No</th>
                                        <th class="all">Form Types</th>
                                        <th class="all">Value</th>
                                        <th class="desktop">Last Update</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($point as $key=>$item)
                                        @if(isset($trainees_by_id[$item['id']][0]['name']) && ($item['trainee_id'] == 2 || $item['trainee_id'] == 5 ))
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>
                                                {{$trainees_by_id[$item['id']][0]['name']}}
                                            </td>
                                            <td>
                                                {{$item['point']}}
                                            </td>
                                            <td>
                                                <i class="icon-calendar"></i> {{\Carbon\Carbon::parse($item['updated_at'])->format('d D M, Y - h:m a')}}
                                            </td>
                                        </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    <div class="tab-pane  show" id="multiplication">
                            <div class="panel-box">
                                <div class="panel-title">
                                    <h3>Multiplication Point</h3>
                                    
                                    <!-- new btn -->
                                    <a href="{{ route('dashboard.point.resetpoint') }}" style="float: right;" class="reset"></a>

                                    
                                    <!-- end btn -->
                                </div>
                                <!-- <div class="table-option custom">
                                    
                                    <div class="table-first">
                                        <div class="table-search has-advance">
                                            <div class="custom-search">
                                                <label class="control-label">Search points</label>
                                                <input id="searchItem" type="text" class="form-control reset" placeholder="enter keywords">
                                            </div>
                                            <div class="button-group">
                                                <button type="button" class="btn btn-default btn-adv"><i class="icon-list"></i> Advance Filter</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="table-advance">
                                        <div class="advance-search">
                                            <div class="form-group reset">
                                                <label class="control-label">Form Types</label>
                                                <input type="text" class="form-control" placeholder="enter title">
                                            </div>
                                            <div class="form-group reset">
                                                <label class="control-label">Update</label>
                                                <input type="text" class="form-control" placeholder="enter date">
                                            </div>
                                            <div class="form-group reset">
                                                <label class="control-label">Value</label>
                                                <input type="text" class="form-control" placeholder="example: 1500">
                                            </div>

                                        </div>
                                        <div class="button-group">
                                            <button type="button" class="btn btn-default btn-table">Cancel</button>
                                            <button type="button" class="btn btn-default">Search</button>
                                        </div>
                                    </div>
                                
                                </div> -->
                               <!--  <div class="table-option custom">
                                            <div class="table-search has-advance">
                                                <div class="button-group">
                                                    
                                                   <button type="button" class="btn btn-default" onclick="location.href='{{route('dashboard.point.addMultiPoint')}}'"><i class="icon-plus"></i> Add new trainee multiplication</button>
                                               
                                                </div>
                                            </div>
                                        </div> -->
                                <table id="pointsTable" class="table table-hover table-striped" cellspacing="0" width="100%" style="margin-top:10px !important;">
                                    <thead>
                                    <tr>
                                      
                                        <th class="all">Trainee</th>
                                        <th class="desktop">Tier 1 & 2</th>
                                        <th class="desktop">Tier 3</th>
                                        <th class="desktop">Tier 4</th>
                                        <th class="desktop">Tier 5</th>
                                        <th class="all">Information</th>
                                        <th class="desktop">Options</th>

                                    </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($multiplication_point as $po)

                                          <tr>
                                              <td>{{$po->get_trainee->name}}</td>
                                              <td>{{$po->tier_2_1}}</td>
                                              <td>{{$po->tier_3}}</td>
                                              <td>{{$po->tier_4}}</td>
                                              <td>{{$po->tier_5}}</td>
                                              <td>{{$po->information}}</td>
                                              <td>
                                                  <div class="table-button">
                                                    <button type="button" class="btn btn-default btn-sm my-2" onclick="location.href='{{route('dashboard.point.editMultiPoint',$po->id)}}'">
                                                        <i class="icon-edit-2"></i> Edit
                                                    </button>
                                                </div>
                                              </td>
                                          </tr>
                                        @endforeach
                                   
                                    </tbody>
                                </table>
                            </div>
                        </div>



                    </div>
                </div>

            </div>





        </div>

    </main>

@endsection
@section('styles')
<style>
	#tab-nav .nav-link span.d-none {
		font-size: 14px;
		color: #909090;
		display: none!important
	}
	@media (min-width: 768px) {
		#tab-nav .nav-link span.d-none.d-md-inline-block {
			display: inline-block!important
		}
	}
	#pointsTable .table-button {
		justify-content: center;
	}
</style>
@endsection
@section('scripts')
    <script src="{{ url('js/setting.js') }}"></script>
    <script>
    function confirm(event){
            swal({
                title: 'Anda yakin akan melakukan reset point ?',
                showCancelButton: true,
                showConfirmButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                type: 'warning',
                buttonsStyling: false
            }).then(function (yes) {
                // Called if you click Yes.
                if (yes) {
                    
                    window.location.assign("{{route('dashboard.point.resetpoint')}}");
                }
            },
            function (no) {
                // Called if you click No.
                if (no == 'cancel') {
                }
            });
    }

    $(document).ready(function(){
        $(".btn_block").click(function(){
            //alert("ok");
            $.blockUI({ 
                message:"<img src='{{ url('images/loading.gif') }}' style='width:80px;height:80px;'><br>Loading...",
                css: {color : '#ffffff',
                background: 'none',
                border : 'none'}
            });
        });
    });
    </script>
@endsection
