@extends('core.admin.main')
@section('head')
@endsection
@section('content')
    @include('core.admin.header')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

@if (session('completed'))

    <script>

    $(document).ready(function(){
  
     swal("Good job!", "Update multiplication point successfully!", "success");
});

</script>
                                     
@endif
    <main class="main-content">
        <div class="main-dashboard">
            <div class="dashboard-full">
                <div class="dashboard-breadcrumb clean">
                    <div class="container-fluid">
                        <div class="sub-breadcrumb">
                            <div class="sub-left">
                                <button type="button" class="btn btn-clean" onclick="location.href='{{route('dashboard.point.index')}}'">
                                    <i class="icon-back"></i>
                                    Multiplication Point
                                 </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container">

                    <div class="dashboard-entry">
                        <div class="panel-box mt-24 medium">
                            <div class="panel-title mb-36">
                                <h3 class="big-title">{{$header}}</h3>
                            </div>

                            <div class="item-box">
                                <form action="{{route('dashboard.point.storeMultiPoint')}}" method="post">
                                @csrf

                                @if($status and $status=="add")
                                <div class="form-group reset split">
                                    <label class="control-label">Trainee</label>
                                    <div class="form-field">

                                        <select required class="form-control" name="id_trainee" id="id_trainee">
                                            <option value="">Select</option>
                                            @if($count_check==0 or $count_check==1)
                                             @foreach($trainees as $row)
                                                <option value="{{$row->id}}">{{$row->name}}</option>
                                             @endforeach
                                            @endif
                                        </select>

                                       
                                    </div>
                                </div>

                                @elseif($status and $status=="edit")

                                 <input type="hidden" name="id_trainee" id="id_trainee" @if($trainee_id) value="{{$trainee_id}}" @endif>

                                @endif
                                <div class="row mx-0">
                                	<div class="col-12 col-md-6">
                                		<div class="form-group reset px-2 pb-0">
                                			<label class="control-label">Tier 1 & 2</label>
                                			<div class="form-field">
                                				<input required class="form-control" name="tier_1" placeholder="Tier 1 & 2" id="tier_1" @if($tier_2_1) value="{{$tier_2_1}}" @endif>
                                			</div>
                                		</div>
                                	</div>
                                	<div class="col-12 col-md-6">
                                		<div class="form-group reset px-2 pb-0">
                                			<label class="control-label">Tier 3</label>
                                			<div class="form-field">
                                				<input required class="form-control" name="tier_3" placeholder="Tier 3" id="tier_3" @if($tier_3) value="{{$tier_3}}" @endif>
                                			</div>
                                		</div>
                                	</div>
                                </div>
                                <div class="row mx-0">
                                	<div class="col-12 col-md-6">
                                		<div class="form-group reset px-2 pb-0">
                                			<label class="control-label">Tier 4</label>
                                			<div class="form-field">
                                				<input required class="form-control" name="tier_4" placeholder="Tier 4" id="tier_4" @if($tier_4) value="{{$tier_4}}" @endif>
                                			</div>
                                		</div>
                                	</div>
                                	<div class="col-12 col-md-6">
                                		<div class="form-group reset px-2 pb-0">
                                			<label class="control-label">Tier 5</label>
                                			<div class="form-field">
                                				<input required class="form-control" name="tier_5" placeholder="Tier 5" id="tier_5" @if($tier_5) value="{{$tier_5}}" @endif>
                                			</div>
                                		</div>
                                	</div>
                                </div>
                                
                                <div class="row mx-0">
                                	<div class="col-12">
                                		<div class="form-group reset px-2 pb-0">
                                    <label class="control-label">information</label>
                                    <div class="form-field">
                                       <textarea class="form-control" name="information" placeholder="information" id="information">@if($information){{$information}}@endif</textarea>
                                    </div>
                                </div>

                                <input type="hidden" name="id_multiplication" @if($id_multiplication) value="{{$id_multiplication}}" @endif>
                                	</div>
                                </div>
                                
                                
                                <div class="button-group">
                                    <button type="button" class="btn btn-default" onclick="location.href='{{route('dashboard.item.index')}}'">Cancel</button>
                                    <button type="submit" class="btn btn-default">Submit</button>
                                </div>
                                </form>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>

    </main>
@endsection
@section('scripts')
    <script src="{{ url('js/setting.js') }}"></script>
@endsection
