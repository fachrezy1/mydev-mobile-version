@extends('core.admin.main')
@section('head')
@endsection
@section('content')
    @include('core.admin.header')
    <main class="main-content">
    	<div class="main-dashboard clean">
    		<div class="dashboard-breadcrumb clean">
  			<div class="container-fluid">
  				<div class="sub-menu has-option">
  					<button type="button" class="btn btn-clean" onclick="location.href='{{route('dashboard.setting.employee')}}'">
  						<i class="icon-back"></i> Back
  					</button>
  					<h1 class="text-center">User Detail</h1>
  				</div>
  			</div>
  		</div>

    		<div class="review-top">
    			<div class="container">
    				<div class="box-inline">
    					<div class="box-items">
    						<div class="profile-circle">
    							<div class="circle-thumbnail">
    								<img src="{{url('images/profile.jpeg')}}" alt="" class="img-fluid">
    							</div>
    							<div class="vertical-content text-center">
    								<h5>Your Name Here</h5>
    								<h3>Your Name Here</h3>
    							</div>
    						</div>
    					</div>
    					<div class="box-items">
    						<div class="vertical-content">
    							<h5>User info</h5>
    						</div>
    						<div class="trainer-data">
    							<ul class="list-unstyled">
    								<li>
    									<i class="icon-user-4"></i> <p><strong>NIK</strong> {{$employee['nik']}}</p>
    								</li>
    								<li>
    									<i class="icon-user-4"></i> <p><strong>Position</strong> {{$employee['position']}}</p>
    								</li>
    								<li>
    									<i class="icon-user-4"></i> <p><strong>Division</strong> {{$employee['division']}}</p>
    								</li>
    							</ul>
    						</div>
    					</div>
    					<div class="box-items">
    						<div class="vertical-content">
    							<h5>Date</h5>
    						</div>
    						<div class="trainer-data">
    							<ul class="list-unstyled">
    								<li>
    									<i class="icon-calendar-1"></i> <p><strong>Started</strong> {{\Carbon\Carbon::parse($employee['joining_date'])->format('d M, Y')}}</p>
    								</li>
    								<li>
    									<i class="icon-calendar-1"></i> <p><strong>Ended</strong> {{\Carbon\Carbon::parse($employee['retiring_date'])->format('d M, Y')}}</p>
    								</li>
    								<li>
    									<i class="icon-deal"></i> <p><strong>Status</strong> {{$employee['employee_status']}}</p>
    								</li>
    							</ul>
    							<button type="button" class="btn btn-default d-none">
    								<i class="icon-xlsx-file-format"></i> Export to excel
    							</button>
    						</div>
    					</div>

    					<div class="box-items">
    						<div class="vertical-content">
    							<h5>User Roles</h5>
    						</div>
    						<div class="trainer-data">
    							<div class="check-group px-1 py-2">
    								<div class="form-check">
    									<label class="form-check-label">
    										<input class="form-check-input" name="is_staff" type="radio" value="1" {{$employee['isStaff'] ? 'checked' : ''}} disabled> Staff
    										<span class="form-check-sign">
    											<span class="check"></span>
    										</span>
    									</label>
    								</div>
    								<div class="form-check">
    									<label class="form-check-label">
    										<input class="form-check-input" name="is_manager" type="radio" value="1" {{$employee['isManager'] ? 'checked' : ''}} disabled> Manager
    										<span class="form-check-sign">
    											<span class="check"></span>
    										</span>
    									</label>
    								</div>
    								<div class="form-check">
    									<label class="form-check-label">
    										<input class="form-check-input" name="is_admin" type="radio" value="1" {{$employee['isAdmin'] ? 'checked' : ''}} disabled> Admin
    										<span class="form-check-sign">
    											<span class="check"></span>
    										</span>
    									</label>
    								</div>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>

				{{--     		
				<div class="container">
    			<div class="profile-cost-points custom d-none">
    				<div class="profile-card">
    					<div class="info-course">
    						<i class="icon-analysis-1"></i>
    						<div class="profile-cost">
    							<span>Spent</span>
    							<span class="numeric">IDR 100.000.000</span>
    						</div>
    					</div>

    				</div>
    				<div class="profile-card">
    					<div class="info-course">
    						<i class="icon-podium"></i>
    						<div class="profile-cost">
    							<span>Points</span>
    							<span class="numeric">{{$employee['points']}} <em>points</em></span>
    						</div>
    					</div>

    				</div>
    				<div class="profile-card">
    					<div class="info-course">
    						<i class="icon-contract"></i>
    						<div class="profile-cost">
    							<span>Enrolled</span>
    							<span class="numeric">100</span>
    						</div>
    					</div>

    				</div>
    				<div class="profile-card">
    					<div class="info-course">
    						<i class="icon-training-1"></i>
    						<div class="profile-cost">
    							<span>Assign</span>
    							<span class="numeric">200</span>
    						</div>
    					</div>

    				</div>
    			</div>
    		</div>

    		<div class="container mt-5 pt-5"><br>
    			<div class="review-item">
    				<div class="sub-title">User Roles</div>
    				<div class="center-form">
    					<div class="form-group reset split">
    						<label class="control-label">
    						</label>
    						<div class="check-group">
    							<div class="form-check">
    								<label class="form-check-label">
    									<input class="form-check-input" name="is_staff" type="radio" value="1" {{$employee['isStaff'] ? 'checked' : ''}} disabled> Staff
    									<span class="form-check-sign">
    										<span class="check"></span>
    									</span>
    								</label>
    							</div>
    							<div class="form-check">
    								<label class="form-check-label">
    									<input class="form-check-input" name="is_manager" type="radio" value="1" {{$employee['isManager'] ? 'checked' : ''}} disabled> Manager
    									<span class="form-check-sign">
    										<span class="check"></span>
    									</span>
    								</label>
    							</div>
    							<div class="form-check">
    								<label class="form-check-label">
    									<input class="form-check-input" name="is_admin" type="radio" value="1" {{$employee['isAdmin'] ? 'checked' : ''}} disabled> Admin
    									<span class="form-check-sign">
    										<span class="check"></span>
    									</span>
    								</label>
    							</div>
    						</div>
    					</div>

    				</div>
    			</div>
    		</div> --}}


    	</div>

    </main>
@endsection
@section('styles')
<style>
	.review-top .box-inline .box-items {
		width: 80%
	}
	@media(min-width: 992px) {
		.review-top .box-inline .box-items {
			width: calc(100% / 2 - 16px)
		}
	}
</style>
@endsection
@section('scripts')
    <script src="{{ url('js/setting.js') }}"></script>
    @if (session('success'))
        <script>
            alert('{{session('success')}}')
        </script>
    @endif
@endsection
