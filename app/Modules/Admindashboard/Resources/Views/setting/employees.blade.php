@extends('core.admin.main')
@section('head')
@endsection
@section('content')
    @include('core.admin.header')
    <main class="main-content">
        <div class="main-inner secondary-nav">
            <div class="sub-menu has-option">
                <button type="button" class="btn btn-clean" onclick="location.href='{{route('dashboard.setting.index')}}'">
                    <i class="icon-back"></i> Back
                </button>
                <h1 class="text-center">Employee Setting</h1>
                <button type="button" class="btn btn-clean d-none" onclick="location.href='setting-report.html'">
                    Report Setting <i class="icon-next"></i>
                </button>
            </div>

            <div class="setting-page">
                <div class="container">
                    <ul class="nav nav-pills nav-pills-icons small mt-0 px-0 mt-md-4" id="tab-nav" role="tablist">

                        <li class="nav-item">
                            <a class="nav-link active show" href="#adminUser" role="tab" data-toggle="tab" aria-selected="false">
                                <i class="icon-teamwork"></i> <span>User List</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#requestAccess" role="tab" data-toggle="tab" aria-selected="false">
                                <i class="icon-verified"></i> <span>Access</span>
                             </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#adminRole" role="tab" data-toggle="tab" aria-selected="false">
                                <i class="icon-wrench"></i> <span>User Role </span>
                             </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#user_log" role="tab" data-toggle="tab" aria-selected="false">
                                <i class="icon-wrench"></i> <span>User Log </span>
                             </a>
                        </li>

                    </ul>
                    <!-- new -->
                    @if (session('success'))
                        <div class="dashboard-notify alert alert-success" style="margin-top: 10px;">
                            <div class="alert-icon">
                                <i class="mdi mdi-comment-alert-outline"></i>
                            </div>
                            <div class="notify-content">
                                <p>{{session('success')}}</p>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"><i class="mdi mdi-close"></i></span>
                                </button>
                            </div>
                        </div>
                    @endif
                    <!-- end new -->
                    <div class="tab-content tab-space">
                        <div class="tab-pane active show" id="adminUser">
                            <div class="panel-box">
                                <div class="panel-title">
                                    <h3>Employee List</h3>
                                    <!-- new btn -->
                                    <a href="{{ route('dashboard.setting.integrasi_odsys2') }}" class="btn btn-default btn-md btn_block mx-auto mb-4 mr-md-0 mb-md-0">Sync With ODSys</a>
                                    <!-- end btn -->
                                </div>
                                <table id="employeeList" class="table table-hover table-striped" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="all">Name</th>
                                            <th class="all">NIK</th>
                                            <th class="desktop">Division</th>
                                            <th class="desktop">Position</th>
                                            <th class="desktop">Options</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                        <div class="tab-pane" id="requestAccess">
                            <div class="panel-box">
                                <div class="panel-title">
                                    <h3>User Request Access</h3>
                                </div>
                                <table id="userRequestList" class="table table-hover table-striped" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>NIK</th>
                                            <th>Division<th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                        <div class="tab-pane" id="adminRole">
                            <div class="panel-box">
                                <div class="panel-title">
                                    <h3>User Role</h3>
                                </div>
                                <div class="table-option custom">
                                    <div class="table-advance">
                                        <div class="advance-search">
                                            <div class="form-group reset">
                                                <label class="control-label">Name</label>
                                                <input type="text" class="form-control" placeholder="enter keywords">
                                            </div>
                                            <div class="form-group reset">
                                                <label class="control-label">Position</label>
                                                <input type="text" class="form-control" placeholder="enter keywords">
                                            </div>
                                            <div class="form-group reset">
                                                <label class="control-label">NIK</label>
                                                <input type="text" class="form-control" placeholder="enter keywords">
                                            </div>

                                        </div>
                                        <div class="button-group">
                                            <button type="button" class="btn btn-default btn-table">Cancel</button>
                                            <button type="button" class="btn btn-default">Search</button>
                                        </div>
                                    </div>
                                </div>
                                <table id="roleTable" class="table table-hover table-striped" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Type</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($roles as $key=>$role)
                                            <tr>
                                                <td class="all">{{$key+1}}</td>
                                                <td class="all">
                                                    {{$role['name']}}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="tab-pane" id="user_log">
                            <div class="panel-box">
                                <div class="panel-title">
                                    <h3>User Log</h3>
                                </div>
                                <table id="log" class="table table-hover table-striped" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Deskripsi</th>
                                            <th>Username Yang dipakai</th>
                                            <th>Browser<th>
                                            <th>Internet Protocol</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                    </div>


                </div>
            </div>




        </div>
    </main>


    <div class="modal fade" id="blockingModal" tabindex="-1" role="dialog" aria-labelledby="blockingModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="blockingModalLabel">Blocking User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body blocking-message">
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script src="{{ url('js/setting.js') }}"></script>
    <!-- jquery block -->
    <script src="http://malsup.github.io/jquery.blockUI.js"></script>
    <!-- end jquery block -->
    @if (session('message'))
        <script>
            alert('{{session('message')}}')
        </script>
    @endif
    <script>
        $(document).ready(function(){
            $(".btn_block").click(function(){
                //alert("ok");
                $.blockUI({ 
                    message:"<img src='{{ url('images/loading.gif') }}' style='width:80px;height:80px;'><br>Loading...",
                    css: {color : '#ffffff',
                    background: 'none',
                    border : 'none'}
                });
            });
        });
        $(document).ready(function () {
            $('#employeeList').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                ajax: '{{route('dashboard.setting.employeeList')}}',
                columns: [
                    {data: 'full_name', name: 'full_name'},
                    {data: 'nik', name: 'nik'},
                    {data: 'division', name: 'division'},
                    {data: 'position', name: 'position'},
                    {data: 'options', name: 'options'}
                ]
            });
        });

        $(document).ready(function () {
            $('#log').DataTable({
                "order": [[ 4, "desc" ]],
                processing: true,
                serverSide: true,
                scrollX: true,
                ajax: '{{route('dashboard.setting.loglist')}}',
                columns: [
                    {data: 'description', name: 'description'},
                    {data: 'subject_username', name: 'subject_username'},
                    {data: 'browser', name: 'browser'},
                    {data: 'internet_protocol',title:'Internet Protocol'},
                    {data: 'created_at', title: 'Tanggal'}
                    //{data: 'options', name: 'options',searchable:false}
                ]
            });
        });

        $(document).ready(function () {
            var requestTable = $('#userRequestList').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                ajax: '{{route('dashboard.setting.userRequestList')}}',
                order: [],
                columns: [
                    {data: 'full_name', name: 'full_name'},
                    {data: 'nik', name: 'nik'},
                    {data: 'division', name: 'division'},
                    {data: 'position', name: 'position',title:'Position'},
                    {data: 'options', name: 'options',searchable:false}
                ],
                drawCallback: function () {
                    $(document).find('.blocking-user').click(function(){
                        var elem = $(this);
                        var targetUrl = $(this).attr('data-href');

                        $.ajax({
                            url: targetUrl,
                            type: "GET",
                            dataType: "JSON",
                            success: function(data){
                                if(!data.error){
                                    elem.attr('data-href', data.url);
                                    elem.html(data.block_text);   
                                    elem.removeClass('btn-default').addClass('btn-warning');   
                                }

                                $(document).find('.blocking-message').html(data.message);
                                $('#blockingModal').modal('show');
                                requestTable.draw();
                            }
                        });
                    });
                }
            });
        });
    </script>
@endsection
