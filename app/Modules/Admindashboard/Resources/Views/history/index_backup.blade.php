<div class="tab-pane " id="related">
    <div class="panel-box mt-24">
        <div class="panel-title">
            <h3>My Trainee</h3>
        </div>
        <div class="panel-table">
            {{--<div class="table-option">--}}
                {{--<div class="table-search">--}}
                    {{--<label class="control-label">Search data</label>--}}
                    {{--<input id="searchTable" type="text" class="form-control reset" placeholder="enter keywords">--}}
                {{--</div>--}}
            {{--</div>--}}
            <table class="traineeMytrainee table table-hover table-striped text-center" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Title/Topic</th>
                    <th>Type</th>
                    <th>Employee Name</th>
                    <th>Line Manager</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>Manager</th>
                    <th>HR.</th>
                    <th>Type of Programs</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @php ($n=0)
                {{--@dd($related_group)--}}
                @if(!isset($full_name))
                    @php($full_name = '')
                @endif
                @if($related_group['requests_group'])
                    @foreach($related_group['requests_group'] as $requests)
                        @foreach($requests as $key=>$history)
                            @if($history)
                                @foreach($history['approved'] as $idform=>$item)
                                    @php($n=$n+1)
                                    <tr>
                                        <td>{{$n}}</td>
                                        <td>
                                            @if($item['form_group_id'] == 1)
                                                @if($item['type']=='project')
                                                    {{ isset($item['project_title']) ? substr($item['project_title'], 0, 19) . '...' : ''}}
                                                @else
                                                    {{ isset($item['development_goal']) ? substr($item['development_goal'], 0, 19) . '...' : ''}}
                                                @endif
                                            @elseif($item['form_group_id'] == 4)
                                                {{ isset($item['line_topic']) ? substr($item['line_topic'], 0, 19) . '...' : ''}}
                                            @elseif($item['form_group_id'] == 3 )
                                                {{ isset($item['title']) ? substr($item['title'], 0, 19) . '...' : ''}}
                                            @elseif($item['form_group_id'] == 5 )
                                                {{ isset($item['title']) ? substr($item['title'], 0, 19) . '...' : ''}}
                                            @elseif($item['form_group_id'] == 2)
                                                {{ isset($item['development_goal']) ? substr($item['development_goal'], 0, 19) . '...' : ''}}
                                            @endif
                                        </td>
                                        <td>{{ucfirst(str_replace('_',' ',$form_type[$item['form_group_id']]))}}</td>
                                        <td>{{ $item['form_group_id'] == 2 ? $item['employee_name'] : $full_name  }}</td>
                                        
                                        <td>{{isset($item['manag']) ? $item['manag'] : ''}}</td>

                                        <td><i class="mdi mdi-calendar"></i>{{isset($item['start_date']) ? $item['start_date'] : ''}}</td>
                                        <td><i class="mdi mdi-calendar"></i>{{isset($item['end_date']) ? $item['end_date'] : ''}}</td>
                                        <td><span class="badge badge-pill {{isset($item['first_accepter_class']) ? $item['first_accepter_class'] : ''}}">
                                        {{isset($item['first_accepter'])?$item['first_accepter']:''}}</span></td>
                                        <td><span class="badge badge-pill {{isset($item['second_accepter_class']) ? $item['second_accepter_class'] : ''}}">
                                        {{isset($item['second_accepter'])?$item['second_accepter']:''}}</span></td>
                                        <td> {{isset($item['type_programs']) ? $item['type_programs'] : ( isset($item['type']) ? $item['type'] : '')}} </td>
                                        <td>

                                            <a href="{{route('trainee.previewByUserId',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}">
                                                <button type="submit" class="btn btn-default btn-sm btn-lite ">
                                                    <i class="icon-view"></i>
                                                    Preview </button>
                                            </a>
                                            @if($item['form_group_id'] == 5 && !$item['rating'])
                                                <button type="button" class="btn btn-default btn-sm btn-lite rating-modal"
                                                        data-requestgroup="{{$item['request_group_id']}}"
                                                        data-staffid="{{$item['user_id']}}"
                                                        data-mentorid="5"
                                                        data-toggle="modal"
                                                        data-target="#assignment">
                                                    <i class="icon-verified"></i> Done
                                                </button>
                                            @endif
                                            @if($item['form_group_id'] == 2 && $item['is_completed_by_manager'] != 1)
                                                <a href="{{route('trainee.reviewByUserId',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}">
                                                    <button type="button" class="btn btn-default btn-sm btn-lite">
                                                        <i class="icon-edit-1"></i> Result
                                                    </button>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                @foreach($history['completed'] as $idform=>$item)
                                    @php($n=$n+1)
                                    <tr>
                                        <td>{{$n}}</td>
                                        <td>{{ isset($item['title']) ? substr($item['title'], 0, 19) . '...' : ''}}</td>
                                        <td>{{ucfirst(str_replace('_',' ',$form_type[$item['form_group_id']]))}}</td>
                                        <td>{{ $item['form_group_id'] == 2 ? $item['employee_name'] : $full_name  }}</td>
                                        <td>{{isset($line_manager) ? $line_manager : ''}}</td>
                                        <td><i class="mdi mdi-calendar"></i>{{isset($item['start_date']) ? $item['start_date'] : ''}}</td>
                                        <td><i class="mdi mdi-calendar"></i>{{isset($item['end_date']) ? $item['end_date'] : ''}}</td>
                                        <td><span class="badge badge-pill {{isset($item['first_accepter_class']) ? $item['first_accepter_class'] : ''}}">
                                        {{isset($item['first_accepter'])?$item['first_accepter']:''}}</span></td>
                                        <td><span class="badge badge-pill {{isset($item['second_accepter_class']) ? $item['second_accepter_class'] : ''}}">
                                        {{isset($item['second_accepter'])?$item['second_accepter']:''}}</span></td>
                                        <td> {{isset($item['type_programs']) ? $item['type_programs'] : ( isset($item['type']) ? $item['type'] : '')}} </td>
                                        <td>

                                            <a href="{{route('trainee.previewByUserId',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}">
                                                <button type="submit" class="btn btn-default btn-sm btn-lite ">
                                                    <i class="icon-view"></i>
                                                    Preview </button>
                                            </a>
                                            @if($item['form_group_id'] == 5 && !$item['rating'])
                                                <button type="button" class="btn btn-default btn-sm btn-lite rating-modal"
                                                        data-requestgroup="{{$item['request_group_id']}}"
                                                        data-staffid="{{$item['user_id']}}"
                                                        data-mentorid="5"
                                                        data-toggle="modal"
                                                        data-target="#assignment">
                                                    <i class="icon-verified"></i> Done
                                                </button>
                                            @endif
                                            @if($item['form_group_id'] == 2 && $item['is_completed_by_manager'] != 1)
                                                <a href="{{route('trainee.reviewByUserId',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}">
                                                    <button type="button" class="btn btn-default btn-sm btn-lite">
                                                        <i class="icon-edit-1"></i> Result
                                                    </button>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        @endforeach
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
