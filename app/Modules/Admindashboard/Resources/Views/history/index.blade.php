<div class="tab-pane " id="related">
    <div class="panel-box mt-24">
        <div class="panel-title">
            <h3>My Trainee</h3>
        </div>
        <div class="panel-table">
            {{--<div class="table-option">--}}
                {{--<div class="table-search">--}}
                    {{--<label class="control-label">Search data</label>--}}
                    {{--<input id="searchTable" type="text" class="form-control reset" placeholder="enter keywords">--}}
                {{--</div>--}}
            {{--</div>--}}
            <table class="traineeMytrainee table table-hover table-striped text-center" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Title/Topic</th>
                    <th>Type</th>
                    <th>Employee Name</th>
                    <th>Line Manager</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>Manager</th>
                    <th>HR.</th>
                    <th>Type of Programs</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                
                </tbody>
            </table>
        </div>
    </div>
</div>
