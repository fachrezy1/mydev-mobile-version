@extends('core.admin.main')
@section('head')
    <link rel="stylesheet" href="{{url('css/admin/dashboard/app.css')}}">
@endsection
@php($user = \sentinel::check())
@section('content')
<main class="main-content">
    <div class="main-dashboard hr clean">
        <div class="dashboard-content col-12">
            <div class="container-fluid">
                <div class="dashboard-entry">
                    <div class="dashboard-title mt-5">
                        <h1>Welcome back, {{$user->first_name}}</h1>
                    </div>

                    <div class="trainee-stats">
                        <div class="stats-item">
                            <div class="stats-top">

                                <div class="stats-dual">
                                    <div class="stats-numeric">
                                        <span class="badge badge-danger">{{$requests['new']}}</span>
                                    </div>
                                    <div class="stats-text">
                                        <h3>Waiting For Approval</h3>
                                        Employee Course Request
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="stats-item">
                            <div class="stats-top">

                                <div class="stats-dual">
                                    <div class="stats-numeric">
                                        <span class="badge badge-danger">{{$requests['onProgress']}}</span>
                                    </div>
                                    <div class="stats-text">
                                        <h3>On Progress</h3>
                                        Employee Course Progress
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="stats-item">
                            <div class="stats-top">

                                <div class="stats-dual">
                                    <div class="stats-numeric">
                                        <span class="badge badge-danger">{{$requests['completed']}}</span>
                                    </div>
                                    <div class="stats-text">
                                        <h3>Completed Requests</h3>
                                        Employee Course Completed
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="stats-item">
                            <div class="stats-top">

                                <div class="stats-dual">
                                    <div class="stats-numeric">
                                        <span class="badge badge-danger">{{$requests['rejected']}}</span>
                                    </div>
                                    <div class="stats-text">
                                        <h3>Rejected Requests</h3>
                                        Employee Course Rejected
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="top-hr">
                        <div class="panel-box top-progress">
                            <div class="panel-title">
                                <h3>Course by Month</h3>

                                <select id="reportByFormGroupFilter" class="form-control select-year">
                                    @for($i=0;$i<=3;$i++)
                                        <option value="{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                    @endfor
                                </select>

                                <div class="export-chart">
                                    <button type="button" class="btn btn-default btn-sm btn-lite" id="reportByFormGroupPDF">
                                        <i class="icon-file"></i> PDF
                                    </button>

                                    <a href="#" class="btn btn-default btn-sm btn-lite" id="reportByFormGroupImage" download="course-by-month.png">
                                        <i class="icon-download"></i> Image
                                    </a>
                                    <a href="{{route('dashboard.exportCourseByMonth')}}" id="exportCourseByMonth" class="btn btn-default btn-sm btn-lite">
                                        <i class="icon-xlsx-file-format"></i> Excel
                                    </a>
                                </div>
                            </div>
                            <div class="manager-chart">
                                <canvas id="reportByFormGroup" height="400"></canvas>
                            </div>
                        </div>
                        <div class="panel-side top-progress">
                            <div class="panel-vertical">
                                <div class="panel-box">
                                    <div class="panel-title">
                                        <h3>Division Stats
                                        <small class="text-muted">Most  Active employee </small>
                                        </h3>
                                    </div>

                                    <div id="requestTable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                        <table id="requestTable" class="table table-hover table-striped dataTable no-footer dtr-inline" cellspacing="0" width="100%" role="grid" aria-describedby="requestTable_info" style="width: 100%;">
                                            <thead>
                                              <tr role="row">
                                                <th>Employee Name</th>
                                                <th>Total Request</th>
                                                <th>Total Completed</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($division_stats as $stat)
                                                    <tr role="row" class="odd">
                                                        <td tabindex="0" class="sorting_1">{{ $stat['employee_name'] }}</td>
                                                        <td>{{ $stat['on_progress'] }}</td>
                                                        <td>{{ $stat['completed'] }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                                <div class="panel-box">
                                    <div class="panel-title">
                                        <h3>Competency to be developed</h3>
                                    </div>

                                    <div class="panel-body">
		                                	<div class="dashboard-legends">
		                                    	<h6 class="pt-3">Legend:</h6>
		                                    	<dl class="mb-0">
		                                    		<dt class="child1">AG</dt>
		                                    		<dd class="">Assignment</dd><br>
		                                    		<dt class="child2">MC</dt>
		                                    		<dd class="">Mentoring/Coaching</dd><br>
		                                    		<dt class="child3">PT</dt>
		                                    		<dd class="">Public Training</dd><br>
		                                    		<dt class="child4">IH</dt>
		                                    		<dd class="">InHouse</dd>
		                                    	</dl>
		                                    </div>
		                                </div>

                                    <div id="requestTable_wrapper" class="table-competency dataTables_wrapper dt-bootstrap4 no-footer">

                                        <table id="requestTable" class="table table-hover table-striped dataTable no-footer dtr-inline" cellspacing="0" width="100%" role="grid" aria-describedby="requestTable_info" style="width: 100%;">
                                            <thead>
                                              <tr role="row">
                                                <th>Competency</th>
                                                <th><span class="d-inline-block d-md-none">AG</span><span class="d-none d-md-inline-block">Assignment</span></th>
                                                <th><span class="d-inline-block d-md-none">MC</span><span class="d-none d-md-inline-block">Mentoring Coaching</span></th>
                                                <th><span class="d-inline-block d-md-none">PT</span><span class="d-none d-md-inline-block">Public Training</span></th>
                                                <th><span class="d-inline-block d-md-none">IH</span><span class="d-none d-md-inline-block">InHouse</span></th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($competencies as $competency_name => $competency)
                                                    <tr role="row" class="odd">
                                                        <td tabindex="0" class="sorting_1">{{ ucwords(str_replace('_', ' ', $competency_name)) }}</td>
                                                        <td>{{ $competency[1] }}</td>
                                                        <td>{{ $competency[2] }}</td>
                                                        <td>{{ $competency[3] }}</td>
                                                        <td>{{ $competency[5] }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <button type="button" class="btn btn-float btn-fba">
                <i class="icon-back"></i>
            </button>

        </div>
    </div>

</main>
@endsection
@section('styles')
<style>
	.panel-body {
		background: #fff
	}
	.table thead th:not(:first-child),
	.table tbody td:not(:first-child) {
		text-align: center;
		min-width: 40px!important
	}
	.table-competency thead th:not(:first-child) span {
		color: #fff;
		font-weight: bold;
	}
	.table-competency thead th:nth-child(2){
		background-color: rgb(237,27,37);
	}
	.table-competency thead th:nth-child(3){
		background-color: rgb(255, 159, 64);
	}
	.table-competency thead th:nth-child(4){
		background-color: rgb(255,213,0);
	}
	.table-competency thead th:nth-child(5){
		background-color: rgb(216,216,216)
	}
	.table-competency thead th:nth-child(5) span { color: #333; }

</style>
@endsection
@section('scripts')
    {{-- <script src="{{url('js/manager-chart.js')}}"></script> --}}
    {{-- <script src="{{url('js/manager.js')}}"></script> --}}
    <script>
    	$(document).ready(function() {
					$('.table').DataTable({
						scrollX: true
					});
				});

        var reportChartByFormType = {
            type: 'line',
            data: {
                labels: [
                    @foreach($report['request_by_form_type'] as $key=>$item)
                        "{{$key}}",
                    @endforeach
                ],
                datasets: [{
                    data: [
                        @foreach($report['request_by_form_type'] as $key=>$item)
                            @if(is_array($item))
                            "{{ isset($item['External Speaker']) ? count($item['External Speaker']) : 0}}",
                        @else
                            0,
                        @endif
                        @endforeach
                    ],
                    label: 'External Speaker',
                    borderColor: "#3e95cd",
                    fill: false
                }, {
                    data: [
                        @foreach($report['request_by_form_type'] as $key=>$item)
                            @if(is_array($item))
                            "{{ isset($item['Public Training']) ? count($item['Public Training']) : 0}}",
                        @else
                            0,
                        @endif
                        @endforeach
                    ],
                    label:'Public Training',
                    borderColor: "#6a8da2",
                    fill: false
                }, {
                    data: [
                        @foreach($report['request_by_form_type'] as $key=>$item)
                            @if(is_array($item))
                            "{{ isset($item['Mentoring / Coaching']) ? count($item['Mentoring / Coaching']) : 0}}",
                        @else
                            0,
                        @endif
                        @endforeach
                    ],
                    label:'Mentoring / Coaching',
                    borderColor: "#6ba26e",
                    fill: false
                }, {
                    data: [
                        @foreach($report['request_by_form_type'] as $key=>$item)
                            @if(is_array($item))
                            "{{ isset($item['Assignment']) ? count($item['Assignment']) : 0}}",
                        @else
                            0,
                        @endif
                        @endforeach
                    ],
                    label:'Assignment',
                    borderColor: "#9aa26a",
                    fill: false
                }, {
                    data: [
                        @foreach($report['request_by_form_type'] as $key=>$item)
                            @if(is_array($item))
                            "{{ isset($item['Inhouse']) ? count($item['Inhouse']) : 0}}",
                        @else
                            0,
                        @endif
                        @endforeach
                    ],
                    label:'Inhouse',
                    borderColor: "#a27966",
                    fill: false
                }
                ]
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Course by Month'
                }}};



         window.chartColors = {
            0: 'rgb(237,27,37)',
            1: 'rgb(255, 159, 64)',
            2: 'rgb(255,213,0)',
            3: 'rgb(153,204,0)',
            4: 'rgb(101,196,219)'
        };

        var colors = ["rgb(237,27,37)", "rgb(255, 159, 64)", "rgb(255,213,0)",'rgb(153,204,0)'];


        var barChartData = {
            labels: [
                    @foreach($report['request_by_form_type'] as $key=>$item)
                        "{{$key}}",
                    @endforeach
                    ],
            datasets: [{
                    data: [
                        @foreach($report['request_by_form_type'] as $key=>$item)
                            @if(is_array($item))
                            "{{ isset($item['External Speaker']) ? count($item['External Speaker']) : 0}}",
                        @else
                            0,
                        @endif
                        @endforeach
                    ],
                    label: 'External Speaker',
                    backgroundColor: colors[3],
                    fill: false
                }, {
                    data: [
                        @foreach($report['request_by_form_type'] as $key=>$item)
                            @if(is_array($item))
                            "{{ isset($item['Public Training']) ? count($item['Public Training']) : 0}}",
                        @else
                            0,
                        @endif
                        @endforeach
                    ],
                    label:'Public Training',
                    backgroundColor: colors[2],
                    fill: false
                }, {
                    data: [
                        @foreach($report['request_by_form_type'] as $key=>$item)
                            @if(is_array($item))
                            "{{ isset($item['Mentoring / Coaching']) ? count($item['Mentoring / Coaching']) : 0}}",
                        @else
                            0,
                        @endif
                        @endforeach
                    ],
                    label:'Mentoring / Coaching',
                    backgroundColor: colors[1],
                    fill: false
                }, {
                    data: [
                        @foreach($report['request_by_form_type'] as $key=>$item)
                            @if(is_array($item))
                            "{{ isset($item['Assignment']) ? count($item['Assignment']) : 0}}",
                        @else
                            0,
                        @endif
                        @endforeach
                    ],
                    label:'Assignment',
                    backgroundColor: colors[0],
                    fill: false
                }, {
                    data: [
                        @foreach($report['request_by_form_type'] as $key=>$item)
                            @if(is_array($item))
                            "{{ isset($item['Inhouse']) ? count($item['Inhouse']) : 0}}",
                        @else
                            0,
                        @endif
                        @endforeach
                    ],
                    label:'Inhouse',
                    backgroundColor: colors[4],
                    fill: false
                }
            ]
        };

        window.onload = function() {
            var ctx = document.getElementById('reportByFormGroup').getContext('2d');
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
                    title: {
                        display: true,
                        text: 'Training Type'
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        xAxes: [{
                            stacked: true,                            
                        }],
                        yAxes: [{
                            stacked: true,
                            beginAtZero: true,
                            display: true
                        }]
                    },
                    animation: {
                      onComplete: function () {
                        var chartInstance = this.chart;
                        var ctx = chartInstance.ctx;
                        var height = chartInstance.controller.boxes[0].bottom;
                        // ctx.textAlign = "center";
                        // Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
                        //   var meta = chartInstance.controller.getDatasetMeta(i);
                        //   Chart.helpers.each(meta.data.forEach(function (bar, index) {
                        //     if(dataset.data[index] > 0){
                        //         ctx.fillText( dataset.data[index], bar._model.x, bar._model.y + 20  );    
                        //     }
                        //   }),this)
                        // }),this);

                        var canvasLink = document.getElementById('reportByFormGroup').toDataURL();
                        $('#reportByFormGroupImage').attr('href', canvasLink);
                      }
                    }

                }
            });
        };

        $('#reportByFormGroupPDF').click(function(){
            downloadPDF('reportByFormGroup', 'course-by-month');
        });

        $('#reportByFormGroupFilter').change(function(){
                var year = $(this).val();

                $.ajax({
                    method: "GET",
                    dataType: "json",
                    url: "{{route('dashboard.filterChartValue')}}",
                    data: { year : year }
                }).done(function( response ) {
                    var dataExternalSpeaker = [];
                    var dataPublicTraining = [];
                    var dataMentoringCoaching = [];
                    var dataAssignment = [];
                    var dataInhouse = [];
                    var idx = 0;

                    $(document).find('#exportCourseByMonth').attr('href', "{{route('dashboard.exportCourseByMonth')}}?year="+year);
                    $(document).find("#reportByFormGroupContainer").html('<canvas id="reportByFormGroup" class="d-block d-md-none" height="500"></canvas>');

                    $.each(response.report.request_by_form_type, function(key, value) {  
                        if(value !== null){   
                            dataExternalSpeaker[idx] = ((typeof value['External Speaker'] !== "undefined") ? value['External Speaker'].length : 0);
                        }else{
                            dataExternalSpeaker[idx] = 0;
                        }

                        idx++;
                    })

                    idx = 0;
                    $.each(response.report.request_by_form_type, function(key, value) {  
                        if(value !== null){   
                            dataPublicTraining[idx] = ((typeof value['Public Training'] !== "undefined") ? value['Public Training'].length : 0);
                        }else{
                            dataPublicTraining[idx] = 0;
                        }

                        idx++;
                    })
                    
                    idx = 0;
                    $.each(response.report.request_by_form_type, function(key, value) {  
                        if(value !== null){   
                            dataMentoringCoaching[idx] = ((typeof value['Mentoring / Coaching'] !== "undefined") ? value['Mentoring / Coaching'].length : 0);
                        }else{
                            dataMentoringCoaching[idx] = 0;
                        }

                        idx++;
                    })
                    
                    idx = 0;
                    $.each(response.report.request_by_form_type, function(key, value) {  
                        if(value !== null){   
                            dataAssignment[idx] = ((typeof value['Assignment'] !== "undefined") ? value['Assignment'].length : 0);
                        }else{
                            dataAssignment[idx] = 0;
                        }

                        idx++;
                    })

                    idx = 0;
                    $.each(response.report.request_by_form_type, function(key, value) {  
                        if(value !== null){   
                            dataInhouse[idx] = ((typeof value['Inhouse'] !== "undefined") ? value['Inhouse'].length : 0);
                        }else{
                            dataInhouse[idx] = 0;
                        }

                        idx++;
                    })

                    var colors = ["rgb(237,27,37)", "rgb(255, 159, 64)", "rgb(255,213,0)",'rgb(153,204,0)'];

                    var barChartData = {
                        labels: [
                            @foreach($report['request_by_form_type'] as $key=>$item)
                                "{{$key}}",
                            @endforeach
                        ],
                        datasets: [{
                                data: dataExternalSpeaker,
                                label: 'External Speaker',
                                backgroundColor: colors[3],
                                fill: false
                            }, {
                                data: dataPublicTraining,
                                label:'Public Training',
                                backgroundColor: colors[2],
                                fill: false
                            }, {
                                data: dataMentoringCoaching,
                                label:'Mentoring / Coaching',
                                backgroundColor: colors[1],
                                fill: false
                            }, {
                                data: dataAssignment,
                                label:'Assignment',
                                backgroundColor: colors[0],
                                fill: false
                            }, {
                                data: dataInhouse,
                                label:'Inhouse',
                                backgroundColor: colors[4],
                                fill: false
                            }
                        ]
                    };

                    var ctx = document.getElementById('reportByFormGroup').getContext('2d');
                    window.myBar = new Chart(ctx, {
                        type: 'bar',
                        data: barChartData,
                        options: {
                            title: {
                                display: true,
                                text: 'Training Type'
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false
                            },
                            responsive: true,
                            maintainAspectRatio: false,
                            scales: {
                                xAxes: [{
                                    stacked: true,                            
                                }],
                                yAxes: [{
                                    stacked: true,
                                    beginAtZero:true,
                                    display: false

                                }]
                            },
                            animation: {
                                onComplete: function () {
                                    var chartInstance = this.chart;
                                    var ctx = chartInstance.ctx;
                                    var height = chartInstance.controller.boxes[0].bottom;
                                    // ctx.textAlign = "center";
                                    // Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
                                    // var meta = chartInstance.controller.getDatasetMeta(i);
                                    // Chart.helpers.each(meta.data.forEach(function (bar, index) {
                                    //     if(dataset.data[index] > 0){
                                    //         ctx.fillText( dataset.data[index], bar._model.x, bar._model.y + 20  );    
                                    //     }
                                    // }),this)
                                    // }),this);

                                    var canvasLink = document.getElementById('reportByFormGroup').toDataURL();
                                    $('#reportByFormGroupImage').attr('href', canvasLink);
                                }
                            }
                        }
                    });
                });
            });
    </script>
@endsection
