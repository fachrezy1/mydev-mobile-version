@extends('core.admin.main')
@php($role= new \App\Helpers\UserRolesHelper())
@php($agent = new \Jenssegers\Agent\Agent())

@section('content')
    <main class="main-content">
        <div class="main-dashboard">
            <div class="dashboard-content w-100">
                <div class="container">

                    <div class="dashboard-entry">
                        <div class="dashboard-title">
                            <h1>Trainee Request</h1>
                        </div>

                        @if (session('success'))
                            <div class="dashboard-notify alert alert-success">
                                <div class="alert-icon">
                                    <i class="mdi mdi-comment-alert-outline"></i>
                                </div>
                                <div class="notify-content">
                                    <p>{{session('success')}}</p>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true"><i class="mdi mdi-close"></i></span>
                                    </button>
                                </div>
                            </div>
                        @endif
                        @if($agent->isMobile())
                            <ul class="nav nav-pills nav-pills-icons mt-24" role="tablist">
                                <li class="nav-item col-12 inline">
                                    <a class="nav-link myTrainee active show" href="#myTrainee" role="tab" data-toggle="tab" aria-selected="true">
                                        <i class="icon-portfolio-1"></i> Waiting for Approval
                                    </a>
                                </li>

                                {{--@if ($user->hasAccess('trainee.manage'))--}}
                                <li class="nav-item col-12 inline">
                                    <a class="nav-link myApproval" href="#myApproval" role="tab" data-toggle="tab" aria-selected="false">
                                        <i class="icon-checklist"></i> Ongoing Progress
                                    </a>
                                </li>
                                {{--@endif--}}
                                <li class="nav-item col-12 inline">
                                    <a class="nav-link myHistory" href="#myHistory" role="tab" data-toggle="tab" aria-selected="false">
                                        <i class="icon-profiles"></i> Completed
                                    </a>
                                </li>
                                {{--@if ($user->hasAccess('trainee.manage'))--}}
                                <li class="nav-item col-12 inline">
                                    <a class="nav-link" href="#rejected" role="tab" data-toggle="tab" aria-selected="false">
                                        <i class="icon-profiles"></i> Rejected
                                    </a>
                                </li>
                                {{--@endif--}}
                            </ul>
                        @else
                            <ul class="nav nav-pills nav-pills-icons mt-24" role="tablist">
                                <li class="nav-item inline">
                                    <a class="nav-link myTrainee active show" href="#myTrainee" role="tab" data-toggle="tab" aria-selected="true">
                                        <i class="icon-portfolio-1"></i> Waiting for Approval
                                    </a>
                                </li>

                                {{--@if ($user->hasAccess('trainee.manage'))--}}
                                <li class="nav-item inline">
                                    <a class="nav-link myApproval" href="#myApproval" role="tab" data-toggle="tab" aria-selected="false">
                                        <i class="icon-checklist"></i> Ongoing Progress
                                    </a>
                                </li>
                                {{--@endif--}}
                                <li class="nav-item inline">
                                    <a class="nav-link myHistory" href="#myHistory" role="tab" data-toggle="tab" aria-selected="false">
                                        <i class="icon-contract"></i> Completed
                                    </a>
                                </li>
                                {{--@if ($user->hasAccess('trainee.manage'))--}}

                                <li class="nav-item inline">
                                    <a class="nav-link" href="#rejected" role="tab" data-toggle="tab" aria-selected="false">
                                        <i class="icon-exit"></i> Rejected
                                    </a>
                                </li>
                                {{--@endif--}}
                            </ul>
                        @endif


                        <div class="tab-content tab-space">
                            <div class="tab-pane active show" id="myTrainee">
                                <div class="panel-box mt-24">
                                    @if ($role->inAdmin())
                                        <div class="panel-title">
                                            <h3>Ongoing list</h3>
                                        </div>
                                    @else
                                        <div class="panel-title">
                                            <h3>Waiting for approval list</h3>
                                        </div>
                                    @endif
                                    <div class="panel-table">

                                        <div class="export-table export-mytrainee">
                                            <select id="filterMonthWaitingApproval" class="filterMonth form-control select-filter">
                                                <option value="">Filter Month</option>
                                                @for($i=1;$i<=12;$i++)
                                                    <option value="month-{{ date('m', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}">{{ date('F', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}</option>
                                                @endfor
                                            </select>

                                            <select id="filterYearWaitingApproval" class="filterYear form-control select-filter">
                                                <option value="">Filter Year</option>
                                                @for($i=0;$i<=3;$i++)
                                                    <option value="year-{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                                @endfor
                                            </select>

                                            <a href="{{route('dashboard.traineeRequestExportTrainee')}}?type=new" id="exportWaitingApproval" class="btn btn-default btn-sm btn-lite">
                                                <i class="icon-xlsx-file-format"></i> Excel
                                            </a>
                                        </div>
                                        <div class="clearfix"></div>
                                        
                                        <table class="table table-hover table-striped table-responsive traineeWaitingApprovalTable" cellspacing="0">
                                            <thead>
                                            <tr>
                                                <th width="10">No</th>
                                                <th>title/topic</th>
                                                <th>Type</th>
                                                <th>Employee Name</th>
                                                <th>Line Manager</th>
                                                <th>Start</th>
                                                <th>End</th>
                                                <th>Manager</th>
                                                <th>HR.</th>
                                                <th>Option</th>
                                            </tr>
                                            </thead>
                                            @if (!$role->inAdmin())
                                                <tbody>
                                                @php ($n=0)
                                                @if(isset($requests['new']))
                                                    @foreach($requests['new'] as $idform=>$item)
                                                        @php($n=$n+1)
                                                        <tr>

                                                            <td>{{$n}}</td>
                                                            <td>
                                                                @if($item['form_group_id'] == 1)
                                                                    @if(isset($item['type']) && $item['type']=='project')
                                                                        {{ isset($item['project_title']) ? limit_words($item['project_title'],19) : ''}}
                                                                    @else
                                                                        {{ isset($item['development_goal']) ? limit_words($item['development_goal'],19) : ''}}
                                                                    @endif
                                                                @elseif($item['form_group_id'] == 4)
                                                                    {{ isset($item['line_topic']) ? limit_words($item['line_topic'],19) : ''}}
                                                                @elseif($item['form_group_id'] == 3 )
                                                                    {{ isset($item['title']) ? limit_words($item['title'],19) : ''}}
                                                                @elseif($item['form_group_id'] == 2)
                                                                    {{ isset($item['development_goal']) ? limit_words($item['development_goal'],19) : ''}}
                                                                @endif
                                                            </td>
                                                            <td>{{ $form_type[$item['form_group_id']] == 'mentoring' ? 'Mentoring/Coaching': ucfirst(str_replace('_',' ',$form_type[$item['form_group_id']]))}}</td>
                                                            <td>{{ucfirst(isset($item['employee_name']) ? ( @unserialize($item['employee_name']) ? @unserialize($item['employee_name'])[0] : $item['employee_name'] ) : '' ) }}</td>
                                                            <td>
                                                                {{isset($line_manager) ? $line_manager : ''}}
                                                            </td>
                                                            <td>
                                                                <i class="mdi mdi-calendar"></i>{{isset($item['start_date']) ? $item['start_date'] : ''}}
                                                                @if(isset($item['start_date']))
                                                                    <div class="hidden"> month-{{ date('m', strtotime(str_replace('/', '-', $item['start_date']))) }}</div>
                                                                    <div class="hidden"> year-{{ date('Y', strtotime(str_replace('/', '-', $item['start_date']))) }}</div>
                                                                    <div class="hidden"> month-{{ date('m', strtotime(str_replace('/', '-', $item['start_date']))) }}year-{{ date('Y', strtotime(str_replace('/', '-', $item['start_date']))) }}</div>
                                                                @endif                                                            
                                                            </td>
                                                            <td><i class="mdi mdi-calendar"></i>{{isset($item['end_date']) ? $item['end_date'] : ''}}</td>
                                                            <td><span class="badge badge-pill {{isset($item['first_accepter_class']) ? $item['first_accepter_class'] : ''}}">
                                                                {{isset($item['first_accepter'])?$item['first_accepter']:''}}</span></td>
                                                            <td><span class="badge badge-pill {{isset($item['second_accepter_class']) ? $item['second_accepter_class'] : ''}}">
                                                                {{isset($item['second_accepter'])?$item['second_accepter']:''}}</span></td>
                                                            <td>
                                                                @if ($item['is_rejected'])
                                                                    <button type="button" class="btn btn-default btn-sm btn-lite disabled" title="request has been rejected" disabled>
                                                                        Request rejected
                                                                    </button>
                                                                @else
                                                                    @if ($item['accepted_by_me'])
                                                                        <button type="button" class="btn btn-default btn-sm btn-lite disabled" title="you have been approved this request" disabled>
                                                                            Approved
                                                                        </button>
                                                                    @else
                                                                        <form action="{{route('request.acceptRequest')}}" method="post" class="mb-0">
                                                                            @csrf
                                                                            <input type="hidden" name="user_id" value="{{$item['user_id']}}">
                                                                            <input type="hidden" name="id" value="{{$item['request_group_id']}}">
                                                                            <button type="submit" class="btn btn-success btn-sm btn-lite">
                                                                                <i class="icon-tick"></i>
                                                                                Approve
                                                                            </button>
                                                                        </form>
                                                                        <button type="submit" class="btn btn-danger btn-sm btn-lite reject_button_trainee" data-requestgroup="{{$item['request_group_id']}}" data-userid="{{$item['user_id']}}" data-toggle="modal" data-target="#rejectNotes">
                                                                            <i class="icon-close"></i>
                                                                            Reject </button>
                                                                    @endif
                                                                @endif
                                                                <a href="{{route('trainee.previewByUserId',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}">
                                                                    <button type="submit" class="btn btn-dark btn-sm btn-lite ">
                                                                        <i class="icon-view"></i>
                                                                        Preview </button>
                                                                </a>
                                                                @if($role->inAdmin())
                                                                    <a href="{{route('dashboard.trainee.edit.show',['type'=>$form_type[$item['form_group_id']],'user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}">
                                                                        <button type="button" class="btn btn-default btn-sm btn-lite">
                                                                            <i class="icon-edit-1"></i> Edit
                                                                        </button>
                                                                    </a>
                                                                @endif
                                                                <a href="{{route('trainee.export',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}" target="_blank">
                                                                    <button type="button" class="btn btn-default btn-sm btn-lite">
                                                                        <i class="icon-transfer-data-between-documents"></i> Export
                                                                    </button>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>

                                            @endif

                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="myApproval">
                                <div class="panel-box mt-24">
                                    <div class="panel-title">
                                        <h3>Ongoing Progress list</h3>
                                    </div>
                                    <div class="panel-table">
                                        <div class="export-table export-mytrainee">
                                            <select id="filterMonthOngoing" class="filterMonth form-control select-filter">
                                                <option value="">Filter Month</option>
                                                @for($i=1;$i<=12;$i++)
                                                    <option value="month-{{ date('m', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}">{{ date('F', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}</option>
                                                @endfor
                                            </select>

                                            <select id="filterYearOngoing" class="filterYear form-control select-filter">
                                                <option value="">Filter Year</option>
                                                @for($i=0;$i<=3;$i++)
                                                    <option value="year-{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                                @endfor
                                            </select>

                                            <a href="{{route('dashboard.traineeRequestExportTrainee')}}?type=approved" id="exportOngoing" class="btn btn-default btn-sm btn-lite">
                                                <i class="icon-xlsx-file-format"></i> Excel
                                            </a>
                                        </div>
                                        <div class="clearfix"></div>
                                        
                                        <table class="traineeOngoingTable table table-hover table-striped table-responsive" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th width="10">No</th>
                                                <th>title/topic</th>
                                                <th>Type</th>
                                                <th>Employee Name</th>
                                                <th>Line Manager</th>
                                                <th>Start</th>
                                                <th>End</th>
                                                <th>Manager</th>
                                                <th>HR.</th>
                                                <th>Option</th>
                                            </tr>
                                            </thead>
                                            @if (!$role->inAdmin())
                                                <tbody>
                                                @php ($n=0)
                                                @if(isset($requests['approved']))
                                                    @foreach($requests['approved'] as $idform=>$item)
                                                        @php($n=$n+1)
                                                        <tr>

                                                            <td>{{$n}}</td>
                                                            <td>
                                                                @if($item['form_group_id'] == 1)
                                                                    @if(isset($item['type']) && $item['type']=='project')
                                                                        {{ isset($item['project_title']) ? limit_words($item['project_title'],19) : ''}}
                                                                    @else
                                                                        {{ isset($item['development_goal']) ? limit_words($item['development_goal'],19) : ''}}
                                                                    @endif
                                                                @elseif($item['form_group_id'] == 4)
                                                                    {{ isset($item['line_topic']) ? limit_words($item['line_topic'],19) : ''}}
                                                                @elseif($item['form_group_id'] == 3 )
                                                                    {{ isset($item['title']) ? limit_words($item['title'],19) : ''}}
                                                                @elseif($item['form_group_id'] == 2)
                                                                    {{ isset($item['development_goal']) ? limit_words($item['development_goal'],19) : ''}}
                                                                @endif
                                                            </td>
                                                            <td>{{ $form_type[$item['form_group_id']] == 'mentoring' ? 'Mentoring/Coaching': ucfirst(str_replace('_',' ',$form_type[$item['form_group_id']]))}}</td>
                                                            <td>{{ucfirst(isset($item['employee_name']) ? ( @unserialize($item['employee_name']) ? @unserialize($item['employee_name'])[0] : $item['employee_name'] ) : '' ) }}</td>
                                                            <td>
                                                                {{isset($line_manager) ? $line_manager : ''}}
                                                            </td>
                                                            <td>
                                                                <i class="mdi mdi-calendar"></i>{{isset($item['start_date']) ? $item['start_date'] : ''}}
                                                                @if(isset($item['start_date']))
                                                                    <div class="hidden"> month-{{ date('m', strtotime(str_replace('/', '-', $item['start_date']))) }}</div>
                                                                    <div class="hidden"> year-{{ date('Y', strtotime(str_replace('/', '-', $item['start_date']))) }}</div>
                                                                    <div class="hidden"> month-{{ date('m', strtotime(str_replace('/', '-', $item['start_date']))) }}year-{{ date('Y', strtotime(str_replace('/', '-', $item['start_date']))) }}</div>
                                                                @endif                                                            
                                                            </td>
                                                            <td><i class="mdi mdi-calendar"></i>{{isset($item['end_date']) ? $item['end_date'] : ''}}</td>
                                                            <td><span class="badge badge-pill {{isset($item['first_accepter_class']) ? $item['first_accepter_class'] : ''}}">
                                                                {{isset($item['first_accepter'])?$item['first_accepter']:''}}</span></td>
                                                            <td><span class="badge badge-pill {{isset($item['second_accepter_class']) ? $item['second_accepter_class'] : ''}}">
                                                                {{isset($item['second_accepter'])?$item['second_accepter']:''}}</span></td>
                                                            <td>

                                                                <a href="{{route('trainee.previewByUserId',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}">
                                                                    <button type="submit" class="btn btn-dark btn-sm btn-lite ">
                                                                        <i class="icon-view"></i>
                                                                        Preview </button>
                                                                </a>
                                                                <a href="{{route('trainee.export',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}"  target="_blank">
                                                                    <button type="button" class="btn btn-default btn-sm btn-lite">
                                                                        <i class="icon-transfer-data-between-documents"></i> Export
                                                                    </button>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            @endif
                                        </table>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane" id="myHistory">
                                <div class="panel-box mt-24">
                                    <div class="panel-title">
                                        <h3>Completed list</h3>
                                    </div>
                                    <div class="panel-table">
                                        <div class="export-table export-mytrainee">
                                            <select id="filterMonthCompleted" class="filterMonth form-control select-filter">
                                                <option value="">Filter Month</option>
                                                @for($i=1;$i<=12;$i++)
                                                    <option value="month-{{ date('m', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}">{{ date('F', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}</option>
                                                @endfor
                                            </select>

                                            <select id="filterYearCompleted" class="filterYear form-control select-filter">
                                                <option value="">Filter Year</option>
                                                @for($i=0;$i<=3;$i++)
                                                    <option value="year-{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                                @endfor
                                            </select>

                                            <a href="{{route('dashboard.traineeRequestExportTrainee')}}?type=completed" id="exportCompleted" class="btn btn-default btn-sm btn-lite">
                                                <i class="icon-xlsx-file-format"></i> Excel
                                            </a>
                                        </div>
                                        <div class="clearfix"></div>
                                        
                                        <table class="traineeCompletedTable table table-hover table-striped table-responsive" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th width="10">No</th>
                                                <th>title/topic</th>
                                                <th>Type</th>
                                                <th>Employee Name</th>
                                                <th>Line Manager</th>
                                                <th>Start</th>
                                                <th>End</th>
                                                <th>Manager</th>
                                                <th>HR.</th>
                                                <th>Option</th>
                                            </tr>
                                            </thead>
                                            @if (!$role->inAdmin())
                                                <tbody>
                                                @php ($n=0)
                                                @if(isset($requests['completed']))
                                                    @foreach($requests['completed'] as $idform=>$item)
                                                        @php($n=$n+1)
                                                        <tr>
                                                            <td>{{$n}}</td>
                                                            <td>
                                                                @if($item['form_group_id'] == 1)
                                                                    @if(isset($item['type']) && $item['type']=='project')
                                                                        {{ isset($item['project_title']) ? limit_words($item['project_title'], 19) : ''}}
                                                                    @else
                                                                        {{ isset($item['development_goal']) ? limit_words($item['development_goal'], 19) : ''}}
                                                                    @endif
                                                                @elseif($item['form_group_id'] == 4)
                                                                    {{ isset($item['line_topic']) ? limit_words($item['line_topic'], 19) : ''}}
                                                                @elseif($item['form_group_id'] == 3 )
                                                                    {{ isset($item['title']) ? limit_words($item['title'], 19) : ''}}
                                                                @elseif($item['form_group_id'] == 2)
                                                                    {{ isset($item['development_goal']) ? limit_words($item['development_goal'], 19) : ''}}
                                                                @endif
                                                            </td>
                                                            <td>{{ $form_type[$item['form_group_id']] == 'mentoring' ? 'Mentoring/Coaching': ucfirst(str_replace('_',' ',$form_type[$item['form_group_id']]))}}</td>
                                                            <td>{{ucfirst(isset($item['employee_name']) ? ( @unserialize($item['employee_name']) ? @unserialize($item['employee_name'])[0] : $item['employee_name'] ) : '' ) }}</td>
                                                            <td>
                                                                {{isset($line_manager) ? $line_manager : ''}}
                                                            </td>
                                                            <td>
                                                                <i class="mdi mdi-calendar"></i>{{isset($item['start_date']) ? $item['start_date'] : ''}}
                                                                @if(isset($item['start_date']))
                                                                    <div class="hidden"> month-{{ date('m', strtotime(str_replace('/', '-', $item['start_date']))) }}</div>
                                                                    <div class="hidden"> year-{{ date('Y', strtotime(str_replace('/', '-', $item['start_date']))) }}</div>
                                                                    <div class="hidden"> month-{{ date('m', strtotime(str_replace('/', '-', $item['start_date']))) }}year-{{ date('Y', strtotime(str_replace('/', '-', $item['start_date']))) }}</div>
                                                                @endif                                                            
                                                            </td>
                                                            <td><i class="mdi mdi-calendar"></i>{{isset($item['end_date']) ? $item['end_date'] : ''}}</td>
                                                            <td><span class="badge badge-pill {{isset($item['first_accepter_class']) ? $item['first_accepter_class'] : ''}}">
                                                                {{isset($item['first_accepter'])?$item['first_accepter']:''}}</span></td>
                                                            <td><span class="badge badge-pill {{isset($item['second_accepter_class']) ? $item['second_accepter_class'] : ''}}">
                                                                {{isset($item['second_accepter'])?$item['second_accepter']:''}}</span></td>
                                                            <td>
                                                                <a href="{{route('trainee.previewByUserId',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}">
                                                                    <button type="submit" class="btn btn-dark btn-sm btn-lite ">
                                                                        <i class="icon-view"></i>
                                                                        Preview </button>
                                                                </a>
                                                                <a href="{{route('trainee.export',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}" target="_blank">
                                                                    <button type="button" class="btn btn-default btn-sm btn-lite">
                                                                        <i class="icon-transfer-data-between-documents"></i> Export
                                                                    </button>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            @endif
                                        </table>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane" id="rejected">
                                <div class="panel-box mt-24">
                                    <div class="panel-title">
                                        <h3>Rejected list</h3>
                                    </div>
                                    <div class="panel-table">
                                        <div class="export-table export-mytrainee">
                                            <select id="filterMonthRejected" class="filterMonth form-control select-filter">
                                                <option value="">Filter Month</option>
                                                @for($i=1;$i<=12;$i++)
                                                    <option value="month-{{ date('m', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}">{{ date('F', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}</option>
                                                @endfor
                                            </select>

                                            <select id="filterYearRejected" class="filterYear form-control select-filter">
                                                <option value="">Filter Year</option>
                                                @for($i=0;$i<=3;$i++)
                                                    <option value="year-{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                                @endfor
                                            </select>

                                            <a href="{{route('dashboard.traineeRequestExportTrainee')}}?type=rejected" id="exportRejected" class="btn btn-default btn-sm btn-lite">
                                                <i class="icon-xlsx-file-format"></i> Excel
                                            </a>
                                        </div>
                                        <div class="clearfix"></div>
                                        
                                        <table class="traineeRejectedTable table table-hover table-striped table-responsive" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th width="10">No</th>
                                                <th>title/topic</th>
                                                <th>Type</th>
                                                <th>Employee Name</th>
                                                <th>Line Manager</th>
                                                <th>Start</th>
                                                <th>End</th>
                                                <th>Manager</th>
                                                <th>HR.</th>
                                                <th>Notes</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if (!$role->inAdmin())
                                                @php ($n=0)
                                                @if(isset($requests['rejected']))
                                                    @foreach($requests['rejected'] as $idform=>$item)
                                                        @php($n=$n+1)
                                                        <tr>

                                                            <td>{{$n}}</td>
                                                            <td>
                                                                @if($item['form_group_id'] == 1)
                                                                    @if(isset($item['type']) && $item['type']=='project')
                                                                        {{ isset($item['project_title']) ? limit_words($item['project_title'], 19) : ''}}
                                                                    @else
                                                                        {{ isset($item['development_goal']) ? limit_words($item['development_goal'], 19) : ''}}
                                                                    @endif
                                                                @elseif($item['form_group_id'] == 4)
                                                                    {{ isset($item['line_topic']) ? limit_words($item['line_topic'], 19) : ''}}
                                                                @elseif($item['form_group_id'] == 3 )
                                                                    {{ isset($item['title']) ? limit_words($item['title'], 19) : ''}}
                                                                @elseif($item['form_group_id'] == 2)
                                                                    {{ isset($item['development_goal']) ? limit_words($item['development_goal'], 19) : ''}}
                                                                @endif
                                                            </td>
                                                            <td>{{ $form_type[$item['form_group_id']] == 'mentoring' ? 'Mentoring/Coaching': ucfirst(str_replace('_',' ',$form_type[$item['form_group_id']]))}}</td>
                                                            <td>{{ucfirst(isset($item['employee_name']) ? ( @unserialize($item['employee_name']) ? @unserialize($item['employee_name'])[0] : $item['employee_name'] ) : '' ) }}</td>
                                                            <td>
                                                                {{isset($line_manager) ? $line_manager : ''}}
                                                            </td>
                                                            <td>
                                                                <i class="mdi mdi-calendar"></i>{{isset($item['start_date']) ? $item['start_date'] : ''}}
                                                                @if(isset($item['start_date']))
                                                                    <div class="hidden"> month-{{ date('m', strtotime(str_replace('/', '-', $item['start_date']))) }}</div>
                                                                    <div class="hidden"> year-{{ date('Y', strtotime(str_replace('/', '-', $item['start_date']))) }}</div>
                                                                    <div class="hidden"> month-{{ date('m', strtotime(str_replace('/', '-', $item['start_date']))) }}year-{{ date('Y', strtotime(str_replace('/', '-', $item['start_date']))) }}</div>
                                                                @endif                                                            
                                                            </td>
                                                            <td><i class="mdi mdi-calendar"></i>{{isset($item['end_date']) ? $item['end_date'] : ''}}</td>
                                                            <td><span class="badge badge-pill {{isset($item['first_accepter_class']) ? $item['first_accepter_class'] : ''}}">
                                                                {{isset($item['first_accepter'])?$item['first_accepter']:''}}</span></td>
                                                            <td><span class="badge badge-pill {{isset($item['second_accepter_class']) ? $item['second_accepter_class'] : ''}}">
                                                                {{isset($item['second_accepter'])?$item['second_accepter']:''}}</span></td>
                                                            <td>
                                                                {{($item['rejected_notes'])}}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            @endif
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <button type="button" class="btn btn-float btn-fba">
                    <i class="mdi mdi-more"></i>
                </button>
            </div>
        </div>
    </main>

    @include('trainee-modal')
    <div class="modal fade" id="rejectNotes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Are you sure to reject it?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('request.rejectRequest')}}" method="post">
                        @csrf
                        <input type="hidden" name="user_id" id="rejected_user_id" value="">
                        <input type="hidden" name="id" id="rejected_request_id" value="">
                        <input type="hidden" name="tab" value="myTrainee">
                        <input type="hidden" name="next_page" id="rejected_request_next_page">
                        <div class="form-group reset ">
                            <div class="form-field">
                                <textarea class="form-control counter-text" name="rejected_notes" rows="6" placeholder="Notes for rejected trainee"></textarea>
                                <p class="form-helper text-count"><span class="counter-text-value">0</span>/255</p>
                            </div>
                        </div>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-default"> Confirm </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="render" class="render" value="yes">
@endsection
@section('scripts')
    <script>
        /** TABLE FILTER**/
        @if ($role->inAdmin())
        $( document ).ready(function() {
            $(document).on("click",".page-link",function(){
                $(".render").val("no");
                activeTab = $("ul > .nav-item").find('.nav-link.active').attr("href").replace("#","");
                dataidx = Number($(this).text())-1;

                if (typeof dataidx=="NaN"){
                    dataidx = Number($(this).data("data-id"))-1;
                }
                if(dataidx>0){
                    if (activeTab=="myTrainee"){
                        dtTableWaiting.page(dataidx).draw(false);
                        dtTableWaiting.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                            pageCurrent = dataidx+"1";
                            pageCurrent = Number(pageCurrent);
                            cell.innerHTML = i+1 * pageCurrent;
                        });
                    }

                    if (activeTab=="myApproval"){
                        dtTableOngoing.page(dataidx).draw(false);
                        dtTableOngoing.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                            pageCurrent = dataidx+"1";
                            pageCurrent = Number(pageCurrent);
                            cell.innerHTML = i+1 * pageCurrent;
                        });
                    }

                    if (activeTab=="rejected"){
                        dtTableRejected.page(dataidx).draw(false);
                        dtTableRejected.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                            pageCurrent = dataidx+"1";
                            pageCurrent = Number(pageCurrent);
                            cell.innerHTML = i+1 * pageCurrent;
                        });
                    }

                    if (activeTab=="myHistory"){
                        dtTableCompleted.page(dataidx).draw(false);
                        dtTableCompleted.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                            pageCurrent = dataidx+"1";
                            pageCurrent = Number(pageCurrent);
                            cell.innerHTML = i+1 * pageCurrent;
                        });
                    }
                }
            });

            var urlParams = new URLSearchParams(location.search);
            var dtTableWaiting = $('.traineeWaitingApprovalTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('dashboard.datatableTraineeRequest', ['type' => 'waiting']) }}",
                    data: function (d) {
                        if (urlParams.has('page') && urlParams.has('tab')){
                            currentTab = urlParams.get('tab');
                            if (currentTab=="myTrainee"){
                                if ($(".render").val()=="yes"){
                                    pageNext = Number(urlParams.get('page'));
                                    d.start = Number(pageNext-1 + "0");
                                }
                            }
                        }
                        d.month = $('.filterMonth').val();
                        d.year = $('.filterYear').val();                
                    }
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'no' ,"searchable": false,"orderable": false},
                    { data: 'title', name: 'title' },
                    { data: 'type', name: 'type' },
                    { data: 'employee_name', name: 'employee_name' },
                    { data: 'atasan', name: 'atasan'}, // ini tes data = nama column nama = return val
                    { data: 'start', name: 'start' },
                    { data: 'end', name: 'end' },
                    { data: 'manager', name: 'manager' },
                    { data: 'hr', name: 'hr' },
                    { data: 'option', name: 'option' },
                ],
                initComplete : function(settings, json) {
                    if (urlParams.has('page') && urlParams.has('tab')){
                        currentTab = urlParams.get('tab');
                        if (currentTab=="myTrainee"){
                            pageNext = Number(urlParams.get('page'));
                            dtTableWaiting.page(pageNext-1).draw(false);
                            dtTableWaiting.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                pageCurrent = pageNext-1+"1";
                                pageCurrent = Number(pageCurrent);
                                cell.innerHTML = i+1 * pageCurrent;
                            });
                        }
                    }
                    
                }
            })

            var dtTableOngoing = $('.traineeOngoingTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('dashboard.datatableTraineeRequest', ['type' => 'ongoing']) }}",
                    data: function (d) {
                        if (urlParams.has('page') && urlParams.has('tab')){
                            currentTab = urlParams.get('tab');
                                if (currentTab=="myApproval"){
                                    if ($(".render").val()=="yes"){
                                        pageNext = Number(urlParams.get('page'));
                                        d.start = Number(pageNext-1 + "0");
                                    }
                                }
                        }
                        d.month = $('.filterMonth').val();
                        d.year = $('.filterYear').val();                
                    }
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'no' ,"searchable": false,"orderable": false},
                    { data: 'title', name: 'title' },
                    { data: 'type', name: 'type' },
                    { data: 'employee_name', name: 'employee_name' },
                    { data: 'atasan', name: 'atasan'}, // ini tes data = nama column nama = return val
                    { data: 'start', name: 'start' },
                    { data: 'end', name: 'end' },
                    { data: 'manager', name: 'manager' },
                    { data: 'hr', name: 'hr' },
                    { data: 'option', name: 'option' },
                ],
                initComplete : function(settings, json) {
                    if (urlParams.has('page') && urlParams.has('tab')){
                        currentTab = urlParams.get('tab');
                        if (currentTab=="myApproval"){
                            pageNext = Number(urlParams.get('page'));
                            dtTableOngoing.page(pageNext-1).draw(false);
                            dtTableOngoing.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                pageCurrent = pageNext-1+"1";
                                pageCurrent = Number(pageCurrent);
                                cell.innerHTML = i+1 * pageCurrent;
                            });
                        }
                    }
                    
                }

            });

            var dtTableCompleted = $('.traineeCompletedTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('dashboard.datatableTraineeRequest', ['type' => 'completed']) }}",
                    data: function (d) {
                        if (urlParams.has('page') && urlParams.has('tab')){
                            currentTab = urlParams.get('tab');
                            if (currentTab=="myHistory"){
                                if ($(".render").val()=="yes"){
                                    pageNext = Number(urlParams.get('page'));
                                    d.start = Number(pageNext-1 + "0");
                                }
                            }
                        }
                        d.month = $('.filterMonth').val();
                        d.year = $('.filterYear').val();                
                    }
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'no' ,"searchable": false,"orderable": false},
                    { data: 'title', name: 'title' },
                    { data: 'type', name: 'type' },
                    { data: 'employee_name', name: 'employee_name' },
                    { data: 'atasan', name: 'atasan'}, // ini tes data = nama column nama = return val
                    { data: 'start', name: 'start' },
                    { data: 'end', name: 'end' },
                    { data: 'manager', name: 'manager' },
                    { data: 'hr', name: 'hr' },
                    { data: 'option', name: 'option' },
                ],
                initComplete : function(settings, json) {
                    if (urlParams.has('page') && urlParams.has('tab')){
                        currentTab = urlParams.get('tab');
                        if (currentTab=="myHistory"){
                            pageNext = Number(urlParams.get('page'));
                            dtTableCompleted.page(pageNext-1).draw(false);
                            dtTableCompleted.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                pageCurrent = pageNext-1+"1";
                                pageCurrent = Number(pageCurrent);
                                cell.innerHTML = i+1 * pageCurrent;
                            });
                        }
                    }
                    
                }

            });

            var dtTableRejected = $('.traineeRejectedTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('dashboard.datatableTraineeRequest', ['type' => 'rejected']) }}",
                    data: function (d) {
                        if (urlParams.has('page')){
                            if ($(".render").val()=="yes"){
                                pageNext = Number(urlParams.get('page'));
                                d.start = Number(pageNext-1 + "0");
                            }
                        }
                        d.month = $('.filterMonth').val();
                        d.year = $('.filterYear').val();                
                    }
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'no' ,"searchable": false,"orderable": false},
                    { data: 'title', name: 'title' },
                    { data: 'type', name: 'type' },
                    { data: 'employee_name', name: 'employee_name' },
                    { data: 'atasan', name: 'atasan'}, // ini tes data = nama column nama = return val
                    { data: 'start', name: 'start' },
                    { data: 'end', name: 'end' },
                    { data: 'manager', name: 'manager' },
                    { data: 'hr', name: 'hr' },
                    { data: 'option', name: 'option' },
                ],
                initComplete : function(settings, json) {
                    if (urlParams.has('page')){
                        // pageNext = Number(urlParams.get('page'));
                        // dtTableRejected.page(pageNext-1).draw(false);
                        // dtTableRejected.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        //     pageCurrent = pageNext-1+"1";
                        //     pageCurrent = Number(pageCurrent);
                        //     cell.innerHTML = i+1 * pageCurrent;
                        // });
                    }
                    
                }

            });

            if (urlParams.has('tab')){
                currentTab = urlParams.get('tab');
                setTimeout(function(){
                    $("."+currentTab).trigger("click");
                }, 3000);
            }

            $('.traineeWaitingApprovalTable tbody').on('click', 'tr', function () {
                activeTab = $("ul > .nav-item").find('.nav-link.active').attr("href").replace("#","");
                currentPage = dtTableWaiting.page.info().page;
                currentPage = currentPage+1;
                var data = dtTableWaiting.row( this ).data();
                $("#page" + data.id).val(currentPage);
                var previewUrl = $("#preview" + data.id).attr("href").split("?");
                previewUrl = previewUrl[0];
                $("#preview" + data.id).attr("href", previewUrl+"?page="+currentPage+"&tab="+activeTab);
            });

            $('.traineeOngoingTable tbody').on('click', 'tr', function () {
                activeTab = $("ul > .nav-item").find('.nav-link.active').attr("href").replace("#","");
                currentPage = dtTableOngoing.page.info().page;
                currentPage = currentPage+1;
                var data = dtTableOngoing.row( this ).data();
                $("#page" + data.id).val(currentPage);
                var previewUrl = $("#preview" + data.id).attr("href").split("?");
                previewUrl = previewUrl[0];
                $("#preview" + data.id).attr("href", previewUrl+"?page="+currentPage+"&tab="+activeTab);
            });

            $('.traineeCompletedTable tbody').on('click', 'tr', function () {
                activeTab = $("ul > .nav-item").find('.nav-link.active').attr("href").replace("#","");
                currentPage = dtTableCompleted.page.info().page;
                currentPage = currentPage+1;
                var data = dtTableCompleted.row( this ).data();
                $("#page" + data.id).val(currentPage);
                var previewUrl = $("#preview" + data.id).attr("href").split("?");
                previewUrl = previewUrl[0];
                $("#preview" + data.id).attr("href", previewUrl+"?page="+currentPage+"&tab="+activeTab);
            });

            $('.traineeRejectedTable tbody').on('click', 'tr', function () {
                activeTab = $("ul > .nav-item").find('.nav-link.active').attr("href").replace("#","");
                currentPage = dtTableRejected.page.info().page;
                currentPage = currentPage+1;
                var data = dtTableRejected.row( this ).data();
                $("#page" + data.id).val(currentPage);
                var previewUrl = $("#preview" + data.id).attr("href").split("?");
                previewUrl = previewUrl[0];
                $("#preview" + data.id).attr("href", previewUrl+"?page="+currentPage+"&tab="+activeTab);
            });

                $('.dataTables_filter input').keyup(function(e) {
                    console.log($(this).parent().parent().parent().parent().parent().find(".filterMonth").attr("id"));
                    e.preventDefault();
                    var value = $(this).val();
                    if (value.length==0 || value.length>5) {
                        dtTableWaiting.ajax.reload();
                        dtTableOngoing.ajax.reload();
                        dtTableCompleted.ajax.reload();
                        dtTableRejected.ajax.reload();
                    }
                });

                $('#filterMonthWaitingApproval, #filterYearWaitingApproval').on( 'change', function () {
                    var year = $(document).find('#filterYearWaitingApproval').val();
                    var month = $(document).find('#filterMonthWaitingApproval').val();
                    $(document).find('#exportWaitingApproval').attr('href', "{{route('dashboard.traineeRequestExportTrainee')}}?type=new&year="+year+"&month="+month);
                    dtTableWaiting.ajax.reload();
                });

                $('#filterMonthOngoing, #filterYearOngoing').on( 'change', function () {
                    var year = $(document).find('#filterYearOngoing').val();
                    var month = $(document).find('#filterMonthOngoing').val();
                    $(document).find('#exportOngoing').attr('href', "{{route('dashboard.traineeRequestExportTrainee')}}?type=approved&year="+year+"&month="+month);
                    dtTableOngoing.ajax.reload();
                });

                $('#filterMonthCompleted, #filterYearCompleted').on( 'change', function () {
                    var year = $(document).find('#filterYearCompleted').val();
                    var month = $(document).find('#filterMonthCompleted').val();
                    $(document).find('#exportCompleted').attr('href', "{{route('dashboard.traineeRequestExportTrainee')}}?type=completed&year="+year+"&month="+month);
                    dtTableCompleted.ajax.reload();
                });

                $('#filterMonthRejected, #filterYearRejected').on( 'change', function () {
                    var year = $(document).find('#filterYearRejected').val();
                    var month = $(document).find('#filterMonthRejected').val();
                    $(document).find('#exportRejected').attr('href', "{{route('dashboard.traineeRequestExportTrainee')}}?type=rejected&year="+year+"&month="+month);
                    dtTableRejected.ajax.reload();
                });
            

                $(document).on("click", ".reject_button_trainee", function () {
                    currentPage = dtTableWaiting.page.info().page;
                    currentPage = currentPage+1;
                    
                    if (currentPage){
                        $("#rejected_request_next_page").val(currentPage);
                    } else {
                        $("#rejected_request_next_page").val(1);
                    }

                     var requestgroup = $(this).data('requestgroup');
                     var userid = $(this).data('userid');
                     $(".modal-body #rejected_request_id").val( requestgroup );
                     $(".modal-body #rejected_user_id").val( userid );
                });

                pp = $('.page-item').hasClass('active');
                console.log(pp);

            });
            
        @else
            var waitingApprovalTable = $('.traineeWaitingApprovalTable').DataTable({
                "pageLength": 5,
                "responsive" : true,
                "columnDefs": [
                    { "orderable": false, "targets": 8 }
                ]
            });

            $('#filterMonthWaitingApproval, #filterYearWaitingApproval').on( 'change', function () {
                    var searchKey = this.value;
                    var year = $(document).find('#filterYearWaitingApproval').val();
                    var month = $(document).find('#filterMonthWaitingApproval').val();

                    if(year != '' && month != ''){
                        searchKey = month+year;
                    }

                    year = year.replace("year-", "");
                    month = month.replace("month-", "");

                    $(document).find('#exportWaitingApproval').attr('href', "{{route('dashboard.traineeRequestExportTrainee')}}?type=new&year="+year+"&month="+month);

                    waitingApprovalTable
                    .search( searchKey )
                    .draw();
                });

                var ongoingTable = $('.traineeOngoingTable').DataTable({
                "pageLength": 5,
                "responsive" : true,
                "columnDefs": [
                    { "orderable": false, "targets": 8 }
                ]
            });

            $('#filterMonthOngoing, #filterYearOngoing').on( 'change', function () {
                var searchKey = this.value;
                var year = $(document).find('#filterYearOngoing').val();
                var month = $(document).find('#filterMonthOngoing').val();

                if(year != '' && month != ''){
                    searchKey = month+year;
                }

                year = year.replace("year-", "");
                month = month.replace("month-", "");

                $(document).find('#exportOngoing').attr('href', "{{route('dashboard.traineeRequestExportTrainee')}}?type=approved&year="+year+"&month="+month);

                ongoingTable
                .columns( 5 )
                .search( searchKey )
                .draw();
            });

            var completedTable = $('.traineeCompletedTable').DataTable({
                "pageLength": 5,
                "responsive" : true,
                "columnDefs": [
                    { "orderable": false, "targets": 8 }
                ]
            });

            $('#filterMonthCompleted, #filterYearCompleted').on( 'change', function () {
                var searchKey = this.value;
                var year = $(document).find('#filterYearCompleted').val();
                var month = $(document).find('#filterMonthCompleted').val();

                if(year != '' && month != ''){
                    searchKey = month+year;
                }

                year = year.replace("year-", "");
                month = month.replace("month-", "");

                $(document).find('#exportCompleted').attr('href', "{{route('dashboard.traineeRequestExportTrainee')}}?type=completed&year="+year+"&month="+month);

                completedTable
                .columns( 5 )
                .search( searchKey )
                .draw();
            });

            var rejectedTable = $('.traineeRejectedTable').DataTable({
                "pageLength": 5,
                "responsive" : true,
                "columnDefs": [
                    { "orderable": false, "targets": 8 }
                ]
            });

            $('#filterMonthRejected, #filterYearRejected').on( 'change', function () {
                var searchKey = this.value;
                var year = $(document).find('#filterYearRejected').val();
                var month = $(document).find('#filterMonthRejected').val();

                if(year != '' && month != ''){
                    searchKey = month+year;
                }

                year = year.replace("year-", "");
                month = month.replace("month-", "");
                
                $(document).find('#exportRejected').attr('href', "{{route('dashboard.traineeRequestExportTrainee')}}?type=rejected&year="+year+"&month="+month);

                rejectedTable
                .columns( 5 )
                .search( searchKey )
                .draw();
            });
        @endif


    </script>
@endsection
