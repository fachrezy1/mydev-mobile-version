@extends('core.admin.main')
@section('head')
@endsection
@section('content')
    @include('core.admin.header')
    <main class="main-content">
        <div class="main-dashboard secondary-nav">
            <div class="dashboard-full">
                <div class="dashboard-breadcrumb clean">
                    <div class="container-fluid">
                        <div class="sub-breadcrumb">
                            <div class="sub-left">
                                <button type="button" class="btn btn-clean" onclick="location.href='{{route('dashboard.voucher.index')}}'">
                                    <i class="icon-back"></i>
                                		Voucher Management
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container">

                    <div class="dashboard-entry">
                        <div class="panel-box mt-24 medium">
                            <div class="panel-title mb-36">
                                <h3 class="big-title">Edit voucher</h3>
                            </div>

                            <div class="item-box">
                                <form action="{{route('dashboard.voucher.update')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{ $voucher->id }}">
                                <div class="form-group reset px-4">
                                    <label class="control-label">Voucher Name</label>
                                    <div class="form-field">
                                        <input type="text" name="name" maxlength="250" required class="form-control" value="{{ old('name') ? old('name') : $voucher->name  }}">
                                        @if ($errors->has('name'))
                                            <small class="form-text text-danger">{{ $errors->first('name') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group reset px-4">
                                    <label class="control-label">Voucher Description</label>
                                    <div class="form-field">
                                        <textarea class="form-control" name="description" maxlength="1200" rows="3">{{ old('description') ? old('description') : $voucher->description  }}</textarea>
                                        @if ($errors->has('description'))
                                            <small class="form-text text-danger">{{ $errors->first('description') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                	<div class="col-12 col-md-6">
                                		<div class="col-12">
                                			<div class="profile-cost-points custom mt-0">
                                				<div class="col-12">
                                					<h4> Voucher Code </h4>
                                				</div>

                                			</div>
                                		</div>
                                		<div class="form-group reset px-4">
                                			<label class="control-label" for="exampleFormControlFile1"></label>
                                			<div class="form-field">
                                				<input type="file" class="form-control-file" name="code" accept=".csv">
                                				<i>support extension : (*.csv)</i>

                                				@if ($errors->has('code'))
                                				<small class="form-text text-danger">{{ $errors->first('code') }}</small>
                                				@endif
                                			</div>

                                			<div class="col-12 my-4 px-0">
                                				<table id="voucher-code-table" class="table table-hover table-striped" cellspacing="0" width="100%">
                                					<thead>
                                					</thead>
                                				</table>
                                			</div>     
                                		</div>

                                		<div class="form-group reset">
                                			<label class="control-label" for="exampleFormControlFile1">Image</label>
                                			<img src="{{asset('uploads/'.$voucher->image )}}" class="img-fluid img-thumbnail">
                                			<div class="form-field">
                                				<input type="file" class="form-control-file" value="{{$voucher->image}}" name="image" id="exampleFormControlFile1">
                                			</div>
                                		</div>
                                	</div>
                                	<div class="col-12 col-md-6">
                                		<div class="form-group reset px-4">
                                			<label class="control-label">Amount</label>
                                			<div class="form-field">
                                				<input type="number" name="amount" maxlength="10" required class="form-control" value="{{ old('amount') ? old('amount') : (int)$voucher->amount }}">
                                				@if ($errors->has('amount'))
                                				<small class="form-text text-danger">{{ $errors->first('amount') }}</small>
                                				@endif
                                			</div>
                                		</div>

                                		<div class="form-group reset px-4">
                                			<label class="control-label">Point</label>
                                			<div class="form-field">
                                				<input type="number" name="point" maxlength="10" required class="form-control" value="{{ old('point') ? old('point') : (int)$voucher->point }}">
                                				@if ($errors->has('point'))
                                				<small class="form-text text-danger">{{ $errors->first('point') }}</small>
                                				@endif
                                			</div>
                                		</div>
                                		<div class="form-group reset px-4">
                                			<label class="control-label">Status</label>
                                			<div class="form-field">
                                				<div class="togglebutton">
                                					<label>
                                						Unpublished
                                						<input type="checkbox" name="published" {{ (!empty($errors->all()) && old('published') != null) || ($voucher->status == 1) ? 'checked' : '' }}>
                                						<span class="toggle"></span>
                                						Published
                                					</label>

                                					@if ($errors->has('published'))
                                					<small class="form-text text-danger">{{ $errors->first('published') }}</small>
                                					@endif
                                				</div>
                                			</div>
                                		</div>
                                	</div>
                                </div>

                                <div class="button-group">
                                    <button type="button" class="btn btn-default" onclick="location.href='{{route('dashboard.voucher.index')}}'">Cancel</button>
                                    <button type="submit" class="btn btn-default">Submit</button>
                                </div>
                                </form>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>

    </main>
@endsection

@section('styles')
<style>
	.toggler-labeler {
		width: 100%!important;
	}
	@media(min-width: 375px){
		.toggler-labeler {max-width: 240px}
	}
</style>
@endsection


@section('scripts')
    <script>
        $(document).ready(function () {
            $('#voucher-code-table').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                bFilter: false,
                pageLength:5,
                ajax: '{{ route('dashboard.voucher.voucherCodeDatatable', $voucher->id) }}',
                columns: [
                    {data: 'code', name: 'code', title:'Code'},
                    {data: 'username', name: 'username', title:'Username'},
                    {data: 'status', name: 'status', title:'Status'}
                ]
            });
        })
    </script>
@endsection
