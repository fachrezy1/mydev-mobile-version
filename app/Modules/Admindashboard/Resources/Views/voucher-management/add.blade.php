@extends('core.admin.main')
@section('head')
@endsection
@section('content')
    @include('core.admin.header')
    <main class="main-content">
        <div class="main-dashboard secondary-nav">
            <div class="dashboard-full">
                <div class="dashboard-breadcrumb clean">
                    <div class="container-fluid">
                        <div class="sub-breadcrumb">
                            <div class="sub-left">
                                <button type="button" class="btn btn-clean" onclick="location.href='{{route('dashboard.voucher.index')}}'">
                                    <i class="icon-back"></i>
                                		Voucher Management
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container">

                    <div class="dashboard-entry">
                        <div class="panel-box mt-24 medium">
                            <div class="panel-title mb-36">
                                <h3 class="big-title">Add new voucher</h3>
                            </div>

                            <div class="item-box">
                                <form action="{{route('dashboard.voucher.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group reset px-4">
                                    <label class="control-label">Voucher Name</label>
                                    <div class="form-field">
                                        <input type="text" name="name" maxlength="250" required class="form-control" value="{{ old('name') }}">
                                        @if ($errors->has('name'))
                                            <small class="form-text text-danger">{{ $errors->first('name') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group reset px-4">
                                    <label class="control-label">Voucher Description</label>
                                    <div class="form-field">
                                        <textarea class="form-control" name="description" maxlength="1200" rows="3">{{ old('name') }}</textarea>
                                        @if ($errors->has('description'))
                                            <small class="form-text text-danger">{{ $errors->first('description') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                	<div class="col-12 col-md-6">
                                		<div class="form-group reset px-4">
                                			<label class="control-label" for="exampleFormControlFile1">Voucher Code</label>
                                			<div class="form-field">
                                				<input type="file" class="form-control-file" required name="code" accept=".csv">
                                				<i>support extension : (*.csv)</i>

                                				@if ($errors->has('code'))
                                				<small class="form-text text-danger">{{ $errors->first('code') }}</small>
                                				@endif
                                			</div>
                                		</div>

                                		<div class="form-group reset px-4">
                                			<label class="control-label" for="exampleFormControlFile1">Image</label>
                                			<div class="form-field">
                                				<input type="file" class="form-control-file" required name="image" id="exampleFormControlFile1">
                                			</div>
                                		</div>
                                	</div>
                                	<div class="col-12 col-md-6">
                                		<div class="form-group reset px-4">
                                			<label class="control-label">Amount</label>
                                			<div class="form-field">
                                				<input type="number" name="amount" maxlength="10" required class="form-control" value="{{ old('amount') }}">
                                				@if ($errors->has('amount'))
                                				<small class="form-text text-danger">{{ $errors->first('amount') }}</small>
                                				@endif
                                			</div>
                                		</div>

                                		<div class="form-group reset px-4">
                                			<label class="control-label">Point</label>
                                			<div class="form-field">
                                				<input type="number" name="point" maxlength="10" required class="form-control" value="{{ old('point') }}">
                                				@if ($errors->has('point'))
                                				<small class="form-text text-danger">{{ $errors->first('point') }}</small>
                                				@endif
                                			</div>
                                		</div>
                                		<div class="form-group reset px-4">
                                			<label class="control-label">Status</label>
                                			<div class="form-field">
                                				<div class="togglebutton">
                                					<label class="toggler-labeler">
                                						Unpublished
                                						<input type="checkbox" name="published" {{ (!empty($errors->all()) && old('published') != null) || empty($errors->all()) ? 'checked' : '' }}>
                                						<span class="toggle"></span>
                                						Published
                                					</label>

                                					@if ($errors->has('published'))
                                					<small class="form-text text-danger">{{ $errors->first('published') }}</small>
                                					@endif
                                				</div>
                                			</div>
                                		</div>
                                	</div>
                                </div>

                                <div class="button-group">
                                    <button type="button" class="btn btn-default" onclick="location.href='{{route('dashboard.voucher.index')}}'">Cancel</button>
                                    <button type="submit" class="btn btn-default">Submit</button>
                                </div>
                                </form>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>

    </main>
@endsection

@section('styles')
<style>
	.toggler-labeler {
		width: 100%!important;
	}
	@media(min-width: 375px){
		.toggler-labeler {max-width: 240px}
	}
</style>
@endsection
