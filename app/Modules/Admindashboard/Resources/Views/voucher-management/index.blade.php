@extends('core.admin.main')
@section('head')
@endsection
@section('content')
    @include('core.admin.header')
    <main class="main-content">
        <div class="main-dashboard secondary-nav">
            <div class="dashboard-full">
                <div class="sub-menu has-option">
                    <h1 class="text-center">Voucher Management</h1>
                </div>

                <div class="container">
                    @if (session('success') || session('error'))
                        <div class="dashboard-notify alert {{ session('success') ? 'alert-success' : 'alert-warning' }}" style="margin-top:20px;">
                            <div class="alert-icon">
                                <i class="mdi mdi-comment-alert-outline"></i>
                            </div>
                            <div class="notify-content">
                                <h4>{{ session('success') ? 'Success' : session('error') }}</h4>

                                @if(session('error'))
                                    @foreach(session('error_items') as $line => $error)
                                        <p>{{ 'Error at line '.($line + 2).' : '.$error }}</p>
                                    @endforeach
                                @else
                                    <p>{{session('success')}}</p>
                                @endif

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"><i class="mdi mdi-close"></i></span>
                                </button>
                            </div>
                        </div>
                    @endif
                    
                    <ul class="nav nav-pills nav-pills-icons mt-0 mt-md-4 px-0" id="tab-nav" role="tablist">
                        <li class="nav-item inline">
                            <a class="nav-link active show" href="#manage" role="tab" data-toggle="tab" aria-selected="true">
                                <i class="icon-portfolio-1"></i> Voucher Management
                            </a>
                        </li>
                        <li class="nav-item inline">
                            <a class="nav-link" href="#approved" role="tab" data-toggle="tab" aria-selected="true">
                                <i class="icon-portfolio-1"></i> Voucher History
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active show" id="manage">
                            <div class="panel-box mt-4">
                                <div class="panel-title d-flex mb-4">
                                    <h3>Voucher Management</h3>

                                    <div class="button-group mx-auto mx-md-0">
                                        <button type="button" class="btn btn-default" onclick="location.href='{{route('dashboard.voucher.create')}}'"><i class="icon-plus"></i> Add new voucher</button>
                                    </div>
                                </div>
                                <div class="panel-table">
                                    <table id="voucherManagement" class="table table-hover table-striped" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th class="all">Name</th>
                                            <th class="desktop">Description</th>
                                            <th class="all">Amount</th>
                                            <th class="desktop">Status</th>
                                            <th class="desktop">Options</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="approved">
                            <div class="panel-box mt-4">
                                <div class="panel-title d-flex mb-4">
                                    <h3>Voucher History</h3>
                                    <div class="export-table mx-auto mx-md-0 align-items-md-end">
		                                    <a href="{{route('dashboard.voucher.voucherExportHistory')}}" class="btn btn-default btn-sm btn-lite">
		                                        <i class="icon-xlsx-file-format"></i> Excel
		                                    </a>
		                                </div>
                                </div>
                                <div class="panel-table">
                                    <table id="voucherHistory" class="table table-hover table-striped" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="all">Name</th>
                                                <th class="all">Code</th>
                                                <th class="desktop">Description</th>
                                                <th class="desktop">Amount</th>
                                                <th class="desktop">Username</th>
                                                <th class="desktop">Used Date</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $('#voucherManagement').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                ajax: '{{ route('dashboard.voucher.voucherDatatable') }}',
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'description', name: 'description'},
                    {data: 'amount', name: 'amount', className: 'text-right'},
                    {data: 'status', name: 'status'},
                    {data: 'options', name: 'options',searchable:false}
                ]
            });

            $('#voucherHistory').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                ajax: '{{ route('dashboard.voucher.voucherHistoryDatatable') }}',
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'code', name: 'code'},
                    {data: 'description', name: 'description'},
                    {data: 'amount', name: 'amount', className: 'text-right'},
                    {data: 'username', name: 'username'},
                    {data: 'used_date', name: 'used_date'}
                ]
            });
        });
    </script>
@endsection
