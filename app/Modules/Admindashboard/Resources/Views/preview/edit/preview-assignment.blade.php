<input type="hidden" name="form_group_id" value="{{ $formType }}">
<input type="hidden" name="is_last" value="true">
<div class="center-form" style="margin-top: 200px;">
    <div class="form-group reset">
        <label class="control-label">
            Employee Name
        </label>
        <div class="form-field">
            <input type="text" class="form-control" value="{{$form_detail['employee_name']}}" readonly required>
        </div>
    </div>
    <div class="form-group reset">
        <label class="control-label">
            Employee Nik
        </label>
        <div class="form-field">
            <input type="text" class="form-control" value="{{$form_detail['employee_nik']}}" readonly required>
        </div>
    </div>
    <div class="form-group reset">
        <label class="control-label">
            Development Type
        </label>
        <div class="check-group">
                <div class="form-check">
                    <label class="form-check-label">
                        <input {{(isset($form_detail['project'])) ?'':'disabled'}} class="form-check-input" name="assignment_dev_type" type="radio" value="project" {{$form_detail['type']=='project'?'checked':''}}  required> Project
                        <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input {{(isset($form_detail['internship'])) ?'':'disabled'}} class="form-check-input" name="assignment_dev_type" type="radio" value="internship" {{$form_detail['type']=='internship'?'checked':''}} required> Cross Functional Assignment
                        <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input {{(isset($form_detail['opco'])) ?'':'disabled'}} class="form-check-input" name="assignment_dev_type" type="radio" value="globaltalent" {{(isset($form_detail['opco'])) ?'checked':''}} required> Global Talent Mobility / National Talent Mobility
                        <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                    </label>
                </div>
            </div>
    </div>
    <div class="form-group reset">
        <label class="control-label">
            <button type="button" data-toggle="modal" class="btn-question" data-target="#competencyModal">
                <i class="icon-questions-circular-button" >  </i>
            </button>
            Competency to be Developed
        </label>
        <div class="form-field mb-3">
            <select id="competency_select" class="form-control select2-comp" name="assignment_obj_employee_competency[]" multiple="multiple">
                @if (count($competencies)>0)
                    @foreach($competencies as $key => $c)
                        <optgroup label="{{ $c['option_group'] }}">
                            @foreach($c['option_relation'] as $opt_sub)
                                <option>
                                    {{ $opt_sub['name'] }}
                                </option>
                            @endforeach
                        </optgroup>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
        <div class="project-group {{ ($form_detail['type'] == 'project') ? '': 'd-none' }}">
            <div class="form-group reset ">
                <label class="control-label">Project Leader</label>
                <input name="assignment_pg_project_leader" type="text" class="form-control select-employee-name" value="{{ (isset($form_detail['project_leader'])) ? $form_detail['project_leader'] : null }}">
            </div>

            <div class="form-group reset">
                <label class="control-label">Project Title</label>
                <input name="assignment_pg_project_title" type="text" class="form-control" value="{{ (isset($form_detail['project_title'])) ? $form_detail['project_title'] : null }}">
            </div>

            <div class="form-group reset">
                <label class="control-label">Project Date</label>
                <div class="field-date">
                    <div class="date-input">
                        <input name="assignment_pg_start_date" type="text" autocomplete="off" class="form-control start-date" value="{{ (isset($form_detail['start_date'])) ? $form_detail['start_date'] : null }}" placeholder="Start date">
                        <button type="button" class="btn btn-default btn-icon "><i class="mdi mdi-calendar"></i></button>
                    </div>

                    To

                    <div class="date-input">
                        <input name="assignment_pg_end_date" type="text" autocomplete="off" class="form-control end-date" value="{{ (isset($form_detail['end_date'])) ? $form_detail['end_date'] : null }}" placeholder="End date">
                        <button type="button" class="btn btn-default btn-icon"><i class="mdi mdi-calendar"></i></button>
                    </div>
                </div>
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Project Description
                </label>
                <textarea rows="6" name="assignment_pg_project_description" class="form-control" maxlength="255" placeholder="Enter Mapped to Indosat Ooredoo Competency">{{ (isset($form_detail['project_description'])) ? $form_detail['project_description'] : null }}</textarea>
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Development Goal
                </label>
                <textarea rows="6" name="assignment_pg_development_goal" class="form-control" maxlength="255" placeholder="Enter Mapped to Indosat Ooredoo Competency"> {{ (isset($form_detail['development_goal'])) ? $form_detail['development_goal'] : null }} </textarea>
            </div>
        </div>
        <div class="internship {{ ($form_detail['type'] == 'internship') ? '': 'd-none' }}">

            <div class="form-group reset ">
                <label class="control-label">Group</label>
                <input name="assignment_inter_group" type="text" value="{{ (isset($form_detail['group'])) ? $form_detail['group'] : null }}" class="form-control">
            </div>

            <div class="form-group reset">
                <label class="control-label">Division</label>
                <input name="assignment_inter_division" type="text" value="{{(isset($form_detail['division'])) ? $form_detail['division'] : null}}" class="form-control">
            </div>

            <div class="form-group reset">
                <label class="control-label">Date</label>
                <div class="field-date">
                    <div class="date-input">
                        <input name="assignment_inter_start_date" type="text" autocomplete="off" class="form-control start-date"  value="{{ (isset($form_detail['start_date'])) ? $form_detail['start_date'] : null }}" placeholder="Start date">
                        <button type="button" class="btn btn-default btn-icon "><i class="mdi mdi-calendar"></i></button>
                    </div>

                    To

                    <div class="date-input">
                        <input name="assignment_inter_end_date" type="text" autocomplete="off" class="form-control end-date" value="{{ (isset($form_detail['end_date'])) ? $form_detail['end_date'] : null }}" placeholder="End date">
                        <button type="button" class="btn btn-default btn-icon"><i class="mdi mdi-calendar"></i></button>
                    </div>
                </div>
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Mentor name
                </label>
                <input name="assignment_inter_mentor_name" type="text" value="{{ (isset($form_detail['mentor_name'])) ? $form_detail['mentor_name'] : null }}" class="form-control select-employee-name">
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Mentor NIK
                </label>
                <input name="assignment_inter_mentor_nik" type="text" id="nik_coachee" class="form-control nik_coachee" readonly value="{{ (isset($form_detail['mentor_nik'])) ? $form_detail['mentor_nik'] : null }}">
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Development Goal
                </label>
                <textarea rows="6" name="assignment_inter_development_goal" class="form-control" maxlength="255" placeholder=""> {{ (isset($form_detail['development_goal'])) ? $form_detail['development_goal'] : null }}</textarea>
            </div>
        </div>

        <div class="globaltalent {{ (isset($form_detail['opco'])) ? '': 'd-none' }}">

            <div class="form-group reset ">
                <label class="control-label">Directorat/ Group/ OPCO Designation</label>
                <input name="assignment_gt_opco" type="text" value="{{ (isset($form_detail['opco'])) ? $form_detail['opco'] : null }}" class="form-control">
            </div>

            <div class="form-group reset">
                <label class="control-label">
                    Assigment Type
                </label>
                <input name="assignment_gt_type" id="shortterm_opt" data-val="shortterm" type="radio" value="shortterm" {{(isset($form_detail['type']) && $form_detail['type']=="shortterm") ? 'checked' : ''}}><label for="shortterm_opt" class="form-check-label">&nbsp;&nbsp;Short Term (3-12 Months)</label>&nbsp;
                <input name="assignment_gt_type" id="longterm_opt" data-val="longterm" type="radio" value="longterm" {{(isset($form_detail['type']) && $form_detail['type']=="longterm") ? 'checked' : ''}}><label for="longterm_opt" class="form-check-label">&nbsp;&nbsp;Long Term (1-2 Years)</label>
            </div>

            <div class="form-group reset">
                <label class="control-label">Date</label>
                <div class="field-date">
                    <div class="date-input">
                        <input name="assignment_gt_start_date" type="text" autocomplete="off" class="form-control start-date" value="{{ (isset($form_detail['start_date'])) ? $form_detail['start_date'] : null }}" placeholder="Start date">
                        <button type="button" class="btn btn-default btn-icon "><i class="mdi mdi-calendar"></i></button>
                    </div>

                    To

                    <div class="date-input">
                        <input name="assignment_gt_end_date" type="text" autocomplete="off" class="form-control end-date" value="{{ (isset($form_detail['end_date'])) ? $form_detail['end_date'] : null }}" placeholder="End date">
                        <button type="button" class="btn btn-default btn-icon"><i class="mdi mdi-calendar"></i></button>
                    </div>
                </div>
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Assigment Purpose
                </label>
                <input name="assignment_gt_purpose" data-val="Development" type="radio" value="Development" {{ (isset($form_detail['purpose']) && $form_detail['purpose']=="Development") ? "checked" : "" }}><label>&nbsp;&nbsp;Development</label>&nbsp;&nbsp;
                <input name="assignment_gt_purpose" data-val="Expert Skill Gap" type="radio" value="Expert Skill Gap" {{ (isset($form_detail['purpose']) && $form_detail['purpose']=="Expert Skill Gap") ? "checked" : "" }}><label>&nbsp;&nbsp;Expert Skill Gap</label>
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Reason For Mobility Request
                </label>
                <div id="Development_opt">
                    <label class="checkbox-inline"><input name="assignment_gt_reason[]" type="checkbox" value="Develop National Talent" {{ (isset($form_detail['reason']) && in_array("Develop National Talent", $form_detail['reason'])) ? "checked" : "" }}> Develop National Talent</label><br/>
                    <label class="checkbox-inline"><input name="assignment_gt_reason[]" type="checkbox" value="Exposure to New Technology" {{ (isset($form_detail['reason']) && in_array("Exposure to New Technology", $form_detail['reason'])) ? "checked" : "" }}> Exposure to New Technology</label><br/>
                    <label class="checkbox-inline"><input name="assignment_gt_reason[]" type="checkbox" value="Develop Management Skills" {{ (isset($form_detail['reason']) && in_array("Develop Management Skills", $form_detail['reason'])) ? "checked" : "" }}> Develop Management Skills</label><br/>
                    <label class="checkbox-inline"><input name="assignment_gt_reason[]" type="checkbox" value="Acquire New Skill Set" {{ (isset($form_detail['reason']) && in_array("Acquire New Skill Set", $form_detail['reason'])) ? "checked" : "" }}> Acquire New Skill Set</label><br/>
                    <label class="checkbox-inline"><input id="ag_gt_other" type="checkbox" value="Other" {{ (isset($form_detail['reason_other'])) ? "checked" : "" }}> Other</label>
                </div>
                <div id="skil_gap_opt">
                    <label class="checkbox-inline"><input name="assignment_gt_reason[]" type="checkbox" value="Business Expansion (New Role)" {{ (isset($form_detail['reason']) && in_array("Business Expansion (New Role)", $form_detail['reason'])) ? "checked" : "" }}> Business Expansion (New Role)</label><br/>
                    <label class="checkbox-inline"><input name="assignment_gt_reason[]" type="checkbox" value="Replacing Existing Role (Vacancy)" {{ (isset($form_detail['reason']) && in_array("Replacing Existing Role (Vacancy)", $form_detail['reason'])) ? "checked" : "" }}> Replacing Existing Role (Vacancy)</label><br/>
                    <label class="checkbox-inline"><input name="assignment_gt_reason[]" type="checkbox" value="Project Base (New Product Launch, Network Roll Out)" {{ (isset($form_detail['reason']) && in_array("Project Base (New Product Launch, Network Roll Out)", $form_detail['reason'])) ? "checked" : "" }}> Project Base (New Product Launch, Network Roll Out)</label><br/>
                    <label class="checkbox-inline"><input name="assignment_gt_reason[]" type="checkbox" value="Leadership Development/ Succession Program" {{ (isset($form_detail['reason']) && in_array("Leadership Development/ Succession Program", $form_detail['reason'])) ? "checked" : "" }}> Leadership Development/ Succession Program</label><br/>
                    <label class="checkbox-inline"><input name="assignment_gt_reason[]" type="checkbox" value="Strategic Resource Gap" {{ (isset($form_detail['reason']) && in_array("Strategic Resource Gap", $form_detail['reason'])) ? "checked" : "" }}> Strategic Resource Gap</label><br/>
                    <label class="checkbox-inline"><input id="ag_gt_other" type="checkbox" value="Other" {{ (isset($form_detail['reason_other'])) ? "checked" : "" }}> Other</label>
                </div>
                <div id="ag_gt_other_container">
                    <input type="text" name="assignment_gt_reason_other" value="{{ (isset($form_detail['reason_other'])) ? $form_detail['reason_other'] : '' }}" class="form-control">
                </div>
            </div>
            <div class="form-group reset">
                <div class="form-field">
                    <input id="gtmattachment" type="file" accept=".rar,zip" name="assigment_gt_attachment" class="btn btn-default reset">
                    <button {{ (isset($form_detail['attachment']) && $form_detail['attachment']) ? "disabled readonly" : "" }} type="button" class="btn btn-default reset">
                        <label for="gtmattachment" style="margin-bottom: 0;width: 100%; padding: 0;"><i class="icon-contact"></i> Documents</label>
                    </button><br>
                    {{ (isset($form_detail['attachment']) && $form_detail['attachment']) ? $form_detail['attachment'] : "" }}
                    @if($errors->has('assigment_gt_attachment'))
                        <div class="text-danger">Please upload your documents in rar or zip format</div>
                    @endif
                    @if($errors->has('assigment_gt_reason_other'))
                        <div class="text-danger">Please upload your documents in rar or zip format</div>
                    @endif
                    @if($errors->has('assigment_gt_reason'))
                        <div class="text-danger">Please upload your documents in rar or zip format</div>
                    @endif
                    @if($errors->has('assigment_gt_purpose'))
                        <div class="text-danger">Please upload your documents in rar or zip format</div>
                    @endif
                </div>
            </div>
        
        </div>
</div>
@include('modal-competency')
@section('scripts')
    <script>
        function resetGroupVal(){
            $("#assignment_inter_start_date").val("");
            $("#assignment_inter_end_date").val("");
            $("#assignment_inter_group").val("");
            $("#assignment_inter_division").val("");
        }
        $('[name=assignment_dev_type]').on('click',function () {
            if($(this).val() == 'project') {
                resetGroupVal();
                $(".internship_talent_group_date").addClass("d-none");
                $('[name=user_role]').val('Project Leader');
                $('.select-employee').val(null);
                $('.select2-selection__rendered').html('');
                $('.project-group')
                    .find('select')
                    .attr('required','required')
                $('.project-group')
                    .removeClass('d-none')
                    .find('input')
                        .val('')
                        .attr('required','required')
                    .find('select')
                        .attr('required','required')
                    .find('option:selected')
                    .removeAttr('selected')
                    .find('textarea')
                        .val('')
                        .attr('required','required')
                    .find('.select2-selection__rendered')
                    .html('');
                $('.project-group').find('textarea').val('').attr('required','required');


                $('.internship')
                    .find('select')
                    .removeAttr('required','required');
                $('.internship')
                    .addClass('d-none')
                    .find('input')
                        .val('')
                        .removeAttr('required','required')
                    .find('option:selected')
                    .removeAttr('selected')
                    .find('textarea')
                        .val('')
                        .removeAttr('required','required')
                    .find('.select2-selection__rendered')
                    .html('');
                $('.internship').find('textarea').val('').removeAttr('required','required');


                $('.globaltalent')
                    .find('select')
                    .removeAttr('required','required');
                $('.globaltalent')
                    .addClass('d-none')
                    .find('input')
                        .val('')
                        .removeAttr('required','required')
                    .find('option:selected')
                    .removeAttr('selected')
                    .find('textarea')
                        .val('')
                        .removeAttr('required','required')
                    .find('.select2-selection__rendered')
                    .html('');
                $('.globaltalent').find('textarea').val('').removeAttr('required','required');
            } else if($(this).val() == 'internship') {
                resetGroupVal();
                $(".internship_talent_group_date").removeClass("d-none");
                $('[name=user_role]').val('Mentor');
                $('.select-employee').val(null);
                $('.select2-selection__rendered').html('');
                $('.internship')
                    .find('select')
                $('.internship')
                    .removeClass('d-none')
                    .find('input')
                    .val('')
                    .find('select')
                        .find('option:selected')
                        .removeAttr('selected')
                    .find('textarea')
                        .val('')
                    .find('.select2-selection__rendered')
                    .html('');
                $('.internship').find('textarea').val('');


                $('.project-group')
                    .find('select')
                    .removeAttr('required','required');
                $('.project-group')
                    .addClass('d-none')
                    .find('input')
                    .val('')
                    .removeAttr('required','required')
                    .find('select')
                        .find('option:selected')
                        .removeAttr('selected')
                    .find('textarea')
                    .val('')
                    .removeAttr('required','required')
                    .find('.select2-selection__rendered')
                    .html('');
                $('.project-group').find('textarea').val('').removeAttr('required','required');


                $('.globaltalent')
                    .find('select')
                    .removeAttr('required','required');
                $('.globaltalent')
                    .addClass('d-none')
                    .find('input')
                        .val('')
                        .removeAttr('required','required')
                    .find('option:selected')
                    .removeAttr('selected')
                    .find('textarea')
                        .val('')
                        .removeAttr('required','required')
                    .find('.select2-selection__rendered')
                    .html('');
                $('.globaltalent').find('textarea').val('').removeAttr('required','required');
            } else {
                resetGroupVal();
                $(".internship_talent_group_date").addClass("d-none");
                $('[name=user_role]').val('Mentor');
                $('.select-employee').val(null);
                $('.select2-selection__rendered').html('');
                $('.globaltalent')
                    .find('select').attr('required','required');
                $('.globaltalent')
                    .find('input[type="text"]').attr('required','required');
                $('.globaltalent')
                    .find('input[type="textarea"]').attr('required','required');
                $('.globaltalent')
                    .find('input[type="radio"]').attr('required','required');
                //$('.globaltalent')
                  //  .find('input[type="file"]').attr('required','required');
                $('.globaltalent')
                    .removeClass('d-none');
                //$("#gtmattachment").attr('required','required');

                $("input[name='assignment_gt_reason_other']").removeAttr("required");
                $('.project-group')
                    .find('select')
                    .removeAttr('required','required');
                $('.project-group')
                    .find('input')
                    .removeAttr('required','required');
                $('.project-group')
                    .addClass('d-none');

                $('.internship')
                    .find('select')
                    .removeAttr('required','required');
                $('.internship')
                    .addClass('d-none');
            }
            $("#competency_select").val("");
        });

        function format (d) {
            content = [];
            for (i=0;i<d.length;i++){
                content.push('<tr><td>'+d[i].name+'</td>'+
                '<td>'+d[i].definition+'</td>'+
                '</tr>');
            }
            tableCompetenciesRet = '<table class="table-bordered">'+
                        content.toString().replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '') +
                    '</table>';
            return tableCompetenciesRet;
            ;
        }
        var datatableComp = $('#tableCompetencies').DataTable( {
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {
                url: "{{ route('trainee.dtCompetencies') }}",
            },
            order: [ 1, 'asc' ],
            columns: [
                { data: 'category' },
                { data: 'competency', render: function ( data ) {
                    return format(data);
                }}
            ],
        });

        var competency_selected = [];
        @php if (count($competency_selected)>0){ foreach($competency_selected as $c){ @endphp
            competency_selected.push("<?php echo $c; ?>");
        @php }} @endphp
        
        if (competency_selected.length > 0){
            $("#competency_select").val(competency_selected).trigger('change');
        }

            $(document).on("click","input[name='assignment_gt_purpose']",function(){
                purpose = $(this).data("val");
                if (purpose=="Development"){
                    $("#Development_opt").show();
                    $("#skil_gap_opt").hide();
                    dpar = $("#Development_opt").find("#ag_gt_other");
                    if(dpar.prop("checked")) {
                        $("#ag_gt_other_container").show();
                    } else {
                        $("#ag_gt_other_container").hide();
                    }
                } else {
                    $("#Development_opt").hide();
                    $("#skil_gap_opt").show();
                    dpar = $("#skil_gap_opt").find("#ag_gt_other");
                    if(dpar.prop("checked")) {
                        $("#ag_gt_other_container").show();
                    } else {
                        $("#ag_gt_other_container").hide();
                    }
                }
            });
            
            $("#longterm_opt").on("click",function(){
                $("#assignment_inter_division_gt").val("longterm");
            });

            $("#shortterm_opt").on("click",function(){
                $("#assignment_inter_division_gt").val("shortterm");
            });

            @if(!isset($form_detail['reason_other']))
            $("#ag_gt_other_container").hide();
            @endif

            $(document).on("click","#ag_gt_other",function(){
                if ($(this).prop("checked")) {
                    $("#ag_gt_other_container").show();
                    $("input[name='assignment_gt_reason_other']").attr("required","required");
                } else {
                    $("#ag_gt_other_container").hide();
                    $("input[name='assignment_gt_reason_other']").removeAttr("required");
                }
            });

            @if(isset($form_detail['purpose']) && $form_detail['purpose']=="Development")
                $("#skil_gap_opt").hide();
                $("#skil_gap_opt").find("input").prop("checked",false);
                dpar = $("#Development_opt").find("#ag_gt_other");
                if(dpar.prop("checked")) {
                    $("#ag_gt_other_container").show();
                } else {
                    $("#ag_gt_other_container").hide();
                }
            @elseif(isset($form_detail['purpose']) && $form_detail['purpose']=="Expert Skill Gap")
                $("#Development_opt").hide();
                $("#Development_opt").find("input").prop("checked",false);
                dpar = $("#skil_gap_opt").find("#ag_gt_other");
                if(dpar.prop("checked")) {
                    $("#ag_gt_other_container").show();
                } else {
                    $("#ag_gt_other_container").hide();
                }
            @endif

    </script>

    <script>
        @if (session('success'))
        alert('Success');
        @endif
    </script>
@endsection
