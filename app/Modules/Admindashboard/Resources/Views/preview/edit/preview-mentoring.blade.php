
<input type="hidden" name="form_group_id" value="{{ $formType }}">
<input type="hidden" name="is_last" value="true">
<div class="review-item">
    {{--@dd(job_assignment )--}}
    <div class="sub-title">{{str_replace('_',' ',ucwords($form_type))}} Form</div>
    <div class="center-form">
        @if ($formType == 'Mentoring')
            <div class="form-group reset">
                <label class="control-label">
                    Type of Coaching
                </label>
                <div class="check-group">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" name="coaching_ci_type" type="radio" value="coaching" {{($form_detail['type'] == 'coaching') ? 'checked' : ''}}> Coaching
                            <span class="form-check-sign">
                                                    <span class="check"></span>
                                                  </span>
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" name="coaching_ci_type" type="radio" value="mentoring" {{($form_detail['type'] == 'mentoring') ? 'checked' : ''}}> Mentoring
                            <span class="form-check-sign">
                                                    <span class="check"></span>
                                                  </span>
                        </label>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-group reset">
            <label class="control-label">
                Employee Name
            </label>
            <div class="form-field">
                <input type="text" name="coaching_ci_employee_name" class="form-control" value="{{$form_detail['employee_name']}}" required>
            </div>
        </div>
        <div class="form-group reset">
            <label class="control-label">Employee (Coachee) NIK</label>
            <input name="coaching_ci_employee_nik" type="text" class="form-control" value="{{$form_detail['employee_nik']}}" placeholder="example: IND-010119" required>
        </div>
        <div class="form-group reset">
            <label class="control-label">Coach/Mentor Name</label>

            <select class="form-control select-employee">
                <option value="{{$form_detail['line_manager_nik']}}" selected> {{$form_detail['line_manager_name']}} </option>
            </select>
            <input name="coaching_ci_line_manager_name" type="hidden" class="form-control select-employee-name" value="{{$form_detail['line_manager_name']}}" readonly  required>

            {{--<input name="coaching_ci_line_manager_name" type="text" class="form-control" placeholder="enter name" value="{{$employee['mentor'] ? $employee['mentor']['full_name'] : ''}}" required>--}}
        </div>

        <div class="form-group reset">
            <label class="control-label">Coach/Mentor NIK</label>
            <input name="coaching_ci_line_manager_nik" type="text" id="nik_coachee" class="form-control nik_coachee" value="{{$form_detail['line_manager_nik']}}" placeholder="example: IND-010119" readonly required>
        </div>

        <div class="form-group reset">
            <label class="control-label">Coach Date</label>
            <div class="field-date">
                <div class="date-input">
                    <input name="coaching_ci_start_date" type="text" autocomplete="off" class="form-control start-date" value="{{isset($form_detail['start_date']) ? $form_detail['start_date'] : ''}}" placeholder="enter start date" required>
                    <button type="button" class="btn btn-default btn-icon "><i class="mdi mdi-calendar"></i></button>
                </div>

                To

                <div class="date-input">
                    <input name="coaching_ci_end_date" type="text" autocomplete="off" class="form-control end-date" value="{{isset($form_detail['end_date']) ? $form_detail['end_date'] : ''}}" placeholder="enter end date" required>
                    <button type="button" class="btn btn-default btn-icon"><i class="mdi mdi-calendar"></i></button>
                </div>
            </div>
        </div>
        <div class="form-group reset">
            <label class="control-label">
                Development Goal
            </label>
            <textarea rows="6" name="coaching_cc_development_goal" class="form-control" placeholder="Enter Mapped to Indosat Ooredoo Competency" required> {{isset($form_detail['development_goal']) ? $form_detail['development_goal'] : ''}}</textarea>

            {{--<textarea name="coaching_cc_development_goal" class="form-control" rows="6" placeholder="Enter Development Goal" required></textarea>--}}
        </div>

        <div class="form-group reset">
            <label class="control-label">
                <button type="button" data-toggle="modal" class="btn-question" data-target="#competencyModal">
                    <i class="icon-questions-circular-button" >  </i>
                </button>
                Competency to be Developed
            </label>
            <div class="form-field mb-3">
                <select id="competency_select" class="form-control select2-comp" name="coaching_cc_development_mtioc[]" multiple="multiple" required>
                    @if (count($competencies)>0)
                        @foreach($competencies as $key => $c)
                            <optgroup label="{{ $c['option_group'] }}">
                                @foreach($c['option_relation'] as $opt_sub)
                                    <option>
                                        {{ $opt_sub['name'] }}
                                    </option>
                                @endforeach
                            </optgroup>
                        @endforeach
                    @endif
                </select>
                @if ($errors->has('training_obj_employee_competency'))
                    <div class="validation-msg">
                        Competency to be Developed is Required
                    </div>
                @endif
            </div>
        </div>                

    </div>

</div>
@include('modal-competency')
@section('scripts')

    <script>
       function format (d) {
            content = [];
            for (i=0;i<d.length;i++){
                content.push('<tr><td>'+d[i].name+'</td>'+
                '<td>'+d[i].definition+'</td>'+
                '</tr>');
            }
            tableCompetenciesRet = '<table class="table-bordered">'+
                        content.toString().replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '') +
                    '</table>';
            return tableCompetenciesRet;
            ;
        }
        var datatableComp = $('#tableCompetencies').DataTable( {
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {
                url: "{{ route('trainee.dtCompetencies') }}",
            },
            order: [ 1, 'asc' ],
            columns: [
                { data: 'category' },
                { data: 'competency', render: function ( data ) {
                    return format(data);
                }}
            ],
        });
        
        var competency_selected = [];
        @php if (count($competency_selected)>0){ foreach($competency_selected as $c){ @endphp
            competency_selected.push("<?php echo $c; ?>");
        @php }} @endphp
        
        if (competency_selected.length > 0){
            $("#competency_select").val(competency_selected).trigger('change');
        }
    </script>

    <script>
        @if (session('success'))
        alert('Success');
        @endif
    </script>
@endsection
