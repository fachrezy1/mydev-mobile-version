@php($user = \Sentinel::check())
@php($helper = new \App\Helpers\UserRolesHelper )
@extends($helper->inManager() ? 'core.admin.main' : ($helper->inAdmin() ? 'core.admin.main' : 'core.trainee.main') )
@section('head')
    <link rel="stylesheet" href="{{url('css/admin/dashboard/app.css')}}">
@endsection
@section('content')
    <main class="main-content">
        <div class="main-dashboard clean">
            <div class="dashboard-breadcrumb review">
                <div class="container-fluid">
                    <div class="sub-breadcrumb">
                        <div class="sub-left">
                            <a class="btn-back" href="{{ route( $helper->inManager() ? 'dashboard.traineeRequest' : ($helper->inAdmin() ? 'dashboard.traineeRequest' : 'trainee.myTrainee')) }}">
                            <button type="button" class="btn btn-clean" >
                                <i class="icon-back"></i>
                            </button>
                                <b class="text-white">Back</b>
                            </a>
                        </div>
                    </div>
                    <h1 class="text-center">Edit</h1>
                </div>
            </div>
            <div class="review-top">
                <div class="container">
                    <div class="box-inline">
                        <div class="box-items">
                            <div class="profile-circle">
                                <div class="circle-thumbnail">
                                    <img src="{{url('images/profile.jpeg')}}" alt="" class="img-fluid">
                                </div>
                                <div class="vertical-content text-center">
                                    <h5>Requester</h5>
                                    <h3>{{$requester ? $requester['full_name'] : '' }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="box-items">
                            <div class="vertical-content">
                                <h5>Requester info</h5>
                            </div>
                            <div class="trainer-data">
                                <ul class="list-unstyled">
                                    <li>
                                        <i class="icon-user-4"></i> <p><strong>NIK</strong> {{$requester ? $requester['nik'] : ''}}</p>
                                    </li>
                                    <li>
                                        <i class="icon-user-4"></i> <p><strong>Position</strong> {{$requester ? $requester['position'] : '' }}</p>
                                    </li>
                                    <li>
                                        <i class="icon-user-4"></i> <p><strong>Division</strong> {{$requester ? $requester['division'] : '' }}</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        {{--@dd($form_detail)--}}
                    </div>
                </div>
            </div>
            <div class="container">
                <form id="submit_form" action="{{route('trainee.request.submitStep')}}" method="post" enctype="multipart/form-data">
                    @csrf

                    @if($form_detail['form_group_id'] == 1)
                        @include('admindashboard::preview.edit.preview-assignment')
                    @elseif($form_detail['form_group_id'] == 2 || $form_detail['form_group_id'] == 6)
                        @include('admindashboard::preview.edit.preview-mentoring')
                    @elseif($form_detail['form_group_id'] == 4)
                        @include('admindashboard::preview.edit.preview-external')
                    @elseif($form_detail['form_group_id'] == 3)
                        @include('admindashboard::preview.edit.preview-training')
                    @elseif($form_detail['form_group_id'] == 5)
                        @include('admindashboard::preview.edit.edit-inhouse')
                    @endif
                </form>
            </div>
        </div>

        @if($helper->inManager() || $helper->inAdmin())
            {{--@dd($form_detail)--}}
            @if ($form_detail['is_completed'] != 'Completed')
            <div class="box-items call-to-action">
            	<div class="preview-button d-flex justify-content-between">
            		<input type="hidden" name="user_id" value="{{$form_detail['user_id']}}">
            		<input type="hidden" name="id" value="{{$form_detail['request_group_id']}}">
            		<a href="{{ route( $helper->inManager() ? 'dashboard.traineeRequest' : ($helper->inAdmin() ? 'dashboard.traineeRequest' : 'trainee.myTrainee')) }}" class="btn btn-dark col-md-5 reject_button_trainee">
            			<i class="icon-close"></i>
            			Cancel
            		</a>

            		<button type="submit" form="submit_form" class="btn btn-default col-md-5" title="approve this request">
            			<i class="icon-tick"></i>
            			Save
            		</button>
                </div>
            </div>
            @endif
        @endif
    </main>
    <div class="modal fade" id="rejectNotes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Are you sure to reject it?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('request.rejectRequest')}}" method="post">
                        @csrf
                        <input type="hidden" name="user_id" id="rejected_user_id" value="">
                        <input type="hidden" name="id" id="rejected_request_id" value="">
                        <div class="form-group reset ">
                            <div class="form-field">
                                <textarea class="form-control counter-text" name="rejected_notes" rows="6" placeholder="Notes for rejected trainee"></textarea>
                                <p class="form-helper text-count"><span class="counter-text-value">0</span>/255</p>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between">
	                        <button type="button" class="btn btn-dark" data-dismiss="modal">Cancel</button>
	                        <button type="submit" class="btn btn-default"> Confirm </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
<style>
	@media (max-width: 599px) {
		.center-form {
			max-width: 100%!important;
			margin-top: 30px!important
		}
		.preview-button {
			width: 100%;
			padding-left: 16px;
			padding-right: 16px;
		}
	}
	.select2 {
		width: 100%!important
	}
</style>
@endsection