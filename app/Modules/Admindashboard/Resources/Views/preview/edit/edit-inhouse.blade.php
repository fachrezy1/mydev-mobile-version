{{--@dd(str_replace('_',' ',ucwords($form_type)))--}}
{{--@dd($form_detail)--}}
<input type="hidden" name="form_group_id" value="{{ $formType }}">
<input type="hidden" name="is_last" value="true">
<div class="review-item">
    <div class="sub-title">{{str_replace('_',' ',ucwords($form_type))}} Form</div>
    <div class="center-form">
        <div class="form-group reset split">
            <label class="control-label">
                Training Title
            </label>
            <div class="form-field">
                <input type="text" name="training_ti_title" class="form-control" value="{{$form_detail['title']}}" required>
            </div>
        </div>
        <div class="form-group reset split">
            <label class="control-label">
                Type of programs
            </label>
            <div class="check-group">
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="training_ti_type_programs" type="radio" value="internal trainer" {{isset($form_detail['type_programs']) && $form_detail['type_programs'] == 'internal trainer' ? 'checked' : ''}} > Internal Trainer
                        <span class="form-check-sign">
                        <span class="check"></span>
                      </span>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="training_ti_type_programs" type="radio" value="external trainer" {{isset($form_detail['type_programs']) && $form_detail['type_programs'] == 'external trainer' ? 'checked' : ''}} > External Trainer
                        <span class="form-check-sign">
                        <span class="check"></span>
                      </span>
                    </label>
                </div>

                
            </div>
        </div>
        {{--<div class="form-group reset split">--}}
        {{--<label class="control-label">--}}
        {{--Training Provider--}}
        {{--</label>--}}
        {{--<div class="form-field">--}}
        {{--<input type="text" name="training_ti_provider" class="form-control" value="{{$form_detail['provider']}}" required>--}}
        {{--</div>--}}
        {{--</div>--}}
        <div class="form-group reset external-trainer split {{isset($form_detail['type_programs']) && $form_detail['type_programs'] == 'external trainer' ? '' : 'd-none'}}">
            <label class="control-label">
                Training Provider
            </label>
            <input type="text" name="training_ti_provider" class="form-control" maxlength="300" placeholder="enter name" value="{{isset($form_detail['provider']) ? $form_detail['provider'] : ''}}" {{isset($form_detail['type_programs']) && $form_detail['type_programs'] == 'external trainer' ? 'required' : ''}}>
        </div>
        {{--@dd($form_detail)--}}
        <div class="form-group internal-trainer reset split {{isset($form_detail['type_programs']) && $form_detail['type_programs'] == 'internal trainer' ? '' : 'd-none'}}">
            <label class="control-label text-left">
                Trainer name
            </label>
            <select class="form-control select-employee">
                <option selected> {{isset($form_detail['trainer_name']) ? $form_detail['trainer_name'] : ''}} </option>
            </select>
            <input name="training_obj_trainer_name" type="hidden" class="form-control select-employee-name" placeholder="enter name" value="{{isset($form_detail['trainer_name']) ? $form_detail['trainer_name'] : ''}}" {{isset($form_detail['type_programs']) && $form_detail['type_programs'] == 'internal trainer' ? 'required' : ''}}>

            <label class="control-label text-left">
                Trainer Nik
            </label>
            <input name="training_obj_trainer_nik" type="text" id="nik_coachee" class="form-control nik_coachee" placeholder="example: IND-010119" value="{{isset($form_detail['trainer_nik']) ? $form_detail['trainer_nik'] : ''}}" {{isset($form_detail['type_programs']) && $form_detail['type_programs'] == 'internal trainer' ? 'required' : ''}} readonly>
        </div>

        @if($moderator_name !='notfound')
        <div class="form-group internal-trainer reset split {{isset($form_detail['type_programs'])  ? '' : 'd-none'}}">
            <label class="control-label text-left">
                Moderator name
            </label>
            <select class="form-control select-employee3">
                <option selected> {{isset($form_detail['moderator_name']) ? $form_detail['moderator_name'] : ''}} </option>
            </select>
            <input name="training_obj_moderator_name" type="hidden" class="form-control select-employee-name3" placeholder="enter name" value="{{isset($form_detail['moderator_name']) ? $form_detail['moderator_name'] : ''}}" {{isset($form_detail['type_programs'])  ? 'required' : ''}}>

            <label class="control-label text-left">
                Moderator NIK
            </label>
            <input name="training_obj_moderator_nik" type="textd"  class="form-control nik_coachee3" placeholder="example: IND-010119" value="{{isset($form_detail['moderator_nik']) ? $form_detail['moderator_nik'] : ''}}" {{isset($form_detail['type_programs']) ? 'required' : ''}} readonly>
        </div>
        @endif

        <div class="form-group reset split">
            <label class="control-label">
                Cost
            </label>
            <div class="form-field">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Rp. </span>
                    </div>
                    <input type="number" name="training_ti_cost" maxlength="10" class="form-control" value="{{$form_detail['cost']}}" required>
                </div>
            </div>
        </div>
        <div class="form-group reset split">
            <label class="control-label">
                Date Start
            </label>
            <div class="form-field">
                <input type="text" name="training_ti_start_date" class="form-control start-date" required value="{{$form_detail['start_date']}}" ><br>
            </div>
            <label class="control-label">
                Date End
            </label>
            <div class="form-field">
                <input type="text" name="training_ti_end_date" class="form-control end-date" required value="{{$form_detail['end_date']}}" >
            </div>
        </div>

        <div class="form-group reset split">
            <label class="control-label">
                Training Objective
            </label>
            <div class="form-field">
                <textarea class="form-control" name="training_obj_description" required rows="6" >{{ isset($form_detail['description']) ? $form_detail['description'] : '' }}</textarea>
                <p class="form-helper text-count">{{ isset($form_detail['description']) ? strlen($form_detail['description']) : '0'}}/255</p>
            </div>
        </div>
        {{--@dd($form_detail)--}}
        <div class="form-group reset split">
            <label class="control-label">
                Venue
            </label>
            <div class="check-group">
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="training_obj_venue" type="radio" value="training_obj_venue_domestic" {{( isset($form_detail['venue'])  && $form_detail['venue'] == 'training_obj_venue_domestic') ? 'checked' : ''}}> Domestic
                        <span class="form-check-sign">
                            <span class="check"></span>
                          </span>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="training_obj_venue" type="radio" value="training_obj_venue_overseas" {{ ( isset($form_detail['venue']) && $form_detail['venue'] == 'training_obj_venue_overseas' )  ? 'checked' : ''}}> Overseas
                        <span class="form-check-sign">
                            <span class="check"></span>
                          </span>
                    </label>
                </div>
            </div>
        </div>
        @if (isset($form_detail['employee_name']))
            @php($employee_name = unserialize($form_detail['employee_name']))
            @php($employee_nik = unserialize($form_detail['employee_nik']))
            @php($employee_position = unserialize($form_detail['employee_position']))
            <div class="form-group reset split">
                <label class="control-label">
                    Employees
                </label>
                <div class="form-field">
                    <div class="box-items">
                        @foreach($employee_name as $key=>$employee)
                            <div class="more-item">
                                <div class="field-group select-employee-e-container employee-fields">
                                    <label class="control-label text-left">
                                        Employee name
                                    </label>
                                    <select class="form-control select-employee-e">
                                        <option selected> {{$employee}} </option>
                                    </select>
                                    <input type="hidden" name="training_obj_employee_name[]" class="form-control select-employee-name-e" value="{{$employee}}" placeholder="enter employee name" required>
                                    <label class="control-label text-left">
                                        Employee Nik
                                    </label>
                                    <input type="text" name="training_obj_employee_nik[]" class="form-control select-employee-nik-e" value="{{$employee_nik[$key]}}" placeholder="enter employee nik" required>
                                    <label class="control-label text-left">
                                        Employee Position
                                    </label>
                                    <input type="text" name="training_obj_employee_position[]" class="form-control position_employee-e" value="{{isset($employee_position[$key]) ? $employee_position[$key] : '-'}}" placeholder="enter employee position" required>
                                    @if($key > 0 && !$form_detail['is_completed_by_admin'])
                                        <a href="javascript:void(0)" id="cloneCloseDiv" class="btn btn-default remove-item"><i class="mdi mdi-close-circle"></i> remove</a>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                        <div class="more-item-target">

                        </div>
                    </div>
                </div>
            </div>
            @if(!$form_detail['is_completed_by_admin'])
                <div class="form-group reset split">
                    <label class="control-label">
                    </label>
                    <div class="form-field">
                        <div class="box-items">
                            <div class="more-item">
                                <a href="javascript:void(0)" class="btn btn-default add-more-e"><i class="mdi mdi-plus-circle"></i> Add more Member</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif
        <div class="form-group reset split">
            <label class="control-label">
                <button type="button" data-toggle="modal" class="btn-question" data-target="#competencyModal">
                    <i class="icon-questions-circular-button" >  </i>
                </button>
                Competency to be Developed
            </label>
            <div class="form-field mb-3">

                <select id="competency_select" class="form-control select2-comp" name="training_obj_employee_competency[]" multiple="multiple" required>
                    @if (count($competencies)>0)
                        @foreach($competencies as $key => $c)
                            <optgroup label="{{ $c['option_group'] }}">
                                @foreach($c['option_relation'] as $opt_sub)
                                    <option>
                                        {{ $opt_sub['name'] }}
                                    </option>
                                @endforeach
                            </optgroup>
                        @endforeach
                    @endif
                </select>
                @if ($errors->has('training_obj_employee_competency'))
                    <div class="validation-msg">
                        Competency to be Developed is Required
                    </div>
                @endif
            </div>
        </div>
        @if($form_detail['form_group_id'] != 5)
            <div class="form-group reset split">
                <label class="control-label">
                    Commitment After Training
                </label>

                <div class="form-field mb-4">
                    <select class="form-control select2-comp" name="training_obj_commitment_training[]" multiple="multiple">
                        @if(isset($form_detail['employee_competency']))
                            @php($form_detail['commitment_training'] = unserialize($form_detail['commitment_training']))
                            <option value="Knowledge Sharing" {{in_array('Knowledge Sharing',$form_detail['commitment_training']) ? 'selected' :''}}>Knowledge Sharing </option>
                            <option value="Project" {{in_array('Project',$form_detail['commitment_training']) ? 'selected' :''}}>Project </option>
                            <option value="Job Assignment" {{in_array('Job Assignment',$form_detail['commitment_training']) ? 'selected' :''}}>Job Assignment </option>
                        @endif
                    </select>
                </div>
                <label class="control-label">
                    Committed Date
                </label>
                <div class="form-field mb-4">
                    <div class="field-date">
                        <div class="date-input">
                            <input type="text" name="training_obj_start_date2" autocomplete="off" class="form-control start-date2" onchange="dateStartSelector(this)" value="{{isset($form_detail['start_date2']) ? $form_detail['start_date2'] : ''}}" placeholder="enter start date"   required>
                            <button type="button" class="btn btn-default btn-icon ">
                                <i class="mdi mdi-calendar"></i>
                            </button>
                        </div>
                        To
                        <div class="date-input">
                            <input type="text" name="training_obj_end_date2" autocomplete="off" class="form-control end-date2" placeholder="enter end date" value="{{isset($form_detail['end_date2']) ? $form_detail['end_date2'] : ''}}"   required>
                            <button type="button" class="btn btn-default btn-icon">
                                <i class="mdi mdi-calendar"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <label class="control-label">
                    Commitment Description
                </label>
                <div class="form-field">
                    <textarea name="training_obj_commitment_description" class="form-control counter-text" rows="2" placeholder="enter text"   required>{{isset($form_detail['commitment_description']) ? $form_detail['commitment_description'] : ''}}</textarea>
                    <p class="form-helper text-count"><span class="counter-text-value">0</span>/255</p>
                </div>
            </div>
        @endif
        <div class="form-group reset split">
            <div class="form-field">
                <input id="files" type="file" accept=".pdf,doc,docx,image/*" name="training_obj_attachment" class="btn btn-default reset">
                <button type="button" class="btn btn-default reset">
                    <label for="files" style="margin-bottom: 0;width: 100%; padding: 0;"><i class="icon-contact"></i> Change Attachment</label>
                </button><br><br>
                <small>{{isset($form_detail['attachment']) ? $form_detail['attachment'] : 'No Attachment'}}</small>
            </div>
        </div>
    </div>

    @include('modal-competency')
    @include('modal-type-programs')
</div>

@section('scripts')
    <script>
        $('[name="training_ti_type_programs"]').on('click',function () {
            let external_trainer = $('.external-trainer'),
                internal_trainer = $('.internal-trainer');

            if($(this).val() == 'internal trainer') {
                external_trainer.addClass('d-none');
                external_trainer.find('input').attr('disabled','disabled');

                internal_trainer.removeClass('d-none');
                internal_trainer.find('input').removeAttr('disabled');
            } else {

                internal_trainer.addClass('d-none');
                internal_trainer.find('input').attr('disabled','disabled');

                external_trainer.removeClass('d-none');
                external_trainer.find('input').removeAttr('disabled');
            }
        })
        $(document).ready(function () {

            $('.select-employee-e').select2({
                theme: "bootstrap",
                ajax: {
                    url: ''+$('[name=baseUrl]').val()+'/trainee/request/employee-request',
                    dataType: 'json',
                    // data:{},
                    type:'get',
                    delay: 250,
                    data: function (params) {
                        // console.log($('[name="training_obj_employee_nik"]').val());
                        return {
                            name: params.term,
                            notin: $('[name="training_obj_employee_nik"]').val() ? $('[name="training_obj_employee_nik"]').val() : ''
                        }
                    },
                    placeholder: 'Search for a repository',
                    // escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                    // minimumInputLength: 1,
                }
            });
            $('.select-employee-e').on("select2:select", function(e) {
                let nik = e.params.data.id;
                let position = e.params.data.position;
                // console.log(e.params.data.text);
                $(this).parents('.select-employee-e-container').find('.select-employee-name-e').val(e.params.data.text);
                // console.log($(this).parents('.select-employee-e-container').find('.select-employee-name-e').val());
                // console.log(nik);
                // $('#nik_coachee').val(nik);
                $(this).parents('.select-employee-e-container').find('.select-employee-nik-e').val(nik);

                if($(this).parents('.select-employee-e-container').find('.position_employee-e').length > 0) {
                    $(this).parents('.select-employee-e-container').find('.position_employee-e').val(position)
                }
            });

            $(".more-item-target").on('click', '.remove-item', function () {
                // $(".more-item-target :last").remove();
                $(this).parents('.field-group').remove();
            });
            $(".more-item").on('click', '.remove-item', function () {
                console.log('clicked');
                // $(".more-item-target :last").remove();
                $(this).parents('.field-group').remove();
            });
        })

        $(".add-more-e").click(function () {
            let clone = $(".more-item .employee-fields:last").clone();
            let counter = $('.employee-fields').length + 1;

            clone.find('.select-employee-name')
                .removeClass('select-employee-name')
                .addClass('select-employee-name'+counter);

            clone.find('.nik_coachee')
                .removeClass('nik_coachee')
                .addClass('nik_coachee'+counter);

            clone.find('.position_employee')
                .removeClass('position_employee')
                .addClass('position_employee'+counter);

            clone.find('.select-employee-name'+counter)
                .attr('type','hidden');

            clone.find('select').remove();
            clone.find('.select2').remove();

            $('<select class="form-control select-employee-e'+counter+'"></select>').insertAfter(clone.find('.control-label')[0]);

            clone.find('.select-employee-e'+counter).select2({
                theme: "bootstrap",
                ajax: {
                    url: $('[name=baseUrl]').val()+'/trainee/request/employee-request',
                    dataType: 'json',
                    type:'GET',
                    delay: 250,
                    data: function (params) {
                        return {
                            name: params.term
                        }
                    },
                    placeholder: 'Search for a repository',
                    // escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                    // minimumInputLength: 1,
                }
            });
            clone.find('.select-employee-e'+counter).on("select2:select", function(e) {
                let nik = e.params.data.id;
                let position = e.params.data.position;
                // console.log(e.params.data.text);
                $(this).parents('.select-employee-e-container').find('.select-employee-name-e').val(e.params.data.text);
                // console.log($(this).parents('.select-employee-e-container').find('.select-employee-name-e').val());
                // console.log(nik);
                // $('#nik_coachee').val(nik);
                $(this).parents('.select-employee-e-container').find('.select-employee-nik-e').val(nik);

                if($(this).parents('.select-employee-e-container').find('.position_employee-e').length > 0) {
                    $(this).parents('.select-employee-e-container').find('.position_employee-e').val(position)
                }
            });

            clone.find('input').val('');
            clone.find('textarea').val('');
            clone.find('.remove-item').addClass('d-block');
            clone.appendTo(".more-item-target");
            $('.more-item-target').find('.field-group').removeClass('has-more');



        });

        function format (d) {
            content = [];
            for (i=0;i<d.length;i++){
                content.push('<tr><td>'+d[i].name+'</td>'+
                '<td>'+d[i].definition+'</td>'+
                '</tr>');
            }
            tableCompetenciesRet = '<table class="table-bordered">'+
                        content.toString().replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '') +
                    '</table>';
            return tableCompetenciesRet;
            ;
        }
        var datatableComp = $('#tableCompetencies').DataTable( {
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {
                url: "{{ route('trainee.dtCompetencies') }}",
            },
            order: [ 1, 'asc' ],
            columns: [
                { data: 'category' },
                { data: 'competency', render: function ( data ) {
                    return format(data);
                }}
            ],
        });

        var competency_selected = [];
        @if (count($competency_selected)>0) 
            @foreach($competency_selected as $c)
                competency_selected.push("<?php echo $c; ?>");
            @endforeach
        @endif
        
        if (competency_selected.length > 0){
            $("#competency_select").val(competency_selected).trigger('change');
        }

    </script>

    <script>
        @if (session('success'))
        alert('Success');
        @endif
    </script>
@endsection
