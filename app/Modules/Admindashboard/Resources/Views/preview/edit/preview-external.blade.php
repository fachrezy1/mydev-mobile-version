
<input type="hidden" name="form_group_id" value="{{ $formType }}">
<input type="hidden" name="is_last" value="true">
<div class="review-item">
    {{--@dd(job_assignment )--}}
    <div class="sub-title">{{str_replace('_',' ',ucwords($form_type))}} Form</div>
    <div class="center-form">
        <div class="form-group reset split">
            <label class="control-label">
                Employee (Coachee) Name
            </label>
            <div class="form-field">
                <input type="text" name="mentoring_ci_employee_name" class="form-control" value="{{$form_detail['employee_name']}}" readonly required>
            </div>
        </div>
        @if ($formType !='External Speaker')
        <div class="form-group reset">
            <label class="control-label">
                Position
            </label>
            <div class="check-group">
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="mentoring_ci_position" type="radio" value="coaching" checked=""> Coach
                        <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="mentoring_ci_position" type="radio" value="mentoring"> Mentor
                        <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="mentoring_ci_position" type="radio" value="trainer"> Trainer
                        <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                    </label>
                </div>
            </div>
        </div>
        @endif
        <div class="form-group reset split">
            <label class="control-label">
                Employee Nik
            </label>
            <div class="form-field">
                <input type="text" name="mentoring_ci_employee_nik" class="form-control" value="{{$form_detail['employee_nik']}}" readonly required>
            </div>
        </div>
        <div class="form-group reset split">
            <label class="control-label">
                Topic
            </label>
            <div class="form-field">
                <input type="text" name="mentoring_ci_line_topic" class="form-control" value="{{$form_detail['line_topic']}}" required>
            </div>
        </div>
        <div class="form-group reset split">
            <label class="control-label">
                Coach Date Start
            </label>
            <div class="form-field">
                <input type="text" name="mentoring_ci_start_date" class="form-control start-date" value="{{$form_detail['start_date']}}" required><br>
            </div>
            <label class="control-label">
                Coach Date End
            </label>
            <div class="form-field">
                <input type="text" name="mentoring_ci_end_date" class="form-control end-date" value="{{$form_detail['end_date']}}" required>
            </div>
        </div>

        <div class="form-group reset split">
            <label class="control-label">
                Venue
            </label>
            <div class="form-field">
                <input type="text" name="mentoring_ci_line_venue" class="form-control" value="{{$form_detail['line_venue']}}" required>
            </div>
        </div>
        <div class="form-group reset split">
            <label class="control-label">
                Organization
            </label>
            <div class="form-field">
                <input type="text" name="mentoring_ci_organization" class="form-control" value="{{$form_detail['organization']}}" required>
            </div>
        </div>
    </div>

</div>
@section('scripts')
    <script>
        @if (session('success'))
        alert('Success');
        @endif
    </script>
@endsection
