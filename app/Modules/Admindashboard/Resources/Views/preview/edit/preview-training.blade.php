{{--@dd(str_replace('_',' ',ucwords($form_type)))--}}
<input type="hidden" name="form_group_id" value="{{ $formType }}">
<input type="hidden" name="is_last" value="true">
<div class="review-item">
    <div class="sub-title">{{str_replace('_',' ',ucwords($form_type))}} Form</div>
    <div class="center-form">
        <div class="form-group reset split">
            <label class="control-label">
                Training Title
            </label>
            <div class="form-field">
                <input type="text" name="training_ti_title" class="form-control" value="{{$form_detail['title']}}" required>
            </div>
        </div>
        <div class="form-group reset split">
            {{--@dd($form_detail['form_group_id'])--}}
            @if($form_detail['form_group_id'] == 3)
                <label class="control-label">
                    <button type="button" data-toggle="modal" class="btn-question" data-target="#typeProgramsModal">
                        <i class="icon-questions-circular-button" >  </i>
                    </button>
                    Type of programs
                </label>
                <div class="check-group" style="margin-left: -30px;">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" name="training_ti_type_programs" type="radio" value="general training" {{isset($form_detail['type_programs']) && $form_detail['type_programs'] == 'general training' ? 'checked' : ''}} > General Training
                            <span class="form-check-sign">
                            <span class="check"></span>
                          </span>
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" name="training_ti_type_programs" type="radio" value="technical certification" {{isset($form_detail['type_programs']) && $form_detail['type_programs'] == 'technical certification' ? 'checked' : ''}} > Professional/Technical Certification
                            <span class="form-check-sign">
                            <span class="check"></span>
                          </span>
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" name="training_ti_type_programs" type="radio" value="seminar certification" {{isset($form_detail['type_programs']) && $form_detail['type_programs'] == 'seminar certification' ? 'checked' : ''}} > Seminar/Conference
                            <span class="form-check-sign">
                            <span class="check"></span>
                          </span>
                        </label>
                    </div>
                </div>
            @else
                <label class="control-label">
                    Type of programs
                </label>
                <div class="check-group">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" name="training_ti_is_training" type="radio" value="true" {{isset($form_detail['type_programs']) && $form_detail['type_programs'] == 'training' ? 'checked' : ''}} > Training
                            <span class="form-check-sign">
                            <span class="check"></span>
                          </span>
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" name="training_ti_is_certification" type="radio" value="true" {{isset($form_detail['type_programs']) && $form_detail['type_programs'] == 'certification' ? 'checked' : ''}} > Certification
                            <span class="form-check-sign">
                            <span class="check"></span>
                          </span>
                        </label>
                    </div>
                </div>
            @endif
        </div>
        <div class="form-group reset split">
            <label class="control-label">
                Training Provider
            </label>
            <div class="form-field">
                <input type="text" name="training_ti_provider" class="form-control" value="{{$form_detail['provider']}}" required>
            </div>
        </div>
        <div class="form-group reset split">
            <label class="control-label">
                Cost
            </label>
            <div class="form-field">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Rp. </span>
                    </div>
                    <input type="number" name="training_ti_cost" maxlength="10" class="form-control" value="{{isset($form_detail['cost']) ? $form_detail['cost'] : 0}}" required>
                </div>
            </div>
        </div>
        <div class="form-group reset split">
            <label class="control-label">
                Date Start
            </label>
            <div class="form-field">
                <input type="text" name="training_ti_start_date" class="form-control start-date" required value="{{$form_detail['start_date']}}" ><br>
            </div>
            <label class="control-label">
                Date End
            </label>
            <div class="form-field">
                <input type="text" name="training_ti_end_date" class="form-control end-date" required value="{{$form_detail['end_date']}}" >
            </div>
        </div>

        <div class="form-group reset split">
            <label class="control-label">
                Training Objective
            </label>
            <div class="form-field">
                <textarea class="form-control" name="training_obj_description" required rows="6" >{{ isset($form_detail['description']) ? $form_detail['description'] : '' }}</textarea>
                <p class="form-helper text-count">{{ isset($form_detail['description']) ? strlen($form_detail['description']) : '0'}}/255</p>
            </div>
        </div>
        {{--@dd($form_detail)--}}
        <div class="form-group reset split">
            <label class="control-label">
                Venue
            </label>
            <div class="check-group">
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="training_obj_venue" type="radio" value="training_obj_venue_domestic" {{( isset($form_detail['venue'])  && $form_detail['venue'] == 'training_obj_venue_domestic') ? 'checked' : ''}}> Domestic
                        <span class="form-check-sign">
                            <span class="check"></span>
                          </span>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="training_obj_venue" type="radio" value="training_obj_venue_overseas" {{ ( isset($form_detail['venue']) && $form_detail['venue'] == 'training_obj_venue_overseas' )  ? 'checked' : ''}}> Overseas
                        <span class="form-check-sign">
                            <span class="check"></span>
                          </span>
                    </label>
                </div>
            </div>
        </div>
        @if (isset($form_detail['employee_name']))
            @php($employee_name = unserialize($form_detail['employee_name']))
            @php($employee_nik = unserialize($form_detail['employee_nik']))
            @php($employee_position = unserialize($form_detail['employee_position']))
            @foreach($employee_name as $key=>$employee)
            <div class="form-group reset split">
                <label class="control-label">
                    Employees
                </label>
                <div class="form-field">
                    <div class="box-items">
                        <div class="more-item">
                            <div class="field-group has-more">
                                <label class="control-label text-left">
                                    Employee name
                                </label>
                                <input type="text" name="training_obj_employee_name[]" class="form-control" value="{{$employee}}" placeholder="enter employee name" required>
                                <label class="control-label text-left">
                                    Employee Nik
                                </label>
                                <input type="text" name="training_obj_employee_nik[]" class="form-control" value="{{$employee_nik[$key]}}" placeholder="enter employee nik" required>
                                <label class="control-label text-left">
                                    Employee Position
                                </label>
                                <input type="text" name="training_obj_employee_position[]" class="form-control" value="{{$employee_position[$key]}}" placeholder="enter employee position" required>
                                <a href="javascript:void(0)" id="cloneCloseDiv" class="btn btn-default remove-item"><i class="mdi mdi-close-circle"></i> remove</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="form-group reset split">
                <label class="control-label">
                </label>
                <div class="form-field">
                    <div class="box-items">
                        <div class="more-item">
                            <a href="javascript:void(0)" class="btn btn-default add-more"><i class="mdi mdi-plus-circle"></i> Add more Member</a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-group reset split">
            <label class="control-label">
                <button type="button" data-toggle="modal" class="btn-question" data-target="#competencyModal">
                    <i class="icon-questions-circular-button" >  </i>
                </button>
                Competency to be Developed
            </label>
            <div class="form-field mb-3">

                <select id="competency_select" class="form-control select2-comp" name="training_obj_employee_competency[]" multiple="multiple" required>
                    @if (count($competencies)>0)
                        @foreach($competencies as $key => $c)
                            <optgroup label="{{ $c['option_group'] }}">
                                @foreach($c['option_relation'] as $opt_sub)
                                    <option>
                                        {{ $opt_sub['name'] }}
                                    </option>
                                @endforeach
                            </optgroup>
                        @endforeach
                    @endif
                </select>
                @if ($errors->has('training_obj_employee_competency'))
                    <div class="validation-msg">
                        Competency to be Developed is Required
                    </div>
                @endif
            </div>
        </div>
        @if($form_detail['form_group_id'] != 5)
        <div class="form-group reset split">
            <label class="control-label">
                Commitment After Training
            </label>

            <div class="form-field mb-4">
                <select class="form-control select2-comp" name="training_obj_commitment_training[]" multiple="multiple">
                    @if(isset($form_detail['commitment_training']))
                        @php($form_detail['commitment_training'] = unserialize($form_detail['commitment_training']))
                    @else
                        @php($form_detail['commitment_training'] = [])
                    @endif
                        <option value="Knowledge Sharing" {{in_array('Knowledge Sharing',$form_detail['commitment_training']) ? 'selected' :''}}>Knowledge Sharing </option>
                        <option value="Project" {{in_array('Project',$form_detail['commitment_training']) ? 'selected' :''}}>Project </option>
                        <option value="Job Assignment" {{in_array('Job Assignment',$form_detail['commitment_training']) ? 'selected' :''}}>Job Assignment </option>
                </select>
            </div>
            <label class="control-label">
                Committed Date
            </label>
            <div class="form-field mb-4">
                <div class="field-date">
                    <div class="date-input">
                        <input type="text" name="training_obj_start_date2" autocomplete="off" class="form-control start-date2" onchange="dateStartSelector(this)" value="{{isset($form_detail['start_date2']) ? $form_detail['start_date2'] : ''}}" placeholder="enter start date"   required>
                        <button type="button" class="btn btn-default btn-icon ">
                            <i class="mdi mdi-calendar"></i>
                        </button>
                    </div>
                    To
                    <div class="date-input">
                        <input type="text" name="training_obj_end_date2" autocomplete="off" class="form-control end-date2" placeholder="enter end date" value="{{isset($form_detail['end_date2']) ? $form_detail['end_date2'] : ''}}"   required>
                        <button type="button" class="btn btn-default btn-icon">
                            <i class="mdi mdi-calendar"></i>
                        </button>
                    </div>
                </div>
            </div>
            <label class="control-label">
                Commitment Description
            </label>
            <div class="form-field">
                <textarea name="training_obj_commitment_description" class="form-control counter-text" rows="2" placeholder="enter text"   required>{{isset($form_detail['commitment_description']) ? $form_detail['commitment_description'] : ''}}</textarea>
                <p class="form-helper text-count"><span class="counter-text-value">0</span>/255</p>
            </div>
        </div>
        @endif
        <div class="form-group reset split">
            <div class="form-field">
                <input id="files" type="file" accept=".pdf,doc,docx,image/*" name="training_obj_attachment" class="btn btn-default reset">
                <button type="button" class="btn btn-default reset">
                    <label for="files" style="margin-bottom: 0;width: 100%; padding: 0;"><i class="icon-contact"></i> Change Attachment</label>
                </button><br><br>
                <small>{{isset($form_detail['attachment']) ? $form_detail['attachment'] : 'No Attachment'}}</small>
            </div>
        </div>
    </div>

    @include('modal-competency')
    @include('modal-type-programs')
</div>

@section('scripts')

    <script>
        function format (d) {
            content = [];
            for (i=0;i<d.length;i++){
                content.push('<tr><td>'+d[i].name+'</td>'+
                '<td>'+d[i].definition+'</td>'+
                '</tr>');
            }
            tableCompetenciesRet = '<table class="table-bordered">'+
                        content.toString().replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '') +
                    '</table>';
            return tableCompetenciesRet;
            ;
        }
        var datatableComp = $('#tableCompetencies').DataTable( {
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {
                url: "{{ route('trainee.dtCompetencies') }}",
            },
            order: [ 1, 'asc' ],
            columns: [
                { data: 'category' },
                { data: 'competency', render: function ( data ) {
                    return format(data);
                }}
            ],
        });

        var competency_selected = [];
        @if (count($competency_selected)>0)
            @foreach($competency_selected as $c)
                competency_selected.push("<?php echo $c; ?>");
            @endforeach
        @endif
        
        if (competency_selected.length > 0){
            $("#competency_select").val(competency_selected).trigger('change');
        }
        
        @if (session('success'))
        alert('Success');
        @endif
    </script>
@endsection
