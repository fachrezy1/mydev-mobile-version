@extends('core.admin.main')
@section('head')
    <link rel="stylesheet" href="{{url('css/admin/dashboard/app.css')}}">
@endsection
@section('content')
    @include('core.admin.header')
    @php
        $data_exists = false
    @endphp
    <main class="main-content">
        <div class="main-dashboard">
            <div class="dashboard-content">

                <div class="container-fluid">

                    <div class="dashboard-entry">
                        <div class="side-box">
                            <div class="side-box-description">
                                <h1>Review</h1>
                                <div class="small-box-text">
                                    <p>
                                        Here you can add review or update existing trainee programs of ooredoo, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras at est sit amet odio tincidunt ullamcorper sit amet ac mi. Nam a convallis nulla. Integer vitae metus nec purus cursus interdum vel et lacus.
                                    </p>
                                </div>
                            </div>
                            <div class="side-box-entry">
                                <div class="entry-icon">
                                    <i class="mdi mdi-file-document-box-outline"></i>
                                    <div class="entry-icon-text">
                                        <h3>Form Type</h3>
                                        <p>Mentoring Goal Form</p>
                                    </div>
                                </div>
                                <div class="entry-icon">
                                    <i class="mdi mdi-subtitles-outline"></i>
                                    <div class="entry-icon-text">
                                        <h3>Topic</h3>
                                        <p>Telecommunications challenges for 4.0 Industry</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="review-opt">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default duplicate-input-add-reviews" data-sessios="1">

                                    <i class="mdi mdi-plus-circle"></i>

                                    Add more session
                                </button>
                                <button type="button" class="btn btn-default"> <i class="mdi mdi-reload"></i> Update session</button>
                            </div>
                        </div>
                        <div class="accordion ooredoo" id="reviewList">
                            @if (session('success'))
                            <div class="alert alert-success" role="alert">
                                {{session('success')}}
                            </div>
                            @endif
                            <form  method="post" action="{{ route('request.submitReviews') }}">
                                @csrf
                                <input type="hidden" name="user_id" value="{{$form['user_id']}}">
                                <input type="hidden" name="form_group_id" value="{{$form['form_group_id']}}">
                                <input type="hidden" name="form_type" value="{{$form['form_type']}}">
                                @php
                                    $reviews_date
                                        = $form_type
                                        = $reviews_goal
                                        = $reviews_result
                                        = $reviews_feedback
                                        = $reviews_action
                                        = null;
                                @endphp
                                @if ($current_data)
                                    @php
                                        $data_exists = true;
                                        $reviews_date = $current_data[0]['reviews_date'];
                                        $reviews_goal = $current_data[0]['reviews_goal'];
                                        $reviews_result = $current_data[0]['reviews_result'];
                                        $reviews_feedback = $current_data[0]['reviews_feedback'];
                                        $reviews_action = $current_data[0]['reviews_action'];
                                        unset($current_data[0]);
                                    @endphp
                                @endif
                                <div class="duplicate-input-source">
                                    <div class="card duplicate-input-source_content">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                         <span class="acc-title">
                                            Training name of ooredoo
                                         </span>
                                            <span class="acc-toggle">
                                            1st Sessions
                                             <i class="mdi mdi-chevron-down"></i>
                                          </span>
                                        </button>
                                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#reviewList">
                                            <div class="card-body">
                                                <div class="top-card-body">
                                                    <div class="card-date">
                                                        <div class="form-group bmd-form-group is-filled">
                                                            <label class="label-control bmd-label-static">Day date</label>
                                                            <input type="date" name="reviews_date[]" value="{{$reviews_date}}" class="form-control datetimepicker1">
                                                            @if ($errors->get('reviews_date.0'))
                                                                <small class="text-danger"> {{$errors->get('reviews_date.0')[0]}} </small>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="card-hour">
                                                        3hours
                                                    </div>
                                                </div>
                                                <div class="card-body-entry">
                                                    <div class="card-body-list">
                                                        <div class="form-group">
                                                            <label class="master-label">
                                                                Mentoring goal
                                                            </label>
                                                            <textarea class="form-control reset" name="reviews_goal[]" rows="2">{{$reviews_goal}}</textarea>
                                                            @if ($errors->get('reviews_goal.0'))
                                                                <small class="text-danger"> {{$errors->get('reviews_goal.0')[0]}} </small>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="master-label">
                                                                Mentoring result or progress
                                                            </label>
                                                            <textarea class="form-control reset" name="reviews_result[]" rows="2">{{$reviews_result}}</textarea>
                                                            @if ($errors->get('reviews_result.0'))
                                                                <small class="text-danger"> {{$errors->get('reviews_result.0')[0]}} </small>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="master-label">
                                                                Mentoring Fedback
                                                            </label>
                                                            <textarea class="form-control reset" name="reviews_feedback[]" rows="2">{{$reviews_feedback}}</textarea>
                                                            @if ($errors->get('reviews_feedback.0'))
                                                                <small class="text-danger"> {{$errors->get('reviews_feedback.0')[0]}} </small>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="master-label">
                                                                Mentoring action (optional)
                                                            </label>
                                                            <textarea class="form-control reset" name="reviews_action[]" rows="2">{{$reviews_action}}</textarea>
                                                            @if ($errors->get('reviews_action.0'))
                                                                <small class="text-danger"> {{$errors->get('reviews_action.0')[0]}} </small>
                                                            @endif
                                                        </div>
                                                        @if ($is_completed)
                                                            <button type="button" class="btn btn-disabled" disabled > Review Completed </button>
                                                        @else
                                                            <button type="submit" name="submit_button" class="btn btn-primary" value="save"> Save </button>
                                                            @if ($data_exists)
                                                                <button type="submit" name="submit_button" class="btn btn-primary" value="completed"> Completed </button>
                                                            @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="duplicate-input-taget">
                                    @if ($current_data && count($current_data) > 0)
                                        @foreach($current_data as $key=>$item)
                                            <div class="card">
                                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                 <span class="acc-title">
                                                    Training name of ooredoo
                                                 </span>
                                                            <span class="acc-toggle">
                                                    1st Sessions
                                                     <i class="mdi mdi-chevron-down"></i>
                                                  </span>
                                                </button>
                                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#reviewList">
                                                    <div class="card-body">
                                                        <div class="top-card-body">
                                                            <div class="card-date">
                                                                <div class="form-group bmd-form-group is-filled">
                                                                    <label class="label-control bmd-label-static">Day date</label>
                                                                    <input type="date" name="reviews_date[]" value="{{$item['reviews_date']}}" class="form-control datetimepicker1">
                                                                    @if ($errors->get('reviews_date.'.$key))
                                                                        <small class="text-danger"> {{$errors->get('reviews_date.0')[0]}} </small>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="card-hour">
                                                                3hours
                                                            </div>
                                                        </div>
                                                        <div class="card-body-entry">
                                                            <div class="card-body-list">
                                                                <div class="form-group">
                                                                    <label class="master-label">
                                                                        Mentoring goal
                                                                    </label>
                                                                    <textarea class="form-control reset" name="reviews_goal[]" rows="2">{{$item['reviews_goal']}}</textarea>
                                                                    @if ($errors->get('reviews_goal.'.$key))
                                                                        <small class="text-danger"> {{$errors->get('reviews_goal.0')[0]}} </small>
                                                                    @endif
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="master-label">
                                                                        Mentoring result or progress
                                                                    </label>
                                                                    <textarea class="form-control reset" name="reviews_result[]" rows="2">{{$item['reviews_result']}}</textarea>
                                                                    @if ($errors->get('reviews_result.'.$key))
                                                                        <small class="text-danger"> {{$errors->get('reviews_result.0')[0]}} </small>
                                                                    @endif
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="master-label">
                                                                        Mentoring Fedback
                                                                    </label>
                                                                    <textarea class="form-control reset" name="reviews_feedback[]" rows="2">{{$item['reviews_feedback']}}</textarea>
                                                                    @if ($errors->get('reviews_feedback.'.$key))
                                                                        <small class="text-danger"> {{$errors->get('reviews_feedback.0')[0]}} </small>
                                                                    @endif
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="master-label">
                                                                        Mentoring action (optional)
                                                                    </label>
                                                                    <textarea class="form-control reset" name="reviews_action[]" rows="2">{{$item['reviews_action']}}</textarea>
                                                                    @if ($errors->get('reviews_action.'.$key))
                                                                        <small class="text-danger"> {{$errors->get('reviews_action.0')[0]}} </small>
                                                                    @endif
                                                                </div>
                                                                @if ($is_completed)
                                                                    <button type="button" class="btn btn-disabled" disabled > Save </button>
                                                                @else
                                                                    <button type="submit" name="submit_button" class="btn btn-primary" value="save"> Save </button>
                                                                @endif
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        @if ($is_completed)
                                            <button type="submit" class="btn btn-primary" disabled> Review Completed </button>
                                        @else
                                            <button type="submit" name="submit_button" class="btn btn-primary" value="completed"> Completed </button>
                                        @endif
                                    @endif
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

                <button type="button" class="btn btn-float btn-fba">
                    <i class="mdi mdi-more"></i>
                </button>
            </div>
            <aside class="right-side">

                <div class="user-side">
                    <div class="top-side">
                        <h3>Trainee Points</h3>
                        <a href="javascript:void(0);" class="btn-fba hide-side">
                            Hide
                        </a>
                        <div class="point-user">
                            <div class="point-circle">
                                <div class="point-data">
                                    <div class="point-numeric">
                                        9999
                                        <span>Total Points</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="#0" class="redeem-btn">Redeem Point</a>
                    </div>
                    <div class="recent-act">
                        <h4 class="side-title">Recent Trainee</h4>
                        <div class="trainee-list">
      <span class="circle-icon">
        <i class="mdi mdi-account-check"></i>
      </span>
                            <div class="list-description">
                                <h5>Leadership Trainee</h5>
                                <span><i class="mdi mdi-calendar"></i> January 10, 2019</span>
                                <p>I am learning more course</p>
                            </div>
                        </div>
                        <div class="trainee-list">
      <span class="circle-icon">
        <i class="mdi mdi-account-check"></i>
      </span>
                            <div class="list-description">
                                <h5>Leadership Trainee</h5>
                                <span><i class="mdi mdi-calendar"></i> January 04, 2019</span>
                                <p>I am learning more course</p>
                            </div>
                        </div>
                        <div class="trainee-list">
      <span class="circle-icon">
        <i class="mdi mdi-account-check"></i>
      </span>
                            <div class="list-description">
                                <h5>Leadership Trainee</h5>
                                <span><i class="mdi mdi-calendar"></i> January 03, 2019</span>
                                <p>I am learning more course</p>
                            </div>
                        </div>
                        <div class="trainee-list">
      <span class="circle-icon">
        <i class="mdi mdi-account-check"></i>
      </span>
                            <div class="list-description">
                                <h5>Leadership Trainee</h5>
                                <span><i class="mdi mdi-calendar"></i> January 02, 2019</span>
                                <p>I am learning more course</p>
                            </div>
                        </div>




                    </div>

                </div>

            </aside>
        </div>

    </main>
@endsection
@section('js')
    <script src="{{url('js/general.js')}}"></script>
    <script src="{{url('js/admin/dashboard/app.js')}}"></script>
@endsection
