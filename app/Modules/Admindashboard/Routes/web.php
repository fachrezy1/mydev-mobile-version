<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/integrasi_odsys','EmployeeController@integrasi_odsys')->name('integrasi_odsys');

Route::group(['prefix' => 'dashboard','as'=>'dashboard.', 'middleware' => 'sentinel.login'], function () {
    Route::get('/employee-check','EmployeeController@check')->name('employeeCheck');
    Route::get('/my-trainee','MyTraineeController@view')->name('myTrainee');
    Route::get('/review/{id}','ReviewController@view')->name('review');

    Route::get('/trainee-request','RequestTraineeController@view')->name('traineeRequest');
    Route::get('/trainee-request/datatable/{type}','RequestTraineeController@datatableTraineeRequest')->name('datatableTraineeRequest');
    Route::get('/trainee-request/export','RequestTraineeController@traineeRequestExportTrainee')->name('traineeRequestExportTrainee');
    Route::get('/','DashboardAdminController@index')->name('dashboardIndex');
    Route::get('/export/course-by-month','DashboardAdminController@exportCourseByMonth')->name('exportCourseByMonth');
    Route::get('/filter-chart','DashboardAdminController@filterChartValue')->name('filterChartValue');
    Route::group(['prefix'=>'employee','as'=>'employee.'],function (){
       Route::post('/find','EmployeeController@find')->name('find');
    });
    Route::group(['prefix' => 'setting','as'=>'setting.'],function (){
        Route::get('/','SettingController@index')->name('index');
        Route::get('/employees','EmployeeController@index')->name('employee');
        Route::get('/employees/detail/{id}','EmployeeController@show')->name('employeeDetail');
        Route::get('/employees/blocking/{nik}/{status}','EmployeeController@blocking')->name('employeeBlocking');
        Route::get('/employee-list','EmployeeController@getEmployee')->name('employeeList');
        Route::get('/user-request-list','EmployeeController@userRequestList')->name('userRequestList');
        Route::post('/import-excel-to-db','SettingController@importExcelTodb')->name('importExcelToDb');

        //new integrasi odsys
        Route::get('/user-log-list','EmployeeController@loglist')->name('loglist');
        Route::get('/integrasi_odsys2','EmployeeController@integrasi_odsys')->name('integrasi_odsys2');
        
        
        //end new
    });
    Route::group(['prefix'=>'trainee','as'=>'trainee.'],function (){
       Route::group(['prefix'=>'edit','as'=>'edit.'],function (){
          Route::get('/{type}/{user_id}/{id}','EditTraineeController@edit')->name('show');
          Route::post('/update','EditTraineeController@update')->name('update');
       });
       Route::group(['prefix'=>'history','as'=>'history.'],function (){
//          Route::get('/list','traineeHistoryController@index')->name('list');
       });
    });
    Route::group(['prefix'=>'item','as'=>'item.'],function (){
       Route::get('/','ItemManagementController@index')->name('index');
       Route::get('/add','ItemManagementController@create')->name('add');
       Route::post('/store','ItemManagementController@store')->name('store');
       Route::get('/edit/{id}','ItemManagementController@edit')->name('edit');
       Route::post('/update','ItemManagementController@update')->name('update');
       Route::get('/destroy/{id}','ItemManagementController@destroy')->name('destroy');
       Route::get('/get-items','ItemManagementController@getItems')->name('getItems');
       Route::get('/get-item-requests','ItemManagementController@getItemRequests')->name('getItemRequests');
       Route::get('/approve-item/{id}','ItemManagementController@approveItem')->name('approveItem');
       Route::get('/get-item-approved','ItemManagementController@getApprovedItems')->name('getItemApproved');
    });
    Route::group(['prefix'=>'point','as'=>'point.'],function (){
       Route::get('/','PointManagementController@index')->name('index');
       Route::get('/edit/{id}','PointManagementController@edit')->name('edit');
       Route::post('/update','PointManagementController@update')->name('update');

       //new
       Route::get('/resetpoint','PointManagementController@resetpoint')->name('resetpoint');
       //end new


        Route::get('/add-multi-point','PointManagementController@add_multiplication_point')->name('addMultiPoint');
        Route::post('/store-multiplication-point','PointManagementController@store_multiplication_point')->name('storeMultiPoint');

        Route::get('/edit-multiplication-point/{id}','PointManagementController@edit_multiplication_point')->name('editMultiPoint');
        Route::get('/delete-multiplication-point/{id}','PointManagementController@delete_multiplication_point')->name('deleteMultiPoint');

    });
    Route::group(['prefix'=>'voucher','as'=>'voucher.'],function (){
       Route::resource('/', 'VoucherManagementController');
       Route::get('/edit/{id}','VoucherManagementController@edit')->name('edit');
       Route::post('/update','VoucherManagementController@update')->name('update');
       Route::get('/destroy/{id}','VoucherManagementController@destroy')->name('destroy');
       Route::get('/get-items','VoucherManagementController@voucherDatatable')->name('voucherDatatable');
       Route::get('/get-history','VoucherManagementController@voucherHistoryDatatable')->name('voucherHistoryDatatable');
       Route::get('/export-history','VoucherManagementController@voucherExportHistory')->name('voucherExportHistory');
       Route::get('/get-voucher-code/{id}','VoucherManagementController@voucherCodeDatatable')->name('voucherCodeDatatable');
    });
    Route::group(['prefix'=>'banner','as'=>'banner.'],function (){
       Route::get('/show','BannerController@show')->name('show');
       Route::post('/store','BannerController@store')->name('store');
       Route::post('/update','BannerController@update')->name('update');
       Route::get('/add','BannerController@index')->name('add');
    });

    Route::group(['prefix'=>'report','as'=>'report.'],function (){
       Route::get('/','ReportController@index')->name('index');
       Route::get('/user/{id}','trainee::ReportController@index')->name('user');
       Route::get('/get-employee-data','ReportController@getEmployeeData')->name('getEmployeeData');
       Route::get('/export-all-users','ReportController@exportAllUsers')->name('exportAllUsers');
    });

});
Route::group(['prefix' => 'request','as'=>'request.', 'middleware' => 'sentinel.login'], function () {
    Route::post('/acceptRequest','Requests\NotificationRequestsController@store')->name('acceptRequest');
    Route::post('/rejectRequest','Requests\NotificationRequestsController@rejectRequest')->name('rejectRequest');
    Route::group(['prefix' => 'setting','as'=>'setting.'],function (){
       Route::post('/set-user-role','EmployeeController@setRole')->name('setUserRole');
    });
});

