<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/trainee', function (Request $request) {
    // return $request->trainee();
})->middleware('auth:api');

Route::get('login', [ 'as' => 'login', 'uses' => 'LoginController@do']);
Route::group(['prefix' => 'v1','as'=>'v1.'], function () {
    Route::get('/changepoint', 'PointController@changePoint')->name('changePoint');
    Route::get('/testapi', 'PointController@testapi')->name('testapi');
});