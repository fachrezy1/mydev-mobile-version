<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'trainee','as'=>'trainee.', 'middleware' => 'sentinel.login'], function () {

    Route::get('/datatableCompetencies', 'TraineeFormController@datatableCompetencies')->name('dtCompetencies');
    Route::get('/datatableCompetency', 'TraineeFormController@datatableCompetency')->name('dtCompetency');
    // tambahan
    Route::get('/request-admin', 'TraineeRequestController@loadStaffbyAdmin')->name('request-admin');
    
    Route::get('/my-trainee/datatable/{type}','MyTraineeController@datatableTraineeRequest')->name('datatableTraineeRequest');
    Route::get('/my-trainee/datatable2/{type}','MyTraineeController@datatableTraineeRequest2')->name('datatableTraineeRequest2');

    Route::get('/my-trainee/datatable3/{type}','MyTraineeController@datatableTraineeRequest3')->name('datatableTraineeRequest3');
    
    Route::get('/my-trainee/datatablein/{type}','MyTraineeController@DataTableTraineeInhouse')->name('DataTableTraineeInhouse');

    // end tambahan
    Route::group(['prefix' => 'request'], function (){
      Route::resource('/', 'TraineeRequestController');
      Route::get('/request-training', 'TraineeRequestController@requestTraining')->name('requestTraining');

       Route::get('/public-training', 'TraineeFormController@publicTraining')->name('publicTrainingStep1');

       Route::get('/coaching', 'TraineeFormController@coaching')->name('coaching');
       Route::get('/mentoring', 'TraineeFormController@mentoring')->name('mentoring');
       Route::get('/assignment', 'TraineeFormController@assignment')->name('assignment');
       Route::get('/external-speaker', 'TraineeFormController@externalSpeaker')->name('externalSpeaker');
       Route::get('/inhouse', 'TraineeFormController@inhouse')->name('inhouse');
       Route::get('/internal-coach', 'TraineeFormController@internalCoach')->name('internalCoach');

       Route::get('/employee-request','TraineeFormController@employeeRequest')->name('employeeRequest');

       Route::get('/get-datatables/{type}','MyTraineeController@getDatatables')->name('getDatatables');
    });

    Route::group(['prefix' => 'request','as'=>'request.'], function (){
        Route::post('submitReviews','Requests\ReviewRequestsController@store')->name('submitReviews');
        Route::post('approve-point','Requests\ReviewRequestsController@approvePoint')->name('approvePoint');
        Route::post('submitStep','TraineeFormSubmitController@submitStep')->name('submitStep');

    });

    Route::group(['prefix'=>'rating','as'=>'rating.'],function (){
        Route::post('submit','RatingController@store')->name('submit');
        Route::post('submit2','RatingController@store2')->name('submit2'); //new
    });

    Route::group(['prefix'=>'dashboard','as'=>'dashboard.'],function (){
       Route::get('/','DashboardController@view')->name('index');
       Route::get('/export/chart-training','DashboardController@exportChartTraining')->name('exportChartTraining');
       Route::get('/filter-chart','DashboardController@filterChartValue')->name('filterChartValue');
       Route::get('/get-recent-trainee','DashboardController@recentTrainee')->name('recentTrainee');
    });

    Route::get('my-trainee','MyTraineeController@view')->name('myTrainee');
    Route::get('my-trainee/export','MyTraineeController@exportTrainee')->name('exportTrainee');

    Route::get('review/{id}','ReviewController@view')->name('review');

    Route::get('report/{user_id}/{id}','ReviewController@viewByUserId')->name('reviewByUserId');


    Route::get('preview/{id}','PreviewController@show')->name('preview');
    Route::get('preview/{user_id}/{id}','PreviewController@showByUserId')->name('previewByUserId');

    Route::get('export/{user_id}/{id}','PreviewController@export')->name('export');

    Route::get('export2/{user_id}/{id}','PreviewController@export2')->name('export2');

    Route::get('set-done/{form_id}/{id}','TraineeFormSubmitController@setDone')->name('setDone');

    Route::group(['prefix'=>'notification','as'=>'notification.'], function (){
        Route::get('/','NotificationController@view')->name('index');
        Route::get('redirect/{id}','NotificationController@redirect')->name('redirect');
    });
    Route::group(['prefix'=>'point','as'=>'point.'],function (){
        Route::get('/redeem','PointController@index')->name('redeem');
        Route::get('/show/{id}','PointController@show')->name('show');
        Route::post('/store','PointController@store')->name('store');
        Route::get('/list','PointController@list')->name('list');
    });

    Route::group(['prefix'=>'voucher','as'=>'voucher.'],function (){
        Route::get('/redeem','RedeemVoucherController@index')->name('redeem');
        Route::get('/show/{id}','RedeemVoucherController@show')->name('show');
        Route::post('/store','RedeemVoucherController@store')->name('store');
    });
        
    Route::group(['prefix'=>'report','as'=>'report.'],function (){
        Route::get('/get-trainee-user','ReportController@getTraineeUser')->name('getTraineeUser');
        Route::get('/point-history','ReportController@pointHistory')->name('pointHistory');
        Route::get('/','ReportController@index')->name('index');
        Route::get('/{id}','ReportController@index')->name('byId');
    });
});
