<?php

namespace App\Modules\Trainee\Models;

use Illuminate\Database\Eloquent\Model;

class CompetencyCategory extends Model
{
    //
    protected $table = 'competency_category';
    public function competency()
    {
        return $this->hasMany('App\Modules\Trainee\Models\Competency', 'category_id', 'id');
    }
}