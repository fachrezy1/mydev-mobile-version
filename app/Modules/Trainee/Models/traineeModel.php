<?php

namespace App\Modules\Trainee\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class traineeModel extends Model
{
    protected $table = 'trainees';
    
    protected $fillable = ['id','name','short_description','description','icon','slug'];
}
