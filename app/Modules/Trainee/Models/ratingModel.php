<?php

namespace App\Modules\Trainee\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ratingModel extends Model
{
    //
    protected $table = 'rating';
    protected $fillable = ['staff_id','mentor_id','request_group_id','form_type_id','rating','attachment','notes','start_date','end_date'];
    protected $primaryKey = 'id';

    use SoftDeletes;
}
