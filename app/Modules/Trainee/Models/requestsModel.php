<?php

namespace App\Modules\Trainee\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modules\Trainee\Models\requestsGroupModel;
use Sentinel;
use App\Helpers\UserRolesHelper;
use App\Modules\Auth\Models\UserModel;
use App\Modules\Auth\Models\EmployeeModel;
use Illuminate\Pagination\Paginator;
use DB;
//add
use App\Modules\Trainee\Models\ratingModel;
//end add

class requestsModel extends Model
{
    //
    use SoftDeletes;

    protected $table = 'requests';
    protected $primaryKey = 'id';
    protected $hidden = ['created_at','updated_at','deleted_at'];
    protected $dates = ['deleted_at'];
    const GROUP = [
        'Public Training' => '3',
        'Assignment' => '1',
        'Mentoring' => '2',
        'External Speaker' => '4',
        'Inhouse' => '5',
        'Internal Coach' => '6',
    ];

    const groupById = [
        3 => 'training',
        1 => 'assignment',
        2 => 'mentoring',
        4 => 'external_speaker',
        5 => 'inhouse',
        6 => 'internal_training',
    ];

    public static function getGroupName($id){
        return ucwords(str_replace('_',' ',self::groupById[$id]));
    }

    protected $fillable = ['user_id','form_group_id',
        'form_question','form_answer','request_content',
        'requests_group_id','requests_step'];

    function add($data){
        $role = new UserRolesHelper();
        $requestsGroup = new requestsGroupModel();
        if (isset($data['form_group_id']) && isset(self::GROUP[$data['form_group_id']])){
            $data['form_group_id'] = self::GROUP[$data['form_group_id']];
        } else {
            return false;
        }

        //get group id for update or create new request group
        if(session('group_id')) {
            //if still at same request, get group id from session
            $group_id = session('group_id');
        } else {
            $user = Sentinel::check();

            if ($role->inAdmin() && $data['form_group_id'] == '5') {
                //if admin, automatically set request to approved
                $group_requests = $this->setGroupRequests($data);
                $group_requests['is_accepted'] = 1;
                $group_requests['first_accepter'] = 0;
                $group_requests['second_accepter'] = 0;

                $group_id = $requestsGroup->addRequest($group_requests);
            } else {
                $group_id = $requestsGroup->addRequest($this->setGroupRequests($data));
            }
            session(['group_id'=>$group_id]);
        }

        if ($group_id){
            $data['requests_group_id'] = $group_id;
            $where = [
                'user_id' => $data['user_id'],
                'form_group_id' => $data['form_group_id'],
                'requests_step' => $data['requests_step'],
                'form_question' => $data['form_question'],
                'requests_group_id' => $group_id
            ];

            if ($this->isExists($where)){
                //if data exists then update existing
                $this
                    ->where($where)
                    ->update($data);
            }else{
                return $this->create($data);
            }

        }else{
            return false;
        }
    }

    public function getNotificationRequest($user_id,$current_id,$staff_id) {
        $data = $this->getRequests([
            'user_id' => $user_id
        ],false,$current_id,$staff_id);

        return $data;
    }

    public function getGroupedRequest($user_id = null,$paginate=1) {
        if (!$user_id){
            return false;
        } else {
            $data = $this->getRequests(['user_id' => $user_id],false,null,null,$paginate);
        }
        return $data;
    }

    function getRequestByFormId($where,$request=null,$user_id=null) {
        $data = $this->getRequests(['id' => $where],false,\Sentinel::check()->id,$user_id);
        if ($data) {
            $form_group_id = key($data[key($data)]);
            if($request) {
                if(is_array($request)) {
                    $return = [];
                    $request_id = null;
                    foreach ($request as $item) {
                        foreach($data[self::groupById[$form_group_id]][$form_group_id] as $id=>$dataRequest){
                            $request_id = $id;
                            if ($item == 'form_type') {
                                $return['form_type'] = self::groupById[$form_group_id];
                            } else {
                                $return[$item] = $dataRequest[$item];
                            }
                        }
                    }

                    $return['is_completed'] = $data[self::groupById[$form_group_id]][$form_group_id][$request_id]['is_completed'] == 'Completed' ? true : false;
                    $return['form_detail'] = $data[self::groupById[$form_group_id]][$form_group_id][$request_id];

                    return $return;
                } else {
                    $return[$request] = $data[self::groupById[$form_group_id]][$form_group_id][$request];
                    $return['is_completed'] = $data[self::groupById[$form_group_id]][$form_group_id]['is_completed'] == 'Completed' ? true : false;
                    $return['form_detail'] = $data[self::groupById[$form_group_id]][$form_group_id];

                    return $return;
                }
            }
            return $data;
        } else {
            return false;
        }

    }

    private function getRequests($where=null,$is_accepted = false,$current_id = null,$staff_id=null,$paginate=null) {
        $role = new UserRolesHelper();
        $groupRequest = new requestsGroupModel();
        $paginate = 1;
        Paginator::currentPageResolver(function () use ($paginate) {
            return $paginate;
        });

        if(!is_array($where[key($where)])){
            $where[key($where)] = [$where[key($where)]];
        }

        if ($is_accepted) {
            if ($role->inStaff($staff_id)) {
                $groups = $groupRequest
                    ->where(
                        [
                            'is_accepted' => 1
                        ]
                    )
                    ->where($where)
                    ->whereNotIn('form_group_id',[5,6])
                    ->whereNotNull('published_at')
                    ->orderby('created_at','desc');

                if(request()->has('month')){
                    $groups->where(DB::raw('MONTH(created_at)'), request()->get('month'));
                }
                
                if(request()->has('year')){
                    $groups->where(DB::raw('YEAR(created_at)'), request()->get('year'));
                }

                $groups = $groups->get()->groupBy('id');
            } else {

                $groups = $groupRequest
                    ->where(
                        [
                            'is_accepted' => 1
                        ]
                    )
                    ->where($where)
                    ->whereNotIn('form_group_id',[1,2,4,3])
                    ->whereNotNull('published_at')
                    ->orderby('created_at','desc');

                if(request()->has('month')){
                    $groups->where(DB::raw('MONTH(created_at)'), request()->get('month'));
                }
                
                if(request()->has('year')){
                    $groups->where(DB::raw('YEAR(created_at)'), request()->get('year'));
                }

                $groups = $groups->get()->groupBy('id');
            }
        } else {
            if ($role->inStaff($staff_id)) {
                $groups = $groupRequest
                    ->whereIn(key($where), $where[key($where)] )
                    ->whereNotIn('form_group_id',[5,6])
                    ->whereNotNull('published_at')
                    ->orderby('created_at','desc');

                if(request()->has('month')){
                    $groups->where(DB::raw('MONTH(created_at)'), request()->get('month'));
                }
                
                if(request()->has('year')){
                    $groups->where(DB::raw('YEAR(created_at)'), request()->get('year'));
                }

                $groups = $groups->get()->groupBy('id');
            } else {
                $groups = $groupRequest
                    ->where($where)
                    ->whereNotIn('form_group_id',[1,2,4,3])
                    ->whereNotNull('published_at')
                    ->orderby('created_at','desc');

                if(request()->has('month')){
                    $groups->where(DB::raw('MONTH(created_at)'), request()->get('month'));
                }
                
                if(request()->has('year')){
                    $groups->where(DB::raw('YEAR(created_at)'), request()->get('year'));
                }

                $groups = $groups->get()->groupBy('id');
            }
        }
        if ($groups->isEmpty()) {
            return false;
        } else {
            $groups = $groups->toArray();
        }

        if(key($where) == 'id') {

            if ($role->inStaff($staff_id)) {
                $requests = $this
                    ->where(['requests_group_id' => $where['id']])
                    ->whereNotIn('form_group_id',[5,6])
                    ->orderby('created_at','desc');

                if(request()->has('month')){
                    $requests->where(DB::raw('MONTH(created_at)'), request()->get('month'));
                }
                
                if(request()->has('year')){
                    $requests->where(DB::raw('YEAR(created_at)'), request()->get('year'));
                }

                $requests = $requests->get()->groupBy('requests_group_id');
            } else {
                $requests = $this
                    ->where(['requests_group_id' => $where['id']])
                    ->whereNotIn('form_group_id',[1,2,3,4])
                    ->orderby('created_at','desc');
                
                    if(request()->has('month')){
                        $requests->where(DB::raw('MONTH(created_at)'), request()->get('month'));
                    }
                    
                    if(request()->has('year')){
                        $requests->where(DB::raw('YEAR(created_at)'), request()->get('year'));
                    }
    
                $requests = $requests->get()->groupBy('requests_group_id');
            }
        } else {
            if ($role->inStaff($staff_id)) {
                $requests = $this
                    ->whereIn(key($where), $where[key($where)] )
                    ->whereNotIn('form_group_id',[5,6])
                    ->orderby('created_at','desc');
                
                    if(request()->has('month')){
                        $requests->where(DB::raw('MONTH(created_at)'), request()->get('month'));
                    }
                    
                    if(request()->has('year')){
                        $requests->where(DB::raw('YEAR(created_at)'), request()->get('year'));
                    }
    
                $requests = $requests->get()->groupBy('requests_group_id');
            } else {
                $requests = $this
                    ->where($where)
                    ->whereNotIn('form_group_id',[1,2,3,4])
                    ->orderby('created_at','desc');
                
                    if(request()->has('month')){
                        $requests->where(DB::raw('MONTH(created_at)'), request()->get('month'));
                    }
                    
                    if(request()->has('year')){
                        $requests->where(DB::raw('YEAR(created_at)'), request()->get('year'));
                    }
    
                $requests = $requests->get()->groupBy('requests_group_id');
            }
        } 

        $result = [];
        $temp = [];
        foreach ($requests->toArray() as $key => $values) {
            foreach ($values as $value) {
                $temp[$value['form_group_id']][$value['requests_group_id']][] = $value;
            }
        }
        $requests = $temp;
        foreach ($groups as $group) {
            //pengelompokan data berdasarkan form type kemudian request group
            $group = $group[0];
            $form_group_id = $group['form_group_id'];
            if (isset($requests[$form_group_id])) {

                foreach ($requests[$form_group_id] as $key=>$request):

                    if(isset($groups[$key])){
                        foreach ($request as $value) {
                            //ambil detail data yang ada di form
                            $temp = explode('_',$value['form_answer']);
                            $name = (isset($temp[2]) ? $temp[2] : '').(isset($temp[3]) ? '_'.$temp[3]:'').(isset($temp[4]) ? '_'.$temp[4]:'');
                            $result[self::groupById[$form_group_id]][$form_group_id][$key][$name] = $value['request_content'];
                        }


                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['user_id'] = $groups[$key][0]['user_id'];
                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['form_group_id'] = $groups[$key][0]['form_group_id'];
                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['request_group_id'] = $groups[$key][0]['id'];
                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['is_accepted'] = $groups[$key][0]['is_accepted'];
                        if ($current_id) {
                            //jika request diapprove oleh hr/manager yang sedang login
                            if ($groups[$key][0]['first_accepter'] == $current_id || $groups[$key][0]['second_accepter'] == $current_id) {
                                $result[self::groupById[$form_group_id]][$form_group_id][$key]['accepted_by_me'] = true;
                            } else {
                                $result[self::groupById[$form_group_id]][$form_group_id][$key]['accepted_by_me'] = false;
                            }
                        }

                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['is_completed_by_manager'] = $groups[$key][0]['is_completed_by_manager'];
                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['is_completed_by_admin'] = $groups[$key][0]['is_completed_by_admin'];
                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['is_completed_by_staff'] = $groups[$key][0]['is_completed_by_staff'];

                        if ($groups[$key][0]['is_completed'] == 0) {
                            //jika requests selesai direview
                            $result[self::groupById[$form_group_id]][$form_group_id][$key]['is_completed'] = 'On Progress';
                            $result[self::groupById[$form_group_id]][$form_group_id][$key]['is_completed_class'] = 'badge-info';
                        } else {
                            //jika requests belum selesai
                            $result[self::groupById[$form_group_id]][$form_group_id][$key]['is_completed'] = 'Completed';
                            $result[self::groupById[$form_group_id]][$form_group_id][$key]['is_completed_class'] = 'badge-success';
                        }

                        //set default state for accepter
                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['first_accepter'] = 'Approved';
                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['first_accepter_id'] = $groups[$key][0]['first_accepter'];
                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['first_accepter_class'] = 'badge-success';
                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['second_accepter'] = 'Approved';
                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['second_accepter_id'] = $groups[$key][0]['second_accepter'];
                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['second_accepter_class'] = 'badge-success';
                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['is_accepted'] = true;

                        if(is_null($groups[$key][0]['first_accepter'])) {
                            //jika belum diapprove manager
                            $result[self::groupById[$form_group_id]][$form_group_id][$key]['first_accepter'] = 'Waiting Approval';
                            $result[self::groupById[$form_group_id]][$form_group_id][$key]['first_accepter_class'] = 'badge-info';
                            $result[self::groupById[$form_group_id]][$form_group_id][$key]['is_accepted'] = false;

                        }

                        if (is_null($groups[$key][0]['second_accepter'])) {
                            //jika belum diapprove hr
                            $result[self::groupById[$form_group_id]][$form_group_id][$key]['second_accepter'] = 'Waiting Approval';
                            $result[self::groupById[$form_group_id]][$form_group_id][$key]['second_accepter_class'] = 'badge-info';
                            $result[self::groupById[$form_group_id]][$form_group_id][$key]['is_accepted'] = false;
                        }

                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['is_rejected'] = false;
                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['rejected_notes'] = null;

                        if($groups[$key][0]['is_rejected'] == 1) {
                            if(is_null($groups[$key][0]['first_accepter'])) {
                                //rejected by manager
                                $result[self::groupById[$form_group_id]][$form_group_id][$key]['first_accepter'] = 'Rejected';
                                $result[self::groupById[$form_group_id]][$form_group_id][$key]['first_accepter_class'] = 'badge-default';

                                $result[self::groupById[$form_group_id]][$form_group_id][$key]['second_accepter'] = 'Rejected';
                                $result[self::groupById[$form_group_id]][$form_group_id][$key]['second_accepter_class'] = 'badge-default';
                            }
                            elseif (is_null($groups[$key][0]['second_accepter'])) {
                                //rejected by hr
                                $result[self::groupById[$form_group_id]][$form_group_id][$key]['first_accepter'] = 'Rejected';
                                $result[self::groupById[$form_group_id]][$form_group_id][$key]['first_accepter_class'] = 'badge-default';

                                $result[self::groupById[$form_group_id]][$form_group_id][$key]['second_accepter'] = 'Rejected';
                                $result[self::groupById[$form_group_id]][$form_group_id][$key]['second_accepter_class'] = 'badge-default';
                            }

                            $result[self::groupById[$form_group_id]][$form_group_id][$key]['is_rejected'] = true;
                            $result[self::groupById[$form_group_id]][$form_group_id][$key]['rejected_notes'] = $groups[$key][0]['rejected_notes'];
                        }

                        $rating = ratingModel::where(['request_group_id'=>$groups[$key][0]['id']])->avg('rating');
                        $rated_by_staff = ratingModel::where(['request_group_id'=>$groups[$key][0]['id'], 'staff_id'=>Sentinel::check()->id])->get(['id'])->first() ? true : false;

                        $rating_detail = ratingModel::join('users',function ($join) use ($groups,$key){
                            $join
                                ->on('users.id','=','rating.staff_id')
                                ->where(['request_group_id'=>$groups[$key][0]['id']]);
                        })
                        ->join('employee',function ($join)  use ($groups,$key) {
                            $join
                                ->on('users.username','=','employee.nik');
                        })
                        ->get(['rating.rating','employee.full_name','employee.nik','rating.notes','rating.attachment'])
                        ->toArray();

                        $rating_by_staff = ratingModel::where(['request_group_id'=>$groups[$key][0]['id'], 'staff_id'=>Sentinel::check()->id])->get(['rating'])->first();
                        $rating_by_staff = $rating_by_staff ? number_format($rating_by_staff->rating,1) : null;

                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['rating'] = $rating ? number_format($rating,1) : null;
                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['rating_detail'] = $rating_detail;
                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['rated_by_staff'] = $rated_by_staff;
                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['rating_by_staff'] = $rating_by_staff;

                        if(!isset($result[self::groupById[$form_group_id]][$form_group_id][$key]['employee_name'])){
                            $employee_data = UserModel::where((new UserModel)->getTable().'.id', $result[self::groupById[$form_group_id]][$form_group_id][$key]['user_id'])
                                ->join((new EmployeeModel)->getTable(), (new EmployeeModel)->getTable().'.email', '=', (new UserModel)->getTable().'.email')
                                ->first();
                            
                            $result[self::groupById[$form_group_id]][$form_group_id][$key]['employee_name'] = $employee_data->full_name;
                            $result[self::groupById[$form_group_id]][$form_group_id][$key]['employee_nik'] = $employee_data->nik;
                        }

                        // ini buat ambil line manager//
                        $user_typ = NULL;
                        $id_us =  $groups[$key][0]['user_id'];
                        $arr_line = $role->getLeaders((int)$id_us);

                        $user = UserModel::where('id',$id_us)->first();
                        
                        $user_typ = $arr_line['user_type'];
                        $name_manager = '-';
                        $email = '';

                        //$email = $user->email;
                        if (isset($user->email)) {
                            $email = $user->email;
                        }else{
                            $email = '';   
                        }
                        
                        if ($arr_line['user_type'] != NULL) {
                             if ( $user_typ == 'staff' || $user_typ == 'sroff' || $user_typ == 'off') {
                                 //mengambil avp
                                 if (array_key_exists('avp', $arr_line)) {
                                     if (is_array($arr_line['avp']) && array_key_exists('name', $arr_line['avp'])) {
                                         $atasan = $arr_line['avp']['name'];
                                     }else{
                                         $atasan = '-';
                                         $manag = $role->manag_id($email);
                                         $atasan = $manag['manager'];
                                     }
                                 }else{
                                     //mengambil vp
                                     if (array_key_exists('vp', $arr_line)) {
                                         if (is_array($arr_line['vp']) && array_key_exists('name', $arr_line['vp'])) {
                                             $atasan = $arr_line['vp']['name'];
                                         }else{
                                             $atasan = '-';
                                             $manag = $role->manag_id($email);
                                             $atasan = $manag['manager'];
                                         }
                                     }else{
                                         //mengambil svp
                                         if (array_key_exists('svp', $arr_line)) {
                                             if (is_array($arr_line['svp']) && array_key_exists('name', $arr_line['svp'])) {
                                                 $atasan = $arr_line['svp']['name'];
                                             }else{
                                                 $atasan = '-';
                                                 $manag = $role->manag_id($email);
                                                 $atasan = $manag['manager'];
                                             }
                                         }else{
                                             //end else
                                             $atasan = '-';
                                             $manag = $role->manag_id($email);
                                             $atasan = $manag['manager'];
                                         }
                                     }
                                 }
                             }elseif ( $user_typ == 'avp') {
                                 if (array_key_exists('vp', $arr_line)) {
                                     if (is_array($arr_line['vp']) && array_key_exists('name', $arr_line['vp'])) {
                                         $atasan = $arr_line['vp']['name'];
                                     }else{
                                         $atasan = '-';
                                         $manag = $role->manag_id($email);
                                         $atasan = $manag['manager'];
                                     }
                                 }else{
                                     if (array_key_exists('svp', $arr_line)) {
                                         if (is_array($arr_line['svp']) && array_key_exists('name', $arr_line['svp'])) {
                                             $atasan = $arr_line['svp']['name'];
                                         }else{
                                             $atasan = '-';
                                         }
                                     }else{
                                         //end else
                                         $atasan = '-';
                                         $manag = $role->manag_id($email);
                                         $atasan = $manag['manager'];
                                     }
                                 }
                             }elseif ( $user_typ == 'vp') {
                                 if (array_key_exists('svp', $arr_line)) {
                                     if (is_array($arr_line['svp']) && array_key_exists('name', $arr_line['svp'])) {
                                         $atasan = $arr_line['svp']['name'];
                                     }else{
                                         $atasan = '-';
                                         $manag = $role->manag_id($email);
                                         $atasan = $manag['manager'];
                                     }
                                 }else{
                                     if (array_key_exists('dir', $arr_line)) {
                                         if ( is_array($arr_line['dir']) && array_key_exists('name', $arr_line['dir'])) {
                                             $atasan = $arr_line['dir']['name'];
                                         }else{
                                             $atasan = '-';
                                             $manag = $role->manag_id($email);
                                             $atasan = $manag['manager'];
                                         }
                                     }else{
                                         //end else
                                         $atasan = '-';
                                         $manag = $role->manag_id($email);
                                         $atasan = $manag['manager'];
                                     }
                                 }
                             }elseif ($user_typ == 'svp') {
                                 //ambil dir
                                 if (array_key_exists('dir', $arr_line)) {
                                     if (is_array($arr_line['dir']) && array_key_exists('name', $arr_line['dir'])) {
                                         $atasan = $arr_line['dir']['name'];
                                     }else{
                                         $atasan = '-';
                                         $manag = $role->manag_id($email);
                                         $atasan = $manag['manager'];
                                     }
                                 }else{
                                     //end else
                                     $atasan = '-';
                                     $manag = $role->manag_id($email);
                                     $atasan = $manag['manager'];
                                 }
                             }else{
                             //isi sesuai manager_id
                             $manag = $role->manag_id($email);
                             $atasan = $manag['manager'];
                             }
                        }else{
                             //kosong di bagian user type
                             $atasan = '-';
                        }
                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['manag'] = $atasan;
                        // end ini buat ambil line manager
                        
                        //new count rating dari table rating
                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['count_rating'] = "";
                        $rating_count = ratingModel::where('request_group_id','=',$groups[$key][0]['id'])->count();
                        $result[self::groupById[$form_group_id]][$form_group_id][$key]['count_rating'] = $rating_count;
                        // end new

                    }

                endforeach;
            }
        }

        return $result;
    }

    private function setGroupRequests($data) {
        $return = [
            'user_id' => $data['user_id'],
            'form_group_id' => $data['form_group_id'],
            'last_step' => isset($data['last_step']) ? true :false,
        ];

        return $return;
    }

    private function isExists($where){
        return $this
            ->where($where)
            ->get()->isNotEmpty();
    }

    public function groupRequest($datas,$history=false) {

        $user = UserModel::get()->groupBy('id')->toArray();
        $employee = EmployeeModel::get()->groupBy('nik')->toArray();
        $role = new UserRolesHelper();


        $new = $approved = $completed = $rejected = [];
        if (is_array($datas)) {
            foreach ($datas as $formType=>$data) {
                foreach ($data as $formTypeId=>$items){
                    foreach ($items as $requestId => $item) {
                        if(isset($user[$item['user_id']])){
                            $item['employee_name'] = isset ($employee[$user[$item['user_id']][0]['username']] ) ? $employee[$user[$item['user_id']][0]['username']][0]['full_name'] : $user[$item['user_id']][0]['username'];
                        }else{
                            $item['employee_name'] = isset($user[$item['user_id']][0]) ? $user[$item['user_id']][0]['username'] : '-';
                        }

                        if($item['is_rejected']){
                            $rejected[$requestId] = $item;
                        } elseif( $role->inManager() && !($item['is_completed_by_manager']) && ( $item['form_group_id'] == 2) && !$item['is_accepted'] ) {
                            $new[$requestId] = $item;
                        }
                        elseif ($history && $item['is_completed_by_manager'] ) {
                            $approved[$requestId] = $item;
                        }
                        elseif ( ($history || in_array(\Route::current()->getName(), ['dashboard.traineeRequest', 'dashboard.traineeRequestExportTrainee'])) && $item['is_accepted'] && $item['is_completed'] != 'Completed' ) {
                            $approved[$requestId] = $item;
                        }
                        elseif ($history && $item['is_completed_by_manager'] ) {
                            $approved[$requestId] = $item;
                        }

                        elseif ( $item['is_accepted'] && $item['is_completed'] != 'Completed' && in_array(\Route::current()->getName(), ['dashboard.traineeRequest','trainee.myTrainee']) ) {
                            if ($role->inAdmin()){
                                if($item['is_completed'] == 'Completed'){
                                    $completed[$requestId] = $item;
                                } else {
                                    $new[$requestId] = $item;
                                }
                            }

                            if ( (in_array(\Route::current()->getName(), ['trainee.myTrainee','dashboard.traineeRequest']) && ( $role->inManager() || $role->inAdmin() ) && !$item['is_completed_by_manager'] && !$item['is_accepted'] ) || ( ( ($role->inManager() && !$item['is_completed_by_manager'])) && ($item['form_group_id'] == 2) ) ) {

                                $new[$requestId] = $item;
                            }elseif(($role->inStaff() && $item['is_completed_by_staff'])){
                                $completed[$requestId] = $item;
                            }
                            elseif( !($role->inAdmin() && $item['is_completed_by_manager']) && $item['is_completed'] != 'Completed' ) {
                                $approved[$requestId] = $item;
                            } elseif(($role->inAdmin() && $item['is_completed_by_manager']) || $item['is_completed'] != 'On Progress'){
                                $completed[$requestId] = $item;
                            }
                        }
                        elseif($role->inStaff() &&  ($item['form_group_id'] == 4 && $item['is_completed'] != 'On Progress')){

                            $completed[$requestId] = $item;
                        }
                        elseif ($role->inStaff() && ($item['is_accepted'] && $item['is_completed_by_staff'] != 1)){
                            if($item['is_completed'] == 'Completed' && $item['is_completed_by_staff']){
                                $completed[$requestId] = $item;
                            }else{
                                $approved[$requestId] = $item;
                            }
                        }
                        elseif ( $item['is_completed'] != 'On Progress') {
                            if($item['is_completed_by_staff'] == 0){
                                $approved[$requestId] = $item;
                            }else{
                                $completed[$requestId] = $item;
                            }
                        }
                        else {
                            $new[$requestId] = $item;
                        }
                    }
                }
            }
            
            $data = [
                'new' => collect($new)->sortKeysDesc()->toArray(),
                'approved' => collect($approved)->sortKeysDesc()->toArray(),
                'completed' => collect($completed)->sortKeysDesc()->toArray(),
                'rejected' => collect($rejected)->sortKeysDesc()->toArray()
            ];
            return $data;
        } else {
            return null;
        }

    }
}
