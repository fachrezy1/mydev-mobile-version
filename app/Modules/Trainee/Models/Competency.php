<?php

namespace App\Modules\Trainee\Models;

use Illuminate\Database\Eloquent\Model;

class Competency extends Model
{
    //
    protected $table = 'competency';
}