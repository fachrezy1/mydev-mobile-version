<?php

namespace App\Modules\Trainee\Models;

use Illuminate\Database\Eloquent\Model;

class users_point_history_model extends Model
{
    //
    protected $table = 'users_point_history';
    protected $fillable = ['user_id','form_group_id','requests_group_id','point', 'description'];
    protected $guarded = ['created_at','deleted_at','updated_at'];
}
