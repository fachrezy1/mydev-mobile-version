<?php

namespace App\Modules\Trainee\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationsModel extends Model
{
    //
    protected $table = 'notifications';
    protected $primaryKey = 'id';
    protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['read_at'];
}
