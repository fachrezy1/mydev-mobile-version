<?php

namespace App\Modules\Trainee\Models;

use Illuminate\Database\Eloquent\Model;

class reviewModel extends Model
{
    //
    protected $table = 'reviews';
    protected $fillable = ['user_id','form_group_id','form_type','reviews_goal','reviews_result','reviews_feedback','reviews_action','reviews_date'];
    protected $primaryKey = 'id';

    function add($data) {

    }
}
