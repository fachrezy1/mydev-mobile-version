<?php

namespace App\Modules\Trainee\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RequestsUserModel extends Model
{
    //
    use SoftDeletes;

    protected $table = 'requests_user';
    protected $fillable = [
        'user_id','requests_group_id','user_position'];

    protected $primaryKey = 'id';
}
