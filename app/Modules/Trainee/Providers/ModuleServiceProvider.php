<?php

namespace App\Modules\Trainee\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('trainee', 'Resources/Lang', 'app'), 'trainee');
        $this->loadViewsFrom(module_path('trainee', 'Resources/Views', 'app'), 'trainee');
        $this->loadMigrationsFrom(module_path('trainee', 'Database/Migrations', 'app'), 'trainee');
        $this->loadConfigsFrom(module_path('trainee', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('trainee', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
