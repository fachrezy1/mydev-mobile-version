@php($helper = new \App\Helpers\UserRolesHelper )

@extends($helper->inStaff() ? 'core.trainee.main' : 'core.admin.main')

@section('content')
<main class="main-content">
    <section class="trainee-block center">
        <div class="container">
            <div class="trainee-title">
                <h4>Hooray ! Indosat Ooredoo is supporting my optimum development !</h4>
                <h1>I'm Here For Requesting</h1>
            </div>


            @if(!$helper->inAdmin())
                <div class="text-center request-label wrap-trainee-request-button">
                    @if($user->is_block == 0)
                        <b class="trainee-request-button">Click here</b> for requesting access registrasion form
                    @elseif($user->is_block == 1)
                        You have already sent a request, please contact admin for further information
                    @endif
                </div>
            @endif

            {{-- basic --}}
            <div class="trainee-box">
                @foreach($trainees as $key=>$trainee)
                    <div class="item-card">
                        <div class="item-card-top">
                            <i class="{{ $trainee->icon }}"></i>
                        </div>
                        <div class="item-card-description">
                            <span class="circle-badge">
                                <i class="icon-note-1"></i>
                            </span>
                             <em class="trainee-text">{{ ( $trainee->label== "External Speaker")? "To be" : "To join" }}</em>
                            <h3>{{ $trainee->label }}</h3>

                            <span class="item-card-ext">
                                <p>
                                    {{ $trainee->description }}
                                </p>
                                @if(!$helper->inAdmin())
                                    <button type="button" class="btn btn-default trainee-button {{ ($user->is_block == 0) ? 'trainee-request-button' : '' }}" data-href="{{ url($trainee->slug) }}" {{ ($user->is_block == 1 ) ? 'disabled' : '' }}>Select</button>
                                @else   
                                    <button type="button" class="btn btn-default trainee-button" onClick="window.location = '{{ url($trainee->slug) }}'">Select</button>
                                @endif
                            </span>
                        </div>
                    </div>
                @endforeach
            </div>
            <!--

						{{-- slider --}}
            <div class="trainee-box" id="slide">
                @foreach($trainees as $key=>$trainee)
                    <div class="item-card">
                        <div class="item-card-top">
                            <i class="{{ $trainee->icon }}"></i>
                        </div>
                        <div class="item-card-description">
                            <span class="circle-badge">
                                <i class="icon-note-1"></i>
                            </span>
                             <em class="trainee-text">{{ ( $trainee->label== "External Speaker")? "To be" : "To join" }}</em>
                            <h3>{{ $trainee->label }}</h3>

                            <span class="item-card-ext">
                                <p>
                                    {{ $trainee->description }}
                                </p>
                                @if(!$helper->inAdmin())
                                    <button type="button" class="btn btn-default trainee-button {{ ($user->is_block == 0) ? 'trainee-request-button' : '' }}" data-href="{{ url($trainee->slug) }}" {{ ($user->is_block == 1 ) ? 'disabled' : '' }}>Select</button>
                                @else   
                                    <button type="button" class="btn btn-default trainee-button" onClick="window.location = '{{ url($trainee->slug) }}'">Select</button>
                                @endif
                            </span>
                        </div>
                    </div>
                @endforeach
            </div>
        -->
        </div>
    </section>
</main>


<div class="modal fade" id="traineeRequestModal" tabindex="-1" role="dialog" aria-labelledby="traineeRequestModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="traineeRequestModalLabel">You need access permission</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-training-request" action="{{route('trainee.requestTraining')}}" method="post">
                    <p><input class="form-check-input training-term" id="request1" type="checkbox" value="1" required> <label for="request1">I have discussed with my Line Manager regarding my development</label></p>
                    <p><input class="form-check-input training-term" id="request2" type="checkbox" value="1" required> <label for="request2">I have confirmed with HR regarding my development</label></p>

                    <center><input type="submit" class="btn btn-default center submit-training-request" value="Request Access"></center>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="traineeRequesMessagetModal" tabindex="-1" role="dialog" aria-labelledby="traineeRequesMessagetModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="traineeRequesMessagetModalLabel">Training Request</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body request-message">
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
{{-- Slider --}}
<link href="{{url('js/lib/slick/slick.css')}}" type="text/css" rel="stylesheet"/>
<link href="{{url('js/lib/slick/slick-theme.css')}}" type="text/css" rel="stylesheet"/>
<style>

	@media (min-width: 768px) {
		.main-content {
			padding-top: 60px
		}
	}
	.main-header.clean {
		background-color: #fff;
	}

	.slick-prev {
		left: 10px;
		z-index: 100;
		width: 40px;
		height: 40px;
	}
	.slick-next {
		right: 10px;
		z-index: 100;
		width: 40px;
		height: 40px;
	}
	.slick-prev:before, .slick-next:before {
		font-size: 40px;
	}
</style>
@endsection

@section('scripts')
	{{-- Slider --}}
	<script src="{{ url('js/lib/slick/slick.min.js') }}"></script>

  <script type="text/javascript">
    jQuery(function($){
        $('#traineeDetail').on('shown.bs.modal', function (e) {
            $('.scroll-content').slimScroll({
                height: '330px',
                size: '5px',
                position: 'right',
                color: '#fcd401',
                alwaysVisible: true,
                distance: '0px',

                railVisible: true,
                railColor: '#222',
                railOpacity: 0.1,

                allowPageScroll: true,
                disableFadeOut: false
            });
        });

        $(".trainee-option").owlCarousel({
            items: 4,
            loop: false,
            margin: 10,
            nav: true,
            dots: false,
            smartSpeed: 900,
            autoplay: true,
            navigationText: ["<i class='mdi mdi-chevron-left'></i>", "<i class='mdi mdi-chevron-right'></i>"],
            responsive: {
                0: {
                    items: 2
                },
                990: {
                    items: 3
                },
                1023: {
                    items: 5
                }
            }
        });

        @if(!$helper->inAdmin())
            $(document).find('.trainee-button').click(function(){
                @if($user->is_block == 2)
                    location.href = $(this).data('href');
                @endif
            });

            $(document).find('.trainee-request-button').click(function(){    
                $(document).find('.training-term').prop('checked', false);
                $('#traineeRequestModal').modal('show');
            });

            $(document).find('#form-training-request').submit(function(){
                var targetUrl = $(this).attr('action');

                $.ajax({
                    url: targetUrl,
                    type: "GET",
                    dataType: "JSON",
                    data: { 
                        userID: "{{ $user->id }}"
                    },
                    success: function(data){
                        $(document).find('.training-term').prop('checked', false);
                        $('#traineeRequestModal').modal('hide');
                        $(document).find('.request-message').html(data.message);
                        $('#traineeRequesMessagetModal').modal('show');
                        $('.request-label').text('You have already sent a request, please contact admin for further information');
                    }
                });
                return false;
            });
        @endif
    });

    $(document).ready(function() {
    	$('#slide.trainee-box').slick({
    		autoplay: false,
    		infinite: false,
    		arrows: true,
			  slidesToShow: 2,
			  slidesToScroll: 1,
			  responsive: [
			    {
			      breakpoint: 1024,
			      settings: {
			        slidesToShow: 2,
			        slidesToScroll: 1,
			      }
			    },
			    {
			      breakpoint: 600,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1
			      }
			    },
			  ]
    	})
    });

  </script>
@endsection
