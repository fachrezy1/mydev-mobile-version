@php
    $user = \Sentinel::check();
    $helper = new \App\Helpers\UserRolesHelper();
@endphp

@extends($helper->inManager() ? 'core.admin.main' :'core.trainee.main')

@section('content')
    @php
        $data_exists = false
    @endphp
    <main class="main-content">
        <div class="main-dashboard">
            <div class="dashboard-content {{$helper->inManager() ? 'w-100' : ''}}">
                <div class="{{$helper->inManager() ? 'container' : 'container-fluid'}}">
                    {{--@dd($trainees)--}}
                    <div class="dashboard-entry ">
                        <div class="side-box">
                            <div class="side-box-description">
                                <h1>{{$form['form_detail']['employee_name']}}</h1>
                                <div class="small-box-text">
                                    <p>
                                        {{$form['form_detail']['employee_nik']}}
                                    </p>
                                </div>
                            </div>
                            <div class="side-box-entry">
                                <div class="entry-icon">
                                    <i class="mdi mdi-file-document-box-outline"></i>
                                    <div class="entry-icon-text">
                                        <h3>Form Type</h3>
                                        <p>{{$trainees['name']}} Goal Form</p>
                                    </div>
                                </div>
                                <div class="entry-icon">
                                    <i class="mdi mdi-subtitles-outline"></i>
                                    <div class="entry-icon-text">
                                        <h3>Topic / Title / Goal</h3>
                                        {{--@dd($form)--}}
                                        <p>{{
                                            ucwords( isset($form['form_detail']['title']) ? $form['form_detail']['title'] : ( isset($form['form_detail']['goal']) ?
                                            $form['form_detail']['goal'] : ( isset($form['form_detail']['development_goal']) ?
                                            ( @unserialize($form['form_detail']['development_goal']) ? implode(", ",unserialize($form['form_detail']['development_goal'])) : $form['form_detail']['development_goal'])
                                             : (isset($form['form_detail']['development_goal']) ? $form['form_detail']['development_goal'] : $form['form_detail']['line_topic']) ) ) ) }} </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="{{$helper->inManager() ? 'container' : 'container-fluid'}}">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{session('success')}}
                        </div>
                    @endif
                    <form  method="post" action="{{ route('trainee.request.submitReviews') }}" enctype="multipart/form-data" id="form_review">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$form['user_id']}}">
                        <input type="hidden" name="request_group_id" value="{{$form['request_group_id']}}">
                        <input type="hidden" name="form_type" value="{{$form['form_type']}}">
                        @php
                            $reviews_date
                                = $form_type
                                = $reviews_goal
                                = $reviews_result
                                = $reviews_feedback
                                = $reviews_action
                                = $reviews_course_name
                                = $reviews_session_number
                                = $reviews_duration
                                = $reviews_condition
                                = $reviews_committed_date
                                = $reviews_criteria
                                = $reviews_project_title
                                = $reviews_location
                                = $reviews_provider
                                = $reviews_country
                                = $reviews_topic
                                = $reviews_end_date
                                = $reviews_cost
                                = $reviews_attachment
                                = null;
                            $is_disabled = null;
                        @endphp
                        @if ($current_data)
                            <input type="hidden" name="review_id[]" value="{{$current_data[0]['id']}}">
                            @php
                                $data_exists = true;
                                $reviews_date = $current_data[0]['reviews_date'];
                                $reviews_goal = $current_data[0]['reviews_goal'];
                                $reviews_result = $current_data[0]['reviews_result'];
                                $reviews_feedback = $current_data[0]['reviews_feedback'];
                                $reviews_action = $current_data[0]['reviews_action'];
                                $reviews_course_name = $current_data[0]['reviews_course_name'];
                                $reviews_session_number = $current_data[0]['reviews_session_number'];
                                $reviews_duration = $current_data[0]['reviews_duration'];

                                $reviews_condition = $current_data[0]['reviews_condition'];
                                $reviews_committed_date = $current_data[0]['reviews_committed_date'];

                                $reviews_criteria = $current_data[0]['reviews_criteria'];
                                $reviews_project_title = $current_data[0]['reviews_project_title'];

                                $reviews_location = $current_data[0]['reviews_location'];
                                $reviews_provider = $current_data[0]['reviews_provider'];
                                $reviews_country = $current_data[0]['reviews_country'];
                                $reviews_topic = $current_data[0]['reviews_topic'];
                                $reviews_end_date = $current_data[0]['reviews_end_date'];
                                $reviews_cost = $current_data[0]['reviews_cost'];

                                $reviews_attachment = $current_data[0]['reviews_attachment'];

                                $flagging = $current_data[0]['flagging'];
                                
                                unset($current_data[0]);
                            @endphp
                        @endif
                        {{--@dd($form['form_type'])--}}
                        @if ($form['form_type'] == 'assignment')
                            @include('trainee::reviews.review-assignment')
                        @elseif($form['form_type'] == 'mentoring')
                            @include('trainee::reviews.review-mentoring')
                        @elseif($form['form_type'] == 'training' || $form['form_type'] == 'inhouse')
                            @include('trainee::reviews.review-training')
                        @elseif($form['form_type'] == 'external_speaker')
                            @include('trainee::reviews.review-external')
                        @elseif($form['form_type'] == 'internal_training')
                            @include('trainee::reviews.review-internal_coach')
                        @endif
                    </form>
                    {{--@dd($form['form_detail'])--}}
                    @if( ($helper->inAdmin() && !$form['form_detail']['is_completed_by_admin']) && ($form['form_detail']['form_group_id'] == 2) )
                        <form id="formApprovePoint" action="{{route('trainee.request.approvePoint')}}" method="post">
                            @csrf
                            <input type="hidden" name="form_group_id" value="{{$form['form_detail']['form_group_id']}}">
                            <input type="hidden" name="request_group_id" value="{{$form['form_detail']['request_group_id']}}">
                            <input type="hidden" name="mentor" value="{{$form['form_detail']['line_manager_nik']}}">
                        </form>
                    @endif
                </div>

                <button type="button" class="btn btn-float btn-fba">
                    <i class="mdi mdi-more"></i>
                </button>
            </div>
            @if ($helper->inStaff())
                @include('sidebar')
            @endif
        </div>

    </main>
    
@include('trainee-modal')

@endsection
@section('styles')
<style>
	@media (max-width: 599px) {
		.side-box {
			padding-right: 15px;
			padding-left: 15px;
		}
	}
	.center-form {
		max-width: none!important;
	}
	.bottom-button {
		margin: 0!important;
		padding: 0;
		max-width: none;
	}
	.bottom-button .btn {
		margin-bottom: 15px!important
	}
	#form_review {
		margin-bottom: 0
	}
</style>
@endsection
