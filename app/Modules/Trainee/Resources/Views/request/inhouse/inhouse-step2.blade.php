@extends('core.trainee.main')

@section('content')
    {{-- <div class="main-site"> --}}
        @include('trainee::request.header')
        <main class="main-content secondary-nav">
            <div class="dashboard-steps">
                <div class="container">
                    <div class="form-section">
                        <form id="submit_step" action="{{route('trainee.request.submitStep')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="form_group_id" value="{{ $formData['type'] }}">
                            <input type="hidden" name="redirect" value="{{ url('trainee/request/'.$redirect.'?step=3') }}">
                            <input type="hidden" name="is_last" value="true">
                            <div class="form-title m-24">
                                <h1>{{$title}} Objectives</h1>
                            </div>



                            <div class="split-field medium">
                                <div class="field-right">
                                    <div class="form-group reset">
                                        
                                         <label class="control-label">
                                       Moderator
                                    </label>
                                    <div class="form-field employees-form">
                                        <div class="box-items">
                                            @if (in_array(\Route::current()->getName(), ['trainee.inhouse']))
                                                <div class="more-item">
                                                    <div class="field-group has-more">
                                                        <label class="control-label text-left">
                                                            Moderator name
                                                        </label>
                                                        <select class="form-control select-employee2">

                                                        </select>
                                                        <input name="training_obj_moderator_name" type="hidden" class="form-control select-employee-name2" placeholder="enter name" >
                                                        @if ($errors->has('training_obj_employee_name'))
                                                            <div class="validation-msg">
                                                                Employee name is Required
                                                            </div>
                                                        @endif

                                                        <label class="control-label text-left">
                                                            Moderator Nik
                                                        </label>
                                                        <input name="training_obj_moderator_nik" type="text" class="form-control nik_coachee2" placeholder="example: 68025191" readonly>
                                                        @if ($errors->has('training_obj_employee_nik'))
                                                            <div class="validation-msg">
                                                                Employee Nik is Required
                                                            </div>
                                                        @endif
                                                        
                                                    </div>
                                                    
                                                </div>
                                           
                                            @endif
                                        </div>
                                    </div>

                                    </div>
                                	<div class="form-group reset">
                                		<label class="control-label">
                                			Training objective
                                		</label>
                                		<div class="form-field">
                                			<textarea class="form-control" name="training_obj_description" rows="6" placeholder="enter objective"></textarea>
                                			{{-- <p class="form-helper text-count">0/255</p> --}}
                                			@if ($errors->has('training_obj_description'))
                                			<div class="validation-msg">
                                				Training objective is Required
                                			</div>
                                			@endif
                                		</div>
                                	</div>
                                	<div class="form-group reset">
                                		<label class="control-label">
                                			Venue
                                		</label>
                                		<div class="form-field d-flex justify-content-between">
                                			<button type="button" class="btn btn-default btn-sm venue_button" data-target="#training_obj_venue_domestic">
                                				<i class="icon-placeholder-outline-maps-interface-tool"></i> Domestic
                                				<input type="radio" class="d-none" id="training_obj_venue_domestic" name="training_obj_venue" value="training_obj_venue_domestic">
                                			</button>
                                			<button type="button" class="btn btn-default btn-sm venue_button training_obj_venue_overseas" data-target="#training_obj_venue_overseas">
                                				<i class="icon-placeholder-outline-maps-interface-tool"></i> Overseas
                                				<input type="radio" class="d-none" id="training_obj_venue_overseas" name="training_obj_venue" value="training_obj_venue_overseas">
                                			</button>
                                		</div>
                                	</div>
                                	<div class="form-group reset">
                                		<label class="control-label">
                                			Employees
                                		</label>
                                		<div class="form-field employees-form">
                                			<div class="box-items">
                                				@if (in_array(\Route::current()->getName(), ['trainee.inhouse']))
                                				<div class="more-item">
                                					<div class="field-group has-more">
                                						<label class="control-label text-left">
                                							Employee name
                                						</label>
                                						<select class="form-control select-employee">
                                						</select>
                                						<input name="training_obj_employee_name[]" type="hidden" class="form-control select-employee-name" placeholder="enter name" >
                                						@if ($errors->has('training_obj_employee_name'))
                                						<div class="validation-msg">
                                							Employee name is Required
                                						</div>
                                						@endif

                                						<div class="row mt-2">
                                							<div class="col-4 pr-1">
                                								{{-- <label class="control-label text-left">
		                                							Employee NIK
		                                						</label> --}}
		                                						<input name="training_obj_employee_nik[]" type="text" id="nik_coachee" class="form-control nik_coachee" placeholder="example: 68025191" readonly>
		                                						@if ($errors->has('training_obj_employee_nik'))
		                                						<div class="validation-msg">
		                                							Employee NIK is Required
		                                						</div>
		                                						@endif
                                							</div>
                                							<div class="col-8 pl-1">
                                								{{-- <label class="control-label text-left">
		                                							Employee Position
		                                						</label> --}}
		                                						<input type="text" name="training_obj_employee_position[]" class="form-control position_employee" value="" placeholder="enter employee position">
		                                						@if ($errors->has('training_obj_employee_position'))
		                                						<div class="validation-msg">
		                                							Employee Position is Required
		                                						</div>
		                                						@endif
                                							</div>
                                						</div>

                                						<a href="javascript:void(0)" id="cloneCloseDiv" class="btn btn-default remove-item"><i class="mdi mdi-close-circle"></i> remove</a>
                                					</div>
                                					<div class="more-item-target">

                                					</div>
                                					@if (in_array(\Route::current()->getName(), ['trainee.inhouse']))
                                					<a href="javascript:void(0)" class="btn btn-default add-more "><i class="mdi mdi-plus-circle"></i> Add more Member</a>
                                					@endif
                                				</div>
                                				@else
                                				<div class="more-item">
                                					<div class="field-group has-more">
                                						<label class="control-label text-left">
                                							Employee name
                                						</label>
                                						<input name="training_obj_employee_name[]" type="text" class="form-control select-employee-name" placeholder="enter name" value="{{$employee['staff']['full_name']}}" readonly>
                                						@if ($errors->has('training_obj_employee_name'))
                                						<div class="validation-msg">
                                							Employee name is Required
                                						</div>
                                						@endif

                                						<div class="row mt-2">
                                							<div class="col-4 pr-1">
                                								{{-- <label class="control-label text-left">
		                                							Employee Nik
		                                						</label> --}}
		                                						<input name="training_obj_employee_nik[]" type="text" id="nik_coachee" class="form-control nik_coachee" placeholder="example: 68025191" value="{{$employee['staff']['nik']}}" readonly>
		                                						@if ($errors->has('training_obj_employee_nik'))
		                                						<div class="validation-msg">
		                                							Employee Nik is Required
		                                						</div>
		                                						@endif
                                							</div>
                                							<div class="col-8 pl-1">
                                								{{-- <label class="control-label text-left">
		                                							Employee Position
		                                						</label> --}}
		                                						<input type="text" name="training_obj_employee_position[]" class="form-control position_employee" value="{{$employee['staff']['position']}}" placeholder="enter employee position" readonly>
		                                						@if ($errors->has('training_obj_employee_position'))
		                                						<div class="validation-msg">
		                                							Employee Position is Required
		                                						</div>
		                                						@endif
                                							</div>
                                						</div>
                                						<a href="javascript:void(0)" id="cloneCloseDiv" class="btn btn-default remove-item"><i class="mdi mdi-close-circle"></i> remove</a>
                                					</div>
                                					<div class="more-item-target">

                                					</div>
                                					@if (in_array(\Route::current()->getName(), ['trainee.inhouse']))
                                					<a href="javascript:void(0)" class="btn btn-default add-more"><i class="mdi mdi-plus-circle"></i> Add more Member</a>
                                					@endif
                                				</div>
                                				@endif


                                				@if (in_array(\Route::current()->getName(), ['trainee.inhouse']))
                                				<div class="form-field d-flex justify-content-between flex-column flex-md-row">
                                					<input id="import" type="file" accept=".csv,.xls,.xlsx" name="training_obj_import_employee" class="btn btn-default reset">
                                					<button type="button" class="btn btn-default btn-block reset mr-md-2">
                                						<label for="import" style="margin-bottom: 0;width: 100%; padding: 0;"><i class="icon-contact"></i> Import Employee</label>
                                					</button>
                                					<a href="https://docs.google.com/spreadsheets/d/14y-AUnF3Ng9Rf59hUY4PqsobcNodPCPHgcUlkkJs_is/edit?usp=sharing" target="_blank" class="btn btn-default btn-block reset ml-md-2">
                                						<label style="margin-bottom: 0;width: 100%; padding: 0;"><i class="icon-contact"></i> View Sample Import Format</label>
                                					</a>
                                				</div>
                                				@endif
                                			</div>
                                		</div>
                                	</div>

                                	<div class="form-group reset">
                                		<label class="control-label">
                                			<button type="button" data-toggle="modal" class="btn-question" data-target="#competencyModal">
                                				<i class="icon-questions-circular-button" >  </i>
                                			</button>
                                			Competency to be Developed
                                		</label>
                                		<div class="form-field mb-3">
                                			<select class="form-control select2-comp" name="training_obj_employee_competency[]" multiple="multiple">
                                				@if (count($competencies)>0)
                                				@foreach($competencies as $key => $c)
                                				<optgroup label="{{ $c['option_group'] }}">
                                					@foreach($c['option_relation'] as $opt_sub)
                                					<option>
                                						{{ $opt_sub['name'] }}
                                					</option>
                                					@endforeach
                                				</optgroup>
                                				@endforeach
                                				@endif
                                			</select>
                                			@if ($errors->has('training_obj_employee_competency'))
                                			<div class="validation-msg">
                                				Competency to be Developed is Required
                                			</div>
                                			@endif
                                		</div>
                                	</div>
                                	@if (in_array(\Route::current()->getName(), ['trainee.inhouse']))
                                	<div class="import-input d-none">

                                	</div>
                                	<div class="form-group reset import-table d-none">
                                		<label class="control-label">
                                			Preview Import Employees<br>
                                			<small class="text-danger"> Please notice : duplicated and invalid NIK or Data will be removed </small>
                                		</label>
                                		<table id="importTable" class="table table-hover table-striped table-lite" cellspacing="0" width="100%">
                                			<thead>
                                				<tr>
                                					<th>Employee Names</th>
                                					<th>Employee Nik</th>
                                					<th>Employee Position</th>
                                				</tr>
                                			</thead>

                                			<tbody id="import-list">

                                			</tbody>
                                		</table>
                                		<small class="text-danger d-none invalid-nik"> Invalid NIK : <span class="invalid-nik-target"></span> </small>
                                	</div>
                                	@endif
                                	@if (in_array(\Route::current()->getName(), ['trainee.publicTrainingStep1']))
                                	<div class="form-group reset">
                                		<div class="form-field">
                                			<div class="multifield">
                                				<label class="control-label">
                                					Commitment After Training *choose max 3 type
                                				</label>
                                				<select class="form-control select2-tags" name="training_obj_commitment_training[]" multiple="multiple">
                                					<option value="Knowledge Sharing">Knowledge Sharing </option>
                                					<option value="Project">Project </option>
                                					<option value="Job Assignment">Job Assignment </option>
                                				</select>
                                				<p class="form-helper text-count">0/255</p>
                                				@if ($errors->has('training_obj_commitment_training'))
                                				<div class="validation-msg">
                                					Commitment After Training is Required
                                				</div>
                                				@endif
                                			</div>
                                			<div class="multifield">
                                				<label class="control-label">
                                					Commited Date
                                				</label>
                                				<div class="field-date">
                                					<div class="date-input">
                                						<input type="text" name="training_obj_start_date2" autocomplete="off" class="form-control start-date2" onchange="dateStartSelector(this)" placeholder="enter start date" required>
                                						<button type="button" class="btn btn-default btn-icon ">
                                							<i class="mdi mdi-calendar"></i>
                                						</button>
                                					</div>
                                					To
                                					<div class="date-input">
                                						<input type="text" name="training_obj_end_date2" autocomplete="off" class="form-control end-date2" placeholder="enter end date" required>
                                						<button type="button" class="btn btn-default btn-icon">
                                							<i class="mdi mdi-calendar"></i>
                                						</button>
                                					</div>
                                				</div>
                                				@if ($errors->has('training_obj_start_date2') || $errors->has('training_obj_end_date2'))
                                				<div class="validation-msg">
                                					Commited Date is Required
                                				</div>
                                				@endif
                                			</div>
                                			<div class="multifield">
                                				<label class="control-label">
                                					Description
                                				</label>
                                				<textarea name="training_obj_commitment_description" class="form-control" rows="2" placeholder="enter text" required></textarea>
                                				<p class="form-helper text-count">0/255</p>
                                				@if ($errors->has('training_obj_commitment_description'))
                                				<div class="validation-msg">
                                					Description is Required
                                				</div>
                                				@endif
                                			</div>
                                		</div>
                                	</div>
                                	@endif
                                	<div class="form-group reset">
                                		<div class="form-field">
                                			<input id="files" type="file" accept=".pdf,doc,docx,image/*" name="training_obj_attachment" class="btn btn-default reset">
                                			<button type="button" class="btn btn-default reset w-100">
                                				<label for="files" style="margin-bottom: 0;width: 100%; padding: 0;"><i class="icon-contact"></i> Add attachment</label>
                                			</button><br>
                                			<small class="name-files">Brochure/Proposal of Training , file pdf,doc,docx,jpeg,jpg and png</small>
                                		</div>
                                	</div>
                                </div>
                                <div class="field-right d-none d-xl-block">
                                    <figure>
                                        <img src="{{ url('images/media05.png') }}" alt="ooredoo" class="img-fluid">
                                    </figure>
                                </div>
                            </div>

                            <div class="bottom-step">
                            	<button type="button" class="btn btn-default" onclick="location.href='{{ url('trainee/request/inhouse?step=1') }}'"><i class="icon-back"></i> Back</button>
                            	<div class="steps-count">
                            		<div class="step-item done">
                            			<span class="circle-number">
                            				<em>1</em>
                            				<i class="icon-verified"></i>
                            			</span>
                            			Training Information
                            		</div>
                            		<div class="step-item active">
                            			<span class="circle-number">
                            				<em>2</em>
                            				<i class="icon-verified"></i>
                            			</span>
                            			Training Objective
                            		</div>

                            		<div class="step-item">
                            			<span class="circle-number">
                            				<em>3</em>
                            				<i class="icon-verified"></i>
                            			</span>
                            			Completed
                            		</div>

                            		<div class="step-item counter">
                            			<p class="mb-0 text-white">Step <span class="text-white">2</span> / 3</p>
                            		</div>
                            	</div>
                            	<button type="submit" class="btn btn-default btn_block" >Completed <i class="icon-next"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    {{-- </div> --}}

    <br><br><br>
    <!-- Modal -->
    @include('modal-competency')
@endsection
@section('styles')
<style>
	.bottom-nav {
		display: none!important
	}
	.dataTables_filter .form-control,
	.dataTables_filter .form-control-sm {
		padding: 5px 10px!important;
		height: 24px!important
	}
</style>
@endsection
@section('scripts')
    <script>
        function format (d) {
            content = [];
            for (i=0;i<d.length;i++){
                content.push('<tr><td>'+d[i].name+'</td>'+
                '<td>'+d[i].definition+'</td>'+
                '</tr>');
            }
            tableCompetenciesRet = '<table class="table-bordered">'+
                        content.toString().replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '') +
                    '</table>';
            return tableCompetenciesRet;
            ;
        }
        var datatableComp = $('#tableCompetencies').DataTable( {
            processing: true,
            serverSide: true,
            scrollX: true,
            bFilter: false,
            ajax: {
                url: "{{ route('trainee.dtCompetencies') }}",
            },
            order: [ 1, 'asc' ],
            columns: [
                { data: 'category' },
                { data: 'competency', render: function ( data ) {
                    return format(data);
                }}
            ],
        });
    </script>
    <script>
        @if(session('message'))
        alert('{{session("message")}}');
        @endif
    </script>
@endsection
