@extends('core.trainee.main')

@section('content')
    @php ($user = \Sentinel::check())
    {{-- <div class="main-site"> --}}
        @include('trainee::request.header')
        <main class="main-content secondary-nav">
            <div class="dashboard-steps">

                <div class="container">

                    <div class="form-section">

                        <div class="form-title">
                            <h1>Thank You</h1>
                            @if ($user->hasAccess('admin.createTrainee'))
                                <p>Trainee list has been created, view your <a href="#">trainee list</a> here</p>
                            @else
                                <p>Your trainee request has sent, please wait for HR Department and Manager line approval</p>
                            @endif
                        </div>

                        <div class="center-form">
                            <figure>
                                <img src="{{url('images/media02.png')}}" alt="" class="img-fluid for-media">
                            </figure>

                        </div>

                        <div class="bottom-step">
                        	<div class="steps-count">
                        		<div class="step-item done">
                        			<span class="circle-number">
                        				<em>1</em>
                        				<i class="icon-verified"></i>
                        			</span>
                        			Training Information
                        		</div>
                        		<div class="step-item done">
                        			<span class="circle-number">
                        				<em>2</em>
                        				<i class="icon-verified"></i>
                        			</span>
                        			Training Objective
                        		</div>

                        		<div class="step-item done">
                        			<span class="circle-number">
                        				<em>3</em>
                        				<i class="icon-verified"></i>
                        			</span>
                        			Completed
                        		</div>
                        		<div class="step-item counter mx-auto mr-md-0">
                        			<p class="mb-0 text-white">Step <span class="text-white">3</span> / 3</p>
                        		</div>
                        	</div>
                        	<button type="button" class="btn btn-default btn-red-default mx-auto mr-md-0 text-center" onclick="location.href='{{ $user->hasAccess('admin.createTrainee') ? route('trainee.myTrainee') : url('trainee/dashboard') }}'">Dashboard <i class="icon-next"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    {{-- </div> --}}

    <br><br><br>

@endsection
