@extends('core.trainee.main')

@section('content')
    {{-- <div class="main-site"> --}}
        @include('trainee::request.header')
        <main class="main-content secondary-nav">
            <div class="dashboard-steps">
                <div class="container">
                    <div class="form-section">
                        <form id="submit_step" action="{{route('trainee.request.submitStep')}}" method="post">
                            @csrf
                            <input type="hidden" name="form_group_id" value="{{ $formData['type'] }}">
                            <input type="hidden" name="redirect" value="{{ url('trainee/request/'.$redirect.'?step=2') }}">

                            <div class="form-title m-24">
                                <h1>{{$title}} Information</h1>
                            </div>
                            <div class="split-field medium">
                                <div class="field-right">
                                    <div class="form-group reset">
                                        <label class="control-label">
                                            Training Title
                                        </label>
                                        <input type="text" name="training_ti_title" class="form-control" placeholder="enter title" maxlength="150">
                                        @if ($errors->has('training_ti_title'))
                                            <div class="validation-msg">
                                                Training Title is Required
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group reset">
                                        <label class="control-label">
                                            Type of programs
                                        </label>
                                        <div class="check-group">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" name="training_ti_type_programs" type="radio" value="internal trainer" checked=""> Internal Trainer
                                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" name="training_ti_type_programs" type="radio" value="external trainer"> External Trainer
                                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                                </label>
                                            </div>
                                            <!--

                                             <div class="form-check">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" name="training_ti_type_programs" type="radio" value="moderator"> Moderator
                                                    <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                                </label>
                                            </div>
                                        -->


                                        </div>
                                    </div>

                                    <div class="form-group reset">
                                    	<div class="external-trainer d-none">
                                        <div class="form-group reset">
                                            <label class="control-label">
                                                Training Provider
                                            </label>
                                            <input type="text" name="training_ti_provider" class="form-control" maxlength="300" placeholder="enter name" disabled>
                                            @if ($errors->has('training_ti_title'))
                                                <div class="validation-msg">
                                                    Training Provider is Required
                                                </div>
                                            @endif
                                        </div>
                                      </div>
                                    </div>
                                    <div class="internal-trainer">
                                    	<div class="more-item">
                                    		<div class="field-group has-more">
                                    			<div class="form-group reset mb-2">
	                                    			<label class="control-label text-left">
	                                    				Trainer name
	                                    			</label>
	                                    			<select class="form-control select-employee">

	                                    			</select>
	                                    			<input name="training_obj_trainer_name" type="hidden" class="form-control select-employee-name" placeholder="enter name" >
                                    		  </div>

                                    			<div class="form-group reset">
	                                    			<label class="control-label text-left">
	                                    				Trainer NIK
	                                    			</label>
	                                    			<input name="training_obj_trainer_nik" type="text" id="nik_coachee" class="form-control nik_coachee" placeholder="example: 68025191" readonly>
	                                    		</div>

                                    		</div>
                                    	</div>
                                    	@if ($errors->has('training_obj_trainer_name') || $errors->has('training_obj_trainer_nik'))
                                    	<div class="validation-msg">
                                    		Trainer name and Trainer Nik is Required
                                    	</div>
                                        @endif
                                    </div>

                                    <div class="form-group reset">
                                        <label class="control-label">
                                            Cost
                                        </label>
                                        <input type="number" name="training_ti_cost" class="form-control" maxlength="10" placeholder="enter cost">
                                        @if ($errors->has('training_ti_cost'))
                                            <div class="validation-msg">
                                                Cost is Required
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group reset">
                                        <label class="control-label">
                                            Date
                                        </label>
                                        <div class="field-date">
                                            <div class="date-input">
                                                <input type="text" name="training_ti_start_date" autocomplete="off" class="form-control start-date" onchange="dateStartSelector(this)" placeholder="enter start date">
                                                <button type="button" class="btn btn-default btn-icon ">
                                                    <i class="mdi mdi-calendar"></i>
                                                </button>
                                            </div>
                                            To
                                            <div class="date-input">
                                                <input type="text" name="training_ti_end_date" autocomplete="off" class="form-control end-date" placeholder="enter end date">
                                                <button type="button" class="btn btn-default btn-icon">
                                                    <i class="mdi mdi-calendar"></i>
                                                </button>
                                            </div>
                                        </div>
                                        @if ($errors->has('training_ti_start_date') || $errors->has('training_ti_end_date'))
                                            <div class="validation-msg">
                                                Date is Required
                                            </div>
                                        @endif

                                    </div>
                                </div>
                                <div class="field-right d-none d-xl-block">
                                    <figure>
                                        <img src="{{ url('images/media05.png') }}" alt="ooredoo" class="img-fluid">
                                    </figure>
                                </div>
                            </div>

                            <div class="bottom-step">
                            	<button type="button" class="btn btn-default" onclick="location.href='{{ url('trainee/request') }}'"><i class="icon-close"></i> Cancel</button>
                            	<div class="steps-count">
                            		<div class="step-item active">
                            			<span class="circle-number">
                            				<em>1</em>
                            				<i class="icon-verified"></i>
                            			</span>
                            			Training Information
                            		</div>
                            		<div class="step-item">
                            			<span class="circle-number">
                            				<em>2</em>
                            				<i class="icon-verified"></i>
                            			</span>
                            			Training Objective
                            		</div>

                            		<div class="step-item">
                            			<span class="circle-number">
                            				<em>3</em>
                            				<i class="icon-verified"></i>
                            			</span>
                            			Completed
                            		</div>

                            		<div class="step-item counter">
                            			<p class="mb-0 text-white">Step <span class="text-white">1</span> / 3</p>
                            		</div>
                            	</div>
                            	<button type="submit" class="btn btn-default btn_block" >Objective <i class="icon-next"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    {{-- </div> --}}

    @include('modal-type-programs')
    <br><br><br>

@endsection
@section('styles')
<style>
	.bottom-nav {
		display: none!important
	}
</style>
@endsection
@section('scripts')
    <script>
        $('[name="training_ti_type_programs"]').on('click',function () {
            let external_trainer = $('.external-trainer'),
                internal_trainer = $('.internal-trainer');

            if($(this).val() == 'internal trainer' || $(this).val() == 'moderator') {
                external_trainer.addClass('d-none');
                external_trainer.find('input').attr('disabled','disabled');

                internal_trainer.removeClass('d-none');
                internal_trainer.find('input').removeAttr('disabled');
            } else {

                internal_trainer.addClass('d-none');
                internal_trainer.find('input').attr('disabled','disabled');

                external_trainer.removeClass('d-none');
                external_trainer.find('input').removeAttr('disabled');
            }
        })
    </script>
@endsection
