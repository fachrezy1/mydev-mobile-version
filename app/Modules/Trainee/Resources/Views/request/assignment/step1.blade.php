@extends('core.trainee.main')

@section('head')
    <link rel="stylesheet" href="{{url('css/general.css')}}">
    <link rel="stylesheet" href="{{url('css/trainee/app.css')}}">
    <link rel="stylesheet" href="{{url('css/formwizard/gsdk-bootstrap-wizard.css')}}">
    <link rel="stylesheet" href="{{url('css/formwizard/demo.css')}}">
@endsection
@section('content')
    @include('core.user.header')
    <body class="coachee">

    <div class="main-site">
        <main class="main-content">
            <div class="main-dashboard">
                <div class="dashboard-steps">
                    <div class="dashboard-entry">
                        <div class="container-fluid">

                            <div class="form-section">

                                <div class="form-title">
                                    <h1>Assignee Information</h1>
                                    <p>Enter an employee who will including </p>
                                </div>

                                <div class="split-field small">
                                    <div class="field-left">
                                        <figure>
                                            <img src="{{url('/images/vector1.png')}}" alt="ooredoo" class="img-fluid">
                                        </figure>
                                    </div>

                                    <div class="field-right">
                                        <form id="assignment_submit" action="{{route('trainee.request.submitStep')}}" method="post">
                                            @csrf
                                            <input type="hidden" name="form_group_id" value="Assignment">
                                            <div class="form-group reset">
                                                <label class="control-label">
                                                    Employee name
                                                </label>
                                                <input type="text" class="form-control" name="assignment_ai_employee_name" placeholder="enter name">
                                            </div>
                                            <div class="form-group reset">
                                                <label class="control-label">
                                                    Employee NIK
                                                </label>
                                                <input type="text" class="form-control" name="assignment_ai_employee_nik" placeholder="example: IND-010119">
                                            </div>
                                            <div class="form-group reset">
                                                <label class="control-label">
                                                    Line Manager name
                                                </label>
                                                <input type="text" class="form-control" name="assignment_ai_line_manager_name" placeholder="enter name">
                                            </div>
                                            <div class="form-group reset">
                                                <label class="control-label">
                                                    Line Manager NIK
                                                </label>
                                                <input type="text" class="form-control" name="assignment_ai_line_manager_nik" placeholder="example: IND-010119">
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="bottom-form right">
                                    <button form="assignment_submit" type="submit" class="btn btn-default">Development Goal <i class="mdi mdi-chevron-right"></i></button>
                                </div>

                            </div>

                        </div>



                    </div>
                </div>

            </div>

        </main>

    </div>

    <div class="modal fade" id="coachGoal" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Coach goal</h3>
                    <button type="button" class="btn btn-close" data-dismiss="modal">
                        <i class="icon-close"></i>
                </div>
                <div class="modal-body has-scroll">
                    <div class="scroll-content">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dapibus, sapien eu convallis imperdiet, purus nulla ullamcorper dolor, et rhoncus lacus lorem non tellus. Nunc luctus neque eu lorem rhoncus egestas. Donec auctor ex dolor, in consectetur turpis laoreet sit amet. Phasellus eu tortor sem. Phasellus lectus nisl, varius vitae massa in, lobortis dignissim tellus. Quisque malesuada risus eget ex gravida fringilla. Fusce tincidunt sagittis arcu, id sodales augue dictum eget. Pellentesque cursus ex metus, et semper lectus laoreet ut. Aliquam erat volutpat. Aenean venenatis leo nisl, eu tincidunt elit ultricies eget. Fusce cursus luctus sem vitae faucibus. In euismod diam in aliquet porttitor. Vestibulum viverra, ex sit amet egestas feugiat, odio velit scelerisque elit, et feugiat metus felis a ante.

                        Cras neque enim, facilisis et pulvinar quis, tempus non eros. Ut lacus ante, blandit et hendrerit sit amet, semper eu dui. Ut sit amet mauris malesuada, euismod urna vitae, euismod massa. Sed rhoncus neque a massa eleifend tincidunt. Sed sodales eros vitae malesuada lobortis. Etiam at sapien pharetra, pulvinar augue sit amet, congue erat. Aenean laoreet est purus, in dapibus massa posuere eu. Aliquam vehicula nibh aliquam tellus sollicitudin, id euismod nisi sollicitudin. Etiam egestas felis quis lectus finibus, in blandit augue porttitor. Vivamus vehicula vehicula aliquam. Curabitur enim lacus, viverra tincidunt ligula sed, accumsan aliquam augue. Aliquam vitae tortor egestas, mollis velit vel, placerat felis. Curabitur suscipit auctor erat, ac porta massa molestie ut. Suspendisse dui velit, maximus a dignissim id, sagittis at felis. Integer maximus odio id ultricies auctor. Nullam ac purus mi.
                        Cras neque enim, facilisis et pulvinar quis, tempus non eros. Ut lacus ante, blandit et hendrerit sit amet, semper eu dui. Ut sit amet mauris malesuada, euismod urna vitae, euismod massa. Sed rhoncus neque a massa eleifend tincidunt. Sed sodales eros vitae malesuada lobortis. Etiam at sapien pharetra, pulvinar augue sit amet, congue erat. Aenean laoreet est purus, in dapibus massa posuere eu. Aliquam vehicula nibh aliquam tellus sollicitudin, id euismod nisi sollicitudin. Etiam egestas felis quis lectus finibus, in blandit augue porttitor. Vivamus vehicula vehicula aliquam. Curabitur enim lacus, viverra tincidunt ligula sed, accumsan aliquam augue. Aliquam vitae tortor egestas, mollis velit vel, placerat felis. Curabitur suscipit auctor erat, ac porta massa molestie ut. Suspendisse dui velit, maximus a dignissim id, sagittis at felis. Integer maximus odio id ultricies auctor. Nullam ac purus mi.
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="coachDetail" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Coache Form Detail</h3>
                    <button type="button" class="btn btn-close" data-dismiss="modal">
                        <i class="icon-close"></i>
                    </button>
                </div>
                <div class="modal-body has-scroll">
                    <div class="scroll-content">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dapibus, sapien eu convallis imperdiet, purus nulla ullamcorper dolor, et rhoncus lacus lorem non tellus. Nunc luctus neque eu lorem rhoncus egestas. Donec auctor ex dolor, in consectetur turpis laoreet sit amet. Phasellus eu tortor sem. Phasellus lectus nisl, varius vitae massa in, lobortis dignissim tellus. Quisque malesuada risus eget ex gravida fringilla. Fusce tincidunt sagittis arcu, id sodales augue dictum eget. Pellentesque cursus ex metus, et semper lectus laoreet ut. Aliquam erat volutpat. Aenean venenatis leo nisl, eu tincidunt elit ultricies eget. Fusce cursus luctus sem vitae faucibus. In euismod diam in aliquet porttitor. Vestibulum viverra, ex sit amet egestas feugiat, odio velit scelerisque elit, et feugiat metus felis a ante.

                        Cras neque enim, facilisis et pulvinar quis, tempus non eros. Ut lacus ante, blandit et hendrerit sit amet, semper eu dui. Ut sit amet mauris malesuada, euismod urna vitae, euismod massa. Sed rhoncus neque a massa eleifend tincidunt. Sed sodales eros vitae malesuada lobortis. Etiam at sapien pharetra, pulvinar augue sit amet, congue erat. Aenean laoreet est purus, in dapibus massa posuere eu. Aliquam vehicula nibh aliquam tellus sollicitudin, id euismod nisi sollicitudin. Etiam egestas felis quis lectus finibus, in blandit augue porttitor. Vivamus vehicula vehicula aliquam. Curabitur enim lacus, viverra tincidunt ligula sed, accumsan aliquam augue. Aliquam vitae tortor egestas, mollis velit vel, placerat felis. Curabitur suscipit auctor erat, ac porta massa molestie ut. Suspendisse dui velit, maximus a dignissim id, sagittis at felis. Integer maximus odio id ultricies auctor. Nullam ac purus mi.
                        Cras neque enim, facilisis et pulvinar quis, tempus non eros. Ut lacus ante, blandit et hendrerit sit amet, semper eu dui. Ut sit amet mauris malesuada, euismod urna vitae, euismod massa. Sed rhoncus neque a massa eleifend tincidunt. Sed sodales eros vitae malesuada lobortis. Etiam at sapien pharetra, pulvinar augue sit amet, congue erat. Aenean laoreet est purus, in dapibus massa posuere eu. Aliquam vehicula nibh aliquam tellus sollicitudin, id euismod nisi sollicitudin. Etiam egestas felis quis lectus finibus, in blandit augue porttitor. Vivamus vehicula vehicula aliquam. Curabitur enim lacus, viverra tincidunt ligula sed, accumsan aliquam augue. Aliquam vitae tortor egestas, mollis velit vel, placerat felis. Curabitur suscipit auctor erat, ac porta massa molestie ut. Suspendisse dui velit, maximus a dignissim id, sagittis at felis. Integer maximus odio id ultricies auctor. Nullam ac purus mi.
                    </div>
                </div>

            </div>
        </div>
    </div>



    </body>
@endsection
@section('js')
    <script src="{{url('js/general.js')}}"></script>
    <script src="{{url('js/trainee/app.js')}}"></script>
@endsection
