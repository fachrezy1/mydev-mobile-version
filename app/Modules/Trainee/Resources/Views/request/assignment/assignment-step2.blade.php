@php
    $user = \Sentinel::check();
    $helper = new \App\Helpers\UserRolesHelper;
@endphp
@extends('core.trainee.main')


@section('content')

    <main class="main-content secondary-nav">
        <div class="dashboard-steps">
            @include('trainee::request.header')

            <div class="dashboard-entry">
                <div class="container-fluid">
                    <div class="form-section">
                        <div class="form-title">
                            <h1>Thank You</h1>
                            <p>Your Request Has been submited, please wait for the approval</p>
                        </div>

                        <div class="center-form">
                            <figure>
                                <img src="{{ url('images/media02.png') }}" alt="" class="img-fluid for-media">
                            </figure>
                        </div>
                        <div class="bottom-step">
                        	<div class="steps-count">
                        		<div class="step-item done">
                        			<span class="circle-number">
                        				<em>1</em>
                        				<i class="icon-verified"></i>
                        			</span>
                        			My Information
                        		</div>

                        		<div class="step-item done">
                        			<span class="circle-number">
                        				<em>2</em>
                        				<i class="icon-verified"></i>
                        			</span>
                        			Completed
                        		</div>

                        		<div class="step-item counter mx-auto mr-md-0">
                        			<p class="mb-0 text-white">Step <span class="text-white">2</span> / 2</p>
                        		</div>
                        	</div>
                        	@if($helper->inAdmin())
                        	<button type="button" class="btn btn-default btn-red-default mx-auto mr-md-0 text-center" onclick="location.href='{{ url('dashboard') }}'">Dashboard<i class="icon-next"></i></button>
                            @else
                            <button type="button" class="btn btn-default btn-red-default mx-auto mr-md-0 text-center" onclick="location.href='{{ url('trainee/dashboard') }}'">Dashboard<i class="icon-next"></i></button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection
