@php
    $user = \Sentinel::check();
    $helper = new \App\Helpers\UserRolesHelper;
@endphp

@extends('core.trainee.main')

@section('content')
    <main class="main-content secondary-nav">
        <div class="dashboard-steps pb-lg-5">
            @include('trainee::request.header')
            <div class="dashboard-entry">
                <div class="container-fluid">
                    <div class="form-section">
                        <form id="submit_step" action="{{route('trainee.request.submitStep')}}" method="post">
                            @csrf
                            <input type="hidden" name="form_group_id" value="{{ $formData['type'] }}">
                            <input type="hidden" name="redirect" value="{{ url('trainee/request/'.$redirect.'?step=2') }}">
                            <input type="hidden" name="is_last" value="true">
                            <input name="user_role" type="hidden" value="Project Leader" readonly required>
                            <input name="user_nik" type="hidden" class="nik_coachee" value="project_leader" readonly required>

                            <div class="form-title m-24">
                                <h1>Assignment Information</h1>
                            </div>

                            <div class="split-field medium">
                                <div class="field-right">
                                    
                                    @if ($helper->inAdmin())
                                    <div class="form-group reset">
                                        <label class="control-label">Employee Name</label>
                                        <select class="form-control select-employee2">

                                        </select>
                                        <input name="assignment_ci_employee_name" type="hidden" class="form-control select-employee-name2" placeholder="enter name"  required>
                                        @if ($errors->has('assignment_ci_employee_name'))
                                            <div class="validation-msg">
                                                My Name Name is Required
                                            </div>
                                        @endif
                                    </div>
                                    @else
                                    <div class="form-group reset">
                                        <label class="control-label">My Name</label>
                                        <input type="text" name="assignment_ci_employee_name" class="form-control nik_coachee2" placeholder="enter name" value="{{$employee['staff'] ? $employee['staff']['full_name'] : ''}}" readonly required>
                                    </div>
                                    @endif

                                    <div class="form-group reset">
                                        
                                        @if ($helper->inAdmin())
                                            <label class="control-label">Employee ID</label>
                                        @else
                                            <label class="control-label">My Employee ID</label>
                                        @endif
                                        <input name="assignment_ci_employee_nik" type="text" class="form-control nik_coachee2" placeholder="example: IND-010119" value="{{$employee['staff'] ? $employee['staff']['nik'] : ''}}" readonly required>
                                    </div>

                                    <div class="form-group reset">
                                        <label class="control-label">
                                            Development Type
                                        </label>
                                        <div class="check-group flex-column">
                                        	<div class="form-check">
                                        		<label class="form-check-label" >
                                        			<input class="form-check-input" title="Task that you are given to do, especially as part of job enlargement or enrichment program" data-toggle="tooltip" data-placement="top"  name="assignment_dev_type" type="radio" value="project" checked="" required> Project
                                        			<span class="form-check-sign">
                                        				<span class="check"></span>
                                        			</span>
                                        		</label>
                                        	</div>
                                        	<div class="form-check">
                                        		<label class="form-check-label">
                                        			<input class="form-check-input" title="Temporary position with an emphasis on on-the-job training" data-toggle="tooltip" data-placement="top" name="assignment_dev_type" type="radio" value="internship" required> Cross Functional Assignment
                                        			<span class="form-check-sign">
                                        				<span class="check"></span>
                                        			</span>
                                        		</label>
                                        	</div>
                                        	<div class="form-check">
                                        		<label class="form-check-label">
                                        			<input class="form-check-input" title="Temporary position with an emphasis on on-the-job training all around the world" data-toggle="tooltip" data-placement="top" name="assignment_dev_type" type="radio" value="globaltalent" required> Global Talent Mobility / National Talent Mobility
                                        			<span class="form-check-sign">
                                        				<span class="check"></span>
                                        			</span>
                                        		</label>
                                        	</div>
                                        </div>
                                    </div>

                                    <div class="form-group reset">
                                        <label class="control-label">
                                            <button type="button" data-toggle="modal" class="btn-question" data-target="#competencyModal">
                                                <i class="icon-questions-circular-button" >  </i>
                                            </button>
                                            Competency to be Developed
                                        </label>
                                        <div class="form-field mb-3">
                                            <select id="competency_select" class="form-control select2-comp" name="assignment_obj_employee_competency[]" multiple="multiple">
                                                @if (count($competencies)>0)
                                                    @foreach($competencies as $key => $c)
                                                        <optgroup label="{{ $c['option_group'] }}">
                                                            @foreach($c['option_relation'] as $opt_sub)
                                                                <option>
                                                                    {{ $opt_sub['name'] }}
                                                                </option>
                                                            @endforeach
                                                        </optgroup>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="project-group">
                                        <div class="form-group reset ">
                                            <label class="control-label">My future project leader</label>
                                            <select class="form-control select-employee">

                                            </select>
                                            <input name="assignment_pg_project_leader" type="hidden" class="form-control select-employee-name">
                                            <input name="assignment_pg_project_leader_nik" type="hidden" id="nik_coachee" class="form-control nik_coachee" readonly>
                                            @if ($errors->has('assignment_pg_project_leader') || $errors->has('assignment_pg_project_leader'))
                                                <div class="validation-msg">
                                                    My future project leader is Required
                                                </div>
                                            @endif
                                        </div>

                                        <div class="form-group reset">
                                            <label class="control-label">My future project title</label>
                                            <input name="assignment_pg_project_title" type="text" class="form-control">
                                            @if ($errors->has('assignment_pg_project_title'))
                                                <div class="validation-msg">
                                                    My future project title is Required
                                                </div>
                                            @endif
                                        </div>

                                        <div class="form-group reset">
                                            <label class="control-label">My future project period</label>
                                            <div class="field-date">
                                                <div class="date-input">
                                                    <input name="assignment_pg_start_date" type="text" autocomplete="off" class="form-control start-date" placeholder="Start date">
                                                    <button type="button" class="btn btn-default btn-icon "><i class="mdi mdi-calendar"></i></button>
                                                </div>

                                                To

                                                <div class="date-input">
                                                    <input name="assignment_pg_end_date" type="text" autocomplete="off" class="form-control end-date" placeholder="End date">
                                                    <button type="button" class="btn btn-default btn-icon"><i class="mdi mdi-calendar"></i></button>
                                                </div>
                                            </div>
                                            @if ($errors->has('assignment_pg_start_date') || $errors->has('assignment_pg_end_date'))
                                                <div class="validation-msg">
                                                    My future project period is Required
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group reset">
                                            <label class="control-label">
                                                My future project description
                                            </label>
                                            <textarea rows="6" name="assignment_pg_project_description" class="form-control" maxlength="255" placeholder="Enter Project Description"></textarea>
                                            @if ($errors->has('assignment_pg_project_description'))
                                                <div class="validation-msg">
                                                    My future project description is Required
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group reset">
                                            <label class="control-label">
                                                What do i want to develop by joining this project
                                            </label>
                                            <textarea rows="6" name="assignment_pg_development_goal" class="form-control" maxlength="255"></textarea>
                                            @if ($errors->has('assignment_pg_development_goal'))
                                                <div class="validation-msg">
                                                    What do i want to develop by joining this project is Required
                                                </div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="internship_talent_group_date d-none">
                                        <div class="form-group reset ">
                                            <label class="control-label">My future group</label>
                                            <input id="assignment_inter_group" name="assignment_inter_group" type="text" class="form-control">
                                            @if ($errors->has('assignment_inter_group'))
                                                <div class="validation-msg">
                                                    My future group is Required
                                                </div>
                                            @endif
                                        </div>

                                        <div class="form-group reset">
                                            <label class="control-label">My future division</label>
                                            <input id="assignment_inter_division" name="assignment_inter_division" type="text" class="form-control">
                                            @if ($errors->has('assignment_inter_division'))
                                                <div class="validation-msg">
                                                    My future division is Required
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group reset">
                                            <label class="control-label">My future internship date</label>
                                            <div class="field-date">
                                                <div class="date-input">
                                                    <input id="assignment_inter_start_date" name="assignment_inter_start_date" type="text" autocomplete="off" class="form-control start-date2" placeholder="Start date">
                                                    <button type="button" class="btn btn-default btn-icon "><i class="mdi mdi-calendar"></i></button>
                                                </div>

                                                To

                                                <div class="date-input">
                                                    <input id="assignment_inter_end_date" name="assignment_inter_end_date" type="text" autocomplete="off" class="form-control end-date2" placeholder="End date">
                                                    <button type="button" class="btn btn-default btn-icon"><i class="mdi mdi-calendar"></i></button>
                                                </div>
                                            </div>
                                            @if ($errors->has('assignment_inter_start_date') || $errors->has('assignment_inter_end_date'))
                                                <div class="validation-msg">
                                                    My future internship date is Required
                                                </div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="internship d-none">
                                        <div class="form-group reset">
                                            <label class="control-label">
                                                My future mentor’s name
                                            </label>
                                            <select class="form-control select-employee">

                                            </select>
                                            <input name="assignment_inter_mentor_name" type="hidden" class="form-control select-employee-name">
                                            @if ($errors->has('assignment_inter_mentor_name'))
                                                <div class="validation-msg">
                                                    My future mentor’s name is Required
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group reset">
                                            <label class="control-label">
                                                My future mentor ID
                                            </label>
                                            <input name="assignment_inter_mentor_nik" type="text" id="nik_coachee" class="form-control nik_coachee" readonly>
                                        </div>
                                        <div class="form-group reset">
                                            <label class="control-label">
                                                What do i want to develop by joining this project
                                            </label>
                                            <textarea rows="6" name="assignment_inter_development_goal" class="form-control" maxlength="255"></textarea>
                                            @if ($errors->has('assignment_inter_development_goal'))
                                                <div class="validation-msg">
                                                    What do i want to develop by joining this project is Required
                                                </div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="globaltalent d-none">
                                        <div class="form-group reset">
                                            <label class="control-label">
                                                Directorat/ Group/ OPCO Designation
                                            </label>
                                            <input name="assignment_gt_opco" type="text" class="form-control">
                                            @if ($errors->has('assignment_gt_opco'))
                                                <div class="validation-msg">
                                                    Directorat/ Group/ OPCO Designation is Required
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group reset">
                                            <label class="control-label">
                                                Assignment Type
                                            </label>
                                            <div class="check-group flex-column">
                                            	<div class="form-check">
	                                            	<input name="assignment_gt_type" id="shortterm_opt" data-val="shortterm" type="radio" value="shortterm"><label for="shortterm_opt" class="form-check-label pl-1"> Short Term (3-12 Months)</label>&nbsp;
                                            	</div>
                                            	<div class="form-check">
		                                            <input name="assignment_gt_type" id="longterm_opt" data-val="longterm" type="radio" value="longterm"><label for="longterm_opt" class="form-check-label pl-1"> Long Term (1-2 Years)</label>
		                                          </div>
	                                          </div>
                                        </div>
                                         <div class="form-group reset">
                                            <label class="control-label">Assignment Duration</label>
                                            <div class="field-date">
                                                <div class="date-input">
                                                    <input name="assignment_gt_start_date" type="text" autocomplete="off" class="form-control start-date3" placeholder="Start date">
                                                    <button type="button" class="btn btn-default btn-icon "><i class="mdi mdi-calendar"></i></button>
                                                </div>

                                                To

                                                <div class="date-input">
                                                    <input name="assignment_gt_end_date" type="text" autocomplete="off" class="form-control end-date3" placeholder="End date">
                                                    <button type="button" class="btn btn-default btn-icon"><i class="mdi mdi-calendar"></i></button>
                                                </div>
                                            </div>
                                            @if ($errors->has('assignment_gt_start_date') || $errors->has('assignment_gt_end_date'))
                                                <div class="validation-msg">
                                                    Date is Required
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group reset">
                                            <label class="control-label">
                                                Assignment Purpose
                                            </label>
                                            <div class="check-group flex-column">
	                                            <div class="form-check">
	                                            	<input name="assignment_gt_purpose" id="assignment_gt_purpose_dev" data-val="Development" type="radio" value="Development" checked><label for="assignment_gt_purpose_dev" class="form-check-label pl-1"> Development</label>&nbsp;
	                                            </div>
	                                            <div class="form-check">
	                                            	<input name="assignment_gt_purpose" id="assignment_gt_purpose_expert" data-val="Expert Skill Gap" type="radio" value="Expert Skill Gap"><label for="assignment_gt_purpose_expert" class="form-check-label pl-1"> Expert Skill Gap</label>
	                                            </div>
	                                          </div>
                                        </div>
                                        <div class="form-group reset">
                                            <label class="control-label">
                                                Reason For Mobility Request
                                            </label>
                                            <div id="Development_opt">
                                            	<div class="check-group flex-column">
                                            		<div class="form-check">
                                            			<label class="form-check-label" >
                                            				<input class="form-check-input" name="assignment_gt_reason[]" type="checkbox" value="Develop National Talent"> Develop National Talent
                                            				<span class="form-check-sign">
                                            					<span class="check"></span>
                                            				</span>
                                            			</label>
                                            		</div>
                                            		<div class="form-check">
                                            			<label class="form-check-label" >
                                            				<input class="form-check-input" name="assignment_gt_reason[]" type="checkbox" value="Exposure to New Technology"> Exposure to New Technology
                                            				<span class="form-check-sign">
                                            					<span class="check"></span>
                                            				</span>
                                            			</label>
                                            		</div>
                                            		<div class="form-check">
                                            			<label class="form-check-label" >
                                            				<input class="form-check-input" name="assignment_gt_reason[]" type="checkbox" value="Develop Management Skills"> Develop Management Skills
                                            				<span class="form-check-sign">
                                            					<span class="check"></span>
                                            				</span>
                                            			</label>
                                            		</div>
                                            		<div class="form-check">
                                            			<label class="form-check-label" >
                                            				<input class="form-check-input" name="assignment_gt_reason[]" type="checkbox" value="Acquire New Skill Set"> Acquire New Skill Set
                                            				<span class="form-check-sign">
                                            					<span class="check"></span>
                                            				</span>
                                            			</label>
                                            		</div>
                                            		<div class="form-check">
                                            			<label class="form-check-label" >
                                            				<input class="form-check-input" id="ag_gt_other" type="checkbox" value="Other"> Other
                                            				<span class="form-check-sign">
                                            					<span class="check"></span>
                                            				</span>
                                            			</label>
                                            		</div>
                                            	</div>
                                            </div>
                                            <div id="skil_gap_opt">
                                            	<div class="check-group flex-column">
                                            		<div class="form-check">
                                            			<label class="form-check-label" >
                                            				<input class="form-check-input" name="assignment_gt_reason[]" type="checkbox" value="Business Expansion (New Role)"> Business Expansion (New Role)
                                            				<span class="form-check-sign">
                                            					<span class="check"></span>
                                            				</span>
                                            			</label>
                                            		</div>
                                            		<div class="form-check">
                                            			<label class="form-check-label" >
                                            				<input class="form-check-input" name="assignment_gt_reason[]" type="checkbox" value="Replacing Existing Role (Vacancy)"> Replacing Existing Role (Vacancy)
                                            				<span class="form-check-sign">
                                            					<span class="check"></span>
                                            				</span>
                                            			</label>
                                            		</div>
                                            		<div class="form-check">
                                            			<label class="form-check-label" >
                                            				<input class="form-check-input" name="assignment_gt_reason[]" type="checkbox" value="Project Base (New Product Launch, Network Roll Out)"> Project Base (New Product Launch, Network Roll Out)
                                            				<span class="form-check-sign">
                                            					<span class="check"></span>
                                            				</span>
                                            			</label>
                                            		</div>
                                            		<div class="form-check">
                                            			<label class="form-check-label" >
                                            				<input class="form-check-input" name="assignment_gt_reason[]" type="checkbox" value="Leadership Development/ Succession Program"> Leadership Development/ Succession Program
                                            				<span class="form-check-sign">
                                            					<span class="check"></span>
                                            				</span>
                                            			</label>
                                            		</div>
                                            		<div class="form-check">
                                            			<label class="form-check-label" >
                                            				<input class="form-check-input" name="assignment_gt_reason[]" type="checkbox" value="Strategic Resource Gap"> Strategic Resource Gap
                                            				<span class="form-check-sign">
                                            					<span class="check"></span>
                                            				</span>
                                            			</label>
                                            		</div>
                                            		<div class="form-check">
                                            			<label class="form-check-label" >
                                            				<input class="form-check-input" id="ag_gt_other" type="checkbox" value="Other"> Other
                                            				<span class="form-check-sign">
                                            					<span class="check"></span>
                                            				</span>
                                            			</label>
                                            		</div>
                                            	</div>
                                            </div>
                                            <div id="ag_gt_other_container">
                                                <input type="text" name="assignment_gt_reason_other" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group reset">
                                            <div class="form-field">
                                                <input id="gtmattachment" type="file" accept=".rar,.zip" name="assigment_gt_attachment" class="btn btn-default reset">
                                                <button type="button" class="btn btn-default reset">
                                                    <label for="gtmattachment" style="margin-bottom: 0;width: 100%; padding: 0;"><i class="icon-contact"></i> Documents</label>
                                                </button><br>
                                                @if($errors->has('assigment_gt_attachment'))
                                                    <div class="text-danger">Please upload your documents in rar or zip format</div>
                                                @endif
                                                <small class="name-files">Documents Required (Please upload your documents in rar or zip format)
                                                <ol style="font-size: 90%;padding-left: 10px;">
                                                    <li>Assignee CV + Job Description
                                                    <li>Assignee Personal Details Form</li>
                                                    <li>Assignee Role Profile (Expert of Strategic)</li>
                                                    <li>Assignment KPI Form</li>
                                                </ol></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="field-left d-none d-xl-block" id="fl-lef">
                                    <figure>
                                        <img src="{{ url('images/vector1.png') }}" alt="ooredoo" class="img-fluid">
                                    </figure>
                                </div>
                            </div>
                            <div class="bottom-step">
                                <button type="button" class="btn btn-default" onclick="location.href='{{ url('trainee/request') }}'"><i class="icon-close"></i> Cancel</button>
                                <div class="steps-count">
                                	<div class="step-item active">
                                		<span class="circle-number">
                                			<em>1</em>
                                			<i class="icon-verified"></i>
                                		</span>
                                		My Information
                                	</div>
                                	<div class="step-item">
                                		<span class="circle-number">
                                			<em>2</em>
                                			<i class="icon-verified"></i>
                                		</span>
                                		Completed
                                	</div>
                                	<div class="step-item counter">
	                            			<p class="mb-0 text-white">Step <span class="text-white">1</span> / 2</p>
	                            		</div>
                                </div>
                                <button form="submit_step" type="submit" class="btn btn-default btn_block" >Submit <i class="icon-next"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <br><br><br>

    @include('modal-competency')

@endsection
@section('scripts')
    <script>
        function format (d) {
            content = [];
            for (i=0;i<d.length;i++){
                content.push('<tr><td>'+d[i].name+'</td>'+
                '<td>'+d[i].definition+'</td>'+
                '</tr>');
            }
            tableCompetenciesRet = '<table class="table-bordered">'+
                        content.toString().replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '') +
                    '</table>';
            return tableCompetenciesRet;
            ;
        }
        var datatableComp = $('#tableCompetencies').DataTable( {
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {
                url: "{{ route('trainee.dtCompetencies') }}",
            },
            order: [ 1, 'asc' ],
            columns: [
                { data: 'category' },
                { data: 'competency', render: function ( data ) {
                    return format(data);
                }}
            ],
        });
        function resetGroupVal(){
            $("#assignment_inter_start_date").val("");
            $("#assignment_inter_end_date").val("");
            $("#assignment_inter_group").val("");
            $("#assignment_inter_division").val("");
        }

        $('[name=assignment_dev_type]').on('click',function () {
            if($(this).val() == 'project') {
                resetGroupVal();
                $(".internship_talent_group_date").addClass("d-none");
                $('[name=user_role]').val('Project Leader');
                $('.select-employee').val(null);
                $('.select2-selection_rendered').html('');
                $('.project-group')
                    .find('select')
                    .attr('required','required')
                $('.project-group')
                    .removeClass('d-none')
                    .find('input')
                        .val('')
                        .attr('required','required')
                    .find('select')
                        .attr('required','required')
                    .find('option:selected')
                    .removeAttr('selected')
                    .find('textarea')
                        .val('')
                        .attr('required','required')
                    .find('.select2-selection__rendered')
                    .html('');
                $('.project-group').find('textarea').val('').attr('required','required');


                $('.internship')
                    .find('select')
                    .removeAttr('required','required');
                $('.internship')
                    .addClass('d-none')
                    .find('input')
                        .val('')
                        .removeAttr('required','required')
                    .find('option:selected')
                    .removeAttr('selected')
                    .find('textarea')
                        .val('')
                        .removeAttr('required','required')
                    .find('.select2-selection__rendered')
                    .html('');
                $('.internship').find('textarea').val('').removeAttr('required','required');


                $('.globaltalent')
                    .find('select')
                    .removeAttr('required','required');
                $('.globaltalent')
                    .addClass('d-none')
                    .find('input')
                        .val('')
                        .removeAttr('required','required')
                    .find('option:selected')
                    .removeAttr('selected')
                    .find('textarea')
                        .val('')
                        .removeAttr('required','required')
                    .find('.select2-selection__rendered')
                    .html('');
                $('.globaltalent').find('textarea').val('').removeAttr('required','required');
            } else if($(this).val() == 'internship') {
                resetGroupVal();
                $(".internship_talent_group_date").removeClass("d-none");
                $('[name=user_role]').val('Mentor');
                $('.select-employee').val(null);
                $('.select2-selection_rendered').html('');
                $('.internship')
                    .find('select')
                $('.internship')
                    .removeClass('d-none')
                    .find('input')
                    .val('')
                    .find('select')
                        .find('option:selected')
                        .removeAttr('selected')
                    .find('textarea')
                        .val('')
                    .find('.select2-selection__rendered')
                    .html('');
                $('.internship').find('textarea').val('');


                $('.project-group')
                    .find('select')
                    .removeAttr('required','required');
                $('.project-group')
                    .addClass('d-none')
                    .find('input')
                    .val('')
                    .removeAttr('required','required')
                    .find('select')
                        .find('option:selected')
                        .removeAttr('selected')
                    .find('textarea')
                    .val('')
                    .removeAttr('required','required')
                    .find('.select2-selection__rendered')
                    .html('');
                $('.project-group').find('textarea').val('').removeAttr('required','required');


                $('.globaltalent')
                    .find('select')
                    .removeAttr('required','required');
                $('.globaltalent')
                    .addClass('d-none')
                    .find('input')
                        .val('')
                        .removeAttr('required','required')
                    .find('option:selected')
                    .removeAttr('selected')
                    .find('textarea')
                        .val('')
                        .removeAttr('required','required')
                    .find('.select2-selection__rendered')
                    .html('');
                $('.globaltalent').find('textarea').val('').removeAttr('required','required');
            }else{
                resetGroupVal();
                $(".internship_talent_group_date").addClass("d-none");
                $('[name=user_role]').val('Mentor');
                $('.select-employee').val(null);
                $('.select2-selection_rendered').html('');
                $('.globaltalent')
                    .find('select').attr('required','required');
                $('.globaltalent')
                    .find('input[type="text"]').attr('required','required');
                $('.globaltalent')
                    .find('input[type="textarea"]').attr('required','required');
                $('.globaltalent')
                    .find('input[type="radio"]').attr('required','required');
                //$('.globaltalent')
                  //  .find('input[type="file"]').attr('required','required');
                //$("#gtmattachment").attr('required','required');
                $('.globaltalent')
                    .removeClass('d-none');

                $("input[name='assignment_gt_reason_other']").removeAttr("required");
                $('.project-group')
                    .find('select')
                    .removeAttr('required','required');
                $('.project-group')
                    .find('input')
                    .removeAttr('required','required');
                $('.project-group')
                    .addClass('d-none');

                $('.internship')
                    .find('select')
                    .removeAttr('required','required');
                $('.internship')
                    .addClass('d-none');
            }
            $("#competency_select").val("");
            $("#skil_gap_opt").hide();
            $(document).on("click","input[name='assignment_gt_purpose']",function(){
                purpose = $(this).data("val");
                console.log(purpose);
                if (purpose=="Development"){
                    $("#Development_opt").show();
                    $("#skil_gap_opt").hide();
                } else {
                    $("#Development_opt").hide();
                    $("#skil_gap_opt").show();
                }
            });
            
            $("#longterm_opt").on("click",function(){
                $("#assignment_inter_division_gt").val("longterm");
            });

            $("#shortterm_opt").on("click",function(){
                $("#assignment_inter_division_gt").val("shortterm");
            });

            $("#ag_gt_other_container").hide();

            $(document).on("click","#ag_gt_other",function(){
                if ($(this).prop("checked")) {
                    $("#ag_gt_other_container").show();
                    $("input[name='assignment_gt_reason_other']").attr("required","required");
                } else {
                    $("#ag_gt_other_container").hide();
                    $("input[name='assignment_gt_reason_other']").removeAttr("required");
                }
            });

        })
    </script>
<!-- new -->
    <script>
    var delayInMilliseconds = 3000; //1 second

    setTimeout(function() {
        var a = document.getElementById('fl-lef');
        a.className += ' class_two';
        a.style.display = 'block';
    
        // a.classList.remove('d-none');
    }, delayInMilliseconds);
    
    </script>
<!-- new -->
@endsection
