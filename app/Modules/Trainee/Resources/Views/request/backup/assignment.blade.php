@extends('core.trainee.main')

@section('head')
    <link rel="stylesheet" href="{{url('css/general.css')}}">
    <link rel="stylesheet" href="{{url('css/trainee/app.css')}}">
    <link rel="stylesheet" href="{{url('css/formwizard/gsdk-bootstrap-wizard.css')}}">
    <link rel="stylesheet" href="{{url('css/formwizard/demo.css')}}">
@endsection
@section('content')
    @include('core.user.header')
    <section class="container-fluid trainee-form-request">
        <div class="row">
            <div class="col-12 trainee-form-request-text text-center">
                <h2> You are requesting a new trainee </h2>
                <h5> Ooredoo here to help you learn, grow, and boost. what you'd like to achieve </h5>
            </div>
            <div class="col-12 trainee-form-request-content">

                <div class="wizard-container">

                    <div class="card wizard-card" data-color="orange" id="wizardProfile">
                        <form action="{{route('trainee.request.submitStep')}}" method="post">
                            @csrf
                            <input type="hidden" name="form_group_id" value="Assignment">
                            <div class="wizard-header">
                            </div>

                            <div class="wizard-navigation">
                                <ul>
                                    <li><a href="#information" data-toggle="tab">2</a></li>
                                    <li><a href="#projectInformation" data-toggle="tab">3</a></li>
                                    <li><a href="#projectDescription" data-toggle="tab">4</a></li>
                                    <li><a href="#sign" data-toggle="tab">5</a></li>
                                </ul>

                            </div>

                            <div class="tab-content">
                                <div class="tab-pane show active" id="information">
                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>Assignment Information <small>(required)</small></label>
                                                <input name="assignment_ai_employee_name" type="text" class="form-control" placeholder="Employee Name">
                                                <input name="assignment_ai_employee_nik" type="text" class="form-control" placeholder="Employee NIK">
                                                <input name="assignment_ai_line_manager_name" type="text" class="form-control" placeholder="Line Manager Name">
                                                <input name="assignment_ai_line_manager_nik" type="text" class="form-control" placeholder="Line Manager NIK">
                                            </div>
                                        </div>
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>Development Goal <small>(required)</small></label>
                                                <textarea name="assignment_development_goal" placeholder="describe Skill/Experience to be developed, e.g exposure to BOD level, bigger project, etc"></textarea>
                                                <small class="text-danger"> </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="projectInformation">
                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>Project Information <small>(required)</small></label>
                                                <input name="assignment_pi_project_leader" type="text" class="form-control" placeholder="Project Leader">
                                                <input name="assignment_pi_division" type="text" class="form-control" placeholder="Division/Group">
                                                <input name="assignment_pi_project_member" type="text" class="form-control" placeholder="Project Member">
                                                <input name="assignment_pi_project_title" type="text" class="form-control" placeholder="Project Title">
                                                <input name="assignment_pi_kriteria_penugasan" type="text" class="form-control" placeholder="Kriteria Penugasan">
                                                <input name="assignment_pi_jangka_waktu" type="text" class="form-control" placeholder="Jangka Waktu">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="projectDescription">
                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>Project Description <small>(required)</small></label>
                                                <textarea name="assignment_project_description"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>Project Background <small>(required)</small></label>
                                                <textarea name="assignment_project_background" placeholder="how the project can support the Development Goal"></textarea>
                                                <small class="text-danger d-none">@lang('general.validation.error')</small>
                                            </div>
                                        </div>
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>Measurable Project Outcomes <small>(required)</small></label>
                                                <input name="assignment_mpo_business" type="text" class="form-control" placeholder="e.g increase revenue 20%">
                                                <input name="assignment_mpo_behavior" type="text" class="form-control" placeholder="e.g demonstrate high quality decision making">
                                            </div>
                                        </div>
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>Project Timeline <small>(required)</small></label>
                                                <textarea name="assignment_project_timeline" placeholder="Start & End dates for every project phases. E.g Planning, Production, Delivery, etc"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>Barriers / Challenges might be encountered <small>(required)</small></label>
                                                <textarea name="assignment_barrier" placeholder=""></textarea>
                                            </div>
                                        </div>
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>Support / resources needed <small>(required)</small></label>
                                                <textarea name="assignment_support" placeholder=""></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="sign">
                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>Sign <small>(required)</small></label>
                                                <div class="col-12">
                                                    <label> Employee </label>
                                                    <input name="assignment_sign_employee_name" type="text" class="form-control" placeholder="Employee Name">
                                                    <input name="assignment_sign_employee_nik" type="text" class="form-control" placeholder="Employee NIK">
                                                    <input name="assignment_sign_employee_signature" type="text" class="form-control" placeholder="Signature">
                                                </div>
                                                <div class="col-12">
                                                    <label> Sponsor 1 (Line Manager) </label>
                                                    <small> Initial Unit </small>
                                                    <input name="assignment_sponsor_1_name" type="text" placeholder="Name">
                                                    <input name="assignment_sponsor_1_nik" type="text" placeholder="NIK">
                                                    <input name="assignment_sponsor_1_signature" type="text" placeholder="Signature">
                                                </div>
                                                <div class="col-12">
                                                    <label> Sponsor 2 (Line Manager) </label>
                                                    <small> Designated Unit </small>
                                                    <input name="assignment_sponsor_2_name" type="text" placeholder="Name">
                                                    <input name="assignment_sponsor_2_nik" type="text" placeholder="NIK">
                                                    <input name="assignment_sponsor_2_signature" type="text" placeholder="Signature">
                                                </div>
                                                <div class="col-12">
                                                    <label> HR Development </label>
                                                    <input name="assignment_hr_dev_name" type="text" placeholder="Name">
                                                    <input name="assignment_hr_dev_nik" type="text" placeholder="NIK">
                                                    <input name="assignment_hr_dev_signature" type="text" placeholder="Signature">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>Notes <small>(required)</small></label>
                                                <textarea name="assignment_notes"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wizard-footer height-wizard">
                                <div class="pull-right">
                                    <input type='button' class='btn btn-next btn-fill btn-warning btn-wd btn-sm' id="nextWizard" name='next' value='Next' />
                                    <input type='submit' class='btn btn-finish btn-fill btn-warning btn-wd btn-sm' />

                                </div>

                                <div class="pull-left">
                                    <input type='button' class='btn btn-previous btn-fill btn-default btn-wd btn-sm' name='previous' value='Previous' />
                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script src="{{url('js/general.js')}}"></script>
    <script src="{{url('js/trainee/app.js')}}"></script>
@endsection
