@extends('core.trainee.main')

@section('content')
    <div class="main-site">
        @include('trainee::request.header')
        <main class="main-content secondary-nav">
            <div class="dashboard-steps">
                <div class="container">
                    <div class="form-section">
                        <form id="submit_step" action="{{route('trainee.request.submitStep')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="form_group_id" value="{{ $formData['type'] }}">
                            <input type="hidden" name="redirect" value="{{ url('trainee/request/'.$redirect.'?step=3') }}">
                            <input type="hidden" name="is_last" value="true">
                        <div class="form-title m-24">
                            <h1>{{$title}} Objectives</h1>
                        </div>
                        <div class="center-form mb-0">
                           
                            <div class="form-group reset align-items-center">
                                <label class="control-label">
                                    Training Provider
                                </label>
                                <div class="form-field">
                                    <input type="text" name="training_ti_provider" class="form-control" maxlength="300" placeholder="Enter name">
                                    @if ($errors->has('training_ti_provider'))
                                        <div class="validation-msg">
                                            Training Provider is Required
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group reset align-items-center">
                                <label class="control-label">
                                    Cost
                                </label>
                                <div class="form-field">
                                    <input type="number" name="training_ti_cost" class="form-control" maxlength="10" placeholder="Enter cost">
                                    @if ($errors->has('training_ti_cost'))
                                        <div class="validation-msg">
                                            Cost is Required
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group reset align-items-center">
                                <label class="control-label">
                                    Venue
                                </label>
                                <div class="form-field">
                                    <button type="button" class="btn btn-default btn-sm venue_button" data-target="#training_obj_venue_domestic">
                                        <i class="icon-placeholder-outline-maps-interface-tool"></i> Domestic
                                        <input type="radio" class="d-none" id="training_obj_venue_domestic" name="training_obj_venue" value="training_obj_venue_domestic">
                                    </button>
                                    <button type="button" class="btn btn-default btn-sm venue_button training_obj_venue_overseas" data-target="#training_obj_venue_overseas">
                                        <i class="icon-placeholder-outline-maps-interface-tool"></i> Overseas
                                        <input type="radio" class="d-none" id="training_obj_venue_overseas" name="training_obj_venue" value="training_obj_venue_overseas">
                                    </button>
                                </div>
                            </div>
                            @if (in_array(\Route::current()->getName(), ['trainee.inhouse']))
                            <div class="import-input d-none">

                            </div>
                            <div class="form-group reset import-table d-none">
                                <label class="control-label">
                                    Preview Import Employees<br>
                                    <small class="text-danger"> Please notice : duplicated and invalid NIK or Data will be removed </small>
                                </label>
                                <table id="importTable" class="table table-hover table-striped table-lite" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Employee Names</th>
                                        <th>Employee Nik</th>
                                        <th>Employee Position</th>
                                    </tr>
                                    </thead>

                                    <tbody id="import-list">

                                    </tbody>
                                </table>
                                <small class="text-danger d-none invalid-nik"> Invalid NIK : <span class="invalid-nik-target"></span> </small>
                            </div>
                            @endif
                            <div class="form-group reset">
                                <div class="form-field">
                                    <input id="files" type="file" accept=".pdf,doc,docx,image/*" name="training_obj_attachment" class="btn btn-default reset">
                                    <button type="button" class="btn btn-default reset">
                                        <label for="files" style="margin-bottom: 0;width: 100%; padding: 0;"><i class="icon-contact"></i> Add attachment</label>
                                    </button><br>
                                    <small class="name-files">Brochure/Proposal of Training , file pdf,doc,docx,jpeg,jpg and png</small>
                                </div>
                            </div>
                        </div>
                        <div class="bottom-step">
                            <button type="button" class="btn btn-default" onclick="location.href='{{ in_array(\Route::current()->getName(), ['trainee.inhouse']) ? url('trainee/request/inhouse?step=1') : url('trainee/request/public-training?step=1') }}'"><i class="icon-back"></i> Back</button>
                            <div class="steps-count">
                                <div class="step-item done">
                                <span class="circle-number">
                                  <em>1</em>
                                  <i class="icon-verified"></i>
                                </span>
                                    My Information
                                </div>
                                <div class="step-item active">
                                <span class="circle-number">
                                  <em>2</em>
                                  <i class="icon-verified"></i>
                                </span>
                                    Training Information
                                </div>

                                <div class="step-item">
                                <span class="circle-number">
                                  <em>3</em>
                                  <i class="icon-verified"></i>
                                </span>
                                    Completed
                                </div>
                                <div class="step-item counter">
                            			<p class="mb-0 text-white">Step <span class="text-white">2</span> / 3</p>
                            		</div>
                            </div>
                            <button type="submit" class="btn btn-default btn_block" >Submit <i class="icon-next"></i></button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    </div>

    <br><br><br>
    <!-- Modal -->
@endsection
@section('scripts')
    <script>

    </script>
    <script>
        @if(session('message'))
            alert('{{session("message")}}');
        @endif
    </script>
@endsection
