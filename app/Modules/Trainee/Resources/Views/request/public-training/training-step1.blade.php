@php
    $user = \Sentinel::check();
    $helper = new \App\Helpers\UserRolesHelper;
@endphp
@extends('core.trainee.main')
 
@section('content')
        <div class="main-content secondary-nav">
                <div class="dashboard-steps">
                    @include('trainee::request.header')
                    <div class="dashboard-entry">
                        <div class="container-fluid">

                            <div class="form-section">
                                <form id="submit_step" action="{{route('trainee.request.submitStep')}}" method="post">
                                    @csrf
                                    <input type="hidden" name="form_group_id" value="{{ $formData['type'] }}">
                                    <input type="hidden" name="redirect" value="{{ url('trainee/request/'.$redirect.'?step=2') }}">

                                <div class="form-title m-24">
                                    <h1>{{$title}} Information</h1>
                                </div>
                                <div class="split-field medium">
                                    <div class="field-right">
                                        <div class="form-group reset">
                                            <label class="control-label">
                                                Propose Training Title
                                            </label>
                                            <input type="text" name="training_ti_title" class="form-control" placeholder="Enter Propose Training Title" maxlength="150">
                                            @if ($errors->has('training_ti_title'))
                                                <div class="validation-msg">
                                                    Propose Training Title is Required
                                                </div>
                                            @endif
                                        </div> 
                                        <div class="more-item">
                                            <div class="field-group has-more">
                                                @if ($helper->inAdmin())
                                                <div class="form-group reset">
                                                    <label class="control-label">Employee Name</label>
                                                    <select class="form-control select-employee2">
                                                    //
                                                    </select>
                                                    <input name="training_obj_employee_name[]" type="hidden" class="form-control select-employee-name2" placeholder="enter name" readonly required>
                                                    @if ($errors->has('training_obj_employee_name'))
                                                        <div class="validation-msg">
                                                            My Name Name is Required
                                                        </div>
                                                    @endif
                                                </div>
                                                @else
                                                <label class="control-label text-left">
                                                    My Name
                                                </label>
                                                <input name="training_obj_employee_name[]" type="text" class="form-control select-employee-name" placeholder="Enter name" value="{{$employee['staff']['full_name']}}" readonly required>
                                                @endif

                                                @if ($helper->inAdmin())
                                                    <label class="control-label text-left">
                                                        Employee ID
                                                    </label>
                                                @else
                                                    <label class="control-label text-left">
                                                        My Employee ID
                                                    </label>
                                                @endif

                                                <input name="training_obj_employee_nik[]" type="text" id="nik_coachee" class="form-control nik_coachee nik_coachee2" placeholder="Example: 68025191" value="{{$employee['staff']['nik']}}" readonly required>
                                                
                                                @if ($helper->inAdmin())
                                                    <label class="control-label text-left">
                                                        Employee Position
                                                    </label>
                                                @else
                                                    <label class="control-label text-left">
                                                        My Position
                                                    </label>
                                                @endif
                                                
                                                <input type="text" name="training_obj_employee_position[]" class="form-control position_employee position_employee2" value="{{$employee['staff']['position']}}" placeholder="Enter employee position" readonly required>
                                                
                                                <a href="javascript:void(0)" id="cloneCloseDiv" class="btn btn-default remove-item"><i class="mdi mdi-close-circle"></i> remove</a>
                                            </div>
                                            <div class="more-item-target">

                                            </div>
                                            @if (in_array(\Route::current()->getName(), ['trainee.inhouse']))
                                                <a href="javascript:void(0)" class="btn btn-default add-more"><i class="mdi mdi-plus-circle"></i> Add more Member</a>
                                            @endif
                                        </div>
                                        @if($formData['type'] == 'Public Training')
                                            <div class="form-group reset">
                                                <label class="control-label">
                                                    <button type="button" data-toggle="modal" class="btn-question" data-target="#typeProgramsModal">
                                                        <i class="icon-questions-circular-button" >  </i>
                                                    </button>
                                                    Developement type
                                                </label>
                                                <div class="check-group">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="training_ti_type_programs" type="radio" value="general training" checked=""> General Training
                                                            <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="training_ti_type_programs" type="radio" value="technical certification"> Professional/Technical Certification
                                                            <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="training_ti_type_programs" type="radio" value="seminar certification"> Seminar/Conference
                                                            <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            <div class="form-group reset">
                                                <label class="control-label">
                                                    Type of programs
                                                </label>
                                                <div class="check-group">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="training_ti_type_programs" type="radio" value="training" checked=""> Training
                                                            <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="training_ti_type_programs" type="radio" value="certification"> Certification
                                                            <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        
                                        <div class="form-group reset">
                                            <label class="control-label">
                                                Date
                                            </label>
                                            <div class="field-date">
                                                <div class="date-input">
                                                    <input type="text" name="training_ti_start_date" autocomplete="off" class="form-control start-date" onchange="dateStartSelector(this)" placeholder="Enter start date">
                                                    <button type="button" class="btn btn-default btn-icon ">
                                                        <i class="mdi mdi-calendar"></i>
                                                    </button>
                                                </div>
                                                To
                                                <div class="date-input">
                                                    <input type="text" name="training_ti_end_date" autocomplete="off" class="form-control end-date" placeholder="Enter end date">
                                                    <button type="button" class="btn btn-default btn-icon">
                                                        <i class="mdi mdi-calendar"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            @if ($errors->has('training_ti_start_date') || $errors->has('training_ti_end_date'))
                                                <div class="validation-msg">
                                                    Date is Required
                                                </div>
                                            @endif

                                        </div>

                                        <div class="form-group reset">
                                            <label class="control-label">
                                                <button type="button" data-toggle="modal" class="btn-question" data-target="#competencyModal">
                                                    <i class="icon-questions-circular-button" >  </i>
                                                </button>
                                                Competency to be Developed
                                            </label>
                                            <div class="form-field mb-3">
                                                <select class="form-control select2-comp" name="training_obj_employee_competency[]" multiple="multiple">
                                                    @if (count($competencies)>0)
                                                        @foreach($competencies as $key => $c)
                                                            <optgroup label="{{ $c['option_group'] }}">
                                                                @foreach($c['option_relation'] as $opt_sub)
                                                                    <option>
                                                                        {{ $opt_sub['name'] }}
                                                                    </option>
                                                                @endforeach
                                                            </optgroup>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            @if ($errors->has('training_obj_employee_competency'))
                                                <div class="validation-msg">
                                                    Competency to be Developed is Required
                                                </div>
                                            @endif
                                        </div>

                                         <div class="form-group reset">
                                            <label class="control-label">
                                                What do i want to develop by joining this Training
                                            </label>
                                            <div class="form-field">
                                                <textarea class="form-control counter-text" name="training_obj_description" rows="6" placeholder="What do i want to develop by joining this Training"></textarea>
                                                <p class="form-helper text-count"><span class="counter-text-value">0</span>/255</p>
                                            </div>
                                            @if ($errors->has('training_obj_description'))
                                                <div class="validation-msg">
                                                    What do i want to develop by joining this Training is Required
                                                </div>
                                            @endif
                                        </div>

                                        @if (in_array(\Route::current()->getName(), ['trainee.publicTrainingStep1']))
                                        <div class="form-group reset">
                                            <div class="form-field">
                                                <div class="multifield">
                                                    <label class="control-label">
                                                        Commitment After Training *Choose max 3 type
                                                    </label>
                                                    <select class="form-control select2-comp" name="training_obj_commitment_training[]" multiple="multiple">
                                                        <option value="Knowledge Sharing">Knowledge Sharing </option>
                                                        <option value="Project">Project </option>
                                                        <option value="Job Assignment">Job Assignment </option>
                                                    </select>
                                                </div>
                                                @if ($errors->has('training_obj_commitment_training'))
                                                    <div class="validation-msg">
                                                        Commitment After Training is Required
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group reset">
                                            <label class="control-label">
                                                Description
                                            </label>
                                            <textarea name="training_obj_commitment_description" class="form-control counter-text" rows="2" placeholder="Enter Description"></textarea>
                                            <p class="form-helper text-count"><span class="counter-text-value">0</span>/255</p>
                                            @if ($errors->has('training_obj_commitment_description'))
                                                <div class="validation-msg">
                                                    Description is Required
                                                </div>
                                            @endif
                                        </div>

                                        <div class="form-group reset">
                                            <div class="form-field">
                                                <div class="multifield">
                                                    <label class="control-label">
                                                        Commited Date
                                                    </label>
                                                    <div class="field-date">
                                                        <div class="date-input">
                                                            <input type="text" name="training_obj_start_date2" autocomplete="off" class="form-control start-date2" onchange="dateStartSelector(this)" placeholder="Enter start date">
                                                            <button type="button" class="btn btn-default btn-icon ">
                                                                <i class="mdi mdi-calendar"></i>
                                                            </button>
                                                        </div>
                                                        To
                                                        <div class="date-input">
                                                            <input type="text" name="training_obj_end_date2" autocomplete="off" class="form-control end-date2" placeholder="Enter end date">
                                                            <button type="button" class="btn btn-default btn-icon">
                                                                <i class="mdi mdi-calendar"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    @if ($errors->has('training_obj_start_date2') || $errors->has('training_obj_end_date2'))
                                                        <div class="validation-msg">
                                                            Commited date is Required
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        

                                    @endif

                                    </div>
                                    <div class="field-left d-none d-xl-block" id="fl-lef">
                                        <figure>
                                            <img src="{{ url('images/media05.png') }}" alt="ooredoo" class="img-fluid">
                                        </figure>
                                    </div>
                                </div>

                                <div class="bottom-step">
                                    <button type="button" class="btn btn-default" onclick="location.href='{{ url('trainee/request') }}'"><i class="icon-close"></i> Cancel</button>
                                    <div class="steps-count">
                                    	<div class="step-item active">
                                    		<span class="circle-number">
                                    			<em>1</em>
                                    			<i class="icon-verified"></i>
                                    		</span>
                                    		My Information
                                    	</div>
                                    	<div class="step-item">
                                    		<span class="circle-number">
                                    			<em>2</em>
                                    			<i class="icon-verified"></i>
                                    		</span>
                                    		Training Information
                                    	</div>

                                    	<div class="step-item">
                                    		<span class="circle-number">
                                    			<em>3</em>
                                    			<i class="icon-verified"></i>
                                    		</span>
                                    		Completed
                                    	</div>

                                    	<div class="step-item counter">
                                    		<p class="mb-0 text-white">Step <span class="text-white">1</span> / 3</p>
                                    	</div>
                                    </div>
                                    <button type="submit" class="btn btn-default btn_block" >Next <i class="icon-next"></i></button>
                                </div>
                                </form>
                            </div>

                        </div>    
                    </div>
                </div>
        </div>

    @include('modal-type-programs')
    @include('modal-competency')
    <br><br><br>

@endsection
@section('scripts')
    <script type="text/javascript">

    	$('.bottom-nav').addClass('hidden')
        function format (d) {
            content = [];
            for (i=0;i<d.length;i++){
                content.push('<tr><td>'+d[i].name+'</td>'+
                '<td>'+d[i].definition+'</td>'+
                '</tr>');
            }
            tableCompetenciesRet = '<table class="table-bordered">'+
                        content.toString().replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '') +
                    '</table>';
            return tableCompetenciesRet;
            ;
        }
        var datatableComp = $('#tableCompetencies').DataTable( {
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {
                url: "{{ route('trainee.dtCompetencies') }}",
            },
            order: [ 1, 'asc' ],
            columns: [
                { data: 'category' },
                { data: 'competency', render: function ( data ) {
                    return format(data);
                }}
            ],
        });
    </script>
    <!-- new -->
    <script>
    var delayInMilliseconds = 2000; //1 second

    setTimeout(function() {
        var a = document.getElementById('fl-lef');
        a.className += ' class_two';
        a.style.display = 'block';
    
        // a.classList.remove('d-none');
    }, delayInMilliseconds);
    
    </script>
<!-- new -->
@endsection
