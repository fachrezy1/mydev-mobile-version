<div class="tab-pane show active" id="about">
    <div class="row">
        <div class="col-12 mx-auto">
            <div class="row">
                <div class="col-1 trainee-form-request-content_wrapper">

                </div>
                <div class="col-10 trainee-form-request-content_slider">
                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="row trainee-form-request-content_slider-content carousel-item active">
                                <div class="col-3">
                                    <div class="card trainee-form-request-content-card custom-radio-parent {{ $formData['type'] === 'Assignment' ? 'active' :'' }}" >
                                        <img class="card-img-top trainee-form-request-content-card_image" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP88ffHfwAJ0APuzD6SLQAAAABJRU5ErkJggg==" alt="Card image cap">
                                        <div class="card-body trainee-form-request-content-card_content">
                                            <h5 class="card-title">Assignment</h5>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                            <input type="radio" name="type" value="Assignment" class="d-none" id="assignment" {{ $formData['type'] === 'Assignment' ? 'checked' :'' }}>
                                            <a href="{{route('trainee.assignment')}}">
                                                <button data-for="assignment" type="button" class="btn btn-primary trainee-form-request-content-card_button custom-radio">Select</button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="card trainee-form-request-content-card custom-radio-parent {{ $formData['type'] === 'Coaching' ? 'active' :'' }}" >
                                        <img class="card-img-top trainee-form-request-content-card_image" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP88ffHfwAJ0APuzD6SLQAAAABJRU5ErkJggg==" alt="Card image cap">
                                        <div class="card-body trainee-form-request-content-card_content">
                                            <h5 class="card-title">Coaching</h5>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                            <input type="radio" name="type" value="Coaching" class="d-none" id="coaching" {{ $formData['type'] === 'Coaching' ? 'checked' :'' }}>
                                            <button data-for="coaching" type="button" class="btn btn-primary trainee-form-request-content-card_button custom-radio">Select</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="card trainee-form-request-content-card custom-radio-parent {{ $formData['type'] === 'Mentoring' ? 'active' :'' }}" >
                                        <img class="card-img-top trainee-form-request-content-card_image" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP88ffHfwAJ0APuzD6SLQAAAABJRU5ErkJggg==" alt="Card image cap">
                                        <div class="card-body trainee-form-request-content-card_content">
                                            <h5 class="card-title">Mentoring</h5>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                            <input type="radio" name="type" value="Mentoring" class="d-none" id="mentoring" {{ $formData['type'] === 'Mentoring' ? 'checked' :'' }}>
                                            <button data-for="mentoring" type="button" class="btn btn-primary trainee-form-request-content-card_button custom-radio">Select</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="card trainee-form-request-content-card custom-radio-parent {{ $formData['type'] === 'Public Training' ? 'active' :'' }}">
                                        <img class="card-img-top trainee-form-request-content-card_image" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP88ffHfwAJ0APuzD6SLQAAAABJRU5ErkJggg==" alt="Card image cap">
                                        <div class="card-body trainee-form-request-content-card_content">
                                            <h5 class="card-title">Public Training</h5>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                            <input type="radio" name="type" value="Public Training" class="d-none" id="publicTraining" {{ $formData['type'] === 'Public Training' ? 'checkout' :'' }}>
                                            <a href="{{route('trainee.publicTraining')}}">
                                                <button data-for="publicTraining" type="button" class="btn btn-primary trainee-form-request-content-card_button custom-radio">Select</button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <div class="col-1 trainee-form-request-content_wrapper">

                </div>
            </div>
        </div>
    </div>
</div>
