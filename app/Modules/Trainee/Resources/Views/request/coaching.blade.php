@extends('core.trainee.main')

@section('head')
    <link rel="stylesheet" href="{{url('css/general.css')}}">
    <link rel="stylesheet" href="{{url('css/trainee/app.css')}}">
    <link rel="stylesheet" href="{{url('css/formwizard/gsdk-bootstrap-wizard.css')}}">
    <link rel="stylesheet" href="{{url('css/formwizard/demo.css')}}">
@endsection
@section('content')
    @include('core.user.header')
    <section class="container-fluid trainee-form-request">
        <div class="row">
            <div class="col-12 trainee-form-request-text text-center">
                <h2> You are requesting a new trainee </h2>
                <h5> Ooredoo here to help you learn, grow, and boost. what you'd like to achieve </h5>
            </div>
            <div class="col-12 trainee-form-request-content">

                <div class="wizard-container">

                    <div class="card wizard-card" data-color="orange" id="wizardProfile">
                        <form action="{{route('trainee.request.submitStep')}}" method="post">
                            @csrf
                            <input type="hidden" name="form_group_id" value="Coaching Goal">
                            <div class="wizard-header">
                            </div>

                            <div class="wizard-navigation">
                                <ul>
                                    <li><a href="#information" data-toggle="tab">2</a></li>
                                    <li><a href="#development" data-toggle="tab">3</a></li>
                                    <li><a href="#projectInformation" data-toggle="tab">4</a></li>
                                    <li><a href="#projectDescription" data-toggle="tab">5</a></li>
                                    <li><a href="#background" data-toggle="tab">6</a></li>
                                    <li><a href="#mpo" data-toggle="tab">7</a></li>
                                    <li><a href="#timeline" data-toggle="tab">8</a></li>
                                    <li><a href="#barriers" data-toggle="tab">9</a></li>
                                    <li><a href="#support" data-toggle="tab">10</a></li>
                                    <li><a href="#sign" data-toggle="tab">11</a></li>
                                    <li><a href="#notes" data-toggle="tab">12</a></li>
                                </ul>

                            </div>

                            <div class="tab-content">
                                <div class="tab-pane show active" id="information">
                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>Coach & Coachee Information <small>(required)</small></label>
                                                <input name="coach_ci_employee_name" type="text" class="form-control" placeholder="Employee (Coachee) Name">
                                                <input name="coach_ci_employee_nik" type="text" class="form-control" placeholder="Employee (Coachee) NIK">
                                                <input name="coach_ci_coach_name" type="text" class="form-control" placeholder="Coach Name">
                                                <input name="coach_ci_coach_nik" type="text" class="form-control" placeholder="Coach NIK">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="development">
                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>Start Date <small>(required)</small></label>
                                                <input name="coach_start_date" type="date" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="development">
                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>End Date <small>(required)</small></label>
                                                <input name="coach_end_date" type="date" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="projectInformation">
                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>Competency to be developed <small>(required)</small></label>
                                                <textarea name="coach_compt_to_be_developed" type="text" class="form-control"> </textarea>
                                                <label>Mapped to Indosat Ooredoo Competency <small>(required)</small></label>
                                                <textarea name="coach_mapped_to_indosat_ooredoo_compt" type="text" class="form-control"> </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="projectDescription">
                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>Coaching Topic <small>(required)</small></label>
                                                <textarea name="coach_topic"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="projectDescription">
                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>Coaching Objective <small>(required)</small></label>
                                                <textarea name="coach_objective"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="background">
                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>Target Behavior <small>(required)</small></label>
                                                <textarea name="coach_target_behavior" placeholder="expected change of behavior at the end of the coaching program"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="mpo">
                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>Development after Coaching <small>(if Any)</small></label>
                                                <input name="coach_dev_after_coaching" type="text" class="form-control" placeholder="e.g increase revenue 20%">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="sign">
                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>Sign <small>(required)</small></label>
                                                <div class="col-12">
                                                    <label> Employee </label>
                                                    <input name="coach_sign_employee_name" type="text" class="form-control" placeholder="Employee (Coachee) Name">
                                                    <input name="coach_sign_employee_nik" type="text" class="form-control" placeholder="Employee (Coachee) NIK">
                                                    <input name="coach_sign_employee_signature" type="text" class="form-control" placeholder="Signature">
                                                </div>
                                                <div class="col-12">
                                                    <label> Sponsor 1 (Line Manager) </label>
                                                    <small> Initial Unit </small>
                                                    <input name="coach_sponsor_1_name" type="text" placeholder="Name">
                                                    <input name="coach_sponsor_1_nik" type="text" placeholder="NIK">
                                                    <input name="coach_sponsor_1_signature" type="text" placeholder="Signature">
                                                </div>
                                                <div class="col-12">
                                                    <label> Sponsor 2 (Line Manager) </label>
                                                    <small> Designated Unit </small>
                                                    <input name="coach_sponsor_2_name" type="text" placeholder="Name">
                                                    <input name="coach_sponsor_2_nik" type="text" placeholder="NIK">
                                                    <input name="coach_sponsor_2_signature" type="text" placeholder="Signature">
                                                </div>
                                                <div class="col-12">
                                                    <label> HR Development </label>
                                                    <input name="coach_hr_dev_name" type="text" placeholder="Name">
                                                    <input name="coach_hr_dev_nik" type="text" placeholder="NIK">
                                                    <input name="coach_hr_dev_signature" type="text" placeholder="Signature">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="notes">
                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>Notes <small>(required)</small></label>
                                                <textarea name="coach_notes"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wizard-footer height-wizard">
                                <div class="pull-right">
                                    <input type='button' class='btn btn-next btn-fill btn-warning btn-wd btn-sm' id="nextWizard" name='next' value='Next' />
                                    <input type='submit' class='btn btn-finish btn-fill btn-warning btn-wd btn-sm' />

                                </div>

                                <div class="pull-left">
                                    <input type='button' class='btn btn-previous btn-fill btn-default btn-wd btn-sm' name='previous' value='Previous' />
                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script src="{{url('js/general.js')}}"></script>
    <script src="{{url('js/trainee/app.js')}}"></script>
@endsection
