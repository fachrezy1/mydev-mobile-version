@php
    $user = \Sentinel::check();
    $helper = new \App\Helpers\UserRolesHelper;
@endphp
@extends('core.trainee.main')
{{--@dd($user)--}}
@php($user = \Sentinel::check())
@section('content')
    <main class="main-content secondary-nav">
        <div class="dashboard-steps">
            @include('trainee::request.header')

            <div class="dashboard-entry">
                <div class="container-fluid">
                    <div class="form-section">
                        <form id="submit_step" action="{{route('trainee.request.submitStep')}}" method="post">
                            @csrf
                            <input type="hidden" name="form_group_id" value="{{ $formData['type'] }}">
                            <input type="hidden" name="redirect" value="{{ url('trainee/request/'.$redirect.'?step=2') }}">
                            <input type="hidden" name="is_last" value="true">
                            <input type="hidden" name="user_role" value="employee">
                            <input type="hidden" name="user_nik" class="nik_coachee nik_coachee2" value="{{$employee['staff']['nik']}}">

                            <div class="form-title m-24">
                                <h1>{{in_array(\Route::current()->getName(), ['trainee.internalCoach']) ? 'Trainer Information' : 'External Speaker Information'}}</h1>
                            </div>

                            <div class="split-field medium">
                                <div class="field-right">
                                    @if(in_array(\Route::current()->getName(), ['trainee.internalCoach']))
                                        <div class="form-group reset">
                                            <label class="control-label text-left">
                                                My name
                                            </label> 
                                            {{--<select class="form-control select-employee">--}}

                                            {{--</select>--}}
                                            <input name="mentoring_ci_employee_name" type="text" class="form-control select-employee-name" placeholder="Enter Name" value="{{$user->first_name}}" readonly>
                                        </div>

                                        <div class="form-group reset">
                                            <label class="control-label text-left">
                                                My Employee ID
                                            </label>
                                        <input name="mentoring_ci_employee_nik" type="text" class="form-control nik_coachee" placeholder="Employee NIK" readonly>
                                        </div>
                                        @if ($errors->has('mentoring_ci_employee_name') || $errors->has('mentoring_ci_employee_nik'))
                                            <div class="validation-msg">
                                                This Field is Required
                                            </div>
                                        @endif
                                        
                                    @else
                                        <div class="form-group reset">
                                            @if ($helper->inAdmin())
                                                <div class="form-group reset">
                                                <label class="control-label">Employee Name</label>
                                                <select class="form-control select-employee2">
                                                <!-- -->
                                                </select>
                                                <input name="mentoring_ci_employee_name" type="hidden" class="form-control select-employee-name select-employee-name2" placeholder="enter name"  required>
                                                @if ($errors->has('mentoring_ci_employee_name'))
                                                    <div class="validation-msg">
                                                    My Name Name is Required
                                                    </div>
                                                @endif
                                                </div>
                                            @else
                                            
                                            <label class="control-label">My Name</label>

                                            {{-- <select class="form-control select-employee">

                                            </select> --}}
                                            <input type="text" name="mentoring_ci_employee_name" id="nik_coachee" class="form-control select-employee-name " value="{{$employee['staff']['full_name']}}" placeholder="enter name" readonly >

                                            @endif

                                            <input name="mentoring_ci_employee_nik" type="hidden" class="form-control nik_coachee nik_coachee2" value="{{$employee['staff']['nik']}}" placeholder="Enter name" readonly >
                                            @if ($errors->has('mentoring_ci_employee_name') || $errors->has('mentoring_ci_employee_nik'))
                                                <div class="validation-msg">
                                                    This Field is Required
                                                </div>
                                            @endif
                                        </div>
                                    @endif

                                    @if (in_array(\Route::current()->getName(), ['trainee.internalCoach']))
                                        {{--<div class="form-group reset">--}}
                                        {{--<label class="control-label">--}}
                                        {{--Position--}}
                                        {{--</label>--}}
                                        {{--<div class="check-group">--}}
                                        {{--<div class="form-check">--}}
                                        {{--<label class="form-check-label">--}}
                                        {{--<input class="form-check-input" name="mentoring_ci_position" type="radio" value="coach" checked=""> Coach--}}
                                        {{--<span class="form-check-sign">--}}
                                        {{--<span class="check"></span>--}}
                                        {{--</span>--}}
                                        {{--</label>--}}
                                        {{--</div>--}}
                                        {{--<div class="form-check">--}}
                                        {{--<label class="form-check-label">--}}
                                        {{--<input class="form-check-input" name="mentoring_ci_position" type="radio" value="mentor"> Mentor--}}
                                        {{--<span class="form-check-sign">--}}
                                        {{--<span class="check"></span>--}}
                                        {{--</span>--}}
                                        {{--</label>--}}
                                        {{--</div>--}}
                                        {{--<div class="form-check">--}}
                                        {{--<label class="form-check-label">--}}
                                        {{--<input class="form-check-input" name="mentoring_ci_position" type="radio" value="trainer"> Trainer--}}
                                        {{--<span class="form-check-sign">--}}
                                        {{--<span class="check"></span>--}}
                                        {{--</span>--}}
                                        {{--</label>--}}
                                        {{--</div>--}}
                                        {{--</div>--}}
                                        {{--</div>--}}
                                    @endif
                                    {{--<div class="form-group reset">--}}
                                    {{--<label class="control-label">Employee (Coachee) NIK</label>--}}
                                    {{--<input name="mentoring_ci_employee_nik" type="text"  value="{{$employee['staff']['nik']}}" readonly  class="form-control" placeholder="example: IND-010119" required>--}}
                                    {{--</div>--}}

                                    <div class="form-group reset">
                                        <label class="control-label">Topic</label>
                                        <input name="mentoring_ci_line_topic" type="text" class="form-control" placeholder="Enter Topic">
                                        @if ($errors->has('mentoring_ci_line_topic'))
                                            <div class="validation-msg">
                                                Topic is Required
                                            </div>
                                        @endif
                                    </div>
                                    @if ($formData['type'] != 'Internal Coach')
                                        <div class="form-group reset">
                                            <label class="control-label">Organization</label>
                                            <input name="mentoring_ci_organization" type="text" class="form-control" placeholder="Enter Organization">
                                            @if ($errors->has('mentoring_ci_organization'))
                                                <div class="validation-msg">
                                                    Organization is Required
                                                </div>
                                            @endif
                                        </div>
                                    @endif
                                    @if ($formData['type'] == 'Internal Coach')
                                        <div class="form-group reset">
                                            <label class="control-label">Notes</label>
                                            <textarea name="mentoring_ci_notes" rows="4" class="form-control"></textarea>
                                            @if ($errors->has('mentoring_ci_notes'))
                                                <div class="validation-msg">
                                                    Notes is Required
                                                </div>
                                            @endif
                                        </div>
                                    @endif
                                    <div class="form-group reset">
                                        <label class="control-label">Venue</label>
                                        <input name="mentoring_ci_line_venue" type="text" class="form-control" placeholder="Enter Venue">
                                        @if ($errors->has('mentoring_ci_line_venue'))
                                            <div class="validation-msg">
                                                Venue is Required
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group reset">
                                        <label class="control-label">Date</label>
                                        <div class="field-date">
                                            <div class="date-input">
                                                <input name="mentoring_ci_start_date" autocomplete="off" type="text" class="form-control start-date" placeholder="Enter start date">
                                                <button type="button" class="btn btn-default btn-icon "><i class="mdi mdi-calendar"></i></button>
                                            </div>

                                            To

                                            <div class="date-input">
                                                <input name="mentoring_ci_end_date" autocomplete="off" type="text" class="form-control end-date" placeholder="Enter end date">
                                                <button type="button" class="btn btn-default btn-icon"><i class="mdi mdi-calendar"></i></button>
                                            </div>
                                        </div>
                                        @if ($errors->has('mentoring_ci_start_date') || $errors->has('mentoring_ci_end_date'))
                                            <div class="validation-msg">
                                                Date is Required
                                            </div>
                                        @endif
                                    </div>

                                </div>

                                <div class="field-left d-none d-xl-block" id="fl-lef">
                                    <figure>
                                        <img src="{{ url('images/vector1.png') }}" alt="ooredoo" class="img-fluid">
                                    </figure>
                                </div>
                            </div>
                            <div class="bottom-step">
                                <button type="button" class="btn btn-default" onclick="location.href='{{ url('trainee/request') }}'"><i class="icon-close"></i> Cancel</button>
                                <div class="steps-count">
                                	<div class="step-item active">
                                		<span class="circle-number">
                                			<em>1</em>
                                			<i class="icon-verified"></i>
                                		</span>
                                		My Information
                                	</div>
                                	<div class="step-item counter">
	                            			<p class="mb-0 text-white">Step <span class="text-white">1</span> / 2</p>
	                            		</div>
                                </div>
                                <button form="submit_step" type="submit" class="btn btn-default btn_block" >Submit <i class="icon-next"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <!-- new -->
    <script>
    var delayInMilliseconds = 3000; //1 second

    setTimeout(function() {
        var a = document.getElementById('fl-lef');
        a.className += ' class_two';
        a.style.display = 'block';
    
        // a.classList.remove('d-none');
    }, delayInMilliseconds);
    
    </script>
    <!-- new -->
    </main>

    <br><br><br>

@endsection
