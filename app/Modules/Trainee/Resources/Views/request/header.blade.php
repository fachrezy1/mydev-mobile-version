@php($user=\Sentinel::check())
@php($helper = new \App\Helpers\UserRolesHelper )
<header class="main-header for-steps">
    <div class="container-fluid">
        <div class="top-header">
            @if($helper->inStaff())
                <a href="{{ (in_array(\Route::current()->getName(), ['trainee.inhouse'])) ? url('/') : url('trainee/dashboard') }}" class="main-brand">
            @else
                <a href="{{ (in_array(\Route::current()->getName(), ['trainee.inhouse'])) ? url('/') : url('/dashboard') }}" class="main-brand">
            @endif
            
                <img src="{{url('images/logo.png')}}" alt="ooredoo" class="img-fluid">
            </a>
            
            <div class="step-info d-md-block d-none">
                {{$title}}
            </div>
            <div class="exit-button">
                @if ($helper->inManager() || $helper->inAdmin())
                    @if((in_array(\Route::current()->getName(), ['trainee.inhouse'])))
                        <button type="button" class="btn btn-default" onclick="location.href='{{ route('trainee.index') }}'">Exit to dashboard</button>

                    @else
                        <button type="button" class="btn btn-default" onclick="location.href='{{ route('dashboard.traineeRequest') }}'">Exit to dashboard</button>

                    @endif
                @else
                    <button type="button" class="btn btn-default" onclick="location.href='{{ route('trainee.dashboard.index') }}'">Exit to dashboard</button>
                @endif
            </div>
        </div>
    </div>
</header>
