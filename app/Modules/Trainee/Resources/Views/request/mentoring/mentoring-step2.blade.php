@extends('core.trainee.main')

@section('content')

<main class="main-content secondary-nav">
    <div class="dashboard-steps">
        @include('trainee::request.header')
        <div class="dashboard-entry">
            <div class="container-fluid">  
                <div class="form-section">  
                    <div class="form-title m-24">
                        <h1>

                             
                           @if($type_coaching=="coaching")

                              Coach & Coachee Information
                           @else

                               Mentor & Mentee Information
                           @endif
                        </h1>
                    </div>

                    <div class="center-form">
                        <form id="submit_step" action="{{route('trainee.request.submitStep')}}" method="post">
                        @csrf
                        <input type="hidden" name="form_group_id" value="{{ $formData['type'] }}">
                        <input type="hidden" name="redirect" value="{{ url('trainee/request/'.$redirect.'?step=3') }}">
                        <input type="hidden" name="is_last" value="true">

                        <div class="form-group reset">
                            <label class="control-label">
                                What do i want to develop by joining this project
                            </label>
                            <textarea rows="6" name="coaching_cc_development_goal" class="form-control" placeholder="What do i want to develop by joining this project"></textarea>
                            @if ($errors->has('coaching_cc_development_goal'))
                                <div class="validation-msg">
                                    What do i want to develop by joining this project is Required
                                </div>
                            @endif

                            {{--<textarea name="coaching_cc_development_goal" class="form-control" rows="6" placeholder="Enter Development Goal" required></textarea>--}}
                        </div>
                        <div class="form-group reset">
                            <label class="control-label">
                                <button type="button" data-toggle="modal" class="btn-question" data-target="#competencyModal">
                                    <i class="icon-questions-circular-button" >  </i>
                                </button>
                                Competency to be Developed
                            </label>
                            <div class="form-field mb-3">
                                <select class="form-control select2-comp" name="coaching_cc_development_mtioc[]" multiple="multiple" required>
                                    @if (count($competencies)>0)
                                        @foreach($competencies as $key => $c)
                                            <optgroup label="{{ $c['option_group'] }}">
                                                @foreach($c['option_relation'] as $opt_sub)
                                                    <option>
                                                        {{ $opt_sub['name'] }}
                                                    </option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    @endif
                                </select>
                                @if ($errors->has('training_obj_employee_competency'))
                                    <div class="validation-msg">
                                        Competency to be Developed is Required
                                    </div>
                                @endif
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="bottom-step">
                        <button type="button" class="btn btn-default" onclick="location.href='{{ url('trainee/request/mentoring?step=1') }}'"><i class="icon-back"></i> Back</button>
                        <div class="steps-count">
                        	<div class="step-item done">
                        		<span class="circle-number">
                        			<em>1</em>
                        			<i class="icon-verified"></i>
                        		</span>
                        		My Information
                        	</div>
                        	<div class="step-item active">
                        		<span class="circle-number">
                        			<em>2</em>
                        			<i class="icon-verified"></i>
                        		</span>
                        		Competency and topic
                        	</div>

                        	<div class="step-item">
                        		<span class="circle-number">
                        			<em>3</em>
                        			<i class="icon-verified"></i>
                        		</span>
                        		Completed
                        	</div>
                        	<div class="step-item counter">
                        		<p class="mb-0 text-white">Step <span class="text-white">2</span> / 3</p>
                        	</div>
                        </div>
                        <button form="submit_step" type="submit" class="btn btn-default btn_block" >Submit <i class="icon-next"></i></button>
                    </div>
                </div>  
            </div>
        </div>  
    </div>
</main>
@include('modal-competency')
@endsection
@section('scripts')
    <script>
        function format (d) {
            content = [];
            for (i=0;i<d.length;i++){
                content.push('<tr><td>'+d[i].name+'</td>'+
                '<td>'+d[i].definition+'</td>'+
                '</tr>');
            }
            tableCompetenciesRet = '<table class="table-bordered">'+
                        content.toString().replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '') +
                    '</table>';
            return tableCompetenciesRet;
            ;
        }
        var datatableComp = $('#tableCompetencies').DataTable( {
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {
                url: "{{ route('trainee.dtCompetencies') }}",
            },
            order: [ 1, 'asc' ],
            columns: [
                { data: 'category' },
                { data: 'competency', render: function ( data ) {
                    return format(data);
                }}
            ],
        });
    </script>
@endsection
