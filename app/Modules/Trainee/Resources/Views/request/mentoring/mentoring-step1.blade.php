@php
    $user = \Sentinel::check();
    $helper = new \App\Helpers\UserRolesHelper;
@endphp

@extends('core.trainee.main')

@section('content')

<main class="main-content secondary-nav">
    <div class="dashboard-steps">
        @include('trainee::request.header')
        <div class="dashboard-entry">
            <div class="container-fluid">  
                <div class="form-section">
                    <form id="submit_step" action="{{route('trainee.request.submitStep')}}" method="post">
                        @csrf
                        <input type="hidden" name="form_group_id" value="{{ $formData['type'] }}">
                        <input type="hidden" name="redirect" value="{{ url('trainee/request/'.$redirect.'?step=2') }}">
                        <input name="user_role" type="hidden" value="Mentor" readonly required>
                        <input name="user_nik" type="hidden" class="nik_coachee" value="project_leader" readonly required>

                       <div class="form-title m-24 header-coaching">
                            <h1>Coach & Coachee Information</h1>
                        </div>

                          <div class="form-title m-24 header-mentoring d-none">
                            <h1>Mentor & Mentee Information</h1>
                        </div>



                        <div class="split-field medium">
                            <div class="field-right">
                                
                                @if ($helper->inAdmin())
                                <div class="form-group reset">
                                    <label class="control-label">Employee Name</label>
                                    <select class="form-control select-employee2">

                                    </select>
                                    <input name="coaching_ci_employee_name" type="hidden" class="form-control select-employee-name2" placeholder="enter name"  required>
                                    @if ($errors->has('coaching_ci_line_manager_name'))
                                        <div class="validation-msg">
                                            Coach/Mentor Name is Required
                                        </div>
                                    @endif
                                </div>
                                @else
                                <div class="form-group reset">
                                    @php /*<label class="control-label">My {{ ($formData['type']=='Assignment')?'(Asignee)': '(Coachee/Mentee)' }} Name</label>*/ @endphp
                                    <label class="control-label">My Name</label>
                                    <input type="text" name="coaching_ci_employee_name" class="form-control" placeholder="enter name" value="{{$employee['staff'] ? $employee['staff']['full_name'] : ''}}" readonly required>
                                </div>
                                @endif

                                @if (!(in_array(\Route::current()->getName(), ['trainee.assignment'])))
                                <div class="form-group reset">
                                    <label class="control-label">
                                        Type of Coaching/Mentoring
                                    </label>
                                    <div class="check-group">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" title="Obtain guidance or tips & trick from the experts" data-toggle="tooltip" data-placement="top" name="coaching_ci_type" type="radio" value="coaching" checked="" id="coaching_ci_type" onclick="myFunction()"> Coaching
                                                <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" title="Search for solution and enhance your problem solving skill within yourself" data-toggle="tooltip" data-placement="top" name="coaching_ci_type" type="radio" value="mentoring" id="coaching_ci_type" onclick="myFunction()"> Mentoring
                                                <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <div class="form-group reset">
                                    @php /*<label class="control-label">Employee {{ ($formData['type']=='Assignment')?'(Asignee)': '(Coachee/Mentee)' }} NIK</label>*/ @endphp
                                    @if ($helper->inAdmin())
                                        <label class="control-label">Employee ID</label>
                                    @else
                                        <label class="control-label">My Employee ID</label>
                                    @endif
                                    <input name="coaching_ci_employee_nik" type="text" class="form-control nik_coachee2" placeholder="Example: 68025191" value="{{$employee['staff'] ? $employee['staff']['nik'] : ''}}" readonly required>
                                </div>

                                <div class="form-group reset">
                                    <label class="control-label">Coach/Mentor Name</label>
                                    <select class="form-control select-employee">

                                    </select>
                                    <input name="coaching_ci_line_manager_name" type="hidden" class="form-control select-employee-name" placeholder="enter name"  required>
                                    @if ($errors->has('coaching_ci_line_manager_name'))
                                        <div class="validation-msg">
                                            Coach/Mentor Name is Required
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group reset">
                                    <label class="control-label">Coach/Mentor Employee ID</label>
                                    <input name="coaching_ci_line_manager_nik" type="text" id="nik_coachee" class="form-control nik_coachee" placeholder="Example: 68025191" readonly required>
                                </div>

                                <div class="form-group reset">
                                    <label class="control-label">Coaching/Mentoring Date</label>
                                    <div class="field-date">
                                        <div class="date-input">
                                            <input name="coaching_ci_start_date" type="text" autocomplete="off" class="form-control start-date" placeholder="Enter start date">
                                            <button type="button" class="btn btn-default btn-icon "><i class="mdi mdi-calendar"></i></button>
                                        </div>

                                        To

                                        <div class="date-input">
                                            <input name="coaching_ci_end_date" type="text" autocomplete="off" class="form-control end-date" placeholder="Enter end date">
                                            <button type="button" class="btn btn-default btn-icon"><i class="mdi mdi-calendar"></i></button>
                                        </div>
                                        @if ($errors->has('coaching_ci_start_date') || $errors->has('coaching_ci_end_date'))
                                            <div class="validation-msg">
                                                Coaching/Mentoring Date is Required
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            <div class="field-left d-none d-xl-block" id="fl-lef">
                                <figure>
                                    <img src="{{ url('images/vector1.png') }}" alt="ooredoo" class="img-fluid">
                                </figure>
                            </div>
                        </div>
                        <div class="bottom-step">
                            <button type="button" class="btn btn-default" onclick="location.href='{{ url('trainee/request') }}'"><i class="icon-close"></i> Cancel</button>
                            <div class="steps-count">
                            	<div class="step-item active">
                            		<span class="circle-number">
                            			<em>1</em>
                            			<i class="icon-verified"></i>
                            		</span>
                            		My Information
                            	</div>
                            	<div class="step-item">
                            		<span class="circle-number">
                            			<em>2</em>
                            			<i class="icon-verified"></i>
                            		</span>
                            		Competency and topic
                            	</div>

                            	<div class="step-item">
                            		<span class="circle-number">
                            			<em>3</em>
                            			<i class="icon-verified"></i>
                            		</span>
                            		Completed
                            	</div>

                            	<div class="step-item counter">
                          			<p class="mb-0 text-white">Step <span class="text-white">1</span> / 3</p>
                          		</div>
                            </div>
                            <button form="submit_step" type="submit" class="btn btn-default btn_block" >Next <i class="icon-next"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>

<!-- new -->
<script>
    var delayInMilliseconds = 3000;

    setTimeout(function() {
        var a = document.getElementById('fl-lef');
        a.className += ' class_two';
        a.style.display = 'block';
    
        // a.classList.remove('d-none');
    }, delayInMilliseconds);



    function myFunction() {


            let header_coaching = $('.header-coaching'),
                header_mentoring = $('.header-mentoring');
              

        var checkbox = $("#coaching_ci_type:checked").val();

         if(checkbox == 'coaching') {
                header_mentoring.addClass('d-none');

                header_coaching.removeClass('d-none');
                
            } else {
                header_coaching.addClass('d-none');

                header_mentoring.removeClass('d-none');
                
            }

       

    }


</script>
<!-- end new -->
<br><br><br>
@endsection
