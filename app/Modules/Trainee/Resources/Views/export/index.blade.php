@php($user = \Sentinel::check())
@php($helper = new \App\Helpers\UserRolesHelper )
@extends('core.export')
@section('content')

    <main class="main-content">

        <div class="container">
            @if($form_detail['form_group_id'] == 1)
                {{--assignment form type--}}
                @include('trainee::export.assignment')
            @elseif( $form_detail['form_group_id'] == 2)
                {{--mentoring form type--}}
                @include('trainee::export.mentoring')
            @elseif($form_detail['form_group_id'] == 3)
                {{--public training speaker form type--}}
                @include('trainee::export.training')
            @elseif($form_detail['form_group_id'] == 4)
                {{--external speaker form type--}}
                @include('trainee::export.external')
            @elseif($form_detail['form_group_id'] == 5)
                {{--Inhouse form type--}}
                @include('trainee::export.inhouse')
            @endif
        </div>
    </main>
