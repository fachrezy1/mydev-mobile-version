<style>
    @page { margin: 0px; }
    body { margin: 0px; }
    tr{
        height: 50px;
        page-break-before: auto;
    }
    body{
        background: white !important;
    }
    .text-center{
        text-align: center !important;
    }
    .center-form{
        margin: 0 auto !important;
        margin-top: 20px !important;
        max-width: 90% !important;
        width: 100% !important;
    }
    .text-right{
        text-align: right !important;
    }
</style>
{{--@dd($form_detail)--}}
<div class="review-item">
    <div class="text-right" style="width: 100%;">
        <img style="margin-right: 80px;margin-top: 20px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKcAAABaCAYAAADdGkYrAAAPA0lEQVR4nO1dC5AUxRn+Z3Z3Zu/Fwd0Bd3ILRnyC7xiiqER8pEQIRhMjxDKaWEpiTPmgolGiBilQMSSFpVFSqJFETFmgKKgxSKFigRpjNApBxBe7cHDv1x47u3s7qdm7PWbn2d3TPTs3zFd1Vbcz09//9///0/P3Y3o48BhScfESjuPO7Je5FeWxg3u9pl8A9+CJ4JTiomx3jRiTPHcjBWCLkjocJSi1CIL08EFJHC3FxQwAhEnLBwF6eMB1J5O0lkYIAtT/IG69SCAlhCeBSmgOBHkQoN6EHIeaNIhtZspxAM1CTBprp7yrzqXVaqoRBKh3IMXF9QAwC0chK/+55lgWgQlBcHoGDv2bE2NSSHtw2AcnIASoRQdsgxiTvsdKr8MBqYR4LCfDpzSqqvWjr4MTR6YXW2ApLq4BgB8YnPpQjEmnlUAlHSj7tagF9WVwOpHlhSDF0V/ghDqusce088ESLHwq83B8dJyUb4l5NyqRTkROcUMOUDAYy5sIRTau/LScbi2FzqxkcjnYWfjfleB0C7QMNhydXcqbihV8E5y0nZOKi8fQ5LPCcL6pWKBQD6RB+PQe8RcyB38yO+/H4RwOYBdKTi7Fxe0AMMng1BdiTJqIUL6TWEljPqTJCftA5q4WY6m/mZTtJtcQHZaVkGXg0wmxH5XNzChKzinL/IfMasEIVk6mNRLg9sQErjwjLjdaaEWu6WNdUQAnMMGHeY9RfUg6LSRlnMBIViohPkqig9u6q2EYnE6U8WNiXgDtTotbtkrFxWZOhhudcJTCr7rgpKGEHwN0uHZa5PchwgGMpsHltu5FwSl/DtW0iP0UoLTr4qZt0mPFtFuyaKOot54W6PYclWDnJkJXKStoh+OnHLofd77naVWxIcWjq4HWGsVBuHljDbWcLITSDvZSgJUz3HGyPJe9DHMoN37h7+138JcOMx+ET+0RH2ctI4AeUlzcM9zNwjw4OQ7msZYRwBCx4W6WfHD6efgnwPCFrxZ+BPAXsLNUde8WEHu4co5fZDdL/ePrK+GDj3Qr9an1oLV6Tz6+H9au6mXCDZT0PmnqCMhk9Yabc3kafvebg475jfS+4VoJbrsx5Zj7osuqin7feV85REUZNr7Qg8yRrznOY50kOHE5tfh4azdEwmSZhx23Grh1seMOhwE+2Upmn5PPHgHpjPUdTWp7FJs49auZDFTe/Nx6Oh6Z4kgLh0AxlNKCvLoxgi0IJzBZcGezAJPOJNNBHZihEMCOd9g8QZxexxJ8juOpTG2RQGuApQv78neW8vfG+uJVWbcuKMeScMGlVbpjBW6nrcJ9S8uQuXM5R6Ly2L6tC3geivhppTtqvZ96JFl0zkmAavVb+XASW2fPdIgUxWfPyAz9rh8r6yrz4PIoMt/epkNVe/T3fTouJ85dvUYY+l/J/+y4nbZCu78ozsVpPc61PGdNyXpqlqxkwYnqsMeX9Q39/9QzIpGsC6ZlDI/TcIRZx4Smk2fNqRyaabntLn2rTYLhMFXriZZz2WLznud55xoHlhU2vCZgl/EazILnldeFfJD+52P9yIbf4IngrCyjkJipcOIJWap8pUIhFxzXoLfP3OsqfVFHK/hyEP7I8WjBTtqTRoHR+CQpNr3YY9jZcpLLut0bz2G9UzGAw2KGyMgRfX1cUU9a6RHT4obB4a8COII4VXifXatPT5zkitqyRpMerIL2hlsrsMu4ugWim1AcoTa0ndFDGCkcLvf/3sULqBlXDAyDLXywLP8HqsCi2dors3Is8dcVvXD1vEMyFDu9trYHJsTQnmy+bjlRp/hIWiPUMiTcLz2rn1Yt9NbVrT1LvWngW6fpn+UrV6GPuPg6OJUxyG3/tH7FmuZjkha3MlVrV3bScQRJ3CAU7uoq8+ngJff0mZ4jkUUK3z7WCxg18pCjX3o1Att3huDOW50vbCigwN3SysGqv4sw/yb63AqU/HNfE0+N/91N1jft5bPotbCFevz7oxAk9qK3h1wqIc7kZNhATZMAASjAclOFAAFKjSA4A3gW2Dln10/qIbt7YPytamkLCOc4X/RaQNvU8UW/qxa1gnCB8+Q8868odN88Rne8diudd8CSD9VA6oVDQybijCRU3k1vP9e+FdWQWj0C+PEZqH64GbhR9GbUOi4dB7mWgXE08eI+qLynlRq3zp8PtIIwDd2ftjmn3M5D+6xGJDISZ2srYIbqlQcgPEliws2FZah5K47F3XFJI+Q67R88nChDzWY8bkDQvWHPLpB7eyHTWYdHLAO0nY1mF5b+HPncPgg1mk8zKzmnZXCiClKDK89BzesJ2+tIuAHRYKTcwvQ+qFps3XK0z2wEuQM/G+IiMtS8aR+kqLorwalGOmG/nNCLNjfjNu0QKQ4gFSb38bZlSbkLZXsWmLcWTrjTm8styyvnSAJTgZzhLLlR7GYFq7KpNVXMuFHO25VNLqk1PKeztBMHaHlwjuNACaKOmfpUgwa3GQ9T7mkxaL8QLXXC5e762VhI/mEUE26r4zhIbaiAjtnjdCWKopBGJawUp+VgBTnlBuo/tKKCJreWjyV3ZlsZAMUVTEW6ygDZnWQLtG25afuzNVTkT9AGp9L800ahArQdnOc8N4bFLUw5A2pW/wW4Kvr1VFBx3bXoK0gGHdE9n/4rXG3nDNocseODxe2CPwsY6hDZCePKyqD+048MzzWNP9ayrJL0ovQ+jZD98ito+c53LcvawYwbRfdSclf+ch5U3TEf6VoSWWa6s/Rn5y2/hoPPv2jLX9RbtxJm5QA1aBqIBjeqc0mDCEX3nmXLoXf5o0y41cCpAwp3f2IvNE+djqWDgrp1z0Hk9FNtr7PStxCc+cc6jcDMg8frSJVf+UPka3GdpQC11SHh1pZRjF34K9Jh/s2OuWnC7OmnRahR30FBAUpg2tUxtW5gQgN7hkhrfLWQhq92Yt3B1Q8tQebGhVHwqBGZPAnqXl1XJAtVdzW3nMnA/omTdbLqXloDkVNPxuZW9NJCXXbMts0QGkcWODCYnml1LSBUPxbGvLdl6DeO3mBk8wnH5T/JYnbeDMmlNUqTaT23budgw2OI7ySEj5yAzU0arE1HnaA7ltm+Aw6ceAYRnxpcJJLXS/tXCExcqG8YMLBB81nToeXCmUTcdv7s33+A2J9a5Hnk4jWjuP7kUy9XId2GySefRlKq4Wu0rxuPfmsj0nVtl89Bus4SWeOFubluV771lAdfhznNaJGXZXd95lifXCva3D+qP5UWHQX51hQRfPr58vNRLu3+3WJkUppIv/8BNlv5j4y+Am0Ms1yRNkbce1dJ7GeGA6efZXpOPoi/mAc51ZDRNmRrajz6vLAwJf1Bdod4pd3FnCiCLOEtvKCByCknuS4TB6yDmhW4aBTklPGqem1eWgo0JHa/wUfndWxHkV3/2cdIKnbdvRDpupbzZyBdV7d+LdJ1XoQ6DyXBmC2vI5UiuUHqd/0X6TrpzS0IV6HrgGMLyw4RSgKrPdb39DNIgrO7P8fm9mIrZWZsGsNBoQnjofpBfTpF3DHU2K9+9ye23O1XX0cky0hH7TBW64zvW3JgDyVZGQb3sd993/0w4p47kbi9BMXJRUNoNnrj3FRa7vK5V+T/aHBrwQkCVZt3L1kKI+66fei3HbcyYmKEigVtAD8fbDmt1uvhVH7/MXj5YXLlU8jXeq3VRNWHRG+WdcW9UXCQfHwlFe7ozIF9QpGmdOyUlDZttp2OsuK2KptrbvFsp8NOLyd625VV0idSfpRyTvzZevFsx/IVcNLXwmTguXzygbrSZNRjy6H/QDPy8BLKQgFSKNzKWkhlsS4LbmC0AqdmUwK4shwSd9nsmTDykT9Cf9N+aP72NCR+VJuHJx4Fozf/I/+/MtOlzHjR4lZQfs1V+VmvrtsXIF1fsHl+4QeoPljAwgnqu4w2P0vu6NxuqPhVJxNuYBz4BW5pYwX03mu8ytwpN7Dw55Y9AIMrDnWvadB6G9GMjyZ/+Ojij+HS1r0QmCy41XwsucWLkpbXOuE2+u0E/BHZocAsQPcspCXQjIcGf9lPu6B61X4m3COfaTLkYWkX5tyEn8mx47Y6joPonG4YtWafroTpd4j6nqiGg0+QbbdXirf13OImzm95gNq3rfmTi2sh9TL+PpbgQ5sP5Zxg8aEsHKEs33Me9UoC+JF4mwm0X9QIctI+kEj0bp8eA1lCWLGDEJQ67mkxkBHfK8LVveeO0ZDegjY9acYtRKUR6ZRouGoG1Z816/cCV2u+Ux5ScKqR2xuGjiuOGDoSvbIHKm7usCwjNEqhdEJE268vy0HvAzX5HSjKb+qE8DFphEJ4yHXxwFfT3YNeQe/CWshsLYPIlINQuQhtxY/iADu7938Zgc6rGoZ+hyZkYOSzTc4VHkT3LWMg897AO+/l87qg7Br73eVQ9FaQ6+UheX8N5PaFofz6LohMRV9Qog1OZZ8Q6jP+qBU5HDFcbcNab4Uf1B0iMSbhfSINQ0gAS1B3sht2d0NGUULGSiDHwQEWvH6AGJOY7PQnA/x5OJpHHYPMtkBUCxEapXpWcvwAmo1CgSsak+axNg3txkxolIq+vqsLThoC3XyccwBvuCWLJYab3WnLFEJSBcdB0bZzhi3noECiXMhtAwkxCf/lagywqo8RrwNZslt25zhYpj3mVHa+Z34E6DbutCXF6JXJdvkTgx7e52JMOpp1z5EFv51DEWW6bnNKeuchhCNjuIbeFrPzyBFvJlTJE7TNsRnSiegNsiyvQJVph4Kh3BjWoCmDpKVJfx35Zn8u1Fb2jdRXuGVp6Y6rt5SILgJZ/i0ph+s5CgtDuRGcsgw88mQCAp+bkOKi8oqCo08pC40Sz3H0h72s4PoHCwad42iKphQO5jjI0cit6GmEJVd0IjufE7ocmFCqr2mIMSlEYiwhK5WVemB/QL58B04ZoVEKe2FCgkCHHaXU2xMzOHaPZWV+Xmm5SMsTQ5YvE8en11kVt5Lt5Rmy1B7xMY5TXiPTQwCplotBe4lV9EZwOgWr4AymX0sLX3wkKwgifyL4gpsJgoAvPXwTnEEw+Q++cyiN/DMIdG/Ad491h4GVCgLTO/Blzpl/5znHn4tbRoxJpd/7L8AQDotWQoqL6wFglvZ40Ep6GADwf/jA2LIGQOTSAAAAAElFTkSuQmCC">
    </div>
    <div class="text-center">
        <h3  style="color: red"> {{str_replace('_',' ',ucwords($form_type))}} Form </h3>
    </div>
    <div class="center-form" style=>
        <table style="width:100%;" border="1" rules="none">
            <tr style="background: #f9e340;height: 80px">
                <td colspan="4" height="20" style="padding-left: 10px"><b>1. Training Information</b></td>
            </tr>
            <tr>
                <td style="width: 25%;padding: 10px;"> <b>Training Title</b></td>
                <td style="width: 5%;border: none;">:</td>
                <td style="width: 80%;padding-right: 10px;">{{$form_detail['title']}}</td>
                <td style="padding-right: 10px;border-right: 1px solid;"></td>
            </tr>
            <tr>
                <td style="padding: 10px;"><b>Type of Programs</b></td>
                <td style="border: none;">:</td>
                <td style="padding-right: 10px;">{{ucwords($form_detail['type_programs'])}}</td>
                <td style="padding-right: 10px;border-right: 1px solid;"></td>
            </tr>
            @if($form_detail['type_programs'] == 'internal trainer')

                <tr>
                    <td style="padding: 10px;"><b>Trainer Name</b></td>
                    <td style="border: none;">:</td>
                    <td style="padding-right: 10px;">{{$form_detail['trainer_name']}}</td>
                    <td style="padding-right: 10px;border-right: 1px solid;"></td>
                </tr>
                <tr>
                    <td style="padding: 10px;"><b>Trainer NIK</b></td>
                    <td style="border: none;">:</td>
                    <td style="padding-right: 10px;">{{$form_detail['trainer_nik']}}</td>
                    <td style="padding-right: 10px;border-right: 1px solid;"></td>
                </tr>

            






            @else
                <tr>
                    <td style="padding: 10px;"><b>Training Provider</b></td>
                    <td style="border: none;">:</td>
                    <td style="padding-right: 10px;">{{$form_detail['provider']}}</td>
                    <td style="padding-right: 10px;border-right: 1px solid;"></td>
                </tr>
            @endif

            
           @if($moderator_name !='notfound')
            <tr>
                    <td style="padding: 10px;"><b>Moderator Name</b></td>
                    <td style="border: none;">:</td>
                    <td style="padding-right: 10px;">{{$form_detail['moderator_name']}}</td>
                    <td style="padding-right: 10px;border-right: 1px solid;"></td>
                </tr>
                <tr>
                    <td style="padding: 10px;"><b>Moderator NIK</b></td>
                    <td style="border: none;">:</td>
                    <td style="padding-right: 10px;">{{$form_detail['moderator_nik']}}</td>
                    <td style="padding-right: 10px;border-right: 1px solid;"></td>
                </tr>
        @endif
            <tr>
                <td style="padding: 10px;"><b>Cost</b></td>
                <td style="border: none;">:</td>
                <td style="padding-right: 10px;">Rp. {{$form_detail['cost']}}</td>
                <td style="padding-right: 10px;border-right: 1px solid;"></td>
            </tr>
            <tr>
                <td style="padding: 10px;"><b>Total Hours</b></td>
                <td style="border: none;">:</td>
                <td style="padding-right: 10px;">{{isset($form_detail['hours']) ? $form_detail['hours'].' Hour(s)' : 'not filled yet'}}</td>
                <td style="padding-right: 10px;border-right: 1px solid;"></td>
            </tr>
            <tr>
                <td style="padding: 10px;"><b>Start Date</b></td>
                <td style="border: none;">:</td>
                <td style="padding-right: 10px;">{{$form_detail['start_date']}}</td>
                <td style="padding-right: 10px;border-right: 1px solid;"></td>
            </tr>
            <tr>
                <td style="padding: 10px;"><b>End Date</b></td>
                <td style="border: none;">:</td>
                <td style="padding-right: 10px;">{{$form_detail['end_date']}}</td>
                <td style="padding-right: 10px;border-right: 1px solid;"></td>
            </tr>
            <tr>
                <td style="padding: 10px;"><b>Training Objective</b></td>
                <td style="border: none;">:</td>
                <td style="padding-right: 10px;">{{$form_detail['description']}}</td>
                <td style="padding-right: 10px;border-right: 1px solid;"></td>
            </tr>
            <tr>
                <td style="padding: 10px;"><b>Venue</b></td>
                <td style="border: none;">:</td>
                <td style="padding-right: 10px;"></td>
                <td style="padding-right: 10px;border-right: 1px solid;"></td>
            </tr>
        </table>
        <table style="width:100%;margin-top: 20px;" border="1" rules="none">
            <tr style="background: #f9e340;height: 80px">
                <td colspan="4" height="20" style="padding-left: 10px"><b>2. Employee Information</b></td>
            </tr>
            <tr>
                <td style="padding: 10px;"> <b>Employee Name</b></td>
                <td style="border: none;"><b>Employee NIK</b></td>
                <td style="padding-right: 10px;"><b>Employee Position</b></td>
                <td style="padding-right: 10px;border-right: 1px solid;"></td>
            </tr>
            @php
                $employee_nik = unserialize($form_detail['employee_nik']);
                $employee_position = unserialize($form_detail['employee_position']);
            @endphp
            @foreach(unserialize($form_detail['employee_name']) as $key=>$employee_name)
                <tr>
                    <td style="width: 33.33%;padding: 10px;">{{$employee_name}}</td>
                    <td style="width: 33.33%;border: none;">{{$employee_nik[$key]}}</td>
                    <td style="width: 33.33%;padding-right: 10px;">{{isset($employee_position[$key]) ? $employee_position[$key] : '-' }}</td>
                    <td style="padding-right: 10px;border-right: 1px solid;"></td>
                </tr>
            @endforeach

        </table>
        <table style="width:100%;margin-top: 20px;" border="1" rules="none">
            <tr style="background: #f9e340;height: 80px">
                <td height="20" colspan="4" style="padding-left: 10px;"><b>3. Competency to be Developed</b></td>
            </tr>
            <tr>
                <td colspan="4" style="width: 100%;padding-right: 10px;padding-left: 10px;"> {{implode(",",unserialize($form_detail['employee_competency']))}}</td>
            </tr>
        </table>
    </div>

</div>
