@extends('core.trainee.main')

@section('content')
<main class="main-content">
    <section class="trainee-block center">
        <div class="container">
            <div class="trainee-title">
                <h4>Hooray ! Indosat Ooredoo is supporting my optimum development !</h4>
                <h1>I'am Here For Requesting</h1>
            </div>

						{{-- basic --}}
            <div class="trainee-box">
                @foreach($trainees as $trainee)
                    <div class="item-card">
                        <div class="item-card-top">
                            <i class="{{ $trainee->icon }}"></i>
                        </div>
                        <div class="item-card-description">
                            <span class="circle-badge">
                                <i class="icon-note-1"></i>
                            </span>
                             <em class="trainee-text">To join</em>
                            <span class="small-title">{{ $trainee->short_description }}</span>
                            <h3>{{ $trainee->name }}</h3>

                            <span class="item-card-ext">
                                <p>
                                    {{ $trainee->description }}
                                </p>
                                <button type="button" class="btn btn-default" onclick="location.href='{{ url($trainee->slug) }}'">Select</button>
                            </span>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
</main>
@endsection

@section('styles')
{{-- Slider --}}
<link href="{{url('js/lib/slick/slick.css')}}" type="text/css" rel="stylesheet"/>
<link href="{{url('js/lib/slick/slick-theme.css')}}" type="text/css" rel="stylesheet"/>
<style>

	@media (min-width: 768px) {
		.main-content {
			padding-top: 60px
		}
	}
	.main-header.clean {
		background-color: #fff;
	}

	.slick-prev {
		left: 10px;
		z-index: 100;
		width: 40px;
		height: 40px;
	}
	.slick-next {
		right: 10px;
		z-index: 100;
		width: 40px;
		height: 40px;
	}
	.slick-prev:before, .slick-next:before {
		font-size: 40px;
	}
</style>
@endsection


@section('scripts')
	{{-- Slider --}}
	<script src="{{ url('js/lib/slick/slick.min.js') }}"></script>
	
    <script type="text/javascript">
        jQuery(function($){
            $('#traineeDetail').on('shown.bs.modal', function (e) {
                $('.scroll-content').slimScroll({
                    height: '330px',
                    size: '5px',
                    position: 'right',
                    color: '#fcd401',
                    alwaysVisible: true,
                    distance: '0px',

                    railVisible: true,
                    railColor: '#222',
                    railOpacity: 0.1,

                    allowPageScroll: true,
                    disableFadeOut: false
                });
            });

            $(".trainee-option").owlCarousel({
                items: 4,
                loop: false,
                margin: 10,
                nav: true,
                dots: false,
                smartSpeed: 900,
                autoplay: true,
                navigationText: ["<i class='mdi mdi-chevron-left'></i>", "<i class='mdi mdi-chevron-right'></i>"],
                responsive: {
                    0: {
                        items: 2
                    },
                    990: {
                        items: 3
                    },
                    1023: {
                        items: 5
                    }
                }
            });
        });

        $(document).ready(function() {
		    	$('#slide.trainee-box').slick({
		    		autoplay: false,
		    		infinite: false,
					  slidesToShow: 2,
					  slidesToScroll: 1,
					  responsive: [
					    {
					      breakpoint: 1024,
					      settings: {
					        slidesToShow: 2,
					        slidesToScroll: 1,
					      }
					    },
					    {
					      breakpoint: 600,
					      settings: {
					        slidesToShow: 1,
					        slidesToScroll: 1
					      }
					    },
					  ]
		    	})
		    });

    </script>
@endsection
