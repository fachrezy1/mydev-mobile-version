@php($user = \Sentinel::check())
@php($helper = new \App\Helpers\UserRolesHelper )
@php($agent = new \Jenssegers\Agent\Agent())
@extends($helper->inAdmin() ? 'core.admin.main' :'core.trainee.main')
@section('head')
    <link rel="stylesheet" href="{{url('css/admin/dashboard/app.css')}}">
@endsection
@section('content')
    {{--@dd($agent->isMobile())--}}
    <main class="main-content">
        <div class="main-dashboard">
            <div class="dashboard-content w-100">
                <div class="container-fluid">

                    <div class="dashboard-entry">
                        
                        <div class="dashboard-title">
                            <h1></h1>
                        </div>

                        <i class="icon-exam"></i> You have <span>{{$requests['approvedCount']}} courses</span>
                        to review. <a href="#myApproval" class="customTabToggler" >Click here</a>

                        @if($agent->isMobile())
                            <ul class="nav nav-pills nav-pills-icons mt-24" role="tablist">
                                <li class="nav-item col-12 inline">
                                    <a class="nav-link active show" href="#myTrainee" role="tab" data-toggle="tab" aria-selected="true">
                                        <i class="icon-portfolio-1"></i> Waiting for approval
                                    </a>
                                </li>

                                @if ($helper->inStaff())
                                    <li class="nav-item col-12 inline">
                                        <a class="nav-link" href="#myApproval" role="tab" data-toggle="tab" aria-selected="false">
                                            <i class="icon-checklist"></i> Ongoing Progress
                                        </a>
                                    </li>
                                @endif
                                <li class="nav-item col-12 inline">
                                    <a class="nav-link" href="#myHistory" role="tab" data-toggle="tab" aria-selected="false">
                                        <i class="icon-profiles"></i> Completed
                                    </a>
                                </li>
                                @if ($helper->inStaff())

                                    <li class="nav-item col-12 inline">
                                        <a class="nav-link" href="#rejected" role="tab" data-toggle="tab" aria-selected="false">
                                            <i class="icon-profiles"></i> Rejected
                                        </a>
                                    </li>
                                @endif
                                @if (!$helper->inAdmin())
                                <li class="nav-item col-12 inline">
                                    <a class="nav-link" href="#related" role="tab" data-toggle="tab" aria-selected="false">
                                        <i class="icon-profiles"></i> My Trainee
                                    </a>
                                </li>
                                @endif
                            </ul>
                        @else
                            <ul class="nav nav-pills nav-pills-icons mt-24" role="tablist">
                                <li class="nav-item inline">
                                    <a class="nav-link active show" href="#myTrainee" role="tab" data-toggle="tab" aria-selected="true">
                                        <i class="icon-portfolio-1"></i> Waiting for approval
                                    </a>
                                </li>

                                @if ($helper->inStaff())
                                    <li class="nav-item inline">
                                        <a class="nav-link" href="#myApproval" role="tab" data-toggle="tab" aria-selected="false">
                                            <i class="icon-checklist"></i> Ongoing Progress
                                        </a>
                                    </li>
                                @endif
                                <li class="nav-item inline">
                                    <a class="nav-link" href="#myHistory" role="tab" data-toggle="tab" aria-selected="false">
                                        <i class="icon-contract"></i> Completed
                                    </a>
                                </li>
                                @if ($helper->inStaff())

                                    <li class="nav-item inline">
                                        <a class="nav-link" href="#rejected" role="tab" data-toggle="tab" aria-selected="false">
                                            <i class="icon-exit"></i> Rejected
                                        </a>
                                    </li>
                                @endif
                                @if (!$helper->inAdmin())
                                    <li class="nav-item inline" style="border: 1px dashed #ed1b24;">
                                        <a class="nav-link" href="#related" role="tab" data-toggle="tab" aria-selected="false">
                                            <i class="icon-presentation"></i> My Trainee
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        @endif
                        <div class="tab-content tab-space">
                            <div class="tab-pane active show" id="myTrainee">
                                <div class="panel-box mt-24">
                                    <div class="panel-title">
                                        <h3>Waiting for approval List</h3>
                                        <div class="right-notif">
                                            <div class="review-notif">
                                            </div>

                                            @if ($helper->inStaff())

                                                <a href="{{url(route('trainee.index'))}}">
                                                    <button type="button" class="btn btn-default">
                                                        <i class="icon-plus"></i> Create Request
                                                    </button>
                                                </a>
                                            @endif

                                            @if ($helper->inAdmin())
                                                <a href="{{url(route('trainee.index'))}}">
                                                    <button type="button" class="btn btn-default">
                                                        <i class="icon-plus"></i> Create Trainee
                                                    </button>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="panel-table">
                                        <div class="export-table export-mytrainee">
                                            <select id="filterMonthWaitingApproval" class="form-control select-filter">
                                                <option value="">Filter Month</option>
                                                @for($i=1;$i<=12;$i++)
                                                    <option value="month-{{ date('m', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}">{{ date('F', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}</option>
                                                @endfor
                                            </select>

                                            <select id="filterYearWaitingApproval" class="form-control select-filter">
                                                <option value="">Filter Year</option>
                                                @for($i=0;$i<=3;$i++)
                                                    <option value="year-{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                                @endfor
                                            </select>

                                            <a href="{{route('trainee.exportTrainee')}}?type=new" id="exportWaitingApproval" class="btn btn-default btn-sm btn-lite">
                                                <i class="icon-xlsx-file-format"></i> Excel
                                            </a>
                                        </div>
                                        <div class="clearfix"></div>
                                        
                                        <table class="traineeWaitingApprovalTable table table-hover table-striped table-responsive table-lite text-center" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th width="10px">No</th>
                                                <th>Title/Topic</th>
                                                <th width="50px">Type</th>
                                                <th>Employee Name</th>
                                                <th>Line Manager</th>
                                                <th width="40px">Start</th>
                                                <th width="40px">End</th>
                                                @if ($helper->inStaff())
                                                    <th>Manager</th>
                                                    <th>HR.</th>
                                                @endif
                                                <th>Type of programs</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @php ($n=1)
                                            @if(isset($requests['new']) && !is_null($requests['new']))
                                                @foreach($requests['new'] as $key=>$item)
                                                    <tr>
                                                        <td>{{$n}}</td>
                                                        <td>
                                                            @if($item['form_group_id'] == 1)
                                                                @if($item['type']=='project')
                                                                    {{ isset($item['project_title']) ? limit_words($item['project_title'], 19) : ''}}
                                                                @else
                                                                    {{ isset($item['development_goal']) ? limit_words($item['development_goal'], 19) : ''}}
                                                                @endif
                                                            @elseif($item['form_group_id'] == 4)
                                                                {{ isset($item['line_topic']) ? limit_words($item['line_topic'], 19) : ''}}
                                                            @elseif($item['form_group_id'] == 3 )
                                                                {{ isset($item['title']) ? limit_words($item['title'], 19) : ''}}
                                                            @elseif($item['form_group_id'] == 2)
                                                                {{ isset($item['development_goal']) ? limit_words($item['development_goal'], 19) : ''}}
                                                            @endif
                                                        </td>
                                                        <td>{{ucfirst(str_replace('_',' ',$form_type[$item['form_group_id']]))}}</td>
                                                        <td>{{$item['employee_name'] }}</td>
                                                        <td>{{isset($item['manag']) ? $item['manag'] : ''}}</td>
                                                        <td>
                                                            {{isset($item['start_date']) ? $item['start_date'] : ''}}
                                                            @if(isset($item['start_date']))
                                                                <div class="hidden"> month-{{ date('m', strtotime(str_replace('/', '-', $item['start_date']))) }}</div>
                                                            @endif

                                                            @if(isset($item['end_date']))
                                                                <div class="hidden"> year-{{ date('Y', strtotime(str_replace('/', '-', $item['start_date']))) }}</div>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            {{isset($item['end_date']) ? $item['end_date'] : ''}}
                                                        </td>

                                                        @if ($helper->inStaff())
                                                            <td>
                                                                    <span class="badge badge-pill {{ isset($item['first_accepter_class']) ? $item['first_accepter_class'] : ''}}">
                                                            {{ isset($item['first_accepter']) ? $item['first_accepter'] : ''}}</span>
                                                            </td>
                                                            <td>
                                                                    <span class="badge badge-pill {{ isset($item['second_accepter_class']) ? $item['second_accepter_class'] : ''}}">
                                                            {{ isset($item['second_accepter']) ? $item['second_accepter'] : ''}}</span>
                                                            </td>
                                                        @endif
                                                        <td> {{isset($item['type_programs']) ? strtr($item['type_programs'],["shortterm"=>"Global Talent Mobility / National Talent Mobility","longterm"=>"Global Talent Mobility / National Talent Mobility"]) : ( isset($item['type']) ? strtr($item['type'],["shortterm"=>"Global Talent Mobility / National Talent Mobility","longterm"=>"Global Talent Mobility / National Talent Mobility"]) : '')}} </td>
                                                        <td >
                                                            <div class="table-option">
                                                                <div class="hidden">{{ $item['request_group_id'] }}</div>

                                                                <a href="{{route('trainee.export',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}" target="_blank">
                                                                    <button type="button" class="btn btn-default btn-sm btn-lite">
                                                                        <i class="icon-transfer-data-between-documents"></i> Export
                                                                    </button>
                                                                </a>
                                                                @if($item['is_accepted'] == 1)
                                                                    <a href="{{route('trainee.review',$item['request_group_id'])}}">
                                                                        <button type="button" class="btn btn-default btn-sm btn-lite">
                                                                            <i class="icon-edit-1"></i> Review
                                                                        </button>
                                                                    </a>
                                                                @endif
                                                                <a href="{{route('trainee.previewByUserId',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}">
                                                                    <button type="button" class="btn btn-default btn-sm btn-lite">
                                                                        <i class="icon-contract-1"></i> Preview
                                                                    </button>
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @php($n=$n+1)
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @if ($helper->inStaff())
                                <div class="tab-pane" id="myApproval">
                                    <div class="panel-box mt-24">
                                        <div class="panel-title">
                                            <h3>Ongoing Progress List</h3>
                                            <div class="right-notif">
                                                @if ((isset($requests['approved'])) && $helper->inStaff() && ['approvedCount'] > 0)
                                                    <div class="review-notif">
                                                        <i class="icon-exam"></i> You have got

                                                        <span>{{$requests['approvedCount']}} course(s)</span>
                                                        need to add review. <a href="#myApproval" class="customTabToggler" >Click here</a>
                                                    </div>
                                                @endif

                                                @if ($helper->inStaff())

                                                        <a href="{{url(route('trainee.index'))}}">
                                                            <button type="button" class="btn btn-default">
                                                                <i class="icon-plus"></i> Create Request
                                                            </button>
                                                        </a>
                                                @endif

                                                @if ($helper->inAdmin())
                                                    <a href="{{url(route('trainee.index'))}}">
                                                        <button type="button" class="btn btn-default" onclick="location.href='{{url(route('trainee.index'))}}'">
                                                            <i class="icon-plus"></i> Create Trainee
                                                        </button>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="panel-table">
                                            <div class="export-table export-mytrainee">
                                                <select id="filterMonthOngoing" class="form-control select-filter">
                                                    <option value="">Filter Month</option>
                                                    @for($i=1;$i<=12;$i++)
                                                        <option value="month-{{ date('m', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}">{{ date('F', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}</option>
                                                    @endfor
                                                </select>

                                                <select id="filterYearOngoing" class="form-control select-filter">
                                                    <option value="">Filter Year</option>
                                                    @for($i=0;$i<=3;$i++)
                                                        <option value="year-{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                                    @endfor
                                                </select>

                                                <a href="{{route('trainee.exportTrainee')}}?type=approved" id="exportOngoing" class="btn btn-default btn-sm btn-lite">
                                                    <i class="icon-xlsx-file-format"></i> Excel
                                                </a>
                                            </div>
                                            <div class="clearfix"></div>

                                            <table class="traineeOngoingTable table table-hover table-striped table-responsive table-lite text-center" cellspacing="0" width="100%">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Title/Topic</th>
                                                    <th>Type</th>
                                                    <th>Employee Name</th>
                                                    <th>Line Manager</th>
                                                    <th>Start</th>
                                                    <th>End</th>
                                                    <th>Manager</th>
                                                    <th>HR.</th>
                                                    <th> Type of programs </th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                @php ($n=1)
                                                @if(isset($requests['approved']) && !is_null($requests['approved']))
                                                    @foreach($requests['approved'] as $key=>$item)
                                                        <tr>
                                                            <td>{{$n}}</td>
                                                            <td>
                                                                @if($item['form_group_id'] == 1)
                                                                    @if($item['type']=='project')
                                                                        {{ isset($item['project_title']) ? limit_words($item['project_title'], 19) : ''}}
                                                                    @else
                                                                        {{ isset($item['development_goal']) ? limit_words($item['development_goal'], 19) : ''}}
                                                                    @endif
                                                                @elseif($item['form_group_id'] == 4)
                                                                    {{ isset($item['line_topic']) ? limit_words($item['line_topic'], 19) : ''}}
                                                                @elseif($item['form_group_id'] == 3 )
                                                                    {{ isset($item['title']) ? limit_words($item['title'], 19) : ''}}
                                                                @elseif($item['form_group_id'] == 2)
                                                                    {{ isset($item['development_goal']) ? limit_words($item['development_goal'], 19) : ''}}
                                                                @endif
                                                            </td>
                                                            <td>{{ucfirst(str_replace('_',' ',$form_type[$item['form_group_id']]))}}</td>
                                                            <td>{{$item['employee_name'] }}</td>
                                                            <td>{{isset($item['manag']) ? $item['manag'] : ''}}</td>
                                                            <td>
                                                                {{isset($item['start_date']) ? $item['start_date'] : ''}}
                                                                @if(isset($item['start_date']))
                                                                    <div class="hidden"> month-{{ date('m', strtotime(str_replace('/', '-', $item['start_date']))) }}</div>
                                                                @endif

                                                                @if(isset($item['end_date']))
                                                                    <div class="hidden"> year-{{ date('Y', strtotime(str_replace('/', '-', $item['start_date']))) }}</div>
                                                                @endif
                                                            </td>
                                                            <td>{{isset($item['end_date']) ? $item['end_date'] : ''}}</td>
                                                            <td>
                                                                    <span class="badge badge-pill {{ isset($item['first_accepter_class']) ? $item['first_accepter_class'] : ''}}">
                                                            {{ isset($item['first_accepter']) ? $item['first_accepter'] : ''}}</span>
                                                            </td>
                                                            <td>
                                                                    <span class="badge badge-pill {{ isset($item['second_accepter_class']) ? $item['second_accepter_class'] : ''}}">
                                                            {{ isset($item['second_accepter']) ? $item['second_accepter'] : ''}}</span>
                                                            </td>
                                                            <td> {{isset($item['type_programs']) ? strtr($item['type_programs'],["shortterm"=>"Global Talent Mobility / National Talent Mobility","longterm"=>"Global Talent Mobility / National Talent Mobility"]) : ( isset($item['type']) ? strtr($item['type'],["shortterm"=>"Global Talent Mobility / National Talent Mobility","longterm"=>"Global Talent Mobility / National Talent Mobility"]) : '')}} </td>
                                                            <td >
                                                                <div class="table-option">
                                                                    <a href="{{route('trainee.export',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}" target="_blank">
                                                                        <button type="button" class="btn btn-default btn-sm btn-lite">
                                                                            <i class="icon-transfer-data-between-documents"></i> Export
                                                                        </button>
                                                                    </a>
                                                                    {{--@if($item['form_group_id'] == '1')--}}
                                                                    {{--<a href="{{route('trainee.reviewByUserId',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}">--}}
                                                                    {{--<button type="button" class="btn btn-default btn-sm btn-lite">--}}
                                                                    {{--<i class="icon-edit-1"></i> Result--}}
                                                                    {{--</button>--}}
                                                                    {{--</a>--}}
                                                                    {{--@endif--}}
                                                                    @if($item['form_group_id'] == '1' || $item['form_group_id'] == '2')
                                                                        @if($item['is_accepted'] == 1 && $item['form_group_id'] == '1')
                                                                            {{--                                                                                <a href="{{route('trainee.review',$item['request_group_id'])}}">--}}
                                                                            <button type="button" class="btn btn-default btn-sm btn-lite rating-modal"
                                                                                    data-requestgroup="{{$item['request_group_id']}}"
                                                                                    data-staffid="{{$item['user_id']}}"
                                                                                    data-mentorid="{{$item['first_accepter_id']}}"
                                                                                    data-toggle="modal"
                                                                                    data-target="#assignment">
                                                                                <i class="icon-verified"></i> Done
                                                                            </button>
                                                                            {{--</a>--}}
                                                                        @elseif($item['is_completed_by_manager'] == 1 && $item['is_accepted'] == 1 && $item['form_group_id'] == '2')
                                                                            {{--@dd($item)--}}
                                                                            {{--                                                                                <a href="{{route('trainee.review',$item['request_group_id'])}}">--}}
                                                                            <button type="button" class="btn btn-default btn-sm btn-lite rating-modal"
                                                                                    data-requestgroup="{{$item['request_group_id']}}"
                                                                                    data-staffid="{{$item['user_id']}}"
                                                                                    data-mentorid="{{$item['first_accepter_id']}}"
                                                                                    data-toggle="modal"
                                                                                    data-target="#mentoringForm">
                                                                                <i class="icon-verified"></i> Done
                                                                            </button>
                                                                        @else
                                                                            <button type="button" class="btn btn-default btn-sm btn-lite disabled toast_mentor_trigger" data-toggle="tooltip" data-placement="top" title="Mentor need to complete report first">
                                                                                <i class="icon-verified"></i> Done
                                                                            </button>
                                                                        @endif
                                                                    @endif

                                                                    <a href="{{route('trainee.previewByUserId',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}">
                                                                        <button type="button" class="btn btn-default btn-sm btn-lite">
                                                                            <i class="icon-contract-1"></i> Preview
                                                                        </button>
                                                                    </a>

                                                                    <!-- #tambah role by aprove -->
                                                                    @if($form_type[$item['form_group_id']] == 'Mentoring / Coaching')
                                                                    <a href="{{route('trainee.reviewByUserId',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}">
                                                                        <button type="button" class="btn btn-default btn-sm btn-lite">
                                                                        <i class="icon-edit-1"></i> Report
                                                                        </button>
                                                                    </a>
                                                                    @else
                                                                    @endif
                                                                    <!-- -->

                                                                    @if($item['form_group_id'] == '4')
                                                                        <a href="{{route('trainee.setDone',['form_id'=>$item['form_group_id'],'id'=>$item['request_group_id']])}}">
                                                                            <button type="button" class="btn btn-default btn-sm btn-lite">
                                                                                <i class="icon-verified"></i> Done
                                                                            </button>
                                                                        </a>
                                                                        <div class="hidden">{{ $item['request_group_id'] }}</div>
                                                                    @endif
                                                                    @if($item['form_group_id'] == '3')
                                                                        <a href="#!" class="btn btn-default btn-sm btn-lite rating-modal"
                                                                                data-requestgroup="{{$item['request_group_id']}}"
                                                                                data-staffid="{{$item['user_id']}}"
                                                                                data-mentorid="{{$item['first_accepter_id']}}"
                                                                                data-toggle="modal"
                                                                                data-target="#trainingForm">
                                                                            <i class="icon-verified"></i> Done
                                                                        </a>
                                                                    @endif
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @php($n=$n+1)
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="tab-pane" id="myHistory">
                                <div class="panel-box mt-24">
                                    <div class="panel-title">
                                        <h3>History List</h3>
                                        <div class="right-notif">
                                            @if ((isset($requests['approved'])) && $helper->inStaff() && ['approvedCount'] > 0)
                                                <div class="review-notif">
                                                    <i class="icon-exam"></i> You have got

                                                    <span>{{$requests['approvedCount']}} course(s)</span>
                                                    need to add review. <a href="#myApproval" class="customTabToggler" >Click here</a>
                                                </div>
                                            @endif

                                            @if ($helper->inStaff())
                                                <a href="{{url(route('trainee.index'))}}" >
                                                    <button type="button" class="btn btn-default">
                                                        <i class="icon-plus"></i> Create Request
                                                    </button>
                                                </a>
                                            @endif

                                            @if ($helper->inAdmin())
                                                <button type="button" class="btn btn-default" onclick="location.href='{{url(route('trainee.index'))}}'">
                                                    <i class="icon-plus"></i> Create Trainee
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="panel-table">
                                        <div class="export-table export-mytrainee">
                                            <select id="filterMonthCompleted" class="form-control select-filter">
                                                <option value="">Filter Month</option>
                                                @for($i=1;$i<=12;$i++)
                                                    <option value="month-{{ date('m', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}">{{ date('F', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}</option>
                                                @endfor
                                            </select>

                                            <select id="filterYearCompleted" class="form-control select-filter">
                                                <option value="">Filter Year</option>
                                                @for($i=0;$i<=3;$i++)
                                                    <option value="year-{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                                @endfor
                                            </select>

                                            <a href="{{route('trainee.exportTrainee')}}?type=completed" id="exportCompleted" class="btn btn-default btn-sm btn-lite">
                                                <i class="icon-xlsx-file-format"></i> Excel
                                            </a>
                                        </div>
                                        <div class="clearfix"></div>

                                        <table class="traineeCompletedTable table table-hover table-striped table-responsive table-lite text-center" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Title/Topic</th>
                                                <th>Type</th>
                                                <th>Employee Name</th>
                                                <th>Line Manager</th>
                                                <th>Start</th>
                                                <th>End</th>
                                                @if ($helper->inStaff())
                                                    <th>HR.</th>
                                                    <th>Manager</th>
                                                @endif
                                                <th>Rating Mentor/Coach</th>
                                                <th>Type of Programs</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @php ($n=1)
                                            @if(isset($requests['completed']) && !is_null($requests['completed']))
                                                @foreach($requests['completed'] as $key=>$item)
                                                    <tr>
                                                        <td>{{$n}}</td>
                                                        <td>
                                                            @if($item['form_group_id'] == 1)
                                                                @if($item['type']=='project')
                                                                    {{ isset($item['project_title']) ? limit_words($item['project_title'],19) : ''}}
                                                                @else
                                                                    {{ isset($item['development_goal']) ? limit_words($item['development_goal'],19) : ''}}
                                                                @endif
                                                            @elseif($item['form_group_id'] == 4)
                                                                {{ isset($item['line_topic']) ? limit_words($item['line_topic'],19) : ''}}
                                                            @elseif($item['form_group_id'] == 3 )
                                                                {{ isset($item['title']) ? limit_words($item['title'],19) : ''}}
                                                            @elseif($item['form_group_id'] == 2)
                                                                {{ isset($item['development_goal']) ? limit_words($item['development_goal'],19) : ''}}
                                                            @endif
                                                        </td>
                                                        <td>{{ucfirst(str_replace('_',' ',$form_type[$item['form_group_id']]))}}</td>
                                                        <td>{{$item['employee_name'] }}</td>
                                                        <td>{{isset($item['manag']) ? $item['manag'] : ''}}</td>
                                                        <td>
                                                            {{isset($item['start_date']) ? $item['start_date'] : ''}}
                                                            @if(isset($item['start_date']))
                                                                <div class="hidden"> month-{{ date('m', strtotime(str_replace('/', '-', $item['start_date']))) }}</div>
                                                            @endif

                                                            @if(isset($item['end_date']))
                                                                <div class="hidden"> year-{{ date('Y', strtotime(str_replace('/', '-', $item['start_date']))) }}</div>
                                                            @endif
                                                        </td>
                                                        <td>{{isset($item['end_date']) ? $item['end_date'] : ''}}</td>

                                                        @if ($helper->inStaff())
                                                            <td>
                                                                    <span class="badge badge-pill {{ isset($item['first_accepter_class']) ? $item['first_accepter_class'] : ''}}">
                                                            {{ isset($item['first_accepter']) ? $item['first_accepter'] : ''}}</span>
                                                            </td>
                                                            <td>
                                                                    <span class="badge badge-pill {{ isset($item['second_accepter_class']) ? $item['second_accepter_class'] : ''}}">
                                                            {{ isset($item['second_accepter']) ? $item['second_accepter'] : ''}}</span>
                                                            </td>
                                                        @endif
                                                        <td>{{$item['rating']}}</td>
                                                        <td> {{isset($item['type_programs']) ? strtr($item['type_programs'],["shortterm"=>"Global Talent Mobility / National Talent Mobility","longterm"=>"Global Talent Mobility / National Talent Mobility"]) : ( isset($item['type']) ? strtr($item['type'],["shortterm"=>"Global Talent Mobility / National Talent Mobility","longterm"=>"Global Talent Mobility / National Talent Mobility"]) : '')}} </td>
                                                        <td >
                                                            <div class="table-option">
                                                                <a href="{{route('trainee.export',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}" target="_blank">
                                                                <button type="button" class="btn btn-default btn-sm btn-lite">
                                                                    <i class="icon-transfer-data-between-documents"></i> Export
                                                                </button>
                                                                </a>

                                                                @if($item['form_group_id'] == 2)
                                                                    <a href="{{route('trainee.review',$item['request_group_id'])}}">
                                                                        <button type="button" class="btn btn-default btn-sm btn-lite">
                                                                            <i class="icon-edit-1"></i> See Result
                                                                        </button>
                                                                    </a>
                                                                @endif

                                                                <!-- new for rate -->
                                                                @if($form_type[$item['form_group_id']] == 'Mentoring / Coaching')
                                                                    @if($item['count_rating'] == 0)
                                                                        <button type="button" class="btn btn-default btn-sm btn-lite rating-modal"data-requestgroup="{{$item['request_group_id']}}" data-staffid="{{$item['user_id']}}" data-mentorid="{{$item['first_accepter_id']}}" data-toggle="modal" data-target="#assignment2">
                                                                            <i class="icon-edit-1"></i>Rate
                                                                        </button>
                                                                    @else
                                                                        <button type="button" class="btn btn-default btn-sm btn-lite rating-modal" disabled>
                                                                            <i class="icon-verified"></i> Rate
                                                                        </button>
                                                                    @endif
                                                                @endif
                                                                <!-- end rate -->

                                                                <a href="{{route('trainee.previewByUserId',['user_id' => $item['user_id'],'id'=>$item['request_group_id']])}}">
                                                                    <button type="button" class="btn btn-default btn-sm btn-lite">
                                                                        <i class="icon-contract-1"></i> Preview
                                                                    </button>

                                                                    <div class="hidden">{{ $item['request_group_id'] }}</div>
                                                                </a>

                                                                @if($item['form_group_id'] == 5 && !$item['rated_by_staff'] && $helper->inStaff())
                                                                    <button type="button" class="btn btn-default btn-sm btn-lite rating-modal"
                                                                            data-requestgroup="{{$item['request_group_id']}}"
                                                                            data-staffid="{{$user->id}}"
                                                                            data-mentorid="5"
                                                                            data-toggle="modal"
                                                                            data-target="#assignment">
                                                                        <i class="icon-verified"></i> Done
                                                                    </button>
                                                                @endif
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @php($n=$n+1)
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="rejected">
                                <div class="panel-box mt-24">
                                    <div class="panel-title">
                                        <h3>Rejected List</h3>
                                        <div class="right-notif">
                                            @if ((isset($requests['approved'])) && $helper->inStaff() && ['approvedCount'] > 0)
                                                <div class="review-notif">
                                                    <i class="icon-exam"></i> You have got

                                                    <span>{{$requests['approvedCount']}} course(s)</span>
                                                    need to add review. <a href="#myApproval" class="customTabToggler" >Click here</a>
                                                </div>
                                            @endif

                                            @if ($helper->inStaff())
                                                <a href="{{url(route('trainee.index'))}}">
                                                    <button type="button" class="btn btn-default">
                                                        <i class="icon-plus"></i> Create Request
                                                    </button>
                                                </a>
                                            @endif

                                            @if ($helper->inAdmin())
                                                <button type="button" class="btn btn-default" onclick="location.href='{{url(route('trainee.index'))}}'">
                                                    <i class="icon-plus"></i> Create Trainee
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="panel-table">
                                        <div class="export-table export-mytrainee">
                                            <select id="filterMonthRejected" class="form-control select-filter">
                                                <option value="">Filter Month</option>
                                                @for($i=1;$i<=12;$i++)
                                                    <option value="month-{{ date('m', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}">{{ date('F', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}</option>
                                                @endfor
                                            </select>

                                            <select id="filterYearRejected" class="form-control select-filter">
                                                <option value="">Filter Year</option>
                                                @for($i=0;$i<=3;$i++)
                                                    <option value="year-{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                                @endfor
                                            </select>

                                            <a href="{{route('trainee.exportTrainee')}}?type=rejected" id="exportRejected" class="btn btn-default btn-sm btn-lite">
                                                <i class="icon-xlsx-file-format"></i> Excel
                                            </a>
                                        </div>
                                        <div class="clearfix"></div>

                                        <table class="traineeRejectedTable table table-hover table-striped table-responsive table-lite text-center" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Title/Topic</th>
                                                <th>Type</th>
                                                <th>Employee Name</th>
                                                <th>Line Manager</th>
                                                <th>Start</th>
                                                <th>End</th>
                                                @if ($helper->inStaff())
                                                    <th>HR.</th>
                                                    <th>Manager</th>
                                                @endif
                                                <th> Type of Programs </th>
                                                <th>Notes</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @php ($n=1)
                                            @if(isset($requests['rejected']) && !is_null($requests['rejected']))
                                                @foreach($requests['rejected'] as $key=>$item)
                                                    <tr>
                                                        <td>{{$n}}</td>
                                                        <td>
                                                            @if($item['form_group_id'] == 1)
                                                                @if($item['type']=='project')
                                                                    {{ isset($item['project_title']) ? limit_words($item['project_title'],19) : ''}}
                                                                @else
                                                                    {{ isset($item['development_goal']) ? limit_words($item['development_goal'],19) : ''}}
                                                                @endif
                                                            @elseif($item['form_group_id'] == 4)
                                                                {{ isset($item['line_topic']) ? limit_words($item['line_topic'],19) : ''}}
                                                            @elseif($item['form_group_id'] == 3 )
                                                                {{ isset($item['title']) ? limit_words($item['title'],19) : ''}}
                                                            @elseif($item['form_group_id'] == 2)
                                                                {{ isset($item['development_goal']) ? limit_words($item['development_goal'],19) : ''}}
                                                            @endif
                                                        </td>
                                                        <td>{{ucfirst(str_replace('_',' ',$form_type[$item['form_group_id']]))}}</td>
                                                        <td>{{$item['employee_name'] }}</td>
                                                        <td>{{isset($item['manag']) ? $item['manag'] : ''}}</td>
                                                        <td>
                                                            {{isset($item['start_date']) ? $item['start_date'] : ''}}
                                                            @if(isset($item['start_date']))
                                                                <div class="hidden"> month-{{ date('m', strtotime(str_replace('/', '-', $item['start_date']))) }}</div>
                                                            @endif

                                                            @if(isset($item['end_date']))
                                                                <div class="hidden"> year-{{ date('Y', strtotime(str_replace('/', '-', $item['start_date']))) }}</div>
                                                            @endif
                                                        </td>
                                                        <td>{{isset($item['end_date']) ? $item['end_date'] : ''}}</td>

                                                        @if ($helper->inStaff())
                                                            <td>
                                                                    <span class="badge badge-pill {{ isset($item['first_accepter_class']) ? $item['first_accepter_class'] : ''}}">
                                                            {{ isset($item['first_accepter']) ? $item['first_accepter'] : ''}}</span>
                                                            </td>
                                                            <td>
                                                                    <span class="badge badge-pill {{ isset($item['second_accepter_class']) ? $item['second_accepter_class'] : ''}}">
                                                            {{ isset($item['second_accepter']) ? $item['second_accepter'] : ''}}</span>
                                                            </td>
                                                        @endif
                                                        <td> {{isset($item['type_programs']) ? strtr($item['type_programs'],["shortterm"=>"Global Talent Mobility / National Talent Mobility","longterm"=>"Global Talent Mobility / National Talent Mobility"]) : ( isset($item['type']) ? strtr($item['type'],["shortterm"=>"Global Talent Mobility / National Talent Mobility","longterm"=>"Global Talent Mobility / National Talent Mobility"]) : '')}} </td>
                                                        <td >
                                                            {{$item['rejected_notes']}}
                                                        </td>
                                                    </tr>
                                                    @php($n=$n+1)
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @if (!$helper->inAdmin())
                                {{--@dd($full_name)--}}
                                @include('admindashboard::history.index')
                            @endif
                        </div>

                    </div>

                </div>
                <button type="button" class="btn btn-float btn-fba">
                    <i class="mdi mdi-more"></i>
                </button>
            </div>

            
        </div>
    </main>

    @include('trainee-modal')
    <div role="alert" aria-live="assertive" aria-atomic="true" class="toast mentor_toast d-none" data-autohide="false">
        <div class="toast-header">
            {{--<img src="..." class="rounded mr-2" alt="...">--}}
            <strong class="mr-auto">Mydev</strong>
            {{--<small>11 mins ago</small>--}}
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body">
            Mentor need to complete report first
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('.toast_mentor_trigger').on('click',function(){
            $('.mentor_toast').toast('show');
        });
        $('.customTabToggler').click(function () {

            $('.tab-pane')
                .removeClass('active')
                .removeClass('show');
            $($(this).attr('href')).addClass('active').addClass('show');
            $('.nav-link')
                .removeClass('active')
                .removeClass('show');
            $('[href="'+$(this).attr('href')+'"]').addClass('active').addClass('show');
        });


        // /** TABLE FILTER**/
        var waitingApprovalTable = $('.traineeWaitingApprovalTable').DataTable({
            "pageLength": 10,
            "responnsive": true,
            "searching": true
            // "order": [
            //     [ 9, "desc" ]
            // ]
        });

        // waitingApprovalTable.on( 'order.dt search.dt', function () {
        //     waitingApprovalTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        //     cell.innerHTML = i+1;
        //     } );
        // }).draw();

        $('#filterMonthWaitingApproval, #filterYearWaitingApproval').on( 'change', function () {
            var year = $(document).find('#filterYearWaitingApproval').val();
            year = year.replace("year-", "");

            var month = $(document).find('#filterMonthWaitingApproval').val();
            month = month.replace("month-", "");

            $(document).find('#exportWaitingApproval').attr('href', "{{route('trainee.exportTrainee')}}?type=new&year="+year+"&month="+month);

            waitingApprovalTable
            .columns( 5 )
            .search( this.value )
            .draw();
        });

        var ongoingTable = $('.traineeOngoingTable').DataTable({
            "pageLength": 5,
            "order": [
                [ 9, "desc" ]
            ]
        });

        ongoingTable.on( 'order.dt search.dt', function () {
            ongoingTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
            } );
        }).draw();

        $('#filterMonthOngoing, #filterYearOngoing').on( 'change', function () {
            var year = $(document).find('#filterYearOngoing').val();
            year = year.replace("year-", "");

            var month = $(document).find('#filterMonthOngoing').val();
            month = month.replace("month-", "");

            $(document).find('#exportOngoing').attr('href', "{{route('trainee.exportTrainee')}}?type=approved&year="+year+"&month="+month);

            ongoingTable
            .columns( 5 )
            .search( this.value )
            .draw();
        });

        var completedTable = $('.traineeCompletedTable').DataTable({
            "pageLength": 5,
            "order": [
                [ 9, "desc" ]
            ]
        });

        completedTable.on( 'order.dt search.dt', function () {
            completedTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
            } );
        }).draw();

        $('#filterMonthCompleted, #filterYearCompleted').on( 'change', function () {
            var year = $(document).find('#filterYearCompleted').val();
            year = year.replace("year-", "");

            var month = $(document).find('#filterMonthCompleted').val();
            month = month.replace("month-", "");

            $(document).find('#exportCompleted').attr('href', "{{route('trainee.exportTrainee')}}?type=completed&year="+year+"&month="+month);

            completedTable
            .columns( 5 )
            .search( this.value )
            .draw();
        });

        var rejectedTable = $('.traineeRejectedTable').DataTable({
            "pageLength": 5,
            "order": [
                [ 9, "desc" ]
            ]
        });

        rejectedTable.on( 'order.dt search.dt', function () {
            rejectedTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
            } );
        }).draw();

        $('#filterMonthRejected, #filterYearRejected').on( 'change', function () {
            var year = $(document).find('#filterYearRejected').val();
            year = year.replace("year-", "");

            var month = $(document).find('#filterMonthRejected').val();
            month = month.replace("month-", "");

            $(document).find('#exportRejected').attr('href', "{{route('trainee.exportTrainee')}}?type=rejected&year="+year+"&month="+month);

            rejectedTable
            .columns( 5 )
            .search( this.value )
            .draw();
        });
    </script>
@endsection
