
@if ($is_completed)
    @php($is_disabled = 'disabled')
@endif
<div class="review-item">
        <div class="sub-title text-left"><span class="session_number">1st</span> session</div>
        <div class="center-form">

            <div class="form-group reset">
                <label class="control-label">
                    Training Title
                </label>
                <div class="form-field">
                    <input {{$is_disabled}} type="text" name="reviews_project_title[]" value="{{$reviews_project_title}}" class="form-control" placeholder="example: Indosat Projects" required>
                </div>
                @if ($errors->get('reviews_project_title.0'))
                    <small class="text-danger"> {{$errors->get('reviews_project_title.0')[0]}} </small>
                @endif
            </div>

            <div class="form-group reset">
                <label class="control-label">
                    Training Provider
                </label>
                <div class="form-field">
                    <textarea {{$is_disabled}} class="form-control" rows="6" placeholder="enter provider" name="reviews_provider[]" required>{{$reviews_provider}}</textarea>
                    <p class="form-helper text-count">0/255</p>
                </div>
                @if ($errors->get('reviews_provider.0'))
                    <small class="text-danger"> {{$errors->get('reviews_provider.0')[0]}} </small>
                @endif
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Start date
                </label>
                <div class="form-field">
                    <input {{$is_disabled}} type="text" name="reviews_date[]" value="{{$reviews_date}}" class="form-control start-date" autocomplete="off" placeholder="01/01/2019" required>
                </div>
                @if ($errors->get('reviews_date.0'))
                    <small class="text-danger"> {{$errors->get('reviews_date.0')[0]}} </small>
                @endif
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    End Date
                </label>
                <div class="form-field">
                    <input {{$is_disabled}} type="text" name="reviews_end_date[]" value="{{$reviews_end_date}}" class="form-control end-date" autocomplete="off" placeholder="01/01/2019" required>
                </div>
                @if ($errors->get('reviews_end_date.0'))
                    <small class="text-danger"> {{$errors->get('reviews_end_date.0')[0]}} </small>
                @endif
            </div>

            <div class="form-group reset">
                <label class="control-label">
                    Cost
                </label>
                <div class="form-field">
                    <input {{$is_disabled}} type="number" name="reviews_cost[]" value="{{$reviews_cost}}" class="form-control" placeholder="example: 1000000" required>
                </div>
                @if ($errors->get('reviews_cost.0'))
                    <small class="text-danger"> {{$errors->get('reviews_cost.0')[0]}} </small>
                @endif
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Location (City)
                </label>
                <div class="form-field">
                    <input {{$is_disabled}} type="text" name="reviews_location[]" value="{{$reviews_location}}" class="form-control" placeholder="example: Jakarta" required>
                </div>
                @if ($errors->get('reviews_location.0'))
                    <small class="text-danger"> {{$errors->get('reviews_location.0')[0]}} </small>
                @endif
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Country
                </label>
                <div class="form-field">
                    <input {{$is_disabled}} type="text" name="reviews_country[]" value="{{$reviews_country}}" class="form-control" placeholder="example: Indonesia" required>
                </div>
                @if ($errors->get('reviews_country.0'))
                    <small class="text-danger"> {{$errors->get('reviews_country.0')[0]}} </small>
                @endif
            </div>
        </div>

    </div>
@if ($current_data && count($current_data) > 0)
    @foreach($current_data as $key=>$item)
        <input type="hidden" name="review_id[]" value="{{$item['id']}}">
        <div class="review-item">
            <div class="sub-title text-left"><span class="session_number">{{$key+1}}{{ $key == 1 ? 'nd' : ($key == 2 ? 'rd'  : 'th') }}</span> session</div>
            <div class="center-form">

                <div class="form-group reset">
                    <label class="control-label">
                        Training Title
                    </label>
                    <div class="form-field">
                        <input {{$is_disabled}} type="text" name="reviews_project_title[]" value="{{$item['reviews_project_title']}}" class="form-control" placeholder="example: Indosat Projects" required>
                    </div>
                    @if ($errors->get('reviews_project_title.0'))
                        <small class="text-danger"> {{$errors->get('reviews_project_title.0')[0]}} </small>
                    @endif
                </div>

                <div class="form-group reset">
                    <label class="control-label">
                        Training Provider
                    </label>
                    <div class="form-field">
                        <textarea {{$is_disabled}} class="form-control" rows="6" placeholder="enter provider" name="reviews_provider[]" required>{{$item['reviews_provider']}}</textarea>
                        <p class="form-helper text-count">0/255</p>
                    </div>
                    @if ($errors->get('reviews_provider.0'))
                        <small class="text-danger"> {{$errors->get('reviews_provider.0')[0]}} </small>
                    @endif
                </div>
                <div class="form-group reset">
                    <label class="control-label">
                        Start date
                    </label>
                    <div class="form-field">
                        <input {{$is_disabled}} type="text" name="reviews_date[]" value="{{$item['reviews_date']}}" class="form-control start-date" placeholder="01/01/2019" required>
                    </div>
                    @if ($errors->get('reviews_date.0'))
                        <small class="text-danger"> {{$errors->get('reviews_date.0')[0]}} </small>
                    @endif
                </div>
                <div class="form-group reset">
                    <label class="control-label">
                        End Date
                    </label>
                    <div class="form-field">
                        <input {{$is_disabled}} type="text" name="reviews_end_date[]" value="{{$item['reviews_end_date']}}" class="form-control end-date" placeholder="01/01/2019" required>
                    </div>
                    @if ($errors->get('reviews_end_date.0'))
                        <small class="text-danger"> {{$errors->get('reviews_end_date.0')[0]}} </small>
                    @endif
                </div>

                <div class="form-group reset">
                    <label class="control-label">
                        Cost
                    </label>
                    <div class="form-field">
                        <input {{$is_disabled}} type="number" name="reviews_cost[]" value="{{$item['reviews_cost']}}" class="form-control" placeholder="example: 1000000" required>
                    </div>
                    @if ($errors->get('reviews_cost.0'))
                        <small class="text-danger"> {{$errors->get('reviews_cost.0')[0]}} </small>
                    @endif
                </div>
                <div class="form-group reset">
                    <label class="control-label">
                        Location (City)
                    </label>
                    <div class="form-field">
                        <input {{$is_disabled}} type="text" name="reviews_location[]" value="{{$item['reviews_location']}}" class="form-control" placeholder="example: Jakarta" required>
                    </div>
                    @if ($errors->get('reviews_location.0'))
                        <small class="text-danger"> {{$errors->get('reviews_location.0')[0]}} </small>
                    @endif
                </div>
                <div class="form-group reset">
                    <label class="control-label">
                        Country
                    </label>
                    <div class="form-field">
                        <input {{$is_disabled}} type="text" name="reviews_country[]" value="{{$item['reviews_country']}}" class="form-control" placeholder="example: Indonesia" required>
                    </div>
                    @if ($errors->get('reviews_country.0'))
                        <small class="text-danger"> {{$errors->get('reviews_country.0')[0]}} </small>
                    @endif
                </div>
            </div>

        </div>
    @endforeach
@endif
<div class="review-section"></div>
<div class="bottom-button">

    <div class="form-group reset mb-0 mr-3">
        <div class="form-field">
            <input id="files1" type="file" accept=".pdf,doc,docx,image/*" name="reviews_attachment" class="btn mb-0 btn-default reset files">
{{--            @dd($reviews_attachment)--}}
            @if ($is_completed)
                @if ($reviews_attachment)
                    <a href="{{asset('uploads/'.$reviews_attachment)}}">
                        <button type="button" class="btn mb-0 btn-default reset">
                            <label style="margin-bottom: 0;width: 100%; padding: 0;"><i class="icon-contact"></i> View attachment</label>
                        </button>
                    </a>
                @else
                    <button type="button" class="btn mb-0 btn-default reset disabled" disabled>
                        <label for="files1" style="margin-bottom: 0;width: 100%; padding: 0;"><i class="icon-contact"></i> This reviews have no attachment</label>
                    </button>
                @endif
            @else
                @if ($reviews_attachment)
                    <button type="button" class="btn mb-0 btn-default reset" title="{{str_replace('-',' ',$reviews_attachment)}}">
                        <label for="files1" style="margin-bottom: 0;width: 100%; padding: 0;"><i class="icon-contact"></i> Change attachment</label>
                    </button>
                @else
                    <button type="button" class="btn mb-0 btn-default reset">
                        <label for="files1" style="margin-bottom: 0;width: 100%; padding: 0;"><i class="icon-contact"></i> Add attachment</label>
                    </button>
                @endif
            @endif


        </div>
    </div>
    @if ($is_completed)
        <button type="button" class="btn btn-disabled" disabled > Review Completed </button>
    @else
        <button type="button" class="btn btn-default btn-more">Add more</button>
        <button type="button" class="btn btn-default btn-remove" data-toggle="tooltip" data-placement="top" title="Remove last session">Remove</button>
        <button type="submit" name="submit_button" value="save" class="btn btn-default">Submit</button>
        @if ($data_exists)
            <button type="submit" name="submit_button" class="btn btn-default" value="completed"> Completed </button>
        @endif
    @endif


</div>
