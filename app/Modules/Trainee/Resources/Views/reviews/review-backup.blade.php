<div class="review-section">
    @if ($is_completed)
        @php($is_disabled = 'disabled')
    @endif
    <div class="review-item">
        <div class="sub-title">{{ucfirst($form['form_type'])}} Session</div>
        <div class="center-form">
            <div class="form-group reset">
                <label class="control-label">
                    Course Name
                </label>
                <div class="form-field">
                    <input {{$is_disabled}} type="text" name="reviews_course_name[]" value="{{$reviews_course_name}}" class="form-control" placeholder="enter course or training" required>
                </div>
                @if ($errors->get('reviews_course_name.0'))
                    <small class="text-danger"> {{$errors->get('reviews_course_name.0')[0]}} </small>
                @endif
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Session number
                </label>
                <div class="form-field">
                    <input {{$is_disabled}} type="text" name="reviews_session_number[]" value="{{$reviews_session_number}}" class="form-control" placeholder="example: 1" required>
                </div>
                @if ($errors->get('reviews_session_number.0'))
                    <small class="text-danger"> {{$errors->get('reviews_session_number.0')[0]}} </small>
                @endif
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Duration
                </label>
                <div class="form-field">
                    <input {{$is_disabled}} type="text" name="reviews_duration[]" value="{{$reviews_duration}}" class="form-control" placeholder="example: 2 hours" required>
                </div>
                @if ($errors->get('reviews_duration.0'))
                    <small class="text-danger"> {{$errors->get('reviews_duration.0')[0]}} </small>
                @endif
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Date
                </label>
                <div class="form-field">
                    <input {{$is_disabled}} type="text" name="reviews_date[]" value="{{$reviews_date}}" class="form-control datetimepicker2" placeholder="01/01/2019" required>
                </div>
                @if ($errors->get('reviews_date.0'))
                    <small class="text-danger"> {{$errors->get('reviews_date.0')[0]}} </small>
                @endif
            </div>

            <div class="form-group reset">
                <label class="control-label">
                    Mentoring Goal
                </label>
                <div class="form-field">
                    <textarea {{$is_disabled}} class="form-control" rows="6" placeholder="enter coaching topic" name="reviews_goal[]" required>{{$reviews_goal}}</textarea>
                    <p class="form-helper text-count">0/255</p>
                </div>
                @if ($errors->get('reviews_goal.0'))
                    <small class="text-danger"> {{$errors->get('reviews_goal.0')[0]}} </small>
                @endif
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Mentoring result or progress
                </label>
                <div class="form-field">
                    <textarea {{$is_disabled}} class="form-control" rows="6" placeholder="enter coaching topic" name="reviews_result[]" required>{{$reviews_result}}</textarea>
                    <p class="form-helper text-count">0/255</p>
                </div>
                @if ($errors->get('reviews_result.0'))
                    <small class="text-danger"> {{$errors->get('reviews_result.0')[0]}} </small>
                @endif
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Mentoring Feedback
                </label>
                <div class="form-field">
                    <textarea {{$is_disabled}} class="form-control" rows="6" placeholder="enter coaching topic" name="reviews_feedback[]" required>{{$reviews_feedback}}</textarea>
                    <p class="form-helper text-count">0/255</p>
                </div>
                @if ($errors->get('reviews_feedback.0'))
                    <small class="text-danger"> {{$errors->get('reviews_feedback.0')[0]}} </small>
                @endif
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Mentoring action (optional)
                </label>
                <div class="form-field">
                    <textarea {{$is_disabled}} class="form-control" rows="6" placeholder="enter coaching topic" name="reviews_action[]" required>{{$reviews_action}}</textarea>
                    <p class="form-helper text-count">0/255</p>
                </div>
                @if ($errors->get('reviews_action.0'))
                    <small class="text-danger"> {{$errors->get('reviews_action.0')[0]}} </small>
                @endif
            </div>
        </div>

    </div>
</div>
@if ($current_data && count($current_data) > 0)
    @foreach($current_data as $key=>$item)
        <div class="review-item">
            <div class="sub-title">Training Session</div>
            <div class="center-form">
                <div class="form-group reset">
                    <label class="control-label">
                        Course Name
                    </label>
                    <div class="form-field">
                        <input {{$is_disabled}} type="text" name="reviews_course_name[]" value="{{$item['reviews_course_name']}}" class="form-control" placeholder="enter course or training" required>
                    </div>
                    @if ($errors->get('reviews_course_name.0'))
                        <small class="text-danger"> {{$errors->get('reviews_course_name.0')[0]}} </small>
                    @endif
                </div>
                <div class="form-group reset">
                    <label class="control-label">
                        Session number
                    </label>
                    <div class="form-field">
                        <input {{$is_disabled}} type="text" name="reviews_session_number[]" value="{{$item['reviews_session_number']}}" class="form-control" placeholder="example: 1" required>
                    </div>
                    @if ($errors->get('reviews_session_number.0'))
                        <small class="text-danger"> {{$errors->get('reviews_session_number.0')[0]}} </small>
                    @endif
                </div>
                <div class="form-group reset">
                    <label class="control-label">
                        Duration
                    </label>
                    <div class="form-field">
                        <input {{$is_disabled}} type="text" name="reviews_duration[]" value="{{$item['reviews_duration']}}" class="form-control" placeholder="example: 2 hours" required>
                    </div>
                    @if ($errors->get('reviews_duration.0'))
                        <small class="text-danger"> {{$errors->get('reviews_duration.0')[0]}} </small>
                    @endif
                </div>
                <div class="form-group reset">
                    <label class="control-label">
                        Date
                    </label>
                    <div class="form-field">
                        <input {{$is_disabled}} type="text" name="reviews_date[]" value="{{$item['reviews_date']}}" class="form-control datetimepicker2" placeholder="01/01/2019" required>
                    </div>
                    @if ($errors->get('reviews_date.0'))
                        <small class="text-danger"> {{$errors->get('reviews_date.0')[0]}} </small>
                    @endif
                </div>

                <div class="form-group reset">
                    <label class="control-label">
                        Mentoring Goal
                    </label>
                    <div class="form-field">
                        <textarea {{$is_disabled}} class="form-control" rows="6" placeholder="enter coaching topic" name="reviews_goal[]" required>{{$item['reviews_goal']}}</textarea>
                        <p class="form-helper text-count">0/255</p>
                    </div>
                    @if ($errors->get('reviews_goal.0'))
                        <small class="text-danger"> {{$errors->get('reviews_goal.0')[0]}} </small>
                    @endif
                </div>
                <div class="form-group reset">
                    <label class="control-label">
                        Mentoring result or progress
                    </label>
                    <div class="form-field">
                        <textarea {{$is_disabled}} class="form-control" rows="6" placeholder="enter coaching topic" name="reviews_result[]" required>{{$item['reviews_result']}}</textarea>
                        <p class="form-helper text-count">0/255</p>
                    </div>
                    @if ($errors->get('reviews_result.0'))
                        <small class="text-danger"> {{$errors->get('reviews_result.0')[0]}} </small>
                    @endif
                </div>
                <div class="form-group reset">
                    <label class="control-label">
                        Mentoring Feedback
                    </label>
                    <div class="form-field">
                        <textarea {{$is_disabled}} class="form-control" rows="6" placeholder="enter coaching topic" name="reviews_feedback[]" required>{{$item['reviews_feedback']}}</textarea>
                        <p class="form-helper text-count">0/255</p>
                    </div>
                    @if ($errors->get('reviews_feedback.0'))
                        <small class="text-danger"> {{$errors->get('reviews_feedback.0')[0]}} </small>
                    @endif
                </div>
                <div class="form-group reset">
                    <label class="control-label">
                        Mentoring action (optional)
                    </label>
                    <div class="form-field">
                        <textarea {{$is_disabled}} class="form-control" rows="6" placeholder="enter coaching topic" name="reviews_action[]" required>{{$item['reviews_action']}}</textarea>
                        <p class="form-helper text-count">0/255</p>
                    </div>
                    @if ($errors->get('reviews_action.0'))
                        <small class="text-danger"> {{$errors->get('reviews_action.0')[0]}} </small>
                    @endif
                </div>
            </div>

        </div>
    @endforeach
@endif
<div class="bottom-button">

    @if ($is_completed)
        <button type="button" class="btn btn-disabled" disabled > Review Completed </button>
    @else
        <button type="button" class="btn btn-default btn-more">Add more</button>
        <button type="button" class="btn btn-default btn-remove" data-toggle="tooltip" data-placement="top" title="Remove last session">Remove</button>
        <button type="submit" name="submit_button" value="save" class="btn btn-default">Submit</button>
        @if ($data_exists)
            <button type="submit" name="submit_button" class="btn btn-default" value="completed"> Completed </button>
        @endif
    @endif


</div>
