@if ($is_completed)
    @php($is_disabled = 'disabled')
@endif

<input type="hidden" name="employee_nik" value="{{$form['form_detail']['employee_nik']}}">
<div class="review-item">
    <div class="center-form">

        <div class="form-group reset ">
            <label class="control-label">
                Topic
            </label>
            <div class="form-field">
                <input {{$is_disabled}} type="text" name="reviews_topic[]" value="{{$reviews_topic}}" class="form-control" placeholder="Enter topic" required>
            </div>
            @if ($errors->get('reviews_topic.0'))
                <small class="text-danger"> {{$errors->get('reviews_topic.0')[0]}} </small>
            @endif
        </div>
        <div class="form-group reset ">
            <label class="control-label">
                Start date
            </label>
            <div class="form-field">
                <input {{$is_disabled}} autocomplete="off" type="text" name="reviews_date[]" value="{{$reviews_date}}" class="form-control  start-date" placeholder="01/01/2019" required>
            </div>
            @if ($errors->get('reviews_date.0'))
                <small class="text-danger"> {{$errors->get('reviews_date.0')[0]}} </small>
            @endif
        </div>
        <div class="form-group reset ">
            <label class="control-label">
                End Date
            </label>
            <div class="form-field">
                <input {{$is_disabled}} autocomplete="off" type="text" name="reviews_committed_date[]" value="{{$reviews_committed_date}}" class="form-control  end-date" placeholder="01/01/2019" required>
            </div>
            @if ($errors->get('reviews_committed_date.0'))
                <small class="text-danger"> {{$errors->get('reviews_committed_date.0')[0]}} </small>
            @endif
        </div>
        <div class="form-group reset ">
            <label class="control-label">
                Total hours
            </label>
            <div class="form-field">
                <input {{$is_disabled}} type="text" class="form-control"  placeholder="example : 2 hours" name="reviews_duration[]" required value="{{$reviews_duration}}" />
            </div>
            @if ($errors->get('reviews_duration.0'))
                <small class="text-danger"> {{$errors->get('reviews_duration.0')[0]}} </small>
            @endif
        </div>
    </div>
</div>
<div class="review-section">
</div>

<div class="bottom-button">

    @if ($is_completed)
        <button type="button" class="btn btn-disabled" disabled > Review Completed </button>
    @else
        <button type="submit" name="submit_button" value="save" class="btn btn-default">Submit</button>
        @if ($data_exists)
            <button type="submit" name="submit_button" class="btn btn-default" value="completed"> Completed </button>
        @endif
    @endif


</div>
