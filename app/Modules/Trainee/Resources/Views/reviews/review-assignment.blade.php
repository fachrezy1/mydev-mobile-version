@if ($is_completed)
    @php($is_disabled = 'disabled')
@endif
<div class="review-item">
    <div class="center-form">
        <div class="form-group reset">
            <label class="control-label">
                Start date
            </label>
            <div class="form-field">
                <input {{$is_disabled}} type="text" autocomplete="off" name="reviews_date[]" value="{{$reviews_date}}" class="form-control  start-date" placeholder="01/01/2019" required>
            </div>
            @if ($errors->get('reviews_date.0'))
                <small class="text-danger"> {{$errors->get('reviews_date.0')[0]}} </small>
            @endif
        </div>
        <div class="form-group reset">
            <label class="control-label">
                End Date
            </label>
            <div class="form-field">
                <input {{$is_disabled}} type="text" autocomplete="off" name="reviews_end_date[]" value="{{$reviews_end_date}}" class="form-control  end-date" placeholder="01/01/2019" required>
            </div>
            @if ($errors->get('reviews_end_date.0'))
                <small class="text-danger"> {{$errors->get('reviews_end_date.0')[0]}} </small>
            @endif
        </div>
    </div>

</div>
<div class="bottom-button">

    @if ($is_completed)
        <button type="button" class="btn btn-disabled" disabled > Review Completed </button>
    @else
        <button type="submit" name="submit_button" value="save" class="btn btn-default">Submit</button>
    @endif


</div>
