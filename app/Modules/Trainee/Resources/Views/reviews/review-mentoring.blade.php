@php($role = new \App\Helpers\UserRolesHelper())
@if (  (!$role->inManager() && $is_completed) || ($role->inManager() && $form['form_detail']['is_completed_by_manager']) )
    @php($is_disabled = 'disabled')
@endif

@php($readonly_segment = '')
@if(Request::segment(2) == "preview")
    @php($readonly_segment = 'readonly')
@endif

@php($disable_byflag = '')
@if(isset($flagging))
    @if($flagging == '1')
        @if( sentinel::check()->username != $form['form_detail']['line_manager_nik'])
            @php($disable_byflag = 'disabled')
        @else
            @php($disable_byflag = '')
        @endif
    @endif
@endif

@if(isset($requestGroup['first_accepter']))
    @php($first_accepter = $requestGroup['first_accepter'])
@else
    @php($first_accepter = 0)   
@endif

@if(isset($requestGroup['user_id']))
    @php($user_id = $requestGroup['user_id'])
@else
    @php($user_id = 0) 
@endif



<div class="accordion" id="accordionReport">
    <div class="review-item">
        <div class="sub-title">
            <span class="session_number">1st</span> Session <button class="btn btn-link btn-collapse" type="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">Hide</button>
        </div>

        <div id="collapse1" style="margin-top: -75px;" class="collapse show target-collapse" aria-labelledby="headingOne" data-parent="#accordionReport">
            <div class="center-form">
                    <div class="form-group reset">
                        <label class="control-label">
                            Day/Date
                        </label>
                        <div class="form-field">
                            <input {{$is_disabled}} {{$disable_byflag}} {{$readonly_segment}} type="text" name="reviews_date[]" value="{{$reviews_date}}" class="form-control datetimepicker2 di" placeholder="01/01/2019" required>
                        </div>
                        @if ($errors->get('reviews_date.0'))
                            <small class="text-danger"> {{$errors->get('reviews_date.0')[0]}} </small>
                        @endif
                    </div>
                    <div class="form-group reset">
                        <label class="control-label">
                            Total Hours
                        </label>
                        <div class="form-field">
                            <input {{$is_disabled}} {{$disable_byflag}} {{$readonly_segment}} type="number" name="reviews_duration[]" value="{{$reviews_duration}}" class="form-control di" placeholder="example: 2 hours" required>
                        </div>
                        @if ($errors->get('reviews_duration.0'))
                            <small class="text-danger"> {{$errors->get('reviews_duration.0')[0]}} </small>
                        @endif
                    </div>

                    <div class="form-group reset">
                        <label class="control-label">
                            Coaching Goal
                        </label>
                        <div class="form-field">
                            <textarea {{$is_disabled}} {{$disable_byflag}} {{$readonly_segment}} class="form-control counter-text di coachgoal" rows="6" placeholder="enter coaching goal" name="reviews_goal[]" maxlength="225" required>{{$reviews_goal}}</textarea>
                            <p class="form-helper text-count">
                                <span class="counter-text-value co_goal">
                                    @if($reviews_goal == '' || $reviews_goal == null)
                                        {{'0'}}
                                    @else
                                        {{strlen($reviews_goal)}}
                                    @endif
                                </span>
                            /255</p>
                        </div>
                        @if ($errors->get('reviews_goal.0'))
                            <small class="text-danger"> {{$errors->get('reviews_goal.0')[0]}} </small>
                        @endif
                    </div>
                    <div class="form-group reset">
                        <label class="control-label">
                            Current Condition
                        </label>
                        <div class="form-field">
                            <textarea {{$is_disabled}} {{$disable_byflag}} {{$readonly_segment}} class="form-control counter-text di curent" rows="6" placeholder="enter current condition " name="reviews_condition[]" maxlength="225" required>{{$reviews_condition}}</textarea>
                            <p class="form-helper text-count">
                                <span class="counter-text-value co_curent">
                                    @if($reviews_condition == '' || $reviews_condition == null)
                                        {{'0'}}
                                    @else
                                        {{strlen($reviews_condition)}}
                                    @endif
                                </span>
                            /255</p>
                        </div>
                        @if ($errors->get('reviews_condition.0'))
                            <small class="text-danger"> {{$errors->get('reviews_condition.0')[0]}} </small>
                        @endif
                    </div>
                    <div class="form-group reset">
                        <label class="control-label">
                            List Of Action
                        </label>
                        <div class="form-field">
                            <textarea {{$is_disabled}} {{$disable_byflag}} {{$readonly_segment}} class="form-control counter-text di listaction" rows="6" placeholder="enter list of action" name="reviews_action[]" maxlength="225" required>{{$reviews_action}}</textarea>
                            <p class="form-helper text-count">
                                <span class="counter-text-value co_list">
                                    @if($reviews_action == '' || $reviews_action == null)
                                        {{'0'}}
                                    @else
                                        {{strlen($reviews_action)}}
                                    @endif
                                </span>
                            /255</p>
                        </div>
                        @if ($errors->get('reviews_action.0'))
                            <small class="text-danger"> {{$errors->get('reviews_action.0')[0]}} </small>
                        @endif
                    </div>
                    <div class="form-group reset">
                        <label class="control-label">
                            Commited Time/Date
                        </label>
                        <div class="form-field">
                            <input {{$is_disabled}} {{$disable_byflag}} {{$readonly_segment}} type="text" name="reviews_committed_date[]" value="{{$reviews_committed_date}}" class="form-control datetimepicker2 di" placeholder="01/01/2019" required>
                        </div>
                        @if ($errors->get('reviews_committed_date.0'))
                            <small class="text-danger"> {{$errors->get('reviews_committed_date.0')[0]}} </small>
                        @endif
                    </div>
                    <div class="form-group reset">
                        <label class="control-label">
                            Progress
                        </label>
                        <div class="form-field">
                            <textarea {{$is_disabled}} {{$disable_byflag}} {{$readonly_segment}} class="form-control counter-text di progress" rows="6" placeholder="enter progress" name="reviews_result[]" maxlength="225" required>{{$reviews_result}}</textarea>
                            <p class="form-helper text-count">
                                <span class="counter-text-value co_progress">
                                    @if($reviews_result == '' || $reviews_result == null)
                                        {{'0'}}
                                    @else
                                        {{strlen($reviews_result)}}
                                    @endif
                                </span>
                            /255</p>
                        </div>
                        @if ($errors->get('reviews_result.0'))
                            <small class="text-danger"> {{$errors->get('reviews_result.0')[0]}} </small>
                        @endif
                    </div>
                    
                    @php(@$data_flag = 0)
                    @if(!$helper->inAdmin() && sentinel::check()->username == $form['form_detail']['line_manager_nik'])
                        @php($data_flag = '1')
                    @else
                        @if(isset($flagging))
                            @php($data_flag = $flagging)
                        @else
                            @php($data_flag = '0')
                        @endif
                    @endif

                    <input type="hidden" id="flag1" name="flagging[]" value="{{$data_flag}}" required>
                    <!-- data flag for clone -->
                    @if(!$helper->inAdmin() && sentinel::check()->username == $form['form_detail']['line_manager_nik'])
                        <input type="hidden" id="data_flag_clone" value="1">
                    @else
                        <input type="hidden" id="data_flag_clone" value="0">
                    @endif
                    <!-- data flag for clone -->
        </div>
        </div>
    </div>
    {{--@dd($current_data)--}}
    @if (isset($current_data) && $current_data && count($current_data) > 0)
        @foreach($current_data as $key=>$item)
            {{--@dd($current_data)--}}
            <input type="hidden" name="review_id[]" value="{{$item['id']}}">

            <!-- enable disable -->
            @php($disable_byflag = '')
            @if(isset($item['flagging']))
                @if($item['flagging'] == '1')
                    @if( sentinel::check()->username != $form['form_detail']['line_manager_nik'])
                        @php($disable_byflag = 'disabled')
                    @else
                        @php($disable_byflag = '')
                    @endif

                @endif
            @endif
            <!-- end enable disable -->
            
            <!-- data flag-->
            @if(!$helper->inAdmin() && sentinel::check()->username == $form['form_detail']['line_manager_nik'])
                @php($data_flag = '1')
            @else
                @if(isset($item['flagging']))
                    @php($data_flag = $item['flagging'])
                @else
                    @php($data_flag = '0')
                @endif
            @endif
            <!-- data flag-->

            <input type="hidden" id="flag1" name="flagging[]" value="{{$data_flag}}" required>

            <div class="review-item">
                <div class="sub-title">
                    <span class="session_number">{{$key+1}}{{ $key == 1 ? 'nd' : ($key == 2 ? 'rd' : 'th') }}</span> Session
                    <button class="btn btn-link btn-collapse" type="button" data-toggle="collapse" data-target="#collapse{{$key+1}}" aria-expanded="true" aria-controls="collapse1">Show</button>
                </div>
                <div id="collapse{{$key+1}}" style="margin-top: -75px;" class="collapse" aria-labelledby="headingOne" data-parent="#accordionReport">
                    <div class="center-form">
                        <div class="form-group reset">
                            <label class="control-label">
                                Day/Date
                            </label>
                            <div class="form-field">
                                <input {{$is_disabled}} {{$disable_byflag}} type="text" name="reviews_date[]" value="{{$item['reviews_date']}}" class="form-control datetimepicker2" placeholder="01/01/2019" required>
                            </div>
                            @if ($errors->get('reviews_date.0'))
                                <small class="text-danger"> {{$errors->get('reviews_date.0')[0]}} </small>
                            @endif
                        </div>
                        <div class="form-group reset">
                            <label class="control-label">
                                Total Hours
                            </label>
                            <div class="form-field">
                                <input {{$is_disabled}} {{$disable_byflag}} type="number" name="reviews_duration[]" value="{{$item['reviews_duration']}}" class="form-control" placeholder="example: 2 hours" required>
                            </div>
                            @if ($errors->get('reviews_duration.0'))
                                <small class="text-danger"> {{$errors->get('reviews_duration.0')[0]}} </small>
                            @endif
                        </div>

                        <div class="form-group reset">
                            <label class="control-label">
                                Coaching Goal
                            </label>
                            <div class="form-field">
                                <textarea {{$is_disabled}} {{$disable_byflag}} class="form-control counter-text" rows="6" placeholder="enter coaching goal" name="reviews_goal[]" maxlength="225" required>{{$item['reviews_goal']}}</textarea>
                                <p class="form-helper text-count">
                                    <span class="counter-text-value">
                                        @if($item['reviews_goal'] == '' || $item['reviews_goal'] == null)
                                            {{'0'}}
                                        @else
                                            {{strlen($item['reviews_goal'])}}
                                        @endif
                                    </span>
                                /255</p>
                            </div>
                            @if ($errors->get('reviews_goal.0'))
                                <small class="text-danger"> {{$errors->get('reviews_goal.0')[0]}} </small>
                            @endif
                        </div>
                        <div class="form-group reset">
                            <label class="control-label">
                                Current Condition
                            </label>
                            <div class="form-field">
                                <textarea {{$is_disabled}} {{$disable_byflag}} class="form-control counter-text" rows="6" placeholder="enter current condition" name="reviews_condition[]" maxlength="225" required>{{$item['reviews_condition']}}</textarea>
                                <p class="form-helper text-count">
                                    <span class="counter-text-value">
                                        @if($item['reviews_condition'] == '' || $item['reviews_condition'] == null)
                                            {{'0'}}
                                        @else
                                            {{strlen($item['reviews_condition'])}}
                                        @endif
                                    </span>
                                /255</p>
                            </div>
                            @if ($errors->get('reviews_condition.0'))
                                <small class="text-danger"> {{$errors->get('reviews_condition.0')[0]}} </small>
                            @endif
                        </div>
                        <div class="form-group reset">
                            <label class="control-label">
                                List Of Action
                            </label>
                            <div class="form-field">
                                <textarea {{$is_disabled}} {{$disable_byflag}} class="form-control counter-text" rows="6" placeholder="enter list of action" name="reviews_action[]" maxlength="225" required>{{$item['reviews_action']}}</textarea>
                                <p class="form-helper text-count">
                                    <span class="counter-text-value">
                                        @if($item['reviews_action'] == '' || $item['reviews_action'] == null)
                                            {{'0'}}
                                        @else
                                            {{strlen($item['reviews_action'])}}
                                        @endif
                                    </span>
                                /255</p>
                            </div>
                            @if ($errors->get('reviews_action.0'))
                                <small class="text-danger"> {{$errors->get('reviews_action.0')[0]}} </small>
                            @endif
                        </div>
                        <div class="form-group reset">
                            <label class="control-label">
                                Commited Time/Date
                            </label>
                            <div class="form-field">
                                <input {{$is_disabled}} {{$disable_byflag}} type="text" name="reviews_committed_date[]" value="{{$item['reviews_committed_date']}}" class="form-control datetimepicker2" placeholder="01/01/2019" required>
                            </div>
                            @if ($errors->get('reviews_committed_date.0'))
                                <small class="text-danger"> {{$errors->get('reviews_committed_date.0')[0]}} </small>
                            @endif
                        </div>
                        <div class="form-group reset">
                            <label class="control-label">
                                Progress
                            </label>
                            <div class="form-field">
                                <textarea {{$is_disabled}} {{$disable_byflag}} class="form-control counter-text" rows="6" placeholder="enter progress" name="reviews_result[]" maxlength="225" required>{{$item['reviews_result']}}</textarea>
                                <p class="form-helper text-count">
                                    <span class="counter-text-value">
                                        @if($item['reviews_result'] == '' || $item['reviews_result'] == null)
                                            {{'0'}}
                                        @else
                                            {{strlen($item['reviews_result'])}}
                                        @endif
                                    </span>
                                /255</p>
                            </div>
                            @if ($errors->get('reviews_result.0'))
                                <small class="text-danger"> {{$errors->get('reviews_result.0')[0]}} </small>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
        @endforeach
    @endif
    <div class="review-section"></div>
    {{--@dd($rating)--}}
</div>
<div class="bottom-button">
    {{--@dd($form['form_detail']['is_completed_by_manager'])--}}
    {{--@dd($form)--}}



@if($readonly_segment != '')

    @if ( (!$role->inManager() && $is_completed) || (($role->inManager() && $form['form_detail']['is_completed_by_manager'])))
        @if($role->inAdmin() && !$form['form_detail']['is_completed_by_admin'])
        @else
        <button type="button" class="btn btn-disabled" disabled > Result Completed </button>
        @endif
    @elseif(!$helper->inAdmin() && sentinel::check()->username == $form['form_detail']['line_manager_nik'])
    @endif

    @if ( (!$role->inManager() && $is_completed) || ($role->inManager() && $form['form_detail']['is_completed_by_manager']) )
        @if($role->inAdmin() && !$form['form_detail']['is_completed_by_admin'])
            <button type="submit" name="submit_button" form="formApprovePoint" class="btn btn-default"> Approve & Give Point </button>
        @endif
    @endif

@else
    @if ( (!$role->inManager() && $is_completed) || (($role->inManager() && $form['form_detail']['is_completed_by_manager']) ))
        <!-- review btn -->
        @if(sentinel::check()->username == $form['form_detail']['employee_nik'])
            @if(isset($rating_count))
                @if($rating_count == 0)
                    <button type="button" class="btn btn-default rating-modal" data-requestgroup="{{$requestGroup['id']}}" data-staffid="{{$user_id}}" data-mentorid="{{$first_accepter}}" data-toggle="modal" data-target="#assignment2">
                    Review
                    </button>
                @else
                @endif
            @endif
        @endif
        <!-- end review -->

        @if(isset($requestGroup['id']))
        <a class="btn btn-default" href="{{route('trainee.export2',['user_id' => $user_id,'id'=>$requestGroup['id']])}}" target="_blank"  style="margin-right: 10px;"><i class="icon-transfer-data-between-documents"></i> Export Report
        </a>
        @endif

        <button type="button" class="btn btn-disabled" disabled > Result Completed </button>
    @elseif(!$helper->inAdmin() && sentinel::check()->username == $form['form_detail']['line_manager_nik'])
        <button type="button" class="btn btn-default btn-more">Add more</button>
        <button type="button" class="btn btn-default btn-remove" data-toggle="tooltip" data-placement="top" title="Remove last session">Remove</button>
        <button type="submit" name="submit_button" value="save" class="btn btn-default" id="dis">Save</button>
        @if (isset($data_exists) && $data_exists)
        <!-- approve -->

        <!-- end approve -->
            <button type="submit" name="submit_button" class="btn btn-default" value="completed"> Complete </button>
        @endif
    @endif
    
    @if($role->inAdmin() && !$form['form_detail']['is_completed_by_admin'])
        <button type="submit" name="submit_button" form="formApprovePoint" class="btn btn-default"> Approve & Give Point </button>
    @endif

<!-- new -->
    @if(Request::segment(2) == "report")
        @if ( (!$role->inManager() && $is_completed) || (($role->inManager() && $form['form_detail']['is_completed_by_manager']) ))
        @elseif(!$helper->inAdmin() && sentinel::check()->username == $form['form_detail']['line_manager_nik'])
        @elseif($role->inAdmin() && !$form['form_detail']['is_completed_by_admin'])
        @else
            <button type="button" class="btn btn-default btn-remove" data-toggle="tooltip" data-placement="top" title="Remove last session">Remove</button>
            <button type="button" class="btn btn-default btn-more">Add more</button>
            <button type="submit" name="submit_button" value="save" class="btn btn-default" id="dis">Save</button>
            <!--<button type="button" name="submit_button" value="save" class="btn btn-default">Approve</button>-->
        @endif
    @else
    @endif

@endif
<!-- end new -->

</div>
@section('scripts')
    <script>
        $('.btn-collapse').on('click',function() {
            if($(this).text() == 'Hide') {
                $(this).text('Show')
            } else {
                $(this).text('Hide')
            }
        })

        $(document).ready(function(){
            $(".btn_block").click(function(){
                $.blockUI({ 
                    message:"<img src='{{ url('images/loading.gif') }}' style='width:80px;height:80px;'><br>Loading...",
                    css: {color : '#ffffff',
                    background: 'none',
                    border : 'none'}
                });
            });
        });

        $(document).ready(function(){
            $('form#form_review').on('submit',function(){
                $.blockUI({ 
                    message:"<img src='{{ url('images/loading.gif') }}' style='width:80px;height:80px;'><br>Loading...",
                    css: {
                        color : '#ffffff',
                        background: 'none',
                        border : 'none'
                    }
                });
            })
        })

                //progress
        function progress2(va){
            var coun = va.value.length;
            $('.count_progress2').html(coun);
        }
        function progress3(va){
            var coun = va.value.length;
            $('.count_progress3').html(coun);
        }
        function progress4(va){
            var coun = va.value.length;
            $('.count_progress4').html(coun);
        }
        function progress5(va){
            var coun = va.value.length;
            $('.count_progress5').html(coun);
        }
        function progress6(va){
            var coun = va.value.length;
            $('.count_progress6').html(coun);
        }
        // end progress

        // list
        function list2(va){
            var coun = va.value.length;
            $('.count_list2').html(coun);
        }
        function list3(va){
            var coun = va.value.length;
            $('.count_list3').html(coun);
        }
        function list4(va){
            var coun = va.value.length;
            $('.count_list4').html(coun);
        }
        function list5(va){
            var coun = va.value.length;
            $('.count_list5').html(coun);
        }
        function list6(va){
            var coun = va.value.length;
            $('.count_list6').html(coun);
        }
        // end list
        
        // current
        function curent2(va){
            var coun = va.value.length;
            $('.count_current2').html(coun);
        }
        function curent3(va){
            var coun = va.value.length;
            $('.count_current3').html(coun);
        }
        function curent4(va){
            var coun = va.value.length;
            $('.count_current4').html(coun);
        }
        function curent5(va){
            var coun = va.value.length;
            $('.count_current5').html(coun);
        }
        function curent6(va){
            var coun = va.value.length;
            $('.count_current6').html(coun);
        }
        // end current

        //coach
        function coach2(va){
            var coun = va.value.length;
            $('.count_goal2').html(coun);
        }
        function coach3(va){
            var coun = va.value.length;
            $('.count_goal3').html(coun);
        }
        function coach4(va){
            var coun = va.value.length;
            $('.count_goal4').html(coun);
        }
        function coach5(va){
            var coun = va.value.length;
            $('.count_goal5').html(coun);
        }
        function coach6(va){
            var coun = va.value.length;
            $('.count_goal6').html(coun);
        }
        //endcoach

        // enabled all
        $('#dis').on('click',function() {
            //alert("oke");
            $("#form_review :input").prop("disabled", false);
            $("#form_review :textarea").prop("disabled", false);
        })
    </script>
@endsection