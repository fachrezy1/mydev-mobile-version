@if ($is_completed)
    @php($is_disabled = 'disabled')
@endif
<div class="review-item">
    <div class="sub-title text-left"> <span class="session_number">1st</span> session</div>
    <div class="center-form">

        <div class="form-group reset">
            <label class="control-label">
                Topic
            </label>
            <div class="form-field">
                <input {{$is_disabled}} type="text" name="reviews_topic[]" value="{{$reviews_topic}}" class="form-control" placeholder="Enter topic" required>
            </div>
            @if ($errors->get('reviews_topic.0'))
                <small class="text-danger"> {{$errors->get('reviews_topic.0')[0]}} </small>
            @endif
        </div>
        <div class="form-group reset">
            <label class="control-label">
                Start date
            </label>
            <div class="form-field">
                <input {{$is_disabled}} type="text" name="reviews_date[]" value="{{$reviews_date}}" class="form-control  start-date" placeholder="01/01/2019" required>
            </div>
            @if ($errors->get('reviews_date.0'))
                <small class="text-danger"> {{$errors->get('reviews_date.0')[0]}} </small>
            @endif
        </div>
        <div class="form-group reset">
            <label class="control-label">
                End Date
            </label>
            <div class="form-field">
                <input {{$is_disabled}} type="text" name="reviews_committed_date[]" value="{{$reviews_committed_date}}" class="form-control  end-date" placeholder="01/01/2019" required>
            </div>
            @if ($errors->get('reviews_committed_date.0'))
                <small class="text-danger"> {{$errors->get('reviews_committed_date.0')[0]}} </small>
            @endif
        </div>
        <div class="form-group reset">
            <label class="control-label">
                Total hours
            </label>
            <div class="form-field">
                <input {{$is_disabled}} type="text" class="form-control"  placeholder="example : 2 hours" name="reviews_duration[]" required value="{{$reviews_duration}}" />
            </div>
            @if ($errors->get('reviews_duration.0'))
                <small class="text-danger"> {{$errors->get('reviews_duration.0')[0]}} </small>
            @endif
        </div>
        <div class="form-group reset">
            <label class="control-label">
                Location (City)
            </label>
            <div class="form-field">
                <input {{$is_disabled}} type="text" name="reviews_location[]" value="{{$reviews_location}}" class="form-control" placeholder="example: Jakarta" required>
            </div>
            @if ($errors->get('reviews_location.0'))
                <small class="text-danger"> {{$errors->get('reviews_location.0')[0]}} </small>
            @endif
        </div>
        <div class="form-group reset">
            <label class="control-label">
                Country
            </label>
            <div class="form-field">
                <input {{$is_disabled}} type="text" name="reviews_country[]" value="{{$reviews_country}}" class="form-control" placeholder="example: Indonesia" required>
            </div>
            @if ($errors->get('reviews_country.0'))
                <small class="text-danger"> {{$errors->get('reviews_country.0')[0]}} </small>
            @endif
        </div>
    </div>

</div>
@if ($current_data && count($current_data) > 0)
    @foreach($current_data as $key=>$item)
        <input type="hidden" name="review_id[]" value="{{$item['id']}}">
        <div class="review-item">
            <div class="sub-title text-left"> <span class="session_number">{{$key+1}}{{ $key == 1 ? 'nd' : ($key == 2 ? 'rd' : 'th') }}</span> session</div>
            <div class="center-form">

                <div class="form-group reset">
                    <label class="control-label">
                        Topic
                    </label>
                    <div class="form-field">
                        <input {{$is_disabled}} type="text" name="reviews_topic[]" value="{{$item['reviews_topic']}}" class="form-control" placeholder="Enter topic" required>
                    </div>
                    @if ($errors->get('reviews_topic.0'))
                        <small class="text-danger"> {{$errors->get('reviews_topic.0')[0]}} </small>
                    @endif
                </div>
                <div class="form-group reset">
                    <label class="control-label">
                        Start date
                    </label>
                    <div class="form-field">
                        <input {{$is_disabled}} type="text" name="reviews_date[]" value="{{$item['reviews_date']}}" class="form-control start-date" placeholder="01/01/2019" required>
                    </div>
                    @if ($errors->get('reviews_date.0'))
                        <small class="text-danger"> {{$errors->get('reviews_date.0')[0]}} </small>
                    @endif
                </div>
                <div class="form-group reset">
                    <label class="control-label">
                        End Date
                    </label>
                    <div class="form-field">
                        <input {{$is_disabled}} type="text" name="reviews_committed_date[]" value="{{$item['reviews_committed_date']}}" class="form-control end-date" placeholder="01/01/2019" required>
                    </div>
                    @if ($errors->get('reviews_committed_date.0'))
                        <small class="text-danger"> {{$errors->get('reviews_committed_date.0')[0]}} </small>
                    @endif
                </div>
                <div class="form-group reset">
                    <label class="control-label">
                        Total hours
                    </label>
                    <div class="form-field">
                        <input {{$is_disabled}} type="text" class="form-control"  placeholder="example : 2 hours" name="reviews_duration[]" required value="{{$item['reviews_duration']}}" />
                    </div>
                    @if ($errors->get('reviews_duration.0'))
                        <small class="text-danger"> {{$errors->get('reviews_duration.0')[0]}} </small>
                    @endif
                </div>
                <div class="form-group reset">
                    <label class="control-label">
                        Location (City)
                    </label>
                    <div class="form-field">
                        <input {{$is_disabled}} type="text" name="reviews_location[]" value="{{$item['reviews_location']}}" class="form-control" placeholder="example: Jakarta" required>
                    </div>
                    @if ($errors->get('reviews_location.0'))
                        <small class="text-danger"> {{$errors->get('reviews_location.0')[0]}} </small>
                    @endif
                </div>
                <div class="form-group reset">
                    <label class="control-label">
                        Country
                    </label>
                    <div class="form-field">
                        <input {{$is_disabled}} type="text" name="reviews_country[]" value="{{$item['reviews_country']}}" class="form-control" placeholder="example: Indonesia" required>
                    </div>
                    @if ($errors->get('reviews_country.0'))
                        <small class="text-danger"> {{$errors->get('reviews_country.0')[0]}} </small>
                    @endif
                </div>
            </div>

        </div>
    @endforeach
@endif
<div class="review-section">
</div>

<div class="bottom-button">

    @if ($is_completed)
        <button type="button" class="btn btn-disabled" disabled > Review Completed </button>
    @else
        <button type="button" class="btn btn-default btn-more">Add more</button>
        <button type="button" class="btn btn-default btn-remove" data-toggle="tooltip" data-placement="top" title="Remove last session">Remove</button>
        <button type="submit" name="submit_button" value="save" class="btn btn-default">Submit</button>
        @if ($data_exists)
            <button type="submit" name="submit_button" class="btn btn-default" value="completed"> Completed </button>
        @endif
    @endif


</div>
