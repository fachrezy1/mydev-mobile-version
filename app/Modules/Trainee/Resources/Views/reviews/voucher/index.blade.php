@php($user = \Sentinel::check())
@php($helper = new \App\Helpers\UserRolesHelper )

@extends($helper->inAdmin() ? 'core.admin.main' :'core.trainee.main')
@section('head')
    <link rel="stylesheet" href="{{url('css/admin/dashboard/app.css')}}">
@endsection
@section('content')
    <main class="main-content">
        <div class="main-dashboard">
            <div class="dashboard-content">
                <div class="dashboard-breadcrumb clean">
                    <div class="container-fluid">
                        <ul class="list-unstyled top-breadcrumb">
                            <li>
                                <a href="{{route('trainee.dashboard.index')}}">
                                    <i class="icon-home"></i>
                                    Dashboard
                                </a>
                            </li>
                            <li>
                                Redeem Voucher
                            </li>
                        </ul>
                    </div>
                </div>

                @if (session('success') || session('error'))
                        <div class="dashboard-notify alert {{ session('success') ? 'alert-success' : 'alert-warning' }}" style="margin-top:20px;">
                            <div class="alert-icon">
                                <i class="mdi mdi-comment-alert-outline"></i>
                            </div>
                            <div class="notify-content">
                                <h4>{{ session('success') ? session('success') : session('error') }}</h4>
                                
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"><i class="mdi mdi-close"></i></span>
                                </button>
                            </div>
                        </div>
                    @endif

                <h1 class="text-center">Redeem Voucher</h1>
                <div class="container">
                    <div class="point-page">
                        <div class="mid-point">
                            <div class="point-earn col-md-12">
                                <h3>Please insert your voucher code here</h3>
                                
                                <form action="{{route('trainee.voucher.store')}}" method="post">
                                    @csrf

                                    <div class="form-group text-center col-md-4" style="margin: 10px auto;">
                                        <div class="form-field">
                                            <input type="text" name="code" maxlength="250" required class="form-control" value="">
                                            @if ($errors->has('name'))
                                                <small class="form-text text-danger">{{ $errors->first('name') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="button-group text-center">
                                        <button type="submit" class="btn btn-default">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-float btn-fba">
                    <i class="icon-back"></i>
                </button>
            </div>
            @if ($helper->inStaff())
                @include('sidebar')
            @endif
        </div>
    </main>
@endsection
@section('scripts')
    <script src="{{ url('js/point.js') }}"></script>
@endsection
