@php($user = \Sentinel::check())
@php($helper = new \App\Helpers\UserRolesHelper )

@extends($helper->inAdmin() ? 'core.admin.main' :'core.trainee.main')
@section('head')
    <link rel="stylesheet" href="{{url('css/admin/dashboard/app.css')}}">
@endsection
@section('content')
    <main class="main-content">
    <div class="main-dashboard">
        <div class="dashboard-content">
            <div class="dashboard-breadcrumb clean">
                <div class="container-fluid">
                    <div class="sub-breadcrumb">
                        <div class="sub-left">
                            <button type="button" class="btn btn-clean" onclick="location.href='{{route('trainee.point.redeem')}}'">
                                <i class="icon-back"></i>
                            		Redeem Points
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="redeem-page">
                    <div class="sub-title">
                        <h3>{{$item['name']}}</h3>
                    </div>
                    <div class="redeem-entry">
                        <div class="top-redeem">
                            <figure>
                                <img src="{{asset('uploads/'.$item['picture'])}}" alt="" class="img-fluid">
                            </figure>
                            <div class="right-redeem">
                                <div class="redeem-spec">
                                    <div class="item-spec">
                                        <h4>Item Description</h4>
                                        <p>
                                            {{$item['description']}}
                                        </p>
                                    </div>
                                    <div class="item-bottom">
                                        <div class="point-compare">
                                            <div class="item-point clearfix">
                                                <i class="icon-best"></i>
                                                <h4>Points</h4>
                                                <h3>{{number_format($item['point'],0,',','.')}}<span>Points</span></h3>
                                            </div>
                                            <div class="item-point clearfix">
                                                <i class="icon-reward"></i>
                                                <h4>My Points</h4>
                                                <h3>{{ number_format($user['point'],0,',','.') }}<span>Points</span></h3>
                                            </div>
                                            <p>
                                                <a href="javascript:void(0);" class="redeemOpt">Redeem option</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>
                        <div class="bottom-redeem clearfix">
                            <h3>Terms and conditions</h3>
                            <ol start="1">
                                <li>Penghargaan diberikan sebagai bentuk apresiasi dari Perusahaan kepada Karyawan yang telah berpartisipasi dalam program pengembangan kompetensi sebagai Pengajar Internal / Coach / Mentor, dan juga bagi Karyawan yang menyelesaikan <em>e-Learning Course</em> di platform MyLearning.  Penghargaan diberikan dalam bentuk voucher berdasarkan poin yang telah didapat/dikumpulkan. </li>
                                <li>Perolehan Poin <br/><br/>
                                	<div class="table-responsive">
                                    <table class="MsoTableGrid" border=1 cellspacing=0 cellpadding=0 width=436
                                     style='width:436.45pt;;border-collapse:collapse;border:
                                     none'>
                                     <tr style='height:10.95pt'>
                                      <td width=436 colspan=4 valign=top style='width:436.45pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Sebagai
                                      Peserta (Trainee)</span></b></p>
                                      </td>
                                     </tr>
                                     <tr style='height:11.8pt'>
                                      <td width=95 valign=top style='width:94.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:11.8pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Aktivitas</span></b></p>
                                      </td>
                                      <td width=77 valign=top style='width:76.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:11.8pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Poin</span></b></p>
                                      </td>
                                      <td width=149 valign=top style='width:148.5pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:11.8pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Perolehan
                                      Poin</span></b></p>
                                      </td>
                                      <td width=117 valign=top style='width:116.95pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:11.8pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Source
                                      Platform</span></b></p>
                                      </td>
                                     </tr>
                                     <tr style='height:10.95pt'>
                                      <td width=95 style='width:94.5pt;border:solid windowtext 1.0pt;border-top:
                                      none;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph style='margin-left:0cm'><i><span style='font-size:
                                      10.0pt;font-family:"Arial",sans-serif'>e-Learning Course</span></i></p>
                                      </td>
                                      <td width=77 style='width:76.5pt;border-top:none;border-left:none;border-bottom:
                                      solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;
                                      height:10.95pt'>
                                      <p class=MsoListParagraph style='margin-left:0cm'><span style='font-size:
                                      10.0pt;font-family:"Arial",sans-serif'>1 poin @15 menit</span></p>
                                      </td>
                                      <td width=149 valign=top style='width:148.5pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:justify'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>Dihitung dari total
                                      durasi dari 1 <i>course</i> yang telah diselesaikan.</span></p>
                                      </td>
                                      <td width=117 style='width:116.95pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph style='margin-left:0cm'><b><i><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>MyLearning</span></i></b><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'> atau aplikasi yang
                                      fungsinya setara.</span></p>
                                      </td>
                                     </tr>
                                     <tr style='height:10.95pt'>
                                      <td width=436 colspan=4 valign=top style='width:436.45pt;border:solid windowtext 1.0pt;
                                      border-top:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Sebagai
                                      Pengajar (Trainer)</span></b></p>
                                      </td>
                                     </tr>
                                     <tr style='height:10.95pt'>
                                      <td width=95 valign=top style='width:94.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Aktivitas</span></b></p>
                                      </td>
                                      <td width=77 valign=top style='width:76.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Poin</span></b></p>
                                      </td>
                                      <td width=149 valign=top style='width:148.5pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Perolehan
                                      Poin</span></b></p>
                                      </td>
                                      <td width=117 valign=top style='width:116.95pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Source
                                      Platform</span></b></p>
                                      </td>
                                     </tr>
                                     <tr style='height:11.8pt'>
                                      <td width=95 valign=top style='width:94.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:11.8pt'>
                                      <p class=MsoListParagraph style='margin-left:0cm'><span style='font-size:
                                      10.0pt;font-family:"Arial",sans-serif'>Sebagai Pengajar Internal / Coach/Mentor</span></p>
                                      </td>
                                      <td width=77 style='width:76.5pt;border-top:none;border-left:none;border-bottom:
                                      solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;
                                      height:11.8pt'>
                                      <p class=MsoListParagraph style='margin-left:0cm'><span style='font-size:
                                      10.0pt;font-family:"Arial",sans-serif'>12 poin @30 menit</span></p>
                                      </td>
                                      <td width=149 style='width:148.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:11.8pt'>
                                      <p class=MsoListParagraph style='margin-left:0cm'><span style='font-size:
                                      10.0pt;font-family:"Arial",sans-serif'>Setelah submit “<i>Done</i>” di <i>MyDev</i>.</span></p>
                                      </td>
                                      <td width=117 style='width:116.95pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:11.8pt'>
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:justify'><b><i><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>MyDev</span></i></b><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'> atau aplikasi lain
                                      yang fungsinya setara.</span></p>
                                      </td>
                                     </tr>
                                    </table>
                                  </div>
                                </li>
                                <li>Setiap Karyawan yang telah berpartisipasi dan menyelesaikan aktivitas, baik sebagai Trainee maupun Trainer sesuai butir b. di atas, akan mendapatkan Badges yang menunjukkan tingkat pembelajaran dan akumulasi poin yang telah didapat / dikumpulkan. Tingkatan Badges tersebut adalah sebagai berikut :<br><br>

                                	<div class="table-responsive">
                                    <table class="MsoTableGrid" border=1 cellspacing=0 cellpadding=0
                                     style='border-collapse:collapse;border:none;margin-left:6.0pt;margin-right:
                                     6.0pt'>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><b><i><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Badges
                                      Stage</span></i></b></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border:solid windowtext 1.0pt;
                                      border-left:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><b><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Poin</span></b></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border:solid windowtext 1.0pt;
                                      border-left:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><b><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Dapat
                                      dipersamakan dengan (<i>Equivalent</i>)</span></b></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Egg</span></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>0</span></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>&nbsp;</span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Baby</span></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>300</span></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>13 jam <i>Coaching</i>
                                      atau 75 jam <i>MyLearning</i></span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Junior</span></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>700</span></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>29 jam <i>Coaching</i> 
                                      atau 175 jam <i>MyLearning</i></span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Teen </span></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>1200</span></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>50 jam <i>Coaching</i>
                                      atau 300 jam <i>MyLearning</i></span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Curious</span></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>1800</span></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>75 jam <i>Coaching</i>
                                      atau 450 jam <i>MyLearning</i></span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Smart</span></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>2500</span></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>104 jam <i>Coaching</i>
                                      atau 625 jam <i>MyLearning</i></span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Adventurous</span></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>3300</span></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>138 jam <i>Coaching</i>
                                      atau 825 jam <i>MyLearning</i></span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Hunter</span></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>4200</span></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>175 jam <i>Coaching</i>
                                      atau 1050 jam <i>MyLearning</i></span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Philosophical</span></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>5400</span></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>225 jam <i>Coaching</i>
                                      atau 1350 jam <i>MyLearning</i></span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Legend</span></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>6800</span></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>283 jam <i>Coaching</i>
                                      atau 1700 jam <i>MyLearning</i></span></p>
                                      </td>
                                     </tr>
                                    </table>
                                  </div>

                                </li>
                                <li>Akumulasi poin dalam jumlah tertentu dapat ditukarkan dengan penghargaan (reward) dalam bentuk voucher dengan nilai sesuai tabel di bawah ini.<br/><br/>
                                	<div class="table-responsive">
                                    <table class="MsoTableGrid" border=1 cellspacing=0 cellpadding=0 width=167
                                     style='width:166.5pt;border-collapse:collapse;border:none'>
                                     <tr>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      background:#A6A6A6;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Poin</span></b></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border:solid windowtext 1.0pt;
                                      border-left:none;background:#A6A6A6;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Reward</span></b></p>
                                      </td>
                                     </tr>
                                     <tr style='height:3.85pt'>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>100</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:justify'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>Voucher Rp 100.000,-</span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>200</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:justify'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>Voucher Rp 200.000,-</span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>300</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:justify'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>Voucher Rp 300.000,-</span></p>
                                      </td>
                                     </tr>
                                    </table>
                                  </div>

                                </li>
                                <li>Nilai voucher dapat diubah sewaktu-waktu berdasarkan pertimbangan kemampuan perusahaan dan anggaran yang tersedia.</li>
                                <li>Perubahan Nilai voucher diusulkan oleh Division Head People Development, dan disetujui oleh Group Head HR Development dan Direktur & Chief Human Resources Officer atau penamaan jabatan lain yang setara.</li>
                                <li>Mata Anggaran yang digunakan untuk pemberian penghargaan yaitu MTA Z9TM020000 - 6040501000</li>
                            </ol>
                            <h3>Mekanisme Penukaran voucher adalah sebagai berikut :</h3>
                            <ol start="1">
                                <li>Karyawan mengajukan jumlahPoin yang akan ditukarkan dengan voucher,melalui aplikasi yang berlaku Apabila aplikasi yang berlakubelum tersedia, Karyawan dapat mengajukan melalui Form Penukaran Poin sebagaimana terlampir.Selanjutnya, jika aplikasi sudah dapat diakses, maka pengajuan dilakukan melalui aplikasi tanpa harus mengubah Kebijakan ini.</li>
                                <li>Poin yang telah ditukarkan dengan voucher (redeemed) akan mengurangi jumlah poin yang dimiliki Karyawan <em>(Point Wallet)</em> untuk penukaran poin selanjutnya, namun jumlah poin untuk pencapaian <em>Badges Stage </em>karyawan akan tetap terakumulasi. </li>
                                <li>Masa berlaku poin yang dapat ditukarkan dengan voucheradalah 2 tahun.</li>
                                <li>Divisi People Development (atau penamaan lain yang setara) memberikan persetujuan atau penolakan pengajuan penukaran poin yang disampaikan Karyawan, </li>
                                <li>Voucher dalam bentuk voucher code akan dikirimkan ke Karyawan setelah pengajuan disetujui oleh Division Head People Development (atau penamaan lain yang setara).</li>
                            </ol>
                            @if ($item['point'] > $user['point'])
                                <button type="button" class="btn btn-gray disabled btn-redeem" disabled>Your point are not enough</button>
                            @elseif(empty($item['quota']))
                                <button type="button" class="btn btn-gray disabled btn-redeem" disabled>The item quota was empty</button>
                            @else
                                <form action="{{route('trainee.point.store',$item['id'])}}" method="post">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$item['id']}}">
                                    <button type="submit" class="btn btn-default btn-redeem">Redeem now</button>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <button type="button" class="btn btn-float btn-fba">
                <i class="icon-back"></i>
            </button>
        </div>

        @if ($helper->inStaff())
            @include('sidebar')
        @endif
    </div>

</main>
@endsection
@section('styles')
<style>
	.dashboard-breadcrumb .btn.btn-clean {
		transition: all .15s ease-in-out;
		margin-left: 15px;
		color: #777;
	}
</style>
@endsection
@section('scripts')
    <script src="{{ url('js/point.js') }}"></script>
    @if (session('message'))
        <script>
            alert('{{session('message')}}');
        </script>
    @endif
@endsection
