@php($user = \Sentinel::check())
@php($helper = new \App\Helpers\UserRolesHelper )

@extends($helper->inAdmin() ? 'core.admin.main' :'core.trainee.main')
@section('head')
    <link rel="stylesheet" href="{{url('css/admin/dashboard/app.css')}}">
@endsection
@section('content')
    <main class="main-content">
        <div class="main-dashboard">
            <div class="dashboard-content">
                <div class="dashboard-breadcrumb">
                    <div class="container-fluid">
                        <ul class="list-unstyled top-breadcrumb">
                            <li>
                                <a href="{{route('trainee.dashboard.index')}}">
                                    <i class="icon-home"></i>
                                    Dashboard
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="top-point">
                    <div class="container">
                        <div class="dashboard-title">
                            <h1>My Course Points</h1>
                            <div class="point-guide">
                                <p class="mb-0">
                                Points are earned by employee who learn through MyLearning by taking courses or participating in webinars, and employees who share their knowledge or skill through giving coaching / mentoring / training
                                </p>
                            </div>
                            <button type="button" class="btn btn-default btn-redeem">Redeem Points</button>
                        </div>
                    </div>
                </div>


                <div class="container">
                    <div class="point-page">

                        <div class="mid-point">
                            <div class="point-count">
                                <i class="icon-reward"></i>
                                <h3>Redeemable Points</h3>
                                <p>{{ number_format($user['point'],0,',','.') }}<span>Points</span></p>
                            </div>
                            <div class="point-earn">
                                <h3>Points Earned on last Courses</h3>
                                <div class="point-list">
                                    @foreach($point_history as $history)
                                        <div class="point-item">
                                            <i class="{{ ($history['icon']) ? $history['icon'] : 'icon-book' }}"></i> 
                                            <div class="numeric">{{$history['point']}} <span>Points</span></div>

                                            @if(!empty($history['name']))
                                                <a href="{{route('trainee.previewByUserId',['user_id' => $user['id'],'id'=>$history['requests_group_id']])}}" target="_blank">
                                                    <p>{{ $history['name'] }}</p>
                                                </a>
                                            @else
                                                <p>{{ $history['description'] }}</p>
                                            @endif
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <ul class="nav nav-pills nav-pills-icons mt-24 px-0 default-tab-list" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active show" href="#item-redeem" role="tab" data-toggle="tab" aria-selected="true">
                                    Item Redeem
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#voucher-redeem" role="tab" data-toggle="tab" aria-selected="true">
                                    Voucher Redeem
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active show" id="item-redeem">
                                <div class="bottom-point">
                                    <div class="sub-title">
                                        <h3>Redeem Points</h3>
                                        <div class="sub-options">
                                            <div class="filter-opt">
                                                {{-- <p>Filter Result:</p> --}}
                                                <button class="btn btn-clean active" id="allPoints">All Points</button>
                                                <button class="btn btn-clean" data-sort="small" id="small">Small</button>
                                                <button class="btn btn-clean" data-sort="big" id="big">Big</button>
                                                <button class="btn btn-clean" data-sort="popular" id="popular">Popular</button>
                                            </div>
                                            <div class="show-opt">
                                                {{-- <p>View as</p> --}}
                                                <button type="button" class="btn btn-clean grid">
                                                    <i class="mdi mdi-grid"></i>
                                                </button>
                                                <button type="button" class="btn btn-clean list">
                                                    <i class="mdi mdi-format-list-checkbox"></i>
                                                </button>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="point-box">
                                        <div class="box grid">
                                            @foreach($items as $key=>$item)
                                                <div class="card" data-popular="{{$item['counter']}}" data-small="{{$item['point']}}" data-big="{{$item['point']}}">
                                                    <div class="redeem-item">
                                                        <div class="redeem-thumbnail w-100">
                                                            <img src="{{asset('uploads/'.$item['picture'])}}" alt="" class="img-fluid">
                                                        </div>
                                                        <div class="redeem-text">
                                                            <h4>{{$item['name']}}</h4>
                                                            <p>{{$item['description'] != "" ? $item['description'] : "Tidak ada" }}</p>
                                                        </div>
                                                        <div class="redeem-action ">
                                                            <div class="point-numbers">
                                                                <i class="icon-profits"></i>
                                                                <p>{{$item['point']}} <span>Points</span></p>
                                                            </div>
                                                            
                                                            <button type="button" class="btn btn-default mx-auto d-block mt-2 mb-4" onclick="location.href='{{route('trainee.point.show',$item['id'])}}'">Redeem item</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="voucher-redeem">
                                <div class="bottom-point">
                                    <div class="sub-title">
                                        <h3>Voucher Redeem</h3>
                                        <div class="sub-options">
                                            <div class="filter-opt">
                                                {{-- <p>Filter Result:</p> --}}
                                                <button class="btn btn-clean active" id="allPoints">All Points</button>
                                                <button class="btn btn-clean" data-sort="small" id="small">Small</button>
                                                <button class="btn btn-clean" data-sort="big" id="big">Big</button>
                                                <button class="btn btn-clean" data-sort="popular" id="popular">Popular</button>
                                            </div>
                                            <div class="show-opt">
                                                {{-- <p>View as</p> --}}
                                                <button type="button" class="btn btn-clean grid">
                                                    <i class="mdi mdi-grid"></i>
                                                </button>
                                                <button type="button" class="btn btn-clean list">
                                                    <i class="mdi mdi-format-list-checkbox"></i>
                                                </button>
                                            </div>


                                        </div>
                                    </div>

                                    <div class="point-box">
                                        <div class="box grid">
                                            @foreach($vouchers as $key => $voucher)
                                                <div class="card" data-popular="{{$key}}" data-small="{{$voucher['point']}}" data-big="{{$voucher['point']}}">
                                                    <div class="redeem-item">
                                                        <div class="redeem-thumbnail w-100">
                                                            <img src="{{asset('uploads/'.$voucher['image'])}}" alt="" class="img-fluid">
                                                        </div>
                                                        <div class="redeem-text">
                                                            <h4>{{ $voucher['name'] }}</h4>
                                                            <p>{{$voucher['description'] != "" ? $voucher['description'] : "Tidak ada" }}</p>
                                                        </div>
                                                        <div class="redeem-action ">
                                                            <div class="point-numbers">
                                                                <i class="icon-profits"></i>
                                                                <p>{{$voucher['point']}} <span>Points</span></p>
                                                            </div>
                                                            
                                                            <button type="button" class="btn btn-default mx-auto d-block mt-2 mb-4" onclick="location.href='{{route('trainee.voucher.show',$voucher['id'])}}'">Redeem Voucher</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        

                    </div>
                </div>
                <button type="button" class="btn btn-float btn-fba">
                    <i class="icon-back"></i>
                </button>
            </div>
            @if ($helper->inStaff())
                @include('sidebar')
            @endif
        </div>
    </main>
@endsection
@section('styles')
<style>
	ul.top-breadcrumb li:nth-last-child(1) { border-left: none transparent }
</style>
@endsection
@section('scripts')
    <script src="{{ url('js/point.js') }}"></script>
@endsection
