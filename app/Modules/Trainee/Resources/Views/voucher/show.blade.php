@php($user = \Sentinel::check())
@php($helper = new \App\Helpers\UserRolesHelper )

@extends($helper->inAdmin() ? 'core.admin.main' :'core.trainee.main')
@section('head')
    <link rel="stylesheet" href="{{url('css/admin/dashboard/app.css')}}">
@endsection
@section('content')
    <main class="main-content">
    <div class="main-dashboard">
        <div class="dashboard-content">
            <div class="dashboard-breadcrumb clean">
                <div class="container-fluid">
                    <div class="sub-breadcrumb">
                        <div class="sub-left">
                            <button type="button" class="btn btn-clean" onclick="location.href='{{route('trainee.point.redeem')}}'">
                                <i class="icon-back"></i>
                            		Redeem Points
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="redeem-page">
                    <div class="sub-title">
                        <h3>{{ $voucher['name'] }}</h3>
                    </div>
                    <div class="redeem-entry">
                        <div class="top-redeem">
                            <figure>
                                <img src="{{asset('uploads/'.$voucher['image'])}}" alt="" class="img-fluid">
                            </figure>
                            <div class="right-redeem">
                                <div class="redeem-spec">
                                    <div class="item-spec">
                                        <h4>Voucher Description</h4>
                                        <p>
                                            {{$voucher['description']}}
                                        </p>
                                    </div>
                                    <div class="item-bottom">
                                        <div class="point-compare">
                                            <div class="item-point clearfix">
                                                <i class="icon-best"></i>
                                                <h4>Points</h4>
                                                <h3>{{number_format($voucher['point'],0,',','.')}}<span>Points</span></h3>
                                            </div>
                                            <div class="item-point clearfix">
                                                <i class="icon-reward"></i>
                                                <h4>My Points</h4>
                                                <h3>{{ number_format($user['point'],0,',','.') }}<span>Points</span></h3>
                                            </div>
                                            <p>
                                                <a href="javascript:void(0);" class="redeemOpt">Redeem option</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bottom-redeem clearfix">
                            <h3>Terms and conditions</h3>
                            <ol start="1">
                                <li>Penghargaan diberikan sebagai bentuk apresiasi dari Perusahaan kepada Karyawan yang telah berpartisipasi dalam program pengembangan kompetensi sebagai Pengajar Internal / Online Trainer/Speaker / Moderator / Asisten Pengajar / Coach / Mentor, dan juga bagi Karyawan yang menyelesaikan <em>e-Learning Course</em> di platform MyLearning. Penghargaan diberikan dalam bentuk voucher berdasarkan poin yang telah didapat / dikumpulkan.</li>
                                <li>Perolehan Poin <br/><br/>

                                    <div class="table-responsive">
                                    	<table class="table MsoTableGrid" border=1 cellspacing=0 cellpadding=0 width=500
                                     style='width:436.45pt;;border-collapse:collapse;border:
                                     none; text-align: center;'>
                                     <tr style='height:10.95pt'>
                                       <td style='width:436.45pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'> Peran / Role</td>
                                       <td style='width:436.45pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'> Poin / Jam</td>
                                       <td style='width:600pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Perolehan Poin</td>
                                       <td style='width:436.45pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Source Platform</td>
                                     </tr>
                                     <tr>
                                       <td style='width:94.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:11.8pt'>
                                      Pengajar Internal<br>(<em>Internal Trainer</em>)</td>
                                      <td style='width:94.5pt;border:solid windowtext 1.0pt;
                                      border-top:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt;height:11.8pt'>24 Poin</td>
                                      <td rowspan="4" style='width:94.5pt;border:solid windowtext 1.0pt;
                                      border-top:none; border-right:solid windowtext 1.0pt;'>Setelah submit “Done” di MyDev.</td>
                                       <td rowspan="4" style='width:94.5pt;border:solid windowtext 1.0pt;
                                      border-top:none; border-right:solid windowtext 1.0pt;'>MyDev atau aplikasi lain yang fungsinya setara.</td>
                                     </tr>
                                     <tr>
                                       <td style='width:94.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:11.8pt'>Online Trainer / Speaker</td>
                                      <td style='width:94.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:11.8pt'>24 Poin</td>
                                     </tr>
                                     <tr>
                                       <td style='width:94.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:11.8pt'>Moderator dan Asisten Pengajar</td>
                                      <td style='width:94.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:11.8pt'>15 Poin</td>
                                     </tr>
                                     <tr>
                                       <td style='width:94.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:11.8pt'>Coach dan Mentor</td>
                                      <td style='width:94.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:11.8pt'>24 Poin</td>
                                     </tr>
                                     <tr>
                                       <td style='width:94.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:11.8pt'>Peserta e-Learning Course</td>
                                      <td style='width:94.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:11.8pt'>4 Poin</td>
                                      <td style='width:94.5pt;border:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:11.8pt'>Dihitung dari total durasi dari 1 course yang telah diselesaikan.</td>
                                       <td style='width:94.5pt;border:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:11.8pt'> MyLearning atau aplikasi lain yang fungsinya setara.</td>
                                     </tr>



                                     
                                    </table>
                                  </div>
                                </li>
                                <li>Setiap Karyawan yang telah berpartisipasi sesuai butir 2 di atas, akan mendapatkan Badges yang menunjukkan tingkat pembelajaran dan akumulasi poin yang telah dikumpulkan. Tingkatan Badges tersebut adalah sebagai berikut <br><br>


                                    <div class="table-responsive">
                                    	<table class="table MsoTableGrid" border=1 cellspacing=0 cellpadding=0
                                     style='border-collapse:collapse;border:none;margin-left:6.0pt;margin-right:
                                     6.0pt'>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><b><i><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Badges
                                      Stage</span></i></b></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border:solid windowtext 1.0pt;
                                      border-left:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><b><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Poin</span></b></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border:solid windowtext 1.0pt;
                                      border-left:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><b><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Dapat
                                      dipersamakan dengan (<i>Equivalent</i>)</span></b></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Egg</span></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>0</span></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>&nbsp;</span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Baby</span></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>300</span></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>13 jam <i>Coaching</i>
                                      atau 75 jam <i>MyLearning</i></span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Junior</span></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>700</span></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>29 jam <i>Coaching</i> 
                                      atau 175 jam <i>MyLearning</i></span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Teen </span></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>1200</span></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>50 jam <i>Coaching</i>
                                      atau 300 jam <i>MyLearning</i></span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Curious</span></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>1800</span></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>75 jam <i>Coaching</i>
                                      atau 450 jam <i>MyLearning</i></span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Smart</span></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>2600</span></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>104 jam <i>Coaching</i>
                                      atau 625 jam <i>MyLearning</i></span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Adventurous</span></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>3300</span></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>138 jam <i>Coaching</i>
                                      atau 825 jam <i>MyLearning</i></span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Hunter</span></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>4200</span></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>175 jam <i>Coaching</i>
                                      atau 1050 jam <i>MyLearning</i></span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Philosophical</span></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>5400</span></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>225 jam <i>Coaching</i>
                                      atau 1350 jam <i>MyLearning</i></span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=90 valign=top style='width:89.75pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>Legend</span></p>
                                      </td>
                                      <td width=59 valign=top style='width:58.5pt;border-top:none;border-left:none;
                                      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>6800</span></p>
                                      </td>
                                      <td width=243 valign=top style='width:243.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoNormal align=center style='text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;color:black'>283 jam <i>Coaching</i>
                                      atau 1700 jam <i>MyLearning</i></span></p>
                                      </td>
                                     </tr>
                                    </table>
                                  </div>

                                </li>
                                <li>Akumulasi poin dalam jumlah tertentu dapat ditukarkan dengan penghargaan (reward) dalam bentuk voucher dengan nilai konversi sesuai tabel di bawah ini :<br/><br/>

                                    <div class="table-responsive">
                                      <table class="table MsoTableGrid" border=1 cellspacing=0 cellpadding=0 width=500
                                     style='width:436.45pt;;border-collapse:collapse;border:
                                     none; text-align: center;'>
                                     <tr style='height:10.95pt'>


                                       <td style='width:436.45pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt' rowspan="2">
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Aktivitas</td>



                                       <td style='width:436.45pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt' rowspan="2">
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'> Poin / Jam</td>


                                       <td style='width:600pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt' colspan="4">
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Konversi Reward ke Poin</td>


                                      
                                     </tr>

                                      <tr style='height:10.95pt'>
                                       

                                       <td style='width:600pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Tier 5</td>


                                       <td style='width:436.45pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Tier 4</td>

                                        <td style='width:436.45pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Tier 3</td>

                                        <td style='width:436.45pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Tier 2 & 1</td>
                                     </tr>

                                     <tr style='height:3.85pt'>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Pengajar Internal (Internal Trainer)</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt' rowspan="4">
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>24</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt' rowspan="4">
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>180</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt' rowspan="4">
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>120</span></p>
                                      </td>
                                       <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt' rowspan="4">
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>72</span></p>
                                      </td>
                                       <td style='width:94.5pt;border:solid windowtext 1.0pt;
                                      border-top:none; border-right:solid windowtext 1.0pt;' rowspan="4">
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>24</span></p>
                                      </td>
                                     </tr>


                                      <tr style='height:3.85pt'>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Online Trainer / Speaker</span></p>
                                      </td>
                                      
                                     </tr>


                                      <tr style='height:3.85pt'>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Coach</span></p>
                                      </td>
                                      
                                     </tr>


                                      <tr style='height:3.85pt'>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Mentor</span></p>
                                      </td>
                                      
                                     </tr>


                                      <tr style='height:3.85pt'>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Online Moderator</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>15</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt' colspan="4">
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>15</span></p>
                                      </td>
                                     </tr>


                                     <tr style='height:3.85pt'>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>e-Learning Course</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center;'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>4</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt' colspan="4">
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>4</span></p>
                                      </td>
                                     </tr>




                                     
                                    </table>


                                     <table class="table MsoTableGrid" border=1 cellspacing=0 cellpadding=0 width=500
                                     style='width:436.45pt;;border-collapse:collapse;border:
                                     none; text-align: center;'>
                                     <tr style='height:10.95pt'>


                                       <td style='width:436.45pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt' rowspan="2">
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Aktivitas</td>



                                       <td style='width:436.45pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt' rowspan="2">
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'> Poin / Jam</td>


                                       <td style='width:600pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt' colspan="4">
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Konversi Reward ke Jam</td>


                                      
                                     </tr>

                                      <tr style='height:10.95pt'>
                                       

                                       <td style='width:600pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Tier 5</td>


                                       <td style='width:436.45pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Tier 4</td>

                                        <td style='width:436.45pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Tier 3</td>

                                        <td style='width:436.45pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Tier 2 & 1</td>
                                     </tr>

                                     <tr style='height:3.85pt'>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Pengajar Internal (Internal Trainer)</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt' rowspan="4">
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>24</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt' rowspan="4">
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>7.5</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt' rowspan="4">
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>5.0</span></p>
                                      </td>
                                       <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt' rowspan="4">
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>3.0</span></p>
                                      </td>
                                       <td style='width:94.5pt;border:solid windowtext 1.0pt;
                                      border-top:none; border-right:solid windowtext 1.0pt;' rowspan="4">
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>1.0</span></p>
                                      </td>
                                     </tr>


                                      <tr style='height:3.85pt'>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Online Trainer / Speaker</span></p>
                                      </td>
                                      
                                     </tr>


                                      <tr style='height:3.85pt'>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Coach</span></p>
                                      </td>
                                      
                                     </tr>


                                      <tr style='height:3.85pt'>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Mentor</span></p>
                                      </td>
                                      
                                     </tr>


                                      <tr style='height:3.85pt'>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Online Moderator</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>15</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt' colspan="4">
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>1.0</span></p>
                                      </td>
                                     </tr>


                                     <tr style='height:3.85pt'>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>e-Learning Course</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center;'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>4</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt' colspan="4">
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>1.0</span></p>
                                      </td>
                                     </tr>




                                     
                                    </table>

                                    <table class="table MsoTableGrid" border=1 cellspacing=0 cellpadding=0 width=500
                                     style='width:436.45pt;;border-collapse:collapse;border:
                                     none; text-align: center;'>
                                     <tr style='height:10.95pt'>


                                       <td style='width:436.45pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt' rowspan="2">
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Aktivitas</td>



                                       <td style='width:436.45pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt' rowspan="2">
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'> Poin / Jam</td>


                                       <td style='width:600pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt' colspan="4">
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Konversi Reward ke Nilai Voucher</td>


                                      
                                     </tr>

                                      <tr style='height:10.95pt'>
                                       

                                       <td style='width:600pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Tier 5</td>


                                       <td style='width:436.45pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Tier 4</td>

                                        <td style='width:436.45pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Tier 3</td>

                                        <td style='width:436.45pt;border:solid windowtext 1.0pt;
                                      background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.95pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Tier 2 & 1</td>
                                     </tr>

                                     <tr style='height:3.85pt'>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Pengajar Internal (Internal Trainer)</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt' rowspan="4">
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>24</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt' rowspan="4">
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>Rp. 180.000,-</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt' rowspan="4">
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>Rp. 120.000,-</span></p>
                                      </td>
                                       <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt' rowspan="4">
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>Rp. 72.000,-</span></p>
                                      </td>
                                       <td style='width:94.5pt;border:solid windowtext 1.0pt;
                                      border-top:none; border-right:solid windowtext 1.0pt;' rowspan="4">
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>Rp, 24.000,-</span></p>
                                      </td>
                                     </tr>


                                      <tr style='height:3.85pt'>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Online Trainer / Speaker</span></p>
                                      </td>
                                      
                                     </tr>


                                      <tr style='height:3.85pt'>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Coach</span></p>
                                      </td>
                                      
                                     </tr>


                                      <tr style='height:3.85pt'>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Mentor</span></p>
                                      </td>
                                      
                                     </tr>


                                      <tr style='height:3.85pt'>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Online Moderator</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>15</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt' colspan="4">
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>Rp. 72.000,-</span></p>
                                      </td>
                                     </tr>


                                     <tr style='height:3.85pt'>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>e-Learning Course</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center;'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>4</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt' colspan="4">
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:center'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>Rp. 4000,-</span></p>
                                      </td>
                                     </tr>




                                     
                                    </table>


                                    <!--

                                    	<table class="table MsoTableGrid" border=1 cellspacing=0 cellpadding=0 width=167
                                     style='width:166.5pt;border-collapse:collapse;border:none'>
                                     <tr>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      background:#A6A6A6;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Poin</span></b></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border:solid windowtext 1.0pt;
                                      border-left:none;background:#A6A6A6;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Reward</span></b></p>
                                      </td>
                                     </tr>
                                     <tr style='height:3.85pt'>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>100</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt'>
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:justify'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>Voucher Rp 100.000,-</span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>200</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:justify'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>Voucher Rp 200.000,-</span></p>
                                      </td>
                                     </tr>
                                     <tr>
                                      <td width=50 valign=top style='width:49.5pt;border:solid windowtext 1.0pt;
                                      border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoListParagraph align=center style='margin-left:0cm;text-align:
                                      center'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>300</span></p>
                                      </td>
                                      <td width=117 valign=top style='width:117.0pt;border-top:none;border-left:
                                      none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                      padding:0cm 5.4pt 0cm 5.4pt'>
                                      <p class=MsoListParagraph style='margin-left:0cm;text-align:justify'><span
                                      style='font-size:10.0pt;font-family:"Arial",sans-serif'>Voucher Rp 300.000,-</span></p>
                                      </td>
                                     </tr>
                                    </table>
                                  -->
                                  </div>

                                  <p><i>Keterangan</i>
                                    <br>
                                    <i>- Tier ditentukan dari hasil Feedback Evaluation Form </i><br>
                                    <i>-  *) Tidak diberlakukan pengelompokan / Tier.</i>

                                  </p>

                                </li>
                                <li>Nilai voucher dapat diubah sewaktu-waktu berdasarkan pertimbangan kemampuan Perusahaan dan anggaran yang tersedia.</li>
                                <li>Perubahan Nilai Voucher diusulkan oleh VP – Head of People Development & Culture Engagement dan disetujui oleh SVP – Head of HR Development dan Direktur & Chief Human Resources Officer atau penamaan lain yang setara</li>
                                <li>Mata Anggaran yang digunakan untuk pemberian penghargaan yaitu MTA Z9TM020000 – 6040501000.</li>
                            </ol>
                            <h3>Mekanisme Penukaran voucher adalah sebagai berikut :</h3>
                            <ol start="1">
                                <li>Jumlah minimal poin yang dapat ditukarkan dengan voucher adalah 100 poin dan kelipatannya. Karyawan mengajukan jumlah Poin yang akan ditukarkan dengan voucher, melalui platform MyDev atau aplikasi lain yang fungsinya setara.</li>
                                <li>Poin yang telah ditukarkan dengan Voucher (redeemed) akan mengurangi jumlah poin yang dimiliki Karyawan <i>(Point Wallet)</i> untuk penukaran poin selanjutnya, namun jumlah poin untuk pencapaian Badges Stage Karyawan akan tetap terakumulasi.</li>
                                <li>Masa berlaku poin yang dapat ditukarkan adalah 2 tahun.</li>
                                <li>Divisi People Development & Culture Engagement (atau penamaan lain yang setara) memberikan persetujuan atau penolakan pengajuan penukaran poin yang disampaikan Karyawan.</li>
                                <li>Voucher dalam bentuk voucher code akan dikirimkan ke karyawan setelah pengajuan disetujui oleh VP – Head of People Development & Culture Engagement (atau penamaan lain yang setara).</li>
                            </ol>
                            @if ($voucher['point'] > $user['point'])
                                <button type="button" class="btn btn-gray disabled btn-redeem" disabled>Your point are not enough</button>
                            @else
                                <form action="{{route('trainee.voucher.store',$voucher['id'])}}" method="post">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$voucher['id']}}">
                                    <button type="submit" class="btn btn-default btn-redeem">Redeem now</button>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <button type="button" class="btn btn-float btn-fba">
                <i class="icon-back"></i>
            </button>
        </div>

        @if ($helper->inStaff())
            @include('sidebar')
        @endif
    </div>

</main>
@endsection
@section('styles')
<style>
	.dashboard-breadcrumb .btn.btn-clean {
		transition: all .15s ease-in-out;
		margin-left: 15px;
		color: #777;
	}
</style>
@endsection
@section('scripts')
    <script src="{{ url('js/point.js') }}"></script>
    @if (session('message'))
        <script>
            alert('{{session('message')}}');
        </script>
    @endif
@endsection
