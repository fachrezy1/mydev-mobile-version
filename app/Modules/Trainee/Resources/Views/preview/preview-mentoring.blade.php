<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


@if (session('completed'))

    <script>

    $(document).ready(function(){
  
     swal("Good job!", "Give Point successfull!", "success");

    
  
});


</script>

                                     
@endif

<div class="review-item short">
{{--    @dd($form_detail )--}}
    <div class="sub-title">{{str_replace('_',' ',ucwords($form_type))}}/Coaching Form</div>
    <div class="center-form">
        @if (isset($form_detail['type']))
        <div class="form-group reset split">
            <label class="control-label">
                Type
            </label>
            <div class="check-group">
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="training_ti_is_training" type="checkbox" value="coaching" {{$form_detail['type'] == 'coaching' ? 'checked' : ''}} disabled> Coaching
                        <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="training_ti_is_course" type="checkbox" value="mentoring" {{$form_detail['type'] == 'mentoring' ? 'checked' : ''}} disabled> Mentoring
                        <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                    </label>
                </div>
            </div>
        </div>
        @endif
        @if($form_detail['form_group_id'] != 6)
        <div class="form-group reset split">
            <label class="control-label">
                Mentor/Coach Name
            </label>
            <div class="form-field">
                <input type="text" class="form-control" value="{{isset($form_detail['line_manager_name']) ? $form_detail['line_manager_name'] : ''}}" disabled>
            </div>
        </div>
        <div class="form-group reset split">
            <label class="control-label">
                Mentor/Coach Nik
            </label>
            <div class="form-field">
                <input type="text" class="form-control" value="{{isset($form_detail['line_manager_nik']) ? $form_detail['line_manager_nik'] : ''}}" disabled>
            </div>
        </div>
        @endif
        <div class="form-group reset split">
            <label class="control-label">
                Date Start
            </label>
            <div class="form-field">
                <input type="text" class="form-control datetimepicker2" value="{{isset($form_detail['start_date']) ? $form_detail['start_date'] : ''}}" disabled><br>
            </div>
            <label class="control-label">
                Date End
            </label>
            <div class="form-field">
                <input type="text" class="form-control datetimepicker2" value="{{isset($form_detail['end_date']) ? $form_detail['end_date'] : ''}}" disabled>
            </div>
        </div>

        @if($form_detail['form_group_id'] != 6)
        <div class="form-group reset split">
            <label class="control-label">
                Development Goal
            </label>
            <div class="form-field">
                <textarea class="form-control" rows="6" disabled>{{isset($form_detail['development_goal']) ? $form_detail['development_goal'] : ''}}</textarea>
                <p class="form-helper text-count">{{ isset($form_detail['development_goal']) ? strlen($form_detail['development_goal']) : 0  }}/255</p>
            </div>
        </div>
        <div class="form-group reset split">
            <label class="control-label">
                <button type="button" data-toggle="modal" class="btn-question" data-target="#competencyModal">
                    <i class="icon-questions-circular-button" >  </i>
                </button>
                Competency to be Developed
            </label>
            <div class="form-field mb-3">
                <select id="competency_select" class="form-control select2-comp" name="training_obj_employee_competency[]" multiple="multiple" disabled readonly>
                    @if(isset($competency_selected))
                        @foreach($competency_selected as $competency)
                            <option value="{{$competency}}" selected>{{$competency}} </option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        @endif
        @if($form_detail['form_group_id'] == 6)
            <div class="form-group reset split">
                <label class="control-label">
                    Venue
                </label>
                <div class="form-field">
                    <input type="text" class="form-control" value="{{isset($form_detail['line_venue']) ? $form_detail['line_venue'] : ''}}" disabled>
                </div>
            </div>
            <div class="form-group reset split">
                <label class="control-label">
                    Notes
                </label>
                <div class="form-field">
                    <textarea class="form-control" rows="6" disabled>{{isset($form_detail['notes']) ?$form_detail['notes'] :'' }}</textarea>
                    <p class="form-helper text-count">{{isset($form_detail['notes']) ? strlen($form_detail['notes']) : '0'}}/255</p>
                </div>
            </div>
        @endif
        @if($rating)
            <div class="form-group reset split">
                <label class="control-label">
                    Mentor Rating
                </label>
                <div class="check-group">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" name="training_ti_is_training" type="checkbox" value="coaching" {{$rating['rating'] == 1 ? 'checked' : ''}} readonly disabled> 1
                            <span class="form-check-sign">
                        <span class="check"></span>
                      </span>
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" name="training_ti_is_course" type="checkbox" {{$rating['rating'] == 2 ? 'checked' : ''}} value="mentoring" disabled> 2
                            <span class="form-check-sign">
                        <span class="check"></span>
                      </span>
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" name="training_ti_is_course" type="checkbox" {{$rating['rating'] == 3 ? 'checked' : ''}} value="mentor" disabled> 3
                            <span class="form-check-sign">
                        <span class="check"></span>
                      </span>
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" name="training_ti_is_course" type="checkbox" {{$rating['rating'] == 4 ? 'checked' : ''}} value="mentor" disabled> 4
                            <span class="form-check-sign">
                        <span class="check"></span>
                      </span>
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" name="training_ti_is_course" type="checkbox" {{$rating['rating'] == 5 ? 'checked' : ''}} value="mentor" disabled> 5
                            <span class="form-check-sign">
                        <span class="check"></span>
                      </span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group reset split">
                <label class="control-label">
                    Mentor Review
                </label>
                <div class="form-field">
                    <textarea class="form-control" rows="6" disabled>{{$rating['notes']}}</textarea>
                    <p class="form-helper text-count">{{isset($rating['notes']) ? strlen($rating['notes']) : '0'}}/255</p>
                </div>
            </div>
        @endif
    </div>

    @if( ($helper->inAdmin() && $rating && !$form['form_detail']['is_completed_by_admin']) && ($form['form_detail']['form_group_id'] == 2) )
        <form id="formApprovePoint" action="{{route('trainee.request.approvePoint')}}" method="post">
            @csrf
            <input type="hidden" name="form_group_id" value="{{$form['form_detail']['form_group_id']}}">
            <input type="hidden" name="request_group_id" value="{{$form['form_detail']['request_group_id']}}">
            <input type="hidden" name="mentor" value="{{isset($form['form_detail']['line_manager_nik']) ? $form['form_detail']['line_manager_nik'] : ''}}">
            <input type="hidden" name="total_hours_review" value="{{$total_hours_review}}">
        </form>
    @endif
    @php
        $reviews_date
            = $form_type
            = $reviews_goal
            = $reviews_result
            = $reviews_feedback
            = $reviews_action
            = $reviews_course_name
            = $reviews_session_number
            = $reviews_duration
            = $reviews_condition
            = $reviews_committed_date
            = $reviews_criteria
            = $reviews_project_title
            = $reviews_location
            = $reviews_provider
            = $reviews_country
            = $reviews_topic
            = $reviews_end_date
            = $reviews_cost
            = $reviews_attachment
            = null;
        $is_disabled = null;
    @endphp

    @if ($reviews[0])

        @php
            $data_exists = true;
            $reviews_date = $reviews[0]['reviews_date'];
            $reviews_goal = $reviews[0]['reviews_goal'];
            $reviews_result = $reviews[0]['reviews_result'];
            $reviews_feedback = $reviews[0]['reviews_feedback'];
            $reviews_action = $reviews[0]['reviews_action'];
            $reviews_course_name = $reviews[0]['reviews_course_name'];
            $reviews_session_number = $reviews[0]['reviews_session_number'];
            $reviews_duration = $reviews[0]['reviews_duration'];

            $reviews_condition = $reviews[0]['reviews_condition'];
            $reviews_committed_date = $reviews[0]['reviews_committed_date'];

            $reviews_criteria = $reviews[0]['reviews_criteria'];
            $reviews_project_title = $reviews[0]['reviews_project_title'];

            $reviews_location = $reviews[0]['reviews_location'];
            $reviews_provider = $reviews[0]['reviews_provider'];
            $reviews_country = $reviews[0]['reviews_country'];
            $reviews_topic = $reviews[0]['reviews_topic'];
            $reviews_end_date = $reviews[0]['reviews_end_date'];
            $reviews_cost = $reviews[0]['reviews_cost'];

            $reviews_attachment = $reviews[0]['reviews_attachment'];
            unset($reviews[0]);
            $current_data = $reviews;

        @endphp
    @if($form_detail['form_group_id'] == 6)
            @include('trainee::reviews.review-internal_coach')
        @else
            @include('trainee::reviews.review-mentoring')
        @endif
    @endif
    @include('modal-competency')

</div>
@section('scripts')
<script type="text/javascript">
    function format (d) {
            content = [];
            for (i=0;i<d.length;i++){
                content.push('<tr><td>'+d[i].name+'</td>'+
                '<td>'+d[i].definition+'</td>'+
                '</tr>');
            }
            tableCompetenciesRet = '<table class="table-bordered">'+
                        content.toString().replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '') +
                    '</table>';
            return tableCompetenciesRet;
            ;
        }
        var datatableComp = $('#tableCompetencies').DataTable( {
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {
                url: "{{ route('trainee.dtCompetencies') }}",
            },
            order: [ 1, 'asc' ],
            columns: [
                { data: 'category' },
                { data: 'competency', render: function ( data ) {
                    return format(data);
                }}
            ],
        });
</script>
@endsection