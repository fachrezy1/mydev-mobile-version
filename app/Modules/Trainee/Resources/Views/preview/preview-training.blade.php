{{--@dd($form_detail)--}}
<div class="review-item short">
    <div class="sub-title">{{str_replace('_',' ',ucwords($form_type))}} Form</div>
    <div class="center-form">
        <div class="form-group reset split">
            <label class="control-label">
                Training Title
            </label>
            {{--@dd($form_detail)--}}
            <div class="form-field">
                <input type="text" class="form-control" value="{{isset($form_detail['title']) ? $form_detail['title'] : ''}}" disabled>
            </div>
        </div>
        <div class="form-group reset split">
            {{--@dd($form_detail)--}}
            @if($form_detail['form_group_id'] == 'Public Training' || $form_detail['form_group_id'] == 3)
                <label class="control-label">
                    <button type="button" data-toggle="modal" class="btn-question" data-target="#typeProgramsModal">
                        <i class="icon-questions-circular-button" >  </i>
                    </button>
                    Type of programs
                </label>
                <div class="check-group">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" name="training_ti_is_training" type="radio" value="true" {{isset($form_detail['type_programs']) && $form_detail['type_programs'] == 'general training' ? 'checked' : ''}} disabled> General Training
                            <span class="form-check-sign">
                            <span class="check"></span>
                          </span>
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" name="training_ti_is_certification" type="radio" value="true" {{isset($form_detail['type_programs']) && $form_detail['type_programs'] == 'technical certification' ? 'checked' : ''}} disabled> Professional/Technical Certification
                            <span class="form-check-sign">
                            <span class="check"></span>
                          </span>
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" name="training_ti_is_certification" type="radio" value="true" {{isset($form_detail['type_programs']) && $form_detail['type_programs'] == 'seminar certification' ? 'checked' : ''}} disabled> Seminar/Conference
                            <span class="form-check-sign">
                            <span class="check"></span>
                          </span>
                        </label>
                    </div>
                </div>
            @else
                <label class="control-label">
                    Type of programs
                </label>
                <div class="check-group">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" name="training_ti_is_training" type="radio" value="true" {{isset($form_detail['type_programs']) && $form_detail['type_programs'] == 'training' ? 'checked' : ''}} disabled> Training
                            <span class="form-check-sign">
                            <span class="check"></span>
                          </span>
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" name="training_ti_is_certification" type="radio" value="true" {{isset($form_detail['type_programs']) && $form_detail['type_programs'] == 'certification' ? 'checked' : ''}} disabled> Certification
                            <span class="form-check-sign">
                            <span class="check"></span>
                          </span>
                        </label>
                    </div>
                </div>
            @endif
        </div>
        <div class="form-group reset split">
            <label class="control-label">
                Training Provider
            </label>
            <div class="form-field">
                <input type="text" class="form-control" value="{{isset($form_detail['provider']) ? $form_detail['provider'] : ''}}" disabled>
            </div>
        </div>
        <div class="form-group reset split">
            <label class="control-label">
                Cost
            </label>
            <div class="form-field">
                <input type="text" class="form-control" value="Rp. {{ isset($form_detail['cost']) ? number_format($form_detail['cost'],0,',','.') : ''}}" disabled>
            </div>
        </div>
        <div class="form-group reset split">
            <label class="control-label">
                Date Start
            </label>
            <div class="form-field">
                <input type="text" class="form-control datetimepicker2" value="{{isset($form_detail['start_date']) ? $form_detail['start_date'] : ''}}" disabled><br>
            </div>
            <label class="control-label">
                Date End
            </label>
            <div class="form-field">
                <input type="text" class="form-control datetimepicker2" value="{{isset($form_detail['end_date']) ? $form_detail['end_date'] : ''}}" disabled>
            </div>
        </div>

        <div class="form-group reset split">
            <label class="control-label">
                Training Objective
            </label>
            <div class="form-field">
                <textarea class="form-control" rows="6" disabled>{{ isset($form_detail['description']) ? $form_detail['description'] : '' }}</textarea>
                <p class="form-helper text-count">{{ isset($form_detail['description']) ? strlen($form_detail['description']) : '0'}}/255</p>
            </div>
        </div>
        {{--@dd($form_detail)--}}
        <div class="form-group reset split">
            <label class="control-label">
                Venue
            </label>
            <div class="check-group">
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="training_obj_venue" type="checkbox" value="training_obj_venue_domestic" {{( isset($form_detail['venue'])  && $form_detail['venue'] == 'training_obj_venue_domestic') ? 'checked' : ''}} disabled> Domestic
                        <span class="form-check-sign">
                            <span class="check"></span>
                          </span>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="training_obj_venue" type="checkbox" value="training_obj_venue_overseas" {{ ( isset($form_detail['venue']) && $form_detail['venue'] == 'training_obj_venue_overseas' )  ? 'checked' : ''}} disabled> Overseas
                        <span class="form-check-sign">
                            <span class="check"></span>
                          </span>
                    </label>
                </div>
            </div>
        </div>
{{--        @dd()--}}
        @if($form_detail['rating'])
        <div class="form-group reset split">
            <label class="control-label">
                Overall Rating
            </label>
            <div class="form-field">
                <input type="text" class="form-control" value="{{$form_detail['rating']}}" disabled>
                <a href="javascript:void(0)" id="cloneCloseDiv" data-toggle="modal" data-target="#ratingModal" class="btn btn-default remove-item"> Rating detail</a>

            </div>
        </div>
        @endif
        @if (isset($form_detail['employee_name']))
        <!-- <div class="form-group reset bmd-form-group">
        <label class="control-label">
            Preview Employees
        </label>
        <table id="importTable" class="table table-hover table-striped table-lite" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Employee Names</th>
                <th>Employee Nik</th>
                <th>Employee Position</th>
            </tr>
            </thead>

            <tbody id="import-list">
                @php
                    $name = unserialize($form_detail['employee_name']);
                    $nik = unserialize($form_detail['employee_nik']);
                    $position = unserialize($form_detail['employee_position']);
                @endphp
                @for($i=0;$i<count($name);$i++)
                    <tr>
                        <td>{{$name[$i]}}</td>
                        <td>{{$nik[$i]}}</td>
                        <td>{{$position[$i]}}</td>
                    </tr>
                @endfor
            </tbody>
        </table>

        </div> -->
        @endif

        <div class="form-group reset">
            <label class="control-label">
                <button type="button" data-toggle="modal" class="btn-question" data-target="#competencyModal">
                    <i class="icon-questions-circular-button" >  </i>
                </button>
                Competency to be Developed
            </label>
            <div class="form-field mb-3">
                <select class="form-control select2-comp" name="training_obj_employee_competency[]" multiple="multiple" disabled readonly>
                    @if(isset($form_detail['employee_competency']))
                        @foreach(unserialize($form_detail['employee_competency']) as $competency)
                        <option value="{{$competency}}" selected>{{$competency}} </option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        @if($form_detail['form_group_id'] != 5)
        <div class="form-group reset">
            <div class="form-field">
                <div class="multifield">
                    <label class="control-label">
                        Commitment After Training
                    </label>
                    <textarea name="training_obj_commitment_training" class="form-control" rows="2" placeholder="enter text" disabled readonly required>{{isset($form_detail['commitment_training']) ? implode(", ",unserialize($form_detail['commitment_training'])) : ''}}</textarea>
                    <p class="form-helper text-count">0/255</p>
                </div>
                <div class="multifield">
                    <label class="control-label">
                        Committed Date
                    </label>
                    <div class="field-date">
                        <div class="date-input">
                            <input type="text" name="training_obj_start_date2" autocomplete="off" class="form-control start-date2" onchange="dateStartSelector(this)" value="{{isset($form_detail['start_date2']) ? $form_detail['start_date2'] : ''}}" placeholder="enter start date" disabled readonly required>
                            <button type="button" class="btn btn-default btn-icon ">
                                <i class="mdi mdi-calendar"></i>
                            </button>
                        </div>
                        To
                        <div class="date-input">
                            <input type="text" name="training_obj_end_date2" autocomplete="off" class="form-control end-date2" placeholder="enter end date" value="{{isset($form_detail['end_date2']) ? $form_detail['end_date2'] : ''}}" disabled readonly required>
                            <button type="button" class="btn btn-default btn-icon">
                                <i class="mdi mdi-calendar"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="multifield">
                    <label class="control-label">
                        Commitment Description
                    </label>
                    <textarea name="training_obj_commitment_description" class="form-control" rows="2" placeholder="enter text" disabled readonly required>{{isset($form_detail['commitment_description']) ? $form_detail['commitment_description'] : ''}}</textarea>
                    <p class="form-helper text-count">0/255</p>
                </div>
            </div>
        </div>
        @endif
        @if(isset($form_detail['attachment']))
        <div class="form-group">
            <div class="form-field">
                <a href="{{isset($form_detail['attachment']) ? asset('uploads/'.$form_detail['attachment']) : ''}}">
                    <button type="button" class="btn btn-default reset">
                        <label for="files" style="margin-bottom: 0;width: 100%; padding: 0;"><i class="icon-contact"></i> Download attachment</label>
                    </button><br>
                    <small>{{isset($form_detail['attachment']) ? $form_detail['attachment'] : 'No Attachment'}}</small>
                </a>
            </div>
        </div>
        @else

            <div class="form-group">
                <div class="form-field d-flex justify-content-center">
                    <button type="button" class="btn btn-dark  disabled">
                        <label class="text-white" for="files" style="margin-bottom: 0;width: 100%; padding: 0;"><i class="icon-contact text-white"></i> Attachment not available</label>
                    </button>
                </div>
            </div>
        @endif
    </div>
    @include('modal-competency')
    @include('modal-type-programs')
    @include('modal-rating')
</div>
@include('modal-competency')
