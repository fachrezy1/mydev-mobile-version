<div class="review-item short">
    {{--@dd($reviews )--}}
    <div class="sub-title">{{str_replace('_',' ',ucwords($form_type))}} Form</div>
    <div class="center-form">
        <div class="form-group reset">
            <label class="control-label">
                Development Type
            </label>
            <div class="check-group">
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="assignment_dev_type" type="radio" value="project" {{$form_detail['type']=='project'?'checked':''}} readonly disabled required> Project
                        <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="assignment_dev_type" type="radio" value="internship" {{$form_detail['type']=='internship'?'checked':''}} readonly disabled required> Cross Functional Assignment
                        <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="assignment_dev_type" type="radio" value="globaltalent" {{(isset($form_detail['opco'])) ?'checked':''}} readonly disabled required> Global Talent Mobility / National Talent Mobility
                        <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group reset">
            <label class="control-label">
                <button type="button" data-toggle="modal" class="btn-question" data-target="#competencyModal">
                    <i class="icon-questions-circular-button" >  </i>
                </button>
                Competency to be Developed
            </label>
            <div class="form-field mb-3">
                <select class="form-control select2-tags" name="assignment_obj_employee_competency[]" multiple="multiple" disabled readonly>
                    @if(isset($form_detail['employee_competency']))
                        @foreach(unserialize($form_detail['employee_competency']) as $competency)
                        <option value="{{$competency}}" selected>{{$competency}} </option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>

        @if($form_detail['type'] == 'project')
        <div class="project-group">
            <div class="form-group reset ">
                <label class="control-label">Project Leader</label>
                <input name="assignment_pg_project_leader" type="text" class="form-control select-employee-name" value="{{$form_detail['project_leader']}}" readonly disabled required>
            </div>

            <div class="form-group reset">
                <label class="control-label">Project Title</label>
                <input name="assignment_pg_project_title" type="text" class="form-control" value="{{$form_detail['project_title']}}" readonly disabled required>
            </div>

            <div class="form-group reset">
                <label class="control-label">Project Date</label>
                <div class="field-date">
                    <div class="date-input">
                        <input name="assignment_pg_start_date" type="text" autocomplete="off" class="form-control start-date" value="{{$form_detail['start_date']}}" readonly disabled placeholder="Start date">
                        <button type="button" class="btn btn-default btn-icon "><i class="mdi mdi-calendar"></i></button>
                    </div>

                    To

                    <div class="date-input">
                        <input name="assignment_pg_end_date" type="text" autocomplete="off" class="form-control end-date" value="{{$form_detail['end_date']}}" readonly  disabled placeholder="End date">
                        <button type="button" class="btn btn-default btn-icon"><i class="mdi mdi-calendar"></i></button>
                    </div>
                </div>
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Project Description
                </label>
                <textarea rows="6" name="assignment_pg_project_description" class="form-control" maxlength="255" placeholder="Enter Mapped to Indosat Ooredoo Competency" readonly disabled required>{{$form_detail['project_description']}}</textarea>
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Development Goal
                </label>
                <textarea rows="6" name="assignment_pg_development_goal" class="form-control" maxlength="255" placeholder="Enter Mapped to Indosat Ooredoo Competency" required disabled> {{$form_detail['development_goal']}} </textarea>
            </div>
        </div>
        @elseif($form_detail['type'] == 'internship')
        <div class="internship">

            <div class="form-group reset ">
                <label class="control-label">Group</label>
                <input name="assignment_inter_group" type="text" readonly disabled value="{{ (isset($form_detail['group'])) ? $form_detail['group'] : null }}" class="form-control">
            </div>

            <div class="form-group reset">
                <label class="control-label">Division</label>
                <input name="assignment_inter_division" type="text" readonly disabled value="{{(isset($form_detail['division'])) ? $form_detail['division'] : null}}" class="form-control">
            </div>

            <div class="form-group reset">
                <label class="control-label">Date</label>
                <div class="field-date">
                    <div class="date-input">
                        <input name="assignment_inter_start_date" type="text" autocomplete="off" class="form-control start-date" readonly disabled value="{{ (isset($form_detail['start_date'])) ? $form_detail['start_date'] : null }}" placeholder="Start date">
                        <button type="button" class="btn btn-default btn-icon "><i class="mdi mdi-calendar"></i></button>
                    </div>

                    To

                    <div class="date-input">
                        <input name="assignment_inter_end_date" type="text" autocomplete="off" class="form-control end-date" readonly disabled value="{{ (isset($form_detail['end_date'])) ? $form_detail['end_date'] : null }}" placeholder="End date">
                        <button type="button" class="btn btn-default btn-icon"><i class="mdi mdi-calendar"></i></button>
                    </div>
                </div>
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Mentor name
                </label>
                <input name="assignment_inter_mentor_name" type="text" readonly value="{{ (isset($form_detail['mentor_name'])) ? $form_detail['mentor_name'] : null }}" class="form-control select-employee-name" disabled required>
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Mentor NIK
                </label>
                <input name="assignment_inter_mentor_nik" type="text" id="nik_coachee" class="form-control nik_coachee" readonly value="{{ (isset($form_detail['mentor_nik'])) ? $form_detail['mentor_nik'] : null }}" disabled required>
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Development Goal
                </label>
                <textarea rows="6" name="assignment_inter_development_goal" class="form-control" maxlength="255" placeholder="" readonly disabled> {{ (isset($form_detail['development_goal'])) ? $form_detail['development_goal'] : null }}</textarea>
            </div>
        </div>
        @else
        <div class="globaltalent">

            <div class="form-group reset ">
                <label class="control-label">Directorat/ Group/ OPCO Designation</label>
                <input name="assignment_gt_opco" type="text" readonly disabled value="{{ (isset($form_detail['opco'])) ? $form_detail['opco'] : null }}" class="form-control">
            </div>

            <div class="form-group reset">
                <label class="control-label">
                    Assigment Type
                </label>
                <input name="assignment_gt_type" id="shortterm_opt" data-val="shortterm" type="radio" value="shortterm" {{(isset($form_detail['type']) && $form_detail['type']=="shortterm") ? 'checked' : ''}}><label for="shortterm_opt" class="form-check-label">&nbsp;&nbsp;Short Term (3-12 Months)</label>&nbsp;
                <input name="assignment_gt_type" id="longterm_opt" data-val="longterm" type="radio" value="longterm" {{(isset($form_detail['type']) && $form_detail['type']=="longterm") ? 'checked' : ''}}><label for="longterm_opt" class="form-check-label">&nbsp;&nbsp;Long Term (1-2 Years)</label>
            </div>

            <div class="form-group reset">
                <label class="control-label">Date</label>
                <div class="field-date">
                    <div class="date-input">
                        <input name="assignment_gt_start_date" type="text" autocomplete="off" class="form-control start-date" readonly disabled value="{{ (isset($form_detail['start_date'])) ? $form_detail['start_date'] : null }}" placeholder="Start date">
                        <button type="button" class="btn btn-default btn-icon "><i class="mdi mdi-calendar"></i></button>
                    </div>

                    To

                    <div class="date-input">
                        <input name="assignment_gt_end_date" type="text" autocomplete="off" class="form-control end-date" readonly disabled value="{{ (isset($form_detail['end_date'])) ? $form_detail['end_date'] : null }}" placeholder="End date">
                        <button type="button" class="btn btn-default btn-icon"><i class="mdi mdi-calendar"></i></button>
                    </div>
                </div>
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Assigment Purpose
                </label>
                <input readonly disabled name="assignment_gt_purpose" data-val="Development" type="radio" value="Development" {{ (isset($form_detail['purpose']) && $form_detail['purpose']=="Development") ? "checked" : "" }}><label>&nbsp;&nbsp;Development</label>&nbsp;&nbsp;
                <input readonly disabled name="assignment_gt_purpose" data-val="Expert Skill Gap" type="radio" value="Expert Skill Gap" {{ (isset($form_detail['purpose']) && $form_detail['purpose']=="Expert Skill Gap") ? "checked" : "" }}><label>&nbsp;&nbsp;Expert Skill Gap</label>
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Reason For Mobility Request
                </label>
                <div id="Development_opt">
                    <label class="checkbox-inline"><input readonly disabled name="assignment_gt_reason[]" type="checkbox" value="Develop National Talent" {{ (isset($form_detail['reason']) && in_array("Develop National Talent", $form_detail['reason'])) ? "checked" : "" }}> Develop National Talent</label><br/>
                    <label class="checkbox-inline"><input readonly disabled name="assignment_gt_reason[]" type="checkbox" value="Exposure to New Technology" {{ (isset($form_detail['reason']) && in_array("Exposure to New Technology", $form_detail['reason'])) ? "checked" : "" }}> Exposure to New Technology</label><br/>
                    <label class="checkbox-inline"><input readonly disabled name="assignment_gt_reason[]" type="checkbox" value="Develop Management Skills" {{ (isset($form_detail['reason']) && in_array("Develop Management Skills", $form_detail['reason'])) ? "checked" : "" }}> Develop Management Skills</label><br/>
                    <label class="checkbox-inline"><input readonly disabled name="assignment_gt_reason[]" type="checkbox" value="Acquire New Skill Set" {{ (isset($form_detail['reason']) && in_array("Acquire New Skill Set", $form_detail['reason'])) ? "checked" : "" }}> Acquire New Skill Set</label><br/>
                    <label class="checkbox-inline"><input readonly disabled id="ag_gt_other" type="checkbox" value="Other" {{ (isset($form_detail['reason_other'])) ? "checked" : "" }}> Other</label>
                </div>
                <div id="skil_gap_opt">
                    <label class="checkbox-inline"><input readonly disabled name="assignment_gt_reason[]" type="checkbox" value="Business Expansion (New Role)" {{ (isset($form_detail['reason']) && in_array("Business Expansion (New Role)", $form_detail['reason'])) ? "checked" : "" }}> Business Expansion (New Role)</label><br/>
                    <label class="checkbox-inline"><input readonly disabled name="assignment_gt_reason[]" type="checkbox" value="Replacing Existing Role (Vacancy)" {{ (isset($form_detail['reason']) && in_array("Replacing Existing Role (Vacancy)", $form_detail['reason'])) ? "checked" : "" }}> Replacing Existing Role (Vacancy)</label><br/>
                    <label class="checkbox-inline"><input readonly disabled name="assignment_gt_reason[]" type="checkbox" value="Project Base (New Product Launch, Network Roll Out)" {{ (isset($form_detail['reason']) && in_array("Project Base (New Product Launch, Network Roll Out)", $form_detail['reason'])) ? "checked" : "" }}> Project Base (New Product Launch, Network Roll Out)</label><br/>
                    <label class="checkbox-inline"><input readonly disabled name="assignment_gt_reason[]" type="checkbox" value="Leadership Development/ Succession Program" {{ (isset($form_detail['reason']) && in_array("Leadership Development/ Succession Program", $form_detail['reason'])) ? "checked" : "" }}> Leadership Development/ Succession Program</label><br/>
                    <label class="checkbox-inline"><input readonly disabled name="assignment_gt_reason[]" type="checkbox" value="Strategic Resource Gap" {{ (isset($form_detail['reason']) && in_array("Strategic Resource Gap", $form_detail['reason'])) ? "checked" : "" }}> Strategic Resource Gap</label><br/>
                    <label class="checkbox-inline"><input readonly disabled id="ag_gt_other" type="checkbox" value="Other" {{ (isset($form_detail['reason_other'])) ? "checked" : "" }}> Other</label>
                </div>
                <div id="ag_gt_other_container">
                    <input readonly disabled type="text" value="{{ (isset($form_detail['reason_other'])) ? $form_detail['reason_other'] : '' }}" name="assignment_gt_reason_other" class="form-control">
                </div>
            </div>
            <div class="form-group reset">
                <div class="form-field">
                    <input disabled readonly id="files" type="file" accept=".pdf,doc,docx,image/*" name="assigment_gt_attachment" class="btn btn-default reset">
                    <button disabled readonly type="button" class="btn btn-default reset">
                        <label disabled readonly for="files" style="margin-bottom: 0;width: 100%; padding: 0;"><i class="icon-contact"></i> Documents</label>
                    </button><br>
                    {{ (isset($form_detail['attachment'])) ? $form_detail['attachment'] : "" }}
                </div>
            </div>
        </div>
        @endif
        @if($reviews)
            <div class="form-group reset">
                <label class="control-label">Report Date</label>
                <div class="field-date">
                    <div class="date-input">
                        <input name="assignment_inter_start_date" type="text" autocomplete="off" class="form-control start-date" readonly disabled value="{{$reviews['reviews_date']}}" placeholder="Start date">
                        <button type="button" class="btn btn-default btn-icon "><i class="mdi mdi-calendar"></i></button>
                    </div>

                    To

                    <div class="date-input">
                        <input name="assignment_inter_end_date" type="text" autocomplete="off" class="form-control end-date" readonly disabled value="{{$reviews['reviews_end_date']}}" placeholder="End date">
                        <button type="button" class="btn btn-default btn-icon"><i class="mdi mdi-calendar"></i></button>
                    </div>
                </div>
            </div>
        @endif

        @if($rating)
            <div class="form-group reset">
                <label class="control-label">Date</label>
                <div class="field-date">
                    <div class="date-input">
                        <input name="" type="text" autocomplete="off" class="form-control start-date" readonly disabled value="{{$rating['start_date']}}" placeholder="Start date">
                        <button type="button" class="btn btn-default btn-icon "><i class="mdi mdi-calendar"></i></button>
                    </div>

                    To

                    <div class="date-input">
                        <input name="" type="text" autocomplete="off" class="form-control end-date" readonly disabled value="{{$rating['end_date']}}" placeholder="End date">
                        <button type="button" class="btn btn-default btn-icon"><i class="mdi mdi-calendar"></i></button>
                    </div>
                </div>
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Rating
                </label>
                {{$rating['rating']}}/5
            </div>
            <div class="form-group reset">
                <label class="control-label">
                    Rating Notes
                </label>
                <textarea rows="6" name="assignment_inter_development_goal" class="form-control" maxlength="255" placeholder="" disabled readonly> {{$rating['notes']}}</textarea>
            </div>
        @endif
    </div>

</div>
@section('scripts')
<script type="text/javascript">

    $(document).on("click","input[name='assignment_gt_purpose']",function(){
        purpose = $(this).data("val");
        if (purpose=="Development"){
            $("#Development_opt").show();
            $("#skil_gap_opt").hide();
        } else {
            $("#Development_opt").hide();
            $("#skil_gap_opt").show();
        }
    });
    
    $("#longterm_opt").on("click",function(){
        $("#assignment_inter_division_gt").val("longterm");
    });

    $("#shortterm_opt").on("click",function(){
        $("#assignment_inter_division_gt").val("shortterm");
    });

    @if(!isset($form_detail['reason_other']))
    $("#ag_gt_other_container").hide();
    @endif

    $(document).on("click","#ag_gt_other",function(){
        if ($(this).prop("checked")) {
            $("#ag_gt_other_container").show();
        } else {
            $("#ag_gt_other_container").hide();
        }
    });

    @if(isset($form_detail['purpose']) && $form_detail['purpose']=="Development")
        $("#skil_gap_opt").hide();
    @else
        $("#Development_opt").hide();
    @endif

</script>
@endsection