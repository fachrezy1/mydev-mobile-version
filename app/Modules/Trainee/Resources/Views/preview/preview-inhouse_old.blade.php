<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 <link href="{{url('css/style_main.css')}}" type="text/css" rel="stylesheet">


@if (session('completed'))

    <script>

    $(document).ready(function(){
  
     swal("Good job!", "Give Point successfull!", "success");

    
  
});


</script>

                                     
@endif
<div class="review-item short">
    <div class="sub-title">{{str_replace('_',' ',ucwords($form_type))}} Form</div>
    <div class="center-form">
        <div class="form-group reset split">
            <label class="control-label">
                Training Title
            </label>

            <div class="form-field">
                <input type="text" class="form-control" value="{{$form_detail['title']}}" disabled>
            </div>
        </div>
        <div class="form-group reset split">

            <label class="control-label">
                Type of programs
            </label>
            <div class="check-group">
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="training_ti_is_training" type="radio" value="true" {{isset($form_detail['type_programs']) && $form_detail['type_programs'] == 'internal trainer' ? 'checked' : ''}} disabled> Internal Trainer
                        <span class="form-check-sign">
                        <span class="check"></span>
                      </span>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="training_ti_is_certification" type="radio" value="true" {{isset($form_detail['type_programs']) && $form_detail['type_programs'] == 'external trainer' ? 'checked' : ''}} disabled> External Trainer
                        <span class="form-check-sign">
                        <span class="check"></span>
                      </span>
                    </label>
                </div>

                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="training_ti_is_certification" type="radio" value="true" {{isset($form_detail['type_programs']) && $form_detail['type_programs'] == 'moderator' ? 'checked' : ''}} disabled> Moderator
                        <span class="form-check-sign">
                        <span class="check"></span>
                      </span>
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group reset split {{isset($form_detail['type_programs']) && $form_detail['type_programs'] == 'external trainer' ? '' : 'd-none'}}">
            <label class="control-label">
                Training Provider
            </label>
            <input type="text" name="training_ti_provider" class="form-control" maxlength="300" placeholder="enter name" value="{{isset($form_detail['provider']) ? $form_detail['provider'] : ''}}" disabled required>
        </div>
        <div class="form-group reset split {{isset($form_detail['type_programs']) && ($form_detail['type_programs'] == 'internal trainer' or $form_detail['type_programs'] == 'moderator') ? '' : 'd-none'}}">
            <label class="control-label text-left">
                Trainer name
            </label>
            <input name="training_obj_trainer_name" type="text" class="form-control select-employee-name" placeholder="enter name" value="{{isset($form_detail['trainer_name']) ? $form_detail['trainer_name'] : ''}}" disabled required>

            <label class="control-label text-left">
                Trainer Nik
            </label>
            <input name="training_obj_trainer_nik" type="text" id="nik_coachee" class="form-control nik_coachee" placeholder="example: IND-010119" value="{{isset($form_detail['trainer_nik']) ? $form_detail['trainer_nik'] : ''}}" disabled readonly required>

        </div>

        @if(!$helper->inStaff())
            <div class="form-group reset split">
                <label class="control-label">
                    Cost
                </label>
                <div class="form-field">
                    <input type="text" class="form-control" value="Rp. {{number_format($form_detail['cost'],0,',','.')}}" disabled>
                </div>
            </div>
        @endif

        <div class="form-group reset split">
            <label class="control-label">
                Total Hours
            </label>
            <div class="form-field">
                <input type="text" class="form-control" value="{{isset($form_detail['hours']) ? $form_detail['hours'].' Hour(s)' : 'filled by admin'}}" disabled>
            </div>
        </div>
        <div class="form-group reset split">
            <label class="control-label">
                Date Start
            </label>
            <div class="form-field">
                <input type="text" class="form-control datetimepicker2" value="{{$form_detail['start_date']}}" disabled><br>
            </div>
            <label class="control-label">
                Date End
            </label>
            <div class="form-field">
                <input type="text" class="form-control datetimepicker2" value="{{$form_detail['end_date']}}" disabled>
            </div>
        </div>

        <div class="form-group reset split">
            <label class="control-label">
                Training Objective
            </label>
            <div class="form-field">
                <textarea class="form-control" rows="6" disabled>{{ isset($form_detail['description']) ? $form_detail['description'] : '' }}</textarea>
                <p class="form-helper text-count">{{ isset($form_detail['description']) ? strlen($form_detail['description']) : '0'}}/255</p>
            </div>
        </div>
        <div class="form-group reset split">
            <label class="control-label">
                Venue
            </label>
            <div class="check-group">
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="training_obj_venue" type="checkbox" value="training_obj_venue_domestic" {{( isset($form_detail['venue'])  && $form_detail['venue'] == 'training_obj_venue_domestic') ? 'checked' : ''}} disabled> Domestic
                        <span class="form-check-sign">
                            <span class="check"></span>
                          </span>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="training_obj_venue" type="checkbox" value="training_obj_venue_overseas" {{ ( isset($form_detail['venue']) && $form_detail['venue'] == 'training_obj_venue_overseas' )  ? 'checked' : ''}} disabled> Overseas
                        <span class="form-check-sign">
                            <span class="check"></span>
                          </span>
                    </label>
                </div>
            </div>
        </div>
        {{--        @dd()--}}

         @php

             $rating_admin=App\Modules\Trainee\Models\ratingModel::where('request_group_id',$request_group_id)->where('notes','admin')->get();

         @endphp

         @if($form_detail['form_group_id'] == 5 and $helper->inAdmin() and count($rating_admin)>0)

         @php
     
               

               if(count($rating_admin)>0){


                   foreach($rating_admin as $rat){
                     
                       $rating_adm=$rat->rating;
                      
                   }

               }

         @endphp
         <div class="form-group reset split">
                <label class="control-label">
                    Rating Admin 
                </label>
                <div class="form-field">
                    <input type="text" class="form-control" value="{{number_format($rating_adm,1)}}" disabled>
                    <!--
                    <a href="javascript:void(0)" id="cloneCloseDiv" data-toggle="modal" data-target="#ratingModal" class="btn btn-default remove-item"> Rating detail</a>
                -->
                </div>
            </div>
        @endif

        @if($form_detail['rating'])
            <div class="form-group reset split">
                <label class="control-label">
                    Overall Rating
                </label>
                <div class="form-field">
                    <input type="text" class="form-control" value="{{$form_detail['rating']}}" disabled>
                    <a href="javascript:void(0)" id="cloneCloseDiv" data-toggle="modal" data-target="#ratingModal" class="btn btn-default remove-item"> Rating detail</a>
                </div>
            </div>
        @endif
        @if (isset($form_detail['employee_name']))
            <div class="form-group reset bmd-form-group">
                <label class="control-label">
                    Preview Employees
                </label>
                <table id="importTable" class="table table-hover table-striped table-lite" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Employee Names</th>
                        <th>Employee Nik</th>
                        <th>Employee Position</th>
                    </tr>
                    </thead>

                    <tbody id="import-list">
                    @php
                        $name = unserialize($form_detail['employee_name']);
                        $nik = unserialize($form_detail['employee_nik']);
                        $position = unserialize($form_detail['employee_position']);
                    @endphp
                    @for($i=0;$i<count($name);$i++)
                        <tr>
                            <td>{{$name[$i]}}</td>
                            <td>{{$nik[$i]}}</td>
                            <td>{{isset($position[$i]) ? $position[$i] : '-'}}</td>
                        </tr>
                    @endfor
                    </tbody>
                </table>

            </div>
        @endif

     


        <div class="form-group reset split">
            <label class="control-label">
                <button type="button" data-toggle="modal" class="btn-question" data-target="#competencyModal">
                    <i class="icon-questions-circular-button" >  </i>
                </button>
                Competency to be Developed
            </label>
            <div class="form-field mb-3">
                <select class="form-control select2-comp" name="training_obj_employee_competency[]" multiple="multiple" disabled readonly>
                    @if(isset($form_detail['employee_competency']))
                        @foreach(unserialize($form_detail['employee_competency']) as $competency)
                            <option value="{{$competency}}" selected>{{$competency}} </option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        @if($form_detail['form_group_id'] != 5)
            <div class="form-group reset split">
                <div class="form-field">
                    <div class="multifield">
                        <label class="control-label">
                            Commitment After Training
                        </label>
                        <textarea name="training_obj_commitment_training" class="form-control" rows="2" placeholder="enter text" disabled readonly required>{{isset($form_detail['commitment_training']) ? implode(", ",unserialize($form_detail['commitment_training'])) : ''}}</textarea>
                        <p class="form-helper text-count">0/255</p>
                    </div>
                    <div class="multifield">
                        <label class="control-label">
                            Committed Date
                        </label>
                        <div class="field-date">
                            <div class="date-input">
                                <input type="text" name="training_obj_start_date2" autocomplete="off" class="form-control start-date2" onchange="dateStartSelector(this)" value="{{isset($form_detail['start_date2']) ? $form_detail['start_date2'] : ''}}" placeholder="enter start date" disabled readonly required>
                                <button type="button" class="btn btn-default btn-icon ">
                                    <i class="mdi mdi-calendar"></i>
                                </button>
                            </div>
                            To
                            <div class="date-input">
                                <input type="text" name="training_obj_end_date2" autocomplete="off" class="form-control end-date2" placeholder="enter end date" value="{{isset($form_detail['end_date2']) ? $form_detail['end_date2'] : ''}}" disabled readonly required>
                                <button type="button" class="btn btn-default btn-icon">
                                    <i class="mdi mdi-calendar"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="multifield">
                        <label class="control-label">
                            Commitment Description
                        </label>
                        <textarea name="training_obj_commitment_description" class="form-control" rows="2" placeholder="enter text" disabled readonly required>{{isset($form_detail['commitment_description']) ? $form_detail['commitment_description'] : ''}}</textarea>
                        <p class="form-helper text-count">0/255</p>
                    </div>
                </div>
            </div>
        @endif
        @if(isset($form_detail['attachment']))
            <div class="form-group reset split">
                <div class="form-field">
                    <a href="{{isset($form_detail['attachment']) ? asset('uploads/'.$form_detail['attachment']) : ''}}">
                        <button type="button" class="btn btn-default reset">
                            <label for="files" style="margin-bottom: 0;width: 100%; padding: 0;"><i class="icon-contact"></i> Download attachment</label>
                        </button><br>
                        <small>{{isset($form_detail['attachment']) ? $form_detail['attachment'] : 'No Attachment'}}</small>
                    </a>
                </div>
            </div>
        @else

            <div class="form-group reset split">
                <div class="form-field">
                    <button type="button" class="btn btn-dark  disabled">
                        <label class="text-white" for="files" style="margin-bottom: 0;width: 100%; padding: 0;"><i class="icon-contact text-white"></i> Attachment not available</label>
                    </button>
                </div>
            </div>
        @endif
        @if(($helper->inAdmin() && $form_detail['rating']) && ($form_detail['type_programs'] == 'internal trainer' || $form_detail['type_programs'] == 'moderator') && !$form_detail['is_completed_by_admin'])

            <div class="form-group reset split">
                <div class="form-field">
                    <button data-toggle="modal" data-target="#completedModal" class="btn btn-default"> Complete & Give Point </button>
                </div>
            </div>
            
        @endif
        @if(($helper->inAdmin() && $form_detail['rating']) && $form_detail['type_programs'] == 'external trainer' && $form_detail['is_completed'] != 'Completed')
            <div class="form-group reset split">
                <div class="form-field">
                    <button onclick="location.href='{{ route('trainee.setDone',['form_id'=>$form['form_detail']['form_group_id'],'id'=>$form['form_detail']['request_group_id']]) }}'" class="btn btn-default"> Complete </button>
                </div>
            </div>
        @endif
    </div>

    

    @if( ($helper->inAdmin() && $form_detail['rating']) && ($form_detail['type_programs'] == 'internal trainer' || $form_detail['type_programs'] == 'moderator'))
        <div class="modal fade" id="completedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Complete & Give Point</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body modal-competency">
                        <div class="development-goal">
                            <div class="row">
                                  <div class="col-sm-12 col-md-12 col-lg-12 col-sm-12">
                                <form id="formApprovePoint" action="{{route('trainee.request.approvePoint')}}" method="post">
                                    @csrf
                                    <input type="hidden" name="form_group_id" value="{{$form['form_detail']['form_group_id']}}">
                                    <input type="hidden" name="request_group_id" value="{{$form['form_detail']['request_group_id']}}">
                                    <input type="hidden" name="mentor" value="{{$form['form_detail']['trainer_nik']}}">

                                    @if($form['form_detail']['type_programs'])
                                    <input type="hidden" name="type_programs" value="{{$form['form_detail']['type_programs']}}">
                                    @endif

                                    

                                    <div class="form-group reset">
                                        <label class="control-label">
                                            Tier
                                        </label>


                                        <div class="form-field">
                                           <select class="form-control" name="tier" required style="border-color: #DCDCDC;">
                                               <option value="{{$tier_2_1}}^1">Tier 2 & 1</option>
                                               <option value="{{$tier_3}}^3">Tier 3</option>
                                               <option value="{{$tier_4}}^4">Tier 4</option>
                                               <option value="{{$tier_5}}^5">Tier 5</option>
                                           </select>
                                        </div>
                                    </div>


                                    <div class="form-group reset">
                                        <label class="control-label">
                                            Total Hours
                                        </label>
                                        <div class="form-field">
                                            <input type="text" class="form-control" name="training_ti_hours" required style="border-color: rgba(229, 103, 23, 0.8);">
                                        </div>
                                    </div>

                                    <button type="submit" name="submit_button" form="formApprovePoint" class="btn btn-default"> Submit </button>
                                </form>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    


    @include('modal-competency')
    @include('modal-type-programs')
    @include('modal-rating')
</div>
@include('modal-competency')


