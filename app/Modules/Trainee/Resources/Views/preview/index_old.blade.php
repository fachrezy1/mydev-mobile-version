@php($user = \Sentinel::check())
@php($helper = new \App\Helpers\UserRolesHelper )
@extends($helper->inManager() ? 'core.admin.main' : ($helper->inAdmin() ? 'core.admin.main' : 'core.trainee.main') )
@section('head')
    <link rel="stylesheet" href="{{url('css/admin/dashboard/app.css')}}">
@endsection
@section('content')

    <style>
        .dataTables_filter{
            display:none !important;
        }
    </style>
    <main class="main-content">
        <div class="main-dashboard clean">
            <div class="dashboard-breadcrumb review">
                <div class="container-fluid">
                    <div class="sub-breadcrumb">
                        <div class="sub-left text-white">
                            <a href="{{ route( $helper->inManager() ? 'dashboard.traineeRequest' : ($helper->inAdmin() ? 'dashboard.traineeRequest' : 'trainee.myTrainee')) }}?page={{Request::get('page')}}&tab={{Request::get('tab')}}">
                            <button type="button" class="btn btn-clean" >
                                <i class="icon-back"></i>
                            </button>
                            <span><b class="{{$helper->inStaff() ? 'text-black' : 'text-white'}}">Back</b></span>
                            </a>
                        </div>
                    </div>
                    <h1 class="text-center">Preview</h1>
                </div>
            </div>
            <div class="review-top">
                <div class="container">
                    <div class="box-inline">
                        <div class="box-items">
                            <div class="profile-circle">
                                <div class="circle-thumbnail">
                                    <img src="{{url('images/profile.jpeg')}}" alt="" class="img-fluid">
                                </div>
                                <div class="vertical-content text-center">
                                    <h5>Requester</h5>
                                    <h3>{{$requester ? $requester['full_name'] : '' }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="box-items">
                            <div class="vertical-content">
                                <h5>Requester info</h5>
                            </div>
                            <div class="trainer-data">
                                <ul class="list-unstyled">
                                    <li>
                                        <i class="icon-user-4"></i> <p><strong>NIK</strong> {{$requester ? $requester['nik'] : ''}}</p>
                                    </li>
                                    <li>
                                        <i class="icon-user-4"></i> <p><strong>Position</strong> {{$requester ? $requester['position'] : '' }}</p>
                                    </li>
                                    <li>
                                        <i class="icon-user-4"></i> <p><strong>Division</strong> {{$requester ? $requester['division'] : '' }}</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        @if($helper->inManager() || $helper->inAdmin())
                            @if ($form_detail['is_accepted'] || $form_detail['is_rejected'] || $form_detail['accepted_by_me'])
                            <div class="box-items">
                                <div class="vertical-button">
                                    @if($form_detail['is_completed'] == 'Completed')
                                        {{--<a href="{{route('trainee.reviewByUserId',['user_id'=>$form_detail['user_id'],'id'=>$form_detail['request_group_id']])}}">--}}
                                            {{--<button type="button" class="btn btn-default" >--}}
                                                {{--See Review </button>--}}
                                        {{--</a>--}}
                                    @endif
                                    @if($form_detail['is_rejected'])
                                    <button type="submit"  class="btn btn-grey disabled" title="this trainee has been rejected" disabled>
                                        <i class="icon-close"></i>
                                        Has Rejected </button>
                                    @endif
                                </div>
                            </div>
                            @endif
                        @endif
                    </div>
                </div>

                
            </div> 
            <div class="container">
                @if($form_detail['form_group_id'] == 1)
                    @include('trainee::preview.preview-assignment')
                @elseif( $form_detail['form_group_id'] == 2 || $form_detail['form_group_id'] == 6)
                    @include('trainee::preview.preview-mentoring')
                @elseif($form_detail['form_group_id'] == 4)
                    @include('trainee::preview.preview-external')
                @elseif($form_detail['form_group_id'] == 3)
                    @include('trainee::preview.preview-training')
                @elseif($form_detail['form_group_id'] == 5)
                    @include('trainee::preview.preview-inhouse')
                @endif
            </div>
        </div>

        @if (!$helper->inStaff() && !$form_detail['is_accepted'] && !$form_detail['is_rejected'] && !$form_detail['accepted_by_me'])
            <div class="box-items call-to-action">
                <div class="preview-button">
                    <form action="{{route('request.acceptRequest')}}" method="post" class="w-100 d-flex justify-content-between">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$form_detail['user_id']}}">
                        <input type="hidden" name="id" value="{{$form_detail['request_group_id']}}">
                        <input type="hidden" name="page" value="{{Request::get('page')}}">
                        <input type="hidden" name="tab" value="{{Request::get('tab')}}">

                        <button title="reject this request" type="button" class="btn btn-default reject_button_trainee" data-requestgroup="{{$form_detail['request_group_id']}}" data-userid="{{$form_detail['user_id']}}" data-toggle="modal" data-target="#rejectNotes">
		                        <i class="icon-close"></i> Reject 
		                    </button>

                        <button type="submit" class="btn btn-default" title="approve this request">
                            <i class="icon-tick"></i> Approve
                        </button>
                    </form>

                    <div class="clearfix"></div>
                </div>
            </div>
        @endif
    </main>
    <div class="modal fade" id="rejectNotes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Are you sure to reject it?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('request.rejectRequest')}}" method="post">
                        @csrf
                        <input type="hidden" name="user_id" id="rejected_user_id" value="">
                        <input type="hidden" name="id" id="rejected_request_id" value="">
                        <input type="hidden" name="next_page" value="{{Request::get('page')}}">
                        <input type="hidden" name="tab" value="{{Request::get('tab')}}">  
                        <div class="form-group reset ">
                            <div class="form-field">
                                <textarea class="form-control" name="rejected_notes" rows="6" placeholder="Notes for rejected trainee"></textarea>
                                <p class="form-helper text-count">0/255</p>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between">
	                        <button type="button" class="btn btn-dark" data-dismiss="modal">Cancel</button>
	                        <button type="submit" class="btn btn-default"> Confirm </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<style type="text/css">
	@media (min-width: 768px){
		.main-dashboard {
			padding-top: 60px!important
		}

		.main-dashboard > .container {
			padding-top: 40px
		}
	}
	.review-top {
		margin-top: 0
	}
</style>
@endsection
@section('scripts')
    <script>
        @if (session('success'))
            window.alert('{{ session('success') }}')
        @endif
        @if (session('error'))
            window.alert('{{ session('error') }}')
        @endif
    </script>
@endsection
