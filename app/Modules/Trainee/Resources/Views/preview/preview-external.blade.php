<div class="review-item short">
    <div class="sub-title">{{str_replace('_',' ',ucwords($form_type))}} Form</div>
    <div class="center-form">
        <div class="form-group reset split">
            <label class="control-label">
                Employee Name
            </label>
            <div class="form-field">
                <input type="text" class="form-control" value="{{$form_detail['employee_name']}}" disabled>
            </div>
        </div>

        <div class="form-group reset split">
            <label class="control-label">
                Employee Nik
            </label>
            <div class="form-field">
                <input type="text" class="form-control" value="{{$form_detail['employee_nik']}}" disabled>
            </div>
        </div>
        <div class="form-group reset split">
            <label class="control-label">
                Topic
            </label>
            <div class="form-field">
                <input type="text" class="form-control" value="{{$form_detail['line_topic']}}" disabled>
            </div>
        </div>
        <div class="form-group reset split">
            <label class="control-label">
                Date Start
            </label>
            <div class="form-field">
                <input type="text" class="form-control datetimepicker2" value="{{$form_detail['start_date']}}" disabled><br>
            </div>
            <label class="control-label">
                Date End
            </label>
            <div class="form-field">
                <input type="text" class="form-control datetimepicker2" value="{{$form_detail['end_date']}}" disabled>
            </div>
        </div>

        <div class="form-group reset split">
            <label class="control-label">
                Venue
            </label>
            <div class="form-field">
                <input type="text" class="form-control" value="{{$form_detail['line_venue']}}" disabled>
            </div>
        </div>

        <div class="form-group reset split">
            <label class="control-label">
                Organization
            </label>
            <div class="form-field">
                <input type="text" class="form-control" value="{{$form_detail['organization']}}" disabled>
            </div>
        </div>
    </div>

</div>
