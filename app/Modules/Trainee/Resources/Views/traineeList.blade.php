@php($user = \Sentinel::check())
@php($helper = new \App\Helpers\UserRolesHelper )
@php($agent = new \Jenssegers\Agent\Agent())

@extends($helper->inAdmin() ? 'core.admin.main' :'core.trainee.main')
@section('head')
    <link rel="stylesheet" href="{{url('css/admin/dashboard/app.css')}}">
@endsection
@section('content')
    {{--@dd($agent->isMobile())--}}
    <main class="main-content">
        <div class="main-dashboard secondary-nav">
            <div class="dashboard-content w-100">
                <div class="container-fluid">

                    <div class="dashboard-entry">
                        
                        <div class="dashboard-title">
                            <h1>Trainee List</h1>
                        </div>

                        <div class="my-4 px-2">
                        	<i class="icon-exam"></i> You have <span>{{$requests['approvedCount']}} courses</span> to review. <a href="#myTrainee" class="customTabToggler" >Click here</a>
                        </div>

                        <!-- new -->
                        @if (session('message'))
                            <div class="dashboard-notify alert alert-success" style="margin-top: 10px;">
                                <div class="alert-icon">
                                    <i class="mdi mdi-comment-alert-outline"></i>
                                </div>
                                <div class="notify-content">
                                    <p>{{session('message')}}</p>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true"><i class="mdi mdi-close"></i></span>
                                    </button>
                                </div>
                            </div>
                        @endif
                        @if (session('success'))
                            <div class="dashboard-notify alert alert-success" style="margin-top: 10px;">
                                <div class="alert-icon">
                                    <i class="mdi mdi-comment-alert-outline"></i>
                                </div>
                                <div class="notify-content">
                                    <p>{{session('success')}}</p>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true"><i class="mdi mdi-close"></i></span>
                                    </button>
                                </div>
                            </div>
                        @endif
                        <!-- end new -->

                        <ul class="nav nav-pills nav-pills-icons" id="tab-nav" role="tablist">
                            <li class="nav-item inline">
                                <a class="nav-link active show myTrainee" href="#myTrainee" role="tab" data-toggle="tab" aria-selected="true">
                                    <i class="icon-portfolio-1"></i> <span>Waiting for approval</span>
                                </a>
                            </li>

                            @if ($helper->inStaff())
                                <li class="nav-item inline">
                                    <a class="nav-link myApproval" href="#myApproval" role="tab" data-toggle="tab" aria-selected="false">
                                        <i class="icon-checklist"></i> <span>Ongoing Progress</span>
                                    </a>
                                </li>
                            @endif
                            <li class="nav-item inline">
                                <a class="nav-link myHistory" href="#myHistory" role="tab" data-toggle="tab" aria-selected="false">
                                    <i class="icon-contract"></i> <span>Completed</span>
                                </a>
                            </li>
                            @if ($helper->inStaff())

                                <li class="nav-item inline">
                                    <a class="nav-link rejected" href="#rejected" role="tab" data-toggle="tab" aria-selected="false">
                                        <i class="icon-exit"></i> <span>Rejected</span>
                                    </a>
                                </li>
                            @endif
                            @if (!$helper->inAdmin())
                                <li class="nav-item inline" style="border: 1px dashed #ed1b24;">
                                    <a class="nav-link related" href="#related" role="tab" data-toggle="tab" aria-selected="false">
                                        <i class="icon-presentation"></i> <span>My Trainee</span>
                                    </a>
                                </li>
                            @endif
                        </ul>

                        <div class="tab-content tab-space">
                            <div class="tab-pane active show" id="myTrainee">
                                <div class="panel-box mt-24">
                                    <div class="panel-title">
                                        <h3>Waiting for approval List</h3>
                                        <div class="right-notif mx-auto mx-md-0">
                                            <div class="review-notif">
                                            </div>

                                            @if ($helper->inStaff())

                                                <a href="{{url(route('trainee.index'))}}">
                                                    <button type="button" class="btn btn-default">
                                                        <i class="icon-plus"></i> Create Request
                                                    </button>
                                                </a>
                                            @endif

                                            @if ($helper->inAdmin())
                                                <a href="{{url(route('trainee.index'))}}">
                                                    <button type="button" class="btn btn-default">
                                                        <i class="icon-plus"></i> Create Trainee
                                                    </button>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="panel-table">
                                        <div class="export-table export-mytrainee">
                                            <!--
                                            <select id="filterMonthWaitingApproval" class="form-control select-filter">
                                                <option value="">Filter Month</option>
                                                @for($i=1;$i<=12;$i++)
                                                    <option value="month-{{ date('m', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}">{{ date('F', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}</option>
                                                @endfor
                                            </select>
                                            -->
                                            <select id="filterMonthWaitingApproval" class="filterMonth form-control select-filter">
                                                <option value="">Filter Month</option>
                                                @for($i=1;$i<=12;$i++)
                                                    <option value="month-{{ date('m', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}">{{ date('F', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}</option>
                                                @endfor
                                            </select>
                                            <!--
                                            <select id="filterYearWaitingApproval" class="form-control select-filter">
                                                <option value="">Filter Year</option>
                                                @for($i=0;$i<=3;$i++)
                                                    <option value="year-{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                                @endfor
                                            </select>
                                            -->
                                            <select id="filterYearWaitingApproval" class="filterYear form-control select-filter">
                                                <option value="">Filter Year</option>
                                                @for($i=0;$i<=3;$i++)
                                                    <option value="year-{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                                @endfor
                                            </select>

                                            <a href="{{route('trainee.exportTrainee')}}?type=new" id="exportWaitingApproval" class="btn btn-default btn-sm btn-lite">
                                                <i class="icon-xlsx-file-format"></i> Excel
                                            </a>
                                        </div>
                                        <div class="clearfix"></div>
                                        
                                        <table class="traineeWaitingApprovalTable table table-hover table-striped table-responsive table-lite text-center" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="all" width="10px">No</th>
                                                <th class="all">Title/Topic</th>
                                                <th class="all" width="50px">Type</th>
                                                <th class="desktop">Employee Name</th>
                                                <th class="desktop">Line Manager</th>
                                                <th class="desktop" width="40px">Start</th>
                                                <th class="desktop" width="40px">End</th>
                                                @if ($helper->inStaff())
                                                    <th class="desktop">Manager</th>
                                                    <th class="desktop">HR.</th>
                                                @endif
                                                <th class="desktop">Type of programs</th>
                                                <th class="desktop">Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @if ($helper->inStaff())
                                <div class="tab-pane" id="myApproval">
                                    <div class="panel-box mt-24">
                                        <div class="panel-title">
                                            <h3>Ongoing Progress List</h3>
                                            <div class="right-notif mx-auto mx-md-0">
                                                @if ((isset($requests['approved'])) && $helper->inStaff() && ['approvedCount'] > 0)
                                                    <div class="review-notif">
                                                        <i class="icon-exam"></i> You have got

                                                        <span>{{$requests['approvedCount']}} course(s)</span>
                                                        need to add review. <a href="#myApproval" class="customTabToggler" >Click here</a>
                                                    </div>
                                                @endif

                                                @if ($helper->inStaff())

                                                        <a href="{{url(route('trainee.index'))}}">
                                                            <button type="button" class="btn btn-default">
                                                                <i class="icon-plus"></i> Create Request
                                                            </button>
                                                        </a>
                                                @endif

                                                @if ($helper->inAdmin())
                                                    <a href="{{url(route('trainee.index'))}}">
                                                        <button type="button" class="btn btn-default" onclick="location.href='{{url(route('trainee.index'))}}'">
                                                            <i class="icon-plus"></i> Create Trainee
                                                        </button>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="panel-table">
                                            <div class="export-table export-mytrainee">
                                                <!--
                                                <select id="filterMonthOngoing" class="form-control select-filter">
                                                    <option value="">Filter Month</option>
                                                    @for($i=1;$i<=12;$i++)
                                                        <option value="month-{{ date('m', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}">{{ date('F', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}</option>
                                                    @endfor
                                                </select>

                                                <select id="filterYearOngoing" class="form-control select-filter">
                                                    <option value="">Filter Year</option>
                                                    @for($i=0;$i<=3;$i++)
                                                        <option value="year-{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                                    @endfor
                                                </select>
                                                -->
                                                <select id="filterMonthOngoing" class="filterMonth form-control select-filter">
                                                <option value="">Filter Month</option>
                                                @for($i=1;$i<=12;$i++)
                                                    <option value="month-{{ date('m', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}">{{ date('F', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}</option>
                                                @endfor
                                                </select>

                                                <select id="filterYearOngoing" class="filterYear form-control select-filter">
                                                    <option value="">Filter Year</option>
                                                    @for($i=0;$i<=3;$i++)
                                                        <option value="year-{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                                    @endfor
                                                </select>
                                                
                                                <a href="{{route('trainee.exportTrainee')}}?type=approved" id="exportOngoing" class="btn btn-default btn-sm btn-lite">
                                                    <i class="icon-xlsx-file-format"></i> Excel
                                                </a>
                                            </div>
                                            <div class="clearfix"></div>

                                            <table class="traineeOngoingTable table table-hover table-striped table-responsive table-lite text-center" cellspacing="0" width="100%">
                                                <thead>
                                                <tr>
                                                    <th class="all">No</th>
                                                    <th class="all">Title/Topic</th>
                                                    <th class="all">Type</th>
                                                    <th class="desktop">Employee Name</th>
                                                    <th class="desktop">Line Manager</th>
                                                    <th class="desktop">Start</th>
                                                    <th class="desktop">End</th>
                                                    <th class="desktop">Manager</th>
                                                    <th class="desktop">HR.</th>
                                                    <th class="desktop"> Type of programs </th>
                                                    <th class="desktop">Action</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="tab-pane" id="myHistory">
                                <div class="panel-box mt-24">
                                    <div class="panel-title">
                                        <h3>History List</h3>
                                        <div class="right-notif mx-auto mx-md-0">
                                            @if ((isset($requests['approved'])) && $helper->inStaff() && ['approvedCount'] > 0)
                                                <div class="review-notif">
                                                    <i class="icon-exam"></i> You have got

                                                    <span>{{$requests['approvedCount']}} course(s)</span>
                                                    need to add review. <a href="#myApproval" class="customTabToggler" >Click here</a>
                                                </div>
                                            @endif

                                            @if ($helper->inStaff())
                                                <a href="{{url(route('trainee.index'))}}" >
                                                    <button type="button" class="btn btn-default">
                                                        <i class="icon-plus"></i> Create Request
                                                    </button>
                                                </a>
                                            @endif

                                            @if ($helper->inAdmin())
                                                <button type="button" class="btn btn-default" onclick="location.href='{{url(route('trainee.index'))}}'">
                                                    <i class="icon-plus"></i> Create Trainee
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="panel-table">
                                        <div class="export-table export-mytrainee">
                                            <!--
                                            <select id="filterMonthCompleted" class="form-control select-filter">
                                                <option value="">Filter Month</option>
                                                @for($i=1;$i<=12;$i++)
                                                    <option value="month-{{ date('m', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}">{{ date('F', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}</option>
                                                @endfor
                                            </select>

                                            <select id="filterYearCompleted" class="form-control select-filter">
                                                <option value="">Filter Year</option>
                                                @for($i=0;$i<=3;$i++)
                                                    <option value="year-{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                                @endfor
                                            </select>
                                            -->

                                            <select id="filterMonthCompleted" class="filterMonth form-control select-filter">
                                                <option value="">Filter Month</option>
                                                @for($i=1;$i<=12;$i++)
                                                    <option value="month-{{ date('m', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}">{{ date('F', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}</option>
                                                @endfor
                                            </select>

                                            <select id="filterYearCompleted" class="filterYear form-control select-filter">
                                                <option value="">Filter Year</option>
                                                @for($i=0;$i<=3;$i++)
                                                    <option value="year-{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                                @endfor
                                            </select>

                                            <a href="{{route('trainee.exportTrainee')}}?type=completed" id="exportCompleted" class="btn btn-default btn-sm btn-lite">
                                                <i class="icon-xlsx-file-format"></i> Excel
                                            </a>
                                        </div>
                                        <div class="clearfix"></div>

                                        <table class="traineeCompletedTable table table-hover table-striped table-responsive table-lite text-center" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="all">No</th>
                                                <th class="all">Title/Topic</th>
                                                <th class="all">Type</th>
                                                <th class="desktop">Employee Name</th>
                                                <th class="desktop">Line Manager</th>
                                                <th class="desktop">Start</th>
                                                <th class="desktop">End</th>
                                                @if ($helper->inStaff())
                                                    <th class="desktop">HR.</th>
                                                    <th class="desktop">Manager</th>
                                                @endif
                                                <th class="desktop">Rating Mentor/Coach</th>
                                                <th class="desktop">Type of Programs</th>
                                                <th class="desktop">Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="rejected">
                                <div class="panel-box mt-24">
                                    <div class="panel-title">
                                        <h3>Rejected List</h3>
                                        <div class="right-notif mx-auto mx-md-0">
                                            @if ((isset($requests['approved'])) && $helper->inStaff() && ['approvedCount'] > 0)
                                                <div class="review-notif">
                                                    <i class="icon-exam"></i> You have got

                                                    <span>{{$requests['approvedCount']}} course(s)</span>
                                                    need to add review. <a href="#myApproval" class="customTabToggler" >Click here</a>
                                                </div>
                                            @endif

                                            @if ($helper->inStaff())
                                                <a href="{{url(route('trainee.index'))}}">
                                                    <button type="button" class="btn btn-default">
                                                        <i class="icon-plus"></i> Create Request
                                                    </button>
                                                </a>
                                            @endif

                                            @if ($helper->inAdmin())
                                                <button type="button" class="btn btn-default" onclick="location.href='{{url(route('trainee.index'))}}'">
                                                    <i class="icon-plus"></i> Create Trainee
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="panel-table">
                                        <div class="export-table export-mytrainee">
                                            <!--
                                            <select id="filterMonthRejected" class="form-control select-filter">
                                                <option value="">Filter Month</option>
                                                @for($i=1;$i<=12;$i++)
                                                    <option value="month-{{ date('m', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}">{{ date('F', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}</option>
                                                @endfor
                                            </select>

                                            <select id="filterYearRejected" class="form-control select-filter">
                                                <option value="">Filter Year</option>
                                                @for($i=0;$i<=3;$i++)
                                                    <option value="year-{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                                @endfor
                                            </select>
                                            -->

                                            <select id="filterMonthRejected" class="filterMonth form-control select-filter">
                                                <option value="">Filter Month</option>
                                                @for($i=1;$i<=12;$i++)
                                                    <option value="month-{{ date('m', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}">{{ date('F', strtotime(date('F', mktime(0, 0, 0, $i, 1)))) }}</option>
                                                @endfor
                                            </select>

                                            <select id="filterYearRejected" class="filterYear form-control select-filter">
                                                <option value="">Filter Year</option>
                                                @for($i=0;$i<=3;$i++)
                                                    <option value="year-{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                                @endfor
                                            </select>

                                            <a href="{{route('trainee.exportTrainee')}}?type=rejected" id="exportRejected" class="btn btn-default btn-sm btn-lite">
                                                <i class="icon-xlsx-file-format"></i> Excel
                                            </a>
                                        </div>
                                        <div class="clearfix"></div>

                                        <table class="traineeRejectedTable table table-hover table-striped table-responsive table-lite text-center" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="all">No</th>
                                                <th class="all">Title/Topic</th>
                                                <th class="all">Type</th>
                                                <th class="desktop">Employee Name</th>
                                                <th class="desktop">Line Manager</th>
                                                <th class="desktop">Start</th>
                                                <th class="desktop">End</th>
                                                @if ($helper->inStaff())
                                                    <th class="desktop">HR.</th>
                                                    <th class="desktop">Manager</th>
                                                @endif
                                                <th class="desktop"> Type of Programs </th>
                                                <th class="desktop">Notes</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @if (!$helper->inAdmin())
                                {{--@dd($full_name)--}}
                                @include('admindashboard::history.index')
                            @endif
                        </div>

                    </div>

                </div>
                <button type="button" class="btn btn-float btn-fba">
                    <i class="mdi mdi-more"></i>
                </button>
            </div>

            
        </div>
    </main>

    @include('trainee-modal')
    <div role="alert" aria-live="assertive" aria-atomic="true" class="toast mentor_toast d-none" data-autohide="false">
        <div class="toast-header">
            {{--<img src="..." class="rounded mr-2" alt="...">--}}
            <strong class="mr-auto">Mydev</strong>
            {{--<small>11 mins ago</small>--}}
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body">
            Mentor need to complete report first
        </div>
    </div>
@endsection
@section('styles')
<style>
	/* Navigation tab layout fix */
	.main-dashboard.secondary-nav {
		padding-top: 20px
	}
	@media (max-width: 767px){
		#tab-nav .nav-item:nth-child(1),
		#tab-nav .nav-item:nth-child(2),
		#tab-nav .nav-item:nth-child(3) {
			flex: 0 0 33%;
		}
		.main-dashboard.secondary-nav {
			padding-top: 150px
		}
	}
	.dataTables_wrapper .container {
		margin-top: 15px;
		margin-right: 15px;
		margin-left: 15px;

		max-width: calc(100% - 16px);
	}

	@media (max-width: 1023px){
		table > tbody {
			text-align: left;
		}
	}
</style>
@endsection
@section('scripts')
    <script>
    
    $(document).on('click','.rating-modal', function(){
        var staffid = $(this).data('staffid');
        var mentorid = $(this).data('mentorid');
        var requestgroupid = $(this).data('requestgroup');
        
        $('.rating_staff_id').val(staffid);
        $('.rating_mentor_id').val(mentorid);
        $('.rating_request_group').val(requestgroupid);

    })

    // new -------------------------------
    // new -------------------------------
    // new -------------------------------

        $( document ).ready(function() {
            $(document).on("click",".page-link",function(){
                $(".render").val("no");
                activeTab = $("ul > .nav-item").find('.nav-link.active').attr("href").replace("#","");
                dataidx = Number($(this).text())-1;

                if (typeof dataidx=="NaN"){
                    dataidx = Number($(this).data("data-id"))-1;
                }
                if(dataidx>0){
                    if (activeTab=="myTrainee"){
                        dtTableWaiting.page(dataidx).draw(false);
                        dtTableWaiting.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                            pageCurrent = dataidx+"1";
                            pageCurrent = Number(pageCurrent);
                            cell.innerHTML = i+1 * pageCurrent;
                        });
                    }

                    if (activeTab=="myApproval"){
                        dtTableOngoing.page(dataidx).draw(false);
                        dtTableOngoing.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                            pageCurrent = dataidx+"1";
                            pageCurrent = Number(pageCurrent);
                            cell.innerHTML = i+1 * pageCurrent;
                        });
                    }

                    if (activeTab=="rejected"){
                        dtTableRejected.page(dataidx).draw(false);
                        dtTableRejected.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                            pageCurrent = dataidx+"1";
                            pageCurrent = Number(pageCurrent);
                            cell.innerHTML = i+1 * pageCurrent;
                        });
                    }

                    if (activeTab=="myHistory"){
                        dtTableCompleted.page(dataidx).draw(false);
                        dtTableCompleted.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                            pageCurrent = dataidx+"1";
                            pageCurrent = Number(pageCurrent);
                            cell.innerHTML = i+1 * pageCurrent;
                        });
                    }

                    if (activeTab=="related"){
                        dtTableMytrain.page(dataidx).draw(false);
                        dtTableMytrain.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                            pageCurrent = dataidx+"1";
                            pageCurrent = Number(pageCurrent);
                            cell.innerHTML = i+1 * pageCurrent;
                        });
                    }
                }
            });

            var urlParams = new URLSearchParams(location.search);
            var dtTableWaiting = $('.traineeWaitingApprovalTable').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                ajax: {
                    url: "{{ route('trainee.datatableTraineeRequest', ['type' => 'waiting']) }}",
                    data: function (d) {
                        if (urlParams.has('page') && urlParams.has('tab')){
                            currentTab = urlParams.get('tab');
                            if (currentTab=="myTrainee"){
                                if ($(".render").val()=="yes"){
                                    pageNext = Number(urlParams.get('page'));
                                    d.start = Number(pageNext-1 + "0");
                                }
                            }
                        }
                        d.month = $('.filterMonth').val();
                        d.year = $('.filterYear').val();                
                    }
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'no' ,"searchable": false,"orderable": false},
                    { data: 'title', name: 'title' },
                    { data: 'type', name: 'type' },
                    { data: 'employee_name', name: 'employee_name' },
                    { data: 'atasan', name: 'atasan'}, // ini tes data = nama column nama = return val
                    { data: 'start', name: 'start' },
                    { data: 'end', name: 'end' },
                    { data: 'manager', name: 'manager' },
                    { data: 'hr', name: 'hr' },
                    { data: 'type_prog', name: 'type_prog' },
                    { data: 'option', name: 'option' },
                ],
                initComplete : function(settings, json) {
                    if (urlParams.has('page') && urlParams.has('tab')){
                        currentTab = urlParams.get('tab');
                        if (currentTab=="myTrainee"){
                            pageNext = Number(urlParams.get('page'));
                            dtTableWaiting.page(pageNext-1).draw(false);
                            dtTableWaiting.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                pageCurrent = pageNext-1+"1";
                                pageCurrent = Number(pageCurrent);
                                cell.innerHTML = i+1 * pageCurrent;
                            });
                        }
                    }
                    
                }
            })

            var dtTableOngoing = $('.traineeOngoingTable').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                ajax: {
                    url: "{{ route('trainee.datatableTraineeRequest', ['type' => 'ongoing']) }}",
                    data: function (d) {
                        if (urlParams.has('page') && urlParams.has('tab')){
                            currentTab = urlParams.get('tab');
                                if (currentTab=="myApproval"){
                                    if ($(".render").val()=="yes"){
                                        pageNext = Number(urlParams.get('page'));
                                        d.start = Number(pageNext-1 + "0");
                                    }
                                }
                        }
                        d.month = $('#filterMonthOngoing').val();
                        d.year = $('#filterYearOngoing').val();                
                    }
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'no' ,"searchable": false,"orderable": false},
                    { data: 'title', name: 'title' },
                    { data: 'type', name: 'type' },
                    { data: 'employee_name', name: 'employee_name' },
                    { data: 'atasan', name: 'atasan'}, // ini tes data = nama column nama = return val
                    { data: 'start', name: 'start' },
                    { data: 'end', name: 'end' },
                    { data: 'manager', name: 'manager' },
                    { data: 'hr', name: 'hr' },
                    { data: 'type_prog', name: 'type_prog' },
                    { data: 'option', name: 'option' },
                ],
                initComplete : function(settings, json) {
                    if (urlParams.has('page') && urlParams.has('tab')){
                        currentTab = urlParams.get('tab');
                        if (currentTab=="myApproval"){
                            pageNext = Number(urlParams.get('page'));
                            dtTableOngoing.page(pageNext-1).draw(false);
                            dtTableOngoing.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                pageCurrent = pageNext-1+"1";
                                pageCurrent = Number(pageCurrent);
                                cell.innerHTML = i+1 * pageCurrent;
                            });
                        }
                    }
                    
                }

            });

            var dtTableCompleted = $('.traineeCompletedTable').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                ajax: {
                    url: "{{ route('trainee.datatableTraineeRequest2', ['type' => 'completed']) }}",
                    data: function (d) {
                        if (urlParams.has('page') && urlParams.has('tab')){
                            currentTab = urlParams.get('tab');
                            if (currentTab=="myHistory"){
                                if ($(".render").val()=="yes"){
                                    pageNext = Number(urlParams.get('page'));
                                    d.start = Number(pageNext-1 + "0");
                                }
                            }
                        }

                        d.month = $('#filterMonthCompleted').val();
                        d.year = $('#filterYearCompleted').val();                
                    }
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'no' ,"searchable": false,"orderable": false},
                    { data: 'title', name: 'title' },
                    { data: 'type', name: 'type' },
                    { data: 'employee_name', name: 'employee_name' },
                    { data: 'atasan', name: 'atasan'}, // ini tes data = nama column nama = return val
                    { data: 'start', name: 'start' },
                    { data: 'end', name: 'end' },
                    { data: 'manager', name: 'manager' },
                    { data: 'hr', name: 'hr' },
                    { data: 'rating', name: 'rating' },
                    { data: 'type_prog', name: 'type_prog' },
                    { data: 'option', name: 'option' },
                ],
                initComplete : function(settings, json) {
                    if (urlParams.has('page') && urlParams.has('tab')){
                        currentTab = urlParams.get('tab');
                        if (currentTab=="myHistory"){
                            pageNext = Number(urlParams.get('page'));
                            dtTableCompleted.page(pageNext-1).draw(false);
                            dtTableCompleted.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                pageCurrent = pageNext-1+"1";
                                pageCurrent = Number(pageCurrent);
                                cell.innerHTML = i+1 * pageCurrent;
                            });
                        }
                    }
                    
                }

            });

            var dtTableRejected = $('.traineeRejectedTable').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                ajax: {
                    url: "{{ route('trainee.datatableTraineeRequest', ['type' => 'rejected']) }}",
                    data: function (d) {
                        if (urlParams.has('page')){
                            if ($(".render").val()=="yes"){
                                pageNext = Number(urlParams.get('page'));
                                d.start = Number(pageNext-1 + "0");
                            }
                        }
                        d.month = $('#filterMonthRejected').val();
                        d.year = $('#filterYearRejected').val();                
                    }
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'no' ,"searchable": false,"orderable": false},
                    { data: 'title', name: 'title' },
                    { data: 'type', name: 'type' },
                    { data: 'employee_name', name: 'employee_name' },
                    { data: 'atasan', name: 'atasan'}, // ini tes data = nama column nama = return val
                    { data: 'start', name: 'start' },
                    { data: 'end', name: 'end' },
                    { data: 'manager', name: 'manager' },
                    { data: 'hr', name: 'hr' },
                    { data: 'type_prog', name: 'type_prog' },
                    { data: 'option', name: 'option' },
                ],
                initComplete : function(settings, json) {
                    if (urlParams.has('page')){
                        // pageNext = Number(urlParams.get('page'));
                        // dtTableRejected.page(pageNext-1).draw(false);
                        // dtTableRejected.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        //     pageCurrent = pageNext-1+"1";
                        //     pageCurrent = Number(pageCurrent);
                        //     cell.innerHTML = i+1 * pageCurrent;
                        // });
                    }
                    
                }

            });

            var dtTableMytrain = $('.traineeMytrainee').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                ajax: {
                    url: "{{ route('trainee.datatableTraineeRequest3', ['type' => 'completed']) }}",
                    data: function (d) {
                        if (urlParams.has('page') && urlParams.has('tab')){
                            currentTab = urlParams.get('tab');
                            if (currentTab=="related"){
                                if ($(".render").val()=="yes"){
                                    pageNext = Number(urlParams.get('page'));
                                    d.start = Number(pageNext-1 + "0");
                                }
                            }
                        }
                        d.month = $('#filterMonthCompleted').val();
                        d.year = $('#filterYearCompleted').val();                         
                    }
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'no' ,"searchable": false,"orderable": false},
                    { data: 'title', name: 'title' },
                    { data: 'type', name: 'type' },
                    { data: 'employee_name', name: 'employee_name' },
                    { data: 'atasan', name: 'atasan'}, // ini tes data = nama column nama = return val
                    { data: 'start', name: 'start' },
                    { data: 'end', name: 'end' },
                    { data: 'manager', name: 'manager' },
                    { data: 'hr', name: 'hr' },
                    { data: 'type_prog', name: 'type_prog' },
                    { data: 'option', name: 'option' },
                ],
                initComplete : function(settings, json) {
                    if (urlParams.has('page') && urlParams.has('tab')){
                        currentTab = urlParams.get('tab');
                        if (currentTab=="related"){
                            pageNext = Number(urlParams.get('page'));
                            dtTableMytrain.page(pageNext-1).draw(false);
                            dtTableMytrain.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                pageCurrent = pageNext-1+"1";
                                pageCurrent = Number(pageCurrent);
                                cell.innerHTML = i+1 * pageCurrent;
                            });
                        }
                    }
                    
                }

            });

            if (urlParams.has('tab')){
                currentTab = urlParams.get('tab');
                setTimeout(function(){
                    $("."+currentTab).trigger("click");
                }, 3000);
            }

            $('.traineeWaitingApprovalTable tbody').on('click', 'tr', function () {
                activeTab = $("ul > .nav-item").find('.nav-link.active').attr("href").replace("#","");
                currentPage = dtTableWaiting.page.info().page;
                currentPage = currentPage+1;
                var data = dtTableWaiting.row( this ).data();
                $("#page" + data.id).val(currentPage);
                var previewUrl = $("#preview" + data.id).attr("href").split("?");
                previewUrl = previewUrl[0];
                $("#preview" + data.id).attr("href", previewUrl+"?page="+currentPage+"&tab="+activeTab);
            });

            $('.traineeOngoingTable tbody').on('click', 'tr', function () {
                activeTab = $("ul > .nav-item").find('.nav-link.active').attr("href").replace("#","");
                currentPage = dtTableOngoing.page.info().page;
                currentPage = currentPage+1;
                var data = dtTableOngoing.row( this ).data();
                $("#page" + data.id).val(currentPage);
                var previewUrl = $("#preview" + data.id).attr("href").split("?");
                previewUrl = previewUrl[0];
                $("#preview" + data.id).attr("href", previewUrl+"?page="+currentPage+"&tab="+activeTab);
            });

            $('.traineeCompletedTable tbody').on('click', 'tr', function () {
                activeTab = $("ul > .nav-item").find('.nav-link.active').attr("href").replace("#","");
                currentPage = dtTableCompleted.page.info().page;
                currentPage = currentPage+1;
                var data = dtTableCompleted.row( this ).data();
                $("#page" + data.id).val(currentPage);
                var previewUrl = $("#preview" + data.id).attr("href").split("?");
                previewUrl = previewUrl[0];
                $("#preview" + data.id).attr("href", previewUrl+"?page="+currentPage+"&tab="+activeTab);
            });

            $('.traineeRejectedTable tbody').on('click', 'tr', function () {
                activeTab = $("ul > .nav-item").find('.nav-link.active').attr("href").replace("#","");
                currentPage = dtTableRejected.page.info().page;
                currentPage = currentPage+1;
                var data = dtTableRejected.row( this ).data();
                $("#page" + data.id).val(currentPage);
                var previewUrl = $("#preview" + data.id).attr("href").split("?");
                previewUrl = previewUrl[0];
                $("#preview" + data.id).attr("href", previewUrl+"?page="+currentPage+"&tab="+activeTab);
            });

            $('.traineeMytrainee tbody').on('click', 'tr', function () {
                activeTab = $("ul > .nav-item").find('.nav-link.active').attr("href").replace("#","");
                currentPage = dtTableMytrain.page.info().page;
                currentPage = currentPage+1;
                var data = dtTableMytrain.row( this ).data();
                $("#page" + data.id).val(currentPage);
                var previewUrl = $("#preview" + data.id).attr("href").split("?");
                previewUrl = previewUrl[0];
                $("#preview" + data.id).attr("href", previewUrl+"?page="+currentPage+"&tab="+activeTab);
            });

                $('.dataTables_filter input').keyup(function(e) {
                    console.log($(this).parent().parent().parent().parent().parent().find(".filterMonth").attr("id"));
                    e.preventDefault();
                    var value = $(this).val();
                    if (value.length==0 || value.length>5) {
                        dtTableWaiting.ajax.reload();
                        dtTableOngoing.ajax.reload();
                        dtTableCompleted.ajax.reload();
                        dtTableRejected.ajax.reload();
                    }
                });

                
                $('#filterMonthWaitingApproval, #filterYearWaitingApproval').on( 'change', function () {
                    var year = $(document).find('#filterYearWaitingApproval').val();
                    var month = $(document).find('#filterMonthWaitingApproval').val();
                    //$(document).find('#exportWaitingApproval').attr('href', "{{route('dashboard.traineeRequestExportTrainee')}}?type=new&year="+year+"&month="+month);
                    dtTableWaiting.ajax.reload();
                });

                $('#filterMonthOngoing, #filterYearOngoing').on( 'change', function () {
                    var year = $(document).find('#filterYearOngoing').val();
                    var month = $(document).find('#filterMonthOngoing').val();
                    //$(document).find('#exportOngoing').attr('href', "{{route('dashboard.traineeRequestExportTrainee')}}?type=approved&year="+year+"&month="+month);
                    dtTableOngoing.ajax.reload();
                });

                $('#filterMonthCompleted, #filterYearCompleted').on( 'change', function () {
                    var year = $(document).find('#filterYearCompleted').val();
                    var month = $(document).find('#filterMonthCompleted').val();
                    //$(document).find('#exportCompleted').attr('href', "{{route('dashboard.traineeRequestExportTrainee')}}?type=completed&year="+year+"&month="+month);
                    dtTableCompleted.ajax.reload();
                });

                $('#filterMonthRejected, #filterYearRejected').on( 'change', function () {
                    var year = $(document).find('#filterYearRejected').val();
                    var month = $(document).find('#filterMonthRejected').val();
                    //$(document).find('#exportRejected').attr('href', "{{route('dashboard.traineeRequestExportTrainee')}}?type=rejected&year="+year+"&month="+month);
                    dtTableRejected.ajax.reload();
                });
                

                $(document).on("click", ".reject_button_trainee", function () {
                    currentPage = dtTableWaiting.page.info().page;
                    currentPage = currentPage+1;
                    
                    if (currentPage){
                        $("#rejected_request_next_page").val(currentPage);
                    } else {
                        $("#rejected_request_next_page").val(1);
                    }

                     var requestgroup = $(this).data('requestgroup');
                     var userid = $(this).data('userid');
                     $(".modal-body #rejected_request_id").val( requestgroup );
                     $(".modal-body #rejected_user_id").val( userid );
                });

                pp = $('.page-item').hasClass('active');
                console.log(pp);

            });

    // end new ---------------------------
    // end new ---------------------------
    // end new ---------------------------

        $('.toast_mentor_trigger').on('click',function(){
            $('.mentor_toast').toast('show');
        });
        $('.customTabToggler').click(function () {

            $('.tab-pane')
                .removeClass('active')
                .removeClass('show');
            $($(this).attr('href')).addClass('active').addClass('show');
            $('.nav-link')
                .removeClass('active')
                .removeClass('show');
            $('[href="'+$(this).attr('href')+'"]').addClass('active').addClass('show');
        });

        $(window).resize(function(){
        	$($.fn.dataTable.tables( true ) ).css('width', '100%');
		      $($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();
        });

        $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
		        $($.fn.dataTable.tables( true ) ).css('width', '100%');
		        $($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();
		    }); 


        // /** TABLE FILTER**/
    /*
        var waitingApprovalTable = $('.traineeWaitingApprovalTable').DataTable({
            "pageLength": 10,
            "responnsive": true,
            "searching": true
            // "order": [
            //     [ 9, "desc" ]
            // ]
        });

        // waitingApprovalTable.on( 'order.dt search.dt', function () {
        //     waitingApprovalTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        //     cell.innerHTML = i+1;
        //     } );
        // }).draw();

        $('#filterMonthWaitingApproval, #filterYearWaitingApproval').on( 'change', function () {
            var year = $(document).find('#filterYearWaitingApproval').val();
            year = year.replace("year-", "");

            var month = $(document).find('#filterMonthWaitingApproval').val();
            month = month.replace("month-", "");

            $(document).find('#exportWaitingApproval').attr('href', "{{route('trainee.exportTrainee')}}?type=new&year="+year+"&month="+month);

            waitingApprovalTable
            .columns( 5 )
            .search( this.value )
            .draw();
        });

        var ongoingTable = $('.traineeOngoingTable').DataTable({
            "pageLength": 5,
            "order": [
                [ 9, "desc" ]
            ]
        });

        ongoingTable.on( 'order.dt search.dt', function () {
            ongoingTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
            } );
        }).draw();

        $('#filterMonthOngoing, #filterYearOngoing').on( 'change', function () {
            var year = $(document).find('#filterYearOngoing').val();
            year = year.replace("year-", "");

            var month = $(document).find('#filterMonthOngoing').val();
            month = month.replace("month-", "");

            $(document).find('#exportOngoing').attr('href', "{{route('trainee.exportTrainee')}}?type=approved&year="+year+"&month="+month);

            ongoingTable
            .columns( 5 )
            .search( this.value )
            .draw();
        });

        var completedTable = $('.traineeCompletedTable').DataTable({
            "pageLength": 5,
            "order": [
                [ 9, "desc" ]
            ]
        });

        completedTable.on( 'order.dt search.dt', function () {
            completedTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
            } );
        }).draw();

        $('#filterMonthCompleted, #filterYearCompleted').on( 'change', function () {
            var year = $(document).find('#filterYearCompleted').val();
            year = year.replace("year-", "");

            var month = $(document).find('#filterMonthCompleted').val();
            month = month.replace("month-", "");

            $(document).find('#exportCompleted').attr('href', "{{route('trainee.exportTrainee')}}?type=completed&year="+year+"&month="+month);

            completedTable
            .columns( 5 )
            .search( this.value )
            .draw();
        });

        var rejectedTable = $('.traineeRejectedTable').DataTable({
            "pageLength": 5,
            "order": [
                [ 9, "desc" ]
            ]
        });

        rejectedTable.on( 'order.dt search.dt', function () {
            rejectedTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
            } );
        }).draw();

        $('#filterMonthRejected, #filterYearRejected').on( 'change', function () {
            var year = $(document).find('#filterYearRejected').val();
            year = year.replace("year-", "");

            var month = $(document).find('#filterMonthRejected').val();
            month = month.replace("month-", "");

            $(document).find('#exportRejected').attr('href', "{{route('trainee.exportTrainee')}}?type=rejected&year="+year+"&month="+month);

            rejectedTable
            .columns( 5 )
            .search( this.value )
            .draw();
        });

        */
    </script>
@endsection
