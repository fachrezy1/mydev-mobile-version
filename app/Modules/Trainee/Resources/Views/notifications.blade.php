@extends('core.trainee.main')
@php($helper = new \App\Helpers\UserRolesHelper )
@section('head')
    <link rel="stylesheet" href="{{url('css/admin/dashboard/app.css')}}">
@endsection

@section('content')
    <main class="main-content">
        <div class="main-dashboard">
            <div class="dashboard-content w-100">
                <div class="dashboard-breadcrumb clean">
                    <div class="container-fluid">
                        <ul class="list-unstyled top-breadcrumb">
                            <li>
                                <a href="{{url('/')}}">
                                    {{-- <i class="icon-home"></i> --}}
                                    <i class="icon-back"></i>
                                    Dashboard
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="container-fluid px-2 px-md-0 mt-4">
                	<div class="container">
	                	<div class="row">
	                    <div class="col-12">
	                        <div class="dashboard-title custom">
	                            <h1>Notifications</h1>
	                            <form action="" method="get">
	                                <input type="text" class="form-control" name="search" placeholder="search notifications">
	                            </form>
	                        </div>
	                        <smal> {{($notifications['search'] ? 'Result For '.$notifications['search']: '')}} </smal>

	                        @if (isset($notifications['data']))
	                            @foreach($notifications['data'] as $notification)
	                                @if(!($role->inManager() && in_array($notification['data']['type'], ['form manage request', 'form request'])))
	                                    <div class="notification-list">
	                                        <div class="notification-entry">
	                                            <span class="small-text">
	                                                <i class="icon-calendar"></i> {{\Carbon\Carbon::parse($notification['created_at'])->format('M D Y')}} | <i class="icon-clock"></i> {{\Carbon\Carbon::parse($notification['created_at'])->format('h:m a')}}
	                                            </span>
	                                            <h4>{!! strip_tags($notification['data']['message_body']) !!}</h4>
	                                        </div>
	                                        <a href="{{url($notification['data']['url'])}}">
	                                            <button type="button" class="btn btn-default">
	                                                View
	                                            </button>
	                                        </a>
	                                    </div>
	                                @endif
	                            @endforeach
	                            <ul class="custom-pagination list-unstyled">
	                                @for ($i=1;$i<$notifications['last_page'];$i++)
	                                    @if ($notifications['current_page'] != 1 && $i == 1)
	                                        <li>
	                                            <a href="{{$notifications['prev_page_url'].($notifications['search'] ? '&search='.$notifications['search'] : '')}}">
	                                                <i class="icon-back"></i>
	                                            </a>
	                                        </li>
	                                    @endif
	                                    <li> 
	                                        <a href="{{url('/trainee/notification?page='.$i.($notifications['search'] ? '&search='.$notifications['search'] : ''))}}" class="{{$notifications['current_page'] == $i ? 'active' : ''}}">{{$i}}</a>
	                                    </li>
	                                @endfor
	                                @if ($notifications['current_page'] != $notifications['last_page'])
	                                    <li>
	                                        <a href="{{$notifications['next_page_url'].($notifications['search'] ? '&search='.$notifications['search'] : '') }}">
	                                            <i class="icon-next"></i>
	                                        </a>
	                                    </li>
	                                @endif
	                            </ul>
	                        @else
	                        @endif
	                    </div>
	                  </div>
                  </div>
                </div>
                {{-- <button type="button" class="btn btn-float btn-fba">
                    <i class="icon-back"></i>
                </button> --}}
            </div>

            @if (!$helper->inAdmin())
                @include('sidebar')
            @endif
        </div>

    </main>
@endsection

@section('scripts')
@endsection
