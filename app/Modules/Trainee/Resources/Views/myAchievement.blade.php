@php($user = \Sentinel::check())
@php($helper = new \App\Helpers\UserRolesHelper )

@extends($helper->inAdmin() ? 'core.admin.main' :'core.trainee.main')
@section('head')
    <link rel="stylesheet" href="{{url('css/admin/dashboard/app.css')}}">
@endsection
@section('content')

    <main class="main-content">
        <div class="main-dashboard">
            <div class="dashboard-content">
                <div class="container">
                    <div class="dashboard-entry">
                        <div class="achievement mt-24">
                            <ul class="nav nav-pills nav-pills-icons" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active show" href="#achievement" role="tab" data-toggle="tab" aria-selected="true">
                                        <i class="icon-podium"></i> My Achievement
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content tab-space">
                                <div class="tab-pane active show" id="achievement">
                                    @if($requests['completed'])
                                        @foreach($requests['completed'] as $key=>$requests)
                                        <div class="trainee-achieve panel-box">
                                                <div class="top-achieve">
                                                    <div class="panel-title">
                                                        <h3>{{isset($requests['training_obj_employee_name']) ? $requests['training_obj_employee_name'] :
                                                            (isset($requests['coaching_ci_employee_name']) ? $requests['coaching_ci_employee_name']
                                                             : (isset($requests['mentoring_ci_employee_name']) ? $requests['mentoring_ci_employee_name'] : ''))}}</h3>
                                                        <ul class="list-unstyled right-panel">
                                                            <li>
                                                                <i class="icon-presentation-1"></i>
                                                                <p>
                                                                    <strong>Category</strong>
                                                                    {{str_replace('_',' ',ucwords($key))}}
                                                                </p>
                                                            </li>
                                                            <li>
                                                                <i class="icon-calendar"></i>
                                                                <p>
                                                                    <strong>Date</strong>
                                                                    {{isset($requests['start_date']) ? \Carbon\Carbon::parse($requests['start_date'])->format('d M, Y') : ''}}
                                                                    -
                                                                    {{isset($requests['end_date']) ? \Carbon\Carbon::parse($requests['end_date'])->format('d M, Y') : ''}}
                                                                </p>
                                                            </li>
                                                        </ul>
                                                        <button class="btn btn-default" type="button" data-toggle="collapse" onclick="location.href='{{route('trainee.preview',$requests['request_group_id'])}}'" data-target="#detail1">
                                                            Detail
                                                        </button>
                                                    </div>
                                                </div>
                                                <div id="detail1" class="collapse mid-achieve d-none">
                                                    <div class="flex-list d-none">
                                                        <div class="flex-item">
                                                            <div class="side-achive">
                                                                <div class="circle-point big d-none">
                                                                    <img src="images/sample-mgr.jpg" alt="" class="img-fluid">
                                                                </div>
                                                                <div class="custom-list">
                                                                    <span>Coache</span>
                                                                    Jon Stepen
                                                                </div>
                                                                <div class="custom-list">
                                                                    <span>Certificated</span>
                                                                    Yes
                                                                </div>
                                                                <div class="custom-list">
                                                                    <span>Coache NIK</span>
                                                                    IND-1234
                                                                </div>
                                                            </div>
                                                            <div class="side-achive">
                                                                <div class="circle-point">
                                                                    <i class="icon-conference"></i>
                                                                </div>
                                                                You've got <strong>100 points</strong>
                                                            </div>
                                                        </div>
                                                        <div class="flex-item">
                                                            {{--@foreach($requests['reviews'] as $review)--}}
                                                                <div class="achievement-list">
                                                                    <div class="list-text">
                                                                        <h3>Introducing course</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras malesuada nisi sodales mauris condimentum interdum. Fusce volutpat ornare purus, in tempus diam suscipit ac.</p>
                                                                    </div>
                                                                    <div class="list-status completed">
                                                                        <i class="icon-checklist"></i>
                                                                        Completed
                                                                    </div>
                                                                </div>
                                                            {{--@endforeach--}}
                                                            <div class="achievement-list">
                                                                <div class="list-text">
                                                                    <h3>Introducing course 3</h3>
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras malesuada nisi sodales mauris condimentum interdum. Fusce volutpat ornare purus, in tempus diam suscipit ac.</p>
                                                                </div>
                                                                <div class="list-status completed">
                                                                    <i class="icon-checklist"></i>
                                                                    Completed
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    @endforeach
                                    @else
                                        Empty
                                    @endif


                                    <ul class="pagination pagination-info flex-end d-none">
                                        <li class="page-item">
                                            <a href="javascript:void(0);" class="page-link"> prev<div class="ripple-container"></div></a>
                                        </li>
                                        <li class="active  page-item">
                                            <a href="javascript:void(0);" class="page-link">1</a>
                                        </li>
                                        <li class="page-item">
                                            <a href="javascript:void(0);" class="page-link">2</a>
                                        </li>
                                        <li class="page-item">
                                            <a href="javascript:void(0);" class="page-link">3</a>
                                        </li>
                                        <li class="page-item">
                                            <a href="javascript:void(0);" class="page-link">4</a>
                                        </li>
                                        <li class="page-item">
                                            <a href="javascript:void(0);" class="page-link">5</a>
                                        </li>
                                        <li class="page-item">
                                            <a href="javascript:void(0);" class="page-link">next <div class="ripple-container"></div></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>




                    </div>

                </div>

                <button type="button" class="btn btn-float btn-fba">
                    <i class="mdi mdi-more"></i>
                </button>
            </div>
            @if (!$helper->inAdmin())
                @include('sidebar')
            @endif
        </div>

    </main>
@endsection
@section('scripts')
@endsection
