@php($user = \Sentinel::check())
@php($helper = new \App\Helpers\UserRolesHelper )

@extends($helper->inAdmin() ? 'core.admin.main' :'core.trainee.main')
@section('head')
    {{--<link rel="stylesheet" href="{{url('css/admin/dashboard/app.css')}}">--}}
@endsection
@section('content')

    <main class="main-content">
        <div class="main-dashboard">
            <div class="dashboard-content">
                <div class="container-fluid">

                    <div class="dashboard-entry">
                        <div class="dashboard-title">
                          <h1>Report</h1>
                        </div>
                        <div class="row">

                            <div class="col-12 col-md-6">
                                <div class="page-title">
                                    <h3>Trainee Form</h3>
                                </div>

                                <div class="trainee-tasks">
                                    @if($trainee['type']['Training'] || $trainee['type']['Assignment'] || $trainee['type']['External Speaker'] || $trainee['type']['Mentoring'] || $trainee['type']['Inhouse'])
                                        <canvas id="traineeFormPie"></canvas>
                                    @else
                                        <div class="col-12 py-5">
                                            <span> You have no Training form, <a href="{{ route('trainee.index') }}" target="_blank">create requests</a> now. </span>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="page-title">
                                    <h3>As Tranee/Mentor</h3>
                                </div>
                                <div class="trainee-tasks">
                                    @if( $report['user_position']['as_trainee'] || $report['user_position']['as_trainer'])
                                        <canvas id="traineePositionPie"></canvas>
                                    @else
                                        <div class="col-12 py-5">
                                            <span> You have no Training form, <a href="{{ route('trainee.index') }}" target="_blank">create requests</a> now. </span>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="page-title">
                            <h3>Your Trainee</h3>
                        </div>
                        <table id="employeeList" class="table table-hover table-striped" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Trainee Type</th>
                                <th>Position<th>
                            </tr>
                            </thead>
                        </table>

                        <div class="page-title">
                            <h3>Point History</h3>
                        </div>
                        <table id="pointHistory" class="table table-hover table-striped" cellspacing="0" width="100%">
                            <thead>
                            </thead>
                        </table>

                    </div>

                </div>

                <button type="button" class="btn btn-float btn-fba">
                    <i class="icon-back"></i>
                </button>
            </div>
            @if (!$helper->inAdmin())
                @include('sidebar')
            @endif
        </div>

    </main>
@endsection
@section('scripts')
    <script>

        var traineePosition = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: [
                            {{$report['user_position']['as_trainee']}},
                            {{$report['user_position']['as_trainer']}},
                        ],
                        backgroundColor: [
                            '#7fffe2',
                            '#6857ff',
                        ],
                        label: 'Dataset 1'
                    }],
                    labels: [
                        'As Trainee',
                        'As Mentor',
                    ]
                },
                options: {
                    responsive:true,
                    legend: {
                        position:'right',
                    }
                }
            };

        var traineeForm = {
            type: 'pie',
            data: {
                datasets: [{
                    data: [
                        {{$trainee['type']['Training']}},{{$trainee['type']['Assignment']}},
                        {{$trainee['type']['External Speaker']}},{{$trainee['type']['Mentoring']}},
                        {{$trainee['type']['Inhouse']}}
                    ],
                    backgroundColor: [
                        '#ffaaa3',
                        '#ffd794',
                        '#c5ff9c',
                        '#7fffe2',
                        '#6857ff',
                    ],
                    label: 'Dataset 1'
                }],
                labels: [
                    'Public Training',
                    'Assignment',
                    'External Speaker',
                    'Mentoring',
                    'Inhouse'
                ]
            },
            options: {
                responsive:true,
                legend: {
                    position:'right',
                }
            }
        };

        @if($trainee['type']['Training'] || $trainee['type']['Assignment'] || $trainee['type']['External Speaker'] || $trainee['type']['Mentoring'] || $trainee['type']['Inhouse'])
            var ctxTraineeForm = document.getElementById('traineeFormPie').getContext('2d');
        @endif
        @if( $report['user_position']['as_trainee'] || $report['user_position']['as_trainer'])
            var ctxTraineePosition = document.getElementById('traineePositionPie').getContext('2d');
        @endif
        new Chart(ctxTraineePosition, traineePosition);
        new Chart(ctxTraineeForm, traineeForm);
        $(document).ready(function () {
            $('#employeeList').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                pageLength:5,
                ajax: '{{route('trainee.report.getTraineeUser')}}',
                columns: [
                    {data: 'full_name', name: 'full_name'},
                    {data: 'form_group_id', name: 'form_group_id'},
                    {data: 'user_position', name: 'user_position'},
                    {data: 'options', name: 'options',title: 'Options'}
                ]
            });

            $('#pointHistory').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                pageLength:5,
                ajax: '{{route('trainee.report.pointHistory')}}',
                columns: [
                    {data: 'point', name: 'point',title:'Point'},
                    {data: 'created', name: 'created',title:'At'},
                    {data: 'form_group', name: 'form_group',title:'Form Type'},
                ]
            });
        })
    </script>
@endsection
