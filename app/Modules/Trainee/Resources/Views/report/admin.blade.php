@php($user = \Sentinel::check())
@php($helper = new \App\Helpers\UserRolesHelper )

@extends($helper->inAdmin() ? 'core.admin.main' :'core.trainee.main')
@section('head')
    <link rel="stylesheet" href="{{url('css/admin/dashboard/app.css')}}">
@endsection
@section('content')

<main class="main-content">
	<div class="main-dashboard clean">
		<div class="dashboard-breadcrumb review">
			<div class="container-fluid">
				<h1 class="text-center">User Report</h1>
			</div>
		</div>

		<div class="review-top">
			<div class="container">
				<div class="box-inline">
					<div class="box-items">
						<div class="profile-circle">
							<div class="circle-thumbnail">
								<img src="{{ url('images/profile.jpeg') }}" alt="" class="img-fluid">
							</div>
							<div class="vertical-content text-center">
								<h5>Name</h5>
								<h3>{{$employee['full_name']}}</h3>
							</div>
						</div>
					</div>
					<div class="box-items">
						<div class="vertical-content">
							<h5>User info</h5>
						</div>
						<div class="trainer-data">
							<ul class="list-unstyled">
								<li>
									<i class="icon-user-4"></i> <p><strong>NIK</strong> {{$nik}}</p>
								</li>
								<li>
									<i class="icon-user-4"></i> <p><strong>Position</strong> {{$employee['position']}}</p>
								</li>
								<li>
									<i class="icon-user-4"></i> <p><strong>Division</strong> {{$employee['division']}}</p>
								</li>
							</ul>
						</div>
					</div>
					<div class="box-items">
						<div class="vertical-content">
							<h5>Date</h5>
						</div>
						<div class="trainer-data">
							<ul class="list-unstyled">
								<li>
									<i class="icon-deal"></i> <p><strong>Status</strong> {{$employee['employee_status']}}</p>
								</li>
							</ul>
							<button type="button" class="btn btn-default d-none">
								<i class="icon-xlsx-file-format"></i> Export to excel
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container mt-5">
			<div class="profile-cost-points custom">
				<div class="row">
					<div class="col-12">
						<h2>Training Request list</h2>

					</div>
				</div>
				@include('trainee::report.admin.course_widget')
				@include('trainee::report.admin.course_list')
			</div>

			<div class="profile-cost-points custom mt-0">
			  <div class="row">
			  	<div class="col-12">
			  		<h2> Use Assigned as </h2>
			  	</div>
			  </div>

				<div class="row widgets"> 
					<div class="profile-card">
						<div class="info-course">
							<i class="icon-training-1"></i>
							<div class="profile-cost">
								<span>As Mentor</span>
								<span class="numeric">{{$report['user_position']['as_trainer']}}</span>
							</div>
						</div>
					</div>

					<div class="profile-card">
						<div class="info-course">
							<i class="icon-training-1"></i>
							<div class="profile-cost">
								<span>As Trainee</span>
								<span class="numeric">{{$report['user_position']['as_trainee']}}</span>
							</div>
						</div>
					</div>
					<div class="profile-card">
						<div class="info-course">
							<i class="icon-training-1"></i>
							<div class="profile-cost">
								<span>As Internal Mentor</span>
								<span class="numeric">{{$report['user_position']['as_internal_mentor']}}</span>
							</div>
						</div>
					</div>
					<div class="profile-card">
						<div class="info-course">
							<i class="icon-training-1"></i>
							<div class="profile-cost">
								<span>As External Speaker</span>
								<span class="numeric">{{$report['user_position']['as_external_speaker']}}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="profile-cost-points custom mt-0">
				<div class="row">
					<div class="col-12">
						<h2> User Trainee </h2>
					</div>

					<div class="col-12">
						<div class="panel-table report pt-0">
							<table id="employeeList" class="table table-hover table-striped" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Name</th>
										<th>Trainee Type</th>
										<th>Position</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div class="profile-cost-points custom mt-0">
				<div class="row">
					<div class="col-12">
						<h2> Point History </h2>
					</div>

					<div class="col-12">
						<div class="panel-table report pt-0">
							<table id="pointHistory" class="table table-hover table-striped" cellspacing="0" width="100%">
								<thead>
									<th>Point</th>
									<th>At</th>
									<th>type</th>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

	</main>

@endsection
@section('styles')
<style>
	.dt-buttons {
		float: left;
		margin-top: -2px;

		display: flex;
		flex-wrap: wrap;
		justify-content: center;
	}
	.dt-buttons .btn {
		margin-bottom: 10px;
	}
	.widgets {
		margin-left: -8px;
		margin-right: -8px;
	}

	.for-hr .dashboard-breadcrumb.review h1 {
		margin-top: 40px
	}
	@media(max-width: 767px){
		.profile-cost-points.custom {
			margin-top: 0;
		}
	}
	@media(min-width: 769px){
		.for-hr .dashboard-breadcrumb.review h1 {
			margin-top: 100px
		}
	}
	@media(min-width: 1024px){
		.for-hr .dashboard-breadcrumb.review h1 {
			margin-top: 50px
		}
	}
</style>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $('#employeeList').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                pageLength:5,
                ajax: '{{route('trainee.report.getTraineeUser')}}',
                columns: [
                    {data: 'full_name', name: 'full_name'},
                    {data: 'form_group_id', name: 'form_group_id'},
                    {data: 'user_position', name: 'user_position'},
                    {data: 'options', name: 'options',title: 'Options'}
                ]
            });
            $('#pointHistory').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                pageLength:5,
                ajax: '{{route('trainee.report.pointHistory')}}',
                columns: [
                    {data: 'point', name: 'point',title:'Point'},
                    {data: 'created', name: 'created',title:'At'},
                    {data: 'form_group', name: 'form_group',title:'Form Type'},
                ]
            });
        })
    </script>
@endsection
