<div class="row mx-0 mt-4 w-100">
    <div class="panel-table report">
        <table class="exportTable table table-hover table-striped table-lite" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="all">No</th>
                    <th class="all">Title/Topic</th>
                    <th class="all">Type</th>
                    <th class="desktop">Employee Name</th>
                    <th class="desktop">Start</th>
                    <th class="desktop">End</th>
                    <th class="desktop">Manager</th>
                    <th class="desktop">HR.</th>
                </tr>
            </thead>

            <tbody>
                @php ($n=1)
                @if(!empty($requests))
                    @foreach($requests as $req_type => $req)
                        @foreach($req as $form_group_id => $form_group)
                            @foreach($form_group as $req_group_id => $form_data)
                                <tr>
                                    <td>{{ $n++ }}</td>
                                    <td>{{ show_course_title($form_group_id, $form_data) }}</td>
                                    <td>{{ ucfirst(str_replace('_', ' ', $req_type)) }}</td>
                                    <td>{{ format_sentence(@unserialize($form_data['employee_name']) ? @unserialize($form_data['employee_name'])[0] : $form_data['employee_name']) }}</td>
                                    <td><i class="mdi mdi-calendar"></i>{{ isset($form_data['start_date']) ? $form_data['start_date'] : '' }}</td>
                                    <td><i class="mdi mdi-calendar"></i>{{ isset($form_data['end_date']) ? $form_data['end_date'] : '' }}</td>
                                    <td>
                                        <span class="badge badge-pill {{ isset($form_data['first_accepter_class']) ? $form_data['first_accepter_class'] : '' }}">
                                        {{ isset($form_data['first_accepter'])?$form_data['first_accepter']:'' }}</span>
                                    </td>
                                    <td>
                                        <span class="badge badge-pill {{isset($form_data['second_accepter_class']) ? $form_data['second_accepter_class'] : ''}}">
                                        {{isset($form_data['second_accepter'])?$form_data['second_accepter']:''}}</span>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>    
</div>