<div class="row widgets"> 
    <div class="profile-card">
        <div class="info-course">
            <i class="icon-analysis-1"></i>
            <div class="profile-cost">
                <span>Public Training</span>
                <span class="numeric">{{ isset($trainee['type']['Training']) ? $trainee['type']['Training'] : 0}}</span>
            </div>
        </div>
    </div>

    <div class="profile-card">
        <div class="info-course">
            <i class="icon-analysis-1"></i>
            <div class="profile-cost">
                <span>Assignment</span>
                <span class="numeric">{{isset($trainee['type']['Assignment']) ? $trainee['type']['Assignment'] : 0}}</span>
            </div>
        </div>
    </div>

    <div class="profile-card">
        <div class="info-course">
            <i class="icon-analysis-1"></i>
            <div class="profile-cost">
                <span>Mentoring & Coaching</span>
                <span class="numeric">{{isset($trainee['type']['Mentoring']) ? $trainee['type']['Mentoring'] : 0}}</span>
            </div>
        </div>
    </div>

    <div class="profile-card">
        <div class="info-course">
            <i class="icon-analysis-1"></i>
            <div class="profile-cost">
                <span>Inhouse</span>
                <span class="numeric">{{isset($trainee['type']['Inhouse']) ? $trainee['type']['Inhouse'] : 0}}</span>
            </div>
        </div>
    </div>
    
    <div class="profile-card">
        <div class="info-course">
            <i class="icon-analysis-1"></i>
            <div class="profile-cost">
                <span>External Speaker</span>
                <span class="numeric">{{isset($trainee['type']['External Speaker']) ? $trainee['type']['External Speaker'] : 0}}</span>
            </div>
        </div>
    </div>
</div>
