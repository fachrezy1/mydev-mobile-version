@php($helper = new \App\Helpers\UserRolesHelper )
@php($user = \sentinel::check())
@php(
    $setting = \App\Modules\Admindashboard\Models\SettingModel::where('setting_group','banner')->get()
)
@php($banners = null)
@if ($setting->isNotEmpty())
    @php($banners = $setting->toArray())
@endif
@foreach($banners as $key=>$value)
    @php($banner[$value['setting_key']] = $value['setting_value'])
@endforeach
@extends('core.trainee.main')
@section('content')
    <main class="main-content">
        <div class="main-dashboard">
            <div class="dashboard-content">

                <div class="container-fluid">

                    <div class="dashboard-entry">
                        <div class="dashboard-title">
                            <h1>Welcome back, {{$user->first_name}}</h1>
                        </div>

                        <div class="trainee-pick owl-carousel owl-theme">
                            <div class="item">
                                <img src="{{asset('uploads/'.$banner['banner_image'])}}" alt="" class="img-fluid">
                                <div class="item-description">
                                </div>

                            </div><!-- /.item -->
                        </div>

                        <div class="page-title">
                          <h3>My Request Progress</h3>
                        </div>
                        <div class="trainee-tasks">
                          <div class="stats-item">
                            <span class="circle-badge">{{$requests['new']}}</span>
                            <div class="stats-description">
                              <h4>Waiting For Approval</h4>
                              <p>Employee Course Request</p>
                            </div>
                          </div>
                          <div class="stats-item">
                            <span class="circle-badge">{{$requests['onProgress']}}</span>
                            <div class="stats-description">
                              <h4>On Progress</h4>
                              <p>Employee Course Progress</p>
                            </div>
                          </div> 
                          <div class="stats-item">
                            <span class="circle-badge">{{$requests['completed']}}</span>
                            <div class="stats-description">
                              <h4>Completed Requests</h4>
                              <p>Employee Course Completed</p>
                            </div>
                          </div>
                          <div class="stats-item">
                            <span class="circle-badge">{{$requests['rejected']}}</span>
                            <div class="stats-description">
                              <h4>Rejected Requests</h4>
                              <p>Employee Course Rejected</p>
                            </div>
                          </div>

                        </div>

                        <div class="dash-panels">
                            <div class="panel">
                                <div class="panel-title">
                                    <h3>Competency to be developed</h3>
                                </div>

                                <div class="panel-body">
                                	<div class="dashboard-legends">
                                    	<h6 class="pt-3">Legend:</h6>
                                    	<dl class="mb-0">
                                    		<dt class="child1">AG</dt>
                                    		<dd class="">Assignment</dd><br>
                                    		<dt class="child2">MC</dt>
                                    		<dd class="">Mentoring/Coaching</dd><br>
                                    		<dt class="child3">PT</dt>
                                    		<dd class="">Public Training</dd><br>
                                    		<dt class="child4">IH</dt>
                                    		<dd class="">InHouse</dd>
                                    	</dl>
                                    </div>
                                </div>

                                <div id="requestTable_wrapper" class="table-competency table-responsive dataTables_wrapper dt-bootstrap4 no-footer">
                                    <table id="requestTable" class="table table-hover table-striped dataTable no-footer dtr-inline" cellspacing="0" width="100%" role="grid" aria-describedby="requestTable_info" style="width: 100%;">
                                        <thead>
                                          <tr role="row">
                                            <th>Competency</th>
                                                <th><span class="d-inline-block d-md-none">AG</span><span class="d-none d-md-inline-block">Assignment</span></th>
                                                <th><span class="d-inline-block d-md-none">MC</span><span class="d-none d-md-inline-block">Mentoring Coaching</span></th>
                                                <th><span class="d-inline-block d-md-none">PT</span><span class="d-none d-md-inline-block">Public Training</span></th>
                                                <th><span class="d-inline-block d-md-none">IH</span><span class="d-none d-md-inline-block">InHouse</span></th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($competencies as $competency_name => $competency)
                                                <tr role="row" class="odd">
                                                    <td tabindex="0" class="sorting_1">{{ ucwords(str_replace('_', ' ', $competency_name)) }}</td>
                                                    <td>{{ $competency[1] }}</td>
                                                    <td>{{ $competency[2] }}</td>
                                                    <td>{{ $competency[3] }}</td>
                                                    <td>{{ $competency[5] }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        </table>
                                </div>
                            </div>

                        </div>

                        <div class="panel-title">
                            <h3>My Development Status</h3>
                        </div>
                        
                        <div class="dash-panel">
                            <div class="panel col-12">
                                <div class="panel-title">
                                    <h4>Type of Development</h4>

                                    <div class="form-group reset chart-year-filter-container">
                                        <select id="chartTwoContainerMobileFilter" class="form-control select-year">
                                            @for($i=0;$i<=3;$i++)
                                                <option value="{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}">{{ date('Y', strtotime(date('Y').' -'.$i.' year')) }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    
                                    <div class="export-chart">
                                        <button type="button" class="btn btn-default btn-sm btn-lite" id="chartTwoContainerMobilePDF">
                                            <i class="icon-file"></i> PDF
                                        </button>

                                        <a href="#" class="btn btn-default btn-sm btn-lite" id="chartTwoContainerMobileImage" download="chart-training.png">
                                            <i class="icon-download"></i> Image
                                        </a>
                                        <a href="{{route('trainee.dashboard.exportChartTraining')}}" id="exportChartTraining" class="btn btn-default btn-sm btn-lite">
                                            <i class="icon-xlsx-file-format"></i> Excel
                                        </a>
                                    </div>
                                </div>
                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    @if(count($report['yearRange']) > 1)
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Annually</a>
                                        </li>
                                    @endif
                                </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                    <div class="chart-item monthly " >
                                        <!-- <canvas id="chartTwoContainer" class="d-none d-md-block" ></canvas> -->
                                        <div id="chartTwoContainerMobileContainer">
                                            <canvas id="chartTwoContainerMobile" class="d-block d-md-none" height="500"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                    <div class="chart-item monthly " >
                                        <!-- <canvas id="chartMonthContainer" class="d-none d-md-block" ></canvas> -->
                                        <canvas id="chartMonthContainerMobile" class="d-block d-md-none" height="500" ></canvas>
                                    </div>
                                </div>
                                @if(count($report['yearRange']) > 1)
                                    <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                                        <div class="chart-item monthly"  >
                                            <canvas id="chartAnualContainer" class="d-none d-md-block" ></canvas>
                                            <canvas id="chartAnualContainerMobile" class="d-block d-md-none" height="500" ></canvas>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            </div>
                        </div>

                    </div>

                </div>

                <button type="button" class="btn btn-float btn-fba">
                    <i class="icon-back"></i>
                </button>
            </div>
            @if (!$helper->inAdmin())
                @include('sidebar')
            @endif
        </div>

    </main>
@endsection
@section('styles')
<style>
	.panel-body {
		background: #fff;
	}
	.table thead th:not(:first-child),
	.table tbody td:not(:first-child) {
		text-align: center;
		min-width: 40px!important
	}
	.table-competency thead th:not(:first-child) span {
		color: #fff;
		font-weight: bold;
	}
	.table-competency thead th:nth-child(2){
		background-color: rgb(237,27,37);
	}
	.table-competency thead th:nth-child(3){
		background-color: rgb(255, 159, 64);
	}
	.table-competency thead th:nth-child(4){
		background-color: rgb(255,213,0);
	}
	.table-competency thead th:nth-child(5){
		background-color: rgb(216,216,216)
	}
	.table-competency thead th:nth-child(5) span { color: #333; }

</style>
@endsection
@section('scripts')
    <script src="{{ url('js/dashboard-user.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('#employeeList').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                pageLength:5,
                ajax: '{{route('trainee.report.getTraineeUser')}}',
                columns: [
                    {data: 'full_name', name: 'full_name'},
                    {data: 'form_group_id', name: 'form_group_id'},
                    {data: 'user_position', name: 'user_position'},
                    {data: 'options', name: 'options',title: 'Options'}
                ]
            });
        })

        window.chartColors = {
            0: 'rgb(237,27,37)',
            1: 'rgb(255, 159, 64)',
            2: 'rgb(255,213,0)',
            3: 'rgb(153,204,0)',
            4: 'rgb(101,196,219)'
        };

        var colors = ["rgb(237,27,37)", "rgb(255, 159, 64)", "rgb(255,213,0)",'rgb(153,204,0)'];


        var barChartData = {
            labels: [
                        @foreach($report['MonthInYearName'] as $item)
                        "{{$item}}",
                        @endforeach
                    ],
            datasets : [
                @foreach($report['monthly'] as $key => $item)
                    {
                        label: '{{$item['form_alias']}}',
                        backgroundColor: colors['{{$key}}'],
                        data: [
                            @foreach($report['rangeMonthly'][$item['form_alias']] as $range)
                            {{$range}},
                            @endforeach
                        ],
                        borderWidth: 1
                    },
                @endforeach
            ]
        };
        window.onload = function() {
            var ctx = document.getElementById('chartTwoContainerMobile').getContext('2d');
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
                    title: {
                        display: true,
                        text: 'Training Type'
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        xAxes: [{
                            stacked: true,                            
                        }],
                        yAxes: [{
                            stacked: true,
                            beginAtZero:true,
                            display: false //this will remove only the label

                        }]
                    },
                    animation: {
                      onComplete: function () {
                        var chartInstance = this.chart;
                        var ctx = chartInstance.ctx;
                        var height = chartInstance.controller.boxes[0].bottom;
                        ctx.textAlign = "center";
                        Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
                          var meta = chartInstance.controller.getDatasetMeta(i);
                          Chart.helpers.each(meta.data.forEach(function (bar, index) {
                            if(dataset.data[index] > 0){
                                ctx.fillText( dataset.data[index], bar._model.x, bar._model.y + 20  );    
                            }
                          }),this)
                        }),this);

                        var canvasLink = document.getElementById('chartTwoContainerMobile').toDataURL();
                        $('#chartTwoContainerMobileImage').attr('href', canvasLink);
                      }
                    }

                }
            });
        };

        $(document).ready(function(){
            $('.nav-pills').find('a').click(function(event){
                event.preventDefault();
            });

            $('#chartTwoContainerMobilePDF').click(function(){
                downloadPDF('chartTwoContainerMobile', 'chart-training');
            });

            $('#chartTwoContainerMobileFilter').change(function(){
                var year = $(this).val();

                $.ajax({
                    method: "GET",
                    dataType: "json",
                    url: "{{route('trainee.dashboard.filterChartValue')}}",
                    data: { year : year }
                }).done(function( response ) {
                    var datasets = [];

                    $(document).find('#exportChartTraining').attr('href', "{{route('trainee.dashboard.exportChartTraining')}}?year="+year);
                    $(document).find("#chartTwoContainerMobileContainer").html('<canvas id="chartTwoContainerMobile" class="d-block d-md-none" height="500"></canvas>');
    
                    $.each(response.monthly, function(key, value) {
                        var data = [];
                        $.each(response.rangeMonthly[value.form_alias], function(rangeKey, range) {
                            data[(rangeKey-1)] = range;
                        });

                        datasets[key] = {
                            label: value.form_alias,
                            backgroundColor: colors[key],
                            data: data,
                            borderWidth: 1
                        }; 
                    })

                    var barChartData = {
                        labels: [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December",
                        ],
                        
                        datasets : datasets
                    };

                    var ctx = document.getElementById('chartTwoContainerMobile').getContext('2d');
                    window.myBar = new Chart(ctx, {
                        type: 'bar',
                        data: barChartData,
                        options: {
                            title: {
                                display: true,
                                text: 'Training Type'
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false
                            },
                            responsive: true,
                            maintainAspectRatio: false,
                            scales: {
                                xAxes: [{
                                    stacked: true,                            
                                }],
                                yAxes: [{
                                    stacked: true,
                                    beginAtZero:true,
                                    display: false 
                                }]
                            },
                            animation: {
                                onComplete: function () {
                                    var chartInstance = this.chart;
                                    var ctx = chartInstance.ctx;
                                    var height = chartInstance.controller.boxes[0].bottom;
                                    ctx.textAlign = "center";
                                    Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
                                        var meta = chartInstance.controller.getDatasetMeta(i);
                                        Chart.helpers.each(meta.data.forEach(function (bar, index) {
                                            if(dataset.data[index] > 0){
                                                ctx.fillText( dataset.data[index], bar._model.x, bar._model.y + 20  );    
                                            }
                                        }),this)
                                    }),this);

                                    var canvasLink = document.getElementById('chartTwoContainerMobile').toDataURL();
                                    $(document).find('#chartTwoContainerMobileImage').attr('href', canvasLink);
                                }
                            }
                        }
                    });
                });
            });
        });
    </script>
@endsection
