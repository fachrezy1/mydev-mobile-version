require('../../../../../resources/js/duplicate')

var button = $('.custom-radio'),
    nextWizard = $('#nextWizard'),
    submitWizard = $('#submitWizard'),
    parent = $('.custom-radio-parent'),
    next = $('.trainee-form-request-next_submit-button'),
    textarea = $('.trainee-form-request-content_textarea-text'),
    textarea_counter = $('.trainee-form-request-content_textarea-counter'),
    formGroup = '',
    dateStart=null,
    dateEnd=null,
    dateStartSelector = $('#dateStart'),
    dateEndSelector = $('#dateEnd')

$(document).ready(function () {

    button.on('click', function (e) {
        let input = '#'+$(this).data('for')
        parent.removeClass('active')
        $(this).parents('.card').addClass('active')
        $(input).click()
        formGroup = $(input).val();
    })

    nextWizard.on('click', function (e) {
    })


    next.click(function () {
        $('#stepForm').submit()
    })


})

function countRangeDate(start=null,end=null) {
    if (start) {
        dateStart = start
    }

    if (end) {
        dateEnd = end
    }

    return moment(dateStart).twix(dateEnd).length('days');
}

function validateDateRange(lengh) {
    if (lengh !== false) {
        if (lengh < 1) {
            nextWizard.attr('disabled','disabled')
            submitWizard.attr('disabled','disabled')
            console.log($(this).siblings('small'))
            $('.training_date_warn').removeClass('d-none')
        } else {
            nextWizard.removeAttr('disabled')
            submitWizard.removeAttr('disabled')
            console.log($(this).siblings('small'))
            $('.training_date_warn').addClass('d-none')
        }
    } else {
        return false
    }
}

dateStartSelector.on('change',function () {
    let lengh = countRangeDate($(this).val())
    validateDateRange(lengh)

})

dateEndSelector.on('change',function () {
    let lengh = countRangeDate(null,$(this).val())
    validateDateRange(lengh)
})

function validateTextArea(position) {
    let result = validate({max256Chars:$(position).val()}, constraints)
    if (typeof result !== 'undefined') {
        $(position).siblings('small').removeClass('d-none')
    } else {
        $(position).siblings('small').addClass('d-none')
    }
}

$('textarea').on({
    keypress:function () {
        validateTextArea(this)
    },
    keyup: function () {
        validateTextArea(this)
    },
    change: function () {
        validateTextArea(this)
    }
})
