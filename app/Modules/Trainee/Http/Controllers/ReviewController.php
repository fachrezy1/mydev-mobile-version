<?php

namespace App\Modules\Trainee\Http\Controllers;

use Illuminate\Http\Request;

use App\Modules\Trainee\Models\requestsModel;
use App\Modules\Trainee\Models\reviewModel;

//add
use App\Modules\Trainee\Models\requestsGroupModel;
use App\Modules\Trainee\Models\ratingModel;
//end add

use App\Http\Controllers\Controller;

use App\Modules\Trainee\Models\traineeModel;


class ReviewController extends Controller
{
    //
    public function view($id) {
//        dd($user_id);
        $model = new requestsModel();
        $reviewModel = new reviewModel();
        //get current request
        $request = $model->getRequestByFormId($id,['user_id','form_type','request_group_id']);
        $form['form_group_id'] = $request['form_detail']['form_group_id'];

        if (!$request['form_detail']['is_accepted']) {
            //if form not accepted yet
            return redirect(route('trainee.myTrainee'))->withError('Form need to approved before input review');
        }

        $data['is_completed'] = $request['is_completed'];
        unset($request['is_completed']);

        //ger review from current request
        $merged = array_merge($form, $request);
        $where = $merged;
        //remove unnecessary index from where
        unset($where['form_group_id']);
        unset($where['form_detail']);
        $review = $reviewModel->where($where)->get();

        //store current request information
        $data['form'] = $merged;
        $data['current_data'] = null;
        if ($review->isNotEmpty()){
            // if review exist
            $data['current_data'] = $review->toArray();
        }

        /* new */
        $requestGroup = requestsGroupModel::find($id);
        $rating_count = ratingModel::where('request_group_id','=',$id)->count();
        // end new //

        /* new */
        $data['requestGroup'] = $requestGroup;
        $data['rating_count'] = $rating_count;
        //dd($data['rating_count']);
        /* end new */

        $data['trainees'] = traineeModel::where(['id'=>$data['form']['form_group_id']])->get()->toArray()[0];
        return view('trainee::review',$data);
    }

    public function viewByUserId($user_id,$id) {
        $model = new requestsModel();
        $reviewModel = new reviewModel();
        
        /* new */
        $requestGroup = requestsGroupModel::find($id);
        $rating_count = ratingModel::where('request_group_id','=',$id)->count();
        // end new //

        //get current request
        $request = $model->getRequestByFormId($id,['user_id','form_type','request_group_id'],$user_id);
        $form['form_group_id'] = $request['form_detail']['form_group_id'];
        if (!$request['form_detail']['is_accepted'] && $request['form_detail']['form_group_id'] != 2) {
            //if form not accepted yet
            return redirect(route('trainee.myTrainee'))->withError('Form need to approved before input review');
        }

        $data['is_completed'] = $request['is_completed'];
        unset($request['is_completed']);

        //ger review from current request
        $merged = array_merge($form, $request);
        $where = $merged;
        //remove unnecessary index from where
        unset($where['form_group_id']);
        unset($where['form_detail']);
        $review = $reviewModel->where($where)->get();

        //store current request information
        $data['form'] = $merged;
        $data['current_data'] = null;
        if ($review->isNotEmpty()){
            // if review exist
            $data['current_data'] = $review->toArray();
        }

        /* new */
        $data['requestGroup'] = $requestGroup;
        $data['rating_count'] = $rating_count;
        //dd($data['rating_count']);
        /* end new */

        $data['trainees'] = traineeModel::where(['id'=>$data['form']['form_group_id']])->get()->toArray()[0];
        return view('trainee::review',$data);
    }

}
