<?php

namespace App\Modules\Trainee\Http\Controllers\Requests;

use App\Helpers\UserRolesHelper;
use App\Modules\Admindashboard\Http\Requests\StoreReviews;

use App\Http\Controllers\Controller;
use App\Modules\Admindashboard\Models\reviewModel;
use App\Modules\Auth\Models\EmployeeModel;
use App\Modules\Auth\Models\UserModel;
use App\Modules\Trainee\Models\requestsGroupModel;
use App\Modules\Trainee\Models\traineeModel;
use App\User;
use App\Modules\Trainee\Models\requestsModel;
use App\Notifications\Trainee;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Modules\Auth\Models\UserRolesModel;

use App\Helpers\ActivityHelper;
use App\Modules\Trainee\Models\users_point_history_model;

use Sentinel;

use Mail;
use App\Mail\TraineeMailNew;

use App\Modules\Trainee\Models\ratingModel;
use App\Modules\Admindashboard\Models\multiplicationModel;

class ReviewRequestsController extends Controller
{
    //

    protected $reviewModel;
    protected $requestGroupModel;

    function __construct()
    {
        $this->reviewModel = new reviewModel();
        $this->requestGroupModel = new requestsGroupModel();

    }

    function store(StoreReviews $request) {
        if (!$request->user_id) {
            $user = Sentinel::check();
        } else {
            $user = Sentinel::findById($request->user_id);
        }

        if ($user->hasAccess('trainee.manage') || $user->hasAccess('admin.createTrainee') ) {
            if ($request->hasFile('reviews_attachment')) {
                $request->file();

                $file = $request->validate([
                    'reviews_attachment'=>'mimes:pdf,doc,docx,jpeg,jpg,png'
                ]);

                Storage::disk('uploads')->putFileAs('public-training',$request->file('reviews_attachment'),$user->nik.$request->file('reviews_attachment')->getClientOriginalName());
                $validated = $request->toArray();
                $validated['reviews_attachment'] = [
                    0 => 'public-training/'.$user->nik.$request->file('reviews_attachment')->getClientOriginalName()
                ];
            } else {
                $validated = $request->toArray();
            }

            $submitButton = $validated['submit_button'];
            unset($validated['submit_button']);
            unset($validated['_token']);

            $this->setData($validated);

            $where = [
                'user_id' => $validated['user_id'],
                'id' => $validated['request_group_id'],
            ];

            //tambahan
            $where2 = [
                'user_id' => $validated['user_id'],
                'request_group_id' => $validated['request_group_id'],
            ];
            //end tambahan

            if ($submitButton == 'completed') {
                $role = new UserRolesHelper();
                $form_group_id = $this->requestGroupModel->where($where)->get(['form_group_id'])->first()->form_group_id;
                $where_group = $where;
                $where_group['is_completed_by_manager'] = 1;

                if ($this->requestGroupModel
                    ->where($where_group)->get()->isNotEmpty()){
                    return back()->with('success','request completed');
                }

                // tambahan
                $this->reviewModel
                    ->where($where2)
                    ->update(
                        [
                            'flagging' => 1,
                        ]
                );
                // endtambahan

                $this->requestGroupModel
                    ->where($where)
                    ->update(
                        [
                            'is_completed_by_manager' => 1,

                            'is_completed_by_staff' => 1,//tambaha disini auto done
                            
                            'is_completed' => 1,
                        ]
                    );

                if($role->inAdmin()){
                    $employee_id = User::where(['username'=>$request->get('employee_nik')])->get(['id']);
                    $manager = Sentinel::check();

                    if($employee_id->isEmpty()){
                        $allowed_manager_key = ['vp', 'svp', 'dir'];
                        $employee_data = EmployeeModel::where('nik', $request->get('employee_nik'))->first();

                        $register_user = Sentinel::registerAndActivate([
                            'email'    => $employee_data->email,
                            'password' => now()->timestamp,
                            'username' => $request->get('employee_nik')
                        ]);

                        //assign staff role to user
                        if(!empty($employee_data->manager_id)){
                            $staff_role = UserRolesModel::where('slug','staff')->first();
                            $register_user->roles()->save($staff_role);
                        }

                        //assign head role to user
                        if(in_array(strtolower($employee_data->job_key_short), $allowed_manager_key)){
                            $staff_role = UserRolesModel::where('slug','atasan')->first();
                            $register_user->roles()->save($staff_role);
                        }
                        $employee_id = $register_user->id;
                    } else {
                        $employee_id = $employee_id[0]['id'];
                    }

                    $employee = User::find($employee_id);

                    $request_group = $this->requestGroupModel->where($where)->get()->first()->toArray();

                    $form_name = requestsModel::groupById[$request_group['form_group_id']];

                    $messages = [
                        'message_header' => '',
                        'message_body' => '<p>Congratulations on your Coaching/Mentoring completion!</p>
                            <p>As your mentor/coach: '.$manager->first_name.' '.$manager->last_name.' has submitted your mentoring/coaching report, now you are able to finish the Coaching/Mentoring process by clicking \'done\' <a href="'.url('mydev/trainee/my-trainee').'">here</a></p>
                            <p>
                                Be sure to share your development experience with us & give us 5-star ;) <br/>
                                See you on your next development opportunities!
                            </p>',
                        'message_date' => now()->timestamp,
                        'url' => '/mydev/trainee/my-trainee',
                        'type' => 'form request',
                        'subject' => 'Your Coaching/Mentoring Process is Almost Complete',
                        'form_group' => $form_name,
                        'to' => $employee->email
                    ];

                    $request_group = requestsGroupModel::where(['id'=>$request->get('request_group_id')])->get(['form_group_id'])->first();
                    $trainee_point = traineeModel::where(['id'=>$request_group->form_group_id])->get(['point'])->first()->point;
                    $employee_point  = User::where(['id'=>$employee_id])->get(['point'])->first()->point + $trainee_point;

                    User::where(['id'=>$employee_id])->update(['point'=>$trainee_point]);
                    EmployeeModel::where(['nik'=>$request->get('employee_nik')])->update(['points'=>$trainee_point]);

                    $employee->notify(new Trainee($messages));
                    
                    
                    if(isset($employee->email)){
                         Mail::to($employee->email)->send(new TraineeMailNew($messages));
                    }
                    
                    

                    $messages['url'] = '/dashboard/trainee-request';
                    notify_admin($messages);
                } else {

                    $staff = User::find($user->id);
                    $manager = Sentinel::check();

                    $request_group = $this->requestGroupModel->where($where)->get()->first()->toArray();

                    $form_name = requestsModel::groupById[$request_group['form_group_id']];
                    $messages = [
                        'message_header' => '',
                        'message_body' => 'Form report '.$form_name.' Completed by :'.$manager->first_name.' '.$manager->last_name,
                        'message_date' => now()->timestamp,
                        'url' => '/trainee/preview/'.$request_group['user_id'].'/'.$request_group['id'],
                        'type' => 'form request',
                        'subject' => 'Form report completed',
                        'form_group' => $form_name,
                        'to' => $staff->email
                    ];

                    

                    if(isset($staff->email)){
                         Mail::to($staff->email)->send(new TraineeMailNew($messages));
                    }
                    
                    
                    
                    $staff->notify(new Trainee($messages));
                    User::find($request_group['second_accepter'])->notify(new Trainee($messages));
                }
            }

            return back()->with('success','Completed');
        }

        return false;
    }

    private function setData($validated) {
        $review_id = null;
        if (isset($validated['review_id'])) {
            $review_id = $validated['review_id'];
            unset($validated['review_id']);
        }
            if ($review_id) {
                //case 1: dimana data sebagian form adalah data lama dan sebagian lagi data baru
                foreach ($review_id as $keyId=>$id) {
                    //ambil data dari data original
                    foreach($validated as $key=>$item){
                        if (is_array($item)) {
                            if (isset($item[$keyId])) {
                                $data[$key] = $item[$keyId];
                            }
                            //hapus data yang telah didapatkan agar tidak diulang lagi
                            unset($validated[$key][$keyId]);
                        } else {
                            $data[$key] = $item;
                        }
                    }
                    //segera update data
                    $this->reviewModel->updateOrCreate(
                        ['id'=>$id],
                        $data);
                }
                //panggil kembali fungsi jika masih ada item dalam array
                if (count($validated[$key]) > 0){
                    $this->setData($validated);
                }
            } else {
                $data=[];
                foreach ($validated as $key=>$item) {
                    //setelah review id direset jika masih ada data dalam $validated
                        if (is_array($item)) {
                            foreach ($item as $itemKey=>$value){
                                if (!isset($data[$key])) {
                                    $data[$key] = $value;
                                    unset($validated[$key][$itemKey]);
                                }
                            }
                        } else {
                            $data[$key] = $item;
                        }
                }
                //create data

                $this->reviewModel->Create($data);
                //panggil kembali fungsi jika masih ada item dalam array
                if (count($validated[$key]) > 0 ){
                    $this->setData($validated);
                }
            }

    }

    /*
    public function approvePoint(Request $request){
        $activity = new ActivityHelper();
        

        $totalHours = (int)  ( $request->get('training_ti_hours') ? $request->get('training_ti_hours') : ( $request->get('total_hours_review') ? $request->get('total_hours_review') : 0 ) );
        $trainee = traineeModel::where(['id'=>$request->form_group_id])->get()->toArray()[0];

        $requestGroup = requestsGroupModel::where(['id'=>$request->request_group_id])->get()->toArray()[0];

        if ($request->form_group_id == 2 || $request->form_group_id == 5) {
            $user = User::where(['username'=>$request->mentor])->first();

            if(!$user){
                $employee = EmployeeModel::where('nik',$request->mentor)->get()->toArray()[0];

                Sentinel::registerAndActivate([
                    'email'    => $employee['email'],
                    'password' => now()->timestamp,
                    'username' => $employee['nik']
                ]);

                $user = User::where(['username'=>$request->mentor])->first();
            }

        }
//        $form = ucwords(str_replace('_',' ',User::find($requestGroup['first_accepter'])));

        $message = [
            'message_body' => 'Yay! Sharing your knowledge & skill is the best way for you to keep improving and we really appreciate that. Thus, here’re some points for you.',
            'message_header' => '',
            'message_date' => now()->timestamp,
            'url' => '/trainee/point/redeem',
            'type' => 'form request',
            'subject' => 'You Earned Points!',
            'form_group' => requestsModel::getGroupName($request->form_group_id),
            'to' => $user->email
        ];

        //always off
        //$user->notify(new Trainee($message));
        /*
        if(isset($user->email)){
            Mail::to($user->email)->send(new TraineeMailNew($message));
        }
        */

        /*

        User::where(['username'=>$request->mentor])
            ->update(['point'=> $user->point+ ( $trainee['point'] * $totalHours) ]);


        requestsGroupModel::where(['id'=>$request->request_group_id])
            ->update(['is_completed_by_admin'=>1]);


        if($request->form_group_id == 5){
            //if form inhouse set form to completed
            requestsGroupModel::where(['id'=>$request->request_group_id])
                ->update(['is_completed'=>1]);

            $useFormId = requestsModel::where([
                'requests_group_id' => $request->request_group_id
            ])->get(['user_id'])->first();

            requestsModel::create([
                'requests_group_id' => $request->request_group_id,
                'form_group_id' => $request->form_group_id,
                'user_id' => $useFormId['user_id'],
                'form_answer' => 'training_ti_hours',
                'request_content' => $totalHours
            ]);

        }

        //log history point of user
        $point_history_model = new users_point_history_model();
        $point_history = [
            'user_id' => $user->id,
            'form_group_id' => $request->form_group_id,
            'requests_group_id' => $request->request_group_id,
            'point' => $trainee['point'] * $totalHours
        ];
        $point_history_model::create($point_history);

        //record activity admin
        $admin = Sentinel::check();
        $activity->logActivity($admin,['log_type'=>'Point Approval','log_data'=>['ip_address'=>\Request::getClientIp(true),'status'=>'Success','badge_class'=>'badge-success','date'=>now()],],'You\'ve been approved point ');

        /*
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', env('API_URL', 'http://localhost/').'course/mydevpoint?action=Mydev - Point Earning&email='.$user->email.'&point='.($trainee['point'] * $totalHours));
        $callback = json_decode($response->getBody()->getContents());
        
        if($callback->error){
            // debug code here
        }
        */

        /*
        return back()->with('success','Success');
    }

    */



/*
    public function approvePoint(Request $request){
        

      

        

        $tot_rating=0;
        $final_multiplication=0;
        $check_rating=ratingModel::where('request_group_id',$request->request_group_id)->get();



     


        
        foreach($check_rating as $ch){
             $tot_rating +=$ch->rating;
        }
        $final_rating=round($tot_rating/count($check_rating));
        
        $check_multiplication=multiplicationModel::where('trainee_id',$request->form_group_id)->first();

        

        if($final_rating==1 or $final_rating==2){
            $final_multiplication=$check_multiplication->tier_2_1;
        }elseif($final_rating==3){
             $final_multiplication=$check_multiplication->tier_3;
        }elseif($final_rating==4){
             $final_multiplication=$check_multiplication->tier_4;
        }elseif($final_rating==5){
             $final_multiplication=$check_multiplication->tier_5;
        }



   
   



        $activity = new ActivityHelper();


        $totalHours = (int)  ( $request->get('training_ti_hours') ? $request->get('training_ti_hours') : ( $request->get('total_hours_review') ? $request->get('total_hours_review') : 0 ) );

        $trainee = traineeModel::where(['id'=>$request->form_group_id])->get()->toArray()[0];
        $requestGroup = requestsGroupModel::where(['id'=>$request->request_group_id])->get()->toArray()[0];

        if ($request->form_group_id == 2 || $request->form_group_id == 5) {




            $user = User::where(['username'=>$request->mentor])->first();

            if(!$user){
                $employee = EmployeeModel::where('nik',$request->mentor)->get()->toArray()[0];

                Sentinel::registerAndActivate([
                    'email'    => $employee['email'],
                    'password' => now()->timestamp,
                    'username' => $employee['nik']
                ]);

                $user = User::where(['username'=>$request->mentor])->first();
            }

        }
//        $form = ucwords(str_replace('_',' ',User::find($requestGroup['first_accepter'])));

        $message = [
            'message_body' => 'Yay! Sharing your knowledge & skill is the best way for you to keep improving and we really appreciate that. Thus, here’re some points for you.',
            'message_header' => '',
            'message_date' => now()->timestamp,
            'url' => '/trainee/point/redeem',
            'type' => 'form request',
            'subject' => 'You Earned Points!',
            'form_group' => requestsModel::getGroupName($request->form_group_id),
            'to' => $user->email
        ];

        //$user->notify(new Trainee($message));
        /*
        if(isset($user->email)){
            Mail::to($user->email)->send(new TraineeMailNew($message));
        }
        */




/*
    
        User::where(['username'=>$request->mentor])
            ->update(['point'=> $user->point+ (($final_multiplication * $trainee['point']) * $totalHours) ]);


      
        requestsGroupModel::where(['id'=>$request->request_group_id])
            ->update(['is_completed_by_admin'=>1]);


        if($request->form_group_id == 5){
            //if form inhouse set form to completed
            requestsGroupModel::where(['id'=>$request->request_group_id])
                ->update(['is_completed'=>1]);

            $useFormId = requestsModel::where([
                'requests_group_id' => $request->request_group_id
            ])->get(['user_id'])->first();

            requestsModel::create([
                'requests_group_id' => $request->request_group_id,
                'form_group_id' => $request->form_group_id,
                'user_id' => $useFormId['user_id'],
                'form_answer' => 'training_ti_hours',
                'request_content' => $totalHours
            ]);

        }

        //log history point of user
        $point_history_model = new users_point_history_model();
        $point_history = [
            'user_id' => $user->id,
            'form_group_id' => $request->form_group_id,
            'requests_group_id' => $request->request_group_id,
            'point' => ($final_multiplication * $trainee['point']) * $totalHours
        ];
        $point_history_model::create($point_history);

        //record activity admin
        $admin = Sentinel::check();
        $activity->logActivity($admin,['log_type'=>'Point Approval','log_data'=>['ip_address'=>\Request::getClientIp(true),'status'=>'Success','badge_class'=>'badge-success','date'=>now()],],'You\'ve been approved point ');

        /*
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', env('API_URL', 'http://localhost/').'course/mydevpoint?action=Mydev - Point Earning&email='.$user->email.'&point='.($trainee['point'] * $totalHours));
        $callback = json_decode($response->getBody()->getContents());
        
        if($callback->error){
            // debug code here
        }
        */

        /*
        return back()->with('completed', 'Point has been updated!');
    }

    */

    /*

     public function approvePoint(Request $request){
        

       $user = Sentinel::check();



        $tot_rating=0;
        $final_multiplication=0;

        if($request->form_group_id==5){

            $tier=$request->tier;
            $pecah=explode("^",$tier);

            $final_multiplication=$pecah[0];
            $rating_inhouse=$pecah[1];

           
            $requestGroupModel = new requestsGroupModel();
            $role = new UserRolesHelper();

            $data = $request->toArray();
            unset($data['_token']);
       
            $requestGroup = $requestGroupModel->where(['id'=>$request->request_group_id])->get();



            if ($requestGroup->isEmpty()) {
                return back()->withError('Request Not Found');
            } else {

            }

             $data_rating=array(
                        'staff_id'         => $user->id,
                        'mentor_id'        => $request->mentor,
                        'request_group_id' => $request->request_group_id,
                        'from_type_id' => 0,
                        'rating' => $rating_inhouse,
                        'notes' => "admin",

            );

            $add_new_rating = ratingModel::create($data_rating);
            

        $mail = [
            'message_header' => '',
            'subject' => 'Check Out What They Say about Your Sessions',
            'message_body' => '<p>Thank you for sharing your knowledge & improving others!</p>
                <p>Let’s improve your coaching/mentoring experience by learning from your previous coaching/mentoring review <a href="'.url('/trainee/preview/'.$requestGroup[0]['user_id'].'/'.$data['request_group_id']).'">here</a></p>
                <p>See you on the next development opportunities!</p>',
            'message_date' => now()->timestamp,
            'url' => '/trainee/preview/'.$requestGroup[0]['user_id'].'/'.$data['request_group_id'],
            'type' => 'form request',
            'form_group' => requestsModel::groupById[$requestGroup[0]->form_group_id]
        ];

        $message = $mail;

       

        if($role->inAdmin($data['mentor'])){
            $admin = User::where('username',$request->mentor)->first();
            $message['to'] = $admin->email;
            $admin->notify(new Trainee($message));
            
             /*
             Mail::to($admin->email)->send(new TraineeMailNew($message));
             */


             /*
             

        }
        

        }else{

        

        $check_rating=ratingModel::where('request_group_id',$request->request_group_id)->get();
        foreach($check_rating as $ch){
             $tot_rating +=$ch->rating;
        }
        $final_rating=round($tot_rating/count($check_rating));
        
        $check_multiplication=multiplicationModel::where('trainee_id',$request->form_group_id)->first();

        

        if($final_rating==1 or $final_rating==2){
            $final_multiplication=$check_multiplication->tier_2_1;
        }elseif($final_rating==3){
             $final_multiplication=$check_multiplication->tier_3;
        }elseif($final_rating==4){
             $final_multiplication=$check_multiplication->tier_4;
        }elseif($final_rating==5){
             $final_multiplication=$check_multiplication->tier_5;
        }



       }

     

   



        $activity = new ActivityHelper();


        $totalHours = (int)  ( $request->get('training_ti_hours') ? $request->get('training_ti_hours') : ( $request->get('total_hours_review') ? $request->get('total_hours_review') : 0 ) );

        $trainee = traineeModel::where(['id'=>$request->form_group_id])->get()->toArray()[0];
        $requestGroup = requestsGroupModel::where(['id'=>$request->request_group_id])->get()->toArray()[0];

        if ($request->form_group_id == 2 || $request->form_group_id == 5) {




            $user = User::where(['username'=>$request->mentor])->first();

            if(!$user){
                $employee = EmployeeModel::where('nik',$request->mentor)->get()->toArray()[0];

                Sentinel::registerAndActivate([
                    'email'    => $employee['email'],
                    'password' => now()->timestamp,
                    'username' => $employee['nik']
                ]);

                $user = User::where(['username'=>$request->mentor])->first();
            }

        }
//        $form = ucwords(str_replace('_',' ',User::find($requestGroup['first_accepter'])));

        $message = [
            'message_body' => 'Yay! Sharing your knowledge & skill is the best way for you to keep improving and we really appreciate that. Thus, here’re some points for you.',
            'message_header' => '',
            'message_date' => now()->timestamp,
            'url' => '/trainee/point/redeem',
            'type' => 'form request',
            'subject' => 'You Earned Points!',
            'form_group' => requestsModel::getGroupName($request->form_group_id),
            'to' => $user->email
        ];

        //$user->notify(new Trainee($message));

        /*
        
        if(isset($user->email)){
            Mail::to($user->email)->send(new TraineeMailNew($message));
        }
        */
        




        /*
    
        User::where(['username'=>$request->mentor])
            ->update(['point'=> $user->point+ (($final_multiplication * $trainee['point']) * $totalHours) ]);


      
        requestsGroupModel::where(['id'=>$request->request_group_id])
            ->update(['is_completed_by_admin'=>1]);


        if($request->form_group_id == 5){
            //if form inhouse set form to completed
            requestsGroupModel::where(['id'=>$request->request_group_id])
                ->update(['is_completed'=>1]);

            $useFormId = requestsModel::where([
                'requests_group_id' => $request->request_group_id
            ])->get(['user_id'])->first();

            requestsModel::create([
                'requests_group_id' => $request->request_group_id,
                'form_group_id' => $request->form_group_id,
                'user_id' => $useFormId['user_id'],
                'form_answer' => 'training_ti_hours',
                'request_content' => $totalHours
            ]);

        }

        //log history point of user
        $point_history_model = new users_point_history_model();
        $point_history = [
            'user_id' => $user->id,
            'form_group_id' => $request->form_group_id,
            'requests_group_id' => $request->request_group_id,
            'point' => ($final_multiplication * $trainee['point']) * $totalHours
        ];
        $point_history_model::create($point_history);

        //record activity admin
        $admin = Sentinel::check();
        $activity->logActivity($admin,['log_type'=>'Point Approval','log_data'=>['ip_address'=>\Request::getClientIp(true),'status'=>'Success','badge_class'=>'badge-success','date'=>now()],],'You\'ve been approved point ');

        /*
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', env('API_URL', 'http://localhost/').'course/mydevpoint?action=Mydev - Point Earning&email='.$user->email.'&point='.($trainee['point'] * $totalHours));
        $callback = json_decode($response->getBody()->getContents());
        
        if($callback->error){
            // debug code here
        }
        */

/*        return back()->with('completed', 'Point has been updated!');
    }
    */



  public function approvePoint(Request $request){
        
       $activity = new ActivityHelper();
       $user_login = Sentinel::check();




        $tot_rating=0;
        $final_multiplication=0;

        if($request->form_group_id==5){

            $tier=$request->tier;
            $rating_inhouse=$tier;

            

           
            $requestGroupModel = new requestsGroupModel();
            $role = new UserRolesHelper();

            $data = $request->toArray();
            unset($data['_token']);
       
            $requestGroup = $requestGroupModel->where(['id'=>$request->request_group_id])->get();

           

            if ($requestGroup->isEmpty()) {
                return back()->withError('Request Not Found');
            } else {

            }


            //mencari multiplication untuk menghitung point trainer//

            $multiplication=multiplicationModel::where('trainee_id',$request->form_group_id)->first();

            if($rating_inhouse==1){
                $final_multiplication=$multiplication->tier_2_1;
            }elseif($rating_inhouse==3){
                $final_multiplication=$multiplication->tier_3;

            }elseif($rating_inhouse==4){
                $final_multiplication=$multiplication->tier_4;

            }elseif($rating_inhouse==5){
                $final_multiplication=$multiplication->tier_5;

            }

           //mencari multiplication untuk menghitung point moderator//

           if($request->tier_moderator){


            $tier_moderator=$request->tier_moderator;
            $rating_moderator=$tier_moderator;
            $multiplication_model=multiplicationModel::where('trainee_id',6)->first();

            if($rating_moderator==1){
                $final_multiplication_moderator=$multiplication_model->tier_2_1;
            }elseif($rating_moderator==3){
                $final_multiplication_moderator=$multiplication_model->tier_3;

            }elseif($rating_moderator==4){
                $final_multiplication_moderator=$multiplication_model->tier_4;

            }elseif($rating_moderator==5){
                $final_multiplication_moderator=$multiplication_model->tier_5;

            }

             $data_rating_moderator=array(
                        'staff_id'         => $user_login->id,
                        'mentor_id'        => $request->moderator_nik,
                        'request_group_id' => $request->request_group_id,
                        'from_type_id' => 0,
                        'rating' => $rating_moderator,
                        'notes' => "admin",

            );

            $add_new_rating = ratingModel::create($data_rating_moderator);
          }

           
           
           //input rating mentor
           if($request->type_programs=="internal trainer"){

             $data_rating=array(
                        'staff_id'         => $user_login->id,
                        'mentor_id'        => $request->mentor,
                        'request_group_id' => $request->request_group_id,
                        'from_type_id' => 0,
                        'rating' => $rating_inhouse,
                        'notes' => "admin",

            );

            $add_new_rating = ratingModel::create($data_rating);

          }

          //input rating moderator

           
            

        $mail = [
            'message_header' => '',
            'subject' => 'Check Out What They Say about Your Sessions',
            'message_body' => '<p>Thank you for sharing your knowledge & improving others!</p>
                <p>Let?s improve your coaching/mentoring experience by learning from your previous coaching/mentoring review <a href="'.url('/trainee/preview/'.$requestGroup[0]['user_id'].'/'.$data['request_group_id']).'">here</a></p>
                <p>See you on the next development opportunities!</p>',
            'message_date' => now()->timestamp,
            'url' => '/trainee/preview/'.$requestGroup[0]['user_id'].'/'.$data['request_group_id'],
            'type' => 'form request',
            'form_group' => requestsModel::groupById[$requestGroup[0]->form_group_id]
        ];

        $message = $mail;

       
        
        if($request->type_programs=='internal trainer'){

            if($role->inAdmin($data['mentor'])){
                $admin = User::where('username',$request->mentor)->first();
                $message['to'] = $admin->email;
                $admin->notify(new Trainee($message));
            

             Mail::to($admin->email)->send(new TraineeMailNew($message));
             

            }

        }


          if($request->tier_moderator){
            if($role->inAdmin($data['moderator_nik'])){
                $mail_moderator = User::where('username',$request->moderator_nik)->first();
                $message['to'] = $mail_moderator->email;
                $mail_moderator->notify(new Trainee($message));
            

             Mail::to($mail_moderator->email)->send(new TraineeMailNew($message));
             

            }

        }




        

        }else{

        

        $check_rating=ratingModel::where('request_group_id',$request->request_group_id)->get();
        foreach($check_rating as $ch){
             $tot_rating +=$ch->rating;
        }
        $final_rating=round($tot_rating/count($check_rating));
        
        $check_multiplication=multiplicationModel::where('trainee_id',$request->form_group_id)->first();

        

        if($final_rating==1 or $final_rating==2){
            $final_multiplication=$check_multiplication->tier_2_1;
        }elseif($final_rating==3){
             $final_multiplication=$check_multiplication->tier_3;
        }elseif($final_rating==4){
             $final_multiplication=$check_multiplication->tier_4;
        }elseif($final_rating==5){
             $final_multiplication=$check_multiplication->tier_5;
        }



       }

     





        $totalHours = (int)  ( $request->get('training_ti_hours') ? $request->get('training_ti_hours') : ( $request->get('total_hours_review') ? $request->get('total_hours_review') : 0 ) );

        
        
        $trainee = traineeModel::where(['id'=>$request->form_group_id])->get()->toArray()[0];
        

        $requestGroup = requestsGroupModel::where(['id'=>$request->request_group_id])->get()->toArray()[0];

        if ($request->form_group_id == 2 || ($request->form_group_id == 5 and $request->type_programs=="internal trainer")) {

            $user = User::where(['username'=>$request->mentor])->first();

            if(!$user){
                $employee = EmployeeModel::where('nik',$request->mentor)->get()->toArray()[0];

                Sentinel::registerAndActivate([
                    'email'    => $employee['email'],
                    'password' => now()->timestamp,
                    'username' => $employee['nik']
                ]);

                $user = User::where(['username'=>$request->mentor])->first();
            }

        }



        if ($request->form_group_id == 5 and $request->tier_moderator) {


            $user_moderator = User::where(['username'=>$request->moderator_nik])->first();

            if(!$user_moderator){
                $employee_moderator = EmployeeModel::where('nik',$request->moderator_nik)->get()->toArray()[0];

                Sentinel::registerAndActivate([
                    'email'    => $employee_moderator['email'],
                    'password' => now()->timestamp,
                    'username' => $employee_moderator['nik']
                ]);

                $user_moderator = User::where(['username'=>$request->moderator_nik])->first();
            }

        }
//        $form = ucwords(str_replace('_',' ',User::find($requestGroup['first_accepter'])));

  if ($request->form_group_id == 2 || ($request->form_group_id == 5 and $request->type_programs=="internal trainer")) {

    $message = [
            'message_body' => 'Yay! Sharing your knowledge & skill is the best way for you to keep improving and we really appreciate that. Thus, here?re some points for you.',
            'message_header' => '',
            'message_date' => now()->timestamp,
            'url' => '/trainee/point/redeem',
            'type' => 'form request',
            'subject' => 'You Earned Points!',
            'form_group' => requestsModel::getGroupName($request->form_group_id),
            'to' => $user->email
        ];

        //$user->notify(new Trainee($message));
        
        if(isset($user->email)){
            Mail::to($user->email)->send(new TraineeMailNew($message));
        }

        
        
     }
    if($request->form_group_id == 5 and $request->tier_moderator) {

        $message_moderator = [
            'message_body' => 'Yay! Sharing your knowledge & skill is the best way for you to keep improving and we really appreciate that. Thus, here?re some points for you.',
            'message_header' => '',
            'message_date' => now()->timestamp,
            'url' => '/trainee/point/redeem',
            'type' => 'form request',
            'subject' => 'You Earned Points!',
            'form_group' => requestsModel::getGroupName($request->form_group_id),
            'to' => $user_moderator->email
        ];


       /*
        if(isset($user_moderator->email)){
            Mail::to($user_moderator->email)->send(new TraineeMailNew($message_moderator));
        }
        */

    }

        


       


        if($request->form_group_id == 2 || ($request->form_group_id == 5 and $request->type_programs=="internal trainer")){
    
            $update_point_mentor=User::where(['username'=>$request->mentor])
            ->update(['point'=> $user->point+ (($final_multiplication * $trainee['point']) * $totalHours) ]);

        }

        if($request->form_group_id == 5 and $request->tier_moderator) {

             $trainee_moderator = traineeModel::where(['id'=>'6'])->get()->toArray()[0];

           $update_point_moderator=User::where(['username'=>$request->moderator_nik])
             ->update(['point'=> $user_moderator->point+ (($final_multiplication_moderator * $trainee_moderator['point']) * $totalHours) ]);

        }


      
        requestsGroupModel::where(['id'=>$request->request_group_id])
            ->update(['is_completed_by_admin'=>1]);


        if($request->form_group_id == 5){
            //if form inhouse set form to completed
            requestsGroupModel::where(['id'=>$request->request_group_id])
                ->update(['is_completed'=>1]);

            $useFormId = requestsModel::where([
                'requests_group_id' => $request->request_group_id
            ])->get(['user_id'])->first();

            requestsModel::create([
                'requests_group_id' => $request->request_group_id,
                'form_group_id' => $request->form_group_id,
                'user_id' => $useFormId['user_id'],
                'form_answer' => 'training_ti_hours',
                'request_content' => $totalHours
            ]);

        }

        //log history point of user
      if ($request->form_group_id == 2 || ($request->form_group_id == 5 and $request->type_programs=="internal trainer")) {
        $point_history_model = new users_point_history_model();
        $point_history = [
            'user_id' => $user->id,
            'form_group_id' => $request->form_group_id,
            'requests_group_id' => $request->request_group_id,
            'point' => ($final_multiplication * $trainee['point']) * $totalHours
        ];
        $point_history_model::create($point_history);
       }

        if($request->form_group_id == 5 and $request->tier_moderator){

            $trainee_moderator = traineeModel::where(['id'=>'6'])->get()->toArray()[0];

           $point_history_model = new users_point_history_model();
           $point_history_moderator = [
            'user_id' => $user_moderator->id,
            'form_group_id' => $request->form_group_id,
            'requests_group_id' => $request->request_group_id,
            'point' => ($final_multiplication_moderator * $trainee_moderator['point']) * $totalHours
           ];
           $point_history_model::create($point_history_moderator);


        }

        //record activity admin
        $admin = Sentinel::check();
        $activity->logActivity($admin,['log_type'=>'Point Approval','log_data'=>['ip_address'=>\Request::getClientIp(true),'status'=>'Success','badge_class'=>'badge-success','date'=>now()],],'You\'ve been approved point ');


        if($request->form_group_id == 5 and $request->type_programs=="external trainer"){

            $form_type = requestsModel::groupById[$request->form_group_id];

            $activity->logActivity($admin,['log_type'=>'Trainee Request','log_data'=>['ip_address'=>\Request::getClientIp(true),'badge_class'=>'badge-success','status'=>'completed','date'=>now()]],$admin->first_name.' '.$admin->last_name.' has been completed '.$form_type.' trainee');
        }


        /*
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', env('API_URL', 'http://localhost/').'course/mydevpoint?action=Mydev - Point Earning&email='.$user->email.'&point='.($trainee['point'] * $totalHours));
        $callback = json_decode($response->getBody()->getContents());
        
        if($callback->error){
            // debug code here
        }
        */
        return back()->with('completed', 'Point has been updated!');
    }


}
