<?php

namespace App\Modules\Trainee\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Modules\Admindashboard\Models\itemModel;
use App\Helpers\UserRolesHelper;
use App\Modules\Admindashboard\Models\itemRedeemModel;
use App\Modules\Admindashboard\Models\voucherModel;
use App\Modules\Auth\Models\UserModel;
use sentinel;
use App\Modules\Trainee\Models\users_point_history_model;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\User;
use App\Notifications\Trainee;

class PointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = new itemModel();
        $voucher = new voucherModel();
        $role = new UserRolesHelper();
        $user = sentinel::check();

        if (!$role->inStaff() && $user) {
            return redirect('404');
        }

        $data['user'] = $user->toArray();
        $data['items'] = $item->where('status','published')->get()->toArray();
        $data['vouchers'] = $voucher
            ->select('voucher.id', 'voucher.name', 'voucher.image', 'voucher.description', 'voucher.amount', 'voucher.point', 'voucher.status')
            ->join('voucher_codes', 'voucher_codes.voucher_id', '=', 'voucher.id')
            ->where('status', 1)
            ->where('user_id', 0)
            ->groupBy('voucher.id', 'voucher.name', 'voucher.image', 'voucher.description', 'voucher.amount', 'voucher.point', 'voucher.status')
            ->get()->toArray();

        $data['items_redeem'] = itemRedeemModel::where('user_id',$data['user']['id'])
            ->get()->groupBy('item_id')->toArray();

        $point_history = users_point_history_model::leftJoin('trainees', 'form_group_id', '=', 'trainees.id')
            ->where(['user_id'=>$user->id])
            ->limit(3)
            ->orderBy('users_point_history.id', 'desc')
            ->get(['trainees.name','trainees.icon', 'users_point_history.point', 'users_point_history.description']);
        $data['point_history'] = $point_history;

        return view('trainee::point.index',$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $role = new UserRolesHelper();

        if (!$role->inStaff()) {
            return redirect('404');
        }
        $user = sentinel::check()->toArray();
        $item = itemModel::where('id',$request->get('id'))->get();

        if ($item->isEmpty()) {
            return redirect('404');
        }
        $item = $item->first()->toArray();

        if (empty($item['quota'])) {
            return back()->withMessage('The item quota was empty');
        }

        if ($item['point'] > $user['point']) {
            return back()->withMessage('Your point are not enough');
        }
        
        $itemRedeem = new itemRedeemModel();
        $redeemed = [
          'user_id' => $user['id'],
          'item_id' => $item['id'],
          'point_from' => $user['point'],
          'point_to' => ($user['point'] - $item['point']),
          'approved' => 1
        ];

        $itemRedeem->create($redeemed);
        $user_point = $user['point'] - $item['point'];
        $remaining_quota = $item['quota'] - 1;

        itemModel::where('id',$item['id'])->update(['quota'=>$remaining_quota]);
        UserModel::where('id',$user['id'])->update(['point'=>$user_point]);

        return back()->withMessage('Redeemed');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $role = new UserRolesHelper();

        if ($role->inManager()){
            return redirect('404');
        }

        $item = itemModel::where('id',$id)->get();

        if ($item->isEmpty()){
            return redirect('404');
        }

        $data['item'] = $item->first()->toArray();
        $data['user'] = sentinel::check()->toArray();
        $data['items_redeem'] = itemRedeemModel::where('user_id',$data['user']['id'])
            ->get()->groupBy('item_id')->toArray();

        return view('trainee::point.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function list(Request $request){

        $user = Sentinel::check();

        if($request->get('draw')) {
            $searchData = $request->toArray();
            $items = itemRedeemModel::join('item', 'item.id', '=', 'item_redeem.item_id')
                ->where(['user_id'=>$user->id])
                ->where('name','like','%'.$searchData['search']['value'].'%')
                ->offset($searchData['start'])
                ->limit($searchData['length'])
                ->orderBy('item_redeem.created_at','desc')
                ->select('item.*','item_redeem.*')
                ->get()->toArray();

            $vouchers = itemRedeemModel::join('voucher_codes', 'voucher_codes.code', '=', 'item_redeem.voucher_code')
                ->join('voucher', 'voucher.id', '=', 'voucher_codes.voucher_id')
                ->where(['item_redeem.user_id'=>$user->id])
                ->where('name','like','%'.$searchData['search']['value'].'%')
                ->offset($searchData['start'])
                ->limit($searchData['length'])
                ->orderBy('item_redeem.created_at','desc')
                ->select('voucher.id as voucher_id', 'voucher.name', 'voucher.description', 'voucher.amount', 'voucher.point', 'voucher.status','item_redeem.*', 'voucher_codes.code')
                ->groupBy('item_redeem.id','voucher.id', 'voucher.name', 'voucher.description', 'voucher.amount', 'voucher.point', 'voucher.status', 'item_redeem.user_id', 'item_redeem.item_id', 'item_redeem.approved', 'item_redeem.created_at', 'item_redeem.updated_at', 'item_redeem.deleted_at', 'point_from', 'point_to', 'voucher_id', 'voucher_codes.code', 'item_redeem.voucher_code')
                ->get()->toArray();

            $items = array_merge($items,$vouchers);

            usort($items, function($a, $b) {
                return $a['id'] - $b['id'];
            });

            $items = collect($items);

            $searchKey = $searchData['search']['value'];
            if(!$searchData['search']['value']) {
                $searchKey = '';
            }
            $total = itemRedeemModel::join('item', 'item.id', '=', 'item_redeem.item_id')
                ->where(['user_id'=>$user->id])
                ->where('name','like','%'.$searchKey.'%')
                ->orderBy('item_redeem.created_at','desc')
                ->select('item_redeem.id','item.id')->get();

            if($items->isEmpty()){
                $return = [
                    'draw' => $searchData['draw'],
                    'recordsTotal' => count($total),
                    'recordsFiltered' => count($total),
                ];
                $return['data'] = [];
                return json_encode($return);
            }
            $items = $items->toArray();

            $return = [
                'draw' => $searchData['draw'],
                'recordsTotal' => count($total),
                'recordsFiltered' => count($total),
            ];
        } else {

            $items = itemRedeemModel::join('item', 'item.id', '=', 'item_redeem.item_id')
                ->where('user_id',$user->id)
                ->select('item.*','item_redeem.*')
                ->get();

            if($items->isEmpty()){
                $return = [
                    'draw' => 1,
                    'recordsTotal' => count($items),
                    'recordsFiltered' => count($items),
                ];
                $return['data'] = [];
                return json_encode($return);
            }

            $items = $items->toArray();

            $return = [
                'draw' => 1,
                'recordsTotal' => count($items),
                'recordsFiltered' => count($items),
            ];
        }
        
        foreach ($items as $key=>$value) {
            $return['data'][] = [
                '<a href="'.(!empty($value['voucher_code']) ? route('trainee.voucher.show',['id'=>$value['voucher_id']]): route('trainee.point.show',['id'=>$value['item_id']])).'" target="_blank">'.$value['name'].' '.(!empty($value['voucher_code']) ? '('.$value['code'].')': '').'</a>',
                '<img src="'.(!empty($value['voucher_code']) ? asset('images/voucher.png') : asset('uploads/'.$value['picture'])).'" />',
                $value['description'],
                'from '.(int)$value['point_from'].'pt to '.(int)$value['point_to'].'pt',
                Carbon::parse($value['created_at'])->format('l, d M, Y')
            ];
        }

        return json_encode($return);
    }

    public function changePoint(){
        $point = request()->get('point');
        $action = request()->get('action');
        $user_email = request()->get('email');
        $user_data = UserModel::where('email', $user_email)->first();

        if($user_data){
            $user_data->point = ($user_data->point + $point);
            $user_data->save();

            $point_history_model = new users_point_history_model();
            $point_history = [
                'user_id' => $user_data->id,
                'form_group_id' => 0,
                'requests_group_id' => 0,
                'point' => $point,
                'description' => $action
            ];
            $point_history_model::create($point_history);
            
            $response = [
                "error" => false,
                "code" => 200,
                "message" => "you have earned ".$point." point(s) from ".$action,
            ];
        }else{
            $response = [
                "error" => true,
                "code" => 200,
                "message" => "user not found",
            ];
        }

        return response($response, 200);
    }

    public function testapi(){
        echo 'it\'s work!';
    }
}
