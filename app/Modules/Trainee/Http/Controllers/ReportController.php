<?php

namespace App\Modules\Trainee\Http\Controllers;

use App\Modules\Auth\Models\EmployeeModel;
use App\Modules\Trainee\Models\users_point_history_model;
use App\User;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Modules\Trainee\Models\requestsGroupModel;
use App\Modules\Trainee\Models\requestsModel;
use App\Modules\Trainee\Models\RequestsUserModel;

use Sentinel;
use App\Helpers\UserRolesHelper;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        $role = new UserRolesHelper();
        $request_model = new requestsModel();

        if($id){
            $user = User::where(['username'=>$id])->get();
            if($user->isEmpty()){
                return redirect('404');
            }
            $user = $user->first();

            $data['nik'] = $id;
            session(['report_nik'=>$id]);
        } else{
            $user = Sentinel::check();
        }
        
        $data['trainee'] = requestsGroupModel::where(['user_id'=>$user->id])
            ->get(['form_group_id','created_at'])
            ->groupBy(function($d) {
                return [Carbon::parse($d->created_at)->format('D')];
            });

        $data['trainee'] = $data['trainee']->mapToGroups(function ($items,$key){
            return [$key=>$items->mapToGroups(function ($item){
                return [ucwords(str_replace('_',' ',requestsModel::groupById[$item['form_group_id']]))=>$item];
            })];
        });

        $data['trainee'] = $data['trainee']->toArray();

        $trainee = [];
        $trainee['labels'] = null;

        $trainee['type']['Training'] = 0;
        $trainee['type']['Assignment'] = 0;
        $trainee['type']['External Speaker'] = 0;
        $trainee['type']['Mentoring'] = 0;

        foreach ($data['trainee'] as $key=>$item) {
            if(!$trainee['labels']){
                $trainee['labels'] = [$key];
            } else {
                array_push($trainee['labels'],$key);
            }

            foreach ($data['trainee'][$key][0] as $form=>$value){
                $trainee['type']['Training'] = isset($trainee['type']['Training']) ? $trainee['type']['Training'] + ($form == 'Training' ? count($value) : 0) : $form == 'Training' ? count($value) : 0;
                $trainee['type']['Assignment'] = isset($trainee['type']['Assignment']) ? $trainee['type']['Assignment'] + ($form == 'Assignment' ? count($value) : 0) : $form == 'Assignment' ? count($value) : 0;
                $trainee['type']['External Speaker'] = isset($trainee['type']['External Speaker']) ? $trainee['type']['External Speaker'] + ($form == 'External Speaker' ? count($value) : 0) : $form == 'External Speaker' ? count($value) : 0;
                $trainee['type']['Mentoring'] = isset($trainee['type']['Mentoring']) ? $trainee['type']['Mentoring'] + ($form == 'Mentoring' ? count($value) : 0) : $form == 'Mentoring' ? count($value) : 0;
            }
        }
        $data['trainee'] = $trainee;

        $related = $this->related();
        $data['trainee']['type']['Inhouse'] = is_array($related['requests_group']) ? count($related['requests_group']) : 0;

        $data['report']['user_position']['as_trainee'] = RequestsUserModel::where(['user_id'=>$user->id,'user_position'=>'Employee'])->count();
        $data['report']['user_position']['as_trainer'] = RequestsUserModel::where(['user_id'=>$user->id,'user_position'=>'Trainer'])->count();
        $data['report']['user_position']['as_internal_mentor'] = $data['trainee']['type']['Inhouse'];
        $data['report']['user_position']['as_external_speaker'] = RequestsUserModel::where(['user_id'=>$user->id,'user_position'=>'Mentor'])->count();

        $data['role'] = $role;
        $data['requests'] = $request_model->getNotificationRequest([$user->id], $user->id, $user->id);

        if ($role->inStaff()) {
            return view('trainee::report.index',$data);
        } elseif ($role->inAdmin() || $role->inManager()){
            $data['employee'] = EmployeeModel::where(['nik'=>$data['nik']])->get(['full_name','division','position','employee_status'])->first()->toArray();
            return view('trainee::report.admin',$data);
        } else {
            return redirect('404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getTrainee($get,$id = null) {

        if($id) {
            $user = User::where(['username'=>$id])->get()->first();
        } else {
            $user = Sentinel::check();
        }

        $employee_data = RequestsUserModel::
        where(['requests_user.user_id'=>$user->id,'requests_user.user_position'=>'Trainer'])

            ->join('requests_user as ru2',function ($join) use ($user){
                $join->on('requests_user.requests_group_id','=','ru2.requests_group_id')
                    ->whereNotIn('ru2.user_id',[$user->id]);
            })
            ->join('requests_group as rg',function ($join) {
                $join
                    ->on('rg.id','=','ru2.requests_group_id');
            })
            ->join('users',function($join) {
                $join->on('users.id','=','ru2.user_id');
            })
            ->join('employee',function ($join){
                $join->on('employee.nik','=','users.username');
            })

            ->get($get);

        return $employee_data;
    }


    public function getTraineeUser($username=null) {

        if(session('report_nik')){
            $username = session('report_nik');
        }

        $employee_data = $this->getTrainee(
            [
                'rg.id','rg.user_id',
                'ru2.user_position',
                'employee.full_name',
                DB::raw('CASE WHEN rg.form_group_id = 5 THEN "Inhouse" ELSE "Public Training" END as form_group_id')
            ],$username
        );

        $employee_data = DataTables::of($employee_data)
            ->addColumn('options',function ($employee_data){
                return '
            <a href="'.route('trainee.previewByUserId',['user_id' => $employee_data['user_id'],'id'=>$employee_data['id']]).'" target="_blank">        
            <button type="button" class="btn btn-default btn-sm">
                <i class="icon-edit-2"></i> Preview
            </button></a>';
            })
            ->rawColumns(['options'])
            ->make();

        return $employee_data;
    }

    public static function recentTrainee() {
        $user = Sentinel::check();

        if (!$user){
            return null;
        }

        $trainee = requestsGroupModel::
        where(['requests_group.user_id'=>$user->id])
            ->limit(5)
            ->leftJoin('requests as r',function($join) {
                $join
                    ->on('r.requests_group_id','=','requests_group.id')
                    ->where('r.form_answer','not like','mentoring_ci_start_date')
                    ->where('r.form_answer','like','%start_date%');
            })
            ->get([
                'requests_group.id',
                'requests_group.user_id',
                'r.request_content',
                DB::raw('CASE 
                    WHEN requests_group.form_group_id = 5 THEN "Inhouse" 
                    WHEN requests_group.form_group_id = 1 THEN "Assignment" 
                    WHEN requests_group.form_group_id = 2 THEN "Mentoring / Coaching" 
                    WHEN requests_group.form_group_id = 3 THEN "Public Training" 
                    WHEN requests_group.form_group_id = 4 THEN "External Speaker" 
                    ELSE "" 
                    END as form_group_id')
            ])
            ->toArray();
        return $trainee;
    }

    private function related() {
        $role = new UserRolesHelper();

        if ($role->inAdmin()){
            return false;
        }

        $user_id = Sentinel::check()->id;

        $related = RequestsUserModel::where(['user_id'=>$user_id])->get(['user_id','requests_group_id','user_position']);
        $data['requests_group'] = [];
        if ($related->isNotEmpty()) {
            foreach (collect($related)->sortKeysDesc()->toArray() as $key=>$item) {
                $data['position'][$item['requests_group_id']] = $item['user_position'];
                array_push($data['requests_group'],$this->getRequests($item['requests_group_id']));
            }
        } else {
            $related = null;
        }
        $data['form_type'] = requestsModel::groupById;
        return $data;
    }
    private function getRequests($group_id) {
        $model = new requestsModel();

        $data['requests'] = [];

        $requestGroup = requestsGroupModel::where(['id'=>$group_id])->get();

        if($requestGroup->isEmpty()) {
            return null;
        } else {
            $requestGroup=$requestGroup->toArray()[0];
        }

        $data['requests'][$requestGroup['user_id']] = $model->getRequestByFormId($group_id,null,$requestGroup['user_id']);

        if(!empty($data['requests'])){
            foreach ($data['requests'] as $key=>$request){
                $data['requests'][$key] = $model->groupRequest($request,true);
            }

            $data['requests'] = array_filter($data['requests']);
        }
        $data['form_type'] = requestsModel::groupById;
        return $data;
    }

    public function pointHistory($username = null){
        if(session('report_nik')){
            $username = session('report_nik');
        }

        if(!$username){
            $user = Sentinel::check();
        } else {
            $user = User::where(['username'=>$username])->get()->first();
        }
        $point_history = users_point_history_model::where(['user_id'=>$user->id])->get();

        $return = DataTables::of($point_history)
            ->addColumn('created',function ($point){
                return Carbon::parse($point['created_at'])->format('d/M/Y');
            })

            ->addColumn('form_group',function ($point){
                return !empty($point->form_group_id) ? requestsModel::groupById[$point->form_group_id] : $point->description;
            })
            ->make();
        return $return;
    }
}
