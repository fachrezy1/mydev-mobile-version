<?php

namespace App\Modules\Trainee\Http\Controllers;

use App\Helpers\UserRolesHelper;
use App\Modules\Auth\Models\EmployeeModel;
use App\Modules\Trainee\Models\reviewModel;
use Illuminate\Http\Request;
use App\Modules\Trainee\Models\requestsModel;
use App\Modules\Trainee\Models\RequestsUserModel;
use Illuminate\Pagination\Paginator;
use App\Modules\Trainee\Models\requestsGroupModel;
use App\Modules\Trainee\Models\traineeModel;

//add
use App\Modules\Trainee\Models\ratingModel;
use App\Modules\Auth\Models\UserModel;
use DataTables;
use App\User;
use DB;
//end add

use App\Http\Controllers\Controller;

use Excel;
use App\Exports\DefaultExport;

use Sentinel;
class MyTraineeController extends Controller
{
    public function view() {
    
        $role = new UserRolesHelper();
        $user = Sentinel::check();

        if (!$user->hasAccess('trainee.manage')) {
            if (!$user->hasAccess('admin.createTrainee')) {
                return back();
            }
        }

        $model = new requestsModel();

        //$data['requests'] = $model->groupRequest($model->getGroupedRequest($user->id));

        $atasan = '-';
        $approvedCount = 0;
        
        /*
        if ($data['requests']):
            $approvedCount = count($data['requests']['approved']);
        endif;
        $data['requests']['approvedCount'] = $approvedCount;
        */
        
        $trainees = new traineeModel();
        $trainees= $trainees->get(['id','name','label'])->toArray();
        foreach ($trainees as $key => $value) {
            $data['form_type'][$value['id']] = $value['label']; 
        }

        $data['form_type2']  = requestsModel::groupById;

        $related = $this->related();

        $data['related_group'] = $related;
        
        $data['full_name'] = '';
        $data['requests'] = null;
        //dd($data['related_group']);
        
        /*
        if($data['related_group']) {
            foreach ($data['related_group']['requests_group'] as $keys => $items){
                foreach ($items as $key=>$item){
                    if(!empty($item['approved']) && (isset($item['approved'][key($item['approved'])]['form_group_id']) && $item['approved'][key($item['approved'])]['form_group_id'] == 5)) {
                        if(!isset($data['requests']['completed'])){
                            $data['requests']['completed'] = [];
                        }
                        unset($data['related_group']['requests_group'][$keys]);
                        $data['requests']['completed'][key($item['approved'])] = $item['approved'][key($item['approved'])];
                    }

                    if(!empty($item['completed']) && (isset($item['approved'][key($item['approved'])]['form_group_id']) && $item['approved'][key($item['approved'])]['form_group_id'] == 5)) {
                        if(!isset($data['requests']['completed'])){
                            $data['requests']['approved'] = [];
                        }
                        unset($data['related_group']['requests_group'][$keys]);
                        $data['requests']['completed'][key($item['completed'])] = $item['completed'][key($item['completed'])];
                    }
                }
            }
        }
        */
        if($role->inAdmin()) {
            $data['full_name'] = 'Administrator';
            $data['line_manager'] = $atasan;
            return view('admindashboard::traineeList',$data);

        } else {
            $data['full_name'] = EmployeeModel::where(['nik'=>$user->username])->get(['full_name'])->first()->full_name;
            $data['line_manager'] = $atasan;
            return view('trainee::traineeList',$data);
        }
    }

    public function myAchievement(Request $page) {

        $page = $page->page;

        if (!$page){
            $page = 1;
        }

        $role = new UserRolesHelper();
        if (!$role->inStaff()) {
            return redirect('404');
        }
        $per_page = 5;

        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });

        $user = Sentinel::check();
        $request = new requestsModel();
        $data['requests'] = $request->groupRequest($request->getGroupedRequest($user->id));
        $review = reviewModel::where('user_id',$user->id)->get()->groupBy('request_group_id')->toArray();
        if($data['requests']['completed'] ){
            foreach ($data['requests']['completed'] as $keys=>$items) {
                if (is_array($items)) {
                    foreach ($items as $key=>$item) {
                        if (is_array($item)) {
                            foreach ($item as $keyV=>$value){
                                $data['requests']['completed'][$keys][$key][$keyV]['reviews'] = isset($review[$keyV]) ? $review[$keyV] : null;
                            }
                        }
                    }
                }
            }
        }
        
        return view('trainee::myAchievement',$data);
    }

    private function related() {
        $role = new UserRolesHelper();

        if ($role->inAdmin()){
            return false;
        }

        $user_id = Sentinel::check()->id;

        $related = RequestsUserModel::where(['user_id'=>$user_id])->get(['user_id','requests_group_id','user_position']);
        $data['requests_group'] = [];
        //dd($related);
        if ($related->isNotEmpty()) {
            foreach (collect($related)->sortKeysDesc()->toArray() as $key=>$item) {
                $data['position'][$item['requests_group_id']] = $item['user_position'];
                array_push($data['requests_group'],$this->getRequests($item['requests_group_id'],$item['user_id'])['requests']);
            }
        } else {
            $related = null;
        }
        $data['form_type'] = requestsModel::groupById;
        return $data;
    
    }

    private function getRequests($group_id,$user_id) {
        $role = new UserRolesHelper();
        $model = new requestsModel();

        $data['requests'] = [];

        $requestGroup = requestsGroupModel::where(['id'=>$group_id])->get();

        if($requestGroup->isEmpty()) {
            return null;
        } else {
            $requestGroup=$requestGroup->toArray()[0];
        }

        $data['requests'][$requestGroup['user_id']] = $model->getRequestByFormId($group_id,null,$requestGroup['user_id']);

        if(!empty($data['requests'])){
            foreach ($data['requests'] as $key=>$request){
                $data['requests'][$key] = $model->groupRequest($request,true);
            }

            $data['requests'] = array_filter($data['requests']);
        }
        $data['form_type'] = requestsModel::groupById;
        return $data;
    }


    public function getDatatables($type) {

        $role = new UserRolesHelper();

        if($role->inAdmin()) {
            if($type == 'new') {
                $data = requestsGroupModel::where(['is_accepted'=>'1'])
                    ->join('requests','requests.requests_group_id','=','requests_group.id')
                    ->select('requests.*','requests_group.*')
                    ->get()
                    ->groupBy('requests_group_id')[2][0];

            } else {
                $data = requestsGroupModel::where(['is_completed'=>'1']);
            }

        }


        dump($type);

    }

//new ---------------------------------
//new ---------------------------------
//new ---------------------------------

    // ini buat yang request inhouse Admin

    private function getTraineRequestInhouse($type, $forData=null){
        $role = new UserRolesHelper();
        //$staff_ids = $this->getStaffIds();
        $user = Sentinel::check();
        
        $trainee_request = requestsGroupModel::where("form_group_id",5)->where('user_id',5);

        if (request("search.value") && strlen(request("search.value"))>0){
            $trainee_request = $trainee_request->where(function($q) {
                $q->whereHas('user', function ($query) {
                    $query->where("first_name","LIKE",'%'.request("search.value").'%');
                })->orWhereHas('trainee', function ($query) {
                    $query->where("name","LIKE",'%'.request("search.value").'%');
                })->orWhereHas('request_data', function ($query) {
                    $query->where("request_content","LIKE",'%'.request("search.value").'%');
                });
            });
        }
        
        switch($type){
            case 'ongoing' :
                $trainee_request = $trainee_request->where('is_completed_by_admin', 0)->whereNotNull('published_at')
                ;
            break;
            case 'completed' :
                $trainee_request = $trainee_request->where('is_completed_by_admin', 1)->whereNotNull('published_at')
                ;
            break;
        }

        $trainee_request = $trainee_request->orderBy("id","desc")->with(["request_data","trainee","user"]);
        if ($forData=="excel"){
            return $trainee_request->get();
        }
        return $trainee_request;
    }

    public function DataTableTraineeInhouse($type){
        $role = new UserRolesHelper();
        
      

        $trainee_request = $this->getTraineRequestInhouse($type);

        //dd($trainee_request);
        //$trainee_request = array('title' => 'a', 'type' => 'b', 'employee_name' => 'c', 'start' => 'D', 'End');
        return Datatables::of($trainee_request)
            ->addIndexColumn()
            ->addColumn('title', function($trainee_request) {
                
                $request_data = $trainee_request->request_data()->where('form_answer','training_ti_title')->first();

                if($request_data){
                    $title = $request_data->request_content;
                }else{
                    $title = '-';
                }

                return $title;
            })->addColumn('type', function($trainee_request) {
                
                if (isset($trainee_request->trainee->name)){
                    $type = $trainee_request->trainee->name;
                } else {
                    $type = "-";
                }
                return str_replace("&", "/", $type);
                //return $type;
            
            })->addColumn('employee_name', function($trainee_request) {
                
                if (isset($trainee_request->user->first_name)){
                    $full_name = $trainee_request->user->first_name;
                } else {
                    $full_name = "-";
                }
                //$full_name = 'ini full name';
                return $full_name;
            
            })->addColumn('start', function($trainee_request) {
            
                $date = $trainee_request->request_data()->where('form_answer','training_ti_start_date')->first();

                if($date){
                    $date = $date->request_content;
                }else{
                    $date = '-';
                }

                return $date;
            })->addColumn('end', function($trainee_request) {
            
                $date = $trainee_request->request_data()->where('form_answer','training_ti_end_date')->first();
                
                if ($date){
                
                    $date = $date->request_content;
                
                } else {
                
                    $date = "-";
                
                }
                return $date;
            })->addColumn('option', function($trainee_request) {
                $train = traineeModel::where('id',$trainee_request->form_group_id)->first();
                //dd($train);

                $option = '<a href="'.route('trainee.export',['user_id' => $trainee_request->user_id,'id'=> $trainee_request->id]).'" target="_blank"><button type="button" class="btn btn-default btn-sm btn-lite"><i class="icon-transfer-data-between-documents"></i> Export</button></a>';
                $option .= '<a id="preview'.$trainee_request->id.'" href="'.route('trainee.previewByUserId',['user_id' => $trainee_request->user_id,'id'=> $trainee_request->id]).'"><button type="button" class="btn btn-default btn-sm btn-lite"><i class="icon-contract-1"></i> Preview</button></a>';

                if ($trainee_request->is_completed_by_admin != 1) {
                    $option .= '<a href="'.route('dashboard.trainee.edit.show',['type'=>strtolower($train->name),'user_id' => $trainee_request->user_id,'id'=> $trainee_request->id]).'"><button type="button" class="btn btn-default btn-sm btn-lite"><i class="icon-edit-1"></i> Edit</button></a>';
                }
                return $option;
            })->rawColumns(['option'])
            ->make(true);
    }

    // End ini buat yang request inhouse Admin


    // my trainee
    private function getTraineRequest3($type, $forData=null){
        $user = Sentinel::check();

        $query = 'SELECT requests_group.* from requests_group
            join requests_user on requests_group.id = requests_user.requests_group_id
            join trainees on requests_group.form_group_id = trainees.id
            join requests on requests_group.id = requests.requests_group_id
            join users on requests_group.user_id = users.id            
            where ((requests_group.user_id= 5 
            AND requests_user.user_id = '.$user->id.' 
            AND requests_group.form_group_id = 5 
            AND (requests_user.user_position like "%mentor%" OR requests_user.user_position like "%Trainer%" OR requests_user.user_position like "%Moderator%"))
            OR
            (requests_user.user_id='.$user->id.'
            AND requests_group.form_group_id in (1,2,3,4,5,6)
            AND (requests_user.user_position like "%mentor%" OR requests_user.user_position like "%Trainer%" OR requests_user.user_position like "%Moderator%")
            AND first_accepter is not null AND second_accepter is not null)) 
            AND published_at is not null ';

        
        if (request("search.value") && strlen(request("search.value"))>0){
            $query .= 'AND (';
            $query .= ' users.first_name LIKE "%'.request('search.value').'%"';
            $query .= ' OR trainees.name LIKE "%'.request('search.value').'%"'; 
            $query .= ' OR requests.request_content LIKE "%'.request('search.value').'%"';
            $query .= ') ';
        }

        if (request("year") && strlen(request("year"))>0 && request("month") && strlen(request("month"))>0){
            $year = explode('-',request('year'));
            $month = explode('-',request('month'));

            if (count($year)==2 && count($month)==2){

                $date_filter = $month[1].'/'.$year[1];
                $query .= 'AND (';
                $query .= ' requests.request_content LIKE "%'.$date_filter.'%"';
                $query .= ') ';
                
                /*
                $trainee_request = $trainee_request->where(function($q) use ($date_filter) {
                    $q->whereHas('request_data', function ($query) use ($date_filter) {
                        $query->where("request_content","LIKE",'%'.$date_filter.'%');
                    });
                });
                */
            }
        }
        
        $query .= ' GROUP BY requests_group.id DESC';

        $trainee_request = DB::select($query);
        return $trainee_request;
    }

    public function datatableTraineeRequest3($type){
        $role = new UserRolesHelper();
        $trainee_request = $this->getTraineRequest3($type);

        return Datatables::of($trainee_request)
            ->addIndexColumn()
            ->addColumn('title', function($trainee_request) {
                //$title = $trainee_request->user_id;
                switch($trainee_request->form_group_id){
                    case 1:
                        $request_q = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','assignment_pg_project_title')->first();

                        if($request_q){
                            $title = $request_q->request_content;
                        }else{
                            $title = '-';
                        }
                    break;

                    case 2:
                        $request_q = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','coaching_cc_development_goal')->first();

                        if($request_q){
                            $title = $request_q->request_content;
                        }else{
                            $title = '-';
                        }
                    break;

                    case 3:
                        $request_q = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','training_ti_title')->first();
                        
                        if($request_q){
                            $title = $request_q->request_content;
                        }else{
                            $title = '-';
                        }
                    break;

                    case 4:
                        $request_q = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','mentoring_ci_line_topic')->first();

                        if($request_q){
                            $title = $request_q->request_content;
                        }else{
                            $title = '-';
                        }
                    break;
                    
                    case 5:
                        $request_q = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','training_ti_title')->first();

                        if($request_q){
                            $title = $request_q->request_content;
                        }else{
                            $title = '-';
                        }
                    break;

                    default :
                        $title = '-';
                    break;
                }
                return $title;
            })
            ->addColumn('type', function($trainee_request) {
                $trainee_name = traineeModel::where('id',$trainee_request->form_group_id)->first();

                if (isset($trainee_name->name)) {
                    
                    $type = $trainee_name->name;
                
                }else{
                    $type = '-';
                }
                
                return str_replace("&", "/", $type);
            })
            ->addColumn('employee_name', function($trainee_request) {

                $user_name = UserModel::where('id',$trainee_request->user_id)->first();
                
                if (isset($user_name->first_name)){
                    $full_name = $user_name->first_name;
                }else{
                    $full_name = '-';
                }
                
                return $full_name;
            })

            ->addColumn('atasan', function($trainee_request){
               $role = new UserRolesHelper();
               //request_grup id
               $id_train2 =  $trainee_request->id;

               //user id dari request id
               $id_us =  $trainee_request->user_id; 

               $arr_line = $role->getLeaders((int)$id_us);
               
               $user_typ = NULL;
               
               if (isset($arr_line['user_type'])) {
                   $user_typ = $arr_line['user_type'];
               }else{
                   $user_typ = '';
               }
               $atasan = '';
               
               //$managerid = '';
               //$email = '';
               $name_manager = '-';
               
               $users_id = UserModel::where('id',$trainee_request->user_id)->first();
               
               if (isset($users_id->email)) {
                    $email = $users_id->email;
               }else{
                    $email = '';
               }

               if ($arr_line['user_type'] != NULL) {
                    if ( $user_typ == 'staff' || $user_typ == 'sroff' || $user_typ == 'off') {
                        //mengambil avp
                        if (array_key_exists('avp', $arr_line)) {
                            if (is_array($arr_line['avp']) && array_key_exists('name', $arr_line['avp'])) {
                                $atasan = $arr_line['avp']['name'];
                            }else{
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }else{
                            //mengambil vp
                            if (array_key_exists('vp', $arr_line)) {
                                if (is_array($arr_line['vp']) && array_key_exists('name', $arr_line['vp'])) {
                                    $atasan = $arr_line['vp']['name'];
                                }else{
                                    $atasan = '-';
                                    $manag = $role->manag_id($email);
                                    $atasan = $manag['manager'];
                                }
                            }else{
                                //mengambil svp
                                if (array_key_exists('svp', $arr_line)) {
                                    if (is_array($arr_line['svp']) && array_key_exists('name', $arr_line['svp'])) {
                                        $atasan = $arr_line['svp']['name'];
                                    }else{
                                        $atasan = '-';
                                        $manag = $role->manag_id($email);
                                        $atasan = $manag['manager'];
                                    }
                                }else{
                                    //end else
                                    $atasan = '-';
                                    $manag = $role->manag_id($email);
                                    $atasan = $manag['manager'];
                                }
                            }
                        }
                    }elseif ( $user_typ == 'avp') {
                        if (array_key_exists('vp', $arr_line)) {
                            if (is_array($arr_line['vp']) && array_key_exists('name', $arr_line['vp'])) {
                                $atasan = $arr_line['vp']['name'];
                            }else{
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }else{
                            if (array_key_exists('svp', $arr_line)) {
                                if (is_array($arr_line['svp']) && array_key_exists('name', $arr_line['svp'])) {
                                    $atasan = $arr_line['svp']['name'];
                                }else{
                                    $atasan = '-';
                                }
                            }else{
                                //end else
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }
                    }elseif ( $user_typ == 'vp') {
                        if (array_key_exists('svp', $arr_line)) {
                            if (is_array($arr_line['svp']) && array_key_exists('name', $arr_line['svp'])) {
                                $atasan = $arr_line['svp']['name'];
                            }else{
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }else{
                            if (array_key_exists('dir', $arr_line)) {
                                if ( is_array($arr_line['dir']) && array_key_exists('name', $arr_line['dir'])) {
                                    $atasan = $arr_line['dir']['name'];
                                }else{
                                    $atasan = '-';
                                    $manag = $role->manag_id($email);
                                    $atasan = $manag['manager'];
                                }
                            }else{
                                //end else
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }
                    }elseif ($user_typ == 'svp') {
                        //ambil dir
                        if (array_key_exists('dir', $arr_line)) {
                            if (is_array($arr_line['dir']) && array_key_exists('name', $arr_line['dir'])) {
                                $atasan = $arr_line['dir']['name'];
                            }else{
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }else{
                            //end else
                            $atasan = '-';
                            $manag = $role->manag_id($email);
                            $atasan = $manag['manager'];
                        }
                    }else{
                    //isi sesuai manager_id
                    $manag = $role->manag_id($email);
                    $atasan = $manag['manager'];
                    }
               }else{
                    //kosong di bagian user type
                    $atasan = '-';
               }

                if ($atasan == '' || $atasan == NULL) {
                   $atasan = 'Nama belum diinput di database';
                }
                
               return $atasan;

            })

            ->addColumn('start', function($trainee_request) {
                switch($trainee_request->form_group_id){
                    case 1:
                        $date = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','assignment_pg_start_date')->first();

                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 2:
                        $date = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','coaching_ci_start_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 3:
                        $date = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','training_ti_start_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 4:
                        $date = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','mentoring_ci_start_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;
                    
                    case 5:
                        $date = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','training_ti_start_date')->first();

                        if($date){
                            $date = $date->request_content;
                        }else{
                            $date = '-';
                        }
                    break;

                    default :
                        $date = '-';
                    break;
                }

                return $date;
            })
            ->addColumn('end', function($trainee_request) {
                switch($trainee_request->form_group_id){
                    case 1:
                        $date = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','assignment_pg_end_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 2:
                        $date = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','coaching_ci_end_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 3:
                        $date = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','training_ti_end_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 4:
                        $date = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','mentoring_ci_end_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;
                    
                    case 5:
                        $date = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','training_ti_end_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    default :
                        $date = '-';
                    break;
                }

                return $date;
            })
            ->addColumn('manager', function($trainee_request) {
                if (isset($trainee_request->first_accepter)) {
                    $status = '<span class="badge badge-pill badge-success">Approved</span>';
                }else{
                    $status = '<span class="badge badge-pill badge-info">waiting Approval</span>';
                }
                /*
                $status = '<span class="badge badge-pill '.(!empty($trainee_request->first_accepter) ? 'badge-success' : 'badge-info').'">'.(!empty($trainee_request->first_accepter) ? 'Approved' : 'Waiting Approval').'</span>';
                */
                return $status;
            })
            ->addColumn('hr', function($trainee_request) {
                if (isset($trainee_request->second_accepter)) {
                    $status = '<span class="badge badge-pill badge-success">Approved</span>';
                }else{
                    $status = '<span class="badge badge-pill badge-info">waiting Approval</span>';
                }
                /*
                    $status = '<span class="badge badge-pill '.(!empty($trainee_request->second_accepter) ? 'badge-success' : 'badge-info').'">'.(!empty($trainee_request->second_accepter) ? 'Approved' : 'Waiting Approval').'</span>';
                */
                return $status;
            })
            ->addColumn('type_prog', function($trainee_request) {
                switch($trainee_request->form_group_id){
                    case 1:
                        $request_data = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','assignment_dev_type')->first();

                        if($request_data){
                            if ($request_data->request_content == 'internship') {
                                $type_prog = 'Cross Functional assignment';
                            }elseif($request_data->request_content == 'globaltalent'){
                                $type_prog = 'Global Talent Mobility / National Talent Mobility';
                            }else{
                                $type_prog = 'project';
                            }
                        }else{
                            $type_prog = '-';
                        }
                        /*
                        if($request_data){
                            $type_prog = $request_data->request_content;
                        }else{
                            $type_prog = '-';
                        }
                        */
                    break;

                    case 2:
                        $request_data = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','coaching_ci_type')->first();

                        if($request_data){
                            $type_prog = $request_data->request_content;
                        }else{
                            $type_prog = '-';
                        }
                    break;

                    case 3:
                        $request_data = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','training_ti_type_programs')->first();
                        if($request_data){
                            $type_prog = $request_data->request_content;
                        }else{
                            $type_prog = '-';
                        }
                    break;

                    case 4:
                        $type_prog = 'External Speaker';
                    break;
                    
                    case 5:
                        $request_data = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','training_ti_type_programs')->first();
                        if($request_data){
                            $type_prog = $request_data->request_content;
                        }else{
                            $type_prog = '-';
                        }
                    break;

                    default :
                        $type_prog = '-';
                    break;
                }
                return $type_prog;
            })
            ->addColumn('option', function($trainee_request) use ($role) {
                $rated_by_staff = ratingModel::where(['request_group_id'=> $trainee_request->id, 'staff_id'=>Sentinel::check()->id])->get(['id'])->first() ? true : false;

                $rating_count = ratingModel::where('request_group_id','=',$trainee_request->id)->count();

                if ($role->inAdmin()){
                    $accepted = User::select("id")->where("id",$trainee_request->second_accepter)
                        ->orWhere("id",$trainee_request->rejected_by)->first();
                } else {
                    $accepted = User::select("id")
                    ->where("id",$trainee_request->first_accepter)
                    ->orWhere("id",$trainee_request->rejected_by)->first();
                }

                $disabled = "";
                if ($accepted){ 
                    $disabled = "disabled";
                }

                $option = '<td>';
                $user = \Sentinel::check();
                $rating_count = ratingModel::where('request_group_id','=',$trainee_request->id)->count();

                $option .= '<a id="preview'.$trainee_request->id.'" href="'.route("trainee.previewByUserId",["user_id" => $trainee_request->user_id,"id"=>$trainee_request->id]).'">
                    <button type="button" class="btn btn-default btn-sm btn-lite">
                        <i class="icon-contract-1"></i> Preview
                    </button>
                    <div class="hidden">'.$trainee_request->id.'</div>
                </a>';

                if ($trainee_request->form_group_id == 2) {
                    //export report
                    $option .= '<a href="'.route("trainee.export2",["user_id" => $trainee_request->user_id,"id"=>$trainee_request->id]).'" target="_blank" >
                    <button type="button" class="btn btn-default btn-sm btn-lite">
                        <i class="icon-transfer-data-between-documents"></i> Export
                    </button>
                    </a>';
                }else{
                    $option .= '<a href="'.route("trainee.export",["user_id" => $trainee_request->user_id,"id"=>$trainee_request->id]).'" target="_blank" >
                    <button type="button" class="btn btn-default btn-sm btn-lite">
                        <i class="icon-transfer-data-between-documents"></i> Export
                    </button>
                    </a>';
                }

                if($trainee_request->form_group_id == 2 && $trainee_request->is_completed_by_manager != 1){
                    $option .='
                    <a href="'.route('trainee.reviewByUserId',["user_id" => $trainee_request->user_id,"id"=>$trainee_request->id]).'">
                        <button type="button" class="btn btn-default btn-sm btn-lite">
                            <i class="icon-edit-1"></i> Result
                        </button>
                    </a>';
                }

                if($trainee_request->form_group_id == 5 && !$rated_by_staff && $role->inStaff()){
                     $check=RequestsUserModel::where('requests_group_id',$trainee_request->id)->where('user_id',$user->id)->get();
                     
                     if(count($check) > 0){
                     foreach($check as $un){

                         $user_positi=$un->user_position;
                     }

                    if($user_positi and  $user_positi=="Employee"){

                    $option .= '<button type="button" class="btn btn-default btn-sm btn-lite rating-modal"
                        data-requestgroup="'.$trainee_request->id.'" data-staffid="'.$user->id.'" data-mentorid="5" data-toggle="modal" data-target="#assignment">
                    <i class="icon-verified"></i> Rate
                    </button>';
                    }else{


                          $option .= '<button type="button" class="btn btn-default btn-sm btn-lite rating-modal"
                        data-requestgroup="'.$trainee_request->id.'" data-staffid="'.$user->id.'" data-mentorid="5" data-toggle="modal" data-target="#assignment">
                    <i class="icon-verified"></i> Done
                    </button>';
                    }

                   }
                }
                //endadd
                $option .='</td>';

                return $option;
            })
            ->rawColumns(['option', 'manager', 'hr'])
            ->make(true);
    }
    // end my trainee


    private function getTraineRequest($type, $forData=null){
        $role = new UserRolesHelper();
        //$staff_ids = $this->getStaffIds();
        $user = Sentinel::check();
        
        $trainee_request = requestsGroupModel::whereIn("form_group_id",[1,2,3,4,5,6])->where('user_id',$user->id
        );
        
        if (request("search.value") && strlen(request("search.value"))>0){
            $trainee_request = $trainee_request->where(function($q) {
                $q->whereHas('user', function ($query) {
                    $query->where("first_name","LIKE",'%'.request("search.value").'%');
                })->orWhereHas('trainee', function ($query) {
                    $query->where("name","LIKE",'%'.request("search.value").'%');
                })->orWhereHas('request_data', function ($query) {
                    $query->where("request_content","LIKE",'%'.request("search.value").'%');
                });
            });
        }

        if (request("year") && strlen(request("year"))>0 && request("month") && strlen(request("month"))>0){
            $year = explode('-',request('year'));
            $month = explode('-',request('month'));

            if (count($year)==2 && count($month)==2){

                $date_filter = $month[1].'/'.$year[1];
                $trainee_request = $trainee_request->where(function($q) use ($date_filter) {
                        $q->whereHas('request_data', function ($query) use ($date_filter) {
                            $query->where("request_content","LIKE",'%'.$date_filter.'%');
                        });
                    });
            }
        }

        switch($type){
            case 'waiting' :
                $trainee_request = $trainee_request->where([
                'is_completed'=>0, 
                'is_rejected' => 0
            ])
            ->where(function($query) {
                    $query->whereNull('first_accepter')
                        ->orWhereNull('second_accepter');
            })->whereNotNull('published_at')
            ->where('is_accepted', 0);
            break;
            case 'ongoing' :
                $trainee_request = $trainee_request->where('is_completed_by_staff', 0)
            ->where('is_completed', 0)
            ->whereNotNull('first_accepter')
            ->whereNotNull('second_accepter');
            break;
            case 'completed' :
                
                $trainee_request = $trainee_request->where('is_completed','!=',0)
                ->where(['is_completed_by_staff'=>1]);
            break;
            
            case 'rejected' :
                $trainee_request = $trainee_request->where(['is_rejected'=>1])
                ->where('is_rejected', 1)
                ->where('rejected_by',"!=", NULL);
            
            break;
        }

        $trainee_request = $trainee_request->orderBy("id","desc")->with(["request_data","trainee","user"]);
        if ($forData=="excel"){
            return $trainee_request->get();
        }
        return $trainee_request;
    }

    private function getTraineRequest2($type, $forData=null){
        $user = Sentinel::check();

        $query = 'SELECT requests_group.* from requests_group
            LEFT JOIN requests_user on requests_group.id = requests_user.requests_group_id
            join trainees on requests_group.form_group_id = trainees.id
            join requests on requests_group.id = requests.requests_group_id
            join users on requests_group.user_id = users.id            
            where ((requests_group.user_id= 5 
            AND requests_user.user_id = '.$user->id.' 
            AND requests_group.form_group_id = 5 
            AND (requests_group.is_completed = 0 or requests_group.is_completed != 0))
            AND  (requests_user.user_position NOT IN("Trainer","Moderator","mentor"))
            OR
            (requests_group.user_id='.$user->id.'
            AND requests_group.form_group_id in (1,2,3,4,5,6)
        
            AND requests_group.is_completed != 0
            AND requests_group.is_completed_by_staff = 1)

            OR
            (requests_group.user_id='.$user->id.'
           
            AND requests_group.form_group_id = 4
            AND requests_group.is_completed != 0))
            AND published_at is not null ';

        
        if (request("search.value") && strlen(request("search.value"))>0){
            $query .= 'AND (';
            $query .= ' users.first_name LIKE "%'.request('search.value').'%"';
            $query .= ' OR trainees.name LIKE "%'.request('search.value').'%"'; 
            $query .= ' OR requests.request_content LIKE "%'.request('search.value').'%"';
            $query .= ')';
        }

        if (request("year") && strlen(request("year"))>0 && request("month") && strlen(request("month"))>0){
            $year = explode('-',request('year'));
            $month = explode('-',request('month'));

            if (count($year)==2 && count($month)==2){

                $date_filter = $month[1].'/'.$year[1];
                $query .= ' AND (requests.request_content LIKE "%'.$date_filter.'%")';

            }
        }
        
        $query .= ' GROUP BY requests_group.id DESC';

        $trainee_request = DB::select($query);
        return $trainee_request;
    }

    public function datatableTraineeRequest($type){
        $role = new UserRolesHelper();
        $trainee_request = $this->getTraineRequest($type);

        return Datatables::of($trainee_request)
            ->addIndexColumn()
            ->addColumn('title', function($trainee_request) {
                switch($trainee_request->form_group_id){
                    case 1:
                        $request_data = $trainee_request->request_data()->where('form_answer','assignment_pg_project_title')->first();

                        if($request_data){
                            $title = $request_data->request_content;
                        }else{
                            $title = '-';
                        }
                    break;

                    case 2:
                        $request_data = $trainee_request->request_data()->where('form_answer','coaching_cc_development_goal')->first();

                        if($request_data){
                            $title = $request_data->request_content;
                        }else{
                            $title = '-';
                        }
                    break;

                    case 3:
                        $request_data = $trainee_request->request_data()->where('form_answer','training_ti_title')->first();
                        if($request_data){
                            $title = $request_data->request_content;
                        }else{
                            $title = '-';
                        }
                    break;

                    case 4:
                        $request_data = $trainee_request->request_data()->where('form_answer','mentoring_ci_line_topic')->first();

                        if($request_data){
                            $title = $request_data->request_content;
                        }else{
                            $title = '-';
                        }
                    break;
                    
                    case 5:
                        $request_data = $trainee_request->request_data()->where('form_answer','training_ti_title')->first();

                        if($request_data){
                            $title = $request_data->request_content;
                        }else{
                            $title = '-';
                        }
                    break;

                    default :
                        $title = '-';
                    break;
                }

                return $title;
            })
            ->addColumn('type', function($trainee_request) {
                if (isset($trainee_request->trainee->name)){
                    $type = $trainee_request->trainee->name;
                } else {
                    $type = "-";
                }
                return str_replace("&", "/", $type);
            })
            ->addColumn('employee_name', function($trainee_request) {
                
                if (isset($trainee_request->user->first_name)){
                    $full_name = $trainee_request->user->first_name;
                } else {
                    $full_name = "-";
                }

                return $full_name;
            })

            ->addColumn('atasan', function($trainee_request){
               $role = new UserRolesHelper();
               //request_grup id
               $id_train2 =  $trainee_request->id;

               //user id dari request id
               $id_us =  $trainee_request->user->id; 

               $arr_line = $role->getLeaders((int)$id_us);
               
               $user_typ = NULL;
               
               if (isset($arr_line['user_type'])) {
                   $user_typ = $arr_line['user_type'];
               }else{
                   $user_typ = '';
               }
               $atasan = '';
               
               $name_manager = '-';
               
               
               if (isset($trainee_request->user->email)) {
                    $email = $trainee_request->user->email;
               }else{
                    $email = '';
               }

               if ($arr_line['user_type'] != NULL) {
                    if ( $user_typ == 'staff' || $user_typ == 'sroff' || $user_typ == 'off') {
                        //mengambil avp
                        if (array_key_exists('avp', $arr_line)) {
                            if (is_array($arr_line['avp']) && array_key_exists('name', $arr_line['avp'])) {
                                
                                $atasan = $arr_line['avp']['name'];
                            
                            }else{
                                
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                                
                            }
                        }else{
                            //mengambil vp
                            if (array_key_exists('vp', $arr_line)) {
                                if (is_array($arr_line['vp']) && array_key_exists('name', $arr_line['vp'])) {
                                
                                    $atasan = $arr_line['vp']['name'];
                                
                                }else{
                                
                                    $atasan = '-';
                                    $manag = $role->manag_id($email);
                                    $atasan = $manag['manager'];
                                
                                }
                            }else{
                                //mengambil svp
                                if (array_key_exists('svp', $arr_line)) {
                                    if (is_array($arr_line['svp']) && array_key_exists('name', $arr_line['svp'])) {
                                        $atasan = $arr_line['svp']['name'];
                                    }else{
                                        $atasan = '-';
                                        $manag = $role->manag_id($email);
                                        $atasan = $manag['manager'];
                                    }
                                }else{
                                    //end else
                                    $atasan = '-';
                                    $manag = $role->manag_id($email);
                                    $atasan = $manag['manager'];
                                }
                            }
                        }
                    }elseif ( $user_typ == 'avp') {
                        if (array_key_exists('vp', $arr_line)) {
                            if (is_array($arr_line['vp']) && array_key_exists('name', $arr_line['vp'])) {
                                $atasan = $arr_line['vp']['name'];
                            }else{
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }else{
                            if (array_key_exists('svp', $arr_line)) {
                                if (is_array($arr_line['svp']) && array_key_exists('name', $arr_line['svp'])) {
                                    $atasan = $arr_line['svp']['name'];
                                }else{
                                    $atasan = '-';
                                }
                            }else{
                                //end else
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }
                    }elseif ( $user_typ == 'vp') {
                        if (array_key_exists('svp', $arr_line)) {
                            if (is_array($arr_line['svp']) && array_key_exists('name', $arr_line['svp'])) {
                                $atasan = $arr_line['svp']['name'];
                            }else{
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }else{
                            if (array_key_exists('dir', $arr_line)) {
                                if ( is_array($arr_line['dir']) && array_key_exists('name', $arr_line['dir'])) {
                                    $atasan = $arr_line['dir']['name'];
                                }else{
                                    $atasan = '-';
                                    $manag = $role->manag_id($email);
                                    $atasan = $manag['manager'];
                                }
                            }else{
                                //end else
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }
                    }elseif ($user_typ == 'svp') {
                        //ambil dir
                        if (array_key_exists('dir', $arr_line)) {
                            if (is_array($arr_line['dir']) && array_key_exists('name', $arr_line['dir'])) {
                                $atasan = $arr_line['dir']['name'];
                            }else{
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }else{
                            //end else
                            $atasan = '-';
                            $manag = $role->manag_id($email);
                            $atasan = $manag['manager'];
                        }
                    }else{
                    //isi sesuai manager_id
                    $manag = $role->manag_id($email);
                    $atasan = $manag['manager'];
                    }
               }else{
                    //kosong di bagian user type
                    $atasan = '-';
               }

                if ($atasan == '' || $atasan == NULL) {
                   $atasan = 'Nama belum diinput di database';
                }

               return $atasan;

            })

            ->addColumn('start', function($trainee_request) {
                switch($trainee_request->form_group_id){
                    case 1:
                        $date = $trainee_request->request_data()->where('form_answer','assignment_pg_start_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 2:
                        $date = $trainee_request->request_data()->where('form_answer','coaching_ci_start_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 3:
                        $date = $trainee_request->request_data()->where('form_answer','training_ti_start_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 4:
                        $date = $trainee_request->request_data()->where('form_answer','mentoring_ci_start_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;
                    
                    case 5:
                        $date = $trainee_request->request_data()->where('form_answer','training_ti_start_date')->first();

                        if($date){
                            $date = $date->request_content;
                        }else{
                            $date = '-';
                        }
                    break;

                    default :
                        $date = '-';
                    break;
                }

                return $date;
            })
            ->addColumn('end', function($trainee_request) {
                switch($trainee_request->form_group_id){
                    case 1:
                        $date = $trainee_request->request_data()->where('form_answer','assignment_pg_end_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 2:
                        $date = $trainee_request->request_data()->where('form_answer','coaching_ci_end_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 3:
                        $date = $trainee_request->request_data()->where('form_answer','training_ti_end_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 4:
                        $date = $trainee_request->request_data()->where('form_answer','mentoring_ci_end_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;
                    
                    case 5:
                        $date = $trainee_request->request_data()->where('form_answer','training_ti_end_date');
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    default :
                        $date = '-';
                    break;
                }

                return $date;
            })
            ->addColumn('manager', function($trainee_request) {
                //if ($trainee_request->is_rejected==1 && $trainee_request->rejected_by != 0){
                if ($trainee_request->is_rejected==1){
                    $status = '<span class="badge badge-pill badge-default">Rejected</span>';
                } else {
                    $status = '<span class="badge badge-pill '.(!empty($trainee_request->first_accepter) ? 'badge-success' : 'badge-info').'">
                    '.(!empty($trainee_request->first_accepter) ? 'Approved' : 'Waiting Approval').'</span>';
                }
                return $status;
            })
            ->addColumn('hr', function($trainee_request) {
                //if ($trainee_request->is_rejected==1 && $trainee_request->rejected_by != 0){
                if ($trainee_request->is_rejected==1){
                    $status = '<span class="badge badge-pill badge-default">Rejected</span>';
                } else {
                    $status = '<span class="badge badge-pill '.(!empty($trainee_request->second_accepter) ? 'badge-success' : 'badge-info').'">
                    '.(!empty($trainee_request->second_accepter) ? 'Approved' : 'Waiting Approval').'</span>';
                }
                return $status;
            })
            ->addColumn('type_prog', function($trainee_request) {
                switch($trainee_request->form_group_id){
                    case 1:
                        $request_data = $trainee_request->request_data()->where('form_answer','assignment_dev_type')->first();

                        if($request_data){
                            if ($request_data->request_content == 'internship') {
                                $type_prog = 'Cross Functional assignment';
                            }elseif($request_data->request_content == 'globaltalent'){
                                $type_prog = 'Global Talent Mobility / National Talent Mobility';
                            }else{
                                $type_prog = 'project';
                            }
                        }else{
                            $type_prog = '-';
                        }

                    break;

                    case 2:
                        $request_data = $trainee_request->request_data()->where('form_answer','coaching_ci_type')->first();

                        if($request_data){
                            $type_prog = $request_data->request_content;
                        }else{
                            $type_prog = '-';
                        }
                    break;

                    case 3:
                        $request_data = $trainee_request->request_data()->where('form_answer','training_ti_type_programs')->first();
                        if($request_data){
                            if ($request_data->request_content == 'seminar certification') {
                                $type_prog = 'Seminar/Conference';
                            }elseif($request_data->request_content == 'technical certification'){
                                $type_prog = 'Professional/Technical Certification';
                            }else{
                                $type_prog = 'General Training';
                            }
                        }else{
                            $type_prog = '-';
                        }
                    break;

                    case 4:
                        $type_prog = 'External Speaker';
                    break;
                    
                    case 5:
                        $request_data = $trainee_request->request_data()->where('form_answer','training_ti_type_programs')->first();

                        if($request_data){
                            $type_prog = $request_data->request_content;
                        }else{
                            $type_prog = '-';
                        }
                    break;

                    default :
                        $type_prog = '-';
                    break;
                }
                return $type_prog;
            })
            ->addColumn('option', function($trainee_request) use ($role) {
                
                if ($role->inAdmin()){
                    $accepted = User::select("id")->where("id",$trainee_request->second_accepter)
                        ->orWhere("id",$trainee_request->rejected_by)->first();
                } else {
                    $accepted = User::select("id")
                    ->where("id",$trainee_request->first_accepter)
                    ->orWhere("id",$trainee_request->rejected_by)->first();
                }

                if ($trainee_request->is_accepted_by_me){
                    $accepted = true;
                }

                $disabled = "";
                if ($accepted){ 
                    $disabled = "disabled";
                }

                switch($trainee_request->form_group_id){
                    case 1:
                        $type = 'assignment';
                    break;

                    case 2:
                        $type = 'mentoring';
                    break;

                    case 3:
                        $type = 'training';
                    break;

                    case 4:
                        $type = 'external_speaker';
                    break;
                    
                    case 5:
                        $type = 'inhouse';
                    break;

                    default:
                        $type = '-';
                    break;
                }

                $option = '<td>';

                    if ($trainee_request->is_rejected){
                        $option .= $trainee_request->rejected_notes;
                    }else{
                        //$trainee_request->is_completed == 0 &&
                        if ($trainee_request->is_rejected == 0) {
                            
                            if (($trainee_request->first_accepter == NULL) || ($trainee_request->second_accepter == NULL)) {
                                
                                $option .= '<a href="'.route("trainee.export",["user_id" => $trainee_request->user_id,"id"=>$trainee_request->id]).'" target="_blank" >
                                <button type="button" class="btn btn-default btn-sm btn-lite">
                                    <i class="icon-transfer-data-between-documents"></i> Export
                                </button></a>';

                            }else{
                                if ($trainee_request->form_group_id != 2) 
                                {
                                    $option .= '<a href="'.route("trainee.export",["user_id" => $trainee_request->user_id,"id"=>$trainee_request->id]).'" target="_blank" >
                                    <button type="button" class="btn btn-default btn-sm btn-lite">
                                        <i class="icon-transfer-data-between-documents"></i> Export
                                    </button></a>';  
                                }
                            }

                            if ($trainee_request->is_accepted == 1) {
                                if (($trainee_request->first_accepter == NULL) && ($trainee_request->second_accepter == NULL)) 
                                {
                                    $option .= '<a href="'.route("trainee.review",["id"=>$trainee_request->id]).'">
                                        <button type="button" class="btn btn-default btn-sm btn-lite">
                                            <i class="icon-edit-1"></i> Review
                                        </button>
                                    </a>';
                                }
                            }else{
                            //
                            }

                            $option .= '<a id="preview'.$trainee_request->id.'" href="'.route("trainee.previewByUserId",["user_id" => $trainee_request->user_id,"id"=>$trainee_request->id]).'">
                                <button type="button" class="btn btn-default btn-sm btn-lite">
                                    <i class="icon-contract-1"></i> Preview
                                </button>
                            </a>';
                            

                            if($trainee_request->form_group_id == '1' || $trainee_request->form_group_id == '2'){
                                if($trainee_request->is_accepted == '1' && $trainee_request->form_group_id == '1'){
                                //
                                $option .= '<button type="button" class="btn btn-default btn-sm btn-lite rating-modal"data-requestgroup="'.$trainee_request->id.'" data-staffid="'.$trainee_request->user_id.'" data-mentorid="'.$trainee_request->first_accepter.'" data-toggle="modal" data-target="#assignment">
                                        <i class="icon-verified"></i> Done
                                    </button>'; 
                                
                                }elseif($trainee_request->is_completed_by_manager == 1 && $trainee_request->is_accepted == 1 && $trainee_request->form_group_id == '2'){
                                //
                                $option .='<button type="button" class="btn btn-default btn-sm btn-lite rating-modal" data-requestgroup="'.$trainee_request->id.'" data-staffid="'.$trainee_request->user_id.'" data-mentorid="'.$trainee_request->first_accepter_id.'" data-toggle="modal" data-target="#mentoringForm">
                                    <i class="icon-verified"></i> Done
                                </button>';

                                }else{
                                //
                                    /*
                                    if (($trainee_request->first_accepter != NULL) && ($trainee_request->second_accepter != NULL)) {
                                        $option .= '<button type="button" class="btn btn-default btn-sm btn-lite disabled toast_mentor_trigger" data-toggle="tooltip" data-placement="top" title="Mentor need to complete report first">
                                                <i class="icon-verified"></i> Done
                                            </button>';
                                    }
                                    */
                                }

                                // #tambah role by aprove
                                if ($trainee_request->form_group_id == '2' && ($trainee_request->first_accepter != NULL) && ($trainee_request->second_accepter != NULL)) {
                                    $option .= '<a href="'.route("trainee.reviewByUserId",["user_id" => $trainee_request->user_id,"id"=>$trainee_request->id]).'">
                                        <button type="button" class="btn btn-default btn-sm btn-lite">
                                            <i class="icon-edit-1"></i> Report
                                        </button>
                                    </a>';
                                    
                                    //export report
                                    $option .= '<a href="'.route("trainee.export2",["user_id" => $trainee_request->user_id,"id"=>$trainee_request->id]).'" target="_blank" >
                                    <button type="button" class="btn btn-default btn-sm btn-lite">
                                        <i class="icon-transfer-data-between-documents"></i> Export
                                    </button>
                                    </a>';
                                
                                }else{ }

                                if($trainee_request->form_group_id == '4'){
                                    $option = '<a href="'.route("trainee.setDone",["form_id"=>$trainee_request->form_group_id,"id"=>$trainee_request->id]).'">
                                    <button type="button" class="btn btn-default btn-sm btn-lite">
                                        <i class="icon-verified"></i> Done
                                    </button>
                                    </a>
                                    <div class="hidden">'.$trainee_request->id.'</div>';
                                }

                            }

                            if($trainee_request->form_group_id == '4' && ($trainee_request->first_accepter != NULL) && ($trainee_request->second_accepter != NULL)){
                                    $option .= '<a href="'.route("trainee.setDone",["form_id"=>$trainee_request->form_group_id,"id"=>$trainee_request->id]).'">
                                    <button type="button" class="btn btn-default btn-sm btn-lite">
                                        <i class="icon-verified"></i> Done
                                    </button>
                                    </a>
                                    <div class="hidden">'.$trainee_request->id.'</div>';
                            }
                            
                            if($trainee_request->form_group_id == '3' && ($trainee_request->first_accepter != NULL) && ($trainee_request->second_accepter != NULL)){
                                    $option .= '<a href="#!" class="btn btn-default btn-sm btn-lite rating-modal" data-requestgroup="'.$trainee_request->id.'" data-staffid="'.$trainee_request->user_id.'" data-mentorid="'.$trainee_request->first_accepter.'" data-toggle="modal" data-target="#trainingForm"> 
                                    <i class="icon-verified"></i> Done
                                    </a>';
                            }
                            //$option .= $trainee_request->user_id;
                        }else{
                            $option .= '-';
                        }
                    }
                
                $option .='</td>';
                
                //$option = $trainee_request;
                return $option;
            })
            ->rawColumns(['option', 'manager', 'hr'])
            ->make(true);
    }
    
    public function datatableTraineeRequest2($type){
        $role = new UserRolesHelper();
        $trainee_request = $this->getTraineRequest2($type);

        return Datatables::of($trainee_request)
            ->addIndexColumn()
            ->addColumn('title', function($trainee_request) {
                //$title = $trainee_request->user_id;
                switch($trainee_request->form_group_id){
                    case 1:
                        $request_q = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','assignment_pg_project_title')->first();

                        if($request_q){
                            $title = $request_q->request_content;
                        }else{
                            $title = '-';
                        }
                    break;

                    case 2:
                        $request_q = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','coaching_cc_development_goal')->first();

                        if($request_q){
                            $title = $request_q->request_content;
                        }else{
                            $title = '-';
                        }
                    break;

                    case 3:
                        $request_q = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','training_ti_title')->first();
                        
                        if($request_q){
                            $title = $request_q->request_content;
                        }else{
                            $title = '-';
                        }
                    break;

                    case 4:
                        $request_q = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','mentoring_ci_line_topic')->first();

                        if($request_q){
                            $title = $request_q->request_content;
                        }else{
                            $title = '-';
                        }
                    break;
                    
                    case 5:
                        $request_q = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','training_ti_title')->first();

                        if($request_q){
                            $title = $request_q->request_content;
                        }else{
                            $title = '-';
                        }
                    break;

                    default :
                        $title = '-';
                    break;
                }
                return $title;
            })
            ->addColumn('type', function($trainee_request) {
                $trainee_name = traineeModel::where('id',$trainee_request->form_group_id)->first();

                if (isset($trainee_name->name)) {
                    
                    $type = $trainee_name->name;
                
                }else{
                    $type = '-';
                }
                
                return str_replace("&", "/", $type);
            })
            ->addColumn('employee_name', function($trainee_request) {

                $user_name = UserModel::where('id',$trainee_request->user_id)->first();
                
                if (isset($user_name->first_name)){
                    $full_name = $user_name->first_name;
                }else{
                    $full_name = '-';
                }
                
                return $full_name;
            })

            ->addColumn('atasan', function($trainee_request){
               $role = new UserRolesHelper();
               //request_grup id
               $id_train2 =  $trainee_request->id;

               //user id dari request id
               $id_us =  $trainee_request->user_id; 

               $arr_line = $role->getLeaders((int)$id_us);
               
               $user_typ = NULL;
               
               if (isset($arr_line['user_type'])) {
                   $user_typ = $arr_line['user_type'];
               }else{
                   $user_typ = '';
               }
               $atasan = '';
               
               //$managerid = '';
               //$email = '';
               $name_manager = '-';
               
               $users_id = UserModel::where('id',$trainee_request->user_id)->first();
               
               if (isset($users_id->email)) {
                    $email = $users_id->email;
               }else{
                    $email = '';
               }

               if ($arr_line['user_type'] != NULL) {
                    if ( $user_typ == 'staff' || $user_typ == 'sroff' || $user_typ == 'off') {
                        //mengambil avp
                        if (array_key_exists('avp', $arr_line)) {
                            if (is_array($arr_line['avp']) && array_key_exists('name', $arr_line['avp'])) {
                                $atasan = $arr_line['avp']['name'];
                            }else{
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }else{
                            //mengambil vp
                            if (array_key_exists('vp', $arr_line)) {
                                if (is_array($arr_line['vp']) && array_key_exists('name', $arr_line['vp'])) {
                                    $atasan = $arr_line['vp']['name'];
                                }else{
                                    $atasan = '-';
                                    $manag = $role->manag_id($email);
                                    $atasan = $manag['manager'];
                                }
                            }else{
                                //mengambil svp
                                if (array_key_exists('svp', $arr_line)) {
                                    if (is_array($arr_line['svp']) && array_key_exists('name', $arr_line['svp'])) {
                                        $atasan = $arr_line['svp']['name'];
                                    }else{
                                        $atasan = '-';
                                        $manag = $role->manag_id($email);
                                        $atasan = $manag['manager'];
                                    }
                                }else{
                                    //end else
                                    $atasan = '-';
                                    $manag = $role->manag_id($email);
                                    $atasan = $manag['manager'];
                                }
                            }
                        }
                    }elseif ( $user_typ == 'avp') {
                        if (array_key_exists('vp', $arr_line)) {
                            if (is_array($arr_line['vp']) && array_key_exists('name', $arr_line['vp'])) {
                                $atasan = $arr_line['vp']['name'];
                            }else{
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }else{
                            if (array_key_exists('svp', $arr_line)) {
                                if (is_array($arr_line['svp']) && array_key_exists('name', $arr_line['svp'])) {
                                    $atasan = $arr_line['svp']['name'];
                                }else{
                                    $atasan = '-';
                                }
                            }else{
                                //end else
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }
                    }elseif ( $user_typ == 'vp') {
                        if (array_key_exists('svp', $arr_line)) {
                            if (is_array($arr_line['svp']) && array_key_exists('name', $arr_line['svp'])) {
                                $atasan = $arr_line['svp']['name'];
                            }else{
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }else{
                            if (array_key_exists('dir', $arr_line)) {
                                if ( is_array($arr_line['dir']) && array_key_exists('name', $arr_line['dir'])) {
                                    $atasan = $arr_line['dir']['name'];
                                }else{
                                    $atasan = '-';
                                    $manag = $role->manag_id($email);
                                    $atasan = $manag['manager'];
                                }
                            }else{
                                //end else
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }
                    }elseif ($user_typ == 'svp') {
                        //ambil dir
                        if (array_key_exists('dir', $arr_line)) {
                            if (is_array($arr_line['dir']) && array_key_exists('name', $arr_line['dir'])) {
                                $atasan = $arr_line['dir']['name'];
                            }else{
                                $atasan = '-';
                                $manag = $role->manag_id($email);
                                $atasan = $manag['manager'];
                            }
                        }else{
                            //end else
                            $atasan = '-';
                            $manag = $role->manag_id($email);
                            $atasan = $manag['manager'];
                        }
                    }else{
                    //isi sesuai manager_id
                    $manag = $role->manag_id($email);
                    $atasan = $manag['manager'];
                    }
               }else{
                    //kosong di bagian user type
                    $atasan = '-';
               }

                if ($atasan == '' || $atasan == NULL) {
                   $atasan = 'Nama belum diinput di database';
                }

               return $atasan;

            })

            ->addColumn('start', function($trainee_request) {
                switch($trainee_request->form_group_id){
                    case 1:
                        $date = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','assignment_pg_start_date')->first();

                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 2:
                        $date = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','coaching_ci_start_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 3:
                        $date = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','training_ti_start_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 4:
                        $date = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','mentoring_ci_start_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;
                    
                    case 5:
                        $date = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','training_ti_start_date')->first();

                        if($date){
                            $date = $date->request_content;
                        }else{
                            $date = '-';
                        }
                    break;

                    default :
                        $date = '-';
                    break;
                }

                return $date;
            })
            ->addColumn('end', function($trainee_request) {
                switch($trainee_request->form_group_id){
                    case 1:
                        $date = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','assignment_pg_end_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 2:
                        $date = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','coaching_ci_end_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 3:
                        $date = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','training_ti_end_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    case 4:
                        $date = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','mentoring_ci_end_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;
                    
                    case 5:
                        $date = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','training_ti_end_date')->first();
                        if ($date){
                            $date = $date->request_content;
                        } else {
                            $date = "-";
                        }
                    break;

                    default :
                        $date = '-';
                    break;
                }

                return $date;
            })
            ->addColumn('manager', function($trainee_request) {
                if (isset($trainee_request->first_accepter)) {
                    $status = '<span class="badge badge-pill badge-success">Approved</span>';
                }else{
                    $status = '<span class="badge badge-pill badge-info">waiting Approval</span>';
                }
                /*
                $status = '<span class="badge badge-pill '.(!empty($trainee_request->first_accepter) ? 'badge-success' : 'badge-info').'">'.(!empty($trainee_request->first_accepter) ? 'Approved' : 'Waiting Approval').'</span>';
                */
                return $status;
            })
            ->addColumn('hr', function($trainee_request) {
                if (isset($trainee_request->second_accepter)) {
                    $status = '<span class="badge badge-pill badge-success">Approved</span>';
                }else{
                    $status = '<span class="badge badge-pill badge-info">waiting Approval</span>';
                }
                /*
                    $status = '<span class="badge badge-pill '.(!empty($trainee_request->second_accepter) ? 'badge-success' : 'badge-info').'">'.(!empty($trainee_request->second_accepter) ? 'Approved' : 'Waiting Approval').'</span>';
                */
                return $status;
            })
            ->addColumn('rating', function($trainee_request) {
                $rate = ratingModel::where(['request_group_id'=>$trainee_request->id])->first();
                if (isset($rate->rating)) {
                    $rating = $rate->rating;
                }else{
                    $rating = '-';   
                }
                return $rating;
            })
            ->addColumn('type_prog', function($trainee_request) {
                switch($trainee_request->form_group_id){
                    case 1:
                        $request_data = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','assignment_dev_type')->first();

                        if($request_data){
                            if ($request_data->request_content == 'internship') {
                                $type_prog = 'Cross Functional assignment';
                            }elseif($request_data->request_content == 'globaltalent'){
                                $type_prog = 'Global Talent Mobility / National Talent Mobility';
                            }else{
                                $type_prog = 'project';
                            }
                        }else{
                            $type_prog = '-';
                        }
                        /*
                        if($request_data){
                            $type_prog = $request_data->request_content;
                        }else{
                            $type_prog = '-';
                        }
                        */
                    break;

                    case 2:
                        $request_data = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','coaching_ci_type')->first();

                        if($request_data){
                            $type_prog = $request_data->request_content;
                        }else{
                            $type_prog = '-';
                        }
                    break;

                    case 3:
                        $request_data = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','training_ti_type_programs')->first();
                        if($request_data){
                            if ($request_data->request_content == 'seminar certification') {
                                $type_prog = 'Seminar/Conference';
                            }elseif($request_data->request_content == 'technical certification'){
                                $type_prog = 'Professional/Technical Certification';
                            }else{
                                $type_prog = 'General Training';
                            }
                        }else{
                            $type_prog = '-';
                        }
                    break;

                    case 4:
                        $type_prog = 'External Speaker';
                    break;
                    
                    case 5:
                        $request_data = requestsModel::where('requests_group_id',$trainee_request->id)->where('form_answer','training_ti_type_programs')->first();
                        if($request_data){
                            $type_prog = $request_data->request_content;
                        }else{
                            $type_prog = '-';
                        }
                    break;

                    default :
                        $type_prog = '-';
                    break;
                }
                return $type_prog;
            })
            ->addColumn('option', function($trainee_request) use ($role) {
                $rated_by_staff = ratingModel::where(['request_group_id'=> $trainee_request->id, 'staff_id'=>Sentinel::check()->id])->get(['id'])->first() ? true : false;

                $rating_count = ratingModel::where('request_group_id','=',$trainee_request->id)->count();

                if ($role->inAdmin()){
                    $accepted = User::select("id")->where("id",$trainee_request->second_accepter)
                        ->orWhere("id",$trainee_request->rejected_by)->first();
                } else {
                    $accepted = User::select("id")
                    ->where("id",$trainee_request->first_accepter)
                    ->orWhere("id",$trainee_request->rejected_by)->first();
                }

                $disabled = "";
                if ($accepted){ 
                    $disabled = "disabled";
                }

                $option = '<td>';
                $user = \Sentinel::check();
                $rating_count = ratingModel::where('request_group_id','=',$trainee_request->id)->count();
                
                if ($trainee_request->form_group_id != 2) {
                    
                    $option .= '<a href="'.route("trainee.export",["user_id" => $trainee_request->user_id,"id"=>$trainee_request->id]).'" target="_blank" >
                        <button type="button" class="btn btn-default btn-sm btn-lite">
                            <i class="icon-transfer-data-between-documents"></i> Export
                        </button>
                    </a>';
                
                }

                $option .= '<a id="preview'.$trainee_request->id.'" href="'.route("trainee.previewByUserId",["user_id" => $trainee_request->user_id,"id"=>$trainee_request->id]).'">
                    <button type="button" class="btn btn-default btn-sm btn-lite">
                        <i class="icon-contract-1"></i> Preview
                    </button>
                    <div class="hidden">'.$trainee_request->id.'</div>
                </a>';

                if($trainee_request->form_group_id == 2){
                    $option .='
                    <a href="'.route('trainee.review',$trainee_request->id).'">
                        <button type="button" class="btn btn-default btn-sm btn-lite">
                            <i class="icon-edit-1"></i> See Result
                        </button>
                    </a>';

                    //export report
                    $option .= '<a href="'.route("trainee.export2",["user_id" => $trainee_request->user_id,"id"=>$trainee_request->id]).'" target="_blank" >
                    <button type="button" class="btn btn-default btn-sm btn-lite">
                        <i class="icon-transfer-data-between-documents"></i> Export
                    </button>
                    </a>';
                }

                if($trainee_request->form_group_id == 5 && !$rated_by_staff && $role->inStaff()){

                    $check=RequestsUserModel::where('requests_group_id',$trainee_request->id)->where('user_id',$user->id)->get();
                     
                     if(count($check) > 0){
                     foreach($check as $un){

                         $user_positi=$un->user_position;
                     }

                    if($user_positi and  $user_positi=="Employee"){

                    $option .= '<button type="button" class="btn btn-default btn-sm btn-lite rating-modal"
                        data-requestgroup="'.$trainee_request->id.'" data-staffid="'.$user->id.'" data-mentorid="5" data-toggle="modal" data-target="#assignment">
                    <i class="icon-verified"></i> Rate
                    </button>';
                    }else{

                         $option .= '<button type="button" class="btn btn-default btn-sm btn-lite rating-modal"
                        data-requestgroup="'.$trainee_request->id.'" data-staffid="'.$user->id.'" data-mentorid="5" data-toggle="modal" data-target="#assignment">
                    <i class="icon-verified"></i> Done
                    </button>';



                    }

                   }

                   
                }
                        
                if ($trainee_request->form_group_id == 2) {
                    if($rating_count == 0){
                        $option .= '<button type="button" class="btn btn-default btn-sm btn-lite rating-modal" data-requestgroup="'.$trainee_request->id.'" data-staffid="'.$user->id.'" data-mentorid="'.$trainee_request->first_accepter.'" data-toggle="modal" data-target="#assignment2"> 
                                    <i class="icon-edit-1"></i> Rate
                            </button>';

                    }else{
                    }
                }

                //endadd
                $option .='</td>';

                return $option;
            })
            ->rawColumns(['option', 'manager', 'hr'])
            ->make(true);
    }

//end new -----------------------------
//end new -----------------------------
//end new -----------------------------

public function exportTrainee(){
        $user = Sentinel::check();
        $model = new requestsModel();
        $role = new UserRolesHelper();

        $data['requests'] = $model->groupRequest($model->getGroupedRequest($user->id));

        $approvedCount = 0;
        if ($data['requests']){
            $approvedCount = count($data['requests']['approved']);
        }
        $data['requests']['approvedCount'] = $approvedCount;

        $data['form_type'] = requestsModel::groupById;

        $related = $this->related();

        $data['related_group'] = $related;

        if($data['related_group']) {
            foreach ($data['related_group']['requests_group'] as $keys => $items){
                if($items){
                    foreach ($items as $key=>$item){
                        if(!empty($item['approved']) && (isset($item['approved'][key($item['approved'])]['form_group_id']) && $item['approved'][key($item['approved'])]['form_group_id'] == 5)) {
                            if(!isset($data['requests']['completed'])){
                                $data['requests']['completed'] = [];
                            }
                            unset($data['related_group']['requests_group'][$keys]);
                            $data['requests']['completed'][key($item['approved'])] = $item['approved'][key($item['approved'])];
                        }

                        if(!empty($item['completed']) && (isset($item['approved'][key($item['approved'])]['form_group_id']) && $item['approved'][key($item['approved'])]['form_group_id'] == 5)) {
                            if(!isset($data['requests']['completed'])){
                                $data['requests']['approved'] = [];
                            }
                            unset($data['related_group']['requests_group'][$keys]);
                            $data['requests']['completed'][key($item['completed'])] = $item['completed'][key($item['completed'])];
                        }
                    }    
                }
                
            }
        } 

        $export_type = request()->get('type');
        /*
        $excel_data[0] = [
            'Title/Topic' => '',
            'Type' => '',
            // 'Training Provider' => '',
            // 'Cost' => '',
            'Start Date' => '',
            'End Date' => '',
            // 'Training Objective' => '',
            // 'Venue' => '',
            'Employee NIK' => '',
            'Employee Name' => '',
            'Line Manager' => '',
            // 'Employee NIK' => '',
            // 'Employee Position' => '',

            // 'Commitment After Training' => '',
            // 'Commited Start Date' => '',
            // 'Commitment Description' => '',

            'Manager' => '',
            'HR' => '',
            'Mentor/Coach Name' => '',
        ];

        switch($export_type){
            case 'completed' :
                $excel_data[0]['Rating Mentor/Coach'] = '';
                $excel_data[0]['Type of Programs'] = '';
            break;

            case 'rejected' :
                $excel_data[0]['Rating Mentor/Coach'] = '';
                $excel_data[0]['Notes'] = '';
            break;

            default : 
                $excel_data[0]['Type of Programs'] = '';
            break;
        }
        */
        //new 
        $excel_data[0]['Title/Topic'] = '';
        $excel_data[0]['Type'] = '';

        switch($export_type){
            case 'completed' :
                $excel_data[0]['Type of Programs'] = '';
            break;
            case 'rejected' :
                $excel_data[0]['Type of Programs'] = '';
            break;

            default : 
                $excel_data[0]['Type of Programs'] = '';
            break;
        }

        $excel_data[0]['Start Date'] = '';
        $excel_data[0]['End Date'] = '';
        $excel_data[0]['Employee NIK'] = '';
        $excel_data[0]['Employee Name'] = '';
        $excel_data[0]['Line Manager'] = '';
        $excel_data[0]['Manager'] = '';
        $excel_data[0]['HR'] = '';
        $excel_data[0]['Mentor/Coach Nik'] = '';
        $excel_data[0]['Mentor/Coach Name'] = '';

        switch($export_type){
            case 'completed' :
                $excel_data[0]['Rating Mentor/Coach'] = '';
            break;

            case 'rejected' :
                $excel_data[0]['Rating Mentor/Coach'] = '';
                $excel_data[0]['Notes'] = '';
            break;

            default :
                //
            break;
        }
        // end new

        if(isset($data['requests'][$export_type])){
            $data['requests'][$export_type] = array_values($data['requests'][$export_type]);
            $trainee_list = traineeModel::pluck('name','id')->toArray();

            foreach($data['requests'][$export_type] as $key => $request){
                $title = '';
                $description = '';
                $venue = '';
                $program_type = '';
                $type_of_program = '';
                $employee_nik = '';
                $coaching_name = '-';
                //add
                $coaching_nik = '-';
                //
                if (isset($user->email)){

                    $tmp = EmployeeModel::where(['email'=>$user->email])->get(['nik'])->first();
                    $employee_nik = (!empty($tmp->nik) ? $tmp->nik : (!empty($request->user->nik) ? $request->user->nik : $request->user->username));
                }


                if(isset($request['title'])){
                    $title = $request['title'];
                }elseif(isset($request['project_title'])){
                    $title = $request['project_title'];
                }elseif(isset($request['development_goal'])){
                    $title = $request['development_goal'];
                }elseif(isset($request['line_topic'])){
                    $title = $request['line_topic'];
                }

                if(isset($request['line_manager_nik'])){
                    $coaching_nik = $request['line_manager_nik'];
                }else{
                    $coaching_nik = '-';
                }                

                if(isset($request['line_manager_name'])){
                    $coaching_name = $request['line_manager_name'];
                }

                if (isset($request['manag'])) {
                    $atasan = $request['manag'];
                }else{
                    $atasan = '-';
                }

                $program_type = $trainee_list[$request['form_group_id']];               

                if(isset($request['type_programs'])){
                    $type_of_program = $request['type_programs']; 
                }elseif(isset($request['type'])){
                    $type_of_program = $request['type'];
                }elseif(isset($trainee_list[$request['form_group_id']])){
                    $type_of_program = $trainee_list[$request['form_group_id']];
                }

                if ($request['form_group_id'] == 1) {
                    $model_req = requestsModel::where('requests_group_id',$request['request_group_id'])->where('form_answer','assignment_dev_type')->first();
                    //dd($model_req);
                    if (isset($model_req->request_content)) {
                        $type_of_program = $model_req->request_content;
                        if ($type_of_program == 'internship') {
                            $type_of_program = 'Cross Functional assignment';
                        }elseif($type_of_program == 'globaltalent'){
                            $type_of_program = 'Global Talent Mobility / National Talent Mobility';
                        }else{
                            $type_of_program = 'project';
                        }
                    }else{
                        $type_of_program = '';
                    }
                }

                if ($request['form_group_id'] == 3) {
                    $model_req = requestsModel::where('requests_group_id',$request['request_group_id'])->where('form_answer','training_ti_type_programs')->first();
                    //dd($model_req);
                    if (isset($model_req->request_content)) {
                        $type_of_program = $model_req->request_content;
                        if ($type_of_program == 'seminar certification') {
                            $type_of_program = 'Seminar/Conference';
                        }elseif($type_of_program == 'technical certification'){
                            $type_of_program = 'Professional/Technical Certification';
                        }else{
                            $type_of_program = 'General Training';
                        }
                    }else{
                        $type_of_program = '';
                    }
                }

                if(isset($request['description'])){
                    $description = $request['description'];
                }elseif(isset($request['project_description'])){
                    $description = $request['project_description'];
                }

                if(isset($request['venue'])){
                    $venue = $request['venue'];
                }elseif(isset($request['line_venue'])){
                    $venue = $request['line_venue'];
                }

                if(isset($request['employee_position'])){
                    $employee_position = (@unserialize($request['employee_position']) !== false) ? unserialize($request['employee_position'])[0] : $request['employee_position'];
                }else{
                    $employee_position = '';
                }

                if(isset($request['commitment_training'])){
                    $commitment_training = (@unserialize($request['commitment_training']) !== false) ? implode (", ", unserialize($request['commitment_training'])) : $request['commitment_training'];
                }else{
                    $commitment_training = '';
                }

                //new 
                $excel_data[$key]['Title/Topic'] = $title;
                $excel_data[$key]['Type'] = $program_type;

                switch($export_type){
                    case 'completed' :
                        $excel_data[$key]['Type of Programs'] = $type_of_program;
                    break;

                    case 'rejected' :
                        $excel_data[$key]['Type of Programs'] = $type_of_program;
                    break;

                    default : 
                        $excel_data[$key]['Type of Programs'] = $type_of_program;
                    break;
                }

                $excel_data[$key]['Start Date'] = isset($request['start_date']) ? $request['start_date'] : '';
                $excel_data[$key]['End Date'] = isset($request['end_date']) ? $request['end_date'] : '';
                $excel_data[$key]['Employee NIK'] = $employee_nik;
                $excel_data[$key]['Employee Name'] = (@unserialize($request['employee_name']) !== false) ? unserialize($request['employee_name'])[0] : $request['employee_name'];
                $excel_data[$key]['Line Manager'] = $atasan;

                //$excel_data[$key]['Manager'] = ($export_type == 'rejected') ? 'Rejected' : (!empty($request['first_accepter_id']) ? 'Approved' : (($request['first_accepter_id'] === 0) ? 'Approved': 'Waiting Approval'));
                $excel_data[$key]['Manager'] = ($export_type == 'rejected') ? 'Rejected' : (!empty($request['first_accepter_id']) ? 'Approved' : (($request['first_accepter_id'] === '0') ? 'Approved' : 'Waiting Approval'));
                $excel_data[$key]['HR'] = ($export_type == 'rejected') ? 'Rejected' : (!empty($request['second_accepter_id']) ? 'Approved' : (($request['second_accepter_id'] === 0) ? 'Approved' : 'Waiting Approval'));
                $excel_data[$key]['Mentor/Coach Nik'] = $coaching_nik;
                $excel_data[$key]['Mentor/Coach Name'] = $coaching_name;


                switch($export_type){
                    case 'completed' :
                        $excel_data[$key]['Rating Mentor/Coach'] = $request['rating_by_staff'];
                    break;

                    case 'rejected' :
                        $excel_data[$key]['Notes'] = $request['rejected_notes'];
                    break;

                    default :
                    break;
                }
                //end new

            }
        }

        return Excel::download(new DefaultExport($excel_data), 'my-trainee-'.$export_type.'.xlsx');
    }
}
