<?php

namespace App\Modules\Trainee\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Notifications\Trainee;
use App\Helpers\UserRolesHelper;
use App\Modules\Auth\Models\UserModel;
use App\Modules\Admindashboard\Models\voucherModel;
use App\Modules\Admindashboard\Models\itemRedeemModel;
use App\Modules\Admindashboard\Models\voucherCodeModel;
use sentinel;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\User;

use Mail;
use App\Mail\TraineeMailNew;

class RedeemVoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = new UserRolesHelper();
        $user = sentinel::check();

        if (!$role->inStaff() && $user) {
            return redirect('404');
        }

        $data = [];

        return view('trainee::voucher.index',$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = new UserRolesHelper();

        if (!$role->inStaff()) {
            return redirect('404');
        }
        $user = sentinel::check()->toArray();
        $voucher = voucherModel::find($request->get('id'));

        if (!$voucher) {
            return redirect('404');
        }

        if ($voucher->point > $user['point']) {
            return back()->withMessage('Your point are not enough');
        }

        $voucher_code = voucherCodeModel::where('voucher_id',$request->get('id'))
            ->where('user_id', 0)
            ->first();

        if(!$voucher_code){
            return redirect('404');
        }

        $used_voucher = voucherCodeModel::join('voucher', 'voucher.id', '=', 'voucher_codes.voucher_id')
            ->join('item_redeem', 'item_redeem.voucher_code', '=', 'voucher_codes.code')
            ->where('voucher_id',$request->get('id'))
            ->where('voucher_codes.user_id', $user['id'])
            ->whereYear('item_redeem.created_at', date('Y'))
            ->groupBy('voucher_codes.code')
            ->sum('amount');

        if($used_voucher >= 1500000){
            return back()->withMessage('Redeemed voucher in this year has been reached Rp 1.500.000,00');
        }
        
        voucherCodeModel::where('code',$voucher_code->code)->update([
            'user_id' => $user['id']
        ]);

        $voucherRedeem = new itemRedeemModel();
        $redeemed = [
          'user_id' => $user['id'],
          'item_id' => null,
          'voucher_code' => $voucher_code->code,
          'point_from' => $user['point'],
          'point_to' => ($user['point'] - $voucher['point']),
          'approved' => 1
        ];

        $voucherRedeem->create($redeemed);
        $user_point = $user['point'] - $voucher['point'];

        UserModel::where('id',$user['id'])->update(['point'=>$user_point]);

        $messages = [
            'message_header' => 'Voucher Redeemed',
            'message_date' => now()->timestamp,
            'url' => '/profile',
            'type' => 'voucher-redeem',
            'subject' => 'You Have Redeem Voucher',
            'message_body' => '<p>Congratulations!</p>
                <p>Your have successfully to redeem voucher: <b>'.$voucher->name.'</b> with '.$voucher->point.' point(s).</p>
                <p>Please use this code <b>'.$voucher_code->code.'</b> in tokopedia</p><br/>',
            'form_group' => null,
            'to' => $user['email']
        ];

        $user_model = new User();
        $user = $user_model->find($user['id']);
        $user->notify(new Trainee($messages));
        
        
        Mail::to($user['email'])->send(new TraineeMailNew($messages));
        

        return back()->withMessage('Success to Redeem Voucher');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = new UserRolesHelper();

        if ($role->inManager()){
            return redirect('404');
        }

        $voucher = voucherModel::find($id);

        if (!$voucher){
            return redirect('404');
        }

        $data['voucher'] = $voucher->toArray();
        $data['user'] = sentinel::check()->toArray();
        
        return view('trainee::voucher.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
