<?php

namespace App\Modules\Trainee\Http\Controllers;

use App\Modules\Trainee\Models\requestsGroupModel;
use App\Modules\Trainee\Models\traineeModel;
use App\Modules\Trainee\Models\requestsModel;

use App\Http\Controllers\Controller;
use App\User;
use Sentinel; 
use App\Helpers\UserRolesHelper;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Excel;
use App\Exports\DefaultExport;

class DashboardController extends Controller
{
    //
    public function view() {

        $role = new UserRolesHelper();
        $user = Sentinel::check();
        $carbon = new Carbon();

        $report['dayInMonth'] = range(1,$carbon->daysInMonth);
        $report['MonthInYear'] = range(1,12); 
        $report['MonthInYearName'] = [];
        for($m=1; $m<=12; ++$m){
            $report['MonthInYearName'][] = date('F', mktime(0, 0, 0, $m, 1));
        }
        $daily = requestsGroupModel::
        select(DB::raw('count(id) as `data`'),DB::raw('CASE 
            WHEN form_group_id = 5 THEN "Inhouse" 
            WHEN form_group_id = 1 THEN "Assignment" 
            WHEN form_group_id = 2 THEN "Mentoring & Coaching" 
            WHEN form_group_id = 3 THEN "Public Training" 
            WHEN form_group_id = 4 THEN "External Speaker" 
            ELSE ""
            END as form_alias
        '),'form_group_id',DB::raw("DAY(created_at) as day"))
            ->where(['user_id'=>$user->id,'is_completed' => 1])
            ->whereMonth('created_at',$carbon->now()->month)
            ->groupBy('form_group_id','day')
            ->get()
            ->groupBy('day')
            ->toArray();

        $rangeDaily = [];

        $rangeDaily['Inhouse'] = [];
        $rangeDaily['Assignment'] = [];
        $rangeDaily['Mentoring & Coaching'] = [];
        $rangeDaily['Public Training'] = [];
        $rangeDaily['External Speaker'] = [];

        foreach ($report['dayInMonth'] as $key=>$day){

            $rangeDaily['Inhouse'][$day] = 0;
            $rangeDaily['Assignment'][$day] = 0;
            $rangeDaily['Mentoring & Coaching'][$day] = 0;
            $rangeDaily['Public Training'][$day] = 0;
            $rangeDaily['External Speaker'][$day] = 0;

            if(isset($daily[$day])) {
                foreach ($daily[$day] as $item){
                    $rangeDaily[$item['form_alias']][$day] = $item['data'];
                }
            }
        }

        $monthly = requestsGroupModel::
        select(DB::raw('count(id) as `data`'),DB::raw('CASE 
            WHEN form_group_id = 5 THEN "Inhouse" 
            WHEN form_group_id = 1 THEN "Assignment" 
            WHEN form_group_id = 2 THEN "Mentoring & Coaching" 
            WHEN form_group_id = 3 THEN "Public Training" 
            WHEN form_group_id = 4 THEN "External Speaker" 
            ELSE "" 
            END as form_alias
        '),'form_group_id',DB::raw("MONTH(created_at) as month"))
            ->where(['user_id'=>$user->id, 'is_completed' => 1])
            ->whereYear('created_at',$carbon->now()->year )
            ->groupBy('form_group_id','month')
            ->get()
            ->groupBy('month')
            ->toArray();

        $rangeMonthly = [];

        $rangeMonthly['Inhouse'] = [];
        $rangeMonthly['Assignment'] = [];
        $rangeMonthly['Mentoring & Coaching'] = [];
        $rangeMonthly['Public Training'] = [];
        $rangeMonthly['External Speaker'] = [];

        foreach ($report['MonthInYear'] as $key=>$month){
            //set default value to 0 for every form
            $rangeMonthly['Inhouse'][$month] = 0;
            $rangeMonthly['Assignment'][$month] = 0;
            $rangeMonthly['Mentoring & Coaching'][$month] = 0;
            $rangeMonthly['Public Training'][$month] = 0;
            $rangeMonthly['External Speaker'][$month] = 0;

            if(isset($monthly[$month])) {
                foreach ($monthly[$month] as $item){
                    $rangeMonthly[$item['form_alias']][$month] = $item['data'];
                }
            }
        }

        $anual = requestsGroupModel::
        select(DB::raw('count(id) as `data`'),DB::raw('CASE 
            WHEN form_group_id = 5 THEN "Inhouse" 
            WHEN form_group_id = 1 THEN "Assignment" 
            WHEN form_group_id = 2 THEN "Mentoring & Coaching" 
            WHEN form_group_id = 3 THEN "Public Training" 
            WHEN form_group_id = 4 THEN "External Speaker" 
            ELSE "" 
            END as form_alias
        '),'form_group_id',DB::raw("YEAR(created_at) as year"))
            ->where(['user_id'=>$user->id])
            ->groupBy('form_group_id','year')
            ->get()
            ->groupBy('year')
            ->toArray();

        $rangeAnual = [];

        foreach ($anual as $key=>$datas){
            //set default value to 0 for every form
            $rangeAnual['Inhouse'][$key] = 0;
            $rangeAnual['Assignment'][$key] = 0;
            $rangeAnual['Mentoring & Coaching'][$key] = 0;
            $rangeAnual['Public Training'][$key] = 0;
            $rangeAnual['External Speaker'][$key] = 0;

            if(isset($anual[$key])) {
                foreach ($anual[$key] as $item){
                    $rangeAnual[$item['form_alias']][$key] = $item['data'];
                }
            }
        }

        $report['daily'] = (count($daily) > 0 && !empty($daily)) ? $daily[key($daily)] : [];
        $report['monthly'] = (count($daily) > 0 && !empty($monthly)) ? $monthly[key($monthly)] : [];
        $report['anual'] = (count($daily) > 0 && !empty($anual)) ? $anual[key($anual)] : [];

        $report['rangeDaily'] = $rangeDaily;
        $report['rangeMonthly'] = $rangeMonthly;
        $report['rangeAnual'] = $rangeAnual;

        $report['yearRange'] = array_keys($anual);

        $data['report'] = $report;
        $staff_ids = [];
        array_push($staff_ids,$user->id);
        $staff_ids = array_filter($staff_ids);

        $new = requestsGroupModel::whereIn('user_id',$staff_ids)
            ->where([
                'is_completed'=>0, 
                'is_rejected' => 0
            ])
            ->whereNotNull('published_at')
            ->whereNull('first_accepter')
            ->get(['id'])
            ->count();

        $onProgress = requestsGroupModel::whereIn('user_id',$staff_ids)
            ->where('is_completed_by_staff', '!=', 1)
            ->where('is_accepted', 1)
            ->where('is_completed_by_staff', 0)
            ->where('form_group_id', '!=', 4)
            ->get(['id'])
            ->count();

        $completed = requestsGroupModel::where(function ($where) use ($staff_ids){
                $where->whereNotNull('first_accepter')
                    ->whereNotNull('second_accepter')
                    ->where('form_group_id', 5)
                    ->whereIn('requests_user.user_id',$staff_ids);
            })
            ->orWhere(function ($where) use ($staff_ids){
                $where->where(['is_completed'=>1])
                    ->where(['is_completed_by_staff'=>1])
                    ->where('form_group_id', '!=', 5)
                    ->whereIn('requests_group.user_id',$staff_ids);
            })
            ->whereNotNull('requests_group.published_at')
            ->join('requests_user', 'requests_user.requests_group_id','requests_group.id')
            ->get(['requests_group.id'])
            ->count();

        $rejected = requestsGroupModel::whereIn('user_id',$staff_ids)
            ->where(['is_rejected'=>1])
            ->get(['id'])
            ->count();

        $data['requests'] = [
            'new' => $new,
            'onProgress' => $onProgress,
            'completed' => $completed,
            'rejected' => $rejected,
        ];

        $data['competencies'] = $this->getCompetencies($staff_ids);
        $data['development_history'] = $this->getDevelopmentHistory((isset($staff_ids[0]) ? $staff_ids[0] : 0)); 
        $data['trainees'] = traineeModel::all(); 

        if ($role->inStaff()) {
            return view('trainee::dashboard',$data);

        } else {
            return redirect('404');
        }
    }

    public static function getLeaderboards(){
        $user = Sentinel::check();

        if(!$user){
            return route('404');
        }

        $user = User::join('users_point_history AS hp', 'hp.user_id','users.id')
            ->whereNotIn('users.id',[5])
            ->orderBy('point','DESC')
            ->groupBy('users.id', 'users.username', 'users.first_name')
            ->limit(5)
            ->get(['users.id as user_id', 'username','first_name', DB::raw('SUM(hp.point) AS point')]);

        return $user->toArray();
    }

    public static function recentTrainee() {
        $user = Sentinel::check();

        if (!$user){
            return null;
        }

        $trainee = requestsGroupModel::
            where(['requests_group.user_id'=>$user->id])
            ->limit(5)
            ->leftJoin('requests as r',function($join) {
                $join
                    ->on('r.requests_group_id','=','requests_group.id')
                    ->where('r.form_answer','not like','mentoring_ci_start_date')
                    ->where('r.form_answer','like','%start_date%');
            })
            ->get([
                'requests_group.id',
                'requests_group.user_id',
                'r.request_content',
                DB::raw('CASE 
                    WHEN requests_group.form_group_id = 5 THEN "Inhouse" 
                    WHEN requests_group.form_group_id = 1 THEN "Assignment" 
                    WHEN requests_group.form_group_id = 2 THEN "Mentoring & Coaching" 
                    WHEN requests_group.form_group_id = 3 THEN "Public Training" 
                    WHEN requests_group.form_group_id = 4 THEN "External Speaker" 
                    ELSE "" 
                    END as form_group_id')
            ])
            ->toArray();
        return $trainee;
    }

    private function getCompetencies($staff_ids){
        $competencies = [
            'accelerate_business_and_customer' => [1 => 0, 2 => 0, 3 => 0, 5 => 0],
            'cultivate_networks_&_partnerships' => [1 => 0, 2 => 0, 3 => 0, 5 => 0],
            'decisiveness' => [1 => 0, 2 => 0, 3 => 0, 5 => 0],
            'learning_agility' => [1 => 0, 2 => 0, 3 => 0, 5 => 0],
            'making_a_difference' => [1 => 0, 2 => 0, 3 => 0, 5 => 0],
            'people_management' => [1 => 0, 2 => 0, 3 => 0, 5 => 0],
            'translating_strategy_into_action' => [1 => 0, 2 => 0, 3 => 0, 5 => 0]
        ];

        $request_group_ids = requestsGroupModel::whereIn('form_group_id', [1,2,3,5])
            ->whereIn('user_id', $staff_ids)
            ->where(['is_completed'=>1])
            ->where(['is_completed_by_staff'=>1])
            ->pluck('id')->toArray();
        
        $request_data = requestsModel::select('form_group_id', 'form_answer', 'request_content')
            ->whereIn('form_group_id', [1,2,3,5])
            ->whereIn('form_answer', ['assignment_obj_employee_competency','coaching_cc_development_mtioc', 'training_obj_employee_competency'])
            ->groupBy('form_group_id', 'form_answer', 'request_content')
            ->whereIn('requests_group_id', $request_group_ids)
            ->get();
        
        foreach($request_data as $req){
            $req_content = unserialize($req->request_content);
            foreach($req_content as $row){

                $competency_name = strtolower(str_replace(' ', '_', $row));

                if(isset($competencies[$competency_name]) && isset($competencies[$competency_name][$req->form_group_id])){
                    $competencies[$competency_name][$req->form_group_id]++;
                }
            }    
        }
        return $competencies;
    }

    private function getDevelopmentHistory($user_id){
        $development_history = [];
        $available_answer = [
            'assignment_inter_end_date',
            'assignment_inter_start_date',
            'assignment_pg_end_date',
            'assignment_pg_start_date',
            'coaching_ci_end_date',
            'coaching_ci_start_date',
            'mentoring_ci_end_date',
            'mentoring_ci_start_date',
            'training_obj_end_date2',
            'training_obj_start_date2',
            'training_ti_end_date',
            'training_ti_start_date',
            'assignment_pg_project_title',
            'training_ti_title'
        ];

        $history_data = traineeModel::from('trainees AS t')
            ->select('t.id','t.name', 'r.form_answer', 'r.request_content', 'tp.point')
            ->join('requests AS r', 'r.form_group_id', '=', 't.id')
            ->join('requests_group AS rg', 'rg.id', '=', 'r.requests_group_id')
            ->join('trainees_point AS tp', 'tp.trainee_id', '=', 't.id')
            ->where(function ($where){
                $where->orWhere([
                    'rg.is_completed_by_manager'=>1,
                    'rg.is_completed_by_admin'=>1,
                    'rg.is_completed_by_staff'=>1
                ]);
            })
            ->where('rg.user_id', $user_id)
            ->whereIn('form_answer', $available_answer)
            ->groupBy('t.id', 't.name', 'r.form_answer', 'r.request_content', 'tp.point')
            ->get();

        foreach($history_data as $row){
            if(strpos($row->form_answer, 'title') !== false && empty($development_history[$row->id]['title'])){
                $development_history[$row->id]['title'] = $row->request_content;
            }

            if(strpos($row->form_answer, 'start_date') !== false && empty($development_history[$row->id]['start_date'])){
                $development_history[$row->id]['start_date'] = $row->request_content;
            }

            if(strpos($row->form_answer, 'end_date') !== false && empty($development_history[$row->id]['end_date'])){
                $development_history[$row->id]['end_date'] = $row->request_content;
            }

            $development_history[$row->id]['point'] = $row->point;
        }

        return $development_history;
    }

    public function exportChartTraining(){
        $month= [];
        $report = [];
        $role = new UserRolesHelper();
        $user = Sentinel::check();
        $carbon = new Carbon();

        $report['MonthInYear'] = range(1,12);
        $monthly = requestsGroupModel::
        select(DB::raw('count(id) as `data`'),DB::raw('CASE 
            WHEN form_group_id = 5 THEN "Inhouse" 
            WHEN form_group_id = 1 THEN "Assignment" 
            WHEN form_group_id = 2 THEN "Mentoring & Coaching" 
            WHEN form_group_id = 3 THEN "Public Training" 
            WHEN form_group_id = 4 THEN "External Speaker" 
            ELSE "" 
            END as form_alias
        '),'form_group_id',DB::raw("MONTH(created_at) as month"))
            ->where(['user_id'=>$user->id])
            ->whereYear('created_at', (request()->has('year') ? request()->get('year') : $carbon->now()->year))
            ->groupBy('form_group_id','month')
            ->get()
            ->groupBy('month')
            ->toArray();

        $rangeMonthly = [];

        $rangeMonthly['Inhouse'] = [];
        $rangeMonthly['Assignment'] = [];
        $rangeMonthly['Mentoring & Coaching'] = [];
        $rangeMonthly['Public Training'] = [];
        $rangeMonthly['External Speaker'] = [];

        foreach ($report['MonthInYear'] as $key=>$month){
            //set default value to 0 for every form
            $rangeMonthly['Inhouse'][$month] = 0;
            $rangeMonthly['Assignment'][$month] = 0;
            $rangeMonthly['Mentoring & Coaching'][$month] = 0;
            $rangeMonthly['Public Training'][$month] = 0;
            $rangeMonthly['External Speaker'][$month] = 0;

            if(isset($monthly[$month])) {
                foreach ($monthly[$month] as $item){
                    $rangeMonthly[$item['form_alias']][$month] = $item['data'];
                }
            }
        }

        $daily = requestsGroupModel::
        select(DB::raw('count(id) as `data`'),DB::raw('CASE 
            WHEN form_group_id = 5 THEN "Inhouse" 
            WHEN form_group_id = 1 THEN "Assignment" 
            WHEN form_group_id = 2 THEN "Mentoring & Coaching" 
            WHEN form_group_id = 3 THEN "Public Training" 
            WHEN form_group_id = 4 THEN "External Speaker" 
            ELSE ""
            END as form_alias
        '),'form_group_id',DB::raw("DAY(created_at) as day"))
            ->where(['user_id'=>$user->id])
            ->whereMonth('created_at',(request()->has('year') ? request()->get('year') : $carbon->now()->year))
            ->groupBy('form_group_id','day')
            ->get()
            ->groupBy('day')
            ->toArray();

        for($m=1; $m<=12; ++$m){
            $month = date('F', mktime(0, 0, 0, $m, 1));
            $data[$m-1] =[
                'Month' => date("F", strtotime($month)), 
                'External Speaker' => 0, 
                'Public Training' => 0, 
                'Mentoring & Coaching' => 0, 
                'Assignment' => 0,
                'Inhouse' => 0
            ];
        }
        
        foreach($monthly as $month){
            foreach($month as $item){
                if(isset($data[($item['month']-1)])){
                    $data[($item['month']-1)][$item['form_alias']] = $rangeMonthly[$item['form_alias']][$item['month']];
                }
            }
        }

        return Excel::download(new DefaultExport($data), 'chart-training.xlsx');
    }

    public function filterChartValue(){
        $user = Sentinel::check();
        $carbon = new Carbon();
        $monthInYear = range(1,12);
        $rangeMonthly = [];

        $rangeMonthly['Inhouse'] = [];
        $rangeMonthly['Assignment'] = [];
        $rangeMonthly['Mentoring & Coaching'] = [];
        $rangeMonthly['Public Training'] = [];
        $rangeMonthly['External Speaker'] = [];

        $monthly = requestsGroupModel::
        select(DB::raw('count(id) as `data`'),DB::raw('CASE 
            WHEN form_group_id = 5 THEN "Inhouse" 
            WHEN form_group_id = 1 THEN "Assignment" 
            WHEN form_group_id = 2 THEN "Mentoring & Coaching" 
            WHEN form_group_id = 3 THEN "Public Training" 
            WHEN form_group_id = 4 THEN "External Speaker" 
            ELSE "" 
            END as form_alias
        '),'form_group_id',DB::raw("MONTH(created_at) as month"))
            ->where(['user_id'=>$user->id,'is_completed' => 1])
            ->whereYear('created_at', (request()->has('year') ? request()->get('year') : $carbon->now()->year))
            ->groupBy('form_group_id','month')
            ->get()
            ->groupBy('month')
            ->toArray();

        $daily = requestsGroupModel::
            select(DB::raw('count(id) as `data`'),DB::raw('CASE 
                WHEN form_group_id = 5 THEN "Inhouse" 
                WHEN form_group_id = 1 THEN "Assignment" 
                WHEN form_group_id = 2 THEN "Mentoring & Coaching" 
                WHEN form_group_id = 3 THEN "Public Training" 
                WHEN form_group_id = 4 THEN "External Speaker" 
                ELSE ""
                END as form_alias
            '),'form_group_id',DB::raw("DAY(created_at) as day"))
                ->where(['user_id'=>$user->id,'is_completed' => 1])
                ->whereMonth('created_at', $carbon->now()->month)
                ->groupBy('form_group_id','day')
                ->get()
                ->groupBy('day')
                ->toArray();

        foreach ($monthInYear as $key => $month){
            $rangeMonthly['Inhouse'][$month] = 0;
            $rangeMonthly['Assignment'][$month] = 0;
            $rangeMonthly['Mentoring & Coaching'][$month] = 0;
            $rangeMonthly['Public Training'][$month] = 0;
            $rangeMonthly['External Speaker'][$month] = 0;

            if(isset($monthly[$month])) {
                foreach ($monthly[$month] as $item){
                    $rangeMonthly[$item['form_alias']][$month] = $item['data'];
                }
            }
        }

        $monthly = (count($daily) > 0 && !empty($monthly)) ? $monthly[key($monthly)] : [];
        
        return [
            'monthly' => $monthly,
            'rangeMonthly' => $rangeMonthly
        ];
    }
}
