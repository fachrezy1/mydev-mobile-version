<?php

namespace App\Modules\Trainee\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Sentinel;

class AssignmentFormController extends Controller
{
    //

    public function __construct()
    {

    }

    public function step1() {
        return view('trainee::request.assignment.step1');
    }
}
