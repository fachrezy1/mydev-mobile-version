<?php

namespace App\Modules\Trainee\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Sentinel;
use App\User;
use App\Traits\UserRolesTrait;
use App\Notifications\Trainee;
use App\Modules\Auth\Models\UserModel;

use App\Modules\Trainee\Models\traineeModel;

use Mail;
use App\Mail\TraineeMailNew;

class TraineeRequestController extends Controller
{
    use UserRolesTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        if ($this->isMultipleRoles()) {
          if ($this->inStaff()) {
              return $this->loadStaff();
          } elseif($this->inAdmin() || $this->inManager()) {
              return $this->loadAdmin();
          }
//        }
//        if ($user->inRole('hr')) {
//            return $this->loadAdmin();
//        } elseif ($user->inRole('staff')) {
//            return $this->loadStaff();
//        }

        return back();
    }

    private function loadStaff() {
        $user = Sentinel::check();
        $trainees = traineeModel::where(['trainees_for' => 'staff'])->get();
        $data['formData']['type'] = 'Public Training';
        return view('trainee::index', compact('data', 'trainees', 'user'));
    }

    public function loadStaffbyAdmin() {
        $user = Sentinel::check();
        $trainees = traineeModel::where(['trainees_for' => 'staff'])->get();
        $data['formData']['type'] = 'Public Training';
        return view('trainee::index', compact('data', 'trainees', 'user'));
    }

    private function loadAdmin() {
//        dd('admin');
        $trainees = traineeModel::where(['trainees_for' => 'admin'])->get();
        $data['formData']['type'] = 'Public Training';
        return view('trainee::index', compact('data', 'trainees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function requestTraining(){
        $user_model = new User();
        $user = $user_model->find(5);
        
        $return = [
            'error' => true,
            'message' => 'Invalid to create training request',
        ];

        $update_data = UserModel::where('id', request()->get('userID'))
            ->update([
                'is_block' => 1,
            ]);

        $trainee_data = UserModel::find(request()->get('userID'));

        $messages = [
            'message_header' => 'Unlock the registration form',
            'message_date' => now()->timestamp,
            'url' => '/dashboard/setting/employees',
            'type' => 'unlock registration form',
            'subject' => 'Grant Access to This Request',
            'message_body' => ucwords(strtolower($trainee_data->first_name)).' '.ucwords(strtolower($trainee_data->last_name)).' has requested to access MyDev. Respond to the request <a href="'.url('dashboard/setting/employees').'">here</a>',
            'form_group' => '0',
            'to' => $user->email
        ];

        $user->notify(new Trainee($messages));   
        
        Mail::to($user->email)->send(new TraineeMailNew($messages));

        if($update_data){
            $return['error'] = false;
            $return['message'] = 'You have submited your access request';
        }
        
        return json_encode($return);
    }
}
