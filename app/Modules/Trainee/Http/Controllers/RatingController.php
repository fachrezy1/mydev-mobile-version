<?php

namespace App\Modules\Trainee\Http\Controllers;

use App\Helpers\UserRolesHelper;
use App\Modules\Trainee\Models\requestsModel;
use App\User;
use Illuminate\Http\Request;
use App\Modules\Trainee\Models\ratingModel;
use App\Modules\Trainee\Models\requestsGroupModel;
use App\Modules\Trainee\Models\RequestsUserModel;
use App\Notifications\Trainee;
use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\Controller;
use Sentinel;

use Mail;
use App\Mail\TraineeMailNew;

class RatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $model = new ratingModel();
        $requestGroupModel = new requestsGroupModel();
        $role = new UserRolesHelper();

        $data = $request->toArray();
        unset($data['_token']);

        $requestGroup = $requestGroupModel->where(['id'=>$data['request_group_id']])->get();

        if ($requestGroup->isEmpty()) {
            return back()->withError('Request Not Found');
        } else {
              if($requestGroup[0]->form_group_id != 1 && $requestGroup[0]->form_group_id != 3){
                $requestGroupModel->where(['id'=>$data['request_group_id']])->update(['is_completed_by_staff'=>'1']);

            } else {
                $requestGroupModel->where(['id'=>$data['request_group_id']])->update(['is_completed_by_staff'=>'1','is_completed'=>1]);

            }
        }

        if ($request->hasFile('attachment')){
            $file = $request->validate([
                'attachment'=>'mimes:pdf,doc,docx,jpeg,jpg,png'
            ]);

           $stamp = time();
            Storage::disk('uploads')->putFileAs('public-training/rating/',$request->file('attachment'),$stamp.$request->file('attachment')->getClientOriginalName());

            $data['attachment'] = 'public-training/rating/'.$stamp.$request->file('attachment')->getClientOriginalName() ;

        }
        $model->create($data);

        $user = Sentinel::check();

        $mail = [
            'message_header' => '',
            'subject' => 'Check Out What They Say about Your Sessions',
            'message_body' => '<p>Thank you for sharing your knowledge & improving others!</p>
                <p>Let’s improve your coaching/mentoring experience by learning from your previous coaching/mentoring review <a href="'.url('/trainee/preview/'.$requestGroup[0]['user_id'].'/'.$data['request_group_id']).'">here</a></p>
                <p>See you on the next development opportunities!</p>',
            'message_date' => now()->timestamp,
            'url' => '/trainee/preview/'.$requestGroup[0]['user_id'].'/'.$data['request_group_id'],
            'type' => 'form request',
            'form_group' => requestsModel::groupById[$requestGroup[0]->form_group_id]
        ];

        $message = $mail;

        if($role->inAdmin($data['mentor_id'])){
            $admin = User::find($data['mentor_id']);
            $message['to'] = $admin->email;
            $admin->notify(new Trainee($message));
            
            
            //Mail::to($admin->email)->send(new TraineeMailNew($message));
            
            

        } else{

            $mentor = User::find($data['mentor_id']);
            $message['to'] = $mentor->email;
            $mentor->notify(new Trainee($message));
            

            
            //Mail::to($mentor->email)->send(new TraineeMailNew($message));
            
            

            $first_accepter = User::find($requestGroup[0]['first_accepter']);
            $message['to'] = $first_accepter->email;
            $first_accepter->notify(new Trainee($message));

            

            //Mail::to($first_accepter->email)->send(new TraineeMailNew($message));
            

            $second_accepter = User::find($requestGroup[0]['second_accepter']);
            $message['to'] = $second_accepter->email;
            $second_accepter->notify(new Trainee($message));
            
            

            //Mail::to($second_accepter->email)->send(new TraineeMailNew($message));
            
            
        }

        return back()->withSuccess('Completed Successfully');
    }

    // new
    public function store2(Request $request)
    {
        //
        $model = new ratingModel();
        $requestGroupModel = new requestsGroupModel();
        $role = new UserRolesHelper();

        $data = $request->toArray();
        unset($data['_token']);
        //dd($data);
        $requestGroup = $requestGroupModel->where(['id'=>$data['request_group_id']])->get();

        if ($requestGroup->isEmpty()) {
            return back()->withError('Request Not Found');
        } else {

        }

        if ($request->hasFile('attachment')){
            $file = $request->validate([
                'attachment'=>'mimes:pdf,doc,docx,jpeg,jpg,png'
            ]);

           $stamp = time();
            Storage::disk('uploads')->putFileAs('public-training/rating/',$request->file('attachment'),$stamp.$request->file('attachment')->getClientOriginalName());

            $data['attachment'] = 'public-training/rating/'.$stamp.$request->file('attachment')->getClientOriginalName() ;

        }
        $model->create($data);

        $user = Sentinel::check();

        $mail = [
            'message_header' => '',
            'subject' => 'Check Out What They Say about Your Sessions',
            'message_body' => '<p>Thank you for sharing your knowledge & improving others!</p>
                <p>Let’s improve your coaching/mentoring experience by learning from your previous coaching/mentoring review <a href="'.url('/trainee/preview/'.$requestGroup[0]['user_id'].'/'.$data['request_group_id']).'">here</a></p>
                <p>See you on the next development opportunities!</p>',
            'message_date' => now()->timestamp,
            'url' => '/trainee/preview/'.$requestGroup[0]['user_id'].'/'.$data['request_group_id'],
            'type' => 'form request',
            'form_group' => requestsModel::groupById[$requestGroup[0]->form_group_id]
        ];

        $message = $mail;
        
        
        if($role->inAdmin($data['mentor_id'])){
            $admin = User::find($data['mentor_id']);
            $message['to'] = $admin->email;
            $admin->notify(new Trainee($message));
            
            
            //Mail::to($admin->email)->send(new TraineeMailNew($message));
            
            

        } else{

            $mentor = User::find($data['mentor_id']);
            $message['to'] = $mentor->email;
            $mentor->notify(new Trainee($message));
            
            
            //Mail::to($mentor->email)->send(new TraineeMailNew($message));
            
            

            $first_accepter = User::find($requestGroup[0]['first_accepter']);
            $message['to'] = $first_accepter->email;
            $first_accepter->notify(new Trainee($message));
            
            
            //Mail::to($first_accepter->email)->send(new TraineeMailNew($message));
            
            

            $second_accepter = User::find($requestGroup[0]['second_accepter']);
            $message['to'] = $second_accepter->email;
            $second_accepter->notify(new Trainee($message));
            
            
            //Mail::to($second_accepter->email)->send(new TraineeMailNew($message));
            
        }
        

        return back()->withSuccess('Completed Successfully Review');

    }
    // new 

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
