<?php

namespace App\Modules\Trainee\Http\Controllers;

use App\Modules\Auth\Models\EmployeeModel;
use App\Modules\Trainee\Models\ratingModel;
use App\Modules\Trainee\Models\requestsGroupModel;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Trainee\Models\requestsModel;
use App\Traits\ResponsesTrait;
use App\Notifications\Trainee;
use Illuminate\Support\Facades\Storage;
use App\Modules\Trainee\Models\RequestsUserModel;
use App\User;
use App\Helpers\UserRolesHelper;
use App\Modules\Auth\Models\UserRolesModel;
use App\Modules\Auth\Models\UserModel;

use App\Helpers\ActivityHelper;

use Sentinel;
use Mail;
use App\Mail\TraineeMailNew;

class TraineeFormSubmitController extends Controller
{
    //
    use ResponsesTrait;

    private $activity;

    function __construct()
    {
        $this->activity = new ActivityHelper();
    }

    function setSession($data, $key) {
        if (session()->has('formData')){
            $current = session('formData');
            $current[$key] = $data[$key];
            session(['formData'=>$current]);
        }else{
            session(['formData'=>$data]);
        }
    }

    function submitStep(Request $request){


        if($request->training_obj_import_employee){

            $import="yes";

        }else{
            $import="no";
        }


        if (isset($request->assignment_dev_type) && $request->assignment_dev_type=="globaltalent"){
            $validatedData = $request->validate([
                'assignment_obj_employee_competency' => 'sometimes|required',
                'assignment_obj_employee_competency.*' => 'sometimes|required',
                "assignment_gt_opco" => 'sometimes|required',
                "assignment_gt_type" => 'sometimes|required',
                "assignment_gt_start_date" => 'sometimes|required',
                "assignment_gt_end_date" => 'sometimes|required',
                "assignment_gt_purpose" => 'sometimes|required',
                "assignment_gt_reason" => 'sometimes|required',
                "assignment_gt_reason.*" => 'sometimes|required'
                //"assigment_gt_attachment" => 'sometimes|required'
            ]);

        } else {

            $validatedData = $request->validate([
                'assignment_obj_employee_competency' => 'sometimes|required',
                'assignment_obj_employee_competency.*' => 'sometimes|required',
                'assignment_pg_project_leader' => 'sometimes|required',
                'assignment_pg_project_leader_nik' => 'sometimes|required',
                //add
                'assignment_ci_employee_name' => 'sometimes|required',
                'coaching_ci_employee_name' => 'sometimes|required',
                'training_obj_employee_name[]' => 'sometimes|required',
                'training_obj_employee_nik[]' => 'sometimes|required',
                //end add
                
                // ------------  For Cross Functional Type --------
                // 'assignment_pg_project_title' => 'sometimes|required',
                // 'assignment_pg_start_date' => 'sometimes|required',
                // 'assignment_pg_end_date' => 'sometimes|required',
                // 'assignment_pg_project_description' => 'sometimes|required',
                // 'assignment_pg_development_goal' => 'sometimes|required',
                
                // ------------  For Project Type -----------------
                // 'assignment_inter_group' => 'sometimes|required',
                // 'assignment_inter_division' => 'sometimes|required',
                // 'assignment_inter_start_date' => 'sometimes|required',
                // 'assignment_inter_end_date' => 'sometimes|required',
                //'assignment_inter_development_goal' => 'sometimes|required',
                'assignment_inter_mentor_name' => 'sometimes|required',
                'assignment_inter_mentor_nik' => 'sometimes|required',
                'coaching_ci_line_manager_name' => 'sometimes|required',
                'coaching_ci_start_date' => 'sometimes|required',
                'coaching_ci_end_date' => 'sometimes|required',
                'coaching_cc_development_goal' => 'sometimes|required',
                'training_ti_title' => 'sometimes|required',
                'training_obj_employee_competency' => 'sometimes|required',
                'training_obj_description' => 'sometimes|required',
                'training_obj_commitment_description' => 'sometimes|required',
                'training_obj_start_date2' => 'sometimes|required',
                'training_obj_end_date2' => 'sometimes|required',
                'training_ti_start_date' => 'sometimes|required',
                'training_ti_end_date' => 'sometimes|required',
                //'training_ti_provider' => 'sometimes|required',
                'training_ti_cost' => 'sometimes|required',
                'mentoring_ci_employee_name' => 'sometimes|required',
                'mentoring_ci_employee_nik' => 'sometimes|required',
                'mentoring_ci_line_topic' => 'sometimes|required',
                'mentoring_ci_organization' => 'sometimes|required',
                'mentoring_ci_notes' => 'sometimes|required',
                'mentoring_ci_line_venue' => 'sometimes|required',
                'mentoring_ci_start_date' => 'sometimes|required',
                'mentoring_ci_end_date' => 'sometimes|required',
                //'training_obj_trainer_name' => 'sometimes|required',
                //'training_obj_trainer_nik' => 'sometimes|required',
                'training_obj_employee_name' => 'sometimes|required',
                'training_obj_employee_position' => 'sometimes|required',
            ]);

        }

        $model = new requestsModel();
        $datas = $request->toArray();
        $roleHelper = new UserRolesHelper();

        if (isset($datas['user_role'])) {
            $user_role = $datas['user_role'];
            $user_role_nik = $datas['user_nik'];
        }

        $is_last = false;
        if (isset($datas['is_last'])) {
            $is_last = true;
        }
        if (isset($datas['form_group_id'])) {
            $group_id = $datas['form_group_id'];
            $valid_datas = $this->filterReponses($datas,$datas['form_group_id']);
            $user = Sentinel::check();
            /*
            if (session('user_id')) {
                $user_id = session('user_id');

            } else {
                $user_id = $user->id;
            }
            */

            $sess_check = session('user_id');
            
            if ($group_id == 'Inhouse') {
                if (session('user_id')) {
                    $user_id = session('user_id');
                } else {
                    $user_id = $user->id;
                }
            }else{
                if (isset($datas['coaching_ci_employee_nik'])) {
                    if($roleHelper->inAdmin()){
                        $user_get = UserModel::where('username',$datas['coaching_ci_employee_nik'])->first();
                        $user_id_get = '';
                        //
                        if (isset($user_get->id)) {
                            $user_id_get = $user_get->id;
                            if (session('user_id')) {
                                //$user_id = session('user_id');
                                $request->session()->put('user_id_sebelum', session('user_id'));
                            } else {
                                $request->session()->put('user_id_sebelum', $user->id);
                                //$user_id = $user->id;
                            }
                            $request->session()->put('user_id', $user_id_get);
                        }
                        //
                        if ($user_id_get == $sess_check) {
                            if (session('user_id')) {
                                $user_id = session('user_id');
                            } else {
                                $user_id = $user->id;
                            }
                        }else{
                            $user_id = $user_get->id;
                        }
                    }else{
                        if (session('user_id')) {
                            $user_id = session('user_id');
                        } else {
                            $user_id = $user->id;
                        }
                    }
                }elseif(isset($datas['assignment_ci_employee_nik'])){
                    if($roleHelper->inAdmin()){
                        $user_get = UserModel::where('username',$datas['assignment_ci_employee_nik'])->first();
                        $user_id_get = '';
                        //
                        if (isset($user_get->id)) {
                            $user_id_get = $user_get->id;
                            if (session('user_id')) {
                                //$user_id = session('user_id');
                                $request->session()->put('user_id_sebelum', session('user_id'));
                            } else {
                                $request->session()->put('user_id_sebelum', $user->id);
                                //$user_id = $user->id;
                            }
                            $request->session()->put('user_id', $user_id_get);
                        }
                        //
                        if ($user_id_get == $sess_check) {
                            if (session('user_id')) {
                                $user_id = session('user_id');
                            } else {
                                $user_id = $user->id;
                            }
                        }else{
                            $user_id = $user_get->id;
                        }
                    }else{
                        if (session('user_id')) {
                            $user_id = session('user_id');
                        } else {
                            $user_id = $user->id;
                        }
                    }
                }elseif(isset($datas['training_obj_employee_nik'][0])){
                    if($roleHelper->inAdmin()){
                        $user_get = UserModel::where('username',$datas['training_obj_employee_nik'][0])->first();
                        $user_id_get = '';
                        //
                        if (isset($user_get->id)) {
                            $user_id_get = $user_get->id;
                            if (session('user_id')) {
                                //$user_id = session('user_id');
                                $request->session()->put('user_id_sebelum', session('user_id'));
                            } else {
                                $request->session()->put('user_id_sebelum', $user->id);
                                //$user_id = $user->id;
                            }
                            $request->session()->put('user_id', $user_id_get);
                        }
                        //
                        if ($user_id_get == $sess_check) {
                            if (session('user_id')) {
                                $user_id = session('user_id');
                            } else {
                                $user_id = $user->id;
                            }
                        }else{
                            $user_id = $user_get->id;
                        }
                    }else{
                        if (session('user_id')) {
                            $user_id = session('user_id');
                        } else {
                            $user_id = $user->id;
                        }
                    }
                }elseif(isset($datas['mentoring_ci_employee_nik'])){
                    if($roleHelper->inAdmin()){
                        $user_get = UserModel::where('username',$datas['mentoring_ci_employee_nik'])->first();
                        $user_id_get = '';
                        //
                        if (isset($user_get->id)) {
                            $user_id_get = $user_get->id;
                            if (session('user_id')) {
                                //$user_id = session('user_id');
                                $request->session()->put('user_id_sebelum', session('user_id'));
                            } else {
                                $request->session()->put('user_id_sebelum', $user->id);
                                //$user_id = $user->id;
                            }
                            $request->session()->put('user_id', $user_id_get);
                        }
                        //
                        if ($user_id_get == $sess_check) {
                            if (session('user_id')) {
                                $user_id = session('user_id');
                            } else {
                                $user_id = $user->id;
                            }
                        }else{
                            $user_id = $user_get->id;
                        }
                    }else{
                        if (session('user_id')) {
                            $user_id = session('user_id');
                        } else {
                            $user_id = $user->id;
                        }
                    }
                }else{
                    if (session('user_id')) {
                        $user_id = session('user_id');
                    } else {
                        $user_id = $user->id;
                    }
                }
            }
            //end add script
            $step = 1;

            foreach ($valid_datas as $key=>$data) {
                if (is_array($data)) {
                    foreach ($data as $keyItem=>$item) {
                        if(isset($datas[$item])) {
                            if(is_array($datas[$item])) {
                                $datas[$item] = serialize(array_unique($datas[$item]));
                            }

                            if ($request->hasFile($item)){

                                if ($item=="assigment_gt_attachment"){
                                    $file = $request->validate([
                                        'assigment_gt_attachment'=>'mimes:application/x-zip-compressed,application/x-compressed,zip'
                                    ]);

                                   Storage::disk('uploads')->putFileAs('assigment',$request->file('assigment_gt_attachment'),$request->file('assigment_gt_attachment')->getClientOriginalName());

                                    $datas[$item] = 'assigment/'.$request->file('assigment_gt_attachment')->getClientOriginalName() ;
                                } else {
                                    $file = $request->validate([
                                        'training_obj_attachment'=>'mimes:pdf,doc,docx,jpeg,jpg,png'
                                    ]);

                                   Storage::disk('uploads')->putFileAs('public-training',$request->file('training_obj_attachment'),$request->file('training_obj_attachment')->getClientOriginalName());

                                    $datas[$item] = 'public-training/'.$request->file('training_obj_attachment')->getClientOriginalName() ;

                                }
                            }

                            $input_data = [
                                'form_group_id' => $group_id,
                                'user_id' => $user_id,
                                'form_question' => $key.'_'.$keyItem,
                                'form_answer' => $item,
                                'requests_step' => $step,
                                'request_content' => $datas[$item]
                            ];
                            $requestGroup = $model->add($input_data);
                        }
                    }
                }else{
                    $input_data = [
                        'form_group_id' => $group_id,
                        'user_id' => $user_id,
                        'form_question' => $key,
                        'form_answer' => $data,
                        'requests_step' => $step
                    ];
                    $requestGroup = $model->add($input_data);
                }
                $step = $step+1;
            }
        }else{
            return false;
        }

        if($requestGroup) {
            $request_group_id = $requestGroup->requests_group_id;
        } else {
            $request_group_id = session('group_id');
        }

        if( $request_group_id && isset($datas['user_role']) ) {

            if(!is_array($user_role_nik) && is_integer($user_role_nik) ){

                $user_role_id = User::where(['username'=>$user_role_nik])->get();
                if ($user_role_id->isEmpty()){
                    $user_role_id = $this->registerUser($user_role_nik);
                } else {
                    $user_role_id = $user_role_id->toArray()[0]['id'];
                }
                

                $user_role_data = [
                    'user_id' => $user_role_id,
                    'user_position' => $user_role,
                    'requests_group_id' => $request_group_id
                ];
                $requester = User::where(['id'=>$user_id])->get(['first_name','last_name'])->first();

                $assigned = User::find($user_role_id);

                if($assigned){

                    //ganti nama ke mentoring/coaching
                    $group_id_sebelum = '';
                    if ($group_id == 'Mentoring') {
                        $group_id_sebelum = $group_id;
                        $group_id = 'Mentoring/Coaching';
                    }

                    $message = [
                        'message_body' => 'You have been registered as '.$user_role. ' by '.$requester['first_name'].' '.$requester['last_name'],
                        'message_header' => 'You have been registered',
                        'message_date' => now()->timestamp,
                        'url' => '/trainee/preview/'.$user_id.'/'.$request_group_id,
                        'type' => 'form request',
                        'subject' => 'You have been Registered as '.$user_role.' at from '.$group_id.' Form',
                        'form_group' => $group_id,
                        'to' => (isset($assigned->email) ? $assigned->email : NULL)
                    ];

                    $assigned->notify(new Trainee($message));

                  
                    if(isset($assigned->email)){
                            Mail::to($assigned->email)->send(new TraineeMailNew($message));
                    }
                    
                    

                    

                    // untuk group_id mentoring, kembali ke group mentoring dari mentoring/coaching 
                    if ($group_id == 'Mentoring/Coaching') {
                        $group_id = $group_id_sebelum;
                    }

                }
                
                RequestsUserModel::updateOrCreate(
                    [
                        'user_id'=>$user_role_id,
                        'requests_group_id' => $request_group_id
                    ],
                    $user_role_data
                );
            }
        }


        if($request->training_obj_moderator_nik){

             $user_role_id = User::where(['username'=>$request->training_obj_moderator_nik])->get();
              if ($user_role_id->isEmpty()){
                    $user_role_id = $this->registerUser($request->training_obj_moderator_nik);
                } else {
                    $user_role_id = $user_role_id->toArray()[0]['id'];
                }

            RequestsUserModel::updateOrCreate(
                    [
                        'user_id'=>$user_role_id,
                        'requests_group_id' => $request_group_id,
                        'user_position' => "Moderator"
                    ]
                   
            );
                 



        }

        if($is_last && $group_id == 'Inhouse'){
            $employee_data = User::where(['username'=>$request->post('training_obj_employee_nik')])->first();

            if($employee_data){
                $user_id = $employee_data->id;    
            }
        }

        /*
        
        if(isset($datas['coaching_ci_line_manager_nik']) && $datas['form_group_id'] != 'Public Training' ){
            // assign or remove mentor from requests
            $this->assignUsers($datas['coaching_ci_line_manager_nik'],'Mentor',$request_group_id,$group_id,$user_id,$user);
        }

        if(isset($datas['training_obj_employee_name']) && $datas['form_group_id'] != 'Public Training') {
            //assign or remove and notify user
            $this->assignUsers($datas['training_obj_employee_nik'],'Employee',$request_group_id,$group_id,$user_id,$user);
        }

        if(isset($datas['training_obj_trainer_name']) && $datas['form_group_id'] != 'Public Training') {
            //assign or remove and notify user as trainer
            $this->assignUsers($datas['training_obj_trainer_nik'],'Trainer',$request_group_id,$group_id,$user_id,$user);
        }

        */

         if(isset($datas['coaching_ci_line_manager_nik']) && $datas['form_group_id'] != 'Public Training' && $import=="no"){
            // assign or remove mentor from requests

            


            $this->assignUsers($datas['coaching_ci_line_manager_nik'],'Mentor',$request_group_id,$group_id,$user_id,$user);


        }

        if(isset($datas['training_obj_employee_name']) && $datas['form_group_id'] != 'Public Training' && $import=="no") {
            //assign or remove and notify user
            $this->assignUsers($datas['training_obj_employee_nik'],'Employee',$request_group_id,$group_id,$user_id,$user);
        }

        if(isset($datas['training_obj_trainer_name']) && $datas['form_group_id'] != 'Public Training' && $import=="no") {
            //assign or remove and notify user as trainer
            $this->assignUsers($datas['training_obj_trainer_nik'],'Trainer',$request_group_id,$group_id,$user_id,$user);
        }


        if(isset($datas['training_obj_moderator_name']) && $datas['form_group_id'] != 'Public Training' && $import=="no") {
            //assign or remove and notify user as trainer
            $this->assignUsers($datas['training_obj_moderator_nik'],'Moderator',$request_group_id,$group_id,$user_id,$user);
        }





        if($is_last) {
            if (session('edit_form')) {
                //notify owner that form has been editted by admin
                if(!$roleHelper->inAdmin()){
                    $this->notifyEditedForm($user_id,$user,$group_id);
                }
                session()->flash('success','success');
            }
            else {
                // notify admin and manager that form request has been submitted
                $userPosition = $roleHelper->getLeaders($user_id);
                $first_atasan_id = null;

                $admin_id = User::where(['username'=>'Administrator'])->get()->first()->id;

                $second_atasan_id = $admin_id;

                $requestGroupModel = new requestsGroupModel();

                if($userPosition['user_type'] == 'dir') {
                    $requestGroupModel->where('id',$request_group_id)->update(['first_accepted'=>now()]);
                } elseif ($userPosition['user_type'] == 'svp') {
                    $first_atasan_id = $userPosition['dir']['nik'];
                }elseif ($userPosition['user_type'] == 'vp') {
                    $first_atasan_id = $userPosition['svp']['nik'];
                }else {
                    if(!empty( $userPosition['vp'])){
                        $first_atasan_id = $userPosition['vp']['nik'];
                    }elseif(!empty( $userPosition['svp'])){
                        $first_atasan_id = $userPosition['svp']['nik'];
                    }elseif(!empty( $userPosition['dir'])){
                        $first_atasan_id = $userPosition['dir']['nik'];
                    }
                }

                $user_model = new User();
                if (!$roleHelper->inAdmin()){
                    $atasan = $user_model->where(['username'=>$first_atasan_id])->get();
                    if($atasan->isEmpty()) {
                        $first_atasan_id = $this->registerUser($first_atasan_id);
                    } else {
                        $first_atasan_id = $atasan->first()->id;
                    }

                    $first_atasan = $user_model->find($first_atasan_id);
                    $second_atasan = $user_model->find($second_atasan_id);
                    $user_data = $user_model->find($user_id);

                    //ganti nama ke mentoring/coaching
                    $group_id_sebelum = '';
                    if ($group_id == 'Mentoring') {
                        $group_id_sebelum = $group_id;
                        $group_id = 'Mentoring/Coaching';
                    }

                    $message1 = [
                        'message_body' => 'You have '.$group_id.' form request from '.$user->first_name.' '.$user->last_name,
                        'message_header' => 'You have '.$group_id.' form request',
                        'message_date' => now()->timestamp,
                        'url' => '/trainee/preview/'.$user_id.'/'.$request_group_id,
                        'type' => 'form manage request',
                        'subject' => 'Trainee Form Requests',
                        'form_group' => $group_id,
                        //'to' => $first_atasan->email
                        'to' => (isset($first_atasan->email) ? $first_atasan->email : NULL)
                    ];

                    $message2 = [
                        'message_body' => 'You have '.$group_id.' form request from '.$user->first_name.' '.$user->last_name,
                        'message_header' => 'You have '.$group_id.' form request',
                        'message_date' => now()->timestamp,
                        'url' => '/trainee/preview/'.$user_id.'/'.$request_group_id,
                        'type' => 'form manage request',
                        'subject' => 'Trainee Form Requests',
                        'form_group' => $group_id,
                        //'to' => $second_atasan->email
                        'to' => (isset($second_atasan->email) ? $second_atasan->email : NULL)
                    ];

                    $message3 = [
                        'message_body' => '
                            <p>Congrats! You’ve successfully conquer the first step for your development. 2 more steps to go:</p>
                            <p>
                                <ul>
                                    <li>Get approval from your Line Manager</li>
                                    <li>Get approval from MyDev Admin</li>
                                </ul>
                            </p>
                            <p>This process won’t take long, we promise. So, please be patient & keep your development spirit high!</p>',
                        'message_header' => '',
                        'message_date' => now()->timestamp,
                        'url' => '/trainee/preview/'.$user_id.'/'.$request_group_id,
                        'type' => 'form manage request',
                        'subject' => 'Your Development Request Has Been Submitted',
                        'form_group' => $group_id,
                        //'to' => $user_data->email
                        'to' => (isset($user_data->email) ? $user_data->email : NULL)
                    ];

                    if ($first_atasan) {
                        $first_atasan->notify(new Trainee($message1));
                       
                        
                        if(isset($first_atasan->email)){
                            Mail::to($first_atasan->email)->send(new TraineeMailNew($message1));
                        } 
                        
                        
                        
                    }

                    if ($second_atasan) {
                        $second_atasan->notify(new Trainee($message2));
                       
                        if(isset($second_atasan->email)){
                            Mail::to($second_atasan->email)->send(new TraineeMailNew($message2));
                        }
                        
                        
                        
                    }
                    
                    if ($user_data) {
                        $user_data->notify(new Trainee($message3));
                        
                        
                        if(isset($user_data->email)){
                            Mail::to($user_data->email)->send(new TraineeMailNew($message3));
                        }
                        
                        
                    }

                    // untuk group_id mentoring, kembali ke group mentoring dari mentoring/coaching 
                    if ($group_id == 'Mentoring/Coaching') {
                        $group_id = $group_id_sebelum;
                    }
                    
                }
                if($user->id==5){
                    //log for admin requests
                    $this->activity->logActivity($user,['log_type'=>'Create Trainee','log_data'=>['ip_address'=>\Request::getClientIp(true),'badge_class'=>'badge-info','status'=>'created','date'=>now()]],'You has been created '.$group_id.' trainee');
                } else {
                    $this->activity->logActivity($user,['log_type'=>'Trainee Request','log_data'=>['ip_address'=>\Request::getClientIp(true),'badge_class'=>'badge-info','status'=>'requested','date'=>now()]],'You has been requested for '.$group_id.' trainee request');
                }
            }


            if($roleHelper->inAdmin() && strtolower($datas["form_group_id"])=="inhouse") {
                requestsGroupModel::where(['id'=>$request_group_id])->update(['is_accepted'=>'1']);
            }

            requestsGroupModel::where(['id'=>$request_group_id])->update(['published_at'=>date('Y-m-d H:i:s')]);

            session()->forget('group_id');

            // if($user_id != 5){
            //    UserModel::where('id', $user_id)->update(['is_block' => 0]);
            // }
        }

        if($is_last) {
            if ($request->session()->exists('user_id_sebelum')) {
                $request->session()->put('user_id', session('user_id_sebelum'));
            }else{

            }
        }

        if (isset($datas['redirect'])) {
            return redirect($datas['redirect']);
        }

        return back()->withMessage('Success');
    }

    private function notifyEditedForm($user_id,$user,$group_id) {
        //notify user that form has been edited by admin
        $staff = User::find($user_id);
        $request_group_id = session('group_id');
        
        //ganti nama ke mentoring/coaching
        $group_id_sebelum = '';
        if ($group_id == 'Mentoring') {
            $group_id_sebelum = $group_id;
            $group_id = 'Mentoring/Coaching';
        }

        $message = [
            'message_body' => 'Your form request has been Edited by '.$user->first_name.' '.$user->last_name,
            'message_header' => 'Your form request has been Edited',
            'message_date' => now()->timestamp,
            'url' => '/trainee/preview/'.$user_id.'/'.$request_group_id,
            'type' => 'form manage request',
            'subject' => 'Trainee Form Has Been Edited',
            'form_group' => is_numeric($group_id) ? requestsModel::groupById[$group_id] : $group_id,
            'to' => (isset($staff->email) ? $staff->email : NULL)
        ];

        //todo: check log edited from request by admin
        $this->activity->logActivity($staff,['log_type'=>'Trainee Request','log_data'=>['ip_address'=>\Request::getClientIp(true),'badge_class'=>'badge-success','url'=>'/trainee/preview/'.$user_id.'/'.$request_group_id,'status'=>'edited','date'=>now()]],'Your '.$group_id.' form request has been edited by '.$user->first_name.' '.$user->last_name);

        $staff->notify(new Trainee($message));


        
        if(isset($staff->email)){
            Mail::to($staff->email)->send(new TraineeMailNew($message));
        }
        
        

        // untuk group_id mentoring, kembali ke group mentoring dari mentoring/coaching 
        if ($group_id == 'Mentoring/Coaching') {
            $group_id = $group_id_sebelum;
        }


        session()->forget('edit_form');
    }

    private function assignUsers($nik_user,$position,$request_group_id,$group_id,$user_id,$user,$new_users = null){
        if(@unserialize($nik_user)){
            $employee_nik = array_unique(unserialize($nik_user));
        } else {
            $employee_nik = [$nik_user];
        }

        foreach ($employee_nik as $value => $nik) {
        	
        	if(isset($nik) && !empty($nik)){
	            $employee_id = User::where(['username'=>$nik])->get(['id'])->first();

	            if(!$employee_id) {
	            	$employee_id = $this->registerUser($nik);	
	            	
	            } else {
	                $employee_id = $employee_id->id;
	            }
	            
	            RequestsUserModel::updateOrCreate(
	                [
	                    'user_id' => $employee_id,
	                    'requests_group_id' => $request_group_id,
	                    'user_position' => $position
	                ],
	                [
	                'user_id' => $employee_id,
	                'requests_group_id' => $request_group_id,
	                'user_position' => $position
	                ]
	            );

	            if(RequestsUserModel::where([
	                'user_id' => $employee_id,
	                'requests_group_id' => $request_group_id,
	                'user_position' => $position
	            ])->get(['id'])->first() && (session('edit_form'))) {
	                $employee_notice = User::find($employee_id);
	                
                        //ganti nama ke mentoring/coaching
                        $group_id_sebelum = '';
                        if ($group_id == 'Mentoring') {
                            $group_id_sebelum = $group_id;
                            $group_id = 'Mentoring/Coaching';
                        }

                        $message = [
	                        'message_body' => $group_id.' Has edited by '.$user->first_name.' '.$user->last_name,
	                        'message_header' => $group_id.' Has edited',
	                        'message_date' => now()->timestamp,
	                        'url' => '/trainee/preview/'.$user_id.'/'.$request_group_id,
	                        'type' => 'form manage request',
	                        'subject' => 'Trainee Form Has Been Edited',
	                        'form_group' => $group_id,
	                        'to' => (isset($employee_notice->email) ? $employee_notice->email : NULL)
	                    ];

                        
	                    if(isset($employee_notice)){
	                        $employee_notice->notify(new Trainee($message));    
	                    }
                        

                       
                       if(isset($employee_notice->email)){
	                        Mail::to($employee_notice->email)->send(new TraineeMailNew($message));
	                   }
                       
                       
                       
                       

                        // untuk group_id mentoring, kembali ke group mentoring dari mentoring/coaching 
                        if ($group_id == 'Mentoring/Coaching') {
                            $group_id = $group_id_sebelum;
                        }

	            } else {
	                $user_data = User::find($user_id); 
	                $employee_notice = User::find($employee_id);

	                if (isset($employee_notice) && isset($user_data)) {
	                
                    //ganti nama ke mentoring/coaching
                    $group_id_sebelum = '';
                    if ($group_id == 'Mentoring') {
                        $group_id_sebelum = $group_id;
                        $group_id = 'Mentoring/Coaching';
                    }

                     if($group_id=="Inhouse" or $group_id=="inhouse"){
                          $url='/trainee/preview/5/'.$request_group_id;
                    }else{
                        $url='/trainee/preview/'.$user_id.'/'.$request_group_id;
                     
                    }

                    $message = [
	                    'message_body' => '<p>Congrats '.(isset($employee_notice->first_name) ? $employee_notice->first_name : '' ).' '.$employee_notice->last_name.'!</p>
	                        <p>You are choosen as a '.$position.' to '.$user_data->first_name.' '.$user_data->last_name.'. You can preview the '.$group_id.' request <a href="'.url($url).'">here</a>.</p>
	                        <p>We are glad to have your support in developing Sahabat Indosat Ooredoo & also thankful for your participation. Let’s start your '.$group_id.' discussion with your coachee/mentee.</p>',
	                    'message_header' => 'You have assigned',
	                    'message_date' => now()->timestamp,
	                    'url' => $url,
	                    'type' => 'form manage request',
	                    'subject' => 'Time to Share Your Knowledge with Others',
	                    'form_group' => $group_id,
	                    'to' => (isset($employee_notice->email) ? $employee_notice->email : NULL)
	                ];
	                if(isset($employee_notice)){
	                    $employee_notice->notify(new Trainee($message));    
	                }

                    
	                
                    if(isset($employee_notice->email)){
	                    Mail::to($employee_notice->email)->send(new TraineeMailNew($message));
	                }
                    
                    
                    
                    
                    // untuk group_id mentoring, kembali ke group mentoring dari mentoring/coaching 
                    if ($group_id == 'Mentoring/Coaching') {
                        $group_id = $group_id_sebelum;
                    }

                    }
	            }
	         }
        }

        //begin checking duplicated, new and removed users
        $current_user_ids = RequestsUserModel::where([
            'requests_group_id' => $request_group_id,
            'user_position' => $position
        ])
            ->select('user_id as id')
            ->get()->toArray();
        if(@unserialize($nik_user)){
            $new_users_id = User::whereIn('username',unserialize($nik_user))->get(['id'])->toArray();

        } else{
            $new_users_id = User::whereIn('username',[$nik_user])->get(['id'])->toArray();
        }
        $temp1 = [];
        $temp2 = [];
//            $current_user_ids = array_unique($current_user_ids);
        foreach ($current_user_ids as $current){
            if(!in_array($current['id'],$temp1)){
                $temp1[] = $current['id'];
            }
        }

        foreach ($new_users_id as $new) {
            $temp2[] = $new['id'];
        }
        
        $removed_users = (array_diff($temp1,$temp2));
        if(count($removed_users) > 0){
            foreach ($removed_users as $employee_id){

                if(ratingModel::where([
                    'staff_id' => $employee_id,
                    'request_group_id' => $request_group_id
                ])->get(['id'])->first()){
                    //remove rating user if exist
                    ratingModel::where([
                        'staff_id' => $employee_id,
                        'request_group_id' => $request_group_id
                    ])->delete();
                }

                //remove user from trainee
                RequestsUserModel::where([
                    'user_id' => $employee_id,
                    'requests_group_id' => $request_group_id,
                ])->delete();
                //if user removed
                $employee_notice = User::find($employee_id);
                
                //ganti nama ke mentoring/coaching
                $group_id_sebelum = '';
                if ($group_id == 'Mentoring') {
                    $group_id_sebelum = $group_id;
                    $group_id = 'Mentoring/Coaching';
                }

                $message = [
                    'message_body' =>'You have been removed from '. $group_id.' by '.$user->first_name.' '.$user->last_name,
                    'message_header' => 'You have been removed',
                    'message_date' => now()->timestamp,
                    'url' => '/trainee/preview/'.$user_id.'/'.$request_group_id,
                    'type' => 'form manage request',
                    'subject' => 'You have been Removed from Trainee Form',
                    'form_group' => $group_id,
                    'to' => (isset($employee_notice->email) ? $employee_notice->email : NULL)
                ];
                if(isset($employee_notice)){
                    $employee_notice->notify(new Trainee($message));    
                }
                

                
               
                if(isset($employee_notice->email)){
                        Mail::to($employee_notice->email)->send(new TraineeMailNew($message));
                }
                
                
                
                
                
                // untuk group_id mentoring, kembali ke group mentoring dari mentoring/coaching 
                if ($group_id == 'Mentoring/Coaching') {
                    $group_id = $group_id_sebelum;
                }
            }
        }
        //end checking duplicated, new and removed users
    }

    private function registerUser($nik) {
        $allowed_manager_key = ['vp', 'svp', 'dir'];
        $employee_data = EmployeeModel::where('nik', $nik)->first();
        if(!$employee_data){
            return back()->withMessage('Employee data with NIK : '.$nik.' not found');
        }
        $user_role_id = Sentinel::registerAndActivate([
            'email'    => $employee_data->email,
            'password' => now()->timestamp,
            'username' => $employee_data->nik
        ]);

        //assign staff role to user
        if(!empty($employee_data->manager_id)){
            $staff_role = UserRolesModel::where('slug','staff')->first();
            $user_role_id->roles()->save($staff_role);
        }

        //assign head role to user
        if(in_array(strtolower($employee_data->job_key_short), $allowed_manager_key)){
            $staff_role = UserRolesModel::where('slug','atasan')->first();
            $user_role_id->roles()->save($staff_role);
        }

        return $user_role_id->id;
    }

/*

    public function setDone($form_id,$request_group_id) {
        $model = new requestsGroupModel();
        $user = Sentinel::check();

        if ($form_id == 4 || $form_id == 5) {
            $model->where(['id'=>$request_group_id])->update(['is_completed'=>1]);

            $form_type = requestsModel::groupById[$form_id];

            
            //todo: check log completed form request
            $this->activity->logActivity($user,['log_type'=>'Trainee Request','log_data'=>['ip_address'=>\Request::getClientIp(true),'badge_class'=>'badge-success','status'=>'completed','date'=>now()]],$user->first_name.' '.$user->last_name.' has been completed '.$form_type.' trainee');

            return back()->withMessage('Completed Successfully');
        }
        return false;
    }

*/


    public function setDone($form_id,$request_group_id) {
        $model = new requestsGroupModel();
        $user = Sentinel::check();

        


        if ($form_id == 4) {
            $model->where(['id'=>$request_group_id])->update(['is_completed'=>1]);

            $form_type = requestsModel::groupById[$form_id];

            
            //todo: check log completed form request
            $this->activity->logActivity($user,['log_type'=>'Trainee Request','log_data'=>['ip_address'=>\Request::getClientIp(true),'badge_class'=>'badge-success','status'=>'completed','date'=>now()]],$user->first_name.' '.$user->last_name.' has been completed '.$form_type.' trainee');

            return back()->withMessage('Completed Successfully');
        }

        if ($form_id == 5) {
            $model->where(['id'=>$request_group_id])->update(['is_completed'=>1,'is_completed_by_admin'=>1]);

            $form_type = requestsModel::groupById[$form_id];

            
            //todo: check log completed form request
            $this->activity->logActivity($user,['log_type'=>'Trainee Request','log_data'=>['ip_address'=>\Request::getClientIp(true),'badge_class'=>'badge-success','status'=>'completed','date'=>now()]],$user->first_name.' '.$user->last_name.' has been completed '.$form_type.' trainee');

            return back()->withMessage('Completed Successfully');
        }
        return false;
    }

}
