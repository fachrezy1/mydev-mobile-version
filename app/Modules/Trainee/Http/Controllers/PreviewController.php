<?php

namespace App\Modules\Trainee\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Modules\Trainee\Models\requestsModel;
use App\Modules\Trainee\Models\reviewModel;
use sentinel;
use App\Modules\Auth\Models\EmployeeModel;
use App\Modules\Trainee\Http\Controllers\ReviewController;
use App\Modules\Trainee\Models\ratingModel;
use App\Helpers\UserRolesHelper;

use App\Http\Controllers\Controller;
use PDF;
use App\Modules\Admindashboard\Models\multiplicationModel;

class PreviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    private function getRequest($user_id,$id) {
        $request = new requestsModel();

        $data = $request->getRequestByFormId($id,['user_id','form_type','request_group_id'],$user_id);

        if (!$data){
            return redirect('404');
        }

        $data['requester'] = $this->getUser($data['form_detail']['user_id']);

        return  $data;
    }


    /*

    public function showByUserId($user_id,$id) {
        $role = new UserRolesHelper();
        $request = $this->getData($user_id,$id);

        if(!$request){
            return redirect('404');
        }

        if (isset($request["form_detail"]["development_mtioc"])){
            $cmpcpr = $request["form_detail"]["development_mtioc"];
        } else if (isset($request["form_detail"]["employee_competency"])){
            $cmpcpr = $request["form_detail"]["employee_competency"];
        } else {
            $cmpcpr = "";
        }

        $arr = explode(':',$cmpcpr);
        $competency_selected = [];

        foreach ($arr as $key => $value) {
            if (strlen($value)>5){
                array_push($competency_selected,strtr($value,[";i"=>"",";}"=>"",'"'=>""]));
            }
        }

        $request['competency_selected'] = $competency_selected;

        if (isset($request["form_detail"]["reason"])){
            $gtcpr = $request["form_detail"]["reason"];
        } else {
            $gtcpr = "";
        }

        $arrgt = explode(':',$gtcpr);
        $reason_selected = [];

        foreach ($arrgt as $key => $value) {
            if (strlen($value)>5){
                array_push($reason_selected,strtr($value,[";i"=>"",";}"=>"",'"'=>""]));
            }
        }

        $request['form_detail']['reason'] = $reason_selected;

        $request['is_staff'] = $role->inStaff();

        session()->forget('error');

        $rating = ratingModel::where(['request_group_id'=>$request['request_group_id']])->get();
        $request['rating'] = null;

        if ($rating->isNotEmpty()) {
            $request['rating'] = $rating->toArray()[0];
        }
        //dd($request);
        return view('trainee::preview.index',$request);
    }

    */
    public function showByUserId($user_id,$id) {
        $role = new UserRolesHelper();
        $request = $this->getData($user_id,$id);
        $user_login = sentinel::check();



        if(!$request){
            return redirect('404');
        }


        $form_group=$request['form_detail']['form_group_id'];
        if($form_group=="5"){

        $cek_group_id=requestsModel::where('requests_group_id',$request["form_detail"]["request_group_id"])->get();

         if(count($cek_group_id)>0){

            foreach($cek_group_id as $ce){

                $form_ans[]=$ce->form_answer;
            }
         }
    
        if (in_array("training_obj_moderator_nik", $form_ans)) {
             $request['moderator_name']=$request["form_detail"]["moderator_name"];
             $request['moderator_nik']=$request["form_detail"]["moderator_nik"];
        }else{

             $request['moderator_name']='notfound';
                $request['moderator_nik']='notfound';
        }

    }


        if (isset($request["form_detail"]["development_mtioc"])){
            $cmpcpr = $request["form_detail"]["development_mtioc"];
        } else if (isset($request["form_detail"]["employee_competency"])){
            $cmpcpr = $request["form_detail"]["employee_competency"];
        } else {
            $cmpcpr = "";
        }

        $arr = explode(':',$cmpcpr);
        $competency_selected = [];

        foreach ($arr as $key => $value) {
            if (strlen($value)>5){
                array_push($competency_selected,strtr($value,[";i"=>"",";}"=>"",'"'=>""]));
            }
        }

        $request['competency_selected'] = $competency_selected;

        if (isset($request["form_detail"]["reason"])){
            $gtcpr = $request["form_detail"]["reason"];
        } else {
            $gtcpr = "";
        }

        $arrgt = explode(':',$gtcpr);
        $reason_selected = [];

        foreach ($arrgt as $key => $value) {
            if (strlen($value)>5){
                array_push($reason_selected,strtr($value,[";i"=>"",";}"=>"",'"'=>""]));
            }
        }

        $request['form_detail']['reason'] = $reason_selected;

        $request['is_staff'] = $role->inStaff();
        $request['is_admin'] = $role->inAdmin();

        session()->forget('error');

        $rating = ratingModel::where(['request_group_id'=>$request['request_group_id']])->get();
        $request['rating'] = null;

        if ($rating->isNotEmpty()) {
            $request['rating'] = $rating->toArray()[0];
        }
        //dd($request);

      
        /*

        $myarray = array_push_assoc($myarray, 'h', 'hello');
        */

       if($request["form_detail"]["form_group_id"]==5){

        if($request["form_detail"]["type_programs"]=="moderator"){

               $multi = multiplicationModel::whereIn('trainee_id',[6])->where('active',0)->orderBy('updated_at','desc')->get();
            
        }else{

               $multi = multiplicationModel::whereIn('trainee_id',[5])->where('active',0)->orderBy('updated_at','desc')->get();
        }

       
        foreach($multi as $col){
             

            if($col->tier_2_1){
               $request['tier_2_1']=$col->tier_2_1;
            }else{
               $request['tier_2_1']=0;
            }

            if($col->tier_3){
               $request['tier_3']=$col->tier_3;
            }else{
               $request['tier_3']=0;
            }


            if($col->tier_4){
               $request['tier_4']=$col->tier_4;
            }else{
               $request['tier_4']=0;
            }


            if($col->tier_5){
               $request['tier_5']=$col->tier_5;
            }else{
               $request['tier_5']=0;
            }

          }
            
        }
    
    
        return view('trainee::preview.index',$request);
    }

    public function show($id)
    {
        $model = new requestsModel();

        $request = $this->getPreview($id);

        $review = reviewModel::where(['request_group_id'=>$request['request_group_id']])->get();
        $rating = ratingModel::where(['request_group_id'=>$request['request_group_id']])->get();

        $request['reviews'] = null;
        $request['rating'] = null;

        if ($review->isNotEmpty()) {
            $request['reviews'] = $review->toArray();
        }

        if ($rating->isNotEmpty()) {
            $request['rating'] = $rating->toArray()[0];
        }
        $data = $model->getRequestByFormId($id,['user_id','form_type','request_group_id'],$request['user_id']);
        $form['form_group_id'] = $data['form_detail']['form_group_id'];


        $data['is_completed'] = $data['is_completed'];
        unset($data['is_completed']);


        $merged = array_merge($form, $data);

        $request['form'] = $merged;
        return view('trainee::preview.index',$request);
    }

    private function getUser($user) {
        $user = sentinel::findById($user);
        $user = EmployeeModel::where('nik',$user['username'])->get();
        if ($user->isNotEmpty()) {
            $user = $user->first()->toArray();
        } else {
            $user = null;
        }
        return $user;
    }

    private function getPreview($id,$user_id=null) {
        $model = new requestsModel();

        $request = $model->getRequestByFormId($id,['user_id','form_type','request_group_id'],$user_id);

        if (!$request){
            return false;
        }

        $request['requester'] = $this->getUser($request['form_detail']['user_id']);
        return $request;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function export($user_id,$id){
        $request = $this->getData($user_id,$id);
        
        if(!$request){
            return redirect('404');
        }

        $form_group=$request['form_detail']['form_group_id'];
    
        if($form_group=="5"){

        $cek_group_id=requestsModel::where('requests_group_id',$request["form_detail"]["request_group_id"])->get();

         if(count($cek_group_id)>0){

            foreach($cek_group_id as $ce){

                $form_ans[]=$ce->form_answer;
            }
         }
    
        if (in_array("training_obj_moderator_nik", $form_ans)) {
             $request['moderator_name']=$request["form_detail"]["moderator_name"];
             $request['moderator_nik']=$request["form_detail"]["moderator_nik"];
        }else{

             $request['moderator_name']='notfound';
                $request['moderator_nik']='notfound';
        }

    }


        $pdf = PDF::loadView('trainee::export.index',$request);

        return $pdf->stream($request['form_type'].'-'.time().'-'.Carbon::parse(now())->format('d-m-y').'.pdf');
    }
    private function getData($user_id,$id){
        $model = new requestsModel();
        $request = $this->getPreview($id,$user_id);
        
        if(!$request){
            return false;
        }

        $request_group_id = $request['request_group_id'];

        $review = reviewModel::where(['request_group_id'=>$request_group_id])->get([
            'reviews_date','reviews_goal','reviews_result','reviews_feedback','reviews_action',
            'reviews_course_name','reviews_session_number','reviews_duration','reviews_condition',
            'reviews_committed_date','reviews_criteria','reviews_project_title','reviews_location',
            'reviews_provider','reviews_country','reviews_topic','reviews_end_date','reviews_cost',
            'reviews_attachment','id'
        ]);
        $rating = ratingModel::where(['request_group_id'=>$request_group_id])->get();

        $request['reviews'] = null;
        $request['rating'] = null;
        $total_hours_review = $review->sum('reviews_duration');
        if ($review->isNotEmpty()) {
            $request['reviews'] = $review->toArray();
        }

        if ($rating->isNotEmpty()) {
            $request['rating'] = $rating->toArray()[0];
        }

        $data = $model->getRequestByFormId($id,['user_id','form_type','request_group_id'],$user_id);
        $form['form_group_id'] = $data['form_detail']['form_group_id'];

        $data['is_completed'] = $data['is_completed'];
        unset($data['is_completed']);

        //ger review from current request
        $merged = array_merge($form, $data);

        $request['form'] = $merged;
        $request['total_hours_review'] = $total_hours_review;

        return $request;
    }

    public function export2($user_id,$id){
        $request = $this->getData2($user_id,$id);
        
        if(!$request){
            return redirect('404');
        }

        $pdf = PDF::loadView('trainee::export.mentoringreport',$request);

        return $pdf->stream($request['form_type'].'-'.time().'-'.Carbon::parse(now())->format('d-m-y').'.pdf');
    }

    private function getData2($user_id,$id){
        $model = new requestsModel();
        
        $reviews = reviewModel::where(['request_group_id' => $id])->get();

        $request = $this->getPreview($id,$user_id);
        
        if(!$request){
            return false;
        }

        $request_group_id = $request['request_group_id'];

        $review = reviewModel::where(['request_group_id'=>$request_group_id])->get([
            'reviews_date','reviews_goal','reviews_result','reviews_feedback','reviews_action',
            'reviews_course_name','reviews_session_number','reviews_duration','reviews_condition',
            'reviews_committed_date','reviews_criteria','reviews_project_title','reviews_location',
            'reviews_provider','reviews_country','reviews_topic','reviews_end_date','reviews_cost',
            'reviews_attachment','id'
        ]);

        //dd($review);

        $rating = ratingModel::where(['request_group_id'=>$request_group_id])->get();

        $request['reviews'] = null;
        $request['rating'] = null;
        $total_hours_review = $review->sum('reviews_duration');
        if ($review->isNotEmpty()) {
            $request['reviews'] = $review->toArray();
        }

        if ($rating->isNotEmpty()) {
            $request['rating'] = $rating->toArray()[0];
        }

        $data = $model->getRequestByFormId($id,['user_id','form_type','request_group_id'],$user_id);
        $form['form_group_id'] = $data['form_detail']['form_group_id'];

        $data['is_completed'] = $data['is_completed'];
        unset($data['is_completed']);

        //ger review from current request
        $merged = array_merge($form, $data);

        $request['report'] = $review;

        //dd($request['report']);
        $request['form'] = $merged;
        $request['total_hours_review'] = $total_hours_review;

        return $request;
    }
}
