<?php

namespace App\Modules\Trainee\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Auth\Models\EmployeeModel;
use App\Helpers\UserRolesHelper;
use App\Modules\Trainee\Models\Competency;
use App\Modules\Trainee\Models\CompetencyCategory;

use App\Http\Controllers\Controller;
use Sentinel;
use Yajra\DataTables\Facades\DataTables;
use App\Modules\Trainee\Models\requestsModel;

class TraineeFormController extends Controller
{
    //

    protected $role;

    public function __construct()
    {
        $this->role = new UserRolesHelper();
    }

    private function getCompetenciesData(){
        $competency_category = CompetencyCategory::orderBy("name","asc")->get();
        $competencies = [];
        foreach ($competency_category as $key => $value) {
            $competencies[$key] = [
                "option_group"=>$value->name,
                "option_relation"=>Competency::where('category_id',$value->id)->get()->toArray()
            ];
        }
        return $competencies;
    }

    public function publicTraining(){
        $user = Sentinel::check();

        /*
        if (!$this->role->inStaff()) {
            return redirect('404');
        }
        */

        $step = request()->has('step') ? request()->get('step') : 1;



        /*
        if($user->is_block != 2 && $step == 1){
            return redirect('404');
        }
        */

        $data['title'] = 'Public Training';
        $data['formData']['type'] = 'Public Training';
        $data['redirect'] = 'public-training';
        $data['competencies'] = $this->getCompetenciesData();

        $employee = $this->getEmployee();
        $data['employee'] = $employee;

        return view('trainee::request.public-training.training-step'.$step,$data);
    }
    public function assignment() {
        $user = Sentinel::check();

        /*
        if (!$this->role->inStaff()) {
            return redirect('404');
        }
        */

        $step = request()->has('step') ? request()->get('step') : 1;
        /*
        if($user->is_block != 2 && $step == 1){
            return redirect('404');
        }
        */

        $data['title'] = 'Assignment Form';
        $data['formData']['type'] = 'Assignment';
        $data['redirect'] = 'assignment';
        $data['competencies'] = $this->getCompetenciesData();
        $employee = $this->getEmployee();
        $data['employee'] = $employee;
        return view('trainee::request.assignment.assignment-step'.$step,$data);
    }

    public function datatableCompetency(){
        $category_id = CompetencyCategory::where("name",request('category'))->first()->id;
        $competency = Competency::where("category_id",$category_id)->orderBy("name","asc")->get();
        return  Datatables::of($competency)
        ->addColumn("competency",function($query){
            return $query->name;
        })
        ->addColumn("definition",function($query){
            return $query->definition;
        })
        ->make(true);

    }

    public function datatableCompetencies(){
        $competency_category = CompetencyCategory::with("competency");

        if (strlen(request('search.value'))>0){
            $competency_category = $competency_category->where("name","like","%".request('search.value')."%");
            $competency_category = $competency_category->orWhere(function($q) {
                $q->whereHas('competency', function ($query) {
                    $query->where("name","like","%".request('search.value')."%")
                    ->orWhere("definition","like","%".request('search.value')."%");
                });
            });
        }

        return  Datatables::of($competency_category)
        ->addColumn("category",function($query){
            return $query->name;
        })
        ->addColumn("competency",function($query){
            return Competency::where('category_id',$query->id)->orderBy('id','desc')->get()->toArray();
        })
        ->rawColumns(['competency'])
        ->make(true);

    }

    public function externalSpeaker() {
       $user = Sentinel::check();

       /*
       if (!$this->role->inStaff()) {
            return redirect('404');
        }
        */

        $step = request()->has('step') ? request()->get('step') : 1;

        /*
        if($user->is_block != 2 && $step == 1){
            return redirect('404');
        }
        */

        $data['title'] = 'External Speaker Form';
        $data['formData']['type'] = 'External Speaker';
        $data['redirect'] = 'external-speaker';
        $data['competencies'] = $this->getCompetenciesData();

        $employee = $this->getEmployee();
        $data['employee'] = $employee;

        return view('trainee::request.external_speaker.external_speaker-step'.$step,$data);
    }
    
    public function coaching() {
        $user = Sentinel::check();
        if (!$user->hasAccess('trainee.manage')) {
            return back();
        }

        $data['competencies'] = $this->getCompetenciesData();

      
        

        $data['formData']['type'] = 'Coaching';
        return view('trainee::request.coaching',$data);

    }

    /*
    public function mentoring() {
        $user = Sentinel::check();

        /*
        if (!$this->role->inStaff()) {
            return redirect('404');
        }
        */

        /*

        $step = request()->has('step') ? request()->get('step') : 1;



        /*
        if($user->is_block != 2 && $step == 1){
            return redirect('404');
        }
        */

        /*

        $data['title'] = 'Coaching Form';
        $data['formData']['type'] = 'Mentoring';
        $data['redirect'] = 'mentoring';
        $data['competencies'] = $this->getCompetenciesData();

        $employee = $this->getEmployee();
        $data['employee'] = $employee;

        return view('trainee::request.mentoring.mentoring-step'.$step,$data);
    }
    */

    public function mentoring() {
        $user = Sentinel::check();

        /*
        if (!$this->role->inStaff()) {
            return redirect('404');
        }
        */

        $step = request()->has('step') ? request()->get('step') : 1;





        /*
        if($user->is_block != 2 && $step == 1){
            return redirect('404');
        }
        */

        $data['title'] = 'Coaching/Mentoring Form';
        $data['formData']['type'] = 'Mentoring';
        $data['redirect'] = 'mentoring';

        

        if($step==2){

            $request_group_id=session('group_id');

           

            $check_coaching_type=requestsModel::where('requests_group_id',$request_group_id)->where('form_answer','coaching_ci_type')->get();

            if(count($check_coaching_type)>0){

                foreach ($check_coaching_type as $key) {
                     $type=$key->request_content;
                     $data['type_coaching']=$key->request_content;
                }
            }


           
          if($type=="coaching"){

                  $competency_category = CompetencyCategory::orderBy("name","asc")->whereHas('competency', function( $query ){
                      $query->where('is_coaching', '1');
                  })->get();
                 $competencies = [];
                foreach ($competency_category as $key => $value) {
            $competencies[$key] = [
                "option_group"=>$value->name,
                "option_relation"=>Competency::where('category_id',$value->id)->where('is_coaching','1')->get()->toArray()
            ];
        }

               
                 $data['competencies'] = $competencies;

          }else{

             $competency_category = CompetencyCategory::orderBy("name","asc")->whereHas('competency', function( $query ){
                      $query->where('is_coaching', '0');
                  })->get();
                 $competencies = [];
                foreach ($competency_category as $key => $value) {
            $competencies[$key] = [
                "option_group"=>$value->name,
                "option_relation"=>Competency::where('category_id',$value->id)->where('is_coaching','0')->get()->toArray()
            ];
        }


                  $data['competencies'] = $competencies;
          }



            
        }
      

        $employee = $this->getEmployee();
        $data['employee'] = $employee;

        return view('trainee::request.mentoring.mentoring-step'.$step,$data);
    }


    public function inhouse() {
        $user = Sentinel::check();
        if (!$user->hasAccess('admin.createTrainee')) {
            return back();
        }

        $step = request()->has('step') ? request()->get('step') : 1;

        $data['title'] = 'Inhouse';
        $data['formData']['type'] = 'Inhouse';
        $data['redirect'] = 'inhouse';
        $data['competencies'] = $this->getCompetenciesData();

        if($step == 2) {
            $employee = $this->getEmployee(false);
            $data['employee'] = $employee;
        }

        return view('trainee::request.inhouse.inhouse-step'.$step,$data);
    }

    public function getEmployee($isStaff = true) {
        $user = Sentinel::check();
        $employeeModel = new EmployeeModel();

        $data['staff'] = null;
        $data['mentor'] = null;

        if ($isStaff) {
//            dd('sini');
            $staff = $employeeModel->where('nik',$user->username)->get(['full_name','nik','manager_id','position']);
//            $staff = $employeeModel->get();
//            dd($staff);
            if (isset($staff[0])){
                $data['staff'] = $staff[0]->toArray();
            } else {
                $data['staff'] = null;
            }
//            if ($staff->isNotEmpty()) {
//                $data['staff'] = $staff->first()->toArray();
                $mentor = $employeeModel->where([['manager_id','=','']])->orWhere([['manager_id','=',NULL]])->get(['full_name','nik','position']);
                if ($mentor->isNotEmpty()) {
                    $data['mentor'] = $mentor->toArray();
                }
//            }
        } else {
//            if ($mentor->isNotEmpty()) {
//                $data['mentor'] = $mentor->first()->toArray();
                $mentor = $employeeModel->where([['manager_id','!=',''],['manager_id','!=',NULL]])->get(['full_name','nik','manager_id','position']);
                if ($mentor->isNotEmpty()) {
                    $data['staff'] = $mentor->toArray();
                }
//            }
        }

        return $data;

    }

    public function internalCoach() {
        $user = Sentinel::check();
        if (!$user->hasAccess('admin.createTrainee')) {
            return back();
        }

        $step = request()->has('step') ? request()->get('step') : 1;

        $data['title'] = 'Internal Training Form';
        $data['formData']['type'] = 'Internal Coach';
        $data['redirect'] = 'internal-coach';

        $employee = $this->getEmployee();
        $data['employee'] = $employee;

        return view('trainee::request.external_speaker.external_speaker-step'.$step,$data);
    }

    public function employeeRequest(Request $request){
//        if (!$request->ajax()){
//            return redirect('404');
//        }
//        dd($request->get('notin'));
        if($request->get('notin')) {
            $notIn = $request;
            array_push($notIn,Sentinel::check()->username);
        } else {
            $notIn = [Sentinel::check()->username];
        }

        $employee = EmployeeModel::where('full_name','like','%'.$request->get('name').'%')->whereNotIn('nik',$notIn)->limit(10)->get(['full_name','nik','position']);
        if($employee->isNotEmpty()){
            $employees = $employee->toArray();
            $return = [];
            foreach ($employees as $key=>$employee) {
                $return['results'][$key] = [
                  'id' => $employee['nik'],
                  'text' => $employee['full_name'],
                   'position' => $employee['position']
                ];
            }
        }else {
            return false;
        }

        return json_encode($return);
    }
}
