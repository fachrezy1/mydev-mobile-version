<?php

namespace App\Modules\Trainee\Http\Controllers;

use Illuminate\Pagination\Paginator;
use Notification;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use App\Helpers\UserRolesHelper;
use App\Modules\Trainee\Models\NotificationsModel;

use Sentinel;

class NotificationController extends Controller
{
    //

    public function view(Request $request) {
        $data['role'] = new UserRolesHelper();
        $page = (int) $request->get('page');
        $search = $request->get('search') ? $request->get('search') : null;
        $user = Sentinel::check();
        $user = User::find($user->id);
        $per_page = 5;

        if (!$page) {
            $page = 1;
        }

        $user->unreadNotifications->markAsRead();

        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });

        if ($search) {
            $notif = $user->notifications()->where('data','like', '%'.$search.'%')->paginate($per_page);
            if ($notif->isEmpty()) {
                $last_page = $notif->lastPage();

                Paginator::currentPageResolver(function () use ($last_page) {
                    return $last_page;
                });

                $notif = $user->notifications()->where('data','like', '%'.$search.'%')->paginate($per_page);
            }

        } else {
            $notif = $user->notifications()->paginate($per_page);
            if ($notif->isEmpty()) {
                $last_page = $notif->lastPage();

                Paginator::currentPageResolver(function () use ($last_page) {
                    return $last_page;
                });

                $notif = $user->notifications()->paginate(1);
            }
        }

        if ($notif->isEmpty()) {
            $data['notifications'] = null;
        } else {
            $data['notifications'] = $notif->toArray();
        }
        $data['notifications']['search'] = $search;

        return view('trainee::notifications',$data);
    }
    public function redirect(Request $request, $id){
        NotificationsModel::find($id)->update(['read_at'=>now()]);
        if(!empty($request->get('url'))){   
            return redirect($request->get('url'));
        }else{
            return redirect(url('trainee/notification'));
        }
    }
}
