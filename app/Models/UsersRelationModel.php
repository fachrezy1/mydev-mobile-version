<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersRelationModel extends Model
{
    //
    protected $primaryKey = 'id';
    protected $table = 'users_relation';
    protected $fillable = [
        'user_id','user_role','user_staff_id',
        'user_first_atasan_id','user_second_atasan_id',
        'user_group_head_id'];

    public function isRelated($where) {
        $user = $this
            ->where($where)
            ->get();

        if ($user->isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
}
