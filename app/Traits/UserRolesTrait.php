<?php

namespace App\Traits;
use Sentinel;

Trait UserRolesTrait
{

    public function isMultipleRoles() {

        $user = \Sentinel::check();

        if (count($user->roles) > 1) {
            return true;
        } else {
            return false;
        }
    }

    public function inStaff(){
        if ($this->inThisRole('staff')) {
            return true;
        }
        return false;
    }
    public function inManager() {
        if ($this->inThisRole('atasan')) {
            return true;
        }
        return false;
    }
    public function inAdmin() {
        if ($this->inThisRole('hr')) {
            return true;
        }
        return false;
    }

    private function inThisRole($roles) {
        $user = \Sentinel::check();
        if (session('multi_roles')) {
            if (session('roles') == $roles) {
                return true;
            }
        } elseif ($user->inRole($roles)) {
//            dd('staffs');
            return true;
        }
        return false;
    }
}
