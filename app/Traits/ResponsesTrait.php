<?php

namespace App\Traits;


Trait ResponsesTrait
{

//    public $allowedPost;

    function setJsonResponse($message,$status,$data = null){
        $return = [
            'type' => 'json',
            'status' => $status,
            'message' => $message,
            'data' => $data
        ];

        return session('responses',json_encode($return));
    }

    function filterReponses($data,$group) {
        $allowedPost = [
            'Public Training' => [
                'training_information' => [
                    'training_ti_title',
                    'training_ti_type_programs',
                    'training_ti_provider',
                    'training_ti_cost',
                    'training_ti_start_date',
                    'training_ti_end_date',
                ],
                'training_objective' => [
                    'training_obj_description',
                    'training_obj_venue',
                    'training_obj_job_assignment',
                    'training_obj_project_sponsor',
                    'training_obj_target_behaviour',
                    'training_obj_date_evaluation',
                    'training_obj_employee_name',
                    'training_obj_employee_nik',
                    'training_obj_employee_position',
                    'training_obj_employee_competency',
                    'training_obj_attachment',
                    'training_obj_commitment_training',
                    'training_obj_commitment_description',
                    'training_obj_start_date2',
                    'training_obj_end_date2'
                ],
            ],
            'Inhouse' => [
                'training_information' => [
                    'training_ti_title',
                    'training_ti_type_programs',
                    'training_ti_provider',
                    'training_ti_cost',
                    'training_ti_start_date',
                    'training_ti_end_date',
                    'training_obj_trainer_nik',
                    'training_obj_trainer_name',
                    'training_ti_hours'
                ],
                'training_objective' => [
                     'training_obj_moderator_name',
                    'training_obj_moderator_nik',
                    'training_obj_description',
                    'training_obj_venue',
                    'training_obj_job_assignment',
                    'training_obj_project_sponsor',
                    'training_obj_target_behaviour',
                    'training_obj_date_evaluation',
                    'training_obj_employee_name',
                    'training_obj_employee_nik',
                    'training_obj_employee_position',
                    'training_obj_employee_competency',
                    'training_obj_attachment',
                    'training_obj_commitment_training',
                    'training_obj_commitment_description'
                ],
            ],
            'Assignment' => [
                'assignment_ci' =>[
                    'assignment_ci_employee_name',
                    'assignment_ci_employee_nik',
                    'assignment_ci_line_manager_name',
                    'assignment_ci_line_manager_nik',
                    'assignment_ci_start_date',
                    'assignment_ci_end_date',
                    'assignment_dev_type',

                    'assignment_pg_project_leader',
                    'assignment_pg_project_leader_nik',
                    'assignment_pg_project_title',
                    'assignment_obj_employee_competency',
                    'assignment_pg_start_date',
                    'assignment_pg_end_date',
                    'assignment_pg_project_description',
                    'assignment_pg_development_goal',

                    'assignment_inter_group',
                    'assignment_inter_division',
                    'assignment_inter_start_date',
                    'assignment_inter_end_date',
                    'assignment_inter_mentor_name',
                    'assignment_inter_mentor_nik',
                    'assignment_inter_development_goal',

                    "assignment_gt_opco",
                    "assignment_gt_type",
                    "assignment_gt_start_date",
                    "assignment_gt_end_date",
                    "assignment_gt_purpose",
                    "assignment_gt_reason",
                    "assigment_gt_attachment",
                    "assignment_gt_reason_other"
                ],
                'coaching_cc' =>[
                    'coaching_cc_development_goal',
                    'coaching_cc_development_mtioc',
                ]
            ],
            'Mentoring' => [
                'coaching_ci' =>[
                    'coaching_ci_employee_name',
                    'coaching_ci_employee_nik',
                    'mentoring_ci_position',
                    'coaching_ci_line_manager_name',
                    'coaching_ci_line_manager_nik',
                    'coaching_ci_start_date',
                    'coaching_ci_end_date',
                    'coaching_ci_type'
                ],
                'coaching_cc' =>[
                    'coaching_cc_development_goal',
                    'coaching_cc_development_mtioc',
                ]
            ],
            'External Speaker' => [
                'mentoring_ci' => [
                    'mentoring_ci_employee_name',
                    'mentoring_ci_employee_nik',
                    'coach_ci_employee_name',
                    'coach_ci_employee_nik',
                    'mentoring_ci_line_topic',
                    'mentoring_ci_start_date',
                    'mentoring_ci_end_date',
                    'mentoring_ci_line_venue',
                    'mentoring_ci_organization',
                ]
            ],
            'Internal Coach' => [
                'mentoring_ci' => [
                    'mentoring_ci_employee_name',
                    'mentoring_ci_employee_nik',
                    'mentoring_ci_line_topic',
                    'mentoring_ci_start_date',
                    'mentoring_ci_end_date',
                    'mentoring_ci_line_venue',
                    'mentoring_ci_notes',
                ]
            ]
        ];
        $return = [];
//        dd($this->allowedPost['Internal Coach']);
        foreach ($allowedPost[$group] as $key => $allowed) {
            if (is_array($allowed)) {
                foreach ($allowed as $item) {
                    if (isset($return[$key])) {
                        array_push($return[$key],$item);
                    }else{
                        $return[$key] = [$item];
                    }
                }
            }else{
                if (isset($data[$allowed])){
                    $return[$allowed] = $data[$allowed];
                }
            }
        }
        return $return;
    }
}
