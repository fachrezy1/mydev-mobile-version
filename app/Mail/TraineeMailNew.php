<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TraineeMailNew extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
          $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

         $mail  = $this->data;
         //dd($mail);
         return $this->subject($mail['subject'])
        
        //backup
        /*
        ->from([
                'address' => env('MAIL_FROM_ADDRESS', 'mylearning.admin@indosatooredoo.com'), 
                    'name' => 'HRDevelopment (via Mydev)'
        ])
        */
    
                   ->from(env('MAIL_FROM_ADDRESS', 'mylearning.admin@indosatooredoo.com'),'HRDevelopment (via Mydev)')
                   //->cc(env('MAIL_FROM_ADDRESS'))
                   /*
                   ->cc(env('MAIL_FROM_ADDRESS', 'mylearning.admin@indosatooredoo.com'))
                   */
                   ->view('emails.email2')
                   ->with(
                    [
                        'message_header' => $mail['message_header'],
                        'form_group'     => $mail['form_group'],
                        'message_body'   => $mail['message_body'],
                        'url'            => $mail['url'],
                        'mail'            => $mail,
                        
                    ]);
    }
}
