<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Session;
use Sentinel;
use Illuminate\Support\Facades\Cache;

class RedirectIfLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Sentinel::check()){

            if (session('multi_roles')) {
                return $next($request);
            }

            $beforeUrl = Cache::get('beforeLoginUrl');
            if($beforeUrl){
                return redirect($beforeUrl);
            }

            if ($request->path() !== 'login') {
                return back();
            }

            return $next($request);

        }else{
            if(session('directLogin')){
                session()->flash('mustLogin',trans('Please login first'));
            }
            return $next($request);
        }

    }
}
