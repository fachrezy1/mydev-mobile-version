<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Route;
use Sentinel;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Cache;

class SentinelLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Sentinel::guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            }
            Cache::put('beforeLoginUrl', URL::previous(), 10);
            session()->flash('directLogin',true);
            return redirect()->route('auth.');
        }

        return $next($request);
    }
}
