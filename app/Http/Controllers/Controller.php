<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function akses_api(){
	     
	    $client = new Client(['base_uri' => 'http://188.166.184.95:7777/odsys/public/api/']);

        $response = $client->request('POST', 'login_token', ['form_params' => [
            'email' => 'rani@kabayan.id',
            'password' => '10112776'
        ]]);

        $body_token=$response->getBody();
        $get_token = json_decode($body_token, true);
        $token=($get_token["token"]);
	    $uri = 'http://188.166.184.95:7777/odsys/public';
	    //$uri = 'http://localhost:8000';
	    
	    $headers = array(
	        'Authorization' => 'Bearer '.$token
	    );
	
	    $data = array(
	        'token' => $token,
	        'uri' => $uri,
	        'headers' => $headers
	    );
	    
	    return $data;
	}
}
