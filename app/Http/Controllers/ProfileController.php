<?php

namespace App\Http\Controllers;

use App\Helpers\UserRolesHelper;
use App\Modules\Trainee\Models\requestsGroupModel;
use Illuminate\Http\Request;
use Sentinel;
use App\Modules\Auth\Models\EmployeeModel;
use App\Modules\Auth\Models\UserRolesModel;

class ProfileController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = Sentinel::check();
        $role = new UserRolesHelper();
//        $data['log'] = $this->logActivity();
        if(!$role->inAdmin()){

            $data['employee'] = EmployeeModel::where(['nik'=>$user->username])->get()->first()->toArray();
            $data['employee']['points'] = $user->point;
            $data['completed_trainee'] = requestsGroupModel::where(['is_completed'=>1,'user_id'=>$user->id])->count();
            $data['enrolled_trainee'] = requestsGroupModel::where(['user_id'=>$user->id])->count();
        } else {

            $data['employee']['full_name'] = $user->username;
            $data['employee']['points'] = $user->point;
            $data['completed_trainee'] = requestsGroupModel::where(['is_completed'=>1,'user_id'=>$user->id])->count();
            $data['enrolled_trainee'] = requestsGroupModel::where(['user_id'=>$user->id])->count();
        }
//        dd($data);
        return view('profile.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function logActivity() {
//        return Activity::where(['causer_id'=>$this->user->id])->get();
    }
}
