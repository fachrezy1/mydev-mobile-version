<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;
use Sentinel;

class LogActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        $user = Sentinel::check();

//        dd($request->toArray());
        if($request->get('draw')) {
            $searchData = $request->toArray();
            $log = Activity::where(['causer_id'=>$user->id])->
                where('description','like','%'.$searchData['search']['value'].'%')->offset($searchData['start'])->limit($searchData['length'])->orderBy('created_at','desc')->get();
            $searchKey = $searchData['search']['value'];
            if(!$searchData['search']['value']) {
                $searchKey = '';
            }
            $total = Activity::where(['causer_id'=>$user->id])->where('description','like','%'.$searchKey.'%')->orderBy('created_at','desc')->get(['id']);
//            dd(count($total));
            if($log->isEmpty()){
                $return = [
                    'draw' => $searchData['draw'],
                    'recordsTotal' => count($total),
                    'recordsFiltered' => count($total),
                ];
                $return['data'][] = [
                    '',
                    '',
                    '',
                    ''
                ];
                return json_encode($return);
            }
            $log = $log->toArray();


            $return = [
                'draw' => $searchData['draw'],
                'recordsTotal' => count($total),
                'recordsFiltered' => count($total),
            ];
        } else {
            $log = Activity::where(['causer_id'=>$user->id])->orderBy('created_at','desc')->get();

            if($log->isEmpty()){
                $return = [
                    'draw' => 1,
                    'recordsTotal' => count($log),
                    'recordsFiltered' => count($log),
                ];
                $return['data'][] = [
                    '',
                    '',
                    '',
                    ''
                ];
                return json_encode($return);
            }

            $log = $log->toArray();

            $return = [
                'draw' => 1,
                'recordsTotal' => count($log),
                'recordsFiltered' => count($log),
            ];
        }
        foreach ($log as $key=>$value) {
//            dd($log);
            $return['data'][] = [
                $value['properties']['log_type'],
                $value['description'],
                '<span class="badge '.$value['properties']['log_data']['badge_class'].'">'.$value['properties']['log_data']['status'].'</span>',
                Carbon::parse($value['properties']['log_data']['date']['date'])->format('l, d M, Y')
            ];
        }

        return json_encode($return);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
