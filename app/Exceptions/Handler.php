<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($this->isHttpException($exception)) {
            switch ($exception->getStatusCode()) {
                case '401':
                    redirect(401);
                    break;
                case '403':
                    redirect(403);
                    break;
                case '404':
                    # Not Found
                    redirect(404);
                break;
                case '419':
                    redirect(419);
                    break;
                case '429':
                    redirect(429);
                    break;
                case '409':
                    # Conflict
                    return response()->view('errors.409', [], $exception->getStatusCode());
                    break;
                case '444':
                    # No Response
                    return response()->view('errors.444', [], $exception->getStatusCode());
                    break;
                case '502':
                    # Bad Gateway
                    return response()->view('errors.502', [], $exception->getStatusCode());
                break;
                case '504':
                    # Gateway Timeout
                    return response()->view('errors.504', [], $exception->getStatusCode());
                break;
                case '500':
                    # Internal Server Error
                    redirect(500);
                    //return response()->view('errors.500', [], $exception->getStatusCode());
                break;
                case '503':
                    # Service Unavailable
                    redirect(503);
                break;
                default:
                    //return response()->view('errors.404', [], $exception->getStatusCode());
                    redirect(404);
                break;
            }
        }else{
            redirect(404);
        }
        if ($exception->getMessage()) {
            redirect(404);
        }

        return parent::render($request, $exception);
    }
}
