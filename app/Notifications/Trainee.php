<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Mail\TraineeMail as Mailable;

class Trainee extends Notification implements ShouldQueue
{
    use Queueable;
    protected $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->data = $data;
//        $this->delay($appointment->start_date_time)->subDay(1);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        // return ['database','mail'];
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail  = $this->data;
        // return view('emails.email2', compact('mail'));
        //debug_script($mail); exit;
        $mail['view'] = 'emails.email2';


        if(isset($this->data['subject'])){
            return (new Mailable($mail))
                ->to($this->data['to'])
                ->cc(env('MAIL_FROM_ADDRESS', 'mylearning.admin@indosatooredoo.com'))
                ->subject($this->data['subject'])
                ->from([
                    'address' => env('MAIL_FROM_ADDRESS', 'mylearning.admin@indosatooredoo.com'), 
                    'name' => 'HRDevelopment (via Mydev)'
                ])->markdown($mail['view'],['mail'=>$mail]);
        }

        //handling error notification log
        return (new Mailable($mail))
            ->to('yusuf.handian@gmail.com')
            ->subject('Error notification')
            ->from('yusuf.handian@gmail.com')
            ->markdown($mail['view'],['mail'=>$mail]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
            'message_body' => isset($this->data['message_body']) ? $this->data['message_body'] : '',
            'message_title' => isset($this->data['message_title']) ? $this->data['message_title'] : '',
            'message_date' => isset($this->data['message_date']) ? $this->data['message_date'] : '',
            'url' => isset($this->data['url']) ? $this->data['url'] : '',
            'type' => $this->data['type'],
        ];
    }
}
