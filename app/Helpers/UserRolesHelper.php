<?php

namespace App\Helpers;
use Sentinel;

use App\Modules\Auth\Models\UserModel;
use App\Modules\Auth\Models\EmployeeModel;

class UserRolesHelper
{

    public function isMultipleRoles() {
        $user = \Sentinel::check();

        if (count($user->roles) > 1) {
            return true;
        } else {
            return false;
        }
    }

    public function inStaff($current_id = null){

        if ($this->inThisRole('staff',$current_id)) {
            return true;
        }
        return false;
    }
    public function inManager($current_id = null) {
        if ($this->inThisRole('atasan',$current_id)) {
            return true;
        }
        return false;
    }
    public function inAdmin($current_id = null) {
        if ($this->inThisRole('hr',$current_id)) {
            return true;
        }
        return false;
    }

    public function isRoleActive($role,$current_id){
        $user = \Sentinel::check();

        if (session('roles') == $role && $user->id == $current_id) {
            return true;
        } else {
            return false;
        }
    }

    private function inThisRole($roles,$current_id = null) {
        if ($current_id) {
            $user = \Sentinel::findById($current_id);
        } else {
            $user = \Sentinel::check();
        }
        if (session('multi_roles') && !$current_id) {
            if (session('roles') == $roles) {
                return true;
            }
        } elseif (isset($user->roles) && $user->inRole($roles)) {
            return true;
        }
        return false;
    }

    public function isRelated($user_id,$current_nik){
        $role = $this->getLeaders($user_id);

        if ($role['user_type'] == 'vp') {
            $atasan = $role['svp'];
            return true;
        } elseif ($role['user_type'] == 'svp') {
            $atasan = $role['dir'];
            return true;
        } elseif ($role['user_type'] != 'dir' || $role['user_type'] != 'presdir') {
            if(!empty($role['vp'])){
                $atasan = $role['vp'];
                return true;
            }elseif(empty($role['vp'])){
                $atasan = $role['svp'];
                return true;
            }elseif(empty($role['svp'])){
                $atasan = $role['dir'];
                return true;
            }
        }        

        if ($atasan['nik'] == $current_nik){
            return true;
        }
        return false;
    }

    public function getLeaders($user_id){
        $leader_list = [
            'user_type' => null,
            'dir' => null,
            'svp' => null,
            'vp' => null
        ];

        $user = \Sentinel::findById($user_id);
        if($user){
            $employee_data = EmployeeModel::where('email', $user->email)->first();
            if($employee_data){
                $leader_list['user_type'] = strtolower($employee_data->job_key_short);

                if($leader_list['user_type'] == 'cxo'){
                    $leader_list['user_type'] = 'dir';
                }

                if (!$this->inStaff($user_id) || $this->inManager()){
                    //jika tidak punya role staff, ambil relasi ke bawahnya saja
                    $manager_tree = $this->getManagerTree($user_id,$employee_data->nik);
                    $leader_list = array_merge($leader_list, $manager_tree);
                } else {
                    $manager_tree = $this->getManagerTree($user_id,$employee_data->manager_id);
                    $leader_list = array_merge($leader_list, $manager_tree);
                }
            }
        }

        if(isset($leader_list['cxo'])){
            $leader_list['dir'] = $leader_list['cxo'];
        }

        return $leader_list;
    }

    private function getManagerTree($user_id,$nik, &$manager_tree = []){
        $employee_data = EmployeeModel::where('nik', $nik)->first();

        //jika dalam role staff dan sedang tidak dalam role manager, jangan ambil relasi ke atasnya
        if($this->inStaff($user_id) && !$this->isRoleActive('atasan',$user_id)) {
            if($employee_data){
                $user_data = UserModel::where('email', $employee_data->email)->first();
                $job_key_short = strtolower($employee_data->job_key_short);

                $manager_tree[$job_key_short] = [
                    'user_id' => ($user_data) ? $user_data->id : 0,
                    'nik' => $employee_data->nik,
                    'name' => ucwords(strtolower($employee_data->full_name)),
                ];

                if($job_key_short != 'dir'){
                    //karena ada yang manager id == nik maka harus ditambahkan validasi agar tidak infinite loop

                    if($employee_data->manager_id !== $employee_data->nik)
                    $this->getManagerTree($user_id,$employee_data->manager_id, $manager_tree);
                }
            }
        }
        // dd($this->isRoleActive('hr',$user_id));
        if ($this->isRoleActive('atasan',$user_id)) {
            //jika manager, ambil relasi ke bawahnya saja
//            dd($nik);
            $employee_data = EmployeeModel::where('manager_id', $nik)->get(['nik','full_name','job_key_short']);
           // dd($employee_data->toArray());
            if($employee_data->isNotEmpty()) {
                $employee_data = $employee_data->toArray();

                $user_data = UserModel::get()->groupBy('username');
//                dd($user_data);
//            dd($user_data['81147033'][0]->id);
                $employee_data_staff = [];
                foreach ($employee_data as $item) {
                    $manager_tree[strtolower($item['job_key_short'])][] = [
                        'user_id' => isset($user_data[$item['nik']]) ? $user_data[$item['nik']][0]->id : 0,
                        'nik' => $item['nik'],
                        'name' => ucwords(strtolower($item['full_name'])),
                    ];
                    $temp = EmployeeModel::where('manager_id',$item['nik'])->get(['nik','full_name','job_key_short']);



                    if($temp->isNotempty()){
                        foreach ($temp->toArray() as $item_staff) {
//                            dd($user_data['95177765'][0]->id);
                            $manager_tree[strtolower($item_staff['job_key_short'])][] = [
                                'user_id' => isset($user_data[$item_staff['nik']]) ? $user_data[$item_staff['nik']][0]->id : 0,
                                'nik' => $item_staff['nik'],
                                'name' => ucwords(strtolower($item_staff['full_name'])),
                            ];
                        }
                    }
                }
//                dd($manager_tree);
                return $manager_tree;
            }
        }

        return $manager_tree;
    }

    //untuk ngambil nama manager id dengan email
    public function manag_id($email){
        //start
        $manag = [
            'manager' => '-',
        ];
        $count_emp = 0;
        $count_user = 0;
        $count_user2 = 0;
        if ($email != '' || $email != NULL) {
             $employee = EmployeeModel::where('email', $email)->first();
             //$count_emp = $employee->count();
             //dd($employee->manager_id);
             if (isset($employee)) {
                 $count_emp = $employee->count();
             }
             
             
             if ($count_emp != 0) {
                 $managerid = $employee->manager_id;
                 if ($managerid != NULL || $managerid != '') {
                     //$user = UserModel::where('username',$managerid)->first();
                     $user = EmployeeModel::where('nik',$managerid)->first();
                     
                     //$count_user = $user->count();
                     if (isset($user)) {
                        $count_user = $user->count();
                     }
                     //dd($user);
                     //$count_user = count($user);
                     if ($count_user != 0) {
                        if (isset($user->full_name)) {
                            $name_manager = ucwords(strtolower($user->full_name));
                            
                            if ($name_manager == '' || $name_manager == NULL) {
                                $name_manager = 'Nama belum diinput di database';
                            }

                        }else{
                            $name_manager = 'Nama belum diinput di database';
                        }
                     }else{
                        $user2 = UserModel::where('username',$managerid)->first();
                        //count user2 select
                        if (isset($user2)) {
                            $count_user2 = $user2->count();
                        }

                        if ($count_user2 != 0) {
                           if (isset($user2->first_name)) {
                               $name_manager = ucwords(strtolower($user2->first_name));
                               
                               if ($name_manager == '' || $name_manager == NULL) {
                                   $name_manager = 'Nama belum diinput di database';
                               }
                               //
                           }else{
                               $name_manager = 'Nama belum diinput di database';
                           }
                           
                        }else{
                            $name_manager = 'Nama belum diinput di database';
                        }
                     }
                 }else{
                     $name_manager = 'Nama belum diinput di database';
                 }
             }else{
                 $name_manager = 'Nama belum diinput di database';
             }
        }else{
             $name_manager = 'Nama belum diinput di database';
        }
        //end
        $manag['manager'] = $name_manager;
        return $manag;
    }
}
