<?php

namespace App\Helpers;

class ActivityHelper
{
    public function logActivity($causer=null,$properties,$desc) {

        if($causer) {
            return activity()
                ->by($causer)
                ->withProperties($properties)
                ->log($desc);
        } else {
            return activity()
                ->withProperties($properties)
                ->log($desc);
        }


    }
}
