<?php

use App\User;
use App\Notifications\Trainee;
use App\Modules\Trainee\Models\users_point_history_model;

if (! function_exists('debug_script')) {
    function debug_script($data) {
        echo '<pre>'; print_r($data);
    }
}

if (! function_exists('limit_words')) {
    function limit_words($string, $limit) {
        if(strlen($string) > $limit){
            return substr($string, 0, $limit).'...';
        }else{
            return $string;
        }
    }
}

if (! function_exists('format_sentence')) {
    function format_sentence($data) {
        return ucwords(strtolower($data));
    }
}

if (! function_exists('show_course_title')) {
    function show_course_title($form_group_id, $form_data) {
        switch($form_group_id){
            case 1 :
                if($form_data['type'] == 'project'){
                    $title = isset($form_data['project_title']) ? limit_words($form_data['project_title'], 19) : '';
                }else{
                    $title = isset($form_data['development_goal']) ? limit_words($form_data['development_goal'], 19) : '';
                }
            break;

            case 2 :
                $title = isset($form_data['development_goal']) ? limit_words($form_data['development_goal'], 19) : '';
                break;
                
            case 3 :
                $title = isset($form_data['title']) ? limit_words($form_data['title'], 19) : '';
            break;
            
            case 4 :
                $title = isset($form_data['line_topic']) ? limit_words($form_data['line_topic'], 19) : '';
            break;
        }

        return $title;
    }
}

if (! function_exists('get_user_point')) {
    function get_user_point($user_id) {
        $point_history = 0;
        $point_history = users_point_history_model::where(['user_id'=>$user_id])->sum('point');
        return $point_history;
    }
}

if (! function_exists('notify_admin')) {
    function notify_admin($messages) {
        $admin = User::find(5);
        $messages['to'] = $admin->email;
        $admin->notify(new Trainee($messages));
    }
}