jQuery(function($){
  $('#traineeDetail').on('shown.bs.modal', function (e) {
    $('.scroll-content').slimScroll({

      height: '330px',
      size: '5px',
      position: 'right',
      color: '#fcd401',
      alwaysVisible: true,
      distance: '0px',

      railVisible: true,
      railColor: '#222',
      railOpacity: 0.1,

      allowPageScroll: true,
      disableFadeOut: false
    });
  })


  $(".trainee-option").owlCarousel({
    items: 5,
    loop: true,
    margin: 10,
    nav: true,
    dots: false,
    smartSpeed: 900,
    autoplay: true,
    navigationText: ["<i class='mdi mdi-chevron-left'></i>", "<i class='mdi mdi-chevron-right'></i>"],
    responsive: {
      0: {
        items: 2
      },
      990: {
        items: 3
      },
      1023: {
        items: 5
      }
    }
  });


});

