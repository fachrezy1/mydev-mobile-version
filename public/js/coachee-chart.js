jQuery(function($){

  var barChartData = {
    "series": [2014, 2015, 2016],
    "labels": [
      "Lille", "Maubeuge", "Poissy SG", "Morlaix", "Sud Gironde", "Elbeuf", "Douarnenez",
      "Dieppe", "Le Havre", "Limoges", "Lorient", "Fécamp", "Paris", "Aurillac", "Etampes", "Quimperlé",
      "Meulan", "Fougères", "Melun", "Quimper", "Roscoff", "Saint-Malo", "Redon", "La Roche-sur-Yon", "Villefranche"
    ],
    "data": [
      [147, 49, 26, 22, 45, 11, 29, 3, 22, 24, 4, 18, 22, 21, 16, 4, 19, 12, 0, 21, 15, 6, 15, 14, 9],
      [130, 58, 59, 49, 20, 37, 20, 40, 30, 19, 33, 34, 20, 11, 16, 21, 8, 15, 24, 5, 7, 12, 7, 8, 8],
      [31, 24, 8, 14, 9, 17, 13, 19, 9, 17, 19, 3, 7, 2, 2, 8, 5, 5, 7, 1, 3, 7, 2, 0, 3]
    ]
  };

  var stackedBarChartData = {
    labels: barChartData.labels,
    datasets: [{
      label: barChartData.series[0],
      backgroundColor: "rgba(249, 108, 110, 0.8)",
      borderColor: "rgba(249, 108, 110, 1)",
      data: barChartData.data[0]
    }]
  };

  stackedBarChartData.datasets.push({
    label: barChartData.series[1],
    backgroundColor: "rgba(179, 153, 255, 0.9)",
    borderColor: "rgba(179, 153, 255, 1)",
    data: barChartData.data[1]
  });

  stackedBarChartData.datasets.push({
    label: barChartData.series[2],
    backgroundColor: "rgba(128, 191, 255, 0.9)",
    borderColor: "rgba(128, 191, 255, 1)",
    data: barChartData.data[2]
  });

  var configBarChart = {
    type: 'bar',
    data: stackedBarChartData,
    options: {
      maintainAspectRatio: false,
      tooltips: {
        mode: 'label',
        bodySpacing: 6,
        titleMarginBottom: 10,
        footerMarginTop: 8,
        titleFontSize: 14,
        bodyFontSize: 14,
        footerFontSize: 14,
        callbacks: {
          footer: function(tooltipItem /*, data*/ ) {
            var total = 0;
            tooltipItem.forEach(function(element /*, index, array*/ ) {
              total += element.yLabel;
            });
            return 'Total: ' + total;
          }
        }
      },
      scales: {
        xAxes: [{
          stacked: true,
          ticks: {
            maxRotation: 60,
            autoSkip: false
          },
          gridLines: {
            color: "rgba(0, 0, 0, 0.06)",
            zeroLineColor: "rgba(0,0,0,0.1)"
          }
        }],
        yAxes: [{
          stacked: true,
          gridLines: {
            color: "rgba(0, 0, 0, 0.06)",
            zeroLineColor: "rgba(0,0,0,0.1)"
          }
        }]
      },
      legend: {
        display: false
      },
      responsive: true
    }
  };

  var ctxBarChart = document.getElementById("barChart").getContext("2d");
  new Chart(ctxBarChart, configBarChart);


  var optionsFour = {
    type: 'line',
    data: {
      labels: ["January", "February", "March", "April", "May", "June", "July"],
      datasets: [{
        label: "My First dataset1",
        data: [65, 0, 80, 81, 56, 85, 40],
        fill: false
      }]
    }

  };

  var optionsFive = {
    type: 'bar',
    data: {
      labels: ["January", "February", "March", "April", "May", "June", "July"],
      datasets: [{
        label: "My First dataset",
        data: [65, 0, 80, 81, 56, 85, 40],
        fill: false
      },
        {
          label: "My Second dataset",
          data: [95, 10, 0, 11, 76, 65, 30],
          fill: false
        }]
    },
    options: {
      legend: {
        display: true
      },
      responsive: true
    }

  };


  var optionsThree = {
    type: 'line',

    data: {
      labels: ["May 1", "May 2", "May 3", "May 4", "May 5", "May 6",
        "May 7", "May 8", "May 9", "May 10", "May 11", "May 12"],
      datasets: [
        {
          label: "Blue",
          fill: true,
          backgroundColor: "rgba(32, 162, 219, 0.3)",
          data: [210, 220, 260, 350, 550, 550, 560, 590, 600, 610, 610, 620]
        },
        {
          label: "Red",
          fill: true,
          backgroundColor: "rgba(196, 93, 105, 0.3)",
          data: [100, 120, 130, 140, 160, 230, 270, 400, 410, 420, 430, 450]
        }
      ]
    },
    options: {
      scales: {
        xAxes: [{
          stacked: true,
        }],
        yAxes: [{
          stacked: true,
        }]
      },
      legend: {
        display: false,
      },
      responsive: true
    }
  }




  var optionsOne = {
    type: 'pie',

    data: {
      labels: ["Green", "Blue"],
      datasets: [{
        backgroundColor: [
          "#2ecc71",
          "#3498db"
        ],
        data: [19, 12]
      }]
    },
    options: {
      legend: {
        display: false
      },
      responsive: true
    }
  }


  var optionsTwo = {
    type: 'line',
    data: {
      labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
      datasets: [
        {
          label: 'Training #1',
          data: [12, 19, 3, 5, 2, 3],
          borderWidth: 1
        },
        {
          label: 'Training #2',
          data: [7, 11, 5, 8, 3, 7],
          borderWidth: 1
        }
      ]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            reverse: false
          }
        }]
      }
    }
  }



  var ctxOne = document.getElementById('chartOneContainer').getContext('2d');
  new Chart(ctxOne, optionsOne);

  var ctxTwo = document.getElementById('chartTwoContainer').getContext('2d');
  new Chart(ctxTwo, optionsTwo);

  var ctxThree = document.getElementById('chartThreeContainer').getContext('2d');
  new Chart(ctxThree, optionsThree);



  var ctxFive = document.getElementById('chartFiveContainer').getContext('2d');
  new Chart(ctxFive, optionsFive);


});

