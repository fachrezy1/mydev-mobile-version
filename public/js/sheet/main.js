window.valid_data = 0;
window.importTable = $('#importTable').DataTable( {
    // responsive: true,
    scrollX: true,
    bFilter: false,
    paging: false,
    "pageLength": 5,
    "bLengthChange": false

} );
/** drop target **/
var _target = document.getElementById('drop');
var _file = document.getElementById('import');

/** Spinner **/
var spinner;

var _workstart = function() { spinner = new Spinner().spin(_target); }
var _workend = function() { spinner.stop(); }

/** Alerts **/
var _badfile = function() {
    console.log('This file does not appear to be a valid Excel file.  If we made a mistake, please send this file to <a href="mailto:dev@sheetjs.com?subject=I+broke+your+stuff">dev@sheetjs.com</a> so we can take a look.', function(){});
};

var _pending = function() {
    console.log('Please wait until the current file is processed.', function(){});
};

var _large = function(len, cb) {
    console.log("This file is " + len + " bytes and may take a few moments.  Your browser may lock up during this process.  Shall we play?", cb);
};

var _failed = function(e) {
    console.log(e, e.stack);
    console.log('We unfortunately dropped the ball here.  Please test the file using the <a href="/js-xlsx/">raw parser</a>.  If there are issues with the file processor, please send this file to <a href="mailto:dev@sheetjs.com?subject=I+broke+your+stuff">dev@sheetjs.com</a> so we can make things right.', function(){});
};


var _onsheet = function(json, sheetnames, select_sheet_cb) {
    // document.getElementById('footnote').style.display = "none";

    // make_buttons(sheetnames, select_sheet_cb);

    /* show grid */
    // _grid.style.display = "block";
    // _resize();

    /* set up table headers */
    let import_list = $('#import-list');
    let import_input = $('.import-input');

//     $.post( baseUrl+'/dashboard/employee/find', {'data':json} )
//         .done(function( data ) {
//             alert( "Data Loaded: " + data );
// });
    let invalid_nik = null;
    let invalid_count = 0;
    let total_data = json.length - 1;

    json.forEach(function(value,key) {
        if (key !== 0) {

            $.ajax({
                method: "GET",
                url: baseUrl+"/dashboard/employee-check",
                data: { nik: value[1] }
            })
            .done(function( response ) {
                // valid_data = response;

            $(import_input).append('<input type="hidden" name="training_obj_employee_name[]" value="'+value[0]+'">')
            $(import_input).append('<input type="hidden" name="training_obj_employee_nik[]" value="'+value[1]+'">')
            $(import_input).append('<input type="hidden" name="training_obj_employee_position[]" value="'+value[2]+'">')
            // console.log(row,'row')

            if(response != 0){
                importTable.row.add(value).draw(false);
            } else {
                invalid_count++;
                if(invalid_nik !== null) {
                    invalid_nik = invalid_nik + value[1] + ', ';
                    // $('.invalid-nik').
                    $('.invalid-nik-target').html(invalid_nik.replace("undefined ",""));
                } else {
                    $('.invalid-nik').removeClass('d-none');

                    invalid_nik = 'Invalid NIK : '.invalid_nik+ ' ' + value[1] + ', ';

                    $('.invalid-nik-target').html(invalid_nik.replace("undefined ",""));
                }
            }
            });
        }
    });
    console.log(invalid_count);
    $('.import-table').removeClass('d-none');
    if(total_data != invalid_count) {
        $('.employees-form').find('input').removeAttr('required');
    }
    // var L = 0;
    // json.forEach(function(r) { if(L < r.length) L = r.length; });
    // console.log(L);
    // for(var i = json[0].length; i < L; ++i) {
    //     json[0][i] = "";
    // }
    //
    // /* load data */
    // cdg.data = json;
};

/** Drop it like it's hot **/
DropSheet({
    file: _file,
    // drop: _target,
    on: {
        workstart: _workstart,
        workend: _workend,
        sheet: _onsheet,
        foo: 'bar'
    },
    errors: {
        badfile: _badfile,
        pending: _pending,
        failed: _failed,
        large: _large,
        foo: 'bar'
    }
})
