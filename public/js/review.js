jQuery(function($){
  $('[data-toggle="tooltip"]').tooltip()


  $('.datetimepicker2').datetimepicker({
      format: 'DD/MM/Y'
  });

  $('.btn-remove').on('click', function() {
    $('.review-section').find('.review-item').last().remove();
  });


  $('.btn-more').on('click', function() {
    let session = $('.review-item').length;

    if (session == 6) {
        alert('Maximum 6 Sessions!');
        return false;
    }

    let clone = $('.review-item').first().clone();



    clone.find('input').val('');
    clone.find('textarea').val('');
    if (clone.find('.datetimepicker2').length > 0 ) {
        clone.find('.datetimepicker2').datetimepicker({
            format: 'DD/MM/Y'
        })
    }

    if (clone.find('.start-date').length > 0) {
        clone.find('.start-date').datetimepicker({
            format: 'DD/MM/Y'
        }).on('dp.change', function (e) {
            var incrementDay = moment(new Date(e.date));
            incrementDay.add(1, 'days');
            clone.find('.end-date').data('DateTimePicker').minDate(incrementDay);
            $(this).data("DateTimePicker").hide();
        });
    }
    if (clone.find('.end-date').length > 0) {
        clone.find('.end-date').datetimepicker({
            format: 'DD/MM/Y'
        }).on('dp.change', function (e) {
            var decrementDay = moment(new Date(e.date));
            decrementDay.subtract(1, 'days');
            clone.find('.start-date').data('DateTimePicker').maxDate(decrementDay);
            $(this).data("DateTimePicker").hide();
        });
    }

    // ini tambahan
    var value = $('#data_flag_clone').val();
    //alert(value);
    clone.find('#flag1').val(value);

    clone.find('input').prop('disabled',false);
    clone.find('textarea').prop('disabled',false);

    clone.find('.progress').attr('onkeyup','progress'+ parseInt(session+1) +'(this)');
    clone.find('.listaction').attr('onkeyup','list'+ parseInt(session+1) +'(this)');
    clone.find('.curent').attr('onkeyup','curent'+ parseInt(session+1) +'(this)');
    clone.find('.coachgoal').attr('onkeyup','coach'+ parseInt(session+1) +'(this)');

    clone.find('.co_goal').addClass('count_goal'+ parseInt(session+1));
    clone.find('.co_curent').addClass('count_current'+ parseInt(session+1));
    clone.find('.co_list').addClass('count_list'+ parseInt(session+1));
    clone.find('.co_progress').addClass('count_progress'+ parseInt(session+1));

    clone.find('.co_goal').html('0');
    clone.find('.co_curent').html('0');
    clone.find('.co_list').html('0');
    clone.find('.co_progress').html('0');
    // end tambahan

    clone.find('.btn-collapse').attr('data-target','#collapse'+parseInt(session+1) );
    clone.find('.target-collapse').attr('id','collapse'+parseInt(session+1) );

    clone.find('.target-collapse').removeClass('collapse');
    clone.find('.target-collapse').addClass('collapse');
    clone.find('.target-collapse').addClass('show');


    let sessionItem = session == 1 ? '<span class="session_number">' + parseInt(session+1) + 'nd' + '</span> session' : (session == 2 ?  '<span class="session_number">' + parseInt(session+1) + 'rd' + '</span> session' :  '<span class="session_number">' + parseInt(session+1) + 'th' + '</span> session');

    sessionItem = sessionItem + '<button class="btn btn-link btn-collapse" type="button" data-toggle="collapse" data-target="#collapse'+parseInt(session+1)+'" aria-expanded="true" aria-controls="collapse'+parseInt(session+1)+'">Hide</button>';

    clone.find('.sub-title').html(sessionItem);
    clone.find('input').attr('required','required');
    clone.find('textarea').attr('required','required');
    clone.appendTo('.review-section');
  });

    // $('.start-date').datetimepicker().on('dp.change', function (e) {
    //     console.log('change')
    //     var incrementDay = moment(new Date(e.date));
    //     incrementDay.add(1, 'days');
    //     $('.end-date').data('DateTimePicker').minDate(incrementDay);
    //     $(this).data("DateTimePicker").hide();
    // });
    //
    // $('.end-date').datetimepicker().on('dp.change', function (e) {
    //     var decrementDay = moment(new Date(e.date));
    //     decrementDay.subtract(1, 'days');
    //     $('.start-date').data('DateTimePicker').maxDate(decrementDay);
    //     $(this).data("DateTimePicker").hide();
    // });
});
