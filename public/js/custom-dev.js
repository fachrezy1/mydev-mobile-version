var excludeSearch = [];

function matchCustom(params, data) {
    data.parentText = data.parentText || "";

    // Always return the object if there is nothing to compare
    if ($.trim(params.term) === '') {
      return data;
    }

    // Do a recursive check for options with children
    if (data.children && data.children.length > 0) {
      // Clone the data object if there are children
      // This is required as we modify the object to remove any non-matches
      var match = $.extend(true, {}, data);

      // Check each child of the option
      for (var c = data.children.length - 1; c >= 0; c--) {
        var child = data.children[c];
        child.parentText += data.parentText + " " + data.text;

        var matches = matchCustom(params, child);

        // If there wasn't a match, remove the object in the array
        if (matches == null) {
          match.children.splice(c, 1);
        }
      }

      // If any children matched, return the new object
      if (match.children.length > 0) {
        return match;
      }

      // If there were no matching children, check just the plain object
      return matchCustom(params, match);
    }

    // If the typed-in term matches the text of this term, or the text from any
    // parent term, then it's a match.
    var original = (data.parentText + ' ' + data.text).toUpperCase();
    var term = params.term.toUpperCase();


    // Check if the text contains the term
    if (original.indexOf(term) > -1) {
      return data;
    }

    // If it doesn't contain the term, don't return anything
    return null;
}

$(document).ready(function(){
    window.baseUrl = $('[name=baseUrl]').val();
    // console.log(baseUrl+'/trainee/request/employee-request');

    $('.select2').select2({
    	dropdownParent: $('body')
    });
    $('.select2-tags').select2({
        maximumSelectionLength: 3
    });


    $('.select2-comp').select2({
        maximumSelectionLength: 3,
        matcher: function(params, data){
            return matchCustom(params, data);
        }
    });

    $('.select-employee').select2({
        theme: "bootstrap",
        minimumInputLength: 3,
        ajax: {
            url: baseUrl+'/trainee/request/employee-request',
            dataType: 'json',
            // data:{},
            type:'get',
            delay: 250,
            data: function (params) {
                return {
                    name: params.term,
                    notin: $('[name="training_obj_employee_nik"]').val() ? $('[name="training_obj_employee_nik"]').val() : ''
                }
            },
            placeholder: 'Search for a repository',
            // escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        }
    });
    $('.select-employee').on("select2:open", function(e) {
        if(typeof results !== 'undefined'){
            $('.select2-results .select2-results__option:contains('+results+')').hide();
        }
    });

    $('.select-employee').on('change', function() {
        var name = $(".select-employee option:selected").text();
    });

    $('.select-employee').on("select2:select", function(e) {

        let nik = e.params.data.id;
        let position = e.params.data.position;

        $('.select-employee-name').val(e.params.data.text);

        $('.nik_coachee').val(nik);

        if($('.position_employee').length > 0) {
            $('.position_employee').val(position)
        }
    });

    // add js data >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    $('.select-employee2').select2({
        theme: "bootstrap",
        minimumInputLength: 3,
        ajax: {
            url: baseUrl+'/trainee/request/employee-request',
            dataType: 'json',
            // data:{},
            type:'get',
            delay: 250,
            data: function (params) {
                return {
                    name: params.term,
                    notin: $('[name="training_obj_employee_nik"]').val() ? $('[name="training_obj_employee_nik"]').val() : ''
                }
            },
            placeholder: 'Search for a repository',
            // escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        }
    });
    $('.select-employee2').on("select2:open", function(e) {
        if(typeof results !== 'undefined'){
            $('.select2-results .select2-results__option:contains('+results+')').hide();
        }
    });

    $('.select-employee2').on('change', function() {
        var name = $(".select-employee option:selected").text();
    });

    $('.select-employee2').on("select2:select", function(e) {

        let nik = e.params.data.id;
        let position = e.params.data.position;

        $('.select-employee-name2').val(e.params.data.text);

        $('.nik_coachee2').val(nik);

        if($('.position_employee2').length > 0) {
            $('.position_employee2').val(position)
        }
    });

     $('.select-employee3').select2({
        theme: "bootstrap",
        minimumInputLength: 3,
        ajax: {
            url: baseUrl+'/trainee/request/employee-request',
            dataType: 'json',
            // data:{},
            type:'get',
            delay: 250,
            data: function (params) {
                return {
                    name: params.term,
                    notin: $('[name="training_obj_employee_nik"]').val() ? $('[name="training_obj_employee_nik"]').val() : ''
                }
            },
            placeholder: 'Search for a repository',
            // escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        }
    });
    $('.select-employee3').on("select2:open", function(e) {
        if(typeof results !== 'undefined'){
            $('.select2-results .select2-results__option:contains('+results+')').hide();
        }
    });

    $('.select-employee3').on('change', function() {
        var name = $(".select-employee option:selected").text();
    });

    $('.select-employee3').on("select2:select", function(e) {

        let nik = e.params.data.id;
        let position = e.params.data.position;

        $('.select-employee-name3').val(e.params.data.text);

        $('.nik_coachee3').val(nik);

        if($('.position_employee3').length > 0) {
            $('.position_employee3').val(position)
        }
    });


    // end add >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    var submitButton = '';

    $('#files').change(function(e){
        var fileName = e.target.files[0].name;
        let filetype = e.target.files[0].type;
        let nameFiles = $('.name-files');

        if( (filetype.search('application/pdf') == '-1') && (filetype.search('image') == '-1')) {
            submitButton = $('[type="submit"]')
                .attr('disabled','disabled')
                .attr('type','button')
                .data('isdisabled','1');
            nameFiles.addClass('text-danger');
        } else {
            nameFiles.removeClass('text-danger');
            nameFiles.html(fileName)
            submitButton.removeAttr('disabled').attr('type','submit');
        }
    });

    $(".field-left").addClass("d-none");

    $('#gtmattachment').change(function(e){
        var fileName = e.target.files[0].name;
        let filetype = e.target.files[0].type;
        let nameFiles = $('.name-files');
        var mimesType = new Array("", "application/x-rar-compressed", "application/octet-stream", "application/zip", "application/octet-stream", "application/x-zip-compressed","multipart/x-zip","application/x-rar");
        if(mimesType.indexOf(filetype) != -1) {
            nameFiles.removeClass('text-danger');
            nameFiles.html(fileName)
            submitButton.removeAttr('disabled').attr('type','submit');
        } else {
            submitButton = $('[type="submit"]')
                .attr('disabled','disabled')
                .attr('type','button')
                .data('isdisabled','1');
            nameFiles.addClass('text-danger');
        }
    });

    $('.files').change(function (e) {
        var fileName = e.target.files[0].name;
        let filetype = e.target.files[0].type;
        let nameFiles = $(this).siblings('.name-files');

        if( (filetype.search('application/pdf') == '-1') && (filetype.search('image') == '-1')) {
            submitButton = $('[type="submit"]')
                .attr('disabled','disabled')
                .attr('type','button')
                .data('isdisabled','1');
            nameFiles.addClass('text-danger');
        } else {
            nameFiles.removeClass('text-danger');
            nameFiles.html(fileName)
            submitButton.removeAttr('disabled').attr('type','submit');
        }
    })

    // Datatable reset on tabs
    $.fn.DataTable.ext.pager.numbers_length = 5
    $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    } );
    $(window).resize(function(event) {
    	$.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    });
});

$(function () {
    $('.start-date,.end-date').datetimepicker({
        useCurrent: false,
        format: 'DD/MM/Y',
        allowInputToggle: true
    });
    $('.start-date').datetimepicker().on('dp.change', function (e) {
        $('.end-date').val();
        var incrementDay = moment(new Date(e.date));
        incrementDay.add(0, 'days');
        $('.end-date').data('DateTimePicker').minDate(incrementDay);
        $(this).data("DateTimePicker").hide();
    });

    $('.end-date').datetimepicker().on('dp.change', function (e) {
        var decrementDay = moment(new Date(e.date));
        decrementDay.subtract(0, 'days');
        $('.start-date').data('DateTimePicker').maxDate(decrementDay);
        $(this).data("DateTimePicker").hide();
    });

    $('.date-input button').click(function(event){
        event.preventDefault();
        $(this).closest(".date-input").find('input').focus();
    });


    $('.start-date2,.end-date2').datetimepicker({
        useCurrent: false,
        format: 'DD/MM/Y'
    });
    $('.start-date2').datetimepicker().on('dp.change', function (e) {
        $('.end-date2').val();
        var incrementDay = moment(new Date(e.date));
        incrementDay.add(0, 'days');
        $('.end-date2').data('DateTimePicker').minDate(incrementDay);
        $(this).data("DateTimePicker").hide();
    });

    $('.end-date2').datetimepicker().on('dp.change', function (e) {
        var decrementDay = moment(new Date(e.date));
        decrementDay.subtract(0, 'days');
        $('.start-date2').data('DateTimePicker').maxDate(decrementDay);
        $(this).data("DateTimePicker").hide();
    });

    $('.start-date3,.end-date3').datetimepicker({
        useCurrent: false,
        format: 'DD/MM/Y'
    });
    $('.start-date3').datetimepicker().on('dp.change', function (e) {
        $('.end-date3').val();
        var incrementDay = moment(new Date(e.date));
        incrementDay.add(0, 'days');
        $('.end-date3').data('DateTimePicker').minDate(incrementDay);
        $(this).data("DateTimePicker").hide();
    });

    $('.end-date3').datetimepicker().on('dp.change', function (e) {
        var decrementDay = moment(new Date(e.date));
        decrementDay.subtract(0, 'days');
        $('.start-date3').data('DateTimePicker').maxDate(decrementDay);
        $(this).data("DateTimePicker").hide();
    });

});

$('.reject_button_trainee').on('click',function () {
    $('#rejected_user_id').val($(this).data('userid'))
    $('#rejected_request_id').val($(this).data('requestgroup'))
})

$('.rating-modal').on('click',function () {
    $('.rating_staff_id').val($(this).data('staffid'));
    $('.rating_mentor_id').val($(this).data('mentorid'));
    $('.rating_request_group').val($(this).data('requestgroup'));
})



$('.counter-text').change(function countText() {
    $(this).next('.text-count').find('.counter-text-value').html($(this).val().length);
});
$('.counter-text').keyup(function countText() {
    $(this).next('.text-count').find('.counter-text-value').html($(this).val().length);
});
$('.counter-text').keypress(function countText() {
    $(this).next('.text-count').find('.counter-text-value').html($(this).val().length);
});

$('.date-input .button-icon').on('click',function(){
    $(this).parents('.date-input').find('input').focus();
})

$(".select-employee").on('select2-loaded', function (e) {
    alert('ada');
});
