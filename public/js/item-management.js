jQuery(function($){
  $('#itemTable').DataTable( {
    // responsive: true,
    scrollX: true,
    "pageLength": 5,
    columns: [
      { width: '5%' },
      { width: '10%' },
      { width: '55%' },
      { width: '5%' },
      { width: '5%' },
      { width: '20%' }
    ]


  } );

  oTable = $('#itemTable').DataTable();

  $('#searchItem').on('keyup change', function(){
    oTable.search($(this).val()).draw();
  })

  $('.btn-adv').on('click', function () {
    $('.table-search.has-advance').slideToggle();
    $('.table-advance').slideToggle();
  })

  $('.btn-table').on('click', function () {
    $('.table-search.has-advance').slideToggle();
    $('.table-advance').slideToggle();
  })

});
