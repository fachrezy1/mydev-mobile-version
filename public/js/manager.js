;(function($){

  $(".add-more").click(function () {
    $(".field-group.has-more:last").clone().appendTo(".more-item");
  });

  $(".more-item").on('click', '.remove-item', function () {
    $(".field-group.has-more:last").remove();
  });


  $(".more-session").click(function () {
    $(".session-box:last").clone().appendTo(".session-list");
  });

  $(".remove-session").click(function () {
    $(this).parent().hide();
    return false;
  });

  $('.date-only').datetimepicker({
    format: 'L'
  });
















})(jQuery);


