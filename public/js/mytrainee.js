jQuery(function($){
    $('.traineeTable').DataTable( {
        // responsive: true,
        scrollX: true,
        "pageLength": 5

    } );

    $('.exportTable').DataTable( {
        dom: 'Bfrtip',
        scrollX : true,
        pageLength : 5,
        paging: false,
        buttons: [
            'csv', 'excel', 'pdf', 'print'
        ]
    } );

    oTable = $('.traineeTable').DataTable();

    $('#searchTable').on('keyup change', function(){
        oTable.search($(this).val()).draw();
    })

});
