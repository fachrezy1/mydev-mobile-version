jQuery(function($){
  $('.btn-redeem').click(function (){
    $('html, body').animate({
      scrollTop: $(".bottom-point").offset().top - 90
    }, 1000)
  })

  $('.redeemOpt').click(function (){
    $('html, body').animate({
      scrollTop: $(".btn-redeem").offset().top - 320
    }, 1000)
  })

  $('.show-opt button').on('click',function(e) {

    if ($(this).hasClass('grid')) {

      $('.box').removeClass('list').addClass('grid');
    }
    else if($(this).hasClass('list')) {

      $('.box').removeClass('grid').addClass('list');
    }
  });


  var $tpoints = $('.filter-opt > .btn').click(function() {
    if (this.id == 'allPoints') {
      $('.box > div').fadeIn(450);
    } else {
        let sort = $(this).data('sort');
        // custom from dev
        var items = $('.card');

        if (sort === 'small') {
            items.sort(function(a, b){
                return +$(a).data('small') - +$(b).data('small');
            });
        } else if(sort === 'popular') {
            console.log('sini');
            items.sort(function(a, b){
                return +$(b).data('popular') - +$(a).data('popular');
            });
        } else if (sort === 'big') {
            items.sort(function(a, b){
                return +$(b).data('big') - +$(a).data('big');
            });
        }


        $('.point-box .box').html('');
        items.appendTo('.point-box .box');
    }
    $tpoints.removeClass('active');
    $(this).addClass('active');
  })



});

