jQuery(function($){



    $('#courseEmployee, #purchaseTable').DataTable( {
        // responsive: true,
        scrollX: true,
        "pageLength": 5
    } );



    oTable = $('#courseEmployee, #purchaseTable').DataTable();

    $('#searchItem').on('keyup change', function(){
        oTable.search($(this).val()).draw();
    })

    $('.btn-adv, .btn-table').on('click', function () {
        $('.table-option').toggleClass('on');
        return false;
    })





    var optionsOne = {
        type: 'polarArea',
        data: {
            labels: ["Public", "Public Speaker", "Mentoring", "Assignment"],
            datasets: [
                {
                    label: "Indosat oordeoo",
                    backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                    data: [2478,5267,734,784,433]
                }
            ]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Bugdet for trainee Period 2018'
            }
        }
    }

    var optionsTwo = {
        type: 'radar',
        data: {
            labels: ["Te", "ddy", "Kus", "Ma", "Na"],
            datasets: [
                {
                    label: "1950",
                    fill: true,
                    backgroundColor: "rgba(179,181,198,0.2)",
                    borderColor: "rgba(179,181,198,1)",
                    pointBorderColor: "#fff",
                    pointBackgroundColor: "rgba(179,181,198,1)",
                    data: [8.77,55.61,21.69,6.62,6.82]
                }, {
                    label: "2050",
                    fill: true,
                    backgroundColor: "rgba(255,99,132,0.2)",
                    borderColor: "rgba(255,99,132,1)",
                    pointBorderColor: "#fff",
                    pointBackgroundColor: "rgba(255,99,132,1)",
                    pointBorderColor: "#fff",
                    data: [25.48,54.16,7.61,8.06,4.45]
                }
            ]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Trainee'
            }
        }
    }


    var optionsThree = {
        type: 'doughnut',
        data: {
            labels: ["Green", "Yellow", "Red", "Purple", "Blue"],
            datasets: [{
                data: [1, 2, 3, 4, 5],
                backgroundColor: [
                    'green',
                    'yellow',
                    'red',
                    'purple',
                    'blue',
                ],
                labels: [
                    'green',
                    'yellow',
                    'red',
                    'purple',
                    'blue',
                ]
            }, {
                data: [6, 7, 8],
                backgroundColor: [
                    'black',
                    'grey',
                    'lightgrey'
                ],
                labels: [
                    'black',
                    'grey',
                    'lightgrey'
                ],
            }, ]
        },
        options: {
            responsive: true,
            legend: {
                display: false,
            }
        }
    }

    /*

    var ctxThree = document.getElementById('chartThreeContainer').getContext('2d');
    new Chart(ctxThree, optionsThree);

    */











});




