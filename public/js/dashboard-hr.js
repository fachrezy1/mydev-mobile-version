jQuery(function($){

  var optionsOne = {
    type: 'line',
    data: {
      labels: ["Jan", "Feb", "Mar", "Jun", "Jul", "Aug", "Sep", "Nov", "Dec"],
      datasets: [
        {
          label: 'Course',
          data: [4, 8, 12, 10, 6, 2, 14, 18, 24, 34, 28, 40],
          borderWidth: 1
        },
        {
          label: 'Cost',
          data: [3, 9, 16, 8, 10, 4, 8, 12, 27, 14, 22, 19],
          borderWidth: 1
        }
      ]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            reverse: false
          }
        }]
      }
    }
  }




  var ctxOne = document.getElementById('topProgress').getContext('2d');
  new Chart(ctxOne, optionsOne);


  $('#requestTable').DataTable( {
    // responsive: true,
    scrollX: true,
    "pageLength": 5
  });







});

