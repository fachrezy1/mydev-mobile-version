jQuery(function($){
  $('.btn-adv, .btn-table').on('click', function () {
    $('.table-option').toggleClass('on');
    return false;
  })



  $('#bannerTable').DataTable( {
    // "responsive": true,
    "scrollX": true,
    "pageLength": 5,
    columns: [
      { width: '5%' },
      { width: '10%' },
      { width: '52%' },
      { width: '5%' },
      { width: '18%' }
    ]
  } );

  $('#pointsTable, #employeeTable, #reportTable').DataTable( {
    // "responsive": true,
    "scrollX": true,
    "pageLength": 5
  } );



  oTable = $('#bannerTable,  #employeeTable, #reportTable').DataTable();

  $('#searchItem').on('keyup change', function(){
    oTable.search($(this).val()).draw();
  })







});
