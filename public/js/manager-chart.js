jQuery(function($){






  var optionsFour = {
    type: 'line',
    data: {
      labels: ["January", "February", "March", "April", "May", "June", "July"],
      datasets: [{
        label: "My First dataset1",
        data: [65, 0, 80, 81, 56, 85, 40],
        fill: false
      }]
    }

  };

  var optionsFive = {
    type: 'bar',
    data: {
      labels: ["January", "February", "March", "April", "May", "June", "July"],
      datasets: [{
        label: "My First dataset",
        data: [65, 0, 80, 81, 56, 85, 40],
        fill: false
      },
        {
          label: "My Second dataset",
          data: [95, 10, 0, 11, 76, 65, 30],
          fill: false
        }]
    },
    options: {
      legend: {
        display: true
      },
      responsive: true
    }

  };


  var optionsThree = {
    type: 'line',

    data: {
      labels: ["May 1", "May 2", "May 3", "May 4", "May 5", "May 6",
        "May 7", "May 8", "May 9", "May 10", "May 11", "May 12"],
      datasets: [
        {
          label: "Blue",
          fill: true,
          backgroundColor: "rgba(32, 162, 219, 0.3)",
          data: [210, 220, 260, 350, 550, 550, 560, 590, 600, 610, 610, 620]
        },
        {
          label: "Red",
          fill: true,
          backgroundColor: "rgba(196, 93, 105, 0.3)",
          data: [100, 120, 130, 140, 160, 230, 270, 400, 410, 420, 430, 450]
        }
      ]
    },
    options: {
      scales: {
        xAxes: [{
          stacked: true,
        }],
        yAxes: [{
          stacked: true,
        }]
      },
      legend: {
        display: false,
      },
      responsive: true
    }
  };




  var optionsOne = {
    type: 'pie',

    data: {
      labels: ["Green", "Blue"],
      datasets: [{
        backgroundColor: [
          "#2ecc71",
          "#3498db"
        ],
        data: [19, 12]
      }]
    },
    options: {
      legend: {
        display: false
      },
      responsive: true
    }
  };


  var  optionsMgr = {
    type: 'bar',
    data: {
      labels: ['Sub Division A', 'Sub Division B', 'Sub Division C', 'Sub Division D', 'Sub Division E', 'Sub Division F'],
      datasets: [
        {
          label: '2017 Period Course Assign',
          fillColor: '#382765',
          data: [2500, 1902, 1041, 610, 1245, 952]
        },
        {
          label: '2018 Period Course Assign',
          fillColor: '#7BC225',
          data: [3104, 1689, 1318, 589, 1199, 1436]
        }
      ]
    },

    options: {
      responsive: true,
      scales: {
        xAxes: [{
          gridLines: {
            display:false
          }
        }],
        yAxes: [{
          gridLines: {
            display:false
          }
        }]
      }
    }
  };

  var insight = {
    type: 'line',
    data: {
      labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
      datasets: [
        {
          label: '# of Votes',
          data: [12, 19, 3, 5, 2, 3],
          borderWidth: 1
        },
        {
          label: '# of Points',
          data: [7, 11, 5, 8, 3, 7],
          borderWidth: 1
        }
      ]
    },
    options: {
      responsive: true,
      scales: {
        xAxes: [{
          gridLines: {
            display:false
          }
        }],
        yAxes: [{
          gridLines: {
            display:false
          },
          ticks: {
            reverse: false
          }
        }]
      }
    }

  };

  var configProgress = {
    type: 'line',
    data: {
      labels: ["January", "February", "March", "April", "May", "June", "July"],
      datasets: [{
        fillColor: "rgba(120,120,120,0.2)",
        strokeColor: "rgba(120,120,120,1)",
        pointColor: "rgba(120,120,120,1)",
        pointStrokeColor: "#fff",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(120,120,120,1)",
        data: [65, 35, 80, 81, 56, 85, 40]
      }]
    },
    options: {
      responsive: true,
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          display: false,
          gridLines: {
            display:false
          },

          ticks: {
            display: false
          }
        }],
        yAxes: [{
          display: false,
          gridLines: {
            display:false
          },
          ticks: {
            display: false
          }
        }]
      }
    }
  };

 /* var ctxTwo = document.getElementById('courseInsight').getContext('2d');
  new Chart(ctxTwo, insight);*/


  var ctxPro = document.getElementById("courseProgress").getContext("2d");
  new Chart(ctxPro, configProgress);









  var ctxMgr = document.getElementById('chartFirstMgr').getContext('2d');
  new Chart(ctxMgr, optionsMgr);

  /*var ctxOne = document.getElementById('chartPieMgr').getContext('2d');
  new Chart(ctxOne, optionsOne);*/





});

