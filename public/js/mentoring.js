;(function($){

  $(".add-more").click(function () {
    $(".field-group.has-more:last").clone().appendTo(".more-item");
  });

  $(".more-item").on('click', '.remove-item', function () {
    $(".field-group.has-more:last").remove();
  });

  var $sigdiv = $("#signature").jSignature({'UndoButton':false, color:"#222",lineWidth:2, 'background-color': 'transparent',
    'decor-color': 'transparent'});
  var $sigdiv2 = $("#signature2").jSignature({'UndoButton':false, color:"#222",lineWidth:2, 'background-color': 'transparent',
    'decor-color': 'transparent'});
  var $sigdiv3 = $("#signature3").jSignature({'UndoButton':false, color:"#222",lineWidth:2, 'background-color': 'transparent',
    'decor-color': 'transparent'});
  var $sigdiv4 = $("#signature4").jSignature({'UndoButton':false, color:"#222",lineWidth:2, 'background-color': 'transparent',
    'decor-color': 'transparent'});

  $('#click').click(function(){
    var data = $sigdiv.jSignature('getData', 'image');
    //$('#output').val(data);

    $('#sign_prev').attr('src',"data:"+data);
    $('#sign_prev').show();
  });

  $('#clear').click(function (e) {
    $("#signature").jSignature('clear');
    $('#sign_prev').hide();
  });

  $('#click2').click(function(){
    var data = $sigdiv2.jSignature('getData', 'image');
    //$('#output').val(data);

    $('#sign_prev2').attr('src',"data:"+data);
    $('#sign_prev2').show();
  });

  $('#clear2').click(function (e) {
    $("#signature2").jSignature('clear');
    $('#sign_prev2').hide();
  });

  $('#click3').click(function(){
    var data = $sigdiv3.jSignature('getData', 'image');
    //$('#output').val(data);

    $('#sign_prev3').attr('src',"data:"+data);
    $('#sign_prev3').show();
  });

  $('#clear3').click(function (e) {
    $("#signature3").jSignature('clear');
    $('#sign_prev3').hide();
  });

  $('#click4').click(function(){
    var data = $sigdiv4.jSignature('getData', 'image');
    //$('#output').val(data);

    $('#sign_prev4').attr('src',"data:"+data);
    $('#sign_prev4').show();
  });

  $('#clear4').click(function (e) {
    $("#signature4").jSignature('clear');
    $('#sign_prev4').hide();
  });

  $(".more-session").click(function () {
    $(".session-box:last").clone().appendTo(".session-list");
  });

  $(".remove-session").click(function () {
    $(this).parent().hide();
    return false;
  });

  $('.date-only').datetimepicker({
    format: 'L'
  });
















})(jQuery);


